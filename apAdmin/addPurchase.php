  <?php 
  error_reporting(0);
  if(isset($_GET['purchase_id']) && $_GET['purchase_id'] !="")
  {
    $purchase_id = $_GET['purchase_id'];
    $q = $d->select("purchase_master","purchase_id='$purchase_id'");
      $data = mysqli_fetch_array($q);
      extract($data);
      $product_id_arr = array();
  }
  extract(array_map("test_input", $_POST));
  if (isset($edit_purchase)) {
      /* $q = $d->select("purchase_master","purchase_id='$purchase_id'");
      $data = mysqli_fetch_array($q);
      extract($data);
      $product_id_arr = array(); */
     
  }
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Add Purchase</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <!-- <a href="addSiteBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
      </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="addRequirementsForm" action="controller/PurchaseController.php" enctype="multipart/form-data" method="post">
                  <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Site <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" required="" class="form-control single-select" id="site_id" name="site_id">
                                    <option value="">-- Select --</option> 
                                    <?php 
                                        $floor=$d->select("site_master","society_id='$society_id' AND site_delete=0 AND site_active_status=0 ");  
                                        while ($floorData=mysqli_fetch_array($floor)) {
                                    ?>
                                    <option <?php if(isset($data['site_id']) && $data['site_id'] ==$floorData['site_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['site_id']) && $floorData['site_id'] !=""){ echo $floorData['site_id']; } ?>"><?php if(isset($floorData['site_name']) && $floorData['site_name'] !=""){ echo $floorData['site_name']; } ?></option> 
                                    <?php } ?>
                                </select>  
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text"class="form-control single-select" id="vendor_id" name="vendor_id">
                                    <option value="">-- Select --</option> 
                                    <?php 
                                        $floor=$d->select("local_service_provider_users"," service_provider_status=0 AND service_provider_delete_status=0 AND society_id = $society_id");  
                                        while ($floorData=mysqli_fetch_array($floor)) {
                                    ?>
                                    <option <?php if(isset($data['vendor_id']) && $data['vendor_id'] ==$floorData['service_provider_users_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] !=""){ echo $floorData['service_provider_users_id']; } ?>"><?php if(isset($floorData['service_provider_name']) && $floorData['service_provider_name'] !=""){ echo $floorData['service_provider_name'] ; } ?></option> 
                                    <?php } ?>
                                </select>  
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">PO Number </label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <input type="text" class="form-control" placeholder="PO Number" id="po_number" name="po_number" value="<?php if($data['po_number'] !=""){ echo $data['po_number']; } ?>">
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Description	 </label>
                            <div class=" col-md-4" id="">
                              <textarea id="purchase_descripion" name="purchase_descripion" class="form-control "><?php if($data['purchase_descripion'] !=""){ echo $data['purchase_descripion']; } ?></textarea> 
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row w-100 mx-0">
                            <button type="button" onclick="addRequirements()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Req Product </button>
                        </div> 
                    </div>
                  </div>
                
                  <div class="table-responsive">
                  
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Product Name</th>
                              <th>Category</th>
                              <th>Sub Category</th>
                              <th>Measure Unit</th>
                              <th>Quantity</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <?php if(isset($purchase_id) && $purchase_id !=""){?>
                      <tbody>
                          <?php 
                          $product_id_arr = array();
                          //$RequirementsQuery = $d->select("purchase_product_master,product_master,product_category_master,product_sub_category_master,unit_measurement_master", "product_master.product_category_id=product_category_master.product_category_id AND product_master.product_sub_category_id=product_sub_category_master.product_sub_category_id AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND purchase_product_master.purchase_id='$purchase_id'");
                          $RequirementsQuery = $d->select("purchase_product_master,product_master,product_category_master,product_sub_category_master,unit_measurement_master", "purchase_product_master.product_id=product_master.product_id AND product_category_master.product_category_id=product_master.product_category_id AND product_master.product_sub_category_id=product_sub_category_master.product_sub_category_id AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND purchase_product_master.purchase_id='$purchase_id'");
                          while ($RequirementsData=mysqli_fetch_array($RequirementsQuery)) {
                            $product_id_arr[$RequirementsData['product_id']]=$RequirementsData['quantity'];
                            //array_push($product_id_arr,$RequirementsData);
                            //print_r($RequirementsData);
                          ?>
                          <tr>
                              <td><?php if(isset($RequirementsData['product_name']) && $RequirementsData['product_name'] !=""){ echo $RequirementsData['product_name']; } ?></td>
                              <td><?php if(isset($RequirementsData['category_name']) && $RequirementsData['category_name'] !=""){ echo $RequirementsData['category_name']; } ?></td>
                              <td><?php if(isset($RequirementsData['sub_category_name']) && $RequirementsData['sub_category_name'] !=""){ echo $RequirementsData['sub_category_name']; } ?></td>
                              <td><?php if(isset($RequirementsData['unit_measurement_name']) && $RequirementsData['unit_measurement_name'] !=""){ echo $RequirementsData['unit_measurement_name']; } ?></td>
                              <td><?php if(isset($RequirementsData['quantity']) && $RequirementsData['quantity'] !=""){ echo $RequirementsData['quantity']; } ?></td>
                              <td>
                                <button type="button" onclick ="editPurchaseProduct(<?php if(isset($RequirementsData['puchase_product_id']) && $RequirementsData['puchase_product_id'] !=''){ echo $RequirementsData['puchase_product_id']; } ?>)" class="btn btn-sm btn-primary mr-1"><i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn btn-sm btn-danger" onclick="DeletePurchaseProduct(<?php if(isset($RequirementsData['puchase_product_id']) && $RequirementsData['puchase_product_id'] !=''){ echo $RequirementsData['puchase_product_id']; } ?>)" ><i class="fa fa-trash-o"></i></button>

                              </td>
                          </tr>
                          <?php
                          }
                          ?>
                      </tbody>
                      <?php }?>
                     
                        <tbody id="showFilterData" class="purchase_requirements">
                         
                      </tbody>
                     
                    </table>
                  </div>

                  <div class="mt-3 text-center">
                      <input type="hidden" id="addPurchase" name="addPurchase" value="addPurchase">
                      <input type="hidden" id="requirements" name="requirements" >
                      <input type="hidden" id="purchase_id" name="purchase_id" value="<?php if(isset($purchase_id) && $purchase_id !=""){echo $purchase_id;} ?>">
                      <button type="submit" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"> Save </button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

  </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

<div class="modal fade" id="addRequirementsModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Requirements</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
        <form id="addRequirements">
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Category <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="product_category_id" name="product_category_id" onchange="getSubCategoryById(this.value);">
                        <option value="">-- Select --</option> 
                        <?php 
                            $floor=$d->select("product_category_master","society_id='$society_id' AND product_category_status=0 AND product_category_delete = 0");  
                            while ($floorData=mysqli_fetch_array($floor)) {
                        ?>
                        <option <?php if(isset($data['product_category_id']) && $data['product_category_id'] ==$floorData['product_category_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['product_category_id']) && $floorData['product_category_id'] !=""){ echo $floorData['product_category_id']; } ?>"><?php if(isset($floorData['category_name']) && $floorData['category_name'] !=""){ echo $floorData['category_name']; } ?></option> 
                        <?php } ?>
                    </select>                  
                </div>
           </div> 

           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Sub Category </label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text"  class="form-control single-select" id="product_sub_category_id" name="product_sub_category_id" onchange="getProductByCategoryAndSubCategoryId(this.value);">
                        <option value="">-- Select --</option> 
                        
                    </select>                  
                </div>
           </div> 
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Product <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="product_id" name="product_id">
                        <option value="">-- Select --</option> 
                        
                    </select>                  
                </div>
           </div>                
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">QTY <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text" min="0" class="form-control onlyNumber" placeholder="QTY" id="quantity" name="quantity" value="<?php if($data['product_brand'] !=""){ echo $data['product_brand']; } ?>">
               </div>                   
           </div> 
                             
           <div class="form-footer text-center">
             <button id="addSiteBtn" name="addSiteBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
             <input type="hidden" id="key" value="">
             <input type="hidden" id="product_old_id" value="">
             <input type="hidden" id="purchase_product_id" value="">
             <button id="addSiteBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           </div>

        </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/validate7.js"></script>
<script src="assets/js/purchase.js"></script>

<script type="text/javascript">

function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
    return false;
}
</script>
<style>
.hideupdate{
  display:none;
}

</style>