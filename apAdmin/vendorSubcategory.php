<?php
extract(array_map("test_input" , $_REQUEST));
?>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
      <div class="col-sm-4 col-12">
        <h4 class="page-title">Sub Category</h4>
     </div>
      <div class="col-sm-5 col-8">
       <form action="" method="get" id="searchbycatform">
          <select type="text" onchange="this.form.submit();" name="searchcatid" id="searchcatid"  class="single-select form-control" style="width: 100%">
            <option value="">-- All --</option>
             <?php $q12=$d->select("local_service_provider_master","","ORDER BY service_provider_category_name ASC");
            while($row12=mysqli_fetch_array($q12)){ ?> 
            <option value="<?php echo $row12['local_service_provider_id']; ?>" <?php if( isset($_REQUEST) && isset($_REQUEST['searchcatid'])){ if((int)$_REQUEST['searchcatid'] == $row12['local_service_provider_id']){ echo "selected"; } } ?>><?php echo $row12['service_provider_category_name']; ?></option>
            <?php }?>
          </select>
       </form>
      </div>
     <div class="col-sm-3 col-4">
          <a href="#" data-toggle="modal" data-target="#addCategory" class="btn  btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
      </div>
     </div>
    <!-- End Breadcrumb-->
     <div class="row" style="padding-top: 25px;">
        <?php 
          $where="local_service_provider_sub_master.local_service_provider_id=local_service_provider_master.local_service_provider_id";
        if( isset($_REQUEST) && isset($_REQUEST['searchcatid']) && $_REQUEST['searchcatid']!=''){
          $maincatid = (int)$_REQUEST['searchcatid'];
          $where .= " AND local_service_provider_sub_master.local_service_provider_id='$maincatid'";
          // echo $where;
        }
        $q=$d->select("local_service_provider_master,local_service_provider_sub_master",$where,"ORDER BY local_service_provider_sub_master.service_provider_sub_category_name ASC");
        if(mysqli_num_rows($q)>0) {
          while($row=mysqli_fetch_array($q)){ 
            // check usage 
            // $cq= $d->select("local_service_provider_users_category","sub_category_id='$row[local_service_provider_sub_id]'");
            // $totalAsing=  mysqli_num_rows($cq);

            ?> 
          <div class="col-lg-3">
           <div class="card">
              <img height="150" class="card-img-top" src="../img/local_service_provider/local_service_cat/<?php echo $row['service_provider_sub_category_image']; ?>" alt="<?php echo $row['service_provider_sub_category_name']; ?>">
              <div class="p-2 text-center">
                 <h5 class="card-title text-primary"><?php echo $row['service_provider_sub_category_name']; ?></h5>
                 <i><?php echo $row['service_provider_category_name']; ?></i>
                 <hr>
                 <a data-toggle="modal" data-target="#editCategory" href="javascript:void();" onclick="editSubCategory('<?php echo $row['local_service_provider_sub_id']; ?>','<?php echo $row['service_provider_sub_category_name']; ?>','<?php echo $row['service_provider_sub_category_image']; ?>');" class="btn btn-sm btn-primary shadow-primary">Edit</a>
                 <?php
                  $totalAsing = $d->count_data_direct("users_category_id","local_service_provider_users_category","sub_category_id='$row[local_service_provider_sub_id]'");
                  if($totalAsing<=0){
                 ?>
                 <a href="javascript:void();" onclick="deleteSpSubCategory('<?php echo $row['local_service_provider_sub_id']; ?>');" class="btn btn-sm btn-danger shadow-primary">Delete</a>
               <?php } ?>
              </div>
           </div>
        </div>
        <?php } } else {
          echo "<img src='img/no_data_found.png'>";
        } ?>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  
<div class="modal fade" id="addCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Sub Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="personal-info2" action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"> Category  *</label>
                    <div class="col-sm-8">
                      <select required="" type="text" name="local_service_provider_id"  class="single-select form-control">
                        <option value="">-- Select --</option>
                         <?php $q=$d->select("local_service_provider_master","");
                        while($row=mysqli_fetch_array($q)){ ?> 
                        <option value="<?php echo $row['local_service_provider_id']; ?>"><?php echo $row['service_provider_category_name']; ?></option>
                        <?php }?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Sub Category Name *</label>
                    <div class="col-sm-8">
                      <input required="" type="text" name="service_provider_sub_category_name"  class="form-control text-capitalize">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image *</label>
                    <div class="col-sm-8">
                      <input required="" type="file" name="service_provider_sub_category_image"  class="form-control-file border">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <button type="submit" name="addSubCategory" value="addSubCategory" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->


<div class="modal fade" id="editCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Sub Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Sub Category Name *</label>
                    <div class="col-sm-8">
                      <input type="hidden" name="local_service_provider_sub_id" id="local_service_provider_sub_id">
                      <input required="" id="service_provider_sub_category_name" type="text" name="service_provider_sub_category_name"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image </label>
                    <div class="col-sm-8">
                      <input type="hidden" name="service_provider_sub_category_image_old" id="service_provider_sub_category_image">
                      <input  type="file" name="service_provider_sub_category_image"  class="form-control-file border">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <button type="submit" name="editSubCategory" value="editSubCategory" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->