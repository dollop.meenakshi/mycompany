<?php
if($_GET['f'] == "departments" && isset($_GET['changeSort']) && $_GET['changeSort'] == "yes" && isset($_GET['block_id']) && $_GET['block_id'] != '' && !empty($_GET['block_id']))
{
  $block_id = $_GET['block_id'];
  $block = $d->select("floors_master,block_master","block_master.block_id = floors_master.block_id AND block_master.block_id = '$block_id'","ORDER BY floor_sort");
?>
  <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-5">
        <h4 class="page-title"><?php echo $_GET['block_name']; ?></h4>
      </div>
      <div class="col-sm-3 col-7 text-right">
        <div class="btn-group float-sm-right">
        <a class="btn btn-sm btn-primary" href="departments"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row" id="floor-list">
      <?php
        while ($block_details = $block->fetch_assoc())
        {
        ?>
          <div class="col-lg-3 col-md-6 col-12 floorBox" data-post-id="<?php echo $block_details["floor_id"]; ?>">
            <div class="card">
              <div class="card-header text-uppercase">
                <?php echo $block_details['block_name']; ?>
              </div>
              <div class="card-body">
                <?php echo $block_details['floor_name']; ?>
              </div>
            </div>
          </div>
      <?php
        }
      ?>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
<?php
}
else
{
?>
 <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-md-4 col-5">
        <h4 class="page-title"> <?php echo $xml->string->floors; ?></h4>
      </div>
      <div class="col-md-4 d-none d-lg-block">
          <?php if ($_GET['changeOrder']=='yes') {
            $dragId = "department-image-list";
           ?>
          <span class="text-warning"><b>Drag to change order </b></span>
          <?php } else { ?>
          <a  href="departments?changeOrder=yes" class="btn  btn-info btn-sm waves-effect waves-light"><i class="fa fa-swap mr-1"></i> Change Order</a>
          <?php } ?>
          
          
      </div>
      <div class="col-md-4 col-7 text-right">
        <div class="btn-group float-sm-right">
          <a href="department" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->multiple; ?> <?php echo $xml->string->floors; ?></a>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <?php
      $i=1;
      $q3 = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","");
      if(mysqli_num_rows($q3)>0)
      {
        while ($data=mysqli_fetch_array($q3))
        {
          $block_id=$data['block_id'];
          $block_name=$data['block_name'];
          $floorCount= $d->count_data_direct("floor_id","floors_master","society_id='$society_id' AND  block_id='$block_id'");
        ?>
          <div class="col-lg-4">
            <form action="addSingleDepartment" method="post">
              <div class="card">
                <div class="card-header text-uppercase">
                  <a href="#"><?php echo $data['block_name']; ?> <?php echo $xml->string->block; ?> (<?php echo $floorCount; ?>) <?php if($floorCount>0) { ?>
                  <input type="hidden" name="blockName" value="<?php echo $data['block_name']; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                  <input type="hidden" name="floorCount" value="<?php echo $floorCount; ?>">
                  <input type="submit" class="pull-right btn btn-sm btn-primary fa fa-plus" value="&#xf067" name="singleFloor"><?php } ?></a>
                 
                </div>
                <div class="">
                  <ul class="list-group maxCard <?php echo $dragId; ?>"  id="">
                    <?php
                    $fq=$d->select("floors_master","society_id='$society_id' AND block_id='$block_id' $blockAppendQueryFloor","ORDER BY floor_sort ASC");
                    if(mysqli_num_rows($fq)>0)
                    {
                      while ($floorData=mysqli_fetch_array($fq))
                      {
                        $countUsers=$d->count_data_direct("floor_id","users_master","society_id!=0 AND society_id='$society_id' AND block_id='$block_id' AND floor_id='$floorData[floor_id]'");
                        $floorName=mysqli_real_escape_string($con, $floorData['floor_name']);
                        ?>
                          <li class="list-group-item node-treeview1 departmentBox" data-post-id="<?php echo $floorData["floor_id"]; ?>" data-nodeid="4" style="color:undefined;background-color:undefined;"><span class="indent"></span><span class="icon glyphicon"></span><span class="icon node-icon"></span><?php echo $floorData['floor_name']; ?>
                        <?php
                        if ($countUsers==0)
                        {
                        ?>
                          <i title="Delete <?php echo $xml->string->floor; ?>" class="fa fa-trash-o pull-right" onclick="DeleteFloor('<?php echo $floorData['floor_id']; ?>');"></i>

                        <?php
                        }
                        else
                        {
                        ?>
                          <i title="Delete <?php echo $xml->string->floor; ?>" class="fa fa-trash-o pull-right" onclick="showError('Delete This <?php echo $xml->string->floor; ?> All users then delete this <?php echo $xml->string->floor; ?>..!')"></i>
                        <?php
                        }
                        ?>

                          <i title="Edit <?php echo $xml->string->floor; ?> Name" class="fa fa-pencil pull-right"  data-toggle="modal" data-target="#editFloor" onclick="editFloor('<?php echo $floorData['floor_id']; ?>','<?php echo $floorName; ?>');"></i>
                        </li>
                        <?php
                      }
                    }
                    else
                    {
                      echo "No ".$xml->string->floor." Added";
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </form>
          </div>
      <?php
        }
      }
      else
      {
        echo "<img src='img/no_data_found.png'>";
      }
      ?>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
<?php
}
?>



  <div class="modal fade" id="editFloor">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->floor; ?> <?php echo $xml->string->name; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="floorAdd" action="controller/floorController.php" method="post">
            <input type="hidden" id="floorId" name="floor_id">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->floor; ?> <?php echo $xml->string->name; ?>  <span class="text-danger">*</span> </label>
                    <div class="col-sm-8" id="PaybleAmount">
                      <input  type="text" id="oldFloorname" class="form-control" name="floor_name" required="" maxlength="30">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <input type="hidden" name="updateFloor" value="updateFloor">
                  <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
                </div>

          </form>
      </div>
     
    </div>
  </div>
</div><!--End Modal -->