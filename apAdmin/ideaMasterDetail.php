  <?php error_reporting(0);
  if(isset($_GET['id'])){
    $id = (int)$_GET['id'];
  }
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Idea Detail</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <!-- <div class="btn-group float-sm-right">
        <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a>
        <a href="holiday"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        
         <a href="javascript:void(0)" onclick="DeleteAll('deleteHoliday');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div> -->
     </div>
     </div>

    

      <div class="row">
        <div class="col-lg-12">
          <?php 
            $q=$d->select("idea_master, idea_category_master,users_master","users_master.user_id=idea_master.user_id AND idea_master.idea_category_id=idea_category_master.idea_category_id AND idea_master.society_id='$society_id' AND idea_master.idea_id='$id' $blockAppendQueryUser", "ORDER BY idea_master.idea_id DESC");
            while ($data=mysqli_fetch_array($q)) {
          ?>
          <div class="col-12 col-lg-12 col-xl-12">
              <div class="card profile-card-2">
              <?php if($data['idea_status'] == 0){
                  $status_class = "badge-info";
                  $status_value = "Pending";
                } 
                else if($data['idea_status'] == 1){
                  $status_class = "badge-success";
                  $status_value = "Approved";
                }else{
                  $status_class = "badge-danger";
                  $status_value = "Declined";
                }
                
                ?>
               
              
              <div style="width: 60px; position: relative;z-index: 950;left: 0;margin: 5px;" class="badge <?php echo $status_class; ?>"><?php echo $status_value; ?></div>             

                <div class="card-body pt-2">
                    <div class="col-6  float-left">
                      <lable class="card-title" >Idea Title:<lable>
                    </div>
                    <div class="col-4 float-left">
                       <h6 class=""><?php echo $data['idea_title']; ?></h6> 
                    </div>
                    <div class="col-6  float-left">
                      <lable class="card-title" >Idea Category:<lable>
                    </div>
                    <div class="col-4 float-left">
                      <h6 class=""><?php echo $data['idea_category_name']; ?></h6>
                    </div>
                    <div class="col-6  float-left">
                      <lable class="card-title" >Date:<lable>
                    </div>
                    <div class="col-4 float-left">
                      <h6 class=""><?php echo date("d M Y h:i A", strtotime($data['idea_created_date'])); ?></h6>
                    </div>
                    <div class="col-6  float-left">
                      <lable class="card-title" >Attachment:<lable>
                    </div>
                    <div class="col-4 float-left">
                        <?php if($data['idea_attachment'] != ''){?>
                        <h6 class=""><a href="../img/idea/<?php echo $data['idea_attachment']; ?>" target="_blank"><?php echo $data['idea_attachment']; ?></a></h6>
                      <?php }else{ ?>
                        <h6 class="">No attachment available!</h6>
                      <?php } ?>
                    </div>
                    <div class="col-6  float-left">
                      <lable class="card-title" >Description:<lable>
                    </div>
                    <div class="col-4 float-left">
                    <p class="text-primary" ><?php echo $data['idea_description']; ?></p>
                    </div>
                </div>
              </div>
            </div>
          <?php } ?>

        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<!-- <script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script> -->




<div class="modal fade" id="payBill">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Holiday Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="holiday" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>


<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">

$(document).ready( function () {
    $('#example').DataTable();
} );

//////////////////////holiday data 


function holidayModal(id)
{
  var holidayData="";
  $.ajax({
          url: "../api/HolidayDetails.php",
          cache: false,
          type: "POST",
          data: {
              getData:"getHolidayDetails",
              society_id:<?php echo $society_id; ?>,
              holiday_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            console.log(response)
            if(response.status==200) {              
              
              holidayData = '<div class="col-md-6 form-group mb-3 pl-5">'+
                              '<label for="exampleInputEmail1">Holiday Name : </label>'+
                              '<span class="d-block" >'+response.holiday.holiday_name+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 form-group mb-3 pl-5">'+
                              '<label for="exampleInputEmail1">Start Date : </label>'+
                              '<span class="d-block" >'+response.holiday.holiday_start_date+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 form-group mb-3 pl-5">'+
                              '<label for="exampleInputEmail1">End Date : </label>'+
                              '<span class="d-block" >'+response.holiday.holiday_end_date+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 form-group mb-3 pl-5">'+
                              '<label for="exampleInputEmail1">Holiday Description : </label>'+
                              '<span class="d-block" >'+response.holiday.holiday_description+'</span>'+
                            '</div>';
              $('#holiday').html(holidayData);
            } 
          }
        });
}






  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }





</script>.
