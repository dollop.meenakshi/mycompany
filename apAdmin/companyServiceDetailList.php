<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Service Detail List</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyServiceDetailList');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Service Detail List Name</th>
                        <th>Service Detail Name</th>                      
                        <th>Action</th>                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("company_service_details_master,company_service_detail_list_master","company_service_detail_list_master.society_id='$society_id' AND company_service_details_master.company_service_details_id=company_service_detail_list_master.company_service_details_id");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                       
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['company_service_detail_list_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_service_detail_list_id']; ?>">                         
                          
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_service_detail_list_name']; ?></td>
                       <td><?php echo $data['company_service_details_title']; ?></td>
                       <td><button type="submit" class="btn btn-sm btn-primary mr-2" onclick="CompanyServiceDetailListDataSet(<?php echo $data['company_service_detail_list_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Service Detail List</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addCompanyServiceDetailListForm" action="controller/CompanyServiceDetailListController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
              <select name="company_service_details_id" id="company_service_details_id" class="form-control single-select restFrm">
                        <option value="">-- Select Service --</option> 
                           <?php 
                             $qcsd=$d->select("company_service_details_master","society_id='$society_id'");  
                             while ($serviceData=mysqli_fetch_array($qcsd)) {
                           ?>
                         <option value="<?php echo  $serviceData['company_service_details_id'];?>" ><?php echo $serviceData['company_service_details_title'];?></option>
                         <?php } ?>
                 
                </select>
              </div>  
           </div>   
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail List Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
              <input type="text" class="form-control" placeholder="Service Detail List Name" id="company_service_details_title" name="company_service_details_title" value="">
              </div>  
           </div>                 
           <div class="form-footer text-center">
             <input type="hidden" id="company_service_details_id" name="company_service_details_id" value="" >
             <button id="addCompanyServiceDetailListBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addCompanyServiceDetailList"  value="addCompanyServiceDetailList">
             <button id="addCompanyServiceDetailListBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyServiceDetailListForm');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
