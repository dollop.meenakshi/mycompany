<?php error_reporting(0);

$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
if(isset($_REQUEST['uId']))
{
  $uId = (int)$_REQUEST['uId'];
}
else
{
  $uId = 0;
}

?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">User Track</h4>
      </div>
      
    </div>

    <form action="" class="branchDeptFilterWithUser">
        <div class="row pt-2 pb-2">
            <?php /// include('selectBranchDeptForFilter.php'); ?> 
            <div class="col-md-2 form-group">
            <label class="form-control-label">Branch </label>
            <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
              <option value="">-- Select --</option>
              <?php
              $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQuery");
              while ($blockData = mysqli_fetch_array($qb)) {
              ?>
                <option <?php if ($_GET['bId'] == $blockData['block_id']) {echo 'selected';} ?> value="<?php echo $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2 form-group">
            <label class="form-control-label">Department </label>
            <select name="dId" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value)" required="">
              <option value="">--select--</option>
              <?php
              $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]' $blockAppendQueryFloor");
              while ($depaData = mysqli_fetch_array($qd)) {
              ?>
                <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                          echo 'selected';
                        } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
              <?php } ?>

            </select>
          </div>
          <div class="col-md-2 form-group" id="">
            <label class="form-control-label">Employee </label>
            <select id="user_id" type="text" class="form-control single-select " name="uId">
              <option value="">--Select--</option>
              <?php
              if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                $userData = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId] $blockAppendQueryUser");
                while ($user = mysqli_fetch_array($userData)) {?>
                  <option <?php if (isset($uId) && $uId == $user['user_id']) { echo "selected";} ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {echo $user['user_id'];} ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") {
                                                echo $user['user_full_name'];} ?></option>
              <?php }
              } ?>
            </select>
          </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">From Date </label>
              <input type="text" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') {echo $_GET['from'];} else { } ?>">
            </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">To Date </label>
              <input type="text" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $_GET['toDate'];} else { } ?>">
            </div>
            <div class="col-md-2 form-group mt-auto">
             <!--  <input type="hidden" name="uId" value="<?php echo $uId;  ?>"> -->
              <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get Data">
            </div>
        </div> 
    </form>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php
                    $i = 1;
                   
                    if (isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
                      $dateFilterQuery = " AND user_track_master.user_track_date BETWEEN '$_GET[from]' AND '$_GET[toDate]'";
                      
                    }else
                    {
                        $limitSql  = " LIMIT 100";
                    }
                    $q = $d->selectRow("user_track_master.*,users_master.*,block_master.block_id,block_master.block_name,floors_master.floor_id,floors_master.floor_name","user_track_master LEFT JOIN users_master ON users_master.user_id=user_track_master.user_id LEFT JOIN floors_master ON users_master.floor_id=floors_master.floor_id LEFT JOIN block_master ON users_master.block_id=block_master.block_id","user_track_master.user_id=$uId $dateFilterQuery","ORDER BY user_track_master.user_track_id DESC  $limitSql");
                    $counter = 1;
                    
                  ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>User Name</th>
                        <th>Department</th>
                        <th>Track date</th>
                        <th>area</th>
                        <th>locality</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php while ($data = mysqli_fetch_array($q)) {?>
                    <tr>
                       <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['floor_name']."(".$data['block_name'].")"; ?></td>
                       <td><?php if($data['user_track_date'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($data['user_track_date'])); } ?></td>
                       <td><?php echo $data['area']; ?></td>
                       <td><?php echo $data['locality']; ?></td>
                       <td>
                          <div class="d-flex align-items-center">
                            <button type="submit" class="btn btn-sm btn-primary" onclick="userTrackData(<?php echo $data['user_track_id']; ?>)">
                             <i class="fa fa-eye"></i></button>

                             <!--  <input type="hidden" name="shift_time_id" value="<?php echo $data['shift_time_id']; ?>">
                              <input type="hidden" name="edit_shift_timing" value="edit_shift_timing">
                            <?php if ($data['shift_time_id'] > 0 && $data['shift_time_id'] != "") {?>
                              <button type="button" class="btn btn-sm btn-primary mr-1"   onclick="getUserShiftByFloorId(<?php echo $data['floor_id']; ?>,<?php echo $data['user_id']; ?>,<?php if ($data['shift_time_id'] > 0 && $data['shift_time_id'] != '') { echo $data['shift_time_id'] ; }?>)"><i class="fa fa-pencil"></i></button>
                             <?php }else
                             { ?>
                              <button type="button" class="btn btn-sm btn-info mr-1"   onclick="getUserShiftByFloorId(<?php echo $data['floor_id']; ?>,<?php echo $data['user_id']; ?>)"><i class="fa fa-plus"></i></button>
                            <?php } ?> -->
                          </div>
                        </td>

                    </tr>
                    <?php }?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->

    <div class="modal fade" id="userTrackDataModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Tracked Data</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <div class="row col-md-12 " id="userTrackDetail"></div>
            <div class="row col-md-12 ">
            <div class="map" id="trackUserMap" style="width: 100%; height: 280px;">
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
<script type="text/javascript">


function trackUser(d_lat, d_long) {
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var latitute = d_lat;
      var longitute = d_long;

      var map = new google.maps.Map(document.getElementById('trackUserMap'), {
        center: latlng,
        zoom: 13
      });
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: false,
        anchorPoint: new google.maps.Point(0, -29)
      });
      /* var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: latitute,
            lng: longitute
          },
          population: parkingRadition
        }
      }; */

      /* var input = document.getElementById('searchInput5');
      var geocoder = new google.maps.Geocoder();
      var autocomplete10 = new google.maps.places.Autocomplete(input);
      autocomplete10.bindTo('bounds', map); */
      var infowindow = new google.maps.InfoWindow();
      /* autocomplete10.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place5 = autocomplete10.getPlace();
        if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
        }

        // If the place5 has a geometry, then present it on a map.
        if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
        } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
        }

        marker.setPosition(place5.geometry.location);
        marker.setVisible(true);

        var pincode = "";
        for (var i = 0; i < place5.address_components.length; i++) {
          for (var j = 0; j < place5.address_components[i].types.length; j++) {
            if (place5.address_components[i].types[j] == "postal_code") {
              pincode = place5.address_components[i].long_name;
            }
          }
        }
        bindDataToForm(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name);
        infowindow.setContent(place5.formatted_address);
        infowindow.open(map, marker);

      }); */
      // this function will work on marker move event into map
      /*  google.maps.event.addListener(marker, 'dragend', function() {
         geocoder.geocode({
           'latLng': marker.getPosition()
         }, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             if (results[0]) {
               var places = results[0];
               var pincode = "";
               var serviceable_area_locality = places.address_components[4].long_name;
               for (var i = 0; i < places.address_components.length; i++) {
                 for (var j = 0; j < places.address_components[i].types.length; j++) {
                   if (places.address_components[i].types[j] == "postal_code") {
                     pincode = places.address_components[i].long_name;
                   }
                 }
               }
               bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality);
             }
           }
         });
       }); */
    }
    </script>