  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Visitor QR Code</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Visitor QR Code</li>
         </ol>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="#" data-toggle="modal" data-target="#addOwner"  class="btn btn-primary waves-effect btn-sm waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
        <a href="printQrcode.php" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Print QR Code</a>
       
        </button>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
                  <div class="row">
                   <?php 
                    $i=1;
                    $q = $d->select("qr_code_master" ,"society_id='$society_id'","");
                   if(mysqli_num_rows($q)>0) {

                    while ($data=mysqli_fetch_array($q)) {
                      extract($data); ?>  
                    
                    <div class="col-lg-2 text-center">
                      
                        <?php $qr_size          = "150x150";
                          $qr_content       = "$qr_code_master_id";
                          $qr_correction    = strtoupper('H');
                          $qr_encoding      = 'UTF-8';

                            //form google chart api link
                          $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                          ?>
                          <img src="<?php echo $qrImageUrl; ?>" alt="">
                          <form action="controller/qrController.php" method="post" >
                            <input type="hidden" name="qr_code_master_id" value="<?php echo $qr_code_master_id; ?>">
                            <?php if ($qr_code_status==0){ ?>
                              
                            <button type="submit" class="btn form-btn btn-danger btn-sm"> <i class="fa fa-trash-o"></i> Delete</button>
                            <?php } else { ?>
                            <button type="button" onclick="alert('This Qr code Is Assigned  to Visitor')" class="btn btn-warning btn-sm"> <i class="fa fa-trash-o"></i> Delete</button>
                            <?php } ?>
                          </form>
                         
                    </div> 
                    <?php } } else {
                      echo "<img src='img/no_data_found.png'>";
                    } ?>
                   




                </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



<div class="modal fade" id="addOwner">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Generate New QR Code</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="addUserDiv">
          <form id="signupForm" action="controller/qrController.php" method="post" enctype="multipart/form-data">
           
            <div class="form-group row">
              <label for="input-12" class="col-sm-4 col-form-label">No of QR Code <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input required="" min="1" type="text" max="500"   maxlength="10" class="form-control" id="userMobile" name="noOfqrCode">
              </div>
              
            </div>
            
          
           
            <div class="form-footer text-center">
                <button type="submit" id="socAddBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add User </button>
                <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div><!--End Modal -->