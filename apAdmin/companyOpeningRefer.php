<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Opening Refer</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">        
        <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyOpeningRefer');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>    
                        <th>Action</th>
                        <th>Position</th>                      
                        <th>Refer Name</th>                                               
                        <th>Refer By</th>                                               
                        <th>Gender</th>                      
                        <th>Contact No</th>                                               
                        <th>Email Id</th>                                               
                        <th>Experience</th>                                               
                        <th>Resume</th>  
                        <th>Performed By</th>  
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      if(isset($_GET) && $_GET['id']>0){
                        $appnedQuery = " AND company_opening_refer_master.company_current_opening_id='$_GET[id]'";
                      }
                      $q=$d->selectRow("company_opening_refer_master.*,company_current_opening_master.company_current_opening_title,users_master.user_full_name, performUser.user_full_name AS performUserName, performAdmin.admin_name AS performAdminName","company_current_opening_master,users_master, company_opening_refer_master LEFT JOIN users_master performUser ON performUser.user_id=company_opening_refer_master.company_opening_refer_status_change_by_id LEFT JOIN bms_admin_master performAdmin ON performAdmin.admin_id=company_opening_refer_master.company_opening_refer_status_change_by_id","company_opening_refer_master.society_id='$society_id' AND company_opening_refer_master.company_current_opening_id=company_current_opening_master.company_current_opening_id AND users_master.user_id=company_opening_refer_master.company_opening_refer_by $appnedQuery", "ORDER BY company_opening_refer_master.company_opening_refer_id DESC");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['company_opening_refer_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_opening_refer_id']; ?>">                      
                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td>
                            <?php if($data['company_opening_refer_status'] == 0){ ?>
                                <div class="d-flex align-items-center">
                                    <form action="controller/CompanyCurrentOpeningController.php" method="post">
                                        <input type="hidden" name="approvedReferStatus" value="approvedReferStatus">
                                        <input type="hidden" name="company_opening_refer_id" value="<?php echo $data['company_opening_refer_id']; ?>">
                                        <button title="Approved" type="submit" class="btn btn-sm btn-primary ml-1"><i class="fa fa-check"></i></button>
                                    </form>
                                    <button title="Rejected" type="button" class="btn btn-sm btn-danger ml-1" onclick="companyOpeningReferStatusChange('<?php echo $data['company_opening_refer_id']; ?>')"><i class="fa fa-times"></i></button>
                                </div>
                            <?php } elseif($data['company_opening_refer_status'] == 1){ ?>
                                <b><span class="badge badge-pill m-1 badge-success">Approved</span></b>
                            <?php } elseif($data['company_opening_refer_status'] == 2){ ?>
                                <b><span class="badge badge-pill m-1 badge-danger">Rejected</span></b>
                            <?php } ?>
                        </td>
                       <td><?php echo $data['company_current_opening_title']; ?></td>
                       <td><?php echo $data['company_opening_refer_name']; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['company_opening_refer_gender']; ?></td>
                       <td><?php echo $data['company_opening_refer_country_code'].' '.$data['company_opening_refer_contact_no']; ?></td>
                       <td><?php echo $data['company_opening_refer_email_id']; ?></td>
                       <td><?php echo $data['company_opening_refer_experience']; ?></td>
                       <td> 
                        <?php if($data['company_opening_refer_resume']!='') {?>
                        <a href="../img/bring_your_buddy_resume/<?php echo $data['company_opening_refer_resume']; ?>" target="_blank">Resume</a>
                        <?php } ?>
                        </td>
                       <td>
                            <?php if($data['company_opening_refer_status_change_type'] == 0){ ?>
                                <span class="badge badge-pill m-1 badge-success"><?php echo $data['performUserName'] ?></span><br>
                            <?php }else{ ?>
                                <span class="badge badge-pill m-1 badge-success"><?php echo $data['performAdminName'] ?></span><br>
                            <?php }
                            if($data['company_opening_refer_status'] == 2){
                                echo $data['company_opening_refer_admin_note'];
                            }
                            ?>
                        </td>
                       
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>
<div class="modal fade" id="referStatusRejectModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reject Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="referStatusReject" action="controller/CompanyCurrentOpeningController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="company_opening_refer_admin_note"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="rejectReferStatus"  value="rejectReferStatus">
                      <input type="hidden" name="company_opening_refer_id" id="company_opening_refer_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
