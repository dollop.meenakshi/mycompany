<?php error_reporting(0);



?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title">Assing Mulitple Branch/Department To Employee</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="assignMultiDptBrnch" action="controller/assignMultiBranchDptController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="br_id" id="br_id" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?> class="form-control single-select " onchange="getVisitorFloorByBlockId(this.value)">
                                                <option value="">Select Branch</option>
                                                <?php
                                                $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                                                while ($blockData = mysqli_fetch_array($qb)) {
                                                ?>
                                                <option <?php if (isset($gId) && $gId > 0) {
                                                        if ($Userdata['block_id'] == $blockData['block_id']) { echo 'selected';}
                                                    }?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Department <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="floor_id" id="floor_id" class="form-control single-select floor_idGeo" onchange="getUserByFloorId(this.value)" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?>>
                                                <option value="">All Department</option>
                                                <?php
                                                if (isset($gId) && $gId > 0) {
                                                    $qd = $d->select("floors_master,block_master", "floors_master.block_id='$Userdata[block_id]' AND block_master.block_id=floors_master.block_id $blockAppendFloor");
                                                while ($depaData = mysqli_fetch_array($qd)) {
                                                ?>
                                                <option <?php if ($Userdata['floor_id'] == $depaData['floor_id']) {echo 'selected';} ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">User <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select onchange="changeUserForAssign(this.value)" id="user_id" class="form-control single-select" name="user_id" <?php if (isset($gId) && $gId > 0) { echo "disabled";} ?>>
                                                <option value="">Select Employee</option>
                                                <?php
                                                if (isset($gId) && $gId > 0) {
                                                    $qU = $d->select("users_master,block_master", "users_master.floor_id='$Userdata[floor_id]' AND users_master.block_id = block_master.block_id $blockAppendUser");
                                                    while ($user = mysqli_fetch_array($qU)) {
                                                ?>
                                            <option <?php if (isset($gId) && $gId > 0) {
                                                                    if ($uId == $user['user_id']) {
                                                                        echo "selected";
                                                                    }
                                                                } ?> value="<?php echo $user['user_id']; ?>"><?php echo $user['user_full_name']; ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Assign Branch <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="emp_multi_branch" id="emp_multi_branch" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?> class="form-control single-select " onchange="geDeptByAssignBlock(this.value)">
                                                <option value="">Select Branch</option>
                                                <?php
                                                $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                                                while ($blockData = mysqli_fetch_array($qb)) {
                                                ?>
                                                <option <?php if (isset($gId) && $gId > 0) {
                                                        if ($Userdata['block_id'] == $blockData['block_id']) { echo 'selected';}
                                                    }?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label"> Assign Department </label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select multiple name="emp_multi_floor[]" id="emp_multi_floor" class="form-control empAccessForBranchMultiSelectCls single-select "  <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?>>
                                                <option value="">All Department</option>
                                                <?php
                                                if (isset($gId) && $gId > 0) {
                                                    $qd = $d->select("floors_master,block_master", "floors_master.block_id='$Userdata[block_id]' AND block_master.block_id=floors_master.block_id $blockAppendFloor");
                                                    while ($depaData = mysqli_fetch_array($qd)) {
                                                ?>
                                                <option <?php if ($Userdata['floor_id'] == $depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden"  name="user_id_old" id="user_id_old" value="<?php if (isset($uId) && $uId > 0) {echo $uId; } ?>">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <input  type="hidden" name="assignMultiDptBrnch" value="assignMultiDptBrnch">
                                        <input  type="hidden" name="employee_multiple_department_branch_id" value="<?php  ?>">
                                        <button id="" type="submit" class="btn btn-primary  sbmitbtnUserCmpAtnd "><i class="fa fa-check-square-o"></i> <?php if (isset($gId) && $gId > 0) {echo "Update";} else {echo "Add";} ?> </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="assets/js/select.js"></script>
<script src="assets/js/custom17.js"></script>
<script type="text/javascript">
   
</script>