<?php error_reporting(0);

$gId = (int)$_REQUEST['gId'];
if (isset($gId) && $gId > 0) {
    $q = $d->selectRow(
        'user_geofence.*,users_master.block_id,users_master.floor_id,users_master.user_id',
        "user_geofence,users_master",
        "user_geofence.geo_fance_id='$gId' AND users_master.user_id =user_geofence.user_id "
    );
    $Userdata = mysqli_fetch_assoc($q);
    $user_latitud = $Userdata['user_geofence_latitude'];
    $uId = $Userdata['user_id'];
    $user_longitude = $Userdata['user_geofence_longitude'];
    $user_address = $Userdata['user_geo_address'];
} else {
    $q = $d->selectRow('*', "society_master", "society_id='$society_id'");
    $data = mysqli_fetch_assoc($q);
    $user_latitud = $data['society_latitude'];
    $user_longitude = $data['society_longitude'];
    $user_address = $data['society_address'];
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title">Company Attendance Type</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="UsercmpAtnType" action="controller/updateUserGeoController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="br_id" id="br_id" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?> class="form-control single-select " onchange="getVisitorFloorByBlockId(this.value)">
                                                <option value="">Select Branch</option>
                                                <?php
                                                $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                                                while ($blockData = mysqli_fetch_array($qb)) {
                                                ?>
                                                    <option <?php if (isset($gId) && $gId > 0) {
                                                                if ($Userdata['block_id'] == $blockData['block_id']) {
                                                                    echo 'selected';
                                                                }
                                                            }
                                                            ?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Department <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="floor_id" id="floor_id" class="form-control single-select floor_idGeo" onchange="getUserByFloorId(this.value)" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?>>
                                                <option value="">All Department</option>
                                                <?php
                                                if (isset($gId) && $gId > 0) {
                                                    $qd = $d->select("floors_master,block_master", "floors_master.block_id='$Userdata[block_id]' AND block_master.block_id=floors_master.block_id $blockAppendFloor");
                                                    while ($depaData = mysqli_fetch_array($qd)) {
                                                ?>
                                                        <option <?php if ($Userdata['floor_id'] == $depaData['floor_id']) {
                                                                    echo 'selected';
                                                                } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">User <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select id="user_id" class="form-control single-select" name="user_id" <?php if (isset($gId) && $gId > 0) {echo "disabled";} ?>>
                                                <option value="">Select Employee</option>
                                                <?php
                                                if (isset($gId) && $gId > 0) {
                                                    $qU = $d->select("users_master,block_master", "users_master.floor_id='$Userdata[floor_id]' AND users_master.block_id = block_master.block_id $blockAppendUser");
                                                    while ($user = mysqli_fetch_array($qU)) { ?>
                                                        <option <?php if (isset($gId) && $gId > 0) {
                                                                    if ($uId == $user['user_id']) {
                                                                        echo "selected";
                                                                    }
                                                                } ?> value="<?php echo $user['user_id']; ?>"><?php echo $user['user_full_name']; ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label for="input-10" class="col-sm-12 col-form-label">Range(Meter) <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <input required autocomplete="off" type="text" value="<?php if (isset($gId) && $gId > 0) { echo  $Userdata['user_geofence_range'];} ?>" onkeyup="chnageUserRange()" min="5" id="user_geofence_range" name="user_geofence_range[]" class="form-control">
                                            <input type="hidden" id="textInput2" value="<?php if (isset($gId) && $gId > 0) {echo  $Userdata['user_geofence_range'];} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label for="input-10" class="col-sm-12 col-form-label">Work Location name <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <input required autocomplete="off" type="text" value="<?php if (isset($gId) && $gId > 0) { echo  $Userdata['work_location_name'];} ?>" minlength="2" maxlength="50" id="work_location_name" name="work_location_name[]" class="form-control">
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 px-4">
                                    <div class="row form-group">
                                        <input id="searchInput6" name="location_name[]" class="form-control" type="text" placeholder="Enter location" value="<?php if (isset($user_address) && $user_address != "") {
                                                         echo $user_address;} ?>">
                                        <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>
                                <div class=" col-md-12 addMoreMap">

                                </div>
                                <input type="hidden" value="<?php echo $user_latitud; ?>" readonly id="user_latitude" name="user_latitude[]" class="form-control">
                                <input type="hidden" value="<?php echo $user_longitude; ?>" readonly id="user_longitude" name="user_longitude[]" class="form-control">
                                <input type="hidden" name="updateUser" value="updateUser">
                                <input type="hidden" name="geo_fance_id" value="<?php if (isset($gId) && $gId > 0) {echo $gId;} ?>">
                                <input type="hidden" name="zoomValue" id="zoomValue" value="17">
                                <input type="hidden" name="zoomValueBlk" id="zoomValueBlk" value="17">
                                <input type="hidden" name="is_app_attendance_on" id="is_app_attendance_on">
                                <input type="hidden" name="user_id_old" id="user_id_old" value="<?php if (isset($uId) && $uId > 0) { echo $uId;} ?>">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button id="" type="submit" class="btn btn-primary  sbmitbtnUserCmpAtnd "><i class="fa fa-check-square-o"></i> <?php if (isset($gId) && $gId > 0) { echo "Update";} else {echo "Add";} ?> </button>
                                        <?php if ($gId == 0) { ?>
                                            <button id="" type="button" class="btn btn-primary addMoreRangeMap"><i class="fa fa-check-plus"></i> Add More </button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="assets/js/select.js"></script>
<script src="assets/js/custom17.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
<script type="text/javascript">
    var count = 0;
    var countAr = [];
    /*  var arr =  $( ":checkbox" )
         .map(function() {
             return this.id;
         })
         .get() */
    (function() {

        addMore = "";
        $('.addMoreRangeMap').click(function() {

            count += 1;
            addMore = `<div class="col-md-12 px-4">
                            <div class=" row form-group">
                            <input  placeholder="range" autocomplete="off" type="text" value="<?php if (isset($gId) && $gId > 0) {echo  $Userdata['user_geofence_range'];} ?>" onkeyup="chnageMoreUserRange(` + count + `)" min="5" id="user_geofence_range_` + count + `" name="user_geofence_range[]" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12 px-4">
                            <div class=" row form-group">
                            <input  placeholder="Work Location" autocomplete="off" type="text" value="<?php if (isset($gId) && $gId > 0) {echo  $Userdata['work_location_name'];} ?>"  minlength="2" maxlength="50" id="work_location_name_` + count + `" name="work_location_name[]" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo $user_latitud; ?>" readonly id="user_latitude_` + count + `" name="user_latitude[]" class="form-control">
                        <input type="hidden" value="17" readonly id="zoomValue_` + count + `"  class="form-control">
                                <input type="hidden" value="<?php echo $user_longitude; ?>" readonly id="user_longitude_` + count + `" name="user_longitude[]" class="form-control">
                        <div class="col-md-12 px-4">
                            <div class="row form-group">
                                <input id="searchInput_` + count + `" name="location_name[]" class="form-control" type="text" placeholder="Enter location" value="">
                                <div class="map" id="map_` + count + `" style="width: 100%; height: 200px;"></div>
                            </div>
                        </div>`;

            $('.addMoreMap').append(addMore);
            countAr.push(count);
            rangeValid();
            getCount()
        });
    })();


    function getCount() {
        for (let index = 0; index < countAr.length; index++) {
            var latitude = '<?php echo $user_latitud; ?>';
            var longitude = '<?php echo $user_longitude; ?>';
            initializeMore(latitude, longitude, countAr[index])
        }
    }



    function openModalForAdd() {
        $('.userDiv').show();
        $('#user_geofence_range').val("");
        $('.blockDiv').show();
    }

    // $('.single-select').select2();
    var society_attendace_type = $('#society_attendace_type').val();
    if (society_attendace_type != 0 || society_attendace_type != 5) {
        $('.blockList').show();
    } else {
        $('.blockList').hide();

    }
    if (society_attendace_type == 2) {
        $('.empList').show();
        $('#is_app_attendance_on').val(1);
    }

    function chnageMoreUserRange(id) {

        var latitude = $('#user_latitude_'+id).val();
        var longitude = $('#user_longitude_'+id).val();
        
        initializeMore(latitude, longitude, id);
    }

    function chnageBLockRange() {
        var latitude = $('#block_latitude').val();
        var longitude = $('#block_longitude').val();
        initialize(latitude, longitude);
    }

    <?php if (isset($_GET['bId']) && $_GET['bId'] != "") { ?>
        $('.locationRangeDiv').css('display', 'block');
        $('#block_id').prop('disabled', true);

    <?php }
    ?>
    <?php if (isset($_GET['gId']) && $_GET['gId'] != "") { ?>

        $('.UserlocationRangeDiv').css('display', 'block');
        $('.branchDiv').css('display', 'none');
        $('.locationRangeDiv').css('display', 'block');
        //getCompnayAttendaceUser(<?php echo $_GET['dId']; ?>);
    <?php }
    ?>


    ///////////////////////////////////user map
    function chnageUserRange() {
        var latitude = $('#user_latitude').val();
        var longitude = $('#user_longitude').val();
        initialize2(latitude, longitude);
    }
    $('#blockModal').on('hidden.bs.modal', function() {
        var latitude = '<?php echo $user_latitud; ?>';
        var longitude = '<?php echo $user_longitude; ?>';
        initialize2(latitude, longitude);
    })
    $().ready(function() {
        var latitude = '<?php echo $user_latitud; ?>';
        var longitude = '<?php echo $user_longitude; ?>';

        initialize2(latitude, longitude);
    });

    function initialize2(d_lat, d_long) {
        var latlng = new google.maps.LatLng(d_lat, d_long);
        var latitute = 23.037786;
        var longitute = 72.512043;
        zoomVal = $('#zoomValue').val();
        var map = new google.maps.Map(document.getElementById('map2'), {
            center: latlng,
            zoom: parseInt(zoomVal)
        });
        mapzoom = map.getZoom();
        /////////////////on zoom in out
        google.maps.event.addListener(map, 'zoom_changed', function() {

            var zoomLevel = map.getZoom();
            $('#zoomValue').val(zoomLevel);

            mapzoom = map.getZoom(); //to stored the current zoom level of the map

        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });
        // Add circle overlay and bind to marker
        user_geofence_range = $('#user_geofence_range').val();
        if (user_geofence_range == "") {
            user_geofence_range = 10;
        }
        var circle = new google.maps.Circle({
            map: map,
            radius: parseInt(user_geofence_range), // 10 miles in metres
            fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');
        var parkingRadition = 5;
        var citymap = {
            newyork: {
                center: {
                    lat: latitute,
                    lng: longitute
                },
                population: parkingRadition
            }
        };

        var input = document.getElementById('searchInput6');
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        autocomplete10.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place5 = autocomplete10.getPlace();
            if (!place5.geometry) {
                window.alert("Autocomplete's returned place5 contains no geometry");
                return;
            }

            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport) {
                map.fitBounds(place5.geometry.viewport);
            } else {
                map.setCenter(place5.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);

            var pincode = "";
            for (var i = 0; i < place5.address_components.length; i++) {
                for (var j = 0; j < place5.address_components[i].types.length; j++) {
                    if (place5.address_components[i].types[j] == "postal_code") {
                        pincode = place5.address_components[i].long_name;
                    }
                }
            }
            bindDataToForm2(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);

        });
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, 'dragend', function() {
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var places = results[0];
                        var pincode = "";
                        var serviceable_area_locality = places.address_components[4].long_name;
                        for (var i = 0; i < places.address_components.length; i++) {
                            for (var j = 0; j < places.address_components[i].types.length; j++) {
                                if (places.address_components[i].types[j] == "postal_code") {
                                    pincode = places.address_components[i].long_name;
                                }
                            }
                        }
                        bindDataToForm2(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality);
                    }
                }
            });
        });
    }

    function bindDataToForm2(address, lat, lng, pin_code, serviceable_area_locality) {
        // console.log(address);
        document.getElementById('user_latitude').value = lat;
        document.getElementById('searchInput6').value = address;
        document.getElementById('user_longitude').value = lng;
    }

    function initializeMore(d_lat, d_long, id) {
        console.log(id);
        var latlng = new google.maps.LatLng(d_lat, d_long);
        var latitute = 23.037786;
        var longitute = 72.512043;
        zoomVal = $('#zoomValue_' + id).val();

        var map = new google.maps.Map(document.getElementById('map_' + id), {
            center: latlng,
            zoom: parseInt(zoomVal)
        });
        mapzoom = map.getZoom();
        /////////////////on zoom in out
        google.maps.event.addListener(map, 'zoom_changed', function() {
            var zoomLevel = map.getZoom();
            $('#zoomValue_' + id).val(zoomLevel);
            mapzoom = map.getZoom(); //to stored the current zoom level of the map

        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });
        // Add circle overlay and bind to marker
        user_geofence_range = $('#user_geofence_range_' + id).val();
        if (user_geofence_range == "") {
            user_geofence_range = 10;
        }
        var circle = new google.maps.Circle({
            map: map,
            radius: parseInt(user_geofence_range), // 10 miles in metres
            fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');
        var parkingRadition = 5;
        var citymap = {
            newyork: {
                center: {
                    lat: latitute,
                    lng: longitute
                },
                population: parkingRadition
            }
        };

        var input = document.getElementById('searchInput_' + id);
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        autocomplete10.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place5 = autocomplete10.getPlace();
            if (!place5.geometry) {
                window.alert("Autocomplete's returned place5 contains no geometry");
                return;
            }
            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport) {
                map.fitBounds(place5.geometry.viewport);
            } else {
                map.setCenter(place5.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);
            var pincode = "";
            for (var i = 0; i < place5.address_components.length; i++) {
                for (var j = 0; j < place5.address_components[i].types.length; j++) {
                    if (place5.address_components[i].types[j] == "postal_code") {
                        pincode = place5.address_components[i].long_name;
                    }
                }
            }
            bindDataToFormMore(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name, id);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);

        });
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, 'dragend', function() {
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var places = results[0];
                        var pincode = "";
                        var serviceable_area_locality = places.address_components[4].long_name;
                        for (var i = 0; i < places.address_components.length; i++) {
                            for (var j = 0; j < places.address_components[i].types.length; j++) {
                                if (places.address_components[i].types[j] == "postal_code") {
                                    pincode = places.address_components[i].long_name;
                                }
                            }
                        }
                        bindDataToFormMore(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality, id);
                    }
                }
            });
        });
    }

    function bindDataToFormMore(address, lat, lng, pin_code, serviceable_area_locality, id) {
        // console.log(address);
        document.getElementById('user_latitude_' + id).value = lat;
        document.getElementById('searchInput_' + id).value = address;
        document.getElementById('user_longitude_' + id).value = lng;
    }

    /////////////////////////////////////

    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }

    function rangeValid(params) {
        $("[name^=user_geofence_range]").each(function() {
            console.log(this.id);
            $('#' + this.id).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter Range ",
                }
            });
        });
    }
</script>
<style>
    .locationRangeDiv {
        display: none;
    }

    .UserlocationRangeDiv {
        display: none;

    }
</style>