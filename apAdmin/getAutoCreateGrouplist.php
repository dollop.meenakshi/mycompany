<?php
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
extract($_POST);

$branchArray = array();
$departmentArray = array();
$zoneArray = array();
$levelArray = array();
$alq = $d->select("chat_group_master","group_type!=0");
while ($oldGroup=mysqli_fetch_array($alq)) {
	 if ($oldGroup['group_type']==1) {
	 	  array_push($branchArray, $oldGroup['group_common_id']);
	 } else if ($oldGroup['group_type']==2) {
	 		array_push($departmentArray, $oldGroup['group_common_id']);
	 } else if ($oldGroup['group_type']==3) {
	 	  array_push($zoneArray, $oldGroup['group_common_id']);
	 } else if ($oldGroup['group_type']==4) {
	 	  array_push($levelArray, $oldGroup['group_common_id']);
	 }
}

?> 
<input type="hidden" id=""  autocomplete="off" value="<?php echo $group_type;?>" name="group_type" >

<div class="row">
	<div class="col-sm-12">
	<li style="list-style: none;">
     <input type="checkbox"  class="chkparent" id="selectAll"> 
  <label for="selectAll">
   Check/Uncheck All
 </label>
    </li>
  </div>

	<?php  
	if ($group_type==1) {
		 $ids = join("','",$branchArray); 
	$familyCount=$d->select("block_master","block_status=0 AND society_id='$society_id' AND block_id NOT IN ('$ids') ");

    while ($data=mysqli_fetch_array($familyCount)) { ?>
    	<div class="col-sm-12">
    		<h6>
	    	<input type="checkbox" name="group_common_id[]" value="<?php echo $data['block_id'];?>"> <?php echo $data['block_name'];?> </h6>
    	</div>
	    
	<?php } } else if ($group_type==2) {
		 $ids = join("','",$departmentArray); 
	$familyCount=$d->select("floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.floor_status=0 AND floors_master.society_id='$society_id'   AND floors_master.floor_id NOT IN ('$ids')");

    while ($data=mysqli_fetch_array($familyCount)) { ?>
    	<div class="col-sm-12">
    		<h6>
	    	<input type="checkbox" name="group_common_id[]" value="<?php echo $data['floor_id'];?>"> <?php echo $data['floor_name'] .' - '.$data['block_name'];?> </h6>
    	</div>
	    
	<?php } } else if ($group_type==3) {
		 $ids = join("','",$zoneArray); 
	$familyCount=$d->select("zone_master","zone_status=0 AND society_id='$society_id' AND zone_id NOT IN ('$ids') ");

    while ($data=mysqli_fetch_array($familyCount)) { ?>
    	<div class="col-sm-12">
    		<h6>
	    	<input type="checkbox" name="group_common_id[]" value="<?php echo $data['zone_id'];?>"> <?php echo $data['zone_name'];?> </h6>
    	</div>
	    
	<?php } } else if ($group_type==4) {
		 $ids = join("','",$levelArray); 
	$familyCount=$d->select("employee_level_master","level_status=0 AND society_id='$society_id' AND level_id NOT IN ('$ids')");

    while ($data=mysqli_fetch_array($familyCount)) { ?>
    	<div class="col-sm-12">
    		<h6>
	    	<input type="checkbox" name="group_common_id[]" value="<?php echo $data['level_id'];?>"> <?php echo $data['level_name'];?> </h6>
    	</div>
	    
	<?php } }?>
</div>
<script>
    $("#selectAll").click(function() {
        $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
    });

    $("input[type=checkbox]").click(function() {
        if (!$(this).prop("checked")) {
            $("#selectAll").prop("checked", false);
        }
    });
   
</script>
