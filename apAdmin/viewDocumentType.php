  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title"> Document Type</h4>
        </div>
        <div class="col-sm-3 col-6">
       <div class="btn-group float-sm-right">
          <a href="documentType" class="btn  btn-sm btn-primary pull-right"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteDocumentType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          
      </div>
    </div>
  </div>
  <!-- End Breadcrumb-->


  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
        <div class="card-body">
          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>#</th>
                  <th>Document Type Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                 <?php 
                  $i=1;
                  $q=$d->select("document_type_master","society_id='$society_id' ");
                  while ($empData=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       <td class="text-center">
                         <?php 
                          $totalEmp= $d->count_data_direct("document_id","document_master","society_id='$society_id' AND document_type_id='$empData[document_type_id]'"); 
                          if ($totalEmp==0) {
                          ?>
                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $empData['document_type_id']; ?>">
                          <?php } else { ?> 
                            <input type="checkbox"  onclick="showError('Delete this type of Document first..')" name="" class="" value="">
                        <?php }  ?>
                        </td>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $empData['document_type_name']; ?></td>
                    <td>
                        
                      <form action="documentType" method="post" >
                        <input type="hidden" name="document_id_edit" value="<?php echo $empData['document_type_id']; ?>">
                        <input type="hidden" name="editDocumentType" value="editDocumentType">
                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit</button></td>
                      </form>
                    </tr>
                  <?php } ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->



