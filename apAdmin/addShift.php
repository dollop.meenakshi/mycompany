<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    .er {
        color: red;
    }

    .h-28{
        height:28px;
    }
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
<?php
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if (isset($edit_shift_timing))
    {
        $week_off_days_arr = [];
        $alternate_weekoff_days_arr = [];
        $q = $d->selectRow("stm.shift_name,stm.week_off_days,stm.maximum_in_out,stm.late_in_reason,stm.early_out_reason,stm.has_altenate_week_off,stm.alternate_week_off,stm.alternate_weekoff_days,stm.is_multiple_punch_in,stm.take_out_of_range_reason,sdm.*","shift_timing_master AS stm JOIN shift_day_master AS sdm ON sdm.shift_time_id = stm.shift_time_id", "stm.shift_time_id = '$shift_time_id'","ORDER BY sdm.shift_day_id ASC");
        $rrr = 1;
        while($row = $q->fetch_assoc())
        {
            if($rrr == 7)
            {
                $data_s[0] = $row;
            }
            else
            {
                $data_s[$rrr] = $row;
            }
            $rrr++;
        }
        $week_off_days_arr = explode(",",$data_s[0]['week_off_days']);
        $alternate_weekoff_days_arr = explode(",",$data_s[0]['alternate_weekoff_days']);
    }
}
$week_days = array(1=>'Monday', 2=>'Tuesday',3=> 'Wednesday',4=> 'Thursday',5=> 'Friday',6=> 'Saturday',0=>'Sunday');
$weeks = array('Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5');
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title"> Shift Timing</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="addShiftForm" action="controller/ShiftTimingController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Shift Name <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input class="form-control" required type="text" name="shift_name" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[0]['shift_name'] != "") { echo $data_s[0]['shift_name'];} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Week off Days </label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select onchange="showExtraDay();" id="week_off_days" class="form-control week_off_days" multiple name="week_off_days[]">
                                                <?php
                                                if ($data_s[0]['week_off_days'] != "")
                                                {
                                                    $week_off_days = explode(',', $data_s[0]['week_off_days']);
                                                }
                                                for ($i = 0; $i < count($week_days); $i++)
                                                {
                                                ?>
                                                <option <?php if (isset($_POST['edit_shift_timing']) && $data_s[0]['week_off_days'] != "" && isset($data_s[0]['week_off_days']) && in_array($i, $week_off_days_arr)){ echo "selected"; } ?> value="<?php echo $i; ?>"><?php echo $week_days[$i]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Has Alternate Week off <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select class="form-control single-select radio_week_off" name="has_altenate_week_off" id="has_altenate_week_off">
                                                <option value="0" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 0) { echo "selected";} else {echo "selected";} ?>>No</option>
                                                <option value="1" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 1) {echo "selected";} ?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 has_altenate_week_off" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 1) { echo "style='display: block;'"; }else{ echo "style='display: none;'"; } ?>>
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select type="text" class="form-control single-select alternate_week_off" multiple name="alternate_week_off[]">
                                                <?php
                                                if ($data_s[0]['alternate_week_off'] != "")
                                                {
                                                    $alternate_week_off = explode(',', $data_s[0]['alternate_week_off']);
                                                }
                                                for ($j = 0; $j < count($weeks); $j++)
                                                {
                                                ?>
                                                    <option <?php if (isset($_POST['edit_shift_timing']) && $data_s[0]['alternate_week_off'] != "" && isset($data_s[0]['alternate_week_off']) && in_array($j + 1, $alternate_week_off)) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $j + 1; ?>"><?php echo $weeks[$j]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 has_altenate_week_off" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 1) { echo "style='display: block;'"; }else{ echo "style='display: none;'"; } ?>>
                                    <input type="hidden" name="hasAlrtWeekDays" id="hasAlrtWeekDays" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['alternate_weekoff_days']) && $data_s[0]['alternate_weekoff_days'] != "") {echo $data_s[0]['alternate_weekoff_days'];} ?>">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off Days <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select onchange="altWeekOffDays();" id="has_altenate_week_off_opt" type="text" class="form-control single-select alternate_weekoff_days" multiple name="alternate_weekoff_days[]">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Maximum Late In/out </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control onlyNumber" max="31" name="maximum_in_out" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[0]['maximum_in_out'] != "") {echo $data_s[0]['maximum_in_out'];} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Late in </label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select class="form-control single-select" name="late_in_reason" id="late_in_reason">
                                                <option value="0" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['late_in_reason']) && $data_s[0]['late_in_reason'] == 0) { echo "selected";} else {echo "selected";} ?>>No</option>
                                                <option value="1" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['late_in_reason']) && $data_s[0]['late_in_reason'] == 1) { echo "selected";} ?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Early Out</label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select class="form-control single-select" name="early_out_reason" id="early_out_reason">
                                                <option value="0" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['early_out_reason']) && $data_s[0]['early_out_reason'] == 0) { echo "selected";} else {echo "selected";} ?>>No</option>
                                                <option value="1" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['early_out_reason']) && $data_s[0]['early_out_reason'] == 1) { echo "selected";} ?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Multiple Punch In/Out Allow</label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select class="form-control single-select" name="is_multiple_punch_in" id="is_multiple_punch_in">
                                                <option value="0" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['is_multiple_punch_in']) && $data_s[0]['is_multiple_punch_in'] == 0) { echo "selected";} else {echo "selected";}  ?>>No</option>
                                                <option value="1" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['is_multiple_punch_in']) && $data_s[0]['is_multiple_punch_in'] == 1) {echo "selected";}  ?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Out Of Range Reason</label>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <select class="form-control single-select" name="take_out_of_range_reason" id="take_out_of_range_reason">
                                                <option value="1" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['take_out_of_range_reason']) && $data_s[0]['take_out_of_range_reason'] == 1) {echo "selected";} elseif(isset($_POST['edit_shift_timing']) && $data_s[0]['take_out_of_range_reason'] != 0) {echo "selected";}else{ echo "selected"; }  ?>>Yes</option>
                                                <option value="0" <?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['take_out_of_range_reason']) && $data_s[0]['take_out_of_range_reason'] == 0) { echo "selected";} ?>>No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="checkChnageAltFunction" id="checkChnageAltFunction" value=" <?php if ($data_s[0]['week_off_days'] != "") {echo 1;} else {echo 0;} ?>">
                            </div>
                            <hr>

                            <div class="row pt-2">
                                <div class="col-3">Copy To All Days</div>
                                <div class="col text-center">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="copy_checkbox" <?php if(isset($edit_shift_timing)){  }else{ echo "checked";} ?>>
                                    </div>
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>

                            <div class="row text-center">
                                <div class="col-md-12">
                                    <h5>Day Wise Details</h5>
                                </div>
                                <div class="col-md-12" style="font-size:8px;">
                                    <div class="row">
                                        <div class="col-3">
                                        </div>
                                        <div class="col"><label class="mon <?php if(isset($edit_shift_timing) && in_array("1",$week_off_days_arr) && !in_array("1",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="tue <?php if(isset($edit_shift_timing) && in_array("2",$week_off_days_arr) && !in_array("2",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="wed <?php if(isset($edit_shift_timing) && in_array("3",$week_off_days_arr) && !in_array("3",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="thu <?php if(isset($edit_shift_timing) && in_array("4",$week_off_days_arr) && !in_array("4",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="fri <?php if(isset($edit_shift_timing) && in_array("5",$week_off_days_arr) && !in_array("5",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="sat <?php if(isset($edit_shift_timing) && in_array("6",$week_off_days_arr) && !in_array("6",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                        <div class="col"><label class="sun <?php if(isset($edit_shift_timing) && in_array("0",$week_off_days_arr) && !in_array("0",$alternate_weekoff_days_arr)){}else{ echo "d-none"; } ?>">Applicable For Extra Day</label></div>
                                    </div>
                                </div>
                                <div class='col-3'>
                                   <b> Type</b>
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                                    echo "<div class='col'><b>".$value."</b></div>";
                            ?>
                                <input type="hidden" name="week_days[<?php echo $key; ?>]" value="<?php echo $value; ?>"/>
                            <?php
                                    if (isset($_POST['edit_shift_timing']))
                                    {
                            ?>
                                <input type="hidden" name="shift_day_id[<?php echo $key; ?>]" value="<?php echo $data_s[$key]['shift_day_id']; ?>"/>
                            <?php
                                    }
                                }
                            ?>
                            </div>
                            <div class="row mt-2">
                                 <div class='col-3'>
                                    Shift Start Time
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                            ?>
                                <div class='col'>
                                    <input onchange="changeShiftTIme(this.value,<?php echo $key; ?>)" type="text" id="shift_start_time<?php echo $key; ?>" class="form-control changeShiftInTIme h-28" readonly name="shift_start_time[<?php $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['shift_start_time'] != "") { echo date("H:i", strtotime($data_s[$key]['shift_start_time']));}  ?>">
                                </div>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="row mt-2">
                                <div class='col-3'>
                                   Shift End Time
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                            ?>
                                <div class='col'>
                                    <input type="text" onchange="changeShiftOutTIme(this.value,<?php echo $key; ?>)" class="form-control changeShiftOutTIme shiftEndStart<?php echo $key; ?> h-28" readonly id="shift_end_time<?php echo $key; ?>" name="shift_end_time[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['shift_end_time'] != "") {echo date("H:i", strtotime($data_s[$key]['shift_end_time'])); }  ?>">
                                    <i id="showAlert<?php echo $key; ?>"></i>
                                </div>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="row mt-2">
                                 <div class='col-3'>
                                    Lunch Break Start Time
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                            ?>
                                <div class='col'>
                                    <input onchange="changeLunchStartTime(this.value,<?php echo $key; ?>)" type="text" class="form-control lbstp h-28" readonly id="lunch_break_start_time<?php echo $key; ?>" name="lunch_break_start_time[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['lunch_break_start_time'] != "" && $data_s[$key]['lunch_break_start_time']!="00:00:00") { echo date("H:i", strtotime($data_s[$key]['lunch_break_start_time']));}?>">
                                </div>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="row mt-2">
                                 <div class='col-3'>
                                    Lunch Break End Time
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                            ?>
                                <div class='col'>
                                    <input type="text" onchange="changeLunchEndTime(this.value,<?php echo $key; ?>)" class="form-control lbetp h-28" readonly id="lunch_break_end_time<?php echo $key; ?>" name="lunch_break_end_time[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['lunch_break_end_time'] != "" && $data_s[$key]['lunch_break_end_time'] != "00:00:00"){ echo date("H:i", strtotime($data_s[$key]['lunch_break_end_time']));} ?>">
                                </div>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                    Tea Break Start Time
                                </div>
                            <?php
                                foreach($week_days AS $key => $value)
                                {
                            ?>
                                <div class='col'>
                                    <input onchange="changeTeaBrkStart(this.value,<?php echo $key; ?>)" type="text" class="form-control tbstp h-28" readonly id="tea_break_start_time<?php echo $key; ?>" name="tea_break_start_time[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['tea_break_start_time'] != "" && $data_s[$key]['tea_break_start_time'] != "00:00:00") {echo date("H:i", strtotime($data_s[$key]['tea_break_start_time']));} ?>">
                                </div>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                   Tea Break End Time
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" onchange="changeTeaBrkEndTime(this.value,<?php echo $key ?>)" class="form-control tbetp h-28" readonly id="tea_break_end_time<?php echo $key ?>" name="tea_break_end_time[<?php echo $key ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['tea_break_end_time'] != "" && $data_s[$key]['tea_break_end_time'] != "00:00:00") {echo date("H:i", strtotime($data_s[$key]['tea_break_end_time']));} ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                    Half Day After Time
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" onchange="changeHalfDayAfter(this.value,<?php echo $key ?>)" class="form-control hdtsp h-28" readonly id="half_day_time_start<?php echo $key; ?>" name="half_day_time_start[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[$key]['half_day_time_start']) && $data_s[$key]['half_day_time_start'] != "00:00:00" && $data_s[$key]['half_day_time_start'] != "") {echo date("H:i", strtotime($data_s[$key]['half_day_time_start']));} ?>">
                                    <span class="er" id="half_day_time_start_er<?php echo $key; ?>"></span>
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                   Half Day Before Time
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" onchange="changeHalfDayBefore(this.value,<?php echo $key ?>)" class="form-control h-28 hdbtp" readonly id="halfday_before_time<?php echo $key; ?>" name="halfday_before_time[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[$key]['halfday_before_time']) && $data_s[$key]['halfday_before_time'] != "00:00:00" && $data_s[$key]['halfday_before_time'] != "") {echo date("H:i", strtotime($data_s[$key]['halfday_before_time']));} ?>">
                                    <span class="er" id="halfday_before_time_er<?php echo $key; ?>"></span>
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                    Late In Count After Minutes
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                        if(isset($_POST['edit_shift_timing']) && isset($data_s[$key]['late_time_start']) && $data_s[$key]['late_time_start'] !="" && $data_s[$key]['late_time_start'] != "00:00:00")
                                        {
                                            $time = explode(':', $data_s[$key]['late_time_start']);
                                            $time1 = ($time[0]*60) + ($time[1]) + ($time[2]/60);
                                        }
                                ?>
                                <div class='col'>
                                    <input type="text" class="form-control h-28 licam numberEClass" min="0" placeholder="15 min" id="late_time_start<?php echo $key; ?>" name="late_time_start[<?php echo $key; ?>]" value="<?php if(isset($_POST['edit_shift_timing']) && isset($data_s[$key]['late_time_start']) && $data_s[$key]['late_time_start'] != "" && $data_s[$key]['late_time_start'] != "00:00:00")
                                        { echo $time1; } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                           <div class="row  mt-2">
                                 <div class='col-3'>
                                   Early Out Count Before Minutes
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                        if(isset($_POST['edit_shift_timing']) && isset($data_s[$key]['early_out_time']) && $data_s[$key]['early_out_time'] !="" && $data_s[$key]['early_out_time'] != "00:00:00")
                                        {
                                            $time2 = explode(':', $data_s[$key]['early_out_time']);
                                            $time3 = ($time2[0]*60) + ($time2[1]) + ($time2[2]/60);
                                        }
                                ?>
                                <div class='col'>
                                    <input type="text" pattern="[0-9]+" class="form-control numberEClass onlyNumber h-28 eocbm" min="0" placeholder="15 min" id="early_out_time<?php echo $key ?>" name="early_out_time[<?php echo $key ?>]" value="<?php if(isset($_POST['edit_shift_timing']) && isset($data_s[$key]['early_out_time']) && $data_s[$key]['early_out_time'] !="" && $data_s[$key]['early_out_time'] != "00:00:00")
                                        { echo $time3; } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                   Mininum Half-day Hours
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" autocomplete="off" readonly class="form-control h-28 mhdhp" id="maximum_halfday_hours<?php echo $key; ?>" name="maximum_halfday_hours[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[$key]['maximum_halfday_hours']) && $data_s[$key]['maximum_halfday_hours'] != "00:00:00" && $data_s[$key]['maximum_halfday_hours'] != "") {echo date("h:i", strtotime($data_s[$key]['maximum_halfday_hours']));} ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                           <div class="row  mt-2">
                                 <div class='col-3'>
                                  Mininum Full Day Hours
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" autocomplete="off" readonly class="form-control h-28 mhffdp" id="minimum_hours_for_full_day<?php echo $key; ?>" name="minimum_hours_for_full_day[<?php echo $key; ?>]" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[$key]['minimum_hours_for_full_day']) && $data_s[$key]['minimum_hours_for_full_day'] != "00:00:00" && $data_s[$key]['minimum_hours_for_full_day'] != "") { echo date("h:i", strtotime($data_s[$key]['minimum_hours_for_full_day']));
                                         } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                  Maximum Tea Break
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" pattern="[0-9]+" class="form-control numberEClass onlyNumber h-28 mtb" min="0" max="5" placeholder="5" id="max_tea_break<?php echo $key; ?>" name="max_tea_break[<?php echo $key; ?>]" value="<?php if(isset($_POST['edit_shift_timing']) && $data_s[$key]['max_tea_break'] != "" && $data_s[$key]['max_tea_break'] > 0){ echo $data_s[$key]['max_tea_break']; } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                           <div class="row  mt-2">
                                 <div class='col-3'>
                                  Maximum Lunch Break
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" pattern="[0-9]+" class="form-control numberEClass onlyNumber h-28 mlb" min="0" max="5" placeholder="5" id="max_lunch_break<?php echo $key; ?>" name="max_lunch_break[<?php echo $key; ?>]" value="<?php if(isset($_POST['edit_shift_timing']) && $data_s[$key]['max_lunch_break'] != "" && $data_s[$key]['max_lunch_break'] > 0){ echo $data_s[$key]['max_lunch_break']; } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                             <div class="row  mt-2">
                                 <div class='col-3'>
                                  Maximum Personal Break
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input type="text" pattern="[0-9]+" class="form-control numberEClass onlyNumber h-28 mpb" min="0" max="5" placeholder="5" id="max_personal_break<?php echo $key; ?>" name="max_personal_break[<?php echo $key; ?>]" value="<?php if(isset($_POST['edit_shift_timing']) && $data_s[$key]['max_personal_break'] != "" && $data_s[$key]['max_personal_break'] > 0){ echo $data_s[$key]['max_personal_break']; } ?>">
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="row  mt-2">
                                 <div class='col-3'>
                                  Maximum Punch Out Time
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <input onchange="checkaMaxPunchOutTime(this.value,<?php echo $key; ?>)" type="text" class="form-control max_punch_out_time h-28" id="max_punch_out_time<?php echo $key; ?>" name="max_punch_out_time[<?php echo $key; ?>]" placeholder="Set Time"  value="<?php if (isset($_POST['edit_shift_timing']) && $data_s[$key]['max_punch_out_time'] != "" && $data_s[$key]['max_punch_out_time']!="00:00:00") { echo $data_s[$key]['max_punch_out_time'];} ?>">
                                    <span style="font-size: 9px;"><b>After Shift End Time</b></span>
                                </div>
                                <?php
                                    }
                                ?>
                            </div>
                             <div class="row  mt-2">
                                 <div class='col-3'>
                                  Maximum Punch Out Hours
                                </div>
                                <?php
                                    foreach($week_days AS $key => $value)
                                    {
                                ?>
                                <div class='col'>
                                    <select onchange="chekMiniShiftHoursForPunchOut(<?php echo $key; ?>);" class="form-control mhfpo" name="max_shift_hour[<?php echo $key; ?>]" id="max_shift_hour<?php echo $key; ?>">
                                    <?php
                                        if ($data_s[$key]['max_shift_hour'] == "")
                                        {
                                    ?>
                                        <option value="0">Select</option>
                                    <?php
                                        }
                                        else
                                        {
                                    ?>
                                        <option value="0">Remove</option>
                                    <?php
                                        }
                                        $min = array("00","15","30","45");
                                        for($i=1;$i<48.15;$i++)
                                        {
                                            foreach ($min as $v)
                                            {
                                    ?>
                                        <option <?php if ($data_s[$key]['max_shift_hour'] == "$i:$v:00") { echo 'selected';} ?>  value="<?php echo "$i:$v:00";?>"><?php echo "$i:$v:00";?></option>
                                        <?php
                                            }
                                        }
                                    ?>
                                    </select>
                                </div>
                                <?php
                                    }
                                ?>
                            </div>

                            <div class="form-footer text-center">
                                <input type="hidden" name="is_Edit" id="is_Edit" value="<?php if (isset($_POST['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) == 1) { echo 1; } else {echo 0;} ?>">
                            <?php
                                if (isset($_POST['edit_shift_timing']))
                                {
                            ?>
                                <input type="hidden" id="shift_time_id" name="shift_time_id" value="<?php if ($data_s[0]['shift_time_id'] != "") {echo $data_s[0]['shift_time_id'];} ?>">
                                <button id="addShiftTimingBtn" type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                                <input type="hidden" name="addShiftTimingNew" value="addShiftTiming">
                                <button type="reset" value="reset" class="btn btn-sm btn-danger cancel" onclick="resetFrm('addShiftForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                            <?php
                                }
                                else
                                {
                            ?>
                                <button id="addShiftTimingBtn" type="button" onclick="submitFormShiftNew();" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <input type="hidden" name="addShiftTimingNew" value="addShiftTimingNew">
                                <button type="reset" value="reset" class="btn btn-sm btn-danger cancel" onclick="resetFrm('addShiftForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                            <?php
                                }
                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    document.querySelector(".numberEClass").addEventListener("keypress", function(evt)
    {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });

    function changeShiftTIme(value,key)
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.changeShiftOutTIme').val('');
        }
        else
        {
            $('#shift_end_time'+key).val('');
        }
        $('#lunch_break_start_time'+key).val('');
        $('#lunch_break_end_time'+key).val('');
        $('#tea_break_start_time'+key).val('');
        $('#tea_break_end_time'+key).val('');
        $('#half_day_time_start'+key).val('');
        $('#half_day_time_start'+key).val('');
        $('#halfday_before_time'+key).val('');
        $('#late_time_start'+key).val('');
        $('#early_out_time'+key).val('');
        $('#maximum_halfday_hours'+key).val('');
        $('#minimum_hours_for_full_day'+key).val('');
        var shift_end_time = $('#shift_end_time'+key).val('');
        if($('#copy_checkbox').is(":checked"))
        {
            $('.mhfpo').prop('selectedIndex',0);
        }
        else
        {
            $('#max_shift_hour'+key).prop('selectedIndex',0);
        }
        shift_start_time = value;
        lunch_break_start_time = $('#lunch_break_start_time'+key).val();
        tea_break_start_time = $('#tea_break_start_time').val();
        halfday_before_time = $('#halfday_before_time').val();
        half_day_time_start = $('#half_day_time_start').val();
        if(lunch_break_start_time != "")
        {
            if (lunch_break_start_time < shift_start_time)
            {
                $('#lunch_break_start_time'+key).val('');
            }
            if (lunch_break_start_time > shift_end_time)
            {
                $('#lunch_break_start_time'+key).val('');
            }
        }
        if(tea_break_start_time != "")
        {
            if (tea_break_start_time < shift_start_time)
            {
                $('#tea_break_start_time'+key).val('');
            }
            if (tea_break_start_time > shift_end_time)
            {
                $('#tea_break_start_time'+key).val('');
            }
        }
        if(halfday_before_time != "")
        {
            if (halfday_before_time < shift_start_time)
            {
                $('#halfday_before_time'+key).val('');
            }
            if (halfday_before_time > shift_end_time)
            {
                $('#halfday_before_time'+key).val('');
            }
        }
        if(half_day_time_start != "")
        {
            if (half_day_time_start < shift_start_time)
            {
                 $('#half_day_time_start').val('');
            }
            if (half_day_time_start > shift_end_time)
            {
                $('#half_day_time_start').val('');
            }
        }

    <?php
        if (isset($edit_shift_timing))
        {
    ?>
            if (shift_start_time < shift_end_time)
            {
                if (lunch_break_start_time < shift_start_time)
                {
                    $('#lunch_break_start_time'+key).val('');
                }
                if (lunch_break_start_time > shift_end_time)
                {
                    $('#lunch_break_start_time'+key).val('');
                }
                if (tea_break_start_time < shift_start_time)
                {
                    $('#tea_break_start_time'+key).val('');
                }
                if (tea_break_start_time > shift_end_time)
                {
                    $('#tea_break_start_time'+key).val('');
                }
                if (halfday_before_time < shift_start_time)
                {
                    $('#halfday_before_time'+key).val('');
                }
                if (halfday_before_time > shift_end_time)
                {
                    $('#halfday_before_time'+key).val('');
                }
                if (half_day_time_start < shift_start_time)
                {
                    $('#half_day_time_start'+key).val('');
                }
                if (half_day_time_start > shift_end_time)
                {
                    $('#half_day_time_start'+key).val('');
                }
            }
            else
            {
                if (shift_end_time < lunch_break_start_time && shift_start_time > lunch_break_start_time)
                {
                    $('#lunch_break_start_time'+key).val('');
                }
                if (shift_end_time < tea_break_start_time && shift_start_time > tea_break_start_time)
                {
                    $('#tea_break_start_time'+key).val('');
                }
                if (shift_end_time < halfday_before_time && shift_start_time > halfday_before_time)
                {
                    $('#halfday_before_time'+key).val('');
                }
                if (shift_end_time < half_day_time_start && shift_start_time > half_day_time_start)
                {
                    $('#half_day_time_start'+key).val('');
                }
            }

            shift_start_time = $('#shift_start_time'+key).val();
            shift_end_time = $('#shift_end_time'+key).val();
            var startTime = moment(shift_start_time, 'hh:mm:ss');
            var endTime = moment(shift_end_time, 'hh:mm:ss');
            var totalMinutes = endTime.diff(startTime, 'minutes');
            ////////minutes to hours calculation
            var num = totalMinutes;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            shift_diffrance_hours = (rhours + ":" + rminutes);
            /////////////////////minutes to hours calculation
            minimum_hours = $('#minimum_hours_for_full_day'+key).val();
            maximum_halfday_hours = $('#maximum_halfday_hours'+key).val();

            if (shift_diffrance_hours < minimum_hours)
            {
                $('#minimum_hours_for_full_day').val("");
            }
            if (shift_diffrance_hours < maximum_halfday_hours)
            {
                $('#maximum_halfday_hours').val("");
            }
    <?php
        }
    ?>
        if($('#copy_checkbox').is(":checked"))
        {
            $('.changeShiftInTIme').val(value);
        }
    }

    function changeShiftOutTIme(value,key)
    {
        var error = 0;
        $('#lunch_break_start_time'+key).val('');
        $('#lunch_break_end_time'+key).val('');
        $('#tea_break_start_time'+key).val('');
        $('#tea_break_end_time'+key).val('');
        $('#half_day_time_start'+key).val('');
        $('#half_day_time_start'+key).val('');
        $('#halfday_before_time'+key).val('');
        $('#late_time_start'+key).val('');
        $('#early_out_time'+key).val('');
        $('#maximum_halfday_hours'+key).val('');
        $('#minimum_hours_for_full_day'+key).val('');

        var shift_end_time = value;
        var shift_start_time = $('#shift_start_time'+key).val();
        half_day_time_start = $('#half_day_time_start'+key).val();
        halfday_before_time = $('#halfday_before_time'+key).val();
        if($('#copy_checkbox').is(":checked"))
        {
            $('.max_punch_out_time').val('');
        }
        else
        {
            $('#max_punch_out_time'+key).val('');
        }
        if($('#copy_checkbox').is(":checked"))
        {
            $('.mhfpo').prop('selectedIndex',0);
        }
        else
        {
            $('#max_shift_hour'+key).prop('selectedIndex',0);
        }
        if (shift_start_time == shift_end_time && shift_end_time != "")
        {
            Lobibox.notify('error', {
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-times-circle',
                msg: 'Shift End Not Equal to Start'
            });
            $('#shift_end_time'+key).val('');
            error = 1;
        }
        if(shift_end_time != "")
        {
            if (shift_start_time > shift_end_time)
            {
                showAlert = `<h6 class="text-danger" role="alert">Night shift (Next day end)</h6>`;
                $('#showAlert'+key).html(showAlert);
            }
            else
            {
                $('#showAlert'+key).html("");
            }
        }
        if (shift_start_time > half_day_time_start)
        {
            if (half_day_time_start != "")
            {
                $('#half_day_time_start'+key).val("");
            }
        }
        else
        {
            $('#half_day_time_start_er'+key).hide();
        }
        if (halfday_before_time > shift_end_time)
        {
            $('#halfday_before_time'+key).val("");
        }
        else
        {
            $('#halfday_before_time_er'+key).hide();
        }
    <?php
        if (isset($edit_shift_timing))
        {
    ?>
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        var startTime = moment(shift_start_time, 'hh:mm:ss');
        var endTime = moment(shift_end_time, 'hh:mm:ss');
        if (shift_start_time > shift_end_time)
        {
            var timeStart = new Date("01/01/2022 " + shift_start_time).getHours();
            var timeEnd = new Date("02/01/2022 " + shift_end_time).getHours();
            var timeStartMi = new Date("01/01/2022 " + shift_start_time).getMinutes();
            var timeEndMin = new Date("02/01/2022 " + shift_end_time).getMinutes();
            var hourDiff = timeEnd - timeStart;
            var MinDiff = timeEndMin - timeStartMi;
            hourDiff = parseInt(hourDiff);
            MinDiff = parseInt(MinDiff);
            if (MinDiff == 0)
            {
                MinDiff = "00";
            }
            else
            {
                if (MinDiff < 10)
                {
                    MinDiff = "0" + MinDiff;
                }
                else
                {
                    MinDiff = MinDiff;
                }
            }
            shift_diffrance_hours = hourDiff + parseInt(24)+":" + MinDiff;;
        }
        else
        {
            var totalMinutes = endTime.diff(startTime, 'minutes');
            var num = totalMinutes;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            if (rminutes == 0)
            {
                rminutes = "00";
            }
            else
            {
                rminutes = parseInt(rminutes);
            }
            if (rminutes < 10)
            {
                rminutes = "0" + rminutes;
            }
            shift_diffrance_hours = (rhours + ":" + rminutes);
        }
        /////////////////////minutes to hours calculation
        minimum_hours_for_full_day = $('#minimum_hours_for_full_day'+key).val();
        maximum_halfday_hours = $('#maximum_halfday_hours'+key).val();
        if(parseFloat(shift_diffrance_hours) <= parseFloat(minimum_hours_for_full_day))
        {
            if (parseFloat(minimum_hours_for_full_day) == parseFloat(shift_diffrance_hours))
            {
                shift_diffrance_hours = shift_diffrance_hours.split(':');
                minimum_hours_for_full_day = minimum_hours_for_full_day.split(':');
                if (minimum_hours_for_full_day[1] > shift_diffrance_hours[1])
                {
                    $('#minimum_hours_for_full_day'+key).val("");
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'No More Then Shift Total Hours'
                    });
                    error = 1;
                }
            }
            else
            {
                $('#minimum_hours_for_full_day'+key).val("");
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'No More Then Shift Total Hours'
                });
                error = 1;
            }
        }
        if(parseFloat(shift_diffrance_hours) <= parseFloat(maximum_halfday_hours))
        {
            if (parseFloat(maximum_halfday_hours) == parseFloat(shift_diffrance_hours))
            {
                shift_diffrance_hours = shift_diffrance_hours.split(':');
                maximum_halfday_hours = maximum_halfday_hours.split(':');
                if (maximum_halfday_hours[1] > shift_diffrance_hours[1])
                {
                    $('#maximum_halfday_hours'+key).val("");
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'No More Then Shift Total Hours'
                    });
                    error = 1;
                }
            }
            else
            {
                $('#maximum_halfday_hours'+key).val("");
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'No More Then Shift Total Hours'
                });
                error = 1;
            }
        }
    <?php
        }
    ?>
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.changeShiftOutTIme').val(value);
        }
    }

    function changeLunchStartTime(value,key)
    {
        var error = 0;
        half_day_time_start = value;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        lunch_break_end_time = $('#lunch_break_end_time'+key).val();
        if (shift_end_time != "")
        {
            if (shift_start_time < shift_end_time)
            {
                if (half_day_time_start < shift_start_time)
                {
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    $('#lunch_break_start_time'+key).val('');
                    error = 1;
                }
                if (half_day_time_start > shift_end_time)
                {
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    $('#lunch_break_start_time'+key).val('');
                    error = 1;
                }
            }
            else
            {
                if(shift_end_time<half_day_time_start && shift_start_time>half_day_time_start)
                {
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    $('#lunch_break_start_time'+key).val('');
                    error = 1;
                }
            }
        }
        else
        {
            if(value != "")
            {
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please Select Shift End Time Or Start Time'
                });
                $('#lunch_break_start_time'+key).val('');
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.lbstp').val(value);
            $('.lbetp').val('');
        }
        else
        {
            $('#lunch_break_end_time'+key).val('');
        }
    }

    function changeLunchEndTime(value,key)
    {
        var error = 0;
        if ($('#lunch_break_start_time'+key).val() == '')
        {
            if(value != "")
            {
                $('#lunch_break_end_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set lunch start time'
                });
                error = 1;
            }
        }
        else if (value == $('#lunch_break_start_time'+key).val())
        {
            $('#lunch_break_end_time'+key).val('');
            Lobibox.notify('error', {
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-times-circle',
                msg: 'Lunch Break start & End Time Not equal'
            });
            error = 1;
        }

        lunch_break_end_time = value;
        shift_start_time = $('#shift_start_time'+key).val();
        lunch_break_start_time = $('#lunch_break_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        if (shift_end_time != "")
        {
            if (shift_start_time < shift_end_time)
            {
                if (lunch_break_end_time < shift_start_time)
                {
                    $('#lunch_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if (lunch_break_end_time > shift_end_time)
                {
                    $('#lunch_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if (lunch_break_start_time != "")
                {
                    if(lunch_break_start_time>lunch_break_end_time)
                    {
                        $('#lunch_break_end_time'+key).val('');
                        Lobibox.notify('error', {
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: false,
                            position: 'top right',
                            icon: 'fa fa-times-circle',
                            msg: 'Please set time between Greater Then Lunch Start time'
                        });
                        error = 1;
                    }
                }
            }
            else
            {
                if(shift_end_time<lunch_break_end_time && shift_start_time>lunch_break_end_time)
                {
                    $('#lunch_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if(lunch_break_start_time > lunch_break_end_time && lunch_break_start_time >"00:00" && shift_end_time > lunch_break_end_time )
                {
                    $('#lunch_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between Greater Then Lunch Start time'
                    });
                    error = 1;
                }
            }
        }
        else
        {
            if(value != "")
            {
                $('#lunch_break_start_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please Select Shift End Time Or Start Time'
                });
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.lbetp').val(value);
        }
    }

    function changeTeaBrkStart(value,key)
    {
        var error = 0;
        half_day_time_start = value;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        if (shift_end_time != "")
        {
            if (shift_start_time < shift_end_time)
            {
                if (half_day_time_start < shift_start_time)
                {
                    $('#tea_break_start_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if (half_day_time_start > shift_end_time)
                {
                    $('#tea_break_start_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set  time between shift start and end time'
                    });
                    error = 1;
                }
            }
            else
            {
                if(shift_end_time<half_day_time_start && shift_start_time>half_day_time_start)
                {
                    $('#tea_break_start_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
            }
        }
        else
        {
            if(value != "")
            {
                $('#tea_break_start_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please Select Shift End Time Or Start Time'
                });
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.tbstp').val(value);
            $('.tbetp').val('');
        }
        else
        {
            $('#tea_break_end_time'+key).val('');
        }
    }

    function changeTeaBrkEndTime(value,key)
    {
        var error = 0;
        if ($('#tea_break_start_time'+key).val() == '')
        {
            if(value != "")
            {
                $('#tea_break_end_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set tea break start time'
                });
                error = 1;
            }
        }
        else
        {
            if (value == $('#tea_break_start_time'+key).val())
            {
                $('#tea_break_end_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Tea Break start & End Time Not equal'
                });
                error = 1;
            }
        }

        tea_break_end_time = value;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        tea_break_start_time = $('#tea_break_start_time'+key).val();
        if (shift_end_time != "")
        {
            if (shift_start_time < shift_end_time)
            {
                if (tea_break_end_time < shift_start_time)
                {
                    $('#tea_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if (tea_break_end_time > shift_end_time)
                {
                    $('#tea_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if (tea_break_start_time != "")
                {
                    if(tea_break_start_time>tea_break_end_time)
                    {
                        $('#tea_break_end_time'+key).val('');
                        Lobibox.notify('error', {
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: false,
                            position: 'top right',
                            icon: 'fa fa-times-circle',
                            msg: 'Please set time between Greater Then Tea Break Start time'
                        });
                        error = 1;
                    }
                }
            }
            else
            {
                if(shift_end_time<half_day_time_start && shift_start_time>half_day_time_start)
                {
                    $('#tea_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between shift start and end time'
                    });
                    error = 1;
                }
                if(tea_break_start_time > tea_break_end_time && tea_break_start_time >"00:00" && shift_end_time > tea_break_end_time )
                {
                    $('#tea_break_end_time'+key).val('');
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please set time between Greater Then Lunch Start time'
                    });
                    error = 1;
                }
            }
        }
        else
        {
            if(value != "")
            {
                $('#lunch_break_start_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please Select Shift End Time Or Start Time'
                });
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.tbetp').val(value);
        }
    }

    function chekMiniShiftHoursForPunchOut(key)
    {
        var error = 0;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        var a11 = $("#max_shift_hour"+key).val().split(':');
        var max_shift_hour11minutes = (+a11[0]) * 60 + (+a11[1]);
        max_shift_hour11 = $("#max_shift_hour"+key).val().split(':')[0], 10;
        if (shift_end_time != "" && shift_end_time != "")
        {
            var totaShiftHours = parseFloat(shift_end_time.split(':')[0], 10) - parseFloat(shift_start_time.split(':')[0], 10);
            if (shift_start_time > shift_end_time)
            {
                totaShiftHours = parseFloat(totaShiftHours)+24;
                var diff = Math.abs(new Date('2011/10/09 '+shift_start_time) - new Date('2011/10/10 '+shift_end_time));
                var shiftMinutues = Math.floor((diff/1000)/60);
            }
            else
            {
                var diff = Math.abs(new Date('2011/10/09 '+shift_start_time) - new Date('2011/10/09 '+shift_end_time));
                var shiftMinutues = Math.floor((diff/1000)/60);
            }
            if (max_shift_hour11minutes <= shiftMinutues)
            {
                if($('#copy_checkbox').is(":checked"))
                {
                    $('.mhfpo').prop('selectedIndex',0);
                }
                else
                {
                    $('#max_shift_hour'+key).prop('selectedIndex',0);
                }
                if($('#copy_checkbox').is(":checked"))
                {
                    $('.max_punch_out_time').val('');
                }
                else
                {
                    $('#max_punch_out_time'+key).val('');
                }
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set hours greater than shift hours'
                });
            }
            else
            {
                if($('#copy_checkbox').is(":checked"))
                {
                    $('.max_punch_out_time').val('');
                }
                else
                {
                    $('#max_punch_out_time'+key).val('');
                }
            }
        }
        else
        {
            if($('#copy_checkbox').is(":checked"))
            {
                $('.mhfpo').prop('selectedIndex',0);
            }
            else
            {
                $('#max_shift_hour'+key).prop('selectedIndex',0);
            }
            if($('#copy_checkbox').is(":checked"))
            {
                $('.max_punch_out_time').val('');
            }
            else
            {
                $('#max_punch_out_time'+key).val('');
            }
            Lobibox.notify('error', {
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-times-circle',
                msg: 'Please Select Shift Time First'
            });
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.mhfpo').val($('#max_shift_hour'+key).val());
        }
    }

    function checkaMaxPunchOutTime(value,key)
    {
        var error = 0;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        max_shift_hour11 = $("#max_shift_hour"+key).val().split(':')[0], 10;
        if (shift_end_time != "" && shift_end_time != "")
        {
            var totaShiftHours = parseFloat(shift_end_time.split(':')[0], 10) - parseFloat(shift_start_time.split(':')[0], 10);
            if (shift_start_time > shift_end_time)
            {
                totaShiftHours = parseFloat(totaShiftHours)+24;
            }
            if (shift_start_time > shift_end_time)
            {
            }
            else
            {
                if (value >= shift_start_time && value < shift_end_time)
                {
                    if($('#copy_checkbox').is(":checked"))
                    {
                        $('.max_punch_out_time').val('');
                    }
                    else
                    {
                        $('#max_punch_out_time'+key).val('');
                    }
                    Lobibox.notify('error', {
                        pauseDelayOnHover: true,
                        continueDelayOnInactiveTab: false,
                        position: 'top right',
                        icon: 'fa fa-times-circle',
                        msg: 'Please Select After Shift Hours Time'
                    });
                    error = 1;
                }
            }
            if($('#copy_checkbox').is(":checked"))
            {
                $('.mhfpo').prop('selectedIndex',0);
            }
            else
            {
                $('#max_shift_hour'+key).prop('selectedIndex',0);
            }
        }
        else
        {
            if($('#copy_checkbox').is(":checked"))
            {
                $('.mhfpo').prop('selectedIndex',0);
            }
            else
            {
                $('#max_shift_hour'+key).prop('selectedIndex',0);
            }
            if($('#copy_checkbox').is(":checked"))
            {
                $('.max_punch_out_time').val('');
            }
            else
            {
                $('#max_punch_out_time'+key).val('');
            }
            if(value != "")
            {
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please Select Shift Time First'
                });
            }
            error = 1;
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.max_punch_out_time').val(value);
        }
    }

    function changeHalfDayAfter(value,key)
    {
        var error = 0;
        half_day_time_start = value;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();
        var startTime = moment(shift_start_time , 'hh:mm:ss');
        var endTime = moment(shift_end_time, 'hh:mm:ss');
        var totalMinutes = endTime.diff(startTime, 'minutes');

        var num = totalMinutes;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        if (shift_start_time < shift_end_time)
        {
            if (half_day_time_start < shift_start_time)
            {
                $('#half_day_time_start'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
            if (half_day_time_start > shift_end_time)
            {
                $('#half_day_time_start'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
        }
        else
        {
            if(shift_end_time<half_day_time_start && shift_start_time>half_day_time_start)
            {
                $('#half_day_time_start'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.hdtsp').val(value);
        }
    }

    function changeHalfDayBefore(value,key)
    {
        var error = 0;
        halfday_before_time = value;
        shift_start_time = $('#shift_start_time'+key).val();
        shift_end_time = $('#shift_end_time'+key).val();

        if (shift_start_time < shift_end_time)
        {
            if (halfday_before_time < shift_start_time)
            {
                $('#halfday_before_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
            if (halfday_before_time > shift_end_time)
            {
                $('#halfday_before_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
        }
        else
        {
            if(shift_end_time<halfday_before_time && shift_start_time>halfday_before_time)
            {
                $('#halfday_before_time'+key).val('');
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set time between shift start and end time'
                });
                error = 1;
            }
        }
        if($('#copy_checkbox').is(":checked") && error == 0)
        {
            $('.hdbtp').val(value);
        }
    }

    $('.licam').keyup(function ()
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.licam').val(this.value);
        }
    });

    $('.eocbm').keyup(function ()
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.eocbm').val(this.value);
        }
    });

    $('.mtb').keyup(function ()
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.mtb').val(this.value);
        }
    });

    $('.mlb').keyup(function ()
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.mlb').val(this.value);
        }
    });

    $('.mpb').keyup(function ()
    {
        if($('#copy_checkbox').is(":checked"))
        {
            $('.mpb').val(this.value);
        }
    });

    function submitFormShiftNew()
    {
        if($('#addShiftForm').valid())
        {
            var error = 0;
            for(var i = 0; i <= 6; i++)
            {
                if($('#shift_start_time'+i).val() == '')
                {
                    error = 1;
                }
                if($('#shift_end_time'+i).val() == '')
                {
                    error = 1;
                }
            }
            if(error == 0)
            {
                $('#addShiftForm').submit();
            }
            else
            {
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'top right',
                    icon: 'fa fa-times-circle',
                    msg: 'Please set shift start & end time in all days'
                });
            }
        }
        else
        {
            $('#addShiftForm').validate();
        }
    }

    function showExtraDay()
    {
        var wi = $('#week_off_days').val();
        if(wi == null)
        {
            $('.sun').addClass("d-none");
            $('.mon').addClass("d-none");
            $('.tue').addClass("d-none");
            $('.wed').addClass("d-none");
            $('.thu').addClass("d-none");
            $('.fri').addClass("d-none");
            $('.sat').addClass("d-none");
        }
        if(wi.includes("0"))
        {
            $('.sun').removeClass("d-none");
        }
        else
        {
            $('.sun').addClass("d-none");
        }
        if(wi.includes("1"))
        {
            $('.mon').removeClass("d-none");
        }
        else
        {
            $('.mon').addClass("d-none");
        }
        if(wi.includes("2"))
        {
            $('.tue').removeClass("d-none");
        }
        else
        {
            $('.tue').addClass("d-none");
        }
        if(wi.includes("3"))
        {
            $('.wed').removeClass("d-none");
        }
        else
        {
            $('.wed').addClass("d-none");
        }
        if(wi.includes("4"))
        {
            $('.thu').removeClass("d-none");
        }
        else
        {
            $('.thu').addClass("d-none");
        }
        if(wi.includes("5"))
        {
            $('.fri').removeClass("d-none");
        }
        else
        {
            $('.fri').addClass("d-none");
        }
        if(wi.includes("6"))
        {
            $('.sat').removeClass("d-none");
        }
        else
        {
            $('.sat').addClass("d-none");
        }
    }

    function altWeekOffDays()
    {
        var wi = $('#week_off_days').val();
        var awi = $('#has_altenate_week_off_opt').val();
        if(awi == null)
        {
            if(wi.includes("0"))
            {
                $('.sun').removeClass("d-none");
            }
            else
            {
                $('.sun').addClass("d-none");
            }
            if(wi.includes("1"))
            {
                $('.mon').removeClass("d-none");
            }
            else
            {
                $('.mon').addClass("d-none");
            }
            if(wi.includes("2"))
            {
                $('.tue').removeClass("d-none");
            }
            else
            {
                $('.tue').addClass("d-none");
            }
            if(wi.includes("3"))
            {
                $('.wed').removeClass("d-none");
            }
            else
            {
                $('.wed').addClass("d-none");
            }
            if(wi.includes("4"))
            {
                $('.thu').removeClass("d-none");
            }
            else
            {
                $('.thu').addClass("d-none");
            }
            if(wi.includes("5"))
            {
                $('.fri').removeClass("d-none");
            }
            else
            {
                $('.fri').addClass("d-none");
            }
            if(wi.includes("6"))
            {
                $('.sat').removeClass("d-none");
            }
            else
            {
                $('.sat').addClass("d-none");
            }
        }
        if(awi.includes("0"))
        {
            $('.sun').addClass("d-none");
        }
        else
        {
            if(wi.includes("0"))
            {
                $('.sun').removeClass("d-none");
            }
        }
        if(awi.includes("1"))
        {
            $('.mon').addClass("d-none");
        }
        else
        {
            if(wi.includes("1"))
            {
                $('.mon').removeClass("d-none");
            }
        }
        if(awi.includes("2"))
        {
            $('.tue').addClass("d-none");
        }
        else
        {
            if(wi.includes("2"))
            {
                $('.tue').removeClass("d-none");
            }
        }
        if(awi.includes("3"))
        {
            $('.wed').addClass("d-none");
        }
        else
        {
            if(wi.includes("3"))
            {
                $('.wed').removeClass("d-none");
            }
        }
        if(awi.includes("4"))
        {
            $('.thu').addClass("d-none");
        }
        else
        {
            if(wi.includes("4"))
            {
                $('.thu').removeClass("d-none");
            }
        }
        if(awi.includes("5"))
        {
            $('.fri').addClass("d-none");
        }
        else
        {
            if(wi.includes("5"))
            {
                $('.fri').removeClass("d-none");
            }
        }
        if(awi.includes("6"))
        {
            $('.sat').addClass("d-none");
        }
        else
        {
            if(wi.includes("6"))
            {
                $('.sat').removeClass("d-none");
            }
        }
    }
</script>