<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='userPaymentReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-7">
        <h4 class="page-title"> Payment  Report</h4>

        </div>
      
      
     </div>

    
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");

                $q3=$d->select("unit_master,block_master,floors_master","block_master.block_id=floors_master.block_id AND  block_master.block_id=unit_master.block_id AND floors_master.floor_id=unit_master.floor_id AND  unit_master.society_id='$society_id' AND unit_master.unit_status!=0 $blockAppendQuery","ORDER BY block_master.block_sort,unit_master.unit_id ASC ");
                  $i=1;
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->floor; ?></th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Unpaid Penalty</th>
                        <th>Paid Penalty</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q3)) {
                    $unit_id=$data['unit_id'];
                    $floorId=$data['floor_id'];
                    $block_id=$data['block_id'];

                    $gu=$d->select("users_master","delete_status=0 AND unit_id='$unit_id' AND  block_id='$block_id' AND floor_id='$floorId' AND society_id='$society_id' AND member_status=0","ORDER BY user_id DESC");
                            $userData=mysqli_fetch_array($gu);
                    // echo '<pre>';
                    // print_r($data);
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['floor_name']; ?>-<?php echo $data['block_name']; ?></td>
                        <td><?php echo $userData['user_full_name']; ?></td>
                        <td><?php 
                        if(!empty($userData['user_mobile']) && $userData['user_mobile'] != "")
                        {
                          if ($adminData['role_id']==2) 
                          {
                            echo $userData['country_code'] . " " . $userData['user_mobile']; 
                          } 
                          else 
                          {
                            echo $userData['country_code'] . " " . substr($userData['user_mobile'], 0, 2) . '*****' . substr($userData['user_mobile'],  -3);
                          }
                        } ?>
                        </td>
                        <td>
                          <?php $count7=$d->sum_data("penalty_master.penalty_amount","penalty_master,users_master","penalty_master.user_id=users_master.user_id AND penalty_master.paid_status=0 AND penalty_master.unit_id='$data[unit_id]' AND users_master.user_id='$userData[user_id]'");
                            $row3=mysqli_fetch_array($count7);
                            $totalPenalyy=$row3['SUM(penalty_master.penalty_amount)'];
                            echo   number_format($totalPenalyy,2,'.','');
                            ?>
                        </td>
                        <td>
                          <?php $count7=$d->sum_data("penalty_master.penalty_amount","penalty_master,users_master","penalty_master.user_id=users_master.user_id AND penalty_master.paid_status=1 AND penalty_master.unit_id='$data[unit_id]' AND users_master.user_id='$userData[user_id]'");
                            $row3=mysqli_fetch_array($count7);
                            $totalPenalyy=$row3['SUM(penalty_master.penalty_amount)'];
                            echo   number_format($totalPenalyy,2,'.','');
                            ?>
                        </td>
                      
                         
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->