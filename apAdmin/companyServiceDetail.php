<?php error_reporting(0);?>

<?php if(isset($_GET) && $_GET['id']>0){ ?>
    <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Service Details With List </h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <a href="companyServiceDetail"  class="btn btn-sm btn-primary waves-effect waves-light"> Back </a>
      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <?php 
                $q1 = $d->selectRow('*',"company_service_details_master, company_service_master","company_service_details_master.company_service_details_id='$_GET[id]' AND company_service_details_master.company_service_id=company_service_master.company_service_id");
                $resultData = mysqli_fetch_assoc($q1); 
               ?>
              <div class="row">
                <div class="col-md-6 form-group">
                  
                  <label>Service Name</label>
                  <p><?php echo $resultData['company_service_name']; ?></p>
                 
                
                </div>
                <div class="col-md-6 form-group">
                    <label>Service Detail Name</label>
                  <p><?php echo $resultData['company_service_details_title']; ?></p>
                </div>
                <div class="col-md-12 form-group">
                <label>Description</label>
                  <p><?php echo $resultData['company_service_details_description']; ?></p>
                  </div>
                
                <div class="col-md-12 form-group">
                <label>Company Service Details List</label>
                <ul>
                    <?php 
                    $subq = $d->select("company_service_detail_list_master","company_service_details_id='$resultData[company_service_details_id]'");
                    while($dataList=mysqli_fetch_array($subq)){ 
                    ?>
                    <li><?php echo $dataList['company_service_detail_list_name']; ?></li>
                    <?php } ?>
                </ul>
                  </div>
                
                
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <?php }else{ ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Service Details</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addCompanyServiceDetail" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyServiceDetail');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Service Detail Name</th>
                        <th>Service Name</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("company_service_details_master,company_service_master","company_service_details_master.society_id='$society_id' AND company_service_details_master.company_service_id=company_service_master.company_service_id");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                       <?php  $totalServiceDetail = $d->count_data_direct("company_service_details_id","company_service_more_details_master","company_service_details_id='$data[company_service_details_id]'");
                              if( $totalServiceDetail==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['company_service_details_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_service_details_id']; ?>">                         
                        <?php } ?> 
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_service_name']; ?></td>
                       <td><?php echo $data['company_service_details_title']; ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                           <form action="addCompanyServiceDetail" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="company_service_details_id" value="<?php echo $data['company_service_details_id']; ?>">
                              <input type="hidden" name="edit_company_service_details" value="edit_company_service_details">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                            </form>
                            <a href="companyServiceDetail?id=<?php echo $data['company_service_details_id']; ?>" class="btn btn-sm btn-primary mr-2" onclick="productVendorDetailModal(<?php echo $data['company_about_us_id ']; ?>)"> <i class="fa fa-eye"></i></a>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>
<?php } ?>
<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Service Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addCompanyServiceDetailForm" action="controller/CompanyServiceDetailController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <select name="company_service_id" id="company_service_id" class="form-control single-select restFrm">
                        <option value="">-- Select Service --</option> 
                           <?php 
                             $qcs=$d->select("company_service_master","society_id='$society_id'");  
                             while ($serviceData=mysqli_fetch_array($qcs)) {
                           ?>
                         <option value="<?php echo  $serviceData['company_service_id'];?>" ><?php echo $serviceData['company_service_name'];?></option>
                         <?php } ?>
                 
                </select> 
            </div>  
           </div>
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <input type="text" class="form-control" placeholder="Service Detail Title" id="company_service_details_title" name="company_service_details_title" value="">
              </div>  
           </div>   
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail Description <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <textarea class="form-control" placeholder="Service Detail description" id="company_service_details_description" name="company_service_details_description"></textarea>
              </div>  
           </div>                 
           <div class="form-footer text-center">
             <input type="hidden" id="company_service_details_id" name="company_service_details_id" value="" >
             <button id="addCompanyServiceDetailBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addCompanyServiceDetail"  value="addCompanyServiceDetail">
             <button id="addCompanyServiceDetailBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyServiceDetailForm');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
