<?php 
if (filter_var($_GET['election_id'], FILTER_VALIDATE_INT) != true) {
  $_SESSION['msg1']="Invalid Election Details Request";
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='manageElections';
      </script>");
}
extract($_REQUEST);
$q = $d->select("election_master","election_id = '$election_id' AND society_id = '$society_id'");
if (mysqli_num_rows($q)>0) {
$data = mysqli_fetch_array($q);
extract($data);
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Manage Elections Participate <?php echo $xml->string->users; ?></h4>
       
      </div>
      <div class="col-sm-3">

        <?php //IS_720
        /* ?>
        <div class="btn-group float-sm-right">
          <a href="election" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>

          
        </div>
        <?php */ ?> 
      </div>
    </div>
    <!-- End Breadcrumb-->
    <?php //IS_470
    $approvedUsers = 0;
    $qNota=$d->select("election_users"," is_nota = '1' and  election_id='$election_id'","");
    $notaCount= mysqli_num_rows($qNota);
    $approvedUsers = $approvedUsers+$notaCount;
    $qEle=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id'","");
    while($rowData=mysqli_fetch_array($qEle)) {
       if($rowData['election_user_status']==1){  
        $approvedUsers++; 
      }   
    }    
    //IS_470?>

    <div class="row">
     
      <div class="col-lg-12">
        <div class="card">
          
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="row">
             <div class="col-md-6">
                <h4>Election: <?php echo $data['election_name']; ?></h4>
                <p>Description: <?php echo $data['election_description']; ?></p>
              </div>
              <div class="col-md-6">
                  <form action="controller/pullingController.php" method="post" id="statusForm">
                    Status:
                        <input type="hidden" name="election_id" value="<?php echo $data['election_id']; ?>">
                        <input type="hidden" name="election_for" value="<?php echo $data['election_for']; ?>">
                        <?php if($data['election_status']==0){?>
                        <input type="hidden" name="election_status" value="1" >
                        <input type="hidden" name="electionStart" value="electionStart" >
                        <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                        <label class="label label-danger">Nomination Open</label>
                        <?php //IS_470
                        if($approvedUsers>1 && $data['election_date']==date("Y-m-d")) {?> 
                        <input type="submit" name="" class="btn btn-danger form-btn btn-sm" value="Start Election ?" >
                        <?php } else if($approvedUsers>1 && $data['election_date']!=date("Y-m-d")) {?> 
                        <input type="button" name=""  onclick="swal('In Valid Election Start Date!');" class="btn btn-danger  btn-sm" value="Start Election ?" >
                        <?php } }else if($data['election_status']==1){?>
                        <input type="hidden" name="election_status" value="3">
                        <input type="hidden" name="electionEnd" value="electionEnd">
                        <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                        <label class="label label-danger">Election Running</label>
                       
                        <input type="submit" name="" class="btn btn-danger btn-sm form-btn" value="Close Election ?" >
                        <?php } else if($data['election_status']==2){?>
                       <label class="label label-danger">Result Published</label>
                        <?php } else if($data['election_status']==3){?>
                        <input type="hidden" name="election_status" value="3">
                        <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                        <input type="button" name="electionEnd" class="btn btn-warning btn-sm" value="Election Closed" onclick="return  swal('This Election is Closed', {icon: 'info', });">
                        <?php } ?>
                      </form>
                <p>Election Date: <?php echo $data['election_date']; ?></p>
              </div>
            </div>
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th><?php echo $xml->string->unit; ?></th>
                    <th>Status</th>
                    <th>Message</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $q=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id'","");
                  $i = 0;
                  $cnt = 1;
                  while($row=mysqli_fetch_array($q))
                  {
                    $cnt++;
                  // extract($row);
                  $i++;
                  
                  ?>
                  <tr>
                  
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['user_full_name']; ?></td>
                    <td><?php echo $row['user_designation']; ?>-<?php echo $row['block_name']; ?></td>
                    <td>
                      <form action="controller/pullingController.php" method="post" id="statusForm">
                        <input type="hidden" name="unit_id" value="<?php echo $row['unit_id']; ?>">
                        <input type="hidden" name="election_id" value="<?php echo $row['election_id']; ?>">
                        <input type="hidden" name="election_user_id" value="<?php echo $row['election_user_id']; ?>">


                        <?php if($row['election_user_status']==0) {?>
                        <input type="hidden" name="election_user_status" value="1" >
                        <input type="hidden" name="unit_id" value="<?php echo $row['unit_id']; ?>">
                        <?php ////IS_555 ?>
                         <input type="hidden" name="user_id" value="<?php echo $row['user_id']; ?>">
                        <input type="hidden" value="nomApporve" name="nomApporve">
                        <input type="submit"  class="btn btn-primary btn-sm form-btn" value="Approve ?" >
                        <?php ////IS_555 ?>
                        <input data-toggle="modal" data-target="#rejeMdal" type="button" name="nomReject" class="btn btn-danger btn-sm" value="Reject ?" onclick="norReject('<?php echo $row['election_id']; ?>','<?php echo $row['election_user_id']; ?>','<?php echo $row['unit_id']; ?>','<?php echo $row['user_id']; ?>');">
                        <?php }else if($row['election_user_status']==1){?>
                        <input type="hidden" name="election_user_status" value="3">
                         <input type="hidden" name="user_id" value="<?php echo $row['user_id']; ?>">
                         <input type="hidden" name="election_name" value="<?php echo $row['election_name']; ?>" >
                        <input type="button" name="nomRejct" class="btn text-white bg-success btn-sm" value="Approved"  onclick="return  swal('This User is Approved for Election', {icon: 'info', });">
                        <?php } else if($row['election_user_status']==2){?>
                        <input type="button" name="electionEnd" class="btn btn-danger btn-sm" value="Rejected" onclick="return  swal('This user is rejected', {icon: 'info', });">
                        <?php } ?>
                      </form>
                    </td>
                    <td><?php echo $row['reject_msg']; ?></td>
                   
                    
                    
                    
                  </tr>
                  <?php }

                  $q2=$d->select("election_users"," is_nota = '1' and  election_id='$election_id'","");
                  
                   $row2=mysqli_fetch_assoc($q2);  
                   if(!empty($row2)){ ?>
                  <tr>
                    <td><?php echo $cnt; ?></td>
                    <td>NOTA</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
                
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="rejeMdal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reject User </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="common-form" action="controller/pullingController.php" method="post">
            <input type="hidden" id="EUID" name="election_user_id">
            <input type="hidden" id="eId" name="election_id">
            <input type="hidden" id="unitId" name="unit_id">
            <?php ////IS_555 ?>
             <input type="hidden" id="user_id" name="user_id">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Rejection Message <span class="text-danger">*</span></label>
                    <div class="col-sm-8" id="parkingName1">
                      <textarea required="" class="form-control" name="reject_msg"></textarea>
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <input type="hidden" name="nomReject" value="nomReject">
                  <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Reject</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div>

<script type="text/javascript">
  <?php ////IS_555 ?>
  function norReject(election_id,election_user_id,unit_id,user_id) {
    document.getElementById('EUID').value=election_user_id;
    document.getElementById('eId').value=election_id;
    document.getElementById('unitId').value=unit_id;
    document.getElementById('user_id').value=user_id;

  }
</script>
<?php } else{ ?>
<div class="content-wrapper">
  <img src="img/no_data_found.png">
  <div class="container-fluid">
  </div>
</div>
<?php } ?>