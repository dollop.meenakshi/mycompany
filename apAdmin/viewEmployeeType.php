  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-8">
        <h4 class="page-title"><?php echo $xml->string->employee; ?> Types</h4>
     </div>
     <div class="col-sm-3 col-4">
       <div class="btn-group float-sm-right">
        <a href="employeeType" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>   

        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
          <div class="row" id="searchedEmployees">
      <?php
        $i++;
        $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0");
        if(mysqli_num_rows($q)>0) {

          while ($row=mysqli_fetch_array($q)) {
            extract($row);
          
      ?>  
      <div class="col-lg-3 col-6">
        <div id="accordion1">
          <div class="card mb-2">
            <div id="collapse-1" class="collapse show" data-parent="#accordion1" style="">
              <div class="p-2">
                <div class="text-center">
                  <img style="height: 150px !important;width: 80;" onerror="this.src='img/user.png'"   src="../img/emp_icon/<?php echo $row['emp_type_icon']; ?>"class="img-fluid rounded "  alt="card img">
                </div>
                <div class="card-title text-uppercase text-primary text-center"> <?php echo $row['emp_type_name']; ?>
                </div>
                
                <ul  class="list-inline text-center">
                  
                  <li class="list-inline-item">
                    <?php if ($row['emp_type_id']!=1): ?>
                        
                      <form action="employeeType" method="post" >
                        <input type="hidden" name="emp_type_id_edit" value="<?php echo $row['emp_type_id']; ?>">
                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit</button></td>
                      </form>
                      <?php endif ?>
                  </li>

                  <li class="list-inline-item">
                     <?php if ($row['emp_type_id']!=1):
                    $totalEmp= $d->count_data_direct("emp_id","employee_master","society_id='$society_id' AND emp_type_id='$row[emp_type_id]'"); 
                          if ($totalEmp==0) {
                      ?>
                    <form title="Delete" action="controller/employeeController.php" method="post"  >
                      <input type="hidden" name="emp_type_id" value="<?php echo $row['emp_type_id']; ?>">
                      <input type="hidden" value="empTypeDelete" name="empTypeDelete">
                      <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button> 
                    </form>
                  <?php } else { ?>

                      <button type="button"  onclick="swal('Please Delete this type <?php echo $xml->string->employee; ?> First...!');"  class="btn btn-danger btn-sm "><i class="fa fa-trash-o"></i></button> 
                      <?php } endif ?>
                  </li>
                 
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <?php } } else {
        echo "<img src='img/no_data_found.png'>";
      } ?>
    </div>


    

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



