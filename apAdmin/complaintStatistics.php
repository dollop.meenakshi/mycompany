<?php 
error_reporting(0);

  extract($_REQUEST);
  $complaint_category_id=$adminData['complaint_category_id'];
  $compCtgAry = explode(",",$complaint_category_id);
  $ids = join("','",$compCtgAry); 
  $compCtgAryFinal = array_filter($compCtgAry);
  $compCtgAryFinal = array_values($compCtgAryFinal);
  $month = (int)$month;
  $year = (int)$year;
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Complaint Statistics Report</h4>
        </div>
      </div>
      <?php

       if($_GET['year']){ 
        $date = $year.'-'.$month;
        $from = $date.'-01';
        $toDate = $date.'-31';
      }else{
        $month = date('m');
        $date = date('Y-m');
        $from = $date.'-01';
        $toDate = $date.'-31';
      } ?>
     <form action="" method="get">
     <div class="row pt-2 pb-2">
          <div class="col-lg-3 col-6">
            <label  class="form-control-label">Select Month </label>
             <select required="" class="form-control single-select" name="month">
                <option value="01" <?= ($_GET) ? (($_GET['month']=='01') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='01') ? 'selected' : '' ) : ''  ?> >January</option>
                <option value="02" <?= ($_GET) ? (($_GET['month']=='02') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='02') ? 'selected' : '' ) : ''  ?> >February</option>
                <option value="03" <?= ($_GET) ? (($_GET['month']=='03') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='03') ? 'selected' : '' ) : ''  ?> >March</option>
                <option value="04" <?= ($_GET) ? (($_GET['month']=='04') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='04') ? 'selected' : '' ) : ''  ?> >April</option>
                <option value="05" <?= ($_GET) ? (($_GET['month']=='05') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='05') ? 'selected' : '' ) : ''  ?> >May</option>
                <option value="06" <?= ($_GET) ? (($_GET['month']=='06') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='06') ? 'selected' : '' ) : ''  ?> >June</option>
                <option value="07" <?= ($_GET) ? (($_GET['month']=='07') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='07') ? 'selected' : '' ) : ''  ?> >July</option>
                <option value="08" <?= ($_GET) ? (($_GET['month']=='08') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='08') ? 'selected' : '' ) : ''  ?> >August</option>
                <option value="09" <?= ($_GET) ? (($_GET['month']=='09') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='09') ? 'selected' : '' ) : ''  ?> >September</option>
                <option value="10" <?= ($_GET) ? (($_GET['month']=='10') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='10') ? 'selected' : '' ) : ''  ?> >October</option>
                <option value="11" <?= ($_GET) ? (($_GET['month']=='11') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='11') ? 'selected' : '' ) : ''  ?> >November</option>
                <option value="12" <?= ($_GET) ? (($_GET['month']=='12') ? 'selected' : '' ) : ''  ?>  <?= ($month) ? (($month=='12') ? 'selected' : '' ) : ''  ?> >December</option>
             </select>
          </div>
          <div class="col-lg-3 col-6">
            <label  class="form-control-label">Select Year </label>
             <select required="" class="form-control single-select" name="year">
                <?php
                  for($y=date("Y"); $y>=date("Y")-5; $y--){  ?>
                      <option <?php if($y==$year) { echo 'selected'; } ?> value="<?php echo $y; ?>"><?php echo $y; ?></option>";
                  <?php }
                  ?>
             </select>
          </div>           
          <div class="col-lg-1 col-6">
            <label  class="form-control-label"> </label>
              <!-- <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report"> -->
              <button type="submit" class="btn btn-success" name="getReport" style="margin-top:5px;">GET REPORT</button>
          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                  <?php
                    $opentotal=0;
                    $closetotal=0;
                    $inprogresstotal=0;
                    $reopentotal=0;
                    $deletedtotal=0;
                    $total=0;
                  ?>
                  <tr>
                      <th>Month/Year</th>
                      <th>Status</th>
                      <?php $q=$d->select("complaint_category","","");
                        while ($mData=mysqli_fetch_array($q)) { ?>
                          <th><?php echo $mData['category_name']; ?></th>
                      <?php } ?>

                      <th  class="bg-primary text-white">Total</th>           
                  </tr>
                </thead>
                  <tr>
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>Open</th>
                    <?php
                    $from = date('Y-m-d 00:00:00', strtotime($from));
                    $toDate = date('Y-m-d 23:59:00', strtotime($toDate));
                    
                     $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='0' AND flag_delete='0' AND complaint_category ='$catid'","");
                     }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='0' AND flag_delete='0' AND complaint_category ='$catid' AND complaint_category IN ('$ids')","");
                     } ?>
                        <td><?php echo $countopen->num_rows; ?></td>
                        <?php $opentotal = $opentotal+$countopen->num_rows;
                     } ?>
                    <td  class="bg-primary text-white"><?=$opentotal?></td>
                  </tr>
                  <tr>
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>Close</th>
                    <?php $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='1' AND flag_delete='0' AND complaint_category ='$catid'","");
                     }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='1' AND flag_delete='0' AND complaint_category ='$catid' AND complaint_category IN ('$ids')","");
                     }?>
                        <td><?php echo $countopen->num_rows; ?></td>
                    <?php $closetotal = $closetotal+$countopen->num_rows;
                     } ?>
                    <td  class="bg-primary text-white"><?=$closetotal?></td>
                  </tr>
                  <tr>
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>In Progress</th>
                    <?php $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='3' AND flag_delete='0' AND complaint_category ='$catid'","");
                    }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='3' AND flag_delete='0' AND complaint_category ='$catid' AND complaint_category IN ('$ids')","");
                    }?>
                        <td><?php echo $countopen->num_rows; ?></td>
                    <?php $inprogresstotal = $inprogresstotal+$countopen->num_rows;
                     } ?>
                    <td  class="bg-primary text-white"><?=$inprogresstotal?></td>
                  </tr>
                  <tr>
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>Re Open</th>
                    <?php $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='2' AND flag_delete='0' AND complaint_category ='$catid'","");
                     }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complain_status='2' AND flag_delete='0' AND complaint_category ='$catid' AND complaint_category IN ('$ids')","");
                     } ?>
                        <td><?php echo $countopen->num_rows; ?></td>
                    <?php $reopentotal = $reopentotal+$countopen->num_rows;
                     } ?>
                    <td  class="bg-primary text-white"><?=$reopentotal?></td>
                  </tr>
                  <tr>
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>Deleted</th>
                    <?php $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND flag_delete='1' AND complaint_category ='$catid'","");
                     }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND flag_delete='1' AND complaint_category ='$catid' AND complaint_category IN ('$ids')","");
                     } ?>
                        <td><?php echo $countopen->num_rows; ?></td>
                    <?php $deletedtotal = $deletedtotal+$countopen->num_rows;
                     } ?>
                    <td  class="bg-primary text-white"><?=$deletedtotal?></td>
                  </tr>
                  <tr  class="bg-primary text-white">
                    <th><?=date('F-Y', strtotime($from))?></th>
                    <th>Total</th>
                    <?php $q=$d->select("complaint_category","","");
                     while ($mData=mysqli_fetch_array($q)) {
                      $catid = $mData['complaint_category_id'];
                      if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='')) {
                     $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complaint_category ='$catid' AND flag_delete!=2",""); 
                     }else{
                      $countopen = $d->select("complains_master","society_id='$society_id' AND complain_date BETWEEN '$from' AND '$toDate' AND complaint_category ='$catid' AND flag_delete!=2 AND complaint_category IN ('$ids')",""); 
                     } ?>
                        <th><?php echo $countopen->num_rows; ?></th>
                    <?php $total = $total+$countopen->num_rows;
                     } ?>
                    <th><?=$total?></th>
                  </tr>
                <tbody>
                  
                    
                </tbody>  
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->