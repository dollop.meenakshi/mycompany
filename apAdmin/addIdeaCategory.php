<?php
extract(array_map("test_input", $_POST));
if (isset($edit_idea_category)) {
    $q = $d->select("idea_category_master","idea_category_id='$idea_category_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Idea Category</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
           
                <form id="ideaCategoryAdd" action="controller/ideaCategoryController.php" enctype="multipart/form-data" method="post">

                <div class="form-group row">
                    <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Idea Category Name <span class="required">*</span></label>
                    <div class="col-lg-10 col-md-10" id="">
                        <input type="text" class="form-control" placeholder="Idea Category Name" name="idea_category_name" value="<?php if($data['idea_category_name'] !=""){ echo $data['idea_category_name']; } ?>">
                    </div>
                    
                </div>                   
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn 
                  if (isset($edit_idea_category)) {                    
                  ?>
                  <input type="hidden" name="idea_category_id" value="<?php if($data['idea_category_id'] !=""){ echo $data['idea_category_id']; } ?>" >
                  <button id="addIdeaCategoryBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                 <input type="hidden" name="addIdeaCategory"  value="addIdeaCategory">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('ideaCategoryAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                      ?>
                 
                  <button id="addIdeaCategoryBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                 <input type="hidden" name="addIdeaCategory"  value="addIdeaCategory">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('ideaCategoryAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                  <?php } ?>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->

    <script type="text/javascript">
       function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
      }

       function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
      }

    </script>