<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$cId = (int)$_POST['cId'];
$society_id=$_COOKIE['society_id'];
?>


  <div class="form-group row">
    <label for="input-10" class="col-sm-4 col-form-label">Vendor <span class="required">*</span></label>
    <div class="col-lg-8 col-md-8" id="">
      <select type="text" multiple class="form-control single-select frmInputSl" id="vendor_id" name="vendor_id[]">
        <option value="">-- Select --</option>
        <?php
        $getVendorCat = $d->selectRow('*', 'product_category_vendor_master', "product_category_id=$cId");
        $filter = "";
        $vIds = array();
        if (mysqli_num_rows($getVendorCat) > 0) {
          while ($data = mysqli_fetch_assoc($getVendorCat)) {
            if ($data) {
              $vId  = $data['vendor_id'];
              array_push($vIds, $vId);
            }
          }
          if (!empty($vIds)) {
            $vendorIds = join("','", $vIds);
            $filter = " AND service_provider_users_id NOT IN ('$vendorIds');";
          }
        }

        $floor = $d->select("local_service_provider_users", " service_provider_status=0 AND service_provider_delete_status=0 $filter ");
        while ($floorData = mysqli_fetch_array($floor)) {
        ?>
          <option <?php if (isset($data['vendor_id']) && $data['vendor_id'] == $floorData['service_provider_users_id']) { echo "selected";} ?> value="<?php if (isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] != "") { echo $floorData['service_provider_users_id']; } ?>"><?php if (isset($floorData['service_provider_name']) && $floorData['service_provider_name'] != "") { echo $floorData['service_provider_name'];} ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-footer text-center">
    <input type="hidden" id="product_category_id" name="product_category_id" value="<?php echo $cId; ?>" class="">
    <button id="addproductCategoryBtn" name="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
    <input type="hidden" name="addproductCategoryVendor" value="addproductCategoryVendor">

    <button id="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addproductCategoryForm');"><i class="fa fa-check-square-o"></i> Reset</button>
  </div>

<script type="text/javascript" src="assets/js/select.js"></script>