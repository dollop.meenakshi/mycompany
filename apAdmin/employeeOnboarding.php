
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/css/jquery.steps.css">
<style type="text/css">
    .wizard>.steps .current a,
    .wizard>.steps .current a:hover,
    .wizard>.steps .current a:active {
        background: #1a4089;
    }

    .wizard>.actions a,
    .wizard>.actions a:hover,
    .wizard>.actions a:active {
        background: #1a4089;
    }

    .wizard>.steps>ul>li {
        width: auto;
    }

    .wizard>.content>.body {
        position: inherit;
    }

    .dtp .p10>a {
        color: #000;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title"> Employee Onboarding</h4>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-2">
                        <form id="wizard-validation-form" action="#">
                            <input type="hidden" name="employeeOnboarding" value="employeeOnboarding">
                            <input type="hidden" name="unit_status" value="1">
                            <div>
                                <h3>Basic Info</h3>
                                <section class="p-0">
                                    <div class="form-row">
                                        <div class="col-md-12">
                                          <div class="col-sm-12 text-center">
                                            <label for="imgInpProfile">
                                              <img id="profileView" style="border: 1px solid gray;" src="img/user.png"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                                                <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                                               <input accept="image/*" class="photoInput photoOnly d-none" accept="image/*" id="imgInpProfile" type="file" name="user_profile_pic">
                                             </label>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="unit_name" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->employee_id; ?>  </label>
                                                <input type="hidden" name="employee_id_generate" value="<?php echo $sData['employee_id_generate'] ?>">
                                                <input maxlength="50" type="text" class="form-control" id="unit_name" name="unit_name" <?php if($sData['employee_id_generate'] == '1'){echo 'readonly';}?> placeholder="<?php if($sData['employee_id_generate'] == '1'){echo 'Auto Generated';}?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_first_name" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->first_name; ?> <span class="text-danger">*</span></label>
                                                <input autocomplete="off"  required="" maxlength="50" type="text" class="form-control text-capitalize onlyName" name="user_first_name" id="user_first_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_middle_name" class="col-lg-12 col-md-12 col-form-label">Middle Name </label>
                                                <input autocomplete="off" maxlength="50" type="text" class="form-control text-capitalize onlyName" name="user_middle_name" id="user_middle_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_last_name" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->last_name; ?> <span class="text-danger">*</span></label>
                                                <input required="" autocomplete="off"  maxlength="50" type="text" class="form-control text-capitalize onlyName" name="user_last_name" id="user_last_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_mobile" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->mobile_no; ?> <span class="text-danger">*</span></label>
                                                <div class="col-lg-5 col-md-5">
                                                    <input type="hidden" value="+91" id="country_code_get" name="">
                                                    <select name="country_code" class="form-control single-select" id="country_code" required="">
                                                        <?php include 'country_code_option_list.php'; ?>
                                                    </select>
                                                </div>
                                                 <div class="col-lg-7 col-md-7">
                                                    <input required="" autocomplete="off"  min="1" type="text"  maxlength="15" minlength="8" class="form-control onlyNumber  " id="user_mobile" inputmode="numeric" name="user_mobile">
                                                  </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->date_of_birth; ?> </label>
                                                <div class="col-lg-12 col-md-12">
                                                    <input class="form-control" readonly="" id="autoclose-datepicker-dob" name="member_date_of_birth" type="text" value="" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-form-label form-control-label"><?php echo $xml->string->blood_group; ?></label>
                                                <select name="blood_group" class="form-control" >
                                                  <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                                                  <option value="A+">A+</option>
                                                  <option value="A-">A-</option>
                                                  <option value="B+">B+</option>
                                                  <option value="B-">B-</option>
                                                  <option value="AB+">AB+</option>
                                                  <option value="AB-">AB- </option>
                                                  <option value="O+">O+</option>
                                                  <option value="O-">O-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-md-12 col-form-label form-control-label"><?php echo $xml->string->gender; ?></label>
                                                <select name="gender" class="form-control" >
                                                  <option value="Male">Male</option>
                                                  <option value="Female">Female</option>
                                                </select>
                                                <!-- <div class="col-lg-12 col-md-12">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" checked="" class="form-check-input" value="Male" name="gender"> <?php echo $xml->string->male; ?>
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio"  class="form-check-input" value="Female" name="gender"> <?php echo $xml->string->female; ?>
                                                        </label>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-form-label form-control-label">Resume/CV</label>
                                                <input  class="form-control" name="resume_cv_doc" type="file">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-form-label form-control-label">ID Proof</label>
                                                <input type="hidden" value="" name="id_proof_pages" id="id_proof_pages">
                                                <select name="id_proof_id" id="id_proof_id" class="form-control single-select" onchange="selectIdProofDocument(this.value)">
                                                    <option value="">-- Select ID Proof --</option> 
                                                    <?php 
                                                    $idpq=$d->select("id_proof_master","society_id='$society_id' AND id_proof_status=0");  
                                                    while ($idProofData=mysqli_fetch_array($idpq)) {
                                                    ?>
                                                    <option data-id="<?php echo  $idProofData['id_proof_pages'];?>" value="<?php echo  $idProofData['id_proof_id'];?>" ><?php echo $idProofData['id_proof_name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0" id="id_prrof_document_div">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h3>Job Info</h3>
                                <section class="p-0">
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="designation" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->designation; ?> <span class="text-danger">*</span>  </label>
                                                <input maxlength="50" required="" type="text" class="form-control" id="designation" name="designation">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="block_id" class="col-lg-12 col-md-12 col-form-label">Branch <span class="text-danger">*</span></label>
                                                <select name="block_id" id="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
                                                    <option value="">-- Select Branch --</option> 
                                                    <?php 
                                                    $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                                                     while ($blockData=mysqli_fetch_array($qb)) { ?>
                                                    <option  <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="floor_id" class="col-lg-12 col-md-12 col-form-label">Department <span class="text-danger">*</span></label>
                                                <select name="floor_id" id="floor_id" class="form-control single-select" onchange="getShiftByFloorId(this.value); getSubDepartmentByFloorId(this.value)">
                                                    <option value="">-- Select Department --</option> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="sub_department_id" class="col-lg-12 col-md-12 col-form-label">Sub <?php echo $xml->string->floor; ?></label>
                                                <select type="text" class="form-control single-select" name="sub_department_id" id="sub_department_id">
                                                  <option value="">-- <?php echo $xml->string->select; ?> Sub <?php echo $xml->string->floor; ?> --</option>
                                                  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-form-label form-control-label"><?php echo $xml->string->employment_type; ?></label>
                                                <select name="employment_type" class="form-control single-select" id="employment_type">
                                                    <option  value="">--Select Type--</option>
                                                    <option value="1">Full Time</option>
                                                    <option value="2" >Part Time</option>
                                                    <option value="3" >Seasonal</option>
                                                    <option value="4" >Temporary</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php $totalZone = $d->count_data_direct("zone_id","zone_master","society_id='$society_id' AND zone_status = 0");
                                          if( $totalZone > 0) {
                                        ?>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="zone_id" class="col-lg-12 col-md-12 col-form-label">Zone</label>
                                                <select type="text" class="form-control single-select" name="zone_id" id="zone_id">
                                                  <option value="">-- <?php echo $xml->string->select; ?> Zone --</option>
                                                  <?php
                                                      $zq=$d->select("zone_master","society_id='$society_id' AND zone_status='0'");
                                                      while ($zqData=mysqli_fetch_array($zq)) { ?>
                                                        <option value="<?php echo $zqData['zone_id'];?>"> <?php echo $zqData['zone_name'];?></option>
                                                      <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php $totalLevel = $d->count_data_direct("level_id","employee_level_master","society_id='$society_id' AND level_status = 0");
                                          if( $totalLevel > 0) {
                                        ?>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="level_id" class="col-lg-12 col-md-12 col-form-label">Employee Level</label>
                                                <select type="text" class="form-control single-select" name="level_id" id="level_id">
                                                  <option value="">-- <?php echo $xml->string->select; ?> Employee Level --</option>
                                                  <?php
                                                      $lq=$d->select("employee_level_master","society_id='$society_id' AND level_status='0'");
                                                      while ($lqData=mysqli_fetch_array($lq)) { ?>
                                                        <option value="<?php echo $lqData['level_id'];?>"> <?php echo $lqData['level_name'];?></option>
                                                      <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="shift_time_id" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->shift; ?></label>
                                                <select type="text" class="form-control single-select" name="shift_time_id" id="shift_time_id">
                                                  <option value="">-- <?php echo $xml->string->select; ?> <?php echo $xml->string->shift; ?> --</option>
                                                  
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-md-12 col-form-label">Work <?php echo $xml->string->email_id; ?> </label>
                                                <div class="col-lg-12 col-md-12">
                                                    <input  type="email" autocomplete="off" class="form-control"   id="userEmail" name="user_email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->date_of_join; ?>  <span class="text-danger">*</span> </label>
                                                <div class="col-lg-12 col-md-12">
                                                    <input required class="form-control" readonly="" id="autoclose-datepicker-doj" name="joining_date" type="text" value="" >
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $check_sc = $d->selectRow("sister_company_id,sister_company_name","sister_company_master","society_id = '$society_id' AND status = 1");
                                        if(mysqli_num_rows($check_sc) > 0)
                                        {
                                        ?>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label class="col-lg-12 col-md-12 col-form-label">Sister Company</label>
                                                <div class="col-lg-12 col-md-12">
                                                    <select class="form-control single-select" id="sister_company_id" name="sister_company_id">
                                                        <option>Select</option>
                                                        <?php
                                                        while($row = $check_sc->fetch_assoc())
                                                        {
                                                        ?>
                                                        <option <?php if($sister_company_id == $row['sister_company_id']){ echo "selected"; } ?> value="<?php echo $row['sister_company_id']; ?>"><?php echo $row['sister_company_name']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </section>
                                <h3>Contact Info</h3>
                                <section class="p-0">
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_mobile" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->whatsapp; ?> Number </label>
                                                <div class="col-lg-5 col-md-5">
                                                    <input type="hidden" value="<?php echo $country_code_whatsapp; ?>" id="country_code_whatsapp_get" name="">
                                                    <select name="country_code_whatsapp" class="form-control single-select" id="country_code_whatsapp">
                                                      <?php include 'country_code_option_list.php'; ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-7 col-md-7">
                                                    <input class="form-control no_copy onlyNumber" inputmode="numeric" name="whatsapp_number"  maxlength="15" minlength="8"  type="text" id="whatsapp_number">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_mobile" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->alternate_phone_number; ?> </label>
                                                <div class="col-lg-5 col-md-5">
                                                    <input type="hidden" value="<?php echo $country_code_alt; ?>" id="country_code_get_alt" name="">
                                                    <select name="country_code_alt" class="form-control single-select" id="country_code_alt" >
                                                      <?php include 'country_code_option_list.php'; ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-7 col-md-7">
                                                    <input class="form-control onlyNumber" inputmode="numeric" name="alt_mobile"  maxlength="15" minlength="8"  type="text" id="alt_mobile">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="user_mobile" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->emergency_number; ?> </label>
                                                <div class="col-lg-5 col-md-5">
                                                    <input type="hidden" value="<?php echo $country_code_emergency; ?>" id="country_code_emergency_get" name="">
                                                    <select name="country_code_emergency" class="form-control single-select" id="country_code_emergency">
                                                      <?php include 'country_code_option_list.php'; ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-7 col-md-7">
                                                    <input class="form-control no_copy onlyNumber" inputmode="numeric" name="emergency_number"  maxlength="15" minlength="8"  type="text" id="emergency_number">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="designation" class="col-lg-12 col-md-12 col-form-label">Current Address </label>
                                                <textarea maxlength="200" value="<?php echo $last_address; ?>" class="form-control" type="text" name="last_address" value=""></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->permanent_address; ?> </label>
                                                <textarea maxlength="200" value="<?php echo $permanent_address; ?>" class="form-control" type="text" name="permanent_address" value=""></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label">Personal Email </label>
                                                <input class="form-control" type="email" name="personal_email"  value="<?php echo $personal_email; ?>" id="personal_email">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="facebook" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->facebook; ?> </label>
                                                <input class="form-control" type="text" name="facebook"  value="" id="facebook">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="linkedin" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->linkedin; ?> </label>
                                                <input class="form-control" type="text" name="linkedin"  value="" id="linkedin">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="twitter" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->twitter; ?> </label>
                                                <input class="form-control" type="text" name="twitter"  value="" id="twitter">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->instagram; ?> </label>
                                                <input class="form-control" type="text" name="instagram"  value="" id="instagram">
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                <h3>Other Info</h3>
                                <section class="p-0">
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->professional_skills; ?> </label>
                                                <textarea maxlength="300" class="form-control" name="professional_skills" type="text" ></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->hobbies; ?> & INTERESTS </label>
                                                <textarea maxlength="300" class="form-control" name="intrest_hobbies" type="text" ></textarea>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->language_known; ?> </label>
                                                <textarea maxlength="300" class="form-control" name="language_known" type="text" value=""></textarea>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->special_skills; ?> </label>
                                                <textarea maxlength="300" class="form-control" name="special_skills" type="text" value=""></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->marital_status; ?> </label>
                                                <?php 
                                                  $marital_status_string= $xml->string->marital_status_array; 
                                                  $marital_status_array = explode("~", $marital_status_string);
                                                ?>
                                                <select name="marital_status" class="form-control single-select" onchange="change_marital_status(this.value)">
                                                  <?php for ($iR=0; $iR < count($marital_status_array) ; $iR++) {  ?>
                                                    <option value="<?php echo $iR;?>"><?php echo $marital_status_array[$iR];?></option>
                                                  <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mb-3 hide_wedding_anniversary_date d-none">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label">Wedding Anniversary Date </label>
                                                <input class="form-control valid" readonly="" value="" id="wedding_anniversary_date" name="wedding_anniversary_date" type="text"  aria-invalid="false">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->family_members; ?> </label>
                                                <input maxlength="30" class="form-control onlyNumber" name="total_family_members" type="text" value="<?php echo $total_family_members; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->nationality; ?> </label>
                                                <select name="nationality_drop" id="nationality_drop" class="form-control single-select" >
                                                    <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                                                    <option value="indian">Indian</option>
                                                    <option value="other">Other</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mb-3 d-none" id="nationality_div">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-12 col-md-12 col-form-label">Enter <?php echo $xml->string->nationality; ?> </label>
                                                <input maxlength="30" class="form-control" name="nationality" type="text" value="Indian">
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                <h3>Bank Detail</h3>
                                <section class="p-0">
                                    <!-- <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="permanent_address" class="col-lg-4 col-md-4 col-form-label">Add Bank Detail </label>
                                                <div class="col-lg-4 col-md-4">
                                                    <select name="add_bank_detail" id="add_bank_detail" class="form-control" >
                                                    <option value="no">No</option>
                                                    <option value="yes">Yes</option>
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="form-row">
                                        <input type="hidden" name="add_bank_detail" value="yes">
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="account_holders_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Account Holder Name</label>
                                                <input type="text" name="account_holders_name" id="account_holders_name" class="form-control text-capitalize bankInChange" autocomplete="off" maxlength="200">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Bank Name</label>
                                                <input type="text" name="bank_name" id="bank_name" class="form-control text-capitalize bankInChange" autocomplete="off" maxlength="150">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Branch Name</label>
                                                <input type="text" name="bank_branch_name" id="bank_branch_name" class="form-control bankInChange">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Account Type</label>
                                                <select name="account_type" id="account_type" class="form-control bankInChange" >
                                                    <option value="">Select</option>
                                                    <option value="Saving">Saving</option>
                                                    <option value="Current">Current</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Account No</label>
                                                <input type="text" name="account_no" id="account_no" class="form-control bankInChange">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">IFSC Code</label>
                                                <input type="text" maxlength="11"  name="ifsc_code" id="ifsc_code" class="form-control text-uppercase bankInChange">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Customer Id/CRN No. </label>
                                                <input type="text" name="crn_no" id="crn_no" class="form-control bankInChange">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">ESIC No </label>
                                                <input type="text" maxlength="10" name="esic_no" id="esic_no" class="form-control bankInChange">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row w-100 mx-0">
                                                <label for="bank_branch_name" class="col-lg-12 col-md-12 col-form-label bankReqSpan">Pan Card No </label>
                                                <input type="text" maxlength="10"  name="pan_card_no" id="pan_card_no" class="form-control text-uppercase bankInChange">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script src="assets/plugins/jquery.steps/js/jquery.steps.min.js"></script>

<script type="text/javascript">
    $.validator.addMethod("noSpace", function(value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");

    $.validator.addMethod("uniqueMobile", function (value, element) {
        let result = false;
        if(value!=""){
            $.ajax({
                type: "POST",
                url: "controller/uniqueController.php",
                data: { userMobile : value,checkUserMobile:'checkUserMobile',csrf:csrf },
                dataType: "JSON",
                success: function (data) {
                    if(data === 1){
                        result = false;
                    }else{
                        result = true;
                    }
                },
                async: false
            });
        }else{
            result = true;
        }
        return result;
    });

    ! function($) {
        "use strict";
        var FormWizard = function() {};
        FormWizard.prototype.createValidatorForm = function($form_container) {

            $form_container.validate({

                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass('select2-hidden-accessible')) {     
                        error.insertAfter(element.next('span'));  // select2
                        element.next('span').addClass('error').removeClass('valid');
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                },

                rules: {
                    designation: {
                        required: true,
                        noSpace: true,
                    },
                    block_id: {
                        required: true,
                    },
                    floor_id: {
                        required: true,
                    },
                    user_first_name: {
                        required: true,
                        noSpace: true,
                    },
                    user_last_name: {
                        required: true,
                        noSpace: true,
                    },
                    user_mobile: {
                        required: true,
                        noSpace: true,
                        uniqueMobile: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    "id_proof[]": {
                        required: true,
                    },
                    bank_name: {
                        required: false,
                        noSpace:true,
                    },  
                    bank_branch_name: {
                        required: false,
                        noSpace:true,
                    },  
                    account_type: {
                        required: false,
                    },  
                    account_no: {
                        required: false,
                        noSpace:true,
                    },
                    ifsc_code: {
                        required: false,
                        noSpace:true,
                    },
                },
                messages: {
                    designation: {
                        required: "Please Enter Designation",
                        noSpace: "No space please and don't leave it empty",
                    },
                    block_id: {
                        required: "Please Select Branch",
                    },
                    floor_id: {
                        required: "Please Select Branch",
                    },
                    user_first_name: {
                        required: "Please Enter First Name",
                        noSpace: "No space please and don't leave it empty",
                    },
                    user_last_name: {
                        required: "Please Enter Last Name",
                        noSpace: "No space please and don't leave it empty",
                    },
                    user_mobile: {
                        required: "Please Enter Mobile Number",
                        noSpace: "No space please and don't leave it empty",
                        uniqueMobile: "This Mobile Number is Already Used",
                    },
                    "id_proof[]": {
                        required: "Please Select File",
                    },
                    bank_name: {
                        required : "Please Enter Bank Name ",
                        noSpace: "No space please and don't leave it empty",
                    },
                    bank_branch_name: {
                        required : "Please Enter Bank Branch Name ",
                        noSpace: "No space please and don't leave it empty",
                    },
                    account_no: {
                        required : "Please Enter Account",
                        noSpace: "No space please and don't leave it empty",
                    },
                    account_type: {
                        required : "Please Enter Account",
                    },
                    ifsc_code: {
                        required : "Please Enter IFSC",
                        noSpace: "No space please and don't leave it empty",
                    },
                },
            });
            $form_container.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                labels: {
                    finish: 'Submit'
                },
                onStepChanging: function(event, currentIndex, newIndex) {
                    $(".id_proof_validation").each(function () {
                        $('#'+this.id).rules("add", {
                            required: true,
                            messages: {
                                    required: "Please Select File ",
                                }
                        });
                    });
                    if(newIndex > currentIndex )
                    {
                        $form_container.validate().settings.ignore = ":disabled,:hidden";
                        return $form_container.valid();
                    }else{
                        return true;
                    }
                },
                onFinishing: function(event, currentIndex) {
                    $form_container.validate().settings.ignore = ":disabled";
                    return $form_container.valid();
                },
                onFinished: function(event, currentIndex) {
                    var userMobile= $('#user_mobile').val();
                        $.ajax({
                        url: "controller/uniqueController.php",
                        cache: false,
                        type: "POST",
                        data: {userMobile : userMobile,checkUserMobile:'checkUserMobile',csrf:csrf},
                        success: function(response){
                           if (response==1) {
                                swal("This Mobile Number is Already Used.", {
                                            icon: "error",
                                          });
                                return false;
                           }else{

                                var data = [];
                                var cnt = 0;
                                $(".bankInChange").each(function()
                                {
                                    data[cnt] = $(this).val();
                                    cnt++;
                                });
                                var error = 0;
                                $.each(data, function (key, val)
                                {
                                    if(val != "")
                                    {
                                        error++;
                                    }
                                });
                                if (error == 9 || error == 0)
                                {
                                        employeeOnboarding();
                                }
                                else if(error < 9 && error >= 1)
                                {
                                    Lobibox.notify('error', {
                                        pauseDelayOnHover: true,
                                        continueDelayOnInactiveTab: false,
                                        position: 'top right',
                                        icon: 'fa fa-times-circle',
                                        msg: 'Please fill all bank details or none of them'
                                    });
                                }


                           }
                        }
                    });
                }
            });
            return $form_container;
        }, FormWizard.prototype.init = function() {
            this.createValidatorForm($("#wizard-validation-form"));

        }, $.FormWizard = new FormWizard, $.FormWizard.Constructor = FormWizard
    }(window.jQuery),
    function($) {
        "use strict";
        $.FormWizard.init()
    }(window.jQuery);

    function employeeOnboarding() {

        var formData = new FormData(document.getElementById("wizard-validation-form"));
        swal({
            title: 'Please Wait',
            html: 'Processing....',
            type: '',
            padding: '2em',
            showCancelButton: false,
            showConfirmButton: false,
            allowEscapeKey: false,
            allowOutsideClick: false
        })
        $.ajax({
            url: "controller/userController.php",
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);
                if (response.status == "200") {
                    swal({
                        title: 'Success',
                        text: response.message,
                        type: 'success',
                        padding: '2em',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    })
                    setTimeout(function() {
                        window.location.href = "employeeOnboarding";
                    }, 2000);
                } else {
                    swal({
                        title: 'Error',
                        text: response.message,
                        type: 'error',
                        padding: '2em',
                        showCancelButton: false,
                        showConfirmButton: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    setTimeout(function() {
                        window.location.href = "employeeOnboarding";
                    }, 2000);
                }
            },
            error: function() {
                swal({
                    title: 'Error',
                    html: 'Something Wents Wrong. Please Try Again',
                    type: 'error',
                    padding: '2em',
                    showCancelButton: false,
                    showConfirmButton: true,
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }
        });
    }


function change_marital_status(value) {
  if(value==2){
    $('.hide_wedding_anniversary_date').removeClass('d-none');
  }else
  {
    $('.hide_wedding_anniversary_date').addClass('d-none');
  }
}

function selectIdProofDocument(value){
  var option = $('option:selected', '#id_proof_id').attr('data-id');
  var doc_content = ``;
  for (var i = 0; i < option; i++) {
    var indexCount = '';
    if(option > 1){
      indexCount = 'Page '+ (i + 1);
    }
    doc_content += 
    `
            <label class="col-lg-12 col-form-label form-control-label">ID Proof `+indexCount+` <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12">
            <input name="id_proof[`+i+`]" id="id_proof_`+i+`" accept=".png, .jpg, .jpeg, .doc, .docx, .pdf" class="idProof form-control id_proof_validation" type="file">
            <i>Only Photo & Pdf formats are allowed </i>
            </div>
        `;
  }
  $('#id_proof_pages').val(option);
  $('#id_prrof_document_div').html(doc_content);
}

$(".bankInChange").on("change", function(e)
{
    var data = [];
    var cnt = 0;
    $(".bankInChange").each(function()
    {
        data[cnt] = $(this).val();
        cnt++;
    });

    var error = 0;
    $.each(data, function (key, val)
    {
        if(val != "")
        {
            error++;
        }
    });

    if (error == 0)
    {
        console.log('remove validation');
        if ($(".bankReqSpanRem")[0])
        {
            $('.bankReqSpanRem').remove();
        }
    }
    else
    {
        console.log('add validation');
        if (!$(".bankReqSpanRem")[0])
        {
            $('.bankReqSpan').append('<span class="text-danger bankReqSpanRem">*</span>');
        }
    }
})

</script>