  <?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-md-9">
          <h4 class="page-title">Break Type</h4>
     </div>
     <div class="col-md-3 ">
       <div class="btn-group float-sm-right">
        <a href="addAttendanceType" data-toggle="modal" data-target="#addModal"  onclick="buttonSettingForAttendanceType()" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteAttendanceType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
      </div>
     </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                    $i = 1;
                    $q = $d->select("attendance_type_master", "society_id='$society_id' AND attendance_type_delete=0");

                    $counter = 1;
                    while ($data = mysqli_fetch_array($q)) {

                        ?>
                    <tr>
                       <td class="text-center">
                        <?php $totalAttendance = $d->count_data_direct("attendance_break_history_id", "attendance_break_history_master", "attendance_type_id='$data[attendance_type_id]'");
                                    if ($totalAttendance == 0) {
                                        ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['attendance_type_id']; ?>">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['attendance_type_id']; ?>">
                          <?php }?>
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['attendance_type_name']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['attendance_type_created_date'])); ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="attendanceTypeDataSet(<?php echo $data['attendance_type_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                          <?php if ($data['attendance_type_active_status'] == "0") {
                                    $status = "attendanceTypeStatusDeactive";
                                    $active = "checked";
                                } else {
                                    $status = "attendanceTypeStatusActive";
                                    $active = "";
                                }?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['attendance_type_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>

                    </tr>
                    <?php }?>
                </tbody>

            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Break Type</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">

          <form id="addAttendanceType" action="controller/AttendanceTypeController.php" enctype="multipart/form-data" method="post">
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Break Type <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="text" class="form-control restClassIn " placeholder="Attendance Type Name" id="attendance_type_name" name="attendance_type_name" value="">
              </div>
           </div>
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Is Multiple<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select name="is_multipletime_use" id="is_multipletime_use" class="form-control restClass">
                  <option value="1">Yes</option>
                  <option value="0" >No</option>
                </select>
              </div>
           </div>
           <div class="form-footer text-center">
             <input type="hidden" id="attendance_type_id" name="attendance_type_id" value="" >
             <button id="addAttendanceTypeBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addAttendanceType"  value="addAttendanceType">
             <button id="addAttendanceTypeBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addAttendanceType');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">
function buttonSettingForAttendanceType(){
  $('.hideupdate').hide();
  $('.hideAdd').show();
  $('.restClassIn').val("");
  $('.restClass').val("");
  $('.restClass').select2();
}
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

</script>.
