<?php
	  extract($_POST);?>
<div class="content-wrapper">
  <div class="container-fluid">
  	 <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
          	<?php
          	
          		$getData = $d->select("email_configuration","","");
          		$data = mysqli_fetch_array($getData);?>

          		 <form id="configration" action="controller/emailConfigrationController.php" method="POST" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              	<input type="hidden" name="configuration_id" value="<?= $data['configuration_id']; ?>">
              <i class="fa fa-envelope"></i>
              Sender Email Configration 
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">SENDER EMAIL ID <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input required="" type="email" value="<?= $data['sender_email_id'] ?>" class="form-control" id="input-10" name="sender_email_id" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">PASSWORD <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input type="password" value="<?= $data['email_password'] ?>" class="form-control" id="input-10" name="email_password" required>
                </div>
              </div>
             <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">SMTP </label>
                <div class="col-sm-4">
                  <input type="text" value="<?= $data['email_smtp'] ?>" class="form-control" id="input-10" name="email_smtp" required>
                </div>
                <label for="input-17" class="col-sm-2 col-form-label">SMTP TYPE</label>
                <div class="col-sm-4">
                  <input  type="text" value="<?= $data['smtp_type'] ?>" class="form-control" id="input-10" name="smtp_type" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">EMAIL PORT </label>
                <div class="col-sm-4">
                  <input type="text" value="<?= $data['email_port'] ?>"  class="form-control" id="input-10" name="email_port" required>
                </div>
                <label for="input-mobile" class="col-sm-2 col-form-label">SENDER NAME</label>
                <div class="col-sm-4">
                  <input type="text" value="<?= $data['sender_name'] ?>" class="form-control" id="input-10" name="sender_name" required>
                </div>
              </div>
              <div class="form-footer text-center">
                <input type="hidden" name="updateConfigration" value="updateConfigration">
                <button name="" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
              </div>
            </form>
          		
          
          </div>
           <div class="card-body">
            <?php
            
              $getData = $d->select("sms","","");
              $data = mysqli_fetch_array($getData);?>

               <form id="configration" action="controller/emailConfigrationController.php" method="POST" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
                <input type="hidden" name="sms_id" value="<?= $data['sms_id']; ?>">
              <i class="fa fa-envelope"></i>
              Sender SMS Configration 
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">API <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <textarea required="" class="form-control" id="input-10" name="sms_api" required><?= $data['sms_api'] ?></textarea>
                </div>
              </div>
              
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Active Status </label>
                <div class="col-sm-4">
                  
                      <input <?php if($data['active_flag']==0) { echo 'checked';} ?> type="radio"  value="0" name="active_flag"  class="js-switch" data-color="#15ca20" data-size="small"/> Active
                     <input <?php if($data['active_flag']==1) { echo 'checked';} ?>  type="radio" value="1" name="active_flag" class="js-switch" data-color="#15ca20" data-size="small"/> Deactive
                </div>
               
              </div>
              <div class="form-footer text-center">
                <input type="hidden" name="updateConfigration" value="updateConfigration">
                <button name="" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
              </div>
            </form>
              
          
          </div>
        </div>
      </div>
      </div><!--End Row-->
  </div>
</div>

