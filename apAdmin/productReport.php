<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Product Report</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $q = $d->selectRow("rpm.product_name,rpvm.product_variant_name,umm.unit_measurement_name,rpvm.sku,rpvm.variant_bulk_type,rpvm.variant_per_box_piece,rpvm.retailer_selling_price,rpvm.maximum_retail_price,rpvm.menufacturing_cost,rpvm.variant_photo,pcm.category_name,rpvm.variant_description","retailer_product_master AS rpm JOIN retailer_product_variant_master AS rpvm ON rpvm.product_id = rpm.product_id JOIN unit_measurement_master AS umm ON umm.unit_measurement_id = rpvm.unit_measurement_id JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id");
                            $i = 1;
                            ?>
                            <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Unit</th>
                                        <th>SKU</th>
                                        <th>Packing Type</th>
                                        <th>Per Box Piece</th>
                                        <th>Image</th>
                                        <th>Category Name</th>
                                        <th>RSP</th>
                                        <th>Manufacturing Cost</th>
                                        <th>MRP</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = $q->fetch_assoc())
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['product_name'] . " " . $data['product_variant_name']; ?></td>
                                        <td><?php echo $data['unit_measurement_name']; ?></td>
                                        <td><?php echo $data['sku']; ?></td>
                                        <td><?php echo ($data['variant_bulk_type'] == 0) ? "Piece Wise" : "Box Wise"; ?></td>
                                        <td><?php echo ($data['variant_bulk_type'] == 0) ? "0" : $data['variant_per_box_piece']; ?></td>
                                        <td>
                                        <?php
                                        if($data['variant_photo'] != "" && file_exists("../img/vendor_product/".$data['variant_photo']))
                                        {
                                        ?>
                                            <a href="../img/vendor_product/<?php echo $data['variant_photo']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['product_variant_name']; ?>">
                                                <img src="../img/vendor_product/<?php echo $data['variant_photo']; ?>" width="100" height="100" alt="Product Variant Image"/>
                                            </a>
                                        <?php
                                        }
                                        ?>
                                        </td>
                                        <td><?php echo $data['category_name']; ?></td>
                                        <td><?php echo $data['retailer_selling_price']; ?></td>
                                        <td><?php echo $data['menufacturing_cost']; ?></td>
                                        <td><?php echo $data['maximum_retail_price']; ?></td>
                                        <td><?php echo $data['variant_description']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>