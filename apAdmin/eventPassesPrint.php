<?php
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
$bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
if (isset($_REQUEST['id'])) {
$events_day_id = $_REQUEST['id'];
}
if (isset($_REQUEST['id'])) {
$_SESSION['id'] = $_REQUEST['id'];
$events_day_id = $_REQUEST['id'];
} else {
$events_day_id = $_SESSION['id'];
}
$events_day_id = $events_day_id;
if (isset($_REQUEST['user_id'])) {
$_SESSION['user_id'] = $_REQUEST['user_id'];
$user_id = $_REQUEST['user_id'];
} else {
$user_id = $_SESSION['user_id'];
}
$user_id = (int)$user_id;
$id = (int)$id;
$events_day_id = (int)$events_day_id;
$society_id = (int)$society_id;
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Event Passes Print</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=1024">
     <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
     <?php include 'common/colours.php'; ?>
    <link href="assets/css/app-style9.css" rel="stylesheet"/>

      <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

    <style type="text/css">

    body {
    background-color: #d7d6d3;
    font-family: 'Open Sans', sans-serif;
    }
    .id-card-holder {
    width: 200px;
    padding: 4px;
    /*margin: 0 auto;*/
    margin-left: 2px;
    margin-right: : 2px;
    margin-bottom : 10px;
    background-color: #1f1f1f;
     
    position: relative;
    display: inline-block;
    }
    
    .id-card {
    
    background-color: #ccc;
    padding: 10px;
    
    text-align: center;
    box-shadow: 0 0 1.5px 0px #b9b9b9;
    }
    .id-card img {
    margin: 0 auto;
    }
    .header img {
    width: 70px;
    margin-top: 5px;
    }
    .photo img {
    width: 80px;
    margin-top: 5px;
    }
    h2 {
    font-size: 12px;
    margin: 5px 0;
    }
    h3 {
    font-size: 10px;
    margin: 2.5px 0;
    font-weight: 300;
    }
    .qr-code img {
    width: 200px;
    }
    p {
    font-size: 5px;
    margin: 2px;
    }
    .id-card-hook {
    background-color: #000;
    width: 70px;
    margin: 0 auto;
    height: 15px;
    border-radius: 5px 5px 0 0;
    }
    .id-card-hook:after {
    content: '';
    background-color: #d7d6d3;
    width: 47px;
    height: 6px;
    display: block;
    margin: 0px auto;
    position: relative;
    top: 6px;
    border-radius: 4px;
    }
    .id-card-tag-strip {
    width: 45px;
    height: 40px;
    background-color: #0950ef;
    margin: 0 auto;
    border-radius: 5px;
    position: relative;
    top: 9px;
    z-index: 1;
    border: 1px solid #0041ad;
    }
    .id-card-tag-strip:after {
    content: '';
    display: block;
    width: 100%;
    height: 1px;
    background-color: #c1c1c1;
    position: relative;
    top: 10px;
    }
    .id-card-tag {
    width: 0;
    height: 0;
    border-left: 100px solid transparent;
    border-right: 100px solid transparent;
    border-top: 100px solid #0958db;
    margin: -10px auto -30px auto;
    }
    .id-card-tag:after {
    content: '';
    display: block;
    width: 0;
    height: 0;
    border-left: 50px solid transparent;
    border-right: 50px solid transparent;
    border-top: 100px solid #d7d6d3;
    margin: -10px auto -30px auto;
    position: relative;
    top: -130px;
    left: -50px;
    }
    .addressSoc {
    font-size: 7px;
    font-weight: bold;
    }
    .bg-primary {
    background-color: var(--primary) !important;
}
.card-header {
    padding: 1rem 1rem;
    margin-bottom: 0;
     
     
    font-weight: 600;
    font-size: 14px;
    border-radius: 20px !important; 
     border-top: 0px !important;
     border-bottom: 0px !important;
    
     
}
 
.box {
   background: #fff; /* fallback */
  background:
    linear-gradient(135deg, transparent 10px, #fff 0) top left,
    linear-gradient(225deg, transparent 10px, #fff 0) top right,
    linear-gradient(315deg, transparent 10px, #fff 0) bottom right,
    linear-gradient(45deg,  transparent 10px, #fff 0) bottom left;
  background-size: 50% 50%;
  background-repeat: no-repeat;
}

div.round {
  background-image:
    radial-gradient(circle at 0 0, rgba(204,0,0,0) 14px, #fff 15px),
    radial-gradient(circle at 100% 0, rgba(204,0,0,0) 14px, #fff 15px),
    radial-gradient(circle at 100% 100%, rgba(204,0,0,0) 14px, #fff 15px),
    radial-gradient(circle at 0 100%, rgba(204,0,0,0) 14px, #fff 15px);
}

.box-header {
   background: #fff ; /* fallback */
  background:
    linear-gradient(135deg, transparent 0px, #fff 0) top left,
    linear-gradient(225deg, transparent 0px, #fff 0) top right,
    linear-gradient(315deg, transparent 10px, #fff  0) bottom right,
    linear-gradient(45deg,  transparent 10px, #fff  0) bottom left;
  background-size: 50% 50%;
  background-repeat: no-repeat;
}

div.round-header {
  background-image:
    radial-gradient(circle at 0 0, rgba(204,0,0,0) 0px, var(--primary)  0px),
    radial-gradient(circle at 100% 0, rgba(204,0,0,0) 0px, var(--primary)  0px),
    radial-gradient(circle at 100% 100%, rgba(204,0,0,0) 14px, var(--primary)   15px),
    radial-gradient(circle at 0 100%, rgba(204,0,0,0) 14px, var(--primary)   15px);
}

.box-footer {
   background: #fff; /* fallback */
  background:
    linear-gradient(135deg, transparent 10px, #fff 0) top left,
    linear-gradient(225deg, transparent 10px, #fff 0) top right,
    linear-gradient(315deg, transparent 0px, #fff 0) bottom right,
    linear-gradient(45deg,  transparent 0px, #fff 0) bottom left;
  background-size: 50% 50%;
  background-repeat: no-repeat;
}

div.round-footer {
  background-image:
    radial-gradient(circle at 0 0, rgba(204,0,0,0) 14px, #fff 15px),
    radial-gradient(circle at 100% 0, rgba(204,0,0,0) 14px, #fff 15px),
    radial-gradient(circle at 100% 100%, rgba(204,0,0,0) 0px, #fff 0px),
    radial-gradient(circle at 0 100%, rgba(204,0,0,0) 0px, #fff 0px);
}
.col-lg-4 {
    padding: 5px !important;
    -ms-flex: 0 0 33.333333% !important;
    flex: 0 0 33.333333% !important;
    max-width: 300px !important;

}
.col-lg-6 {
    -ms-flex: 0 0 50% !important;
    flex: 0 0 50% !important;
    max-width: 50% !important;
}
.card{
  /*box-shadow: 3px 5px 20px #59597d !important;*/
     
    background-color: #ccc !important;
    border : 0px !important;
}
.card-footer{
  background-color: #fff !important;
}
.card-footer {

    background-color: #ccc !important;

}
.card-footer {

    padding: .75rem 1.25rem;
    background-color: rgba(0,0,0,.03);
    border-top: 0px solid var(--primary)  !important;
    border-radius: 15px !important;

}
 

hr{
  border: 2px solid   var(--primary)  !important;
  border-radius: 5px;
   
}
.text-primary{
  color: var(--primary)  !important;
  font-weight: 600px;
}
    </style>
   
  </head>
  <body onload="window.print()">

 


    <div class="">
    <div class="container-fluid">
    <?php
    if (isset($events_day_id)) {
    $event_attend_list_qry=$d->select("event_attend_list","event_attend_id='$id' and user_id ='$user_id' AND  book_status=1","");
    $event_attend_list_data = mysqli_fetch_array($event_attend_list_qry);
    $events_day_id = $event_attend_list_data['events_day_id'];

    $unit_id =  $event_attend_list_data['unit_id'];         
    $event_id =  $event_attend_list_data['event_id'];         

    $event_days_master_qry=$d->select("event_days_master","events_day_id='$events_day_id'","");
    $event_days_master_data = mysqli_fetch_array($event_days_master_qry);
    $qcheck=$d->selectRow("user_type,member_status","users_master","user_id='$user_id'");
    $userData=mysqli_fetch_array($qcheck);
    $user_type=$userData['user_type'];
    $q=$d->select("event_passes_master,event_master","event_passes_master.event_id=event_master.event_id AND event_passes_master.event_id='$event_id' AND event_passes_master.unit_id='$unit_id' AND event_passes_master.userType='$user_type' AND event_passes_master.event_attend_id='$id'" );
    if(mysqli_num_rows($q)>0){
    $response["passes"] = array();
    ?>  <div class="row" style="background-color: #CCC !important
    ;"> <?php
      while($data=mysqli_fetch_array($q)) {
      $passes = array();
      
      $qr_size = "300x300";
      $qr_content = "FIN_" . $data['pass_id'];
      $qr_correction = strtoupper('H');
      $qr_encoding = 'UTF-8';
      $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
      $passes["pass_id"]=$data['pass_id'];
      $passes["event_id"]=$data['event_id'];
      $passes["pass_no"]='TKT'.$data['user_id'].$data['pass_id'];
      $passes["entry_status"]=$data['entry_status'];
      $passes["event_title"]=$data['event_title'];
      $passes["event_start_date"]=date("d M Y h:i A", strtotime($data['event_start_date']));

      $pass_type_data ="";
      if ($data['pass_type']==0) {
      if ($data['event_type']==0) {
      $passes["pass_type"] = $pass_type_data ="Employee Pass";
      } else {
      $passes["pass_type"] = $pass_type_data ="Member Adult Pass";
      }
      } elseif ($data['pass_type']==1) {
      $passes["pass_type"] = $pass_type_data  ="Child Pass";
      } else if ($data['pass_type']==2){
      $passes["pass_type"] = $pass_type_data  ="Guest Pass";
      }
      //bg-primary
      ?>
      <div class="col-lg-4">
        <div class="card">
          <div class="card-header  text-white  box-header round-header " title="">
            <div class="row">
            <div class="col-lg-6">
            <span class="text-left">Pass No.<br> <?php echo 'TKT'.$data['user_id'].$data['pass_id'];  ?></span>
          </div>
          <div class="col-lg-6">
            <span class="text-right">Date & Time <br> <?php echo date("d M Y h:i A", strtotime($event_days_master_data['event_date'].' '.$event_days_master_data['event_time'] )); ?></span>
          </div>
        </div>
          </div>
          <div class="card-body qr-code box round"><span class="text-primary text-uppercase">


            <h5 class="text-primary text-uppercase"><center><?php echo $pass_type_data;?> </center></h5><br>
           <center> <img src="<?php echo $qrImageUrl; ?>" alt=""> </center>
          <?php  if($data['entry_status']==1) { ?><h2 class="text-success">Verified</h2> <?php } ?>
          </span>
        </div>
          <div  class="card-footer text-center box-footer round-footer" style="font-size: 10px;">
            <hr>
            <b><?php echo custom_echo($data['event_title']." - ".$event_days_master_data['event_day_name'],30);?></b></div>
        </div>
      </div>
      <?php
      /*$passes['qr_code'] = $qrImageUrl;
      $passes['qr_code_ios'] = $qrImageUrl . '.png';
      array_push($response["passes"], $passes);*/
      }
      ?>
    </div>
    <?php
    
    } else {
       echo "<img src='img/no_data_found.png'>";
    }


  
     } ?>
   </div>
 </div>
  </body>
   <script type="text/javascript">
        Android.print('print');
  </script>
</html>