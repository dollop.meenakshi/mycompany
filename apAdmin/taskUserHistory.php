<?php error_reporting(0);
if(isset($_GET['id'])){
    $id = (int)$_GET['id'];
  }
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Task User History</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="javascript:void(0)" onclick="DeleteAll();" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>                        
                        <th>User Name</th>
                        <th>Assign Date</th>
                        <th>Status Change Date</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("task_user_history,users_master","task_user_history.user_id=users_master.user_id AND task_user_history.society_id='$society_id' AND task_user_history.task_id='$id'","ORDER BY task_user_history.task_user_history_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['task_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['task_id']; ?>">                      
                        </td> -->
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo date("d M Y", strtotime($data['task_assign_date'])); ?></td>
                        <td><?php if($data['status_change_date'] != '0000-00-00 00:00:00' && $data['status_change_date'] != null){ echo date("d M Y h:i A", strtotime($data['status_change_date']));} ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <?php $totalPause = $d->count_data_direct("task_pause_history_id","task_pause_history","task_user_history_id='$data[task_user_history_id]' AND user_id='$data[user_id]'");
                                if($totalPause > 0){ ?>
                                <a href="userTaskPauseHistory?id=<?php echo $data['task_user_history_id']?>" title="Pause History" class="btn btn-sm btn-warning mr-2"> <i class="fa fa-history"></i></a>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
