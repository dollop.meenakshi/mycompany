
<?php

session_start();

include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$assets_category_id = $_POST['assets_category_id'];
$q=$d->select("assets_category_master","assets_category_id='$assets_category_id'");
$row = mysqli_fetch_array($q);
//print_r($row);
?>
<form id="editAssets" enctype="multipart/form-data" method="POST" class="form-horizontal" action="controller/assetsCategoryController.php">
    <div class="modal-body">
        
        <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Assets Category<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="hidden" name="csrf" id="assets_category" value="<?php echo $_SESSION['token'] ?>">
                <input type="hidden" name="assets_category" id="assets_category">
                <input type="text" class="form-control" name="assets_category" id="assets_category"  value="<?php echo $row['assets_category'] ?>" >
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="col-md-12 text-center">
            <input type="hidden" name="assets_category_id" id="assets_category_id_update" value="<?php echo $row['assets_category_id'] ?>" />
            <input type="hidden" name="update_assets" value="<?php $row['assets_category_id'] ?>" />
            <button type="submit" class="btn btn-success" name="update_assets">Update</button>
        </div>  
    </div>
</form>
<script type="text/javascript">

    $("#editAssets").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
error.insertAfter(element.parent());      // radio/checkbox?
} else if (element.hasClass('select2-hidden-accessible')) {     
error.insertAfter(element.next('span'));  // select2
element.next('span').addClass('error').removeClass('valid');
} else {                                      
error.insertAfter(element);               // default
}
},
rules: {

    assets_category: {
        required: true,
        noSpace:true
    },

},

messages: {
    assets_category:{
        required: "Please Enter Assets Category Name"
    },
},
});

</script>
