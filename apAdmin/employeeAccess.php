<?php error_reporting(0);

$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6 col-6">
                <h4 class="page-title"> Employee Access</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
            <div class="btn-group float-sm-right">
                <a href="addEmployeeAccess" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                <a href="javascript:void(0)" onclick="DeleteAll('deleteEmployeeAppAccess');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            </div>
            </div>
        </div>   
        <form class="branchDeptFilter" action="" method="get">
            <div class="row pt-2 pb-2">
              <?php include 'selectBranchDeptEmpForFilter.php'; ?>
              <div class="col-md-3 form-group">
                <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
              </div>
            </div>
        </form>
    

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php if((isset($bId) && $bId >0) && (isset($dId) && $dId >0) )
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Employee</th>
                                        <th>Designation</th>
                                        <th>Access Type</th>
                                        <th>Access For</th>
                                        <th>Action</th>
                                    
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                        if(isset($uId) && $uId > 0) {
                                            $userFilterQuery = " AND users_master.user_id=$uId";
                                        }
                                        if(isset($dId) && $dId > 0) {
                                            $dIdFilterQuery = " AND users_master.floor_id=$dId";
                                        }
                                    $q = $d->select("app_access_master,users_master,access_type_master","app_access_master.access_by_id=users_master.user_id AND access_type_master.access_type_id=app_access_master.access_type AND app_access_master.society_id='$society_id' AND users_master.delete_status= 0 $dIdFilterQuery $userFilterQuery $blockAppendQueryUser");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="id" id="id"  value="<?php echo $data['app_access_id']; ?>">                  
                                                <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['app_access_id']; ?>"> 
                                            </td>
                                            <td><?php echo $counter++; ?></td>
                                            <td><?php echo $data['user_full_name']; ?></td>
                                            <td><?php echo $data['user_designation']; ?></td>
                                            <td><?php echo $data['access_type']; ?></td>
                                            <td><?php 
                                                if($data['access_for'] == 0){
                                                    echo "All Company";
                                                }
                                                else if($data['access_for'] == 1){
                                                    echo "Branch Wise";
                                                }
                                                else if($data['access_for'] == 2){
                                                    echo "Department Wise";
                                                }
                                                else if($data['access_for'] == 3){
                                                    echo "Employee Wise";
                                                }
                                             ?></td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                <form action="addEmployeeAccess" method="post">
                                                    <input type="hidden" name="app_access_id" value="<?php echo $data['app_access_id'];?>">
                                                    <input type="hidden" name="edit_app_access" value="edit_app_access">
                                                    <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                                </div>
                                            </td>
                                        

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            
                            </table>
                            <?php }else
                            { ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Department</span>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->