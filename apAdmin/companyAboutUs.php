<?php error_reporting(0);
  ?>
  <?php if(isset($_GET) && $_GET['id']>0){ ?>
    <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company About Us </h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <a href="companyAboutUs"  class="btn btn-sm btn-primary waves-effect waves-light"> Back </a>
      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <?php 
                $q1 = $d->selectRow('*',"company_about_us_master","company_about_us_id='$_GET[id]'");
                $resultData = mysqli_fetch_assoc($q1); 
               ?>
              <div class="row">
                <div class="col-md-12 form-group">
                  <div class="text-center">
                    <a href="../img/company_about_us/<?php echo $resultData['company_about_us_top_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $resultData["company_about_us_top_image"]; ?>"><img width="50%" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $resultData['company_about_us_top_image']; ?>"  href="#divForm<?php echo $resultData['company_about_us_id'];?>" class="btnForm lazyload" ></a>
                  </div>
                
                </div>
                <div class="col-md-12 form-group">
                  <p><?php echo $resultData['company_about_us_top_description']; ?></p>
                </div>
                <div class="col-md-6 form-group">
                  <div class="text-center">
                    <a href="../img/company_about_us/<?php echo $resultData['company_about_us_image_one']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $resultData["company_about_us_image_one"]; ?>"><img width="50%" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $resultData['company_about_us_image_one']; ?>"  href="#divForm<?php echo $resultData['company_about_us_id'];?>" class="btnForm lazyload" ></a>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="text-center">
                    <a href="../img/company_about_us/<?php echo $resultData['company_about_us_image_two']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $resultData["company_about_us_image_two"]; ?>"><img width="50%" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $resultData['company_about_us_image_two']; ?>"  href="#divForm<?php echo $resultData['company_about_us_id'];?>" class="btnForm lazyload" ></a>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <h6><?php echo $resultData['company_about_us_bottom_title']; ?></h6>
                </div>
                <div class="col-md-12 form-group">
                  <p><?php echo $resultData['company_about_us_bottom_description']; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <?php }else{ ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company About Us</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
          <?php $total = $d->count_data_direct("company_about_us_id","company_about_us_master","society_id='$society_id'");
            if( $total==0) {
          ?>        
          <a href="addCompanyAboutUs"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        <?php } ?>
        <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyAboutUs');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>    
                        <th>Top Image</th>                      
                        <th>Image 1</th>                      
                        <th>Image 2</th>                                               
                        <th>Bottom Title</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      $q=$d->select("company_about_us_master, bms_admin_master","company_about_us_master.society_id='$society_id' AND company_about_us_master.company_about_us_added_by = bms_admin_master.admin_id");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                         
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['company_about_us_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_about_us_id']; ?>">                      
                          
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><a href="../img/company_about_us/<?php echo $data['company_about_us_top_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["company_about_us_top_image"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $data['company_about_us_top_image']; ?>"  href="#divForm<?php echo $data['company_about_us_id'];?>" class="btnForm lazyload" ></a></td>
                       <td><a href="../img/company_about_us/<?php echo $data['company_about_us_image_one']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["company_about_us_image_one"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $data['company_about_us_image_one']; ?>"  href="#divForm<?php echo $data['company_about_us_id'];?>" class="btnForm lazyload" ></a></td>
                       <td><a href="../img/company_about_us/<?php echo $data['company_about_us_image_two']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["company_about_us_image_two"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_about_us/<?php echo $data['company_about_us_image_two']; ?>"  href="#divForm<?php echo $data['company_about_us_id'];?>" class="btnForm lazyload" ></a></td>
                       <td><?php echo $data['company_about_us_bottom_title']; ?></td>
                       <td>
                         <div class="d-flex align-items-center">
                            <form action="addCompanyAboutUs" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="company_about_us_id" value="<?php echo $data['company_about_us_id']; ?>">
                              <input type="hidden" name="edit_company_about_us" value="edit_company_about_us">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                            </form>
                            <a href="companyAboutUs?id=<?php echo $data['company_about_us_id']; ?>" class="btn btn-sm btn-primary mr-2" onclick="productVendorDetailModal(<?php echo $data['company_about_us_id ']; ?>)"> <i class="fa fa-eye"></i></a>
                          </div>
                        </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <?php } ?>



<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
