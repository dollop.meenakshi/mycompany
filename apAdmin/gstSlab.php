<?php 
  extract($_POST);
  error_reporting(0);

  $q=$d->select("society_master","society_id='$society_id'");
  $bData=mysqli_fetch_array($q);
  extract($bData);

  ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title"><?php echo $xml->string->tax_slab; ?></h4>
        
      </div>
     
      <div class="col-sm-3 col-4 text-right">
        <div class="btn-group">
          <a  data-toggle="modal" data-target="#addGSTModal" class="btn btn-primary text-white float-right btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> </a>  

          <a href="javascript:void(0)" onclick="DeleteAll('deleteSlab');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> <?php echo $xml->string->delete; ?> </a>


           
        </div>
      </div>
    </div>
    <div class="row pt-2 pb-2">
       
    </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="buildingDetails" action="controller/gstController.php" method="post" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-building"></i>
              <?php echo $xml->string->tax_number; ?> Details
              </h4>
              
             <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_number; ?> </label>
                <div class="col-sm-4">
                  <input minlength="5" maxlength="30" value="<?php echo $gst_no; ?>" type="text" class="form-control text-uppercase alphanumeric" id="input-10" name="gst_no">
                </div>
                  <label for="plan_expire_date" class="col-sm-2 col-form-label"><?php echo $xml->string->pan_number; ?></label>
                  <div class="col-sm-4">
                    <input type="text" id="pan_number"  maxlength="30" value="<?php  echo $pan_number; ?>" class="form-control text-uppercase" name="pan_number">
                  </div>
                
              </div>
               
              
              <div class="form-footer text-center">
                <input type="hidden" name="updateGSTNumber" value="updateGSTNumber">
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->


    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?php echo $xml->string->tax_slab; ?></th>
                    <th><?php echo $xml->string->description_add_note; ?></th>
                    <th><?php echo $xml->string->action; ?></th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                $q=$d->select("gst_master","","ORDER BY slab_percentage ASC");
                        
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                        
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['slab_id']; ?>">
                        
                        </td>
                        <td>
                           <?php echo $data['slab_percentage']." %";?>  
                        </td>
                        <td><?php  $content = $data['description'];
                   
                  
                    $description_new = (strlen($data['description']) > 20) ? substr($data['description'],0,20) : $data['description'];
                    echo   $description_new;
                    if(strlen($content) > 20){ 
                      echo "...";
                     ?>
                     &nbsp;
                     <button data-toggle="modal" data-target="#ContentModal"
                   class="btn btn-primary btn-sm" onclick="contemtModal('<?php echo $content; ?>');" ><?php echo $xml->string->view; ?> <?php echo $xml->string->description_add_note; ?></button>
                   <?php 
                  }  
                   ?>
                            
                        </td>
                        <td>
                          <?php
                                if($data['status']=="0"){
                                ?>
                                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['slab_id']; ?>','GSTDeactive');" data-size="small"/>
                                  <?php } else { ?>
                                 <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['slab_id']; ?>','GSTActive');" data-size="small"/>
                                <?php } ?>

                            <a class="btn btn-sm btn-warning" title="Edit"  onclick="getslabEdit('<?php echo $data['slab_id']; ?>');" data-toggle="modal"  data-target="#editGSTModal" href="javascript:void();" > <i class="fa fa-pencil"></i></a>



                        </td>
                         
                    </tr>
                  <?php }  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 

<div class="modal fade" id="addGSTModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->tax_slab; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="slabFrm" method="POST" action="controller/gstController.php" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="slab_percentage" class="col-sm-3 col-form-label"><?php echo $xml->string->tax_slab; ?> %<span class="required">*</span></label>
              <div class="col-sm-9">
                <input type="text" maxlength="5" class="form-control onlyNumber" inputmode="numeric" required="" name="slab_percentage" id="slab_percentage">
                
              </div>
            </div>
            
           
            <div class="row form-group">
              <label for="description" class="col-sm-3 form-control-label"><?php echo $xml->string->description_add_note; ?></label>
              <div class="col-sm-9">
                <textarea type="text" maxlength="100" class="form-control" name="description"></textarea>
              </div>
            </div>
            <div class="form-footer text-center">
              <input type="hidden" name="addGST">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> <?php echo $xml->string->save; ?></button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>
 
 <div class="modal fade" id="editGSTModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->tax_slab->description_add_note; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="slabFrm" method="POST" action="controller/gstController.php" enctype="multipart/form-data">
            <div id="editGSTModalDiv"></div>


            <div class="form-footer text-center">
              <input type="hidden" name="editGST">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> <?php echo $xml->string->update; ?></button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<div class="modal fade" id="ContentModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->description_add_note; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
             <div class="col-sm-12" id="content">  </div>
          </div>
      </div>
      
    </div>
  </div>
</div><!--End Modal -->
<script type="text/javascript">
  function contemtModal(content){
     $('#content').html(content); 
  }
</script>