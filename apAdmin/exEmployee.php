<?php //error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-3">
            <h4 class="page-title">Ex Employee</h4>
            </div>
        </div>
    
    <div class="row blockList">
        <?php
        $blockArray = array();
        $i=1;
        $q = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly", "ORDER BY block_sort ASC");
        while ($data = mysqli_fetch_array($q)) {
            $j = $i++;
            extract($data);
            array_push($blockArray,$data);
        ?>
          <div class="col-lg-2 col-6">
          <a href="exEmployee?bId=<?php echo $block_id;if (isset($_GET['type'])) { ?>&type=<?php echo $_GET['type'];} ?>">
            <?php
            if (isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true) {
            ?>
              <div class="card <?php if ($block_id == $_GET['bId']) {echo 'bg-primary';
            $bId = $block_id;} else {echo 'bg-google-plus';}?> ">
                <div class="card-body text-center text-white">
                  <h5 class="mt-2 text-white"><?php echo $block_name; ?></h5>
                </div>
              </div>
            <?php
            } else {
            ?>
              <div class="card <?php if ($j == 1) { echo 'bg-primary';} else { echo 'bg-google-plus';}?> ">
                <div class="card-body text-center text-white">
                  <h5 class="mt-2 text-white"><?php echo $block_name; ?></h5>
                </div>
              </div>
            <?php
            }
            ?>
          </a>
        </div>
        <?php
        }
        ?>
    </div>

    <!--  -->
    
        <div class="row">
            <?php if(isset($_GET['bId']) && $_GET['bId'] !="") { 
            $block_id = $_GET['bId'];
            }
            else
            {
                $block_id = $blockArray[0]['block_id'];

            }
            $q = $d->select("floors_master", "block_id='$block_id' $blockAppendQueryFloor");

            while ($data = mysqli_fetch_array($q)) {
            // print_R($data);die;
            ?>
            <div class="col-12 col-lg-12">
                <div class="card border border-info floorList">
                    <div class="card-body">
                        
                        <div class="floor text-left p-l row">
                            <div class="col pl-3">
                            <h6 class="mb-0"><?php echo $data['floor_name']; ?></h6>
                            </div>
                        </div>
                    <!-- -----------data -->
                        <div class="media align-items-center pt-2">
                            <div class="media-body text-left">
                                <div class="row m-0">
                                <?php
                                //print_r($data['floor_id']);
                                $q2 = $d->select("users_master", "floor_id=$data[floor_id] AND delete_status=1");
                                if(mysqli_num_rows($q2)>0){
                                while ($data2 = mysqli_fetch_array($q2)) {

                                ?>
                                
                                    <div class="col-6 col-md-6 col-12 col-lg-3 col-xl-3 pt-3">
                                        <div class="card gradient-scooter  no-bottom-margin">
                                            <div class="card-body text-center">
                                                <a href="employeeDetails?id=<?php echo $data2['user_id'];?>">
                                                    <div class="row">
                                                        
                                                        <div class="col-3">
                                                            <img id="blah" onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $data2['user_profile_pic']; ?>" width="60" height="60" alt="your image"
                                                                class="rounded-circle p-1">
                                                        </div>
                                                        <div class="col-9">
                                                            <h6 class="text-white mt-2 text-capitalize"><b></b> </h6>
                                                            <i class="text-white" style="font-size: 11px;font-style: normal;">
                                                            <?php echo $data2['user_full_name']; ?></br>
                                                            (<?php echo $data2['user_designation']; ?>)
                                                            <i class="fa fa-building"></i>
                                                             
                                                            <div class="d-flex align-items-center">
                                                                <?php if ($data2['delete_status'] == "0") {
                                                                    $status = "checked";
                                                                ?>
                                                                <?php } else { 
                                                                    $status = "";
                                                                } ?>
                                                                <!-- <input type="checkbox" <?php echo $status; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data2['user_id']; ?>','UserDeleteActive');" data-size="small" /> -->
                                                            </div>
                                                        </i>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                    <?php }
                                    }
                                    else
                                    { ?>
                                            <p>No Data</p>
                                <?php }
                                    
                                    ?>
                                </div>
                            </div>
                        </div>
                    <!-- -----------data -->
                    </div>
                </div>
            </div>
            <?php  } ?>
        </div>
    
    
    <?php  ?>
    </div>
    <!--  -->

</div>
