<?php
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
 if(isset($_GET['year'])){
	$year = (int)$_REQUEST['year'];
} else {
	$year =  date('Y');
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-3 col-md-6 col-6">
				<h4 class="page-title">Leave Pay Out</h4>
			</div>
			<div class="col-sm-3 col-md-6 col-6">
			</div>
		</div>
    <!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form class="branchDeptFilterWithUser" action="" enctype="multipart/form-data" method="get">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Branch <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required <?php if($bId>0) { echo 'disabled';} ?>>
												<option value="">-- Select Branch --</option> 
												<?php 
												$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
												while ($blockData=mysqli_fetch_array($qb)) {
												?>
												<option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
												<?php } ?>

											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Department <span class="required">*</span></label>
										<div class="col-md-12" id="">
											<select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required <?php if($dId>0) { echo 'disabled';} ?>>
												<option value="">-- Select Department --</option> 
												<?php 
													$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
													while ($depaData=mysqli_fetch_array($qd)) {
												?>
												<option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
												<?php } ?>
												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-lg-12 col-md-12 col-form-label ">Employee <span class="required">*</span></label>
										<div class="col-lg-12 col-md-12" id="">
											<select name="uId" onchange="this.form.submit()" id="uId" class="form-control single-select">
												<option value="">-- Select Employee --</option> 
												<?php 
													$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id = '$dId' $blockAppendQueryUser");  
													while ($userdata=mysqli_fetch_array($user)) {
												?>
												<option <?php if($uId==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group" >
										<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year  <span class="required">*</span></label>
										<div class="col-lg-12 col-md-12" id="">
											<select name="year" class="form-control" onchange="this.form.submit()">
												<option <?php if($year==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
												<option <?php if($year==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
												<option <?php if($year==$currentYear) { echo 'selected';}?> <?php if($year=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
												<option <?php if($year==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
											</select>
										</div>
									</div>
								</div>
								
							</div>
							 
							
						</form>
						<form action="controller/leaveController.php" method="post">
							<?php
							$q=$d->select("leave_assign_master,leave_type_master,users_master","users_master.user_id=leave_assign_master.user_id AND leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND  leave_assign_master.society_id='$society_id' AND users_master.delete_status=0 AND leave_assign_master.user_id='$uId' AND leave_assign_master.assign_leave_year='$year' AND leave_assign_master.pay_out=0 $blockAppendQueryUser","ORDER BY leave_assign_master.user_leave_id ASC");
							if(mysqli_num_rows($q)>0){
								$sq=$d->select("salary_slip_master","user_id='$uId'","ORDER BY salary_slip_id DESC LIMIT 1");
								$salaryData = mysqli_fetch_array($sq);?>
								<input type="hidden" id="per_day_salary" value="<?php echo $salaryData['per_day_salary']; ?>">
							<?php $i = 0;
							while($data = mysqli_fetch_array($q)){
								$approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
								$approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
								$approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
								$approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
								$noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave']/2;
								$payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_payout_year = '$year'");
								$payOutLeaveData = mysqli_fetch_assoc($payOutLeave);
								$remaining_leave = $data['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'];
								if($remaining_leave >0){
								?>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Leave Type <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<input type="text" class="form-control" disabled value="<?php echo $data['leave_type_name']; ?>">
											<input type="hidden" name="leave_type_id[]" value="<?php echo $data['leave_type_id']; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Remaining Leave <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<input type="text" class="form-control" disabled value="<?php echo $remaining_leave; ?>">
											<input type="hidden" name="remaining_leave[<?php echo $i; ?>]" id="remaining_leave<?php echo $i; ?>" value="<?php echo $remaining_leave; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">No Of Pay Out Leave <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<select class="form-control single-select no_of_payout_leaves" name="no_of_payout_leaves[<?php echo $i; ?>]" id="no_of_payout_leaves<?php echo $i; ?>" data-id="<?php echo $i; ?>" onchange="checkvalidation(<?php echo $i; ?>)">
												<option value="">--Select Leave--</option>
												<?php $x= 1;
													for($a=($x-0.5); $a<=$remaining_leave; $a+=0.5){ ?>
														<option value="<?php echo $a; ?>"><?php echo $a; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Pay Out Amount <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<input type="text" class="form-control onlyNumber" id="leave_payout_amount<?php echo $i; ?>" name="leave_payout_amount[]" value="">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Pay Out Remark</label>
										<div class=" col-md-12" id="">
											<textarea type="text" class="form-control" name="leave_payout_remark[]"></textarea>
										</div>
									</div>
								</div>
							</div>
							<?php } $i++; } ?>
							<div class="form-footer text-center">
								<input type="hidden" name="block_id" value="<?php echo $bId; ?>">
								<input type="hidden" name="floor_id" value="<?php echo $dId; ?>">
								<input type="hidden" name="user_id" value="<?php echo $uId; ?>">
								<input type="hidden" name="leave_payout_year" value="<?php echo $year; ?>">
								<input type="hidden" name="addPayOutLeave" value="addPayOutLeave">
                                <button type="submit " disabled class="btn btn-success addPayOutLeaveBtn"><i class="fa fa-check-square-o"></i> Add </button>
                            </div>
							<?php } ?>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/validate7.js"></script>
<script src="assets/plugins/select2/js/select2.min.js"></script>

<script type="text/javascript">

function checkvalidation(id){
	no_of_payout_leaves = $('#no_of_payout_leaves'+id).val();
	
	$('#leave_payout_amount'+id).val('');
	if(no_of_payout_leaves !=''){
		//$('.addPayOutLeaveBtn').prop("disabled", false);
		no_of_payout_leaves = parseFloat(no_of_payout_leaves);
		per_day_salary = parseFloat($('#per_day_salary').val());
		if(no_of_payout_leaves !='' && per_day_salary >0){
			var leave_payout_amount = per_day_salary*no_of_payout_leaves;
			$('#leave_payout_amount'+id).val(leave_payout_amount);
		}
	}
	blArray =[];
	$("[name^=no_of_payout_leaves]").each(function () {
		if(this.value !=""){
			blArray.push(this.value);
		}
	});

	if (blArray.length > 0) {
		$('.addPayOutLeaveBtn').prop("disabled", false);
	}else{
		$('.addPayOutLeaveBtn').prop("disabled", true);
	}
}

</script>