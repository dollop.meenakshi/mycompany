<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Services</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyService');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Image</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("company_service_master","society_id='$society_id'");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                       <?php  $totalServiceDetail = $d->count_data_direct("company_service_id","company_service_details_master","company_service_id='$data[company_service_id]'");
                              if( $totalServiceDetail==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['company_service_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_service_id']; ?>">                         
                        <?php } ?>
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_service_name']; ?></td>
                       <td><a href="../img/company_services/<?php echo $data['company_service_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["company_service_image"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/company_services/<?php echo $data['company_service_image']; ?>"  href="#divForm<?php echo $data['company_service_id'];?>" class="btnForm lazyload" ></a></td>
                       <td><button type="submit" class="btn btn-sm btn-primary mr-2" onclick="companyServiceDataSet(<?php echo $data['company_service_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Service</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addCompanyServiceForm" action="controller/CompanyServiceController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <input type="text" class="form-control" placeholder="Service Name" id="company_service_name" name="company_service_name" value="">
              </div>  
           </div>   
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Image <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <input type="file" accept="image/png, image/jpeg" name="company_service_image" class="form-control-file border photoOnly">
                <input type="hidden"  accept="image/png, image/jpeg" id="company_service_image_old" class="form-control-file border ">
              </div>  
           </div>                 
           <div class="form-footer text-center">
             <input type="hidden" id="company_service_id" name="company_service_id" value="" >
             <button id="addCompanyServiceBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addCompanyService"  value="addCompanyService">
             <button id="addCompanyServiceBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyServiceForm');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
