<?php
error_reporting(0);
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title"><?php echo $xml->string->employees; ?> Report</h4>
        </div>
     </div>
    <form action="" method="get">
     <div class="row pt-2 pb-2">
          <?php error_reporting(0); ?>
          <div class="col-lg-4 col-6">
            <label  class="form-control-label"> <?php echo $xml->string->employee; ?> Type</label>
            <select class="form-control select-sm single-select" name="empType" id="empType" onchange="this.form.submit()">
              <option value="All"> All </option>
              <?php
              if ($_GET['emp_type']=='0') {
                $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0","");
              } else if ($_GET['emp_type']=='1') {
                $q=$d->select("emp_type_master","society_id='$society_id' ","");
              } else {
                $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0","");
              }
              while ($row=mysqli_fetch_array($q)) {
              ?>
              <option <?php if($_REQUEST['empType']==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
              <?php }?>
            </select>
          </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <?php
                  extract(array_map("test_input" , $_GET));
                   $queryAry=array();
                    if ($_GET['emp_type']==0 && $_GET['emp_type']!='' && $_GET['emp_type']!='All') {
                      $append_query="emp_type=0";
                      array_push($queryAry, $append_query);
                    }
                    if ($_GET['emp_type']==1) {
                      $append_query1 ="emp_type=1";
                      array_push($queryAry, $append_query1);
                    }
                    if ($_GET['empType']=="1") {
                      $append_query1 ="emp_type_id=0";
                      array_push($queryAry, $append_query1);
                    } else if($_GET['empType']!="All"){
                      $append_query1 ="emp_type_id='$_GET[empType]'";
                      array_push($queryAry, $append_query1);
                    }
                    // echo $_GET['emp_type'];
                    // if ($_GET['empType']!="1" && $_GET['empType']!='' && $_GET['empType']!='All') {
                    //   echo "string";
                    //   $append_query1 ="emp_type_id='$_GET[empType]'";
                    //   array_push($queryAry, $append_query1);
                    // }
                   $appendQuery= implode(" AND ", $queryAry);
                   if ($appendQuery!='') {
                   $appendQuery= " AND ".$appendQuery;
                   }
                    $q=$d->select("employee_master","society_id='$society_id'  $appendQuery ");
                  $i=1;
               ?>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->employee; ?></th>
                        <th>Type</th>
                        <th>Mobile</th>
                        <th>Joining Date</th>
                        <th>Salary</th>
                        <th>Last Visit Date</th>
                        <th>Days ago</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  while ($data=mysqli_fetch_array($q)) {
                      $EW=$d->select("emp_type_master","emp_type_id='$data[emp_type_id]'");
                      $empTypeData=mysqli_fetch_array($EW);
                   ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo custom_echo($data['emp_name'],15); ?></td>
                        <td><?php if ($empTypeData['emp_type_name']!='') {
                          echo custom_echo($empTypeData['emp_type_name'],10);
                          } else {
                          echo "Security Guard";
                          } ?></td>
                        <td><?php echo $data['country_code'] . " " . substr($data['emp_mobile'], 0, 2) . '*****' . substr($data['emp_mobile'], -3); ?></td>
                        <td><?php echo $data['emp_date_of_joing']; ?></td>
                        <td><?php if($data['emp_sallary']!=0) { echo $data['emp_sallary']; } else { echo "Not Available";} ?></td>
                        <td>
                          <?php     
                          if ($data['in_out_time']!='') {
                            echo $data['in_out_time'];
                          } else {
                              echo "Not in yet";
                          }
                          ?>
                        </td>
                         <td>
                          <?php
                          if ($data['in_out_time']!='') {
                            $today = date("Y-m-d");
                            $last_time = date("Y-m-d", strtotime($data['in_out_time']));

                            $earlier = new DateTime($today);
                            $later = new DateTime($last_time);
                            $diff = $later->diff($earlier)->format("%a");
                            if ($diff==0) {
                               echo  'Today';
                            } else {
                               echo  $diff;
                            }
                          }
                          ?>
                        </td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->