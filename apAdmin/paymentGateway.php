  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Payment Gateway</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Payment Gateway</li>
         </ol>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      
                      <th>#</th>
                      <th>Logo</th>
                      <th>Company</th>
                      <th>Email</th>
                      <th>Mobile</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                   $i=1;
                   $q=$d->select("payment_getway_master","");
                   while ($data=mysqli_fetch_array($q)) {
                    ?>
                    <tr>
                    
                      <td><?php echo $i++; ?></td>
                      <td><img width="200" src="img/PayUmoney_Logo.jpg"></td>
                      <td><?php echo $data['payment_getway_name']; ?></td>
                      <td><?php echo $data['payment_getway_email']; ?></td>
                      <td><?php echo $data['payment_getway_contact']; ?></td>
                        
                    </tr>
                    <?php } ?>
                   
                    
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



