<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Task Priority</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteTaskPriority');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("task_priority_master","society_id='$society_id' ");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                        	<?php $totalTask = $d->count_data_direct("task_id","task_master","task_priority_id='$data[task_priority_id]'");
                              if( $totalTask==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['task_priority_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['task_priority_id']; ?>">                      
                          <?php } ?>    
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['task_priority_name']; ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="taskPriorityDataSet(<?php echo $data['task_priority_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['task_priority_status']=="0"){
                              $status = "taskPriorityStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "taskPriorityStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['task_priority_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Task Priority</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form class="taskPriorityFrom" id="addTaskPriorityFrom" action="controller/TaskController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Priority Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <input type="text" class="form-control" placeholder="Task Priority Name" name="task_priority_name">
              </div>  
           </div>                    
           <div class="form-footer text-center">
            <input type="hidden" name="addTaskPriority"  value="addTaskPriority">
             <button id="addTaskPriorityBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addTaskPriorityFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="editModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Task Priority</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form class="taskPriorityFrom" id="editTaskPriorityFrom" action="controller/TaskController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Priority Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <input type="text" class="form-control" placeholder="Task Priority Name" id="task_priority_name" name="task_priority_name" value="">
              </div>  
           </div>                    
           <div class="form-footer text-center">
             <input type="hidden" id="task_priority_id" name="task_priority_id" value="" >
            <input type="hidden" name="editTaskPriority"  value="editTaskPriority">
             <button id="editTaskPriorityBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
             <button type="reset" class="btn btn-danger cancel" onclick="resetFrm('editTaskPriorityFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
