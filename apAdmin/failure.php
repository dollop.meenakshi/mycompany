<?php 
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="LkSmIDpkYr";

if(!isset($txnid)) {
  // header("location:buyPlan");
  echo ("<script LANGUAGE='JavaScript'>window.location.href='buyPlan';</script>");
}
$q=$d->select("transection_master","payment_txnid='$txnid'");
$data = mysqli_fetch_array($q);
extract($data);
// print_r($_REQUEST);
extract($_REQUEST);
// Salt should be same Post Request 

If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
  } else {
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
         }
		 $hash = hash("sha512", $retHashSeq);
      $invoice_no= "0".date('ymdi').$udf1;

  $a1= array (
    'user_mobile'=> $udf3,
    'payment_mode'=> $mode,
    'transection_amount'=> $amount,
    'transection_date'=> $addedon,
    'payment_status'=> $status,
    'payuMoneyId'=> $payuMoneyId,
    'payment_txnid'=> $txnid,
    'payment_firstname'=> $firstname,
    'payment_phone'=> $udf3,
    'payment_email'=> $udf4,
    'bank_ref_num'=> $bank_ref_num,
    'bankcode'=> $bankcode,
    'name_on_card'=> $name_on_card,
    'cardnum'=> $cardnum,
    'error_Message'=> $error_Message,
    'invoice_no'=> $invoice_no,
     );

  	 $traDate= DateTime::createFromFormat("Y-m-d H:i:s", $addedon)->format("Y-m-d");

   	$qq=$d->selectRow("payment_txnid","transection_master","payment_txnid='$txnid'");
	  $oT=mysqli_fetch_array($qq);
	  if($oT>0) {
	     $d->update("transection_master",$a1,"payment_txnid='$txnid'");
	  } else {
	    $d->insert("transection_master",$a1);
	  }
		  $q=$d->select("package_master","package_id='$udf2'","");
		  $row=mysqli_fetch_array($q);

 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Payment Failed</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Payment Failed</li>
        </ol>
        <div class="alert alert-danger alert-dismissible" role="alert">
    			<button type="button" class="close" data-dismiss="alert">×</button>
    			<div class="alert-icon">
    			 <i class="fa fa-check-circle"></i>
    			</div>
    			<div class="alert-message">
    			  <span><strong>Warning!</strong> Invalid Transaction. Please try again</span>
    			</div>
    		</div>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card" id="printableArea">
          <div class="card-body">
            <!-- Content Header (Page header) -->
              <?php 
                 echo "<h3>Your order status is ". $status .".</h3>";
                echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
               ?>
                    <a href="buyPlan" class="btn btn-primary m-1"><i class="fa fa-envelope"></i> Try Again</a>
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->
