<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Building Polling</h4>
        <ol class="breadcrumb">
             <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Polling</li>
         </ol>
     </div>
    
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="controller/pullingController.php" method="post">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bullhorn"></i>
                   Building Polling
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Polling Question</label>
                  <div class="col-sm-10">
                    <input required="" type="text" class="form-control" id="input-10" name="pulling_question">
                  </div>
                  
                  <div class="col-sm-4">
                   
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Polling Description</label>
                  <div class="col-sm-10">
                    <input required="" type="text" class="form-control" id="input-12" name="pulling_description">
                  </div>
                  
                </div>
               

                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Voting Start Date</label>
                  <div class="col-sm-10">
                    <div id="dateragne-picker">
                      <div class="input-daterange input-group">
                      <input readonly="" type="text" class="form-control" required="" name="pulling_start_date"/>
                      <div class="input-group-prepend">
                       <span class="input-group-text">to</span>
                      </div>
                      <input readonly="" type="text" class="form-control" required="" name="voting_end_date"/>
                     </div>
                    </div>
                  </div>
                  
                 
                </div><div class="form-group row">
                 
                  <label for="input-12" class="col-sm-2 col-form-label">No Of Options </label>
                    <div class="col-sm-4">
                    <input required="" autocomplete="off" maxlength="2" onkeyup ="getOptionList();" type="text" id="no_of_option" class="form-control no_of_option" id="input-10" name="no_of_option">
                  </div>
                </div>
                
                <div id="OptionResp" class="form-group row">

                </div>
                
                 <div class="form-group row">
                 
                </div> 
                <div class="form-footer text-center">
                  <input type="hidden" name="addPullingQue" value="addPullingQue">
                    <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> SAVE</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">

       function getOptionList() {
        var data= $('#no_of_option').val();
        if (data>1) {
             $.ajax({
              url: "getOptionList.php",
              cache: false,
              type: "POST",
              data: {no_of_option : data},
              success: function(response){
                  $('#OptionResp').html(response);
                  
              }
           });
          } else {
           swal("Minimum 2 Option Required !", {
                            icon: "error",
                          });
           $('#OptionResp').html(' ');
          }
       }
                       

  </script>