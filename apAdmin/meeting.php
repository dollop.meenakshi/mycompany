<?php error_reporting(0);
$startDate = date('Y-m-d', strtotime('-1 month'));
$endDate = date('Y-m-d');
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Meetings</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
    </div>

    <form action="" >
      <div class="row ">
          <div class="col-md-2 col-6 form-group">
            <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') { echo $_GET['from']; } else { echo $startDate; } ?>">
          </div>
          <div class="col-md-2 col-6 form-group">
            <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $_GET['toDate']; } else { echo $endDate; } ?>">
          </div>
          <div class="col-md-1">
              <input class="btn btn-success" type="submit" name="getReport"  value="Get Data">
          </div>    
      </div>
    
    </form>

    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                    if (isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
                      $dateFilterQuery = " AND DATE_FORMAT(meeting_master.meeting_created_date, '%Y-%m-%d') BETWEEN '$_GET[from]' AND '$_GET[toDate]'";
                    }else{
                      $dateFilterQuery = " AND DATE_FORMAT(meeting_master.meeting_created_date, '%Y-%m-%d')  BETWEEN '$startDate' AND '$endDate'";
                    }
                    $q=$d->select("meeting_master,users_master","meeting_master.society_id='$society_id' AND meeting_master.meeting_host_by=users_master.user_id $dateFilterQuery $blockAppendQueryUser","ORDER BY meeting_master.meeting_id DESC");
                ?>
                <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>                        
                            <th>Title</th>                      
                            <th>Meeting Date</th>                      
                            <th>Meeting Time</th>                      
                            <th>Host By</th>                      
                            <th>Status</th>                      
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i=1;                   
                        $counter = 1;
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                            <td><?php echo $counter++; ?></td>
                            <td><?php echo $data['meeting_title']; ?> <a href="meetingMember?id=<?php echo $data['meeting_id']; ?>"><span class="badge badge-pill badge-info m-1">Attendees (<?php echo $totalMember = $d->count_data_direct("meeting_member_id","meeting_members_master	","meeting_id='$data[meeting_id]'"); ?>)</span></a></td>
                            <td><?php echo date("d M Y", strtotime($data['meeting_date']));?></td>
                            <td><?php echo date("h:i A", strtotime($data['meeting_time']));?></td>
                            <td><?php echo $data['user_full_name']; ?></td>
                            <td>
								<?php if($data['meeting_status'] == 0){
                                    $status = "Upcoming";
                                    $statusClass = "badge-primary";
                                }
                                else if($data['meeting_status'] == 1){
                                    $status = "Canceled";
                                    $statusClass = "badge-danger";
                                }else if($data['meeting_status'] == 2){
                                    $status = "Ongoing";
                                    $statusClass = "badge-info";
                                }else{
                                    $status = "Completed";
                                    $statusClass = "badge-success";
                                }
                                ?>
								<p class="card-title badge <?php echo $statusClass; ?>"><?php echo $status; ?></p>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                <a href="meetingMember?id=<?php echo $data['meeting_id']; ?>" class="btn btn-sm btn-primary ml-1"><i class="fa fa-eye"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>


<div class="modal fade" id="meetingDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Meeting Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
      <div class="row mx-0" id="showMeetingData">
        <div class="col-md-12">
			
        </div>
      </div>
      </div>
      
    </div>
  </div>
</div>


