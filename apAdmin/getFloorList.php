<?php 
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../img/$language_id.xml");
$floorLngName = $xml->string->floor;

for ($i=1; $i <=$no_of_floor ; $i++) { 
 ?>

<label for="input-10"  style="padding-bottom: 15px;" class="col-sm-2 col-form-label"><?php echo $floorLngName; ?> Name <?php echo $i; ?> <span class="text-danger">*</span></label>
  <div class="col-sm-4" style="padding-bottom: 15px;">
    <input maxlength="30" value="" type="text" id="floor_name<?php echo $i; ?>" class="form-control" name="floor_name[<?php echo $i; ?>]">
  </div>

<?php }  ?>
<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
<script type="text/javascript">
	$("[name^=floor_name]").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            noSpace: true,
            messages : { required : 'Please Enter Department Name' }
        });
    });
</script>