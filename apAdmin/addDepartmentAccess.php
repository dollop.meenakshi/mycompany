<?php
extract(array_map("test_input", $_POST));
if(isset($_POST) && $_POST['floor_id'] != ''){

$floor_id = $_POST['floor_id'];
$q = $d->selectRow("*","floors_master,block_master" ,"floors_master.society_id='$society_id' AND block_master.block_id = floors_master.block_id AND floors_master.floor_id = '$floor_id' $blockAppendQuery"); 
$data=mysqli_fetch_array($q);
$block_id = $data['block_id'];
$floor_name = $data['floor_name'];
$accessBlockIds = $data['access_block_ids'];
$access_block_ids=explode(",", $accessBlockIds);
array_push($access_block_ids,$block_id);
$access_block_ids=implode("','", $access_block_ids);
$block_name = $data['block_name'];
$access_floor_ids = array();
//$access_floor_ids=explode(",", $accessFloorIds);
$accessBlockIds = $data['access_block_ids'];
$q10 = $d->select('access_floor_master',
					 "access_floor_id=$floor_id");
while ($accessBlockDpData=mysqli_fetch_array($q10)) {
	array_push($access_floor_ids,$accessBlockDpData['floor_id']);
}
	
$accessFloorIds = implode(',',$access_floor_ids);
?>
<?php if(isset($floor_id)){ ?>
<div class="content-wrapper">
    <div class="container-fluid">
      	<!-- Breadcrumb-->
     	<div class="row pt-2 pb-2">
        	<div class="col-sm-9">
        		<h4 class="page-title"> Add Department Restrict</h4>
     		</div>
			<div class="col-sm-3">

			</div>
     	</div>
    	<!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">  
						<form id="addLeaveAssignBulk" action="controller/BranchAndDepartmentAccessController.php" enctype="multipart/form-data" method="post">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row w-100 mx-0">
										<label for="input-10" class="col-lg-2 col-md-2 col-form-label">Department <span class="required">*</span></label>
										<div class="col-lg-4 col-md-4 col-12" id="">
											<input type="text" value="<?php echo $floor_name; ?> (<?php echo $block_name; ?>)" class="form-control" disabled>
											<input type="hidden" name="floor_id" value="<?php echo $floor_id; ?>">
										</div>                   
									</div> 
								</div>  
								<div class="col-md-12 text-center">
									<div class="form-group row w-100 mx-0">
										<div class="col-sm-12">
											<input type="checkbox" id="user-checkbox" class="selectAll checkedCheckBox" />
											<label for="user-checkbox"><?php echo $xml->string->check_uncheck; ?> </label>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row w-100 mx-0">
										<?php 
										if($accessBlockIds != ''){
											$q = $d->select("floors_master,block_master","block_master.block_id = floors_master.block_id AND floors_master.society_id='$society_id' AND (floors_master.block_id NOT IN ('$access_block_ids') OR block_master.block_id = '$block_id') AND floors_master.floor_id != '$floor_id'");
										}else{
											$q = $d->select("floors_master,block_master","block_master.block_id = floors_master.block_id AND floors_master.society_id='$society_id' AND floors_master.floor_id != '$floor_id'");
										}
										$departmentArr = array();
										$branchArr = array();
										while($data01=mysqli_fetch_array($q)){
											array_push($departmentArr, $data01);
											array_push($branchArr, $data01['block_name']);
										}
										$branchArr = array_unique($branchArr);
										$branchArr = array_values($branchArr);
										?>
										<div class="col-lg-12">
											<div class="row">
										<?php
										for ($j=0; count($branchArr)>$j ; $j++) { ?>
										<div class="col-md-4">
        									<div class="form-group row w-100 mx-0">
											<label for=""><?php echo $branchArr[$j]; ?></label>
										<?php
										for($i=0; count($departmentArr)>$i; $i++){
											if($departmentArr[$i]['block_name'] == $branchArr[$j]){
											$floorId=$departmentArr[$i]['floor_id'];?>
											<div class="col-lg-12 col-md-12 col-12" id="">
												<label>
													<input <?php if(in_array($floorId, $access_floor_ids)){ echo "checked"; }?> type="checkbox" class="multiDelteCheckbox checkedCheckBox" value="<?php echo $departmentArr[$i]['floor_id'] ?>" name="access_floor_ids[]">
													
													<input type="hidden" name="block_id[<?php echo $departmentArr[$i]['floor_id'] ?>]" value="<?php echo $departmentArr[$i]['block_id'];?>">
													<span><b><?php echo $departmentArr[$i]['floor_name'] ?></b></span>
												</label>
											</div>
											<?php } } ?>
											</div>
										</div>
										 <?php } ?>
										</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-footer text-center">
								<?php if(isset($accessFloorIds) && $accessFloorIds == '') { ?>
									<button id="addDepartmentAccessBtn" disabled type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
								<?php } else { ?>
									<button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> UPDATE</button>
								<?php } ?>
								<input type="hidden" name="addDepartmentAccess"  value="addDepartmentAccess">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<?php } ?>
<script src="assets/js/jquery.min.js"></script>
<script>
	$(document).on('change', '.checkedCheckBox', function(){
		value = $('input[name="access_floor_ids[]"]:checked').length;
		if(value > 0){
			$('#addDepartmentAccessBtn').prop("disabled", false);
		}else{
			$('#addDepartmentAccessBtn').prop("disabled", true);
		}
	});
</script>
<?php } ?>