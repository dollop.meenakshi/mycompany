<?php
if(isset($_GET['CId']) && !empty($_GET) && isset($_GET['SCId']))
{
    $country_id = $_GET['CId'];
    $ci_st_id = explode("-",$_GET['SCId']);
    $city_id = $ci_st_id[0];
    $state_id = $ci_st_id[1];
}
elseif((isset($_COOKIE['CId']) && !empty($_COOKIE['CId'])) || (isset($_COOKIE['SId']) && !empty($_COOKIE['SId'])) || (isset($_COOKIE['CIId']) && !empty($_COOKIE['CIId'])))
{
    $country_id = $_COOKIE['CId'];
    $state_id = $_COOKIE['SId'];
    $city_id = $_COOKIE['CIId'];
}
if(isset($city_id) && !empty($city_id) && !ctype_digit($city_id))
{
    $_SESSION['msg1'] = "Invalid City Id!";
?>
    <script>
        window.location = "manageArea";
    </script>
<?php
exit;
}

if(isset($state_id) && !empty($state_id) && !ctype_digit($state_id))
{
    $_SESSION['msg1'] = "Invalid State Id!";
?>
    <script>
        window.location = "manageArea";
    </script>
<?php
exit;
}
if(isset($country_id) && !empty($country_id) && !ctype_digit($country_id))
{
    $_SESSION['msg1'] = "Invalid Country Id!";
    ?>
    <script>
        window.location = "manageArea";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Area</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addArea" method="post" id="addAreaBtnForm">
                        <input type="hidden" name="addAreaBtn" value="addAreaBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteArea');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormArea">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">Country </label>
                    <select name="CId" id="CId" class="form-control single-select" required onchange="getStateCity(this.value);">
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->select("countries","");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($country_id == $cd['country_id']) { echo 'selected';} ?> value="<?php echo $cd['country_id']; ?>"><?php echo $cd['country_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">State-City </label>
                    <select class="form-control single-select" id="SCId" name="SCId" required>
                        <option value="">-- Select --</option>
                        <?php
                        if(isset($country_id) && $country_id != 0 && $country_id != "")
                        {
                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                            while ($qd = $qt->fetch_assoc())
                            {
                        ?>
                        <option <?php if($city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if(isset($country_id) && isset($state_id) && isset($city_id))
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Area Name</th>
                                        <th>Pincode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $q = $d->selectRow("dm.distributor_id,rm.retailer_id,amn.*,ci.city_name,s.state_name,co.country_name","area_master_new AS amn JOIN cities AS ci ON ci.city_id = amn.city_id JOIN states AS s ON s.state_id = amn.state_id JOIN countries AS co ON co.country_id = amn.country_id LEFT JOIN retailer_master AS rm ON rm.area_id = amn.area_id LEFT JOIN distributor_master AS dm ON dm.distributor_area_id = amn.area_id","amn.country_id = '$country_id' AND amn.state_id = '$state_id' AND amn.city_id = '$city_id'","GROUP BY amn.area_id ORDER BY area_id ASC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php
                                            if(empty($retailer_id) && $retailer_id == "" && empty($distributor_id) && $distributor_id == "")
                                            {
                                            ?>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $area_id; ?>">
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input disabled type="hidden">
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="addArea" method="post">
                                                    <input type="hidden" name="area_id" value="<?php echo $row['area_id']; ?>">
                                                    <input type="hidden" name="editArea" value="editArea">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if(empty($retailer_id) && $retailer_id == "" && empty($distributor_id) && $distributor_id == "")
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="controller/areaController.php" method="POST">
                                                    <input type="hidden" name="area_id" value="<?php echo $area_id; ?>">
                                                    <input type="hidden" name="country_id" value="<?php echo $country_id; ?>">
                                                    <input type="hidden" name="state_id" value="<?php echo $state_id; ?>">
                                                    <input type="hidden" name="city_id" value="<?php echo $city_id; ?>">
                                                    <input type="hidden" name="deleteArea" value="deleteArea">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $area_name; ?></td>
                                        <td><?php if($pincode != 0){ echo $pincode; } ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select County & State-City</span>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/areaController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#SCId').empty();
            response = JSON.parse(response);
            $('#SCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#SCId').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var country_id = $('#CId').val();
    var state_city_id = $('#SCId').val();
    var city_id = state_city_id.substr(0, state_city_id.indexOf('-'));
    var state_id = state_city_id.split("-")[1];
    if(country_id != "" && country_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'country_id',
            value: country_id
        }).appendTo('#addAreaBtnForm');
    }
    if(city_id != "" && city_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'city_id',
            value: city_id
        }).appendTo('#addAreaBtnForm');
    }if(state_id != "" && state_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'state_id',
            value: state_id
        }).appendTo('#addAreaBtnForm');
    }
    $("#addAreaBtnForm").submit()
}

$(function() {
    $("#CId").change(function()
    {
        setTimeout(
            function()
            {
                $("#filterFormArea").removeAttr("novalidate");
            },700);
    });
});
</script>