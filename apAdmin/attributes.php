<?php error_reporting(0);
  $dId = $_REQUEST['dId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Attributes</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteAttributes');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>
      <form action="" class="">
        <div class="row ">
          <div class="col-md-3 form-group">
            <select name="dId" class="form-control single-select" onchange="this.form.submit()">
              <option value="">-- Select Dimensional --</option>
              <?php
              $dq = $d->select("dimensional_master", "society_id='$society_id' ");
              while ($dData = mysqli_fetch_array($dq)) {
              ?>
                <option <?php if ($_GET['dId'] == $dData['dimensional_id']) { echo 'selected';
                        } ?> value="<?php echo $dData['dimensional_id']; ?>"><?php echo $dData['dimensional_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Attribute</th>
                        <th>Dimensional</th>                     
                        <th>Type</th>                     
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        if(isset($dId) && $dId > 0){
                          $appendQuery = " AND attribute_master.dimensional_id = '$dId'";
                        }
                        $q=$d->select("attribute_master,dimensional_master","attribute_master.society_id='$society_id' AND attribute_master.dimensional_id=dimensional_master.dimensional_id $appendQuery");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                            <?php  /* $totalAttribute = $d->count_data_direct("attribute_id","attribute_master","dimensional_id='$data[dimensional_id]'");
                              if( $totalAttribute==0) { */
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['attribute_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['attribute_id']; ?>">                      
                          <?php //} ?>    
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['attribute_name']; ?></td>
                       <td><?php echo $data['dimensional_name']; if($data['dimensional_status']=="1"){ echo " (Deactive)"; }?></td>
                       <td> 
                            <?php if($data['attribute_type'] == 0){
                                    echo 'Textbox';
                                  } else if($data['attribute_type'] == 1){
                                    echo 'Percentage';
                                  } else if($data['attribute_type'] == 2){
                                    echo 'Star(1 to 5)';
                                  } else if($data['attribute_type'] == 3){
                                    echo 'Number(1 to 10)';
                                  } else if($data['attribute_type'] == 4){
                                    echo 'A B C D E F';
                                  }    
                            ?>
                       </td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="attributeDataSet(<?php echo $data['attribute_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['attribute_status']=="0"){
                              $status = "attributeStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "attributeStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['attribute_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attributes</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addAttributesForm" action="controller/AttributeController.php" enctype="multipart/form-data" method="post"> 
          <div class="form-group row">  
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Dimensional <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id=""> 
                    <select name="dimensional_id" id="dimensional_id" class="form-control single-select" required>
                        <option value="">-- Select Dimensional --</option> 
                        <?php 
                            $dq=$d->select("dimensional_master","society_id='$society_id' AND dimensional_status=0");  
                            while ($dData=mysqli_fetch_array($dq)) {
                        ?>
                        <option value="<?php echo $dData['dimensional_id'];?>" ><?php echo $dData['dimensional_name'];?></option>
                        <?php } ?>
                    </select>
                </div>  
            </div> 
           <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Name <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control" placeholder="Attributes Name" id="attribute_name" name="attribute_name" value="">
                </div>  
           </div>                    
            <div class="form-group row">  
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Type <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id=""> 
                    <select class="form-control single-select" name="attribute_type" id="attribute_type">
                        <option value="">-- Select Type --</option>
                        <option value="0">Textbox</option>
                        <option value="1">Percentage</option>
                        <option value="2">Star(1 to 5)</option>
                        <option value="3">Number(1 to 10)</option>
                        <option value="4">A B C D E F</option>
                    </select>
                </div>  
            </div>                    
            <div class="form-footer text-center">
                <input type="hidden" id="attribute_id" name="attribute_id" value="" >
                <button id="addDimensionalBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                <input type="hidden" name="addAttributes"  value="addAttributes">
                <button id="addAttributesBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
            </div>
        </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
