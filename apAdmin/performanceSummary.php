<?php error_reporting(0); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Performance Summary</h4>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Dimensional</th>
                                        <th>PMS Type</th>
                                        <th>Schedule Date</th>
                                        <th>Reviewer Single Percent</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $q = $d->selectRow(
                                        "users_master.user_full_name,users_master.user_designation, pms_schedule_master.schedule_date, pms_schedule_master.pms_type_on_schedule, pms_schedule_master.reviewer_single_weightage, dimensional_master.dimensional_name",
                                        "users_master, pms_schedule_master, dimensional_master",
                                        "users_master.user_id=pms_schedule_master.user_id AND dimensional_master.dimensional_id=pms_schedule_master.dimensional_id AND review_date IS NOT NULL","ORDER BY pms_schedule_master.reviewer_single_weightage DESC"
                                    );
                                    $counter = 1;
                                    /* $data = mysqli_fetch_array($q);
                                    echo "<pre>";
                                    print_r($data);die; */
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $counter++; ?></td>
                                            <td><?php echo $data['user_full_name']; ?>
                                                (<?php echo $data['user_designation']; ?>)</td>
                                            <td><?php echo $data['dimensional_name']; ?></td>
                                            <?php 
                                                if($data['pms_type_on_schedule']==0){
                                                    $pmsType='Weekly';
                                                }else if($data['pms_type_on_schedule']==1){
                                                    $pmsType='Monthly';
                                                }else if($data['pms_type_on_schedule']==2){
                                                    $pmsType='Quarterly';
                                                }else if($data['pms_type_on_schedule']==3){
                                                    $pmsType='Half Yearly';
                                                }else if($data['pms_type_on_schedule']==4){
                                                    $pmsType='Yearly';
                                                }
                                                ?>
                                            <td> <?php echo $pmsType; ?></td>
                                            <td><?php echo date("d M Y", strtotime($data['schedule_date'])); ?></td>
                                            <td><?php echo $data['reviewer_single_weightage']; ?></td>
                                            
                                        </tr>
                                    <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
