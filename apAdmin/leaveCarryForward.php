<?php
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
if(isset($_GET['year'])){
	$year = (int)$_REQUEST['year'];
} else {
	$year =  date('Y');
}
$nextYearCarray = $year+1;
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-3 col-md-6 col-6">
				<?php if($year>0){ ?>
				<h4 class="page-title">Leave Carry Forward form <?php echo $year; ?> to <?php echo $nextYearCarray ?></h4>
				<?php }else{ ?> 
					<h4 class="page-title">Leave Carry Forward</h4>
				<?php } ?>
			</div>
			<div class="col-sm-3 col-md-6 col-6">
			</div>
		</div>
    <!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form class="branchDeptFilterWithUser" action="" enctype="multipart/form-data" method="get">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Branch <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required <?php if($bId>0) { echo 'disabled';} ?>>
												<option value="">-- Select Branch --</option> 
												<?php 
												$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
												while ($blockData=mysqli_fetch_array($qb)) {
												?>
												<option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Department <span class="required">*</span></label>
										<div class="col-md-12" id="">
											<select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required <?php if($dId>0) { echo 'disabled';} ?>>
												<option value="">-- Select Department --</option> 
												<?php 
													$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
													while ($depaData=mysqli_fetch_array($qd)) {
												?>
												<option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
												<?php } ?>
												
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-lg-12 col-md-12 col-form-label ">Employee <span class="required">*</span></label>
										<div class="col-lg-12 col-md-12" id="">
											<select name="uId" id="uId" class="form-control single-select" onchange="this.form.submit()">
												<option value="">-- Select Employee --</option> 
												<?php 
													$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id = '$dId' $blockAppendQueryUser");  
													while ($userdata=mysqli_fetch_array($user)) {
												?>
												<option <?php if($uId==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group" >
										<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year  <span class="required">*</span></label>
										<div class="col-lg-12 col-md-12" id="">
											<select name="year" class="form-control" onchange="this.form.submit()">
												<option <?php if($year==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
												<option <?php if($year==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
												<option <?php if($year==$currentYear) { echo 'selected';}?> <?php if($year=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
												<option <?php if($year==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
											</select>
										</div>
									</div>
								</div>
								
							</div>
							<?php if(isset($dId) && isset($uId) && $dId==0 && $uId==0){ ?>
							<div class="form-footer text-center">
                                <button id="addAttendanceTypeBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Get Data </button>
                            </div>
							<?php } ?>  
							
						</form>
						<?php if($bId>0 && $dId>0 && $uId>0 && $year>0){
							$leaveTypeIds = array();
						$ldcq = $d->selectRow('*',"leave_assign_master", "user_id='$uId' AND assign_leave_year='$nextYearCarray'");
						if(mysqli_num_rows($ldcq)){
							
						while ($ldcData=mysqli_fetch_array($ldcq)) {
							array_push($leaveTypeIds,$ldcData['leave_type_id']);
						}
						?>
						<form action="controller/leaveController.php" method="post">
							<?php
							$q=$d->select("leave_assign_master,leave_type_master,users_master","users_master.user_id=leave_assign_master.user_id AND leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND leave_assign_master.society_id='$society_id' AND users_master.delete_status=0 AND leave_assign_master.user_id='$uId' AND leave_assign_master.assign_leave_year='$year' AND leave_assign_master.carry_forward_to_next_year=0 $blockAppendQueryUser","ORDER BY leave_assign_master.user_leave_id ASC");
							if(mysqli_num_rows($q)>0){
								$sq=$d->select("salary_slip_master","user_id='$uId'","ORDER BY salary_slip_id DESC LIMIT 1");
								$salaryData = mysqli_fetch_array($sq);?>
								<input type="hidden" id="per_day_salary" value="<?php echo $salaryData['per_day_salary']; ?>">
							<?php $i = 0;
							while($data = mysqli_fetch_array($q)){
								$approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
								$approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
								$approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
								$approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
								$noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave']/2;
								$payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_payout_year = '$year'");
								$payOutLeaveData = mysqli_fetch_assoc($payOutLeave);
								$remaining_leave = $data['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'];
								if($remaining_leave >= $data['min_carry_forward']){
								?>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Leave Type <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<input type="text" class="form-control" disabled value="<?php echo $data['leave_type_name']; ?>">
											<input type="hidden" name="leave_type_id[]" value="<?php echo $data['leave_type_id']; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Remaining Leave <span class="required">*</span></label>
										<div class=" col-md-12" id="">
											<input type="text" class="form-control" disabled value="<?php echo $remaining_leave; ?>">
											<input type="hidden" name="remaining_leave[<?php echo $i; ?>]" id="remaining_leave<?php echo $i; ?>" value="<?php echo $remaining_leave; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="input-10" class="col-md-12 col-form-label">Carry Forward Leaves <span class="required">*</span></label>
										<div class=" col-md-12" id="">
										<select <?php if(!in_array($data['leave_type_id'], $leaveTypeIds)){echo 'disabled';} ?> name="carry_forward_leaves[]" id="" class="form-control single-select">
											<option value="">--Select Leave--</option>
											<?php 
											$x= 1;
											$j = $data['max_carry_forward'];
											if($data['max_carry_forward']>=$remaining_leave){
												$j = $remaining_leave;
											}
											for($a=($x-0.5); $a <= $j; $a+=0.5) { ?>
												<option value="<?php echo $a; ?>"><?php echo $a; ?></option>
											<?php } ?>
										</select>
										</div>
									</div>
								</div>
								<?php if(!in_array($data['leave_type_id'], $leaveTypeIds)){ ?>
								<div class="col-md-3">
									<div class="form-group">
										<p for="input-10" class="col-md-12 col-form-label required">You can not carry forward this leaves to <?php echo $nextYearCarray; ?>, because this leave type not assign to your department </label>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php $i++; } } ?>
							<?php if($i > 0){ ?>
							<div class="form-footer text-center">
								<input type="hidden" name="block_id" value="<?php echo $bId; ?>">
								<input type="hidden" name="floor_id" value="<?php echo $dId; ?>">
								<input type="hidden" name="user_id" value="<?php echo $uId; ?>">
								<input type="hidden" name="leave_payout_year" value="<?php echo $year; ?>">
								<input type="hidden" class="form-control" name="carry_forward_from_year" value="<?php echo $nextYearCarray; ?>">
								<input type="hidden" name="addCarryForwardLeave" value="addCarryForwardLeave">
                                <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                            </div>
							<?php }else{ ?>
								<span class="required">No available leave to carry forward in <?php echo $nextYearCarray; ?></span>
							<?php } }else{ ?>
								<span class="required">Data not available</span>
							<?php  } ?>
						</form>
						<?php }else{ ?>
							<div class="" role="alert">
								<form action="leaveAssignBulk" method="post">
								<span><strong>Note :</strong> Please Assign Leave for year <?php echo $nextYearCarray; ?>!!
									<input type="hidden" name="user_id" value="<?php echo $uId; ?>">
									<input type="hidden" name="floor_id" value="<?php echo $dId; ?>">
									<input type="hidden" name="leave_year" value="<?php echo $nextYearCarray; ?>">
									<button class="btn btn-link">Click here</button>
									</span>
								</form>

							</div>
						<?php } }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
function checkvalidation(id){
	remaining_leave = parseInt($('#remaining_leave'+id).val());
	no_of_payout_leaves = parseInt($('#no_of_payout_leaves'+id).val());
	per_day_salary = parseInt($('#per_day_salary').val());
	$('#leave_payout_amount'+id).val('');
	if(no_of_payout_leaves > remaining_leave){
		$('#no_of_payout_leaves'+id).val('');
		$('#leave_payout_amount'+id).val('');
	}else{
		if(per_day_salary >0  && no_of_payout_leaves>0){
			var leave_payout_amount = per_day_salary*no_of_payout_leaves;
			$('#leave_payout_amount'+id).val(leave_payout_amount);
		}
	}
}
</script>