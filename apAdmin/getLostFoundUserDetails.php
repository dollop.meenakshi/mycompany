<?php
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
$bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
  include 'common/checkLanguage.php';

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));
$q=$d->select("lost_found_master","lost_found_master_id='$lost_id' ");
$data=mysqli_fetch_array($q);
if ($data['user_id']!=0) {
  $qbb=$d->select("lost_found_master,users_master,block_master,unit_master,floors_master","block_master.block_id=floors_master.block_id AND floors_master.floor_id=users_master.floor_id AND lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.user_id=users_master.user_id  AND block_master.block_id=unit_master.block_id AND block_master.block_id=lost_found_master.block_id AND unit_master.unit_id=lost_found_master.unit_id AND lost_found_master.user_id!=0 AND lost_found_master.lost_found_master_id='$data[lost_found_master_id]'","" );
    $subData=mysqli_fetch_array($qbb);
    $userName=$subData['user_full_name'] . " (" . $subData['user_designation'] . ")";
    $block_name=$subData['block_name'].'-'.$subData['floor_name'];
    $mobile=$subData['user_mobile'];
    $user_email=$subData['user_email'];
  } else {
    $qbb=$d->select("lost_found_master,employee_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.gatekeeper_id=employee_master.emp_id  AND lost_found_master.lost_found_master_id='$data[lost_found_master_id]'","" );
      $subData=mysqli_fetch_array($qbb);
      $userName= $subData['emp_name'].'-Security Guard';
      $block_name= $subData['gate_number'];
      $mobile= $subData['emp_mobile'];
      $user_email= $subData['emp_email'];
  }
?>
<div class="row mb-3">
    <div class="col-sm-3 col-4">
      <h6 class="mb-0"><?php echo $xml->string->unit; ?>/Gate No. :</h6>
    </div>
    <div class="col-sm-9 col-8">
      <?php echo $block_name;?>
    </div>
  </div>
	<div class="row mb-3">
    <div class="col-sm-3 col-4">
      <h6 class="mb-0">Name :</h6>
    </div>
    <div class="col-sm-9 col-8">
      <?php echo $userName;?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-sm-3 col-4">
      <h6 class="mb-0">Email :</h6>
    </div>
    <div class="col-sm-9 col-8">
      <?php echo $user_email;?>
    </div>
  </div>
 <div class="row mb-3">
    <div class="col-sm-3 col-4">
      <h6 class="mb-0">Item :</h6>
    </div>
    <div class="col-sm-9 col-8 breakLineClass">
      <?php echo $data['lost_found_title'];?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-sm-3 col-4">
      <h6 class="mb-0">Description :</h6>
    </div>
    <div class="col-sm-9 col-8 breakLineClass">
      <?php echo $data['lost_found_description'];?>
    </div>
  </div>