<div class="content-wrapper"> 
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->society; ?> Sub <?php echo $xml->string->floor; ?> </h4>
        
      </div>
      <div class="col-sm-3">
        
      </div>
    </div>
    <!-- End Breadcrumb-->
    <?php
    extract(array_map("test_input" , $_POST));
    // print_r($_POST);
    /* $q1 = $d->select("floors_master","society_id='$society_id' AND block_id = '$block_id'","ORDER BY floor_id DESC");
    $data1 = mysqli_fetch_array($q1);
    $floorNo = substr($data1['floor_name'],0,1);
    $floorNo++; */
    ?>
    <div class="row">
      
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="subDepartmentForm" action="controller/subDepartmentController.php" method="post">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-address-book-o"></i>
              <?php echo $xml->string->add; ?>  <?php echo $xml->string->single; ?> Sub <?php echo $xml->string->floor; ?> 
              </h4>
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group row w-100 mx-0">
                        <label for="input-12" class="col-lg-12 col-md-12 col-form-label col-12"><?php echo $xml->string->block; ?> <?php echo $xml->string->name; ?> </label>
                        <div class="col-lg-12 col-md-12 col-12">
                        <select class="form-control" name="block_id" id="block_id" >
                            <option><?php echo $blockName; ?></option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row w-100 mx-0">
                        <label for="input-12" class="col-lg-12 col-md-12 col-form-label col-12"><?php echo $xml->string->floor; ?>  <?php echo $xml->string->name; ?>   </label>
                        <div class="col-lg-12 col-md-12 col-12">
                        <select class="form-control" name="floor_id" id="floor_id" >
                            <option><?php echo $floorName; ?></option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row w-100 mx-0">
                        <label for="input-12" class="col-lg-12 col-md-12 col-form-label col-12">Sub <?php echo $xml->string->floor; ?>  <?php echo $xml->string->name; ?>  <span class="text-danger">*</span> </label>
                        <div class="col-lg-12 col-md-12 col-12">
                        <input type="hidden" name="no_of_floor" value="1">
                        <!-- <input type="hidden" name="floorNo" value="<?php echo $floorNo; ?>"> -->
                        <input required="" type="text" id="sub_department_name" class="form-control" name="sub_department_name" value="">
                        </div>
                    </div>
                </div>
              </div>
              
              <div id="floorResp" class="form-group row">
              </div>
              <div class="form-footer text-center">
                 <!-- <input type="hidden" value="0" name="no_of_unit">
                 <input type="hidden" value="1" name="unit_type_single"> -->
                <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                <input type="hidden" name="floor_id" value="<?php echo $floor_id; ?>">
                <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                <input type="hidden" name="addSingleSubDept" value="addSingleSubDept">  
                <button type="submit" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->