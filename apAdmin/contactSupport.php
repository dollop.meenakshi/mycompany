     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Contact Support Team</h4>
       
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
                <form id="contactCompany" action="controller/feedbackController.php" enctype="multipart/form-data" method="post">
              
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Subject <span class="required">*</span></label>
                    <div class="col-sm-10" id="">
                         <input type="text"   maxlength="150" required="" name="subject" class="form-control" id="subject">
                    </div>
                    
                </div>


                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label"> Description <span class="required">*</span></label>
                    <div class="col-sm-10" >
                      <textarea maxlength="300" required="" name="feedback_msg" class="form-control "></textarea>
                    </div>
                </div>
                <div class="form-group row">
                   
                    <label for="input-10" class="col-sm-2 col-form-label">Attachment</label>
                    <div class="col-sm-10" >
                      <input type="file"  accept="image/*"  maxlength="250"  name="attachment" class="form-control-file border photoOnly" id="attachment">
                    </div>
                </div>
              
         
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn ?>

                  <button id="addPenaltiesBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Send</button>
                 <input type="hidden" name="add"  value="add">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" ><i class="fa fa-check-square-o"></i> Reset</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->