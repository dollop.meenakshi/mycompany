<?php
extract(array_map("test_input", $_POST));
$user_id = $_POST['user_id'];
$q = $d->selectRow('*',"task_access_master","user_id='$user_id' AND society_id='$society_id'");
$data = mysqli_fetch_array($q);
extract($data);
if($data['task_access_type']=="Branch Wise"){
	$task_access_ids = explode(',', $data['task_access_ids']);
}
else if($data['task_access_type']=="Department Wise"){
	$task_access_ids = explode(',', $data['task_access_ids']);
	$dept_ids = implode("','", $task_access_ids);
	//$dept_ids = explode("','", $dept_ids);
	$q1 = $d->selectRow('*',"floors_master","floor_id IN ('$dept_ids') AND society_id='$society_id'");	
	$blockIds = array();
	while($data1 = mysqli_fetch_array($q1)){
		array_push($blockIds,$data1['block_id']);
	}
	$blockIds = array_unique($blockIds);
	$blockIds = array_values($blockIds);
	$block_ids = implode("','", $blockIds);
}
else if($data['task_access_type']=="Employee Wise"){
	$task_access_ids = explode(',', $data['task_access_ids']);
	$user_ids = implode("','", $task_access_ids);
	//$dept_ids = explode("','", $dept_ids);
	$q2 = $d->selectRow('*',"users_master","user_id IN ('$user_ids') AND society_id='$society_id'");
	$blockIds = array();
	$floorIds = array();
	while($data2 = mysqli_fetch_array($q2)){
		array_push($blockIds,$data2['block_id']);
		array_push($floorIds,$data2['floor_id']);
	}
	$blockIds = array_unique($blockIds);
	$blockIds = array_values($blockIds);
	$block_ids = implode("','", $blockIds);
	$floorIds = array_unique($floorIds);
	$floorIds = array_values($floorIds);
	$floor_ids = implode("','", $floorIds);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Task Access</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addTaskAccessForm" action="controller/TaskController.php" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Access To Employee <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8 col-12" id="">
                                <select  type="text" required="" class="form-control single-select" id="user_id" name="user_id">
                                    <?php
                                    $userData=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                                    while ($user=mysqli_fetch_array($userData)) {
                                    ?>
                                    <option <?php if(isset($user_id) && $user_id == $user['user_id']){ echo "selected"; } ?> value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                                    <?php }?>
                                </select>
                            </div>                   
                        </div> 
                    </div>  
					<div class="col-md-6">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Access Type <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8 col-12" id="">
                                <select type="text" required="" class="form-control" id="task_access_type" name="task_access_type" onchange="accessType(this.value);">
                                    <option value="">--Select Access Type--</option>
									<option <?php if($data['task_access_type']=="Branch Wise"){echo "selected";} ?> value="Branch Wise">Branch Wise</option>
									<option <?php if($data['task_access_type']=="Department Wise"){echo "selected";} ?> value="Department Wise">Department Wise</option>
									<option <?php if($data['task_access_type']=="Employee Wise"){echo "selected";} ?> value="Employee Wise">Employee Wise</option>
                                </select>
                            </div>                   
                        </div> 
                    </div>  
				</div>
				<div class="row all-access branch-wise <?php if($data['task_access_type']=="Branch Wise"){}else{echo "d-none";} ?>">
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select taskAccessForBranchMultiSelectCls task_access_ids" name="task_access_ids[]" multiple>
									<option value="0">All Branch</option>
									<?php
									$bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
									while ($blockData=mysqli_fetch_array($bq)) {
									?>
									<option <?php if(in_array($blockData['block_id'],$task_access_ids)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
									<?php }?>
								</select>
							</div>                   
						</div> 
					</div>
				</div>
				<div class="row all-access department-wise <?php if($data['task_access_type']=="Department Wise"){}else{echo "d-none";} ?>">
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select dwblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" onchange="getTaskAccessForDepartment();">
									<!-- <option value="0">All Branch</option> -->
									<?php
									$bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
									while ($blockData=mysqli_fetch_array($bq)) {
									?>
									<option <?php if(in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
									<?php }?>
								</select>
							</div>                   
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select dwfloorId taskAccessForDepartmentMultiSelectCls task_access_ids" multiple name="task_access_ids[]">
									<option value="0">All Department</option>
									<?php 
										$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
										while ($depaData=mysqli_fetch_array($qd)) {
									?>
									<option <?php if(in_array($depaData['floor_id'],$task_access_ids)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
									<?php } ?>
								</select>
							</div>                   
						</div> 
					</div>
				</div>
				<div class="row all-access employee-wise <?php if($data['task_access_type']=="Employee Wise"){}else{echo "d-none";} ?>">
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select ewblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" onchange="getTaskAccessForDepartment();">
									<!-- <option value="0">All Branch</option> -->
									<?php
									$bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
									while ($blockData=mysqli_fetch_array($bq)) {
									?>
									<option <?php if(in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
									<?php }?>
								</select>
							</div>                   
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select ewfloorId taskAccessForDepartmentMultiSelectCls" multiple onchange="getTaskAccessForEmployee();" name="floorId[]">
									<!-- <option value="0">All Department</option> -->
									<?php 
										$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
										while ($depaData=mysqli_fetch_array($qd)) {
									?>
									<option <?php if(in_array($depaData['floor_id'],$floorIds)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
									<?php } ?>
								</select>
							</div>                   
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group row w-100 mx-0">
							<label for="input-10" class="col-lg-4 col-md-4 col-form-label">Employee <span class="required">*</span></label>
							<div class="col-lg-8 col-md-8 col-12" id="">
								<select type="text" required="" class="form-control multiple-select ewempId taskAccessForEmployeeMultiSelectCls task_access_ids" multiple name="task_access_ids[]"> 
									<option value="0">All Employee</option>
									<?php 
										$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id IN ('$floor_ids')$blockAppendQueryUser");  
										while ($userdata=mysqli_fetch_array($user)) {
									?>
									<option <?php if(in_array($userdata['user_id'],$task_access_ids)){ echo "selected"; } ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
									<?php } ?>
								</select>
							</div>                   
						</div> 
					</div>
                </div>
                         
                <div class="form-footer text-center">
					<?php if($data['task_access_id'] !=''){ ?>
                    <button id="addTaskAccessBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
					<?php }else{ ?>
						<button id="addTaskAccessBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
					<?php } ?>
                    <input type="hidden" name="addTaskAccess"  value="addTaskAccess">
                    <input type="hidden" name="task_access_id"  value="<?php echo $data['task_access_id']; ?>">
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function getAccessType(value) {
		$('.multiple-select').val('').trigger('change');
		if(value == "Branch Wise"){
			$('.blockId').val('').trigger('change');
			$('.all-access').addClass('d-none');
			$('.branch-wise').removeClass('d-none');
		}
		else if(value == "Department Wise"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.department-wise').removeClass('d-none');
		}
		else if(value == "Employee Wise"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.employee-wise').removeClass('d-none');
		}
        else{
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
		}
    }
</script>
