 <?php 
    if(isset($_POST['society_id_edit'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("society_master","society_id='$society_id_edit'");
    $data=mysqli_fetch_array($q);
    } else {
    $btnName="Add";
    }
     ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Add /Update (<?php echo $xml->string->society; ?>)</h4>
       
     </div>
     
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-building"></i>
                  Building Information
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> Name <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <?php  if(isset($_POST['society_id_edit'])) { ?>
                    <input type="text" value="<?php echo $data['society_name']; ?>"  required="" class="form-control" id="input-10" name="society_name_edit">
                    <input type="hidden" value="<?php echo $data['society_id']; ?>" id="society_id_edit" required="" class="form-control" id="input-10" name="society_id_edit">
                    <input type="hidden" value="<?php echo $data['socieaty_logo']; ?>" required="" class="form-control" id="input-10" name="socieaty_logo_old">
                    <?php } else { ?>
                    <input type="text" value="<?php echo $data['society_name']; ?>" required="" class="form-control" id="input-10" name="society_name">
                    <?php } ?>
                  </div>
                  
                </div>
                <div class="form-group row">
                  <?php 
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "getCountries=getCountries&language_id=1");

                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'key: bmsapikey'
                  ));

                  $server_output = curl_exec($ch);

                  curl_close ($ch);
                  $server_output=json_decode($server_output,true);
                  ?>
                  <label for="country_id" class="col-sm-2 col-form-label"> Country <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <select type="text" required="" id="country_id" onchange="getStates();" class="form-control single-select" name="country_id">
                      <option value="">-- Select --</option>
                      <?php 
                       for ($ic=0; $ic <count($server_output['countries']) ; $ic++) { 
                       ?>
                        <option <?php if($data['country_id']==$server_output['countries'][$ic]['country_id']) {echo "selected";} ?> value="<?php echo $server_output['countries'][$ic]['country_id'];?>"><?php echo $server_output['countries'][$ic]['name'];?></option>
                        <?php }?>
                      </select>
                  </div>
                   <label for="state_id" class="col-sm-2 col-form-label"> State <span class="required">*</span></label>
                  <div class="col-sm-4">
                       <?php  if(isset($_POST['society_id_edit'])) { 
                       $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS,
                                  "getState=getState&country_id=$data[country_id]&language_id=1");

                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      
                      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'key: bmsapikey'
                      ));

                      $server_output = curl_exec($ch);

                      curl_close ($ch);
                      $server_output=json_decode($server_output,true);
                        ?>
                      <select type="text" onchange="getCity();"  required="" class="form-control single-select" id="state_id" name="state_id">
                        <?php
                          for ($is=0; $is <count($server_output['states']) ; $is++) { 
                           ?>
                           <option <?php if($data['state_id']==$server_output['states'][$is]['state_id']) {echo "selected";} ?> value="<?php echo $server_output['states'][$is]['state_id'];?>"><?php echo $server_output['states'][$is]['name'];?></option>
                          <?php }  ?>
                      </select>
                      <?php } else { ?>
                      <select type="text" onchange="getCity();"  required="" class="form-control single-select" id="state_id" name="state_id">
                      <option value="">-- Select --</option>
                      </select>
                      <?php } ?>
                  </div>
                  
                </div>
                 <div class="form-group row">
                  <label for="input-101" class="col-sm-2 col-form-label"> City <span class="required">*</span></label>
                  <div class="col-sm-4">
                      <?php  if(isset($_POST['society_id_edit'])) {
                        $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS,
                                  "getCity=getCity&state_id=$data[state_id]&language_id=1");

                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      
                      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'key: bmsapikey'
                      ));

                      $server_output = curl_exec($ch);

                      curl_close ($ch);
                      $server_output=json_decode($server_output,true);
                      // print_r($server_output);
                       ?>
                      <select type="text"  required="" class="form-control single-select" id="city_id" name="city_id">
                        <?php
                          for ($icity=0; $icity <count($server_output['cities']) ; $icity++) { 
                           ?>
                           <option <?php if($data['city_id']==$server_output['cities'][$icity]['city_id']) {echo "selected";} ?> value="<?php echo $server_output['cities'][$icity]['city_id'];?>~<?php echo $server_output['cities'][$icity]['name'];?>"><?php echo $server_output['cities'][$icity]['name'];?></option>
                          <?php }  ?>
                      </select>
                      <?php } else { ?>
                      <select type="text" required="" class="form-control single-select" name="city_id" id="city_id">
                      <option value="">-- Select --</option>
                      
                      </select>
                      <?php } ?>
                  </div>
                  <label for="input-101" class="col-sm-2 col-form-label"> Address <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <textarea required="" class="form-control" id="input-101" name="society_address"><?php echo $data['society_address']; ?></textarea>
                    <?php  if(isset($_POST['society_id_edit'])) { ?>
                    <input  type="hidden" class="form-control" id="lat" name="soiciety_latitude"  placeholder="Lattitude"  value="<?php echo $data['society_latitude']; ?>">
                     <input  type="hidden" class="form-control" id="lng"  name="sociaty_longitude"  placeholder="sociaty_longitude" value="<?php echo $data['society_longitude']; ?>">
                   <?php } else { ?>
                      <input  type="hidden" class="form-control" id="lat" name="soiciety_latitude"  placeholder="Lattitude"  value="23.0242625">
                     <input  type="hidden" class="form-control" id="lng"  name="sociaty_longitude"  placeholder="sociaty_longitude" value="72.5720625">
                   <?php } ?>
                  </div>
                  
                </div>
                <?php  if(!isset($_POST['society_id_edit'])) { ?>
                <div class="form-group row">
                   <label for="secretary_mobile" class="col-sm-2 col-form-label">Secretary Name <span class="required">*</span></label>
                   <div class="col-sm-10">
                      <input type="text"  maxlength="30" value="" required="" class="form-control text-capitalize onlyName" name="admin_name">
                   </div>
                </div>
                <?php } ?>
                <div class="form-group row">
                  <label for="secretary_mobile" class="col-sm-2 col-form-label">Secretary Mobile <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <?php  if(isset($_POST['society_id_edit'])) { ?>
                    <input type="text" id="secretary_mobile" onblur="checkMobileSocietyEdit()" maxlength="12" value="<?php echo $data['secretary_mobile']; ?>" required="" class="form-control" name="secretary_mobile">
                    <input type="hidden" id="secretary_mobile_old"  maxlength="12" value="<?php echo $data['secretary_mobile']; ?>" required="" class="form-control" >
                    <?php } else { ?>
                    <input type="text" id="secretary_mobile" onblur="checkMobileSociety()" maxlength="12" value="<?php echo $data['secretary_mobile']; ?>" required="" class="form-control" name="secretary_mobile">
                    <?php } ?>
                  </div>
                  <label for="input-13" class="col-sm-2 col-form-label">Secretary Email <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <?php  if(isset($_POST['society_id_edit'])) { ?>
                    <input type="email" maxlength="80" id="secretary_email" required="" onblur="checkemailSocietyEdit()" value="<?php echo $data['secretary_email']; ?>" class="form-control" id="input-13" name="secretary_email">
                    <input type="hidden" maxlength="80" id="secretary_email_old" required="" value="<?php echo $data['secretary_email']; ?>" class="form-control" id="input-13" >
                    <?php } else { ?>
                    <input type="email" maxlength="80" id="secretary_email" required="" onblur="checkemailSociety()" value="<?php echo $data['secretary_email']; ?>" class="form-control" id="input-13" name="secretary_email">
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="sub_domain" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?>  Base URL <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="url" id="sub_domain"  maxlength="120" value="<?php echo $data['sub_domain']; ?>" required="" class="form-control" name="sub_domain">
                  </div>
                  <label for="api_key" class="col-sm-2 col-form-label">API Key <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" maxlength="120"  required="" value="<?php echo $data['api_key']; ?>" class="form-control" id="api_key" name="api_key">
                   
                  </div>
                </div>
                <div class="form-group row">
                 <label for="plan_expire_date" class="col-sm-2 col-form-label">Pincode <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" id="society_pincode"  maxlength="6" value="<?php if($data['society_pincode']!='0') { echo $data['society_pincode']; } ?>" required="" class="form-control onlyNumber" inputmode="numeric" name="society_pincode">
                  </div>
                <?php  if(isset($_POST['society_id_edit'])) { ?>
                  <label for="plan_expire_date" class="col-sm-2 col-form-label">Plan Expire Date <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" readonly="" id="default-datepicker"  maxlength="120" value="<?php echo $data['plan_expire_date']; ?>" required="" class="form-control" name="plan_expire_date1">
                  </div>
                  
                <?php } ?>
                </div>
               
               <div class="form-group row">
                  <?php if(isset($_POST['society_id_edit'])) { ?>
                     <input type="hidden" min="0" max="100" maxlength="3" class="form-control" id="trlDays" value="<?php echo $data['trial_days']; ?>" name="trial_days_old">
                     <input type="hidden" min="0" max="100" maxlength="3" class="form-control" id="package_id" value="<?php echo $data['package_id']; ?>" name="package_id">
                     <input type="hidden" class="form-control"  value="<?php echo $data['plan_expire_date']; ?>" name="plan_expire_date_old">
                    <?php if ($data['package_id']==0){?>
                      
                   <label for="input-12" class="col-sm-2 col-form-label">Trial Plan <span class="required">*</span></label>
                  <div class="col-sm-4">
                   
                      <select required="" name="package_id" class="form-control check" id="input-14">
                      <option value="0">Trial</option>
                    </select>
                  </div>
                   <label for="input-13" class="col-sm-2 col-form-label">Trial Days <span class="required">*</span></label>
                    <div class="col-sm-4" id="TrialDiv2">
                      <input type="text" min="0" max="100" maxlength="3" class="form-control" id="trlDays" value="<?php echo $data['trial_days']; ?>" name="trial_days">
                     
                    </div>
                    <?php } } else { ?>
                  <label for="input-12" class="col-sm-2 col-form-label">Plan <span class="required">*</span></label>
                  <div class="col-sm-4">
                   
                      <select required="" name="package_id" class="form-control check" id="input-14">
                      <option value="0">Trial</option>
                      <?php 
                      $i=1;
                      $q=$d->select("package_master","","");
                      while($row=mysqli_fetch_array($q))
                      { ?>
                        <option <?php if($data['package_id']==$row['package_id']) {echo "selected";} ?> value="<?php echo $row['package_id']; ?>"><?php echo $row['package_name']; ?> - <?php echo $row['no_of_month']; ?> Month (<?php echo $row['package_amount']; ?> INR)</option>
                      <?php } ?>
                    </select>
                  </div>
                  
                  <label id="TrialDiv1" for="input-13" class="col-sm-2 col-form-label">Trial Days <span class="required">*</span></label>
                  <div class="col-sm-4" id="TrialDiv2">
                    <input type="text" min="0" max="100" maxlength="3" class="form-control" id="trlDays" value="<?php echo $data['trial_days']; ?>" name="trial_days">
                   
                  </div>
                  <?php } ?>
                 <label id="startDateDiv1" for="input-13" class="col-sm-2 col-form-label">Plan Start Date <span class="required">*</span></label>
                  <div class="col-sm-4" id="startDateDiv2">
                    <input readonly="" value="<?php echo date("Y-m-d"); ?>" type="text"  class="form-control housie_start_date" id="start_date " name="start_date">
                   
                  </div>


                </div>
                <div class="form-group row" id="PaymentDiv">
                   <label for="input-12" class="col-sm-2 col-form-label">Payment Status <span class="required">*</span></label>
                   <div class="col-sm-4">
                     <select class="form-control paymentSelect" name="amountReceivedType">
                       <option value="0">Not Received</option>
                       <option value="1">Received</option>
                     </select>
                   </div>
                   <label for="input-12" id="amoutLable" class="col-sm-2 col-form-label"> Amount <span class="required">*</span></label>
                   <div class="col-sm-4" id="amoutInput">
                     <input type="text" id="working_days" name="amountReceived" class="form-control">
                   </div>
                </div>
                 <div class="form-group row">
                 
                  <label for="soc_type" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> Type <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <select class="form-control single-select" name="society_type" id="soc_type">
                      <option <?php if(isset($_POST['society_id_edit']) && $data['society_type']==0){echo "selected";} ?> value="0">Residential</option>
                      <option <?php if(isset($_POST['society_id_edit']) && $data['society_type']==1){echo "selected";} ?> value="1">Commercial</option>
                    </select>
                  </div>
                </div>

                <h4 class="form-header text-uppercase">
                  <i class="fa fa-user"></i>
                   <?php echo $xml->string->builder_details; ?> Information
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->builder_details; ?> Name </label>
                  <div class="col-sm-10">
                    <input maxlength="50" type="text" value="<?php echo $data['builder_name']; ?>"  class="form-control" id="input-10" name="builder_name">
                  </div>
                  
                </div>
                 <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->builder_details; ?> Address </label>
                  <div class="col-sm-10">
                    <textarea maxlength="200" class="form-control"  id="input-10" name="builder_address"><?php echo $data['builder_address']; ?></textarea>
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->builder_details; ?> Mobile </label>
                  <div class="col-sm-4">
                    <input maxlength="15" type="text" class="form-control" value="<?php echo $data['builder_mobile']; ?>" id="input-12" name="builder_mobile">
                  </div>
                  <label for="input-13" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> Image/Logo </label>
                  <div class="col-sm-4">
                     <input type="file"  class="form-control" id="input-12" name="socieaty_logo">
                  </div>
                </div>
                <div class="form-group row">
                  <input id="searchInput5" class="form-control" type="text" placeholder="Enter a Google location" >
                    <div class="map" id="map" style="width: 100%; height: 400px;"></div>
                </div>
             
                
                <div class="form-footer text-center">
                  <div class="alert alert-warning alert-dismissible" role="alert">
                  <div class="alert-icon contrast-alert">
                   <i class="fa fa-exclamation-triangle"></i>
                  </div>
                  <div class="alert-message">
                    <span><strong>Warning!</strong> Login id & Password send automaticaly on Secretary Email !</span>
                  </div>
                  </div>

                    <button type="submit" id="socAddBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> RESET</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->


<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    // $('#TrialDiv1').hide();
    $('#amoutInput').hide();
    $('#amoutLable').hide();
    $('#PaymentDiv').hide();
    $('#startDateDiv1').hide();
    $('#startDateDiv2').hide();
    $('.check').change(function(){
      var data= $(this).val();
      // alert(data);
       if(data!=0) {
          $('#PaymentDiv').show();
         $('#TrialDiv1').hide();
          $('#TrialDiv2').hide();
       } else {
          $('#TrialDiv1').show();
          $('#PaymentDiv').hide();
          $('#TrialDiv2').show();
       }          
    });

    $('.paymentSelect').change(function(){
      var data= $(this).val();
      // alert(data);
       if(data!=0) {
          $('#amoutInput').show();
          $('#amoutLable').show();
          $('#startDateDiv1').show();
          $('#startDateDiv2').show();
       } else {
          $('#amoutInput').hide();
          $('#amoutLable').hide();
          $('#startDateDiv1').hide();
          $('#startDateDiv2').hide();
       }          
    });
  
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $d->map_key();?>"></script>
<script>

    /* script */
    function initialize() {
       // var latlng = new google.maps.LatLng(23.05669,72.50606);
       

      var latitute =document.getElementById('lat').value;
      var longitute =document.getElementById('lng').value;
      var latlng = new google.maps.LatLng(latitute,longitute);

        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: true,
          anchorPoint: new google.maps.Point(0, -29)
          // icon:'img/direction/'+dirction+'.png'
       });
       var parkingRadition = 5;
        var citymap = {
            newyork: {
              center: {lat: latitute, lng: longitute},
              population: parkingRadition
            }
        };
       
        var input = document.getElementById('searchInput5');
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();   
        autocomplete10.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place5 = autocomplete10.getPlace();
            if (!place5.geometry) {
                window.alert("Autocomplete's returned place5 contains no geometry");
                return;
            }
      
            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport) {
                map.fitBounds(place5.geometry.viewport);
            } else {
                map.setCenter(place5.geometry.location);
                map.setZoom(17);
            }
           
            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);          
            
            var pincode="";
            for (var i = 0; i < place5.address_components.length; i++) {
              for (var j = 0; j < place5.address_components[i].types.length; j++) {
                if (place5.address_components[i].types[j] == "postal_code") {
                  pincode = place5.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
            bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);
           
        });
        // this function will work on marker move event into map 
        google.maps.event.addListener(marker, 'dragend', function() {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) { 
               var places = results[0]       ;
               console.log(places);
               var pincode="";
               var serviceable_area_locality= places.address_components[4].long_name;
               // alert(serviceable_area_locality);
            for (var i = 0; i < places.address_components.length; i++) {
              for (var j = 0; j < places.address_components[i].types.length; j++) {
                if (places.address_components[i].types[j] == "postal_code") {
                  pincode = places.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
                  bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
                  // infowindow.setContent(results[0].formatted_address);
                  // infowindow.open(map, marker);
              }
            }
            });
        });
    }
    function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality){
       // document.getElementById('poi_point_address').value = address;
       document.getElementById('lat').value = lat;
       document.getElementById('lng').value = lng;
       // document.getElementById('collection_center_pincode').value = pin_code;
       // document.getElementById('serviceable_area_locality').value = serviceable_area_locality;

        // document.getElementById("POISubmitBtn").removeAttribute("hidden"); 
        // initialize();
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <!-- For Map 6 -->
