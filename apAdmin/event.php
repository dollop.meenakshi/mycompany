<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
extract($_REQUEST);
$countAdult = 0;
$countChildren = 0;
$countGuests = 0;
if(isset($editEvent))
{
  $q=$d->select("event_master","event_id='$event_id' AND society_id='$society_id'");
  $row=mysqli_fetch_array($q);
  extract($row);

  $blockIds = explode (",", $block_id);
  $floorIds = explode (",", $floor_id);

  $count_event_days = $d->select("event_days_master","event_id='$event_id'");
  $ted = mysqli_num_rows($count_event_days);

  $start_date = date('Y-m-d',strtotime($event_start_date));
  $end_date = date('Y-m-d',strtotime($event_end_date));

  $countAdult = $d->count_data_direct("pass_id","event_passes_master","event_id='$event_id' AND pass_type=0");
  $countChildren = $d->count_data_direct("pass_id","event_passes_master","event_id='$event_id' AND pass_type=1");
  $countGuests = $d->count_data_direct("pass_id","event_passes_master","event_id='$event_id' AND pass_type=2");

  $BookingCheck = $d->count_data_direct("event_attend_id","event_attend_list","event_id='$event_id' AND society_id='$society_id'");

  $paidDayCount = $d->count_data_direct("event_id","event_days_master","event_id='$event_id' AND event_type=1");


  $tb=$d->select("event_attend_list,users_master"," event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' ","ORDER BY event_attend_list.event_attend_id DESC");
   $toatlBooking   = mysqli_num_rows($tb);
}
?>

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="event" action="controller/eventController.php" method="POST" enctype="multipart/form-data"  >
              <h4 class="form-header text-uppercase">
                <i class="fa fa-calendar"></i>
                 Event
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Event Title <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input maxlength="40" type="text" class="form-control" id="input-10" value="<?php echo $event_title; ?>" name="event_title" required onBlur="myButton.enabled = true; return true;">
                </div>
                <label for="input-12" class="col-sm-2 col-form-label">Event Location <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input maxlength="150" required="" type="text" class="form-control"   name="eventMom" id="eventMom" value="<?php echo $eventMom; ?>" required onBlur="myButton.enabled = true; return true;">
                </div>
                <div class="col-sm-4">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Event Description</label>
                <div class="col-sm-10">
                  <textarea maxlength="1000" type="text"  class="form-control" id="input-12" name="event_description"  ><?php echo $event_description; ?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Branch <span class="required">*</span></label>
                <div class="col-sm-4">
                  <select required="" multiple class="form-control multiple-select eventForBranchMultiSelectCls"  name="block_id[]" id="block_id" onchange="getFloorListEvent();" >
                  <?php if ($access_branchs=="") { ?>
                  <option <?php if(isset($editEvent) && $block_id == 0) {echo 'selected';} ?> value="0">All Branch</option>
                    <?php }
                    $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                      while ($blockData=mysqli_fetch_array($bq)) { ?>
                      <option <?php if(isset($blockIds) && in_array($blockData['block_id'], $blockIds)){echo 'selected';} ?> value="<?php echo $blockData['block_id'];?>"><?php echo $blockData['block_name'];?></option>
                    <?php  }?>
                    
                  </select>
                </div>
                <label for="input-12" class="col-sm-2 col-form-label">Department <span class="required">*</span></label>
                <div class="col-sm-4">
                  <select required="" multiple  class="form-control multiple-select eventForDepartmentMultiSelectCls"  name="floor_id[]" id="floor_id">
                  <option <?php if(isset($editEvent) && $floor_id == 0) {echo 'selected';} ?> value="0">All Department</option>
                    <?php 
                    $block_id = explode (",", $block_id); 
                    $block_id = implode ("','", $block_id);
                    $fq=$d->select("floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.society_id='$society_id' AND block_master.block_id IN ('$block_id') $blockAppendQuery");
                      while ($floorData=mysqli_fetch_array($fq)) { ?>
                      <option <?php if(in_array($floorData['floor_id'], $floorIds)){echo 'selected';} ?> value="<?php echo $floorData['floor_id'];?>"><?php echo $floorData['floor_name'].' ('.$floorData['block_name'].')';?></option>
                    <?php  }?> 
                    
                  </select>
                </div>
                <div class="col-sm-4">
                </div>
              </div>
              
              <!-- <div class="form-group row">
                <?php if(isset($editEvent) && $toatlBooking>0){ ?>
                <label for="input-14" class="col-sm-2 col-form-label">Event Start Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text"  readonly="" class="form-control"   value="<?php echo $start_date; ?>"  name="event_start_time"  >
                </div>
                <label for="input-14" class="col-sm-2 col-form-label">Event End Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" readonly="" class="form-control"  value="<?php echo $end_date; ?>"  name="event_end_time" >
                </div>
                <div class="col-sm-12 offset-2">
                  Note: Event date cannot be editable, due to some user booked this event on this date
                </div>
                <?php } else { ?>
                 <label for="input-14" class="col-sm-2 col-form-label">Event Start Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text"  readonly="" class="form-control event_start_time" id="event-start-date"  value="<?php echo $start_date; ?>"  name="event_start_time"  >
                </div>
                <label for="input-14" class="col-sm-2 col-form-label">Event End Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" readonly="" class="form-control event_end_time" id="event-end-date" value="<?php echo $end_date; ?>"  name="event_end_time" >
                </div>
                <?php } ?>
              </div> -->
              <div class="form-group row">
              <?php if(isset($editEvent)){ ?>
                <input type="hidden" name="total_days" value="<?php if(isset($editEvent)){ echo $ted; } ?>">
                <?php } ?>
                <label for="total_days" class="col-sm-2 col-form-label">Total Days <span class="required">*</span></label>
                <div class="col-sm-4">
                  <?php  $days = $d->select_row("event_attend_list","event_id='$event_id'"); 
                        while($daysc = $days->fetch_assoc()){
                          $day_count = $daysc['num_rows'];
                        }  ?>
                  <select name="total_days" required <?php if(isset($editEvent)){echo 'disabled';} ?> id="total_days" class="form-control single-select">
                    <option value="">-- Select --</option>
                    <?php
                   
                    for($i=1; $i<=31; $i++)
                    {   
                        ?><option  value="<?php echo $i; ?>" <?php if(isset($editEvent) && $ted == $i){ echo "selected"; } ?>><?php echo $i; ?></option>  <?php
                    }                   
                    ?>                  
                  </select>                 
                </div>
                <?php
                if(isset($editEvent))
                {
                ?>
                <!-- <label for="event_day_name" class="col-sm-2 col-form-label">Send Notification <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <select name="send_notification_to" required id="send_notification_to" class="form-control single-select">
                      <option value="">-- Select --</option>
                      <option value="1">To All</option>
                      <option value="2">Users who have booked event</option>
                      <option value="3">No One</option>
                    </select>
                  </div> -->
                <?php
                }
                ?>
              </div>
              <div class="form-group row">
                <label for="input-14" class="col-sm-2 col-form-label">Event Photo </label>
                <div class="col-sm-4">
                  <?php //IS_639 id="event_image" ?>
                  <input accept="image/*" type="file" class="form-control-file border photoOnly"  value="<?php echo $event_image; ?>" name="event_image"  id="event_image"  >
                  <span   >Max Size upto 3MB</span>
                </div>
                <label  <?php if( isset($editEvent) &&  $is_taxble != "" && $paidDayCount>0){ ?>  <?php }  else { ?> style="display: none" <?php } ?> id="billTypeLble" for="is_taxble" class="col-sm-2 col-form-label">Bill Type <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                      <select <?php if ($toatlBooking>0) { echo 'disabled'; } ?> required=""   class="form-control  " name="is_taxble" id="is_taxble"   <?php if( isset($editEvent) &&  $is_taxble != "" && $paidDayCount>0){ ?>  <?php }  else { ?> style="display: none" <?php } ?> >
                      <option value="">-- Select --</option>
                      <?php include 'billTypeOptionList.php'; ?>
                    </select>
                   <?php  if ($toatlBooking>0) { ?>
                        <input type="hidden" value="<?php echo $is_taxble;?>" name="is_taxble">
                      <?php } ?>
                  </div>
              </div>
                <div  <?php
                 if( isset($editEvent) &&  $is_taxble != "0" && $is_taxble != "" && $paidDayCount>0){ ?>  <?php }  else { ?> style="display: none" <?php } ?>    id="gst_detail_div" >

                  <div class="form-group row">
                  <label for="gst" class="col-sm-2 col-form-label">Tax Amount </label>
                   <div class="col-sm-4">
                    <div class=" icheck-inline">
                      <input <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  <?php if(isset($editEvent) && $gst=="0") { echo "checked"; } else if(!isset($editEvent)) { echo "checked";} ?>    type="radio"  id="inline-radio-info" value="0" name="gst">
                      <label for="inline-radio-info">Included</label>
                    </div>
                    <div class=" icheck-inline">
                      <input <?php if ($toatlBooking>0) { echo 'disabled'; } ?> <?php if(isset($editEvent) && $gst=="1") { echo "checked"; }  ?>    type="radio" id="inline-radio-success" value="1" name="gst">
                      <label for="inline-radio-success">Excluded</label>
                    </div>
                  </div>
                  <?php  if ($toatlBooking>0) { ?>
                    <input type="hidden" value="<?php echo $gst;?>" name="gst">
                  <?php } ?>
                  <label for="taxble_type" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_type; ?> </label>
                  <div class="col-sm-4">
                     <select    class="form-control  " name="taxble_type" id="taxble_type">
                      <option value="">-- Select --</option>
                      <?php include 'taxOptionList.php'; ?>
                    </select>
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="tax_slab" class="col-sm-2 col-form-label">Tax Value <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <?php
                          $q=$d->select("gst_master","status='0'","");
                          ?>
                       <select <?php if ($toatlBooking>0) { echo 'disabled'; } ?> <?php if(mysqli_num_rows($q)){ echo 'required=""'; } ?>  class="form-control  " name="tax_slab">
                      <option value="">-- Select --</option>
                        <?php
                           while ($row=mysqli_fetch_array($q)) {
                         ?>
                          <option <?php if(isset($editEvent) && $tax_slab==$row['slab_percentage']) { echo "selected"; }  ?>  title="<?php echo $row['description'];?>" value="<?php echo $row['slab_percentage'];?>"><?php echo $row['slab_percentage'];?>%</option>
                          <?php }?>
                    </select>
                     <?php  if ($toatlBooking>0) { ?>
                          <input type="hidden" value="<?php echo $tax_slab;?>" name="tax_slab">
                        <?php } ?>
                  </div>
                 </div>
                 </div>
                <?php //IS_1470 ?>

              <div id="event_days_detail" >

                <?php if(isset($editEvent)) {

                  $event_days_master=$d->select("event_days_master","event_id ='$event_id' AND society_id='$society_id'");
                $event_start_time = strtotime($event_start_date); // or your date as well
$event_end_time = strtotime($event_end_date. ' +1 day');
$datediff = $event_end_time - $event_start_time;
$days =  round($datediff / (60 * 60 * 24));
?>
                  <input type="hidden" name="event_days" value="<?php echo ($days); ?>">
<?php
$day=0;
 while($event_days_master_result = mysqli_fetch_array($event_days_master)){
  $tbd=$d->select("event_attend_list,users_master","event_attend_list.book_status=1 AND event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' AND  event_attend_list.events_day_id='$event_days_master_result[events_day_id]'","ORDER BY event_attend_list.event_attend_id DESC");
   $toatlBookingDayWise   = mysqli_num_rows($tbd);
   $aq = $d->selectRow("SUM(going_person) as sum_adult, SUM(going_child) as sum_child, SUM(going_guest) as sum_guest","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$event_days_master_result[events_day_id]' AND book_status=1");
   $aData = mysqli_fetch_array($aq);
   $bookAdult= $aData['sum_adult'];
   $bookChild= $aData['sum_child'];
   $bookGuest= $aData['sum_guest'];
   
    if ($bookAdult=='') {
      $bookAdult=0;
    }
    if ($bookChild=='') {
      $bookChild=0;
    }
    if ($bookGuest=='') {
      $bookGuest=0;
    }
  // echo  $event_days_master_result[events_day_id];
//for ($day=0; $day < $dayays ; $day++) {
 if ($is_taxble=='1' && $gst==1) {
  $gst_amount =  $event_days_master_result['adult_charge'] - ($event_days_master_result['adult_charge'] * (100/(100+$tax_slab)));  //only for Include
  $adult_charge = $event_days_master_result['adult_charge']-$gst_amount;
  $adult_charge_new =number_format($adult_charge,2,'.','');

  $gst_amount =  $event_days_master_result['child_charge'] - ($event_days_master_result['child_charge'] * (100/(100+$tax_slab)));  //only for Include
  $child_charge = $event_days_master_result['child_charge']-$gst_amount;
  $child_charge_new =number_format($child_charge,2,'.','');

  $gst_amount =  $event_days_master_result['guest_charge'] - ($event_days_master_result['guest_charge'] * (100/(100+$tax_slab)));  //only for Include
  $guest_charge = $event_days_master_result['guest_charge']-$gst_amount;
  $guest_charge_new = number_format($guest_charge,2,'.','');

  $gst_amount =  $event_days_master_result['adult_charge_tenant'] - ($event_days_master_result['adult_charge_tenant'] * (100/(100+$tax_slab)));  //only for Include
  $adult_charge_tenant = $event_days_master_result['adult_charge_tenant']-$gst_amount;
  $adult_charge_tenant_new =  number_format($adult_charge_tenant,2,'.','');

  $gst_amount =  $event_days_master_result['child_charge_tenant'] - ($event_days_master_result['child_charge_tenant'] * (100/(100+$tax_slab)));  //only for Include
  $child_charge_tenant = $event_days_master_result['child_charge_tenant']-$gst_amount;
  $child_charge_tenant_new = number_format($child_charge_tenant,2,'.','');

  $gst_amount =  $event_days_master_result['guest_charge_tenant'] - ($event_days_master_result['guest_charge_tenant'] * (100/(100+$tax_slab)));  //only for Include
  $guest_charge_tenant = $event_days_master_result['guest_charge_tenant']-$gst_amount;
  $guest_charge_tenant_new = number_format($guest_charge_tenant,2,'.','');
 } else {
  $adult_charge_new = $event_days_master_result['adult_charge'];
  $child_charge_new = $event_days_master_result['child_charge'];
  $guest_charge_new = $event_days_master_result['guest_charge'];
  $adult_charge_tenant_new = $event_days_master_result['adult_charge_tenant'];
  $child_charge_tenant_new = $event_days_master_result['child_charge_tenant'];
  $guest_charge_tenant_new = $event_days_master_result['guest_charge_tenant'];
 }
$maximum_pass_adult_new = $event_days_master_result['maximum_pass_adult'];
$maximum_pass_children_new = $event_days_master_result['maximum_pass_children'];
$maximum_pass_guests_new = $event_days_master_result['maximum_pass_guests'];
$maximum_user_pass_adult_new = $event_days_master_result['maximum_user_pass_adult'];
$maximum_user_pass_children_new = $event_days_master_result['maximum_user_pass_children'];
$maximum_user_pass_guests_new = $event_days_master_result['maximum_user_pass_guests'];
$balancesheet_id_new = $event_days_master_result['balancesheet_id'];
$event_time_new = $event_days_master_result['event_time'];
$events_day_id = $event_days_master_result['events_day_id'];
$eventDate = $event_days_master_result['event_date'];
?>
<h5 class="text-primary text-uppercase">Day <?php echo ($day+1);?> (<?php echo $event_days_master_result['event_date'];?>)</h5>
<div class="form-group row">
  <label for="event_day_name" class="col-sm-2 col-form-label">Event Date <span class="required">*</span></label>
  <div class="col-sm-4">
    <input required type="text" readonly class="form-control" id="<?php echo $day+1; ?>" value="<?php echo $event_days_master_result['event_date'];?>" name="event_date_day[<?php echo $day+1; ?>]">
  </div>
  <label for="event_day_name" class="col-sm-2 col-form-label">Event Day Name <span class="required">*</span></label>
  <div class="col-sm-4">
    <input maxlength="30" required=""  type="text" class="form-control  event-day-name "   name="event_day_name[]"  id="event_day_name_<?php echo $day;?>"  value="<?php echo $event_days_master_result['event_day_name'];?>">
  </div>
</div>
<div class="form-group row">
              <input type="hidden" name="events_day_id[]" value='<?php echo $events_day_id;?>'>
              <label for="state" class="col-sm-2 col-form-label">Event Allow <span class="required">*</span></label>
              <div class="col-sm-4">
                <div class="form-check-inline">
                  <label class="form-check-label">
                    <input <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  type="radio" <?php  if($event_days_master_result['event_allow']==0){ echo "checked"; }?> class="form-check-input" value="0" name="event_allow[<?php echo $day;?>]" id="event_allow_only_emp<?php echo $day;?>" onclick="showAllowInfo('only_employee','<?php echo $day;?>');"> Only Employee
                  </label>
                </div>
                <div class="form-check-inline">
                  <label class="form-check-label">
                    <input <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  type="radio" <?php  if($event_days_master_result['event_allow']==1){ echo "checked"; }?> class="form-check-input" value="1" name="event_allow[<?php echo $day;?>]" id="event_allow_with_guest<?php echo $day;?>" onclick="showAllowInfo('employee_with_guest','<?php echo $day;?>');"> Employee With Guest
                  </label> 
                </div>
              </div>
                  <label for="state" class="col-sm-2 col-form-label">Event Type <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  type="radio"  <?php  if($event_days_master_result['event_type']=="0"){ echo "checked"; }?> class="form-check-input" value="0" name="event_type_day[<?php echo $day;?>]" id="event_typeUnpaid<?php echo $day;?>" onclick="showPaidInfo('Unpaid','<?php echo $day;?>');"> Unpaid
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input  <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  <?php  if($event_days_master_result['event_type']=="1"){ echo "checked"; }?>  type="radio" class="form-check-input paid-event" value="1" name="event_type_day[<?php echo $day;?>]" id="<?php echo $day;?>" onclick="showPaidInfo('paid','<?php echo $day;?>');"> Paid
                      </label>
                    </div>
                  </div>
                   <?php if ($toatlBooking>0) { ?>
                    <input type="hidden" name="event_type_day[<?php echo $day;?>]" value="<?php echo $event_days_master_result['event_type'];?>">
                   <?php } ?>
             </div>
              <div class="form-group row">
                  <label for="maximum_pass" class="col-sm-2 col-form-label">Maximum Allowed <span class="required">*</span></label>
                  <div class="col-sm-2">Employee
                    <input maxlength="6" type="text" class="form-control onlyNumber max-allow" inputmode="numeric" id="maximum_pass_adult<?php echo $day;?>" name="maximum_pass_adult[]"  required min="<?php echo $bookAdult; ?>" placeholder="Employee"
                    value="<?php echo $maximum_pass_adult_new;?>"   >
                  </div>
                  <div class="col-sm-2 event_allow_with_guest_<?php echo $day;?> <?php if($event_days_master_result['event_allow']==0){ echo "d-none"; }?>">Child
                    <input maxlength="6" type="text" class="form-control onlyNumber" inputmode="numeric" id="maximum_pass_children<?php echo $day;?>"  name="maximum_pass_children[]" required min="<?php echo $bookChild; ?>" placeholder="Childern" value="<?php echo $maximum_pass_children_new;?>">
                  </div>
                  <div class="col-sm-2 event_allow_with_guest_<?php echo $day;?> <?php if($event_days_master_result['event_allow']==0){ echo "d-none"; }?>">Guest
                    <input maxlength="6" type="text" class="form-control onlyNumber" inputmode="numeric" id="maximum_pass_guests<?php echo $day;?>"  name="maximum_pass_guests[]" required min="<?php echo $bookGuest; ?>" placeholder="Guests" value="<?php echo $maximum_pass_guests_new;?>">
                  </div>
                  <div class="col-sm-4">Start Time
                   <!--  <input value="<?php echo $event_time_new;?>" maxlength="8" type="text" class="form-control event-start-time" id="event_time<?php echo $day;?>" name="event_time[]" required placeholder="Start Time"> -->
                    <select maxlength="8" type="text" class="form-control" id="event_time<?php echo $day;?>" value="" name="event_time[]" required placeholder="Start Time">
                      <option value="">-- Select Time --</option>
                      <?php
                       $hourFormat = 'H:i';
                        $minutesInterval = new DateInterval('PT15M');
                        $tNow = new DateTime();
                        $tStart = new DateTime();
                        $tStart->setTime(00, 00);
                        $tEnd = new DateTime();
                        $tEnd->setTime(23, 45);
                        $tCount = clone $tStart;
                        while($tCount <= $tEnd){
                          $attribute = "";
                          $dateFormatted = $tCount->format($hourFormat);
                        ?>
                        <option <?php if($event_time_new==$dateFormatted) { echo 'selected';} ?> value="<?php echo $dateFormatted; ?>" <?php echo $attribute; ?>>
                            <?php echo $dateFormatted; ?>
                        </option>
                        <?php
                            $tCount->add($minutesInterval);
                        } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row event_allow_with_guest_<?php echo $day;?> <?php if($event_days_master_result['event_allow']==0){ echo "d-none"; }?>">
                  <label for="maximum_pass" class="col-sm-2 col-form-label">Maximum Booking Per Employee</label>
                  <div class="col-sm-2">
                    <!-- <input maxlength="6" type="text" class="form-control onlyNumber" inputmode="numeric" id="maximum_user_pass_adult<?php echo $day;?>" name="maximum_user_pass_adult[]" onchange="adultMaximum(<?php echo $day;?>)" required  placeholder="Maximum Employee Booking Per User"
                    value="<?php echo $maximum_user_pass_adult_new;?>"   > -->
                  </div>
                  <div class="col-sm-2">
                    <input maxlength="6" type="text" class="form-control onlyNumber" inputmode="numeric" id="maximum_user_pass_children<?php echo $day;?>"  name="maximum_user_pass_children[]" onchange="childMaximum(<?php echo $day;?>)" required placeholder="Max Child Per Employee" value="<?php echo $maximum_user_pass_children_new;?>">
                  </div>
                  <div class="col-sm-2">
                    <input maxlength="6" type="text" class="form-control onlyNumber" inputmode="numeric" id="maximum_user_pass_guests<?php echo $day;?>"  name="maximum_user_pass_guests[]" onchange="guestMaximum(<?php echo $day;?>)" required placeholder="Max Guest Per Employee" value="<?php echo $maximum_user_pass_guests_new;?>">
                  </div>
                  <div class="col-sm-4">
                   
                  </div>
                </div>
          <div class="eventPrice-days<?php echo $day;?>" <?php if($event_days_master_result['event_type'] =="0" ){ ?> style="display: none" <?php } ?> >
                  <div class="form-group row">
                    <label for="adult_charge" class="col-sm-2 col-form-label">Price <span class="required">*</span></label>
                    <div class="col-sm-2">
                      <input maxlength="7" type="text" class="form-control onlyNumber paid-price" inputmode="numeric" id="adult_charge<?php echo $day;?>"  name="adult_charge[<?php echo $day;?>]" placeholder="Adult Price *" required="" value="<?php echo $adult_charge_new;?>">
                    </div>
                    <div class="col-sm-2 event_allow_with_guest_<?php echo $day;?> <?php if($event_days_master_result['event_allow']==0){ echo "d-none"; }?>">
                      <input maxlength="7" type="text" class="form-control onlyNumber" inputmode="numeric" placeholder="Child Price *" id="child_charge<?php echo $day;?>"  name="child_charge[<?php echo $day;?>]" required="" value="<?php echo $child_charge_new;?>">
                    </div>
                    <div class="col-sm-2 event_allow_with_guest_<?php echo $day;?> <?php if($event_days_master_result['event_allow']==0){ echo "d-none"; }?>">
                      <input maxlength="7" required="" type="text" class="form-control onlyNumber" inputmode="numeric" placeholder="Guest Price *" id="guest_charge<?php echo $day;?>" name="guest_charge[<?php echo $day;?>]" value="<?php echo $guest_charge_new;?>" >
                    </div>
                      <div class="col-sm-4">
                         <select type="text" required="" class="form-control" id="balancesheet_id<?php echo $day;?>" name="balancesheet_id[<?php echo $day;?>]">
                          <option value="">Select Balance Sheet</option>
                          <?php
                           $q11=$d->select("balancesheet_master","society_id='$society_id'","");
                            while ($row11=mysqli_fetch_array($q11)) {
                               if ($adminData['admin_type']==1 || $row11['block_id']==0 ||  in_array($row11['block_id'], $blockAryAccess)) {
                              ?>
                              <option  <?php if(isset($editEvent) && $balancesheet_id_new==$row11['balancesheet_id']) { echo "selected"; }  ?>  value="<?php echo $row11['balancesheet_id'];?>"><?php echo $row11['balancesheet_name'];?></option>
                            <?php } }?>
                        </select>
                    </div>
                  </div>
                </div>
      <?php $day++;
      }  ?>
                  <div class="col-sm-12 text-center text-uppercase text-secondary ">
                    Note:Set Maximum Allowed 0 , if not allowed<br>
                  </div>
                <?php } ?>
              </div>
                    <input type="hidden" id="minAdult" value="<?php echo $countAdult ?>">
                    <input type="hidden" id="minChildren" value="<?php echo $countChildren ?>">
                    <input type="hidden" id="minGuests" value="<?php echo $countGuests ?>">
              <?php //if(isset($editEvent) && $event_type==1) {} else {}?>
              <div class="form-footer text-center">
                <?php if(isset($editEvent)) { ?>
                  <input type="hidden" class="form-control-file border"  value="<?php echo $event_image; ?>" name="event_image_old" required>
                  <input type="hidden" value="editevent" name="editevent">
                  <input type="hidden" name="event_id" value="<?php echo ($event_id) ?>">
                  <input type="submit" value="update event" class="btn btn-success">
                <?php } else  { ?>
                  <input type="hidden" value="addevent" name="addevent">
                  <button type="submit" name="myButton" id="myButton" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                  <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> RESET</button>
                <?php } ?>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //IS_804 ?>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
  $( "#event" ).submit(function( event ) {
    $("#myButton").attr("disabled", true);
    window.setTimeout(setDisabled, 10000);
  });
  function setDisabled() {
    document.getElementById('myButton').disabled = false;
  }


  var start_date = new Date();
  start_date.setDate(start_date.getDate());

  $('.event_day_date').datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: start_date,
    // datesDisabled: response.dates,
    format: 'yyyy-mm-dd'
  });

$('.event_day_date').change(function(event)
{
  var date = $(this).val();
  var strtDt  = new Date(date);
  var name = event.target.name;
  var id = event.target.id;
  var i = 0;
  $('.event_day_date').each(function(index, item)
  {
    console.log(item);

    if($(item).val() == date)
    {
      i++;
      if(i == 2)
      {
        swal("Error! Date Already Selected! Select Another Date", {icon: "error",});
        $('input[name="'+name+'"]').val('');
      }
    }
    if(id < this.id)
    {
      var endDt  = new Date($(item).val());
      if(strtDt > endDt)
      {
        swal("Error! Please select less date", {icon: "error",});
        $('input[name="'+name+'"]').val('');
      }
    }
    else
    {
      var endDt  = new Date($(item).val());
      if(strtDt < endDt)
      {
        swal("Error! Please select greater date", {icon: "error",});
        $('input[name="'+name+'"]').val('');
      }
    }
  });
  $(this).valid();
});

function adultMaximum(id){
  adult = $('#maximum_pass_adult'+id).val();
  allowed_adult = $('#maximum_user_pass_adult'+id).val();
  if(parseInt(allowed_adult)>parseInt(adult)){
    swal('Error! Please Enter less or equal value', {icon: "error",});
    $('#maximum_user_pass_adult'+id).val('');
  }
}

function childMaximum(id){
  children = $('#maximum_pass_children'+id).val();
  allowed_children = $('#maximum_user_pass_children'+id).val();
  if(parseInt(allowed_children)>parseInt(children)){
    swal('Error! Please Enter less or equal value', {icon: "error",});
    $('#maximum_user_pass_children'+id).val('');
  }
}

function guestMaximum(id){
  guest = $('#maximum_pass_guests'+id).val();
  allowed_guest = $('#maximum_user_pass_guests'+id).val();
  if(parseInt(allowed_guest)>parseInt(guest)){
    swal('Error! Please Enter less or equal value', {icon: "error",});
    $('#maximum_user_pass_guests'+id).val('');
  }
}

function showAllowInfo(allow,id){
  if(allow == "employee_with_guest"){
    $('.event_allow_with_guest_'+id).removeClass('d-none')
    $('.hideAlrtMsg').show();
  }else{
    $('.event_allow_with_guest_'+id).addClass('d-none');
    $('.hideAlrtMsg').hide();
   
  }
}
</script>