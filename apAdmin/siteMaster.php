  <?php error_reporting(0);

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Sites Master</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">

          <a href="addSiteMaster" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">

              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sr.No</th>
                    <th>Site Name</th>
                    <th>Date</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody id="showFilterData">
                  <?php
                  $i = 1;
                  $q = $d->select("site_master", "site_master.society_id='$society_id' AND site_master.site_delete= 0");
                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                  ?>
                    <tr>
                      <td class="text-center">
                        <input type="hidden" name="id" id="id" value="<?php echo $data['site_id']; ?>">
                        <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['site_id']; ?>">
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['site_name']; ?></td>
                      <td><?php echo date("d M Y h:i A", strtotime($data['site_created_date'])); ?></td>
                      <td>
                        <div class="d-flex align-items-center">
                          <form method="post" accept-charset="utf-8">
                            <input type="hidden" name="site_id" value="<?php echo $data['site_id']; ?>">
                            <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">

                            <a href="addSiteMaster?sId=<?php echo $data['site_id']; ?>"  class="btn btn-sm btn-primary mr-1"> <i class="fa fa-pencil"></i></a>
                          </form>
                          <?php if ($data['site_active_status'] == "0") {
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['site_id']; ?>','siteStatusDeactive');" data-size="small" />
                          <?php } else { ?>
                            <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['site_id']; ?>','siteStatusActive');" data-size="small" />
                          <?php } ?>
                          <button type="button" class="btn btn-sm btn-primary ml-1" onclick="siteMasterShowDetails(<?php echo $data['site_id']; ?>)" data-toggle="modal" data-target="#siteDetailModel"><i class="fa fa-eye"></i></button>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  <!--Start Back To Top Button-->
  <script type="text/javascript">
    function vieComplain(complain_id) {
      $.ajax({
        url: "getComplaineDetails.php",
        cache: false,
        type: "POST",
        data: {
          complain_id: complain_id
        },
        success: function(response) {
          $('#comResp').html(response);


        }
      });
    }
  </script>




  <div class="modal fade" id="siteDetailModel">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Site Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="" id="siteDetailModelDiv"></div>
          <div class="col-lg-12">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">

                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Manager Name</th>
                        <th>Type</th>
                      </tr>
                    </thead>
                    <tbody id="siteMasterData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="addModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Site</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <form id="addSiteForm" action="controller/siteController.php" enctype="multipart/form-data" method="post">
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Site Name <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <input type="text" required="" name="site_name" id="site_name" class="form-control restInput ">
                </div>
                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Branch <span class="required">*</span></label>
                <div class=" col-lg-4 col-md-4" id="">
                  <select name="br_id" id="br_id" class="form-control single-select updateFrom " onchange="getSiteManagerFloorByBLock(this.value)">
                    <option value="">Select Branch</option>
                    <?php
                    $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                    while ($blockData = mysqli_fetch_array($qb)) {
                    ?>
                      <option <?php if ($bId == $blockData['block_id']) {
                                echo 'selected';
                              } ?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
                    <?php } ?>

                  </select>
                </div>

              </div>
              <div class="form-group row ">
                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Department <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <select name="floor_id" id="floor_id" class="form-control single-select updateFrom" onchange="getUserByFloorId(this.value)">
                    <option value="">All Department</option>

                  </select>
                </div>
                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Manager <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <select name="site_manager_id[]" id="user_id" multiple class="form-control updateFrom multiple-select">
                    <option>--select--</option>
                    <?php
                    /* $qd=$d->select("users_master","society_id='$society_id'");  
                              while ($userData=mysqli_fetch_array($qd)) { */
                    ?>
                    <!--  <option value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option> -->
                    <?php //} 
                    ?>

                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Finance <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <select name="finance_manager_id[]" multiple class="form-control uId updateFrom multiple-select">
                    <option>--select--</option>
                    <?php
                    /* $qd=$d->select("users_master","society_id='$society_id'");  
                                    while ($userData=mysqli_fetch_array($qd)) { */
                    ?>
                    <!--  <option value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option> -->
                    <?php //} 
                    ?>
                  </select>
                </div>
                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Procurement <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <select name="procurement_id[]" multiple class="form-control uId updateFrom multiple-select">
                    <option>--select--</option>
                    <?php
                    /* $qd=$d->select("users_master","society_id='$society_id'");  
                                    while ($userData=mysqli_fetch_array($qd)) { */
                    ?>
                    <!--  <option value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option> -->
                    <?php //} 
                    ?>

                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Site Address <span class="required">*</span></label>
                <div class="col-lg-4 col-md-4" id="">
                  <textarea required="" name="site_address" id="site_address" class="form-control restInput "></textarea>
                </div>
                <label for="input-10" class="col-sm-2 col-form-label">Site Discription</label>
                <div class="col-lg-4 col-md-4" id="">
                  <textarea name="site_description" id="site_description" class="form-control restInput"></textarea>
                </div>
              </div>

              <div class="form-footer text-center">
                <input type="hidden" id="site_id" name="site_id" value="">
                <input type="hidden" id="user_id_old" name="user_id_old" value="">
                <input type="hidden" id="floor_id_old" name="floor_id_old" value="">
                <button id="addSiteBtn" name="addSiteBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                <input type="hidden" name="addSite" value="addSite">

                <button id="addSiteBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

                <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addSiteForm');"><i class="fa fa-check-square-o"></i> Reset</button>

              </div>

            </form>

          </div>
        </div>

      </div>
    </div>
  </div>

  <script type="text/javascript">
    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }

    function buttonSettingForSIte() {
      $('#site_name').val('');
      $('.hideAdd').show('');
      $('.hideupdate').hide('');
      $('#br_id').val('');
      $('#floor_id').val('');
      $('.updateFrom').val('');
      $('.restInput').val('');
      $('.updateFrom').select2();

      $('#hr_document_category_id').attr("disabled", false);
    }
  </script>
  <style>
    .hideupdate {
      display: none;
    }
  </style>