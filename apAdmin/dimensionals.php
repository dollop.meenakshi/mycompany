<?php
$usq = $d->select("dimensional_master", "society_id='$society_id'");

?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-lg-12">
                <h4 class="page-title">Dimensionals</h4>
            </div>

        </div>
        <div class="row">
            <div class="card col-lg-12">
                <div class="card-body">
                    <form id="addDimensionalForm" action="controller/DimensionalController.php" method="post" enctype="multipart/form-data">
                        <?php
                        $usCount = mysqli_num_rows($usq);
                        if ($usCount > 0) {
                            $dimensionalWeightage = 0;
                            $i = 0;
                            while ($usData = mysqli_fetch_array($usq)) {
                                $dimensional_weightage += $usData['dimensional_weightage'];
                                
                                ?>
                                <div>
                                    <?php if ($i > 0) { ?>
                                        <hr>
                                    <?php } ?>
                                    <?php  
                                        $totalAssign = $d->count_data_direct("attribute_id","attribute_master","dimensional_id='$usData[dimensional_id]'");
                                        if( $totalAssign==0) { ?>
                                        <div class="form-group row">
                                            <div class="col-lg-12 text-right">
                                                <input type="checkbox" id="checkbox_<?php echo $usData['dimensional_id']; ?>" onclick="checkedDelete('<?php echo $usData['dimensional_id']; ?>');" name="deletedIds[]" value="<?php echo $usData['dimensional_id']; ?>"> <label for="input-14"> Remove This Dimensional </label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group row">
                                        <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Name <?php echo $i + 1; ?> <i class="text-danger">*</i></label>
                                        <div class="col-lg-9">
                                            <input type="hidden" name="dimensional_id[]" value="<?php echo $usData['dimensional_id']; ?>" id="removeDimensionalId_<?php echo $usData['dimensional_id']; ?>">
                                            <input type="text" required="" class="form-control" id="removeDimensionalName_<?php echo $usData['dimensional_id']; ?>" name="dimensional_name[]" value="<?php echo $usData['dimensional_name']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Weightage % <i class="text-danger">*</i></label>
                                        <div class="col-lg-3">
                                            <input max="100" maxlength="3" min="1" type="text" required="" class="form-control onlyNumber onStepPercent" name="dimensional_weightage[]" value="<?php echo $usData['dimensional_weightage']; ?>" id="removeDimensional_<?php echo $usData['dimensional_id']; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Image </label>
                                    <div class="col-lg-3">
                                        <input type="file" class="form-control photoOnly" accept="image/*" id="removeDimensionalImage_<?php echo $usData['dimensional_id']; ?>" name="dimensional_image[]">
                                        <input type="hidden" id="removeDimensionalImageOld_<?php echo $usData['dimensional_id']; ?>" name="dimensional_image_old[]" value="<?php echo $usData['dimensional_image']; ?>">
                                    </div>
                                </div>
                                <?php if ($usData['dimensional_image'] != "") {   ?>
                                    <div class="form-group row">
                                        <div class="col-lg-3">
                                            <a href="../img/dimensionals/<?php echo $usData['dimensional_image']; ?>" data-fancybox="images" data-caption="Dimensional Image : <?php echo $usData['dimensional_name']; ?>">
                                                <img class="lazyload" src="<?php if (file_exists("../img/dimensionals/$usData[dimensional_image]")) 
                                                {
                                                    echo '../img/dimensionals/' . $usData['dimensional_image'];
                                                } else {
                                                    echo '../img/user.png';
                                                } ?>" width="100" height="100">
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>

                            <?php $i++;
                            } ?>
                            <div class="field_wrapper">
                            </div>
                            <span id="" class="error totalDimensionalWeightageMsg"></span>
                            <div class="form-footer text-center">
                                <input type="hidden" id="totalOldDataDimensional" value="<?php echo mysqli_num_rows($usq); ?>" name="">
                                <input type="hidden" name="addDimensionals" value="yes">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> update </button>
                                <a href="javascript:void(0);" class="add_button float-right btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add More Dimensional</a>
                            </div>
                         <?php
                        } else { ?>
                            <div class="field_wrapper">
                                <div class="form-group row">
                                    <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Name 1<i class="text-danger">*</i></label>
                                    <div class="col-lg-9">
                                        <input type="text" required="" class="form-control" name="dimensional_name[0]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Weightage  % <i class="text-danger">*</i></label>
                                    <div class="col-lg-3">
                                        <input max="100" maxlength="3" min="1" type="text" required="" class="form-control onlyNumber onStepPercent" name="dimensional_weightage[]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Image </label>
                                    <div class="col-lg-3">
                                        <input type="file" accept="image/*" class="form-control photoOnly" name="dimensional_image[0]">
                                    </div>
                                </div>
                            </div>
                            <span id="" class="error totalDimensionalWeightageMsg"></span>
                            <div class="form-footer text-center">
                                <input type="hidden" name="addDimensionals" value="yes">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save </button>
                                <a href="javascript:void(0);" class="add_button float-right btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add More Dimensionals</a>
                            </div>
                        <?php } ?>
                        <input type="hidden" id="totalDimensionalWeightage" name="totalDimensionalWeightage" value="<?php echo $dimensional_weightage; ?>">
                    </form>
                </div>
            </div>

        </div><!-- End Row-->
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var maxField = 50; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var x = 1; //Initial field counter is 1
        var oldEntry = parseInt($('#totalOldDataDimensional').val());
        if (oldEntry > 0) {
            var x1 = oldEntry + 1; //Initial field counter is 1
        } else {
            var x1 = x + 1; //Initial field counter is 1
        }


        var fieldHTML = `<div class="">
                          <hr>
                          <div class="form-group row">
                            <div class="col-lg-12">
                              <div class="btn-group float-sm-right">
                                <a href="javascript:void(0);" class="remove_button float-right btn btn-sm btn-danger">Remove</a>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Name ` + x1 + `<i class="text-danger">*</i></label>
                            <div class="col-lg-9">
                              <input type="text" required="" class="form-control" name="dimensional_name[]" >
                            </div>

                            
                          </div>
                          
                          <div class="form-group row">
                            <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Weightage %  <i class="text-danger">*</i></label>
                            <div class="col-lg-3">
                              <input min="1" type="text" required="" class="form-control onlyNumber onStepPercent" name="dimensional_weightage[]" >
                            </div>
                          </div>
                          
                          <div class="form-group row">
                            <label for="input-14" class="col-lg-3 col-form-label"> Dimensional Image </label>
                            <div class="col-lg-3">
                              <input type="file" class="form-control photoOnly" accept="image/*" name="dimensional_image[]">
                            </div>
                          </div>
                        </div>`; //New input field html 


        //Once add button is clicked
        $(addButton).click(function() {
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
            var val2Temp = 0;
            $('.onStepPercent').each(function() {
                val2Temp += (parseFloat($(this).val()) || 0);
            });
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e) {
            e.preventDefault();
            $(this).parent().parent().parent().parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });



    });
</script>