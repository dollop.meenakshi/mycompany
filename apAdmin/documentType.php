
 <?php 
    if(isset($_POST['editDocumentType'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("document_type_master","document_type_id='$document_id_edit'");
    $data=mysqli_fetch_array($q);
    } else {
    $btnName="Add";
    }
     ?>
     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Documents Type</h4>
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="documentTypeAdd" action="controller/documentController.php" method="post">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   <?php echo $xml->string->society; ?> Document Type
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-4 col-form-label">Document Type Name <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <input maxlength="50" type="text" required="" value="<?php echo $data['document_type_name']; ?>" class="form-control text-capitalize" id="input-10" name="document_type_name">
                  </div>
                  
                  <div class="col-sm-4">
                   
                  </div>
                </div>

                <div class="form-footer text-center">
                     <?php if(isset($_POST['document_id_edit'])) { ?>
                      <input type="hidden" value="<?php echo $data['document_type_id']; ?>" name="document_id_edidt">
                      <input type="hidden" name="editDocumentType" value="editDocumentType"> 
                    <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> UPDATE</button>
                     <?php } else { ?>
                      <input type="hidden" name="addDocumentType" value="addDocumentType"> 
                      
                    <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> SAVE</button>
                    <?php } ?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->