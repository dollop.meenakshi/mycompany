<?php
error_reporting(0);
$bId = $_GET['bId'];
$dId = $_GET['dId'];
$uId = $_GET['uId'];
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));

if(isset($dId) && $dId > 0)
{
    $deptFilterQuery = " AND ssm.floor_id = '$dId'";
}
if(isset($bId) && $bId > 0)
{
    $branchFilterQuery = "ssm.block_id = '$bId'";
}
if(isset($uId) && $uId > 0)
{
    $userFilterQuery = " AND ssm.user_id = '$uId'";
}
if(isset($_GET['laYear']) && $_GET['laYear'] > 0)
{
    $YearFilterQuery = " AND YEAR(ssm.salary_end_date) IN ($_GET[laYear])";
}
$salaryArray = array();
$salary_slip_ids_array = array();
$total_net_sal = 0;
$total_gross_sal = 0;
if(isset($dId) && $dId > 0 && isset($bId) && $bId > 0)
{
    $q= $d->selectRow('ssm.total_net_salary,ssm.total_earning_salary,ssm.salary_slip_id,fm.floor_name,bm.block_name,um.user_id,um.user_mobile,um.user_full_name,um.user_designation,um.block_id,um.floor_id,ubn.bank_name,ubn.bank_branch_name,ubn.account_no,ubn.account_type,ubn.ifsc_code',"salary_slip_master AS ssm LEFT JOIN user_bank_master AS ubn ON ssm.user_id = ubn.user_id AND ubn.is_primary = 1 JOIN users_master AS um ON um.user_id = ssm.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","$branchFilterQuery $deptFilterQuery $userFilterQuery $YearFilterQuery AND ssm.salary_slip_status = 2");
    while ($data = $q->fetch_assoc())
    {
        $total_net_sal += $data['total_net_salary'];
        $total_gross_sal += $data['total_earning_salary'];
        $userWiseTotalNetSal[$data['user_id']]['total_net_salary'] += $data['total_net_salary'];
        $userWiseTotalEarningSal[$data['user_id']]['total_earning_salary'] += $data['total_earning_salary'];
        $salaryArray[$data['user_id']] = $data;
        $salary_slip_ids_array[$data['salary_slip_id']] = $data['user_id'];
        // array_push($salary_slip_ids_array, $data['salary_slip_id']);
    }
}
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));

$salaryDeductionType = array();
$qsalType = $d->selectRow("salary_earning_deduction_id,earning_deduction_name","salary_earning_deduction_type_master","society_id = '$society_id' AND earn_deduct_is_delete = 0");
$ttc = 0;
while ($dataSalaryType=mysqli_fetch_array($qsalType))
{
    $ttc++;
    array_push($salaryDeductionType, $dataSalaryType);
}

foreach($salary_slip_ids_array AS $key => $value)
{
    $qn = $d->selectRow("salary_slip_sub_id,salary_slip_id,salary_earning_deduction_id,earning_deduction_name_current,earning_deduction_type_current,earning_deduction_amount","salary_slip_sub_master","salary_slip_id = '$key'","ORDER BY salary_earning_deduction_id ASC");
    while($dq = $qn->fetch_assoc())
    {
        for($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++)
        {
            if($dq['salary_earning_deduction_id'] == $salaryDeductionType[$sTypes]['salary_earning_deduction_id'])
            {
                $salaryArray[$value][$dq['earning_deduction_name_current']] += $dq['earning_deduction_amount'];
            }
        }
        $all_added_type_id[$key][] = $dq['salary_earning_deduction_id'];
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Salary Report</h4>
            </div>
        </div>
        <form action="" method="get" id="fltrFrm" class="branchDeptFilter">
            <div class="row pb-2">
                <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
                <div class="col-md-2 col-3">
                    <select name="laYear" class="form-control">
                        <option value="">Year</option>
                        <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
                        <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
                        <option <?php if ($_GET['laYear'] == $currentYear) {
                                    echo 'selected';
                                } ?> <?php if ($_GET['laYear'] == '') {
                                echo 'selected';
                            } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                        <option <?php if ($_GET['laYear'] == $nextYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
                    </select>
                </div>
                <!-- <input type="hidden" name="is_failed" id="is_failed" value="<?php echo $is_failed; ?>"> -->
                <div class="col-lg-1 from-group col-3">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {echo $_GET['dId']; } ?>">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            ?>
                            <table id="<?php if ($adminData['report_download_access'] == 0) {
                                            echo 'exampleReportWithoutBtn';
                                        } else {
                                            echo 'exampleReport';
                                        } ?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee</th>
                                        <th>Designation</th>
                                        <th>Mobile</th>
                                        <th>Net Salary</th>
                                        <th>Gorss Salary</th>
                                        <?php
                                        for($sTypes=0; $sTypes < count($salaryDeductionType) ; $sTypes++)
                                        {
                                        ?>
                                        <th><?php echo $salaryDeductionType[$sTypes]['earning_deduction_name'];?></th>
                                        <?php
                                        }
                                        ?>
                                        <th>Bank Name</th>
                                        <th>Bank Branch</th>
                                        <th>IFSC</th>
                                        <th>Account Number</th>
                                        <th>Account type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($salaryArray AS $k1 => $v1)
                                    {
                                    ?>
                                        <tr>
                                            <td><?php echo  $i++; ?></td>
                                            <td><?php echo $v1['user_full_name']; ?> <?php if(isset($v1['user_designation']) && $v1['user_designation'] !="") ; ?></td>
                                            <td><?php echo  $v1['user_designation']; ?></td>
                                             <td><?php echo $v1['user_mobile']; ?></td>
                                            <td><?php echo $userWiseTotalNetSal[$k1]['total_net_salary']; ?></td>
                                            <td><?php echo $userWiseTotalEarningSal[$k1]['total_earning_salary']; ?></td>
                                            <?php
                                            for($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++)
                                            {
                                                $rrr = 1;
                                            ?>
                                            <td class="<?php echo 'salary_earn_ded'.$sTypes; ?>"><?php

                                                if(isset($v1[$salaryDeductionType[$sTypes]['earning_deduction_name']]))
                                                {
                                                    echo $v1[$salaryDeductionType[$sTypes]['earning_deduction_name']];
                                                }
                                                else
                                                {
                                                    echo "0";
                                                }
                                            ?>
                                            </td>
                                            <?php
                                            }
                                            ?>
                                           
                                            <td><?php echo $v1['bank_name']; ?></td>
                                            <td><?php echo $v1['bank_branch_name']; ?></td>
                                            <td><?php echo $v1['ifsc_code']; ?></td>
                                            <td><?php echo $v1['account_no']; ?></td>
                                            <td><?php echo $v1['account_type']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b><?php echo $total_net_sal; ?></b></td>
                                        <td><b><?php echo $total_gross_sal; ?></b></td>
                                        <?php
                                        for($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++)
                                        {
                                        ?>
                                        <td class="font-weight-bold tot<?php echo $sTypes; ?>"></td>
                                        <?php
                                        }
                                        ?>
                                        
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<script src="assets/js/jquery.min.js"></script>
<script>
var ttc = '<?php echo $ttc; ?>';
$(document).ready(function()
{
  for (let i = 0; i < ttc; i++)
  {
    var sum = 0
    $(".salary_earn_ded"+i).each(function()
    {
      sum += parseFloat($(this).text());
    });
    $('.tot'+i).text(sum.toFixed(2));
  }
});
</script>