<?php
	$currentYear = date('Y');
	$nextYear = date('Y', strtotime('+1 year'));
	$onePreviousYear = date('Y', strtotime('-1 year'));
	$twoPreviousYear = date('Y', strtotime('-2 year'));
	if (isset($_REQUEST['year'])) {
		$year  = (int)$_REQUEST['year'];
	} else {
		$year  = date('Y');
	}
?>
<div class="col-md-2 form-group">
	<select name="year" class="form-control single-select">
		<option <?php if($year==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
		<option <?php if($year==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
		<option <?php if($year==$currentYear) { echo 'selected';}?> <?php if($year=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
		<option <?php if($year==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
	</select>
</div>