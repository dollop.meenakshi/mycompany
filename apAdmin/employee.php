<?php
error_reporting(0);
extract(array_map("test_input" , $_REQUEST));
if(isset($emp_id) && isset($manageEmployeeUnit) && $manageEmployeeUnit=="yes") { 
 
$id= (int)$emp_id;
  $q=$d->select("employee_master","emp_id=$id");
  $data=mysqli_fetch_array($q);
  if (mysqli_num_rows($q)==0) {
      $_SESSION['msg1']="Invalid Employee";
      echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employees';
      </script>");
  }

  $EW=$d->select("emp_type_master","emp_type_id='$data[emp_type_id]'");
    $empTypeData=mysqli_fetch_array($EW);
      if ($empTypeData['emp_type_name']!='') {
      $type= $empTypeData['emp_type_name'];
      } else {
      $type= "Security Guard";
      } 
 $empType= $data['emp_type'];

  $count5=$d->sum_data("rating_star","employee_rating_master","emp_id='$id'");
        while($row=mysqli_fetch_array($count5))
       {
            $asif=$row['SUM(rating_star)'];
          $totalStar=number_format($asif,2,'.','');
               
        }
        $totalUser=$d->count_data_direct("rating_id","employee_rating_master","emp_id='$id'");
        if ($totalStar>0) {
            $average_rating= $totalStar/$totalUser;
            $average_rating= number_format($average_rating,1,'.','');
        } else {
            $average_rating='0';

        }
?>
<style type="text/css" media="screen">
  .checked {
  color: orange;
}  
</style>
<link href="assets/plugins/fullcalendar/css/fullcalendar.min.css" rel='stylesheet'/>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> <?php echo $xml->string->employee; ?> <?php echo $xml->string->details; ?></h4>
      
     </div>
     
     </div>
    <!-- End Breadcrumb-->
    
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
             
                <?php 
                $emp_id = (int)$emp_id;
                $edit_id =  $visitor_id;
                   $q=$d->select("employee_unit_master","emp_id='$emp_id'");
                   $selected_user_ids = array();
                  while ( $emp_unit_data=mysqli_fetch_array($q)) {
                    $selected_user_ids[] = $emp_unit_data['user_id'];
                  }
                 ?>
                  
               <div class="form-group row">
                  <div class="col-sm-3 text-center">
                    <h5 class="text-capitalize"><?php echo $data['emp_name'];?></h5>
                    <a href="../img/emp/<?php echo $data['emp_profile'];?>" data-fancybox="images" data-caption="Photo Name : Profile Photo"> 
                    <img  onerror="this.src='img/user.png'" height="180" width="180" src="../img/emp/<?php echo $data['emp_profile'];?>"  alt="">
                    </a>

                    <ul  class="list-inline text-center mt-3">

                       <li class="list-inline-item">
                        <form title="Edit" action="employee" method="post">
                          <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                          <input type="hidden" name="updateemployee" value="updateemployee">
                          <button type="submit" name="" value="" class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i> <?php echo $xml->string->edit; ?></button>
                        </form>
                      </li>
                      <li class="list-inline-item">
                        <?php if($data['emp_status']=="1") {  ?>
                          <form title="Delete" action="controller/employeeController.php" method="post"  >
                            <input type="hidden" name="emp_profile" value="<?php if($data['emp_profile']!="user.png") { echo $data['emp_profile']; } ?>">
                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">

                            <input type="hidden" value="deleteemployee" name="deleteemployee">
                            <input type="hidden" name="deleteEmpName" value="<?php echo $data['emp_name'];?>">
                            <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> <?php echo $xml->string->delete; ?></button>
                          </form>
                        <?php } else { ?>
                          <form title="Delete" action="controller/employeeController.php" method="post"  >
                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">

                            <input type="hidden" value="restoreEmployee" name="restoreEmployee">
                            <input type="hidden" name="deleteEmpName" value="<?php echo $data['emp_name'];?>">
                            <button type="submit"  class="btn btn-success btn-sm form-btn"><i class="fa fa-undo"></i> Restore</button>
                          </form>
                        <?php } ?>
                      </li>
                    </ul>
                  <?php for ($i=1; $i <=5 ; $i++) {  
                    if ($i<=$average_rating) {
                    echo "<span class='fa fa-star checked'></span>";
                    } else {
                    echo "<span class='fa fa-star'></span>";
                    }
                    ?>

                    <?php } ?>
                    
                    (<?php echo  $average_rating;?>)

                    <p>Status : <?php if ($data['entry_status']==1) {
                    echo "<span class='badge badge-success m-1'>".$xml->string->inside."</span>";
                    } elseif ($data['entry_status']==2) {
                    echo "<span class='badge badge-danger  m-1'>".$xml->string->outside_adapter."</span>";
                    }  else {
                    echo "<span class='badge badge-info  m-1'>".$xml->string->nodata_adapter."</span>";
                    }?></p>
                  </div>  
                  <div  class="col-sm-9 ">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                     
                      <tbody>
                        <tr>
                          <th><?php echo $xml->string->name; ?></th>
                          <td><?php echo $data['emp_name'];?></td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->block_type; ?></th>
                          <td><?php echo $type;?></td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->mobile_no; ?></th>
                          <td><?php echo $data['country_code'];?> <?php echo $data['emp_mobile'];?></td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->employee; ?> <?php echo $xml->string->for; ?></th>
                          <td><?php if($empType==0) {  echo $xml->string->society; } else { echo $xml->string->members; }?></td>
                        </tr>
                        <?php if ($empType==1) { ?>
                        <tr>
                          <th><?php echo $xml->string->currently_working_in; ?> <?php echo $xml->string->units; ?></th>
                          <td><?php 
                           $uq=$d->select("unit_master,block_master,users_master,employee_unit_master ","users_master.delete_status=0 AND employee_unit_master .unit_id=unit_master.unit_id  AND
                            employee_unit_master .user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master .emp_id ='$data[emp_id]' ","GROUP BY employee_unit_master.unit_id ORDER BY unit_master.unit_id ASC");

                          echo mysqli_num_rows($uq); ?></td>
                        </tr>
                        <?php } if ($data['entry_status']!='0') { ?>
                        <tr>
                          <th><?php if ($data['entry_status']==1) { echo "Visit In Time"; }  else if ($data['entry_status']==2) { echo "Visit Out Time";  } ?></th>
                          <td><?php if ($data['entry_status']==1) {  echo date("d-m-Y h:i A", strtotime($data['in_out_time'])); }  else if ($data['entry_status']==2) { echo date("d-m-Y h:i A", strtotime($data['in_out_time']));  }?></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                          <th><?php echo $xml->string->visiting_time; ?></th>
                          <td>
                            <?php echo $xml->string->no_data; ?>
                          </td>
                        </tr>
                        <?php } if( $data['emp_id_proof']!='' || $data['emp_id_proof1']!='') { ?>
                         <tr>
                          <th><?php echo $xml->string->view; ?> <?php echo $xml->string->resource_id_proof; ?></th>
                          <td>
                            <?php if( $data['emp_id_proof']!='' && $data['emp_id_proof1']!='') { ?>
                              <a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $data['emp_id_proof'];?>" name="Id Proof" class="btn btn-warning btn-sm"><?php echo $xml->string->front_side; ?></a>
                              <a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $data['emp_id_proof1'];?>" name="Id Proof" class="btn btn-warning btn-sm"><?php echo $xml->string->back_side; ?></a>
                            <?php } else  if( $data['emp_id_proof']!='') { ?>
                              <a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $data['emp_id_proof'];?>" name="Id Proof" class="btn btn-warning btn-sm"><?php echo $xml->string->view; ?></a>
                               <?php } else  if( $data['emp_id_proof1']!='') { ?>
                               <a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $data['emp_id_proof1'];?>" name="Id Proof" class="btn btn-warning btn-sm"><?php echo $xml->string->view; ?></a>
                              <?php } ?>
                          </td>
                        </tr>
                      <?php  } ?>
                        <tr>
                          <th><?php echo $xml->string->print_i_card; ?></th>
                          <td> <a title="Print I-Card" href="printIcardSingle.php?emp_id=<?php echo $emp_id; ?>" class="btn btn-info btn-sm"><i class="fa fa-print"></i> Print</a></td>
                        </tr>

                       <?php if($data['emp_type_id'] == 0 && $data['emp_token'] !=''){?> 
                         
                        <tr>
                          <th><?php echo $xml->string->eye_attendance; ?></th>
                          <td> <?php if ($data['ey_attendace']==0) {
                            echo $xml->string->off;
                          } else {
                            echo $xml->string->on;
                          }  ?>
                          <a href="getEyeStatus?manageEmployeeEye=eye&emp_id=<?php echo $emp_id; ?>" name="manageEmployeeUnitBtn" class="btn btn-danger  btn-sm"><i class="fa fa-eye"></i> <?php echo $xml->string->manage; ?> </a>
                          </td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->qr_code_attendance; ?> </th>
                          <td> <?php if ($data['qr_attendace']==0) {
                            echo $xml->string->off;
                          } else {
                           echo $xml->string->on;
                          }  ?>
                          <a href="getEyeStatus?manageEmployeeEye=qr&emp_id=<?php echo $emp_id; ?>" name="manageEmployeeUnitBtn" class="btn btn-danger  btn-sm"><i class="fa fa-qrcode"></i> <?php echo $xml->string->manage; ?> </a>
                          </td>
                        </tr>
                         <tr>
                          <th><?php echo $xml->string->device_mac; ?></th>
                          <td> <?php echo $data['device_mac']; ?> </td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->app_version; ?></th>
                          <td> <?php echo $data['version_code']; ?> / <?php echo $data['phone_brand'].'-'.$data['phone_model']; ?></td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->phone_lock; ?></th>
                          <td> 
                            <?php
                            if($data['device_lock']=="0"){
                            ?>On 
                            <input type="checkbox" checked  class="js-switch"  data-color="#15ca20" onchange ="changeStatus('<?php echo $emp_id; ?>','phoneUnlock');" data-size="small"/> 
                            <?php } else { ?> Off
                            <input type="checkbox" class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $emp_id; ?>','phoneLock');" data-size="small"/> 
                            <?php } ?>
                          </td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->logout; ?></th>
                          <td> 
                            
                           <form title="Logout From App" action="controller/employeeController.php" method="post" >
                              <input type="hidden" name="emp_token" value="<?php echo $data['emp_token']; ?>">
                              <input type="hidden" name="emp_logout" value="emp_logout">
                              <input type="hidden" name="emp_id" value="<?php echo $data['emp_id']; ?>">
                              <input type="hidden" name="empName" value="<?php echo $data['emp_name']; ?>">
                              <button type="submit" class="btn btn-sm form-btn btn-warning" ><i class="fa fa-power-off" aria-hidden="true"></i></button>
                            </form>
                          </td>
                        </tr>
                      
                       <?php } ?>
                         <?php if($data['emp_type_id'] == 0){?> 
                          <tr>
                          <th><?php echo $xml->string->gate_number; ?></th>
                          <td>
                           <?php  echo $data['gate_number'];   
                           ?>
                          </td>
                        </tr>
                         <tr>  
                          <th><?php echo $xml->string->block; ?> <?php echo $xml->string->access; ?></th>
                          <td>
                            <?php
                             $blockAry = array();
                              $duCheck=$d->select("employee_block_master","emp_id='$data[emp_id]' ");
                              while ($oldBlock=mysqli_fetch_array($duCheck)) {
                                  array_push($blockAry , $oldBlock['block_id']);
                              }
                             $q3=$d->select("block_master","society_id='$society_id'","");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                         ?>
                         <?php if(in_array($blockRow['block_id'], $blockAry)) {  echo $blockRow['block_name'].','; }?> 
                        <?php } ?>
                          </td>
                        </tr>
                        <tr>
                          <th><?php echo $xml->string->login; ?> <?php echo $xml->string->mpin; ?> </th>
                          <td> <?php if ($data['mpin']=='' ) {
                            echo $xml->string->not_available;
                          } else {
                            echo $data['mpin'];   
                          }  ?>
                           <a href="javascript:void(0)"  data-toggle="modal"  data-target="#mpinModelChange" class="btn btn-primary  btn-sm"><i class="fa fa-pencil"></i>  </a>
                          </td>
                        </tr>
                      <?php  } ?>
                      </tbody>
                    </table>

                    </div>
                  </div>
                 
                   
                </div>
                 
                 <?php 
                 if ($empType==1 && $data['private_resource']==0) {

                  ?>
                  <form  id="EmpUnitFrm" action="controller/employeeController.php" method="post" enctype="multipart/form-data">
                   <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id;?>">
                   <input type="hidden" name="emp_name"  value="<?php echo  $data['emp_name'];?>">

                <div class="form-group row">
                  <div class="col-sm-12">
                    <label  class="col-form-label"><?php echo $xml->string->namage; ?> <?php echo $xml->string->units; ?> <?php echo $xml->string->for; ?> <?php echo $data['emp_name']; ?></label>
                    <select  name="user_data[]" id="user_data" class="form-control single-select " required="" >
                      <option value="">-- Select User --</option>
                       <?php 
                      $ids = join("','",$selected_user_ids);   
                      $q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND users_master.member_status=0 AND unit_master.unit_status!=0  AND unit_master.unit_status!=4 and  users_master.user_id  NOT IN ('$ids') $blockAppendQuery","ORDER BY unit_master.unit_id ASC");
                    while ($blockRow=mysqli_fetch_array($q3)) {
                      //$blockRow['unit_id'].'~'.
                      ?>
                      <option value="<?php echo $blockRow['unit_id'].'~'. $blockRow['user_id'].'~'. $blockRow['user_type'];?>"><?php echo $blockRow['block_name'];?>-<?php echo $blockRow['unit_name'];?>  <?php echo $blockRow['user_full_name'];?> (<?php
                       if($blockRow['user_type']=="0" && $blockRow['member_status']=="0" ){
                                    echo $xml->string->owner;
                                  } else  if($blockRow['user_type']=="0" && $blockRow['member_status']=="1" ){
                                      echo $xml->string->owner."-".$blockRow['member_relation_name'];
                                  } else  if($blockRow['user_type']=="1" && $blockRow['member_status']=="0"  ){
                                      echo  $xml->string->tenant;
                                  }else  if($blockRow['user_type']=="1" && $blockRow['member_status']=="1"   ){
                                      echo $xml->string->tenant."-".$blockRow['member_relation_name'];
                                  } else   {
                                    echo $xml->string->owner;
                                  }  ?>)</option>
                    <?php }?>


                      ?>
                    </select>
                  </div>

                  <span id="time_slot_detail_div" class="mr-3 ml-3"></span>
                
                  <div class="col-sm-12">
                    <input type="hidden" name="EmployeeUserManageBtn" value="EmployeeUserManageBtn">
                    <button type="submit" id="EmployeeUserManageBtn" class="btn btn-success mt-4" name=""><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?></button>
                  </div>
                </div>
                  </form>
              <?php }

              if ($data['private_resource']==1) {
                 echo $xml->string->employee_is_private;
               } ?>

             
            </div>
          </div>
        </div>
      </div><!--End Row-->

       <?php 
       if ($empType==1) { 

             
          if(mysqli_num_rows($uq)>0) {  ?>
     <div class="row">
       <div class="col-12 col-lg-12">
        <div class="card border border-info floorList">
          <div class="card-body">
            <div class="media align-items-center">
              
             
            <div class="media-body p-1">
              <div class="row">
               <?php
                while ($unitData=mysqli_fetch_array($uq)) { 
                    extract($unitData);
                    // $qqname= $d->selectRow("user_full_name","users_master,employee_unit_master","users_master.delete_status=0 AND employee_unit_master.user_id=users_master.user_id AND employee_unit_master.unit_id='$unitData[unit_id]' AND users_master.member_status=0","ORDER BY users_master.user_id DESC");
                    // $userData=mysqli_fetch_array($qqname);
                    // $primaryName= $userData['user_full_name'];
                  ?>
                <div class="col-6 col-lg-2 col-xl-2 ">
                  <div class="card gradient-scooter no-bottom-margin">
                    <div class="card-body text-center">
                    <h6 class="text-white mt-2"><?php echo $block_name; ?>-<?php echo $unitData['unit_name']; ?> 
                    <!-- <i title="Delete Unit" class="fa fa-trash-o pull-right" onclick="DeleteUnit('<?php echo $unitData['unit_id']; ?>');"></i> -->

                      <br><?php 
                        echo $unitData['user_full_name']; 
                       ?><br>
                        <?php if ($unitData['user_type']==0) { echo '('.$xml->string->owner.')';
                        $user_type= 0;
                        } else {
                          $user_type= 1;
                          echo '('.$xml->string->tenant.')';
                        } ?></h6>

                       
                            

                            <div class="row">
                           
                              <?php
                              if ($adminData['admin_type']==1 || count($blockAryAccess)==0 || in_array($unitData['block_id'], $blockAryAccess) && count($blockAryAccess)>0) { ?>
                             <div class="col-4"  >

                              <a title="Time Slot"  onclick="getEmpUnitEdit('<?php echo $unit_id; ?>','<?php echo $user_id; ?>');" data-toggle="modal" data-target="#updateTimeUnitModal" href="javascript:void();" > <span class="btn btn-primary btn-sm  "  ><i class="fa fa-calendar"></i></span></a>

 
                            </div>

                            <div class="col-4"  >

                              <a title="Notification Members"  onclick="getNotificationData('<?php echo $unit_id; ?>','<?php echo $user_id; ?>');" data-toggle="modal" data-target="#notificationModal" href="javascript:void();" > <span class="btn btn-warning btn-sm  "  ><i class="fa fa-bell"></i></span></a>

                            </div>



                             <div class="col-4"  >
                               <form action="controller/employeeController.php" method="post" >
                                  <input type="hidden" name="employee_unit_id" value="<?php echo $employee_unit_id; ?>">
                                  <input type="hidden" name="emp_name_unit_remove" value="<?php echo $data['emp_name'];?>">
                                  <input type="hidden" name="unit_name_remove" value="<?php echo $block_name; ?>-<?php echo $unitData['unit_name']; ?>">
                                  <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                                  <input type="hidden" name="user_type" value="<?php echo $user_type; ?>">
                                 <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                                  <input type="hidden" name="manageEmpUser" value="yes">
                                <input type="hidden" name="EmpUserDelete" value="EmpUserDelete">
                                <button style="margin-top: 0px !important;" type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> </button>
                                
                              </form>
                            </div>
                          <?php } ?>

                           
                        </div>
                       
                  </div>
                  </div>
                </div>
               
                <?php }   ?>
                
              </div>

            </div>
          </div>
         </div>
       </div>
      </div>
     </div>

  <?php } } ?>

      <div class="row">
       <div class="col-12 col-lg-12">
        <div class="card border border-info floorList">
          <div class="card-body">
            <h5><?php echo $xml->string->ratings; ?> & <?php echo $xml->string->review; ?></h5>
          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $xml->string->name; ?> </th>
                  <th><?php echo $xml->string->ratings; ?></th>
                  <th><?php echo $xml->string->comment; ?></th>
                  <th><?php echo $xml->string->reminder_date; ?></th>
                  <th><?php echo $xml->string->action; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $data = $d->select('employee_master,employee_rating_master,users_master,unit_master,block_master',"unit_master.unit_id=employee_rating_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id=employee_rating_master.user_id AND employee_master.emp_id=employee_rating_master.emp_id AND employee_rating_master.emp_id='$id'","ORDER BY employee_rating_master.rating_id DESC");
                $i1=1;
                while ( $data1 = mysqli_fetch_array($data)) { ?>
                  <tr>
                    <td><?=$i1++;?></td>
                    <td><?=$data1['user_full_name'].' ('.$data1['user_designation'].'-'.$data1['block_name'].')'?></td>
                      <?php
                    
                    ?>
                    <td><?=$data1['rating_star']?></td>
                    <td><?=$data1['rating_msg']?></td>
                    <td><?=$data1['ratting_date']?></td>
                    <td>
                        <?php
                              if ($adminData['admin_type']==1 || count($blockAryAccess)==0 || in_array($data1['block_id'], $blockAryAccess) && count($blockAryAccess)>0) { ?>
                      <form method="POST" action="controller/employeeController.php">
                        <input type="hidden" name="emp_id" value="<?php echo $emp_id ?>">
                        <input type="hidden" name="rating_id_delete" value="<?php echo $data1['rating_id'] ?>">
                        <button class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i></button>
                      </form>
                    <?php } ?>
                    </td>
                  </tr>
                <?php  }?>                  
              </tbody>
            </table>
           </div>
         </div>
       </div>
      </div>
     </div>

     <input type="hidden" id="todayDate" value="<?php echo date("Y-m-d"); ?>" name="">
       <?php 
       $statrDate =  date("Y-m-d", strtotime("-3 month"));
       $startMonth =  date("Y-m", strtotime("-3 month"));
       $viewEmployeeAttandaceResouce = 'viewEmployeeAttandaceResouce'; ?>
      <?php 
      if ($_GET['monthFilter'] !="") {
        $monthFilter = $_GET['monthFilter'];
        $timestamp    = strtotime($monthFilter);
        $from = date('Y-m-01', $timestamp);
        $toDate  = date('Y-m-t', $timestamp);
      } else {
        $from = date("Y-m-")."01";
        $toDate = date("Y-m-d");
        $cMonth = date("F-Y");
        $monthFilter = date("F-Y");
      }
    
     ?>
        <form action="" method="get" >
      <h6><?php echo $xml->string->employee_attendance; ?> <?php echo $xml->string->month; ?> 
        <input type="hidden" name="manageEmployeeUnit" value="yes">
        <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
        <select onchange="this.form.submit()" name="monthFilter">
            <?php $i = 1;
              $month = strtotime($statrDate);
              while($i <= 4)
              {
                $month_name = date('F-Y', $month);
                $monthOnlty = date('m', $month);
                $totalDaysMonth =   date('t', strtotime($month_name));
                $firstDate = date('Y-m-01', strtotime($month_name));
                $lastDate = date('Y-m-t', strtotime($month_name));
                ?>
                <option <?php if($monthFilter=="" && $cMonth==$month_name) { echo 'selected'; } else if($monthFilter==$month_name){  echo 'selected'; } ?> value="<?php echo $month_name; ?>"><?php echo $month_name; ?></option>
               <?php
                $month = strtotime('+1 month', $month);
                $i++; 

                }?>
        </select>
      </h6> 
      </form>
        
      <div class="row">
      <div class="col-lg-12 pt-4">
        <div class="card border border-info floorList">
          <div class="card-body table-responsive">
            <?php 
             $monthFilter;
             $totalDaysinMonth =  date('t', strtotime($monthFilter));
             if ($monthFilter=="") {
               $monthFilter = date('F-Y');
             }
             ?>
          <table  class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo $xml->string->reminder_date; ?></th>
                    <th><?php echo $xml->string->reminder_time; ?></th>
                </tr>
            </thead>
            <tbody>
              <?php 
              $i3= 1;
             for ($iMonth=1; $iMonth <= $totalDaysinMonth; $iMonth++) { 
                $viDate = date("Y-m-d", strtotime($iMonth.'-'.$monthFilter)); 
               ?>
                <tr>
                   
                    <td><?php echo $i3++; ?></td>
                    <td><?php echo $iMonth.'-'.date("M-Y", strtotime($iMonth.'-'.$monthFilter)) ?></td>
                    <td><?php 
                      $entryI= 1;
                      $qs=$d->select("employee_master,staff_visit_master","employee_master.emp_id =staff_visit_master.emp_id  AND employee_master.society_id='$society_id' AND staff_visit_master.filter_data ='$viDate' AND staff_visit_master.emp_id='$emp_id'","ORDER BY staff_visit_master.staff_visit_id");
                      while ($visitData=mysqli_fetch_array($qs)){
                        echo "(".$entryI++.")";
                          if ($visitData['visit_entry_date_time']!="") {
                            echo "<i class='text-success'>IN :".  date("h:i A", strtotime($visitData['visit_entry_date_time'])).'</i>';
                            echo "<br>";
                          }

                          if ($visitData['visit_status']!=0) { 
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;<i class='text-danger'>OUT :". date("h:i A", strtotime($visitData['visit_exit_date_time'])); 
                            if (date("Y-m-d", strtotime($visitData['visit_exit_date_time'])) != $viDate) {
                              echo " (". date("d-M", strtotime($visitData['visit_exit_date_time'])).')'; 
                            }
                            echo "</i><br>";
                          }
                      }

                    ?></td>
                   
                    
                </tr>
              <?php } ?>
            </tbody>  
            
        </table>
        </div>
      </div>
      </div>
    </div>
      
    <!-- End container-fluid-->
    <div class="row">
      <div class="col-lg-12 pt-4">
        <div class="card table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><?php echo $xml->string->month; ?></th>
                <th><?php echo $xml->string->currently_working_in; ?></th>
                <th><?php echo $xml->string->absent; ?></th>
                <th><?php echo $xml->string->total_day; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 1;
              $month = strtotime($statrDate);
              $curnetMonthName = date("F-Y");
              while($i <= 4)
              {
                  $month_name = date('F-Y', $month);
                  $monthOnlty = date('m', $month);
                  if ($month_name==$curnetMonthName) {
                    $totalDaysMonth =   date('d');
                  } else {
                    $totalDaysMonth =   date('t', strtotime($month_name));
                  }
                  $firstDate = date('Y-m-01', strtotime($month_name));
                  $lastDate = date('Y-m-t', strtotime($month_name));
               ?>
              <tr>
                <td><?php echo $month_name;
                  $month = strtotime('+1 month', $month);
                  $i++; ?></td>
                <td><?php 
                $vCount = $d->select("staff_visit_master","emp_id='$id' AND filter_data BETWEEN '$firstDate' AND '$lastDate' ","GROUP BY filter_data ORDER BY staff_visit_id DESC ");
                echo $totalWorkingdays=  mysqli_num_rows($vCount);
                  ?>
                </td>
                <td><?php echo $totalDaysMonth-$totalWorkingdays; ?></td>
                <td><?php  echo $totalDaysMonth; ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>




    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
     <div class="modal fade" id="updateTimeUnitModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->time_slot; ?></h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"  style="align-content: center;">
             <form id="editTimeSlotFrm" action="controller/employeeController.php" method="post" enctype="multipart/form-data">
              
              <span id="updateTimeUnitModalDiv"></span>
              <div class="form-footer text-center">
              <input type="hidden" name="editTimeSlotBtn" value="editTimeSlotBtn">
              <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i><?php echo $xml->string->update; ?></button>
            </div>
             </form>

          </div>

        </div>
      </div>
    </div>

      <div class="modal fade" id="notificationModal">
      <div class="modal-dialog ">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white"><?php echo $xml->string->manage; ?> <?php echo $xml->string->in_out; ?> <?php echo $xml->string->notifications; ?></h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="NotificationListDiv" style="align-content: center;">
            

          </div>

        </div>
      </div>
    </div>
  <?php } else { //IS_1498


if(isset($updateemployee)){
  $q=$d->select("employee_master","emp_id='$emp_id' AND society_id='$society_id'");
$row=mysqli_fetch_array($q);
extract($row);
  $blockAry = array();
  $duCheck=$d->select("employee_block_master","emp_id='$emp_id' ");
  while ($oldBlock=mysqli_fetch_array($duCheck)) {
      array_push($blockAry , $oldBlock['block_id']);
  }
}
?>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2 ">
        <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->employee; ?></h4>
     </div>
     <div class="col-sm-3 col-6 text-right">
        <a href="employeeType" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->employee; ?> <?php echo $xml->string->block_type; ?></a>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="addEmp" action="controller/employeeController.php" method="POST" enctype="multipart/form-data" >
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i> <?php echo $xml->string->employee; ?> <?php echo $xml->string->details; ?>
                </h4>
                <div class="form-group row">
                   <div class="col-sm-12 text-center">

                    <label for="imgInp">
                       <img id="blah" style="border: 1px solid gray;" src="<?php if(file_exists("../img/emp/$emp_profile") && isset($updateemployee)) { echo '../img/emp/'.$emp_profile; }  else { echo 'img/user.png'; } ?>"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="imgInp" type="file" name="emp_profile">
                   </label>
                   </div>
                </div>
                <div class="form-group row">
                   <label for="input-15" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->for; ?> <span class="text-danger">*</span></label>
                   <div class="col-sm-4"  >
                       <select class="form-control check1" name="emp_type" required="">
                          <?php if(isset($updateemployee) && $emp_type_id!=0){ ?>
                          <option <?php if($emp_type==0) { echo 'selected';} ?>  value="0"><?php echo $xml->string->society; ?></option>
                          <?php } else if(isset($updateemployee) && $emp_type_id==0){ ?>
                          <option value="0"><?php echo $xml->string->society; ?></option>
                           <?php } else { ?>
                          <option value="0"><?php echo $xml->string->society; ?></option>
                          <?php } ?>
                       </select>
                   </div>
                   <?php
                   $style="display: none;";
                   if(isset($updateemployee) && ($emp_type_id==1||$emp_type_id==0 ) ){
                    $style="display: block;";
                   } ?>
                   <label id="gate_number_details_lable" style="<?php echo  $style;?>"  for="input-15" class="col-sm-2 col-form-label"><?php echo $xml->string->gate_number; ?> <span class="text-danger">*</span></label>
                   <div id="gate_number_details_div" class="col-sm-4" style="<?php echo  $style;?>"><input maxlength="50" required type="text" class="form-control text-capitalize onlyName" id="gate_number" name="gate_number" value="<?php if(isset($addemployee)){$gate_number='';}else{echo $gate_number;} ?>">
                   </div>
                </div>
                <div class="form-group row">
                  <label for="input-15" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->block_type; ?> <span class="text-danger">*</span></label>
                  <div class="col-sm-4" id="societyType">
                    <?php if(isset($updateemployee)){
                       if ($emp_type_id==0) { ?>
                        <select class="form-control check emp_type_id" name="emp_type_id" id="emp_type_id" required=""  >
                         <?php
                         error_reporting(0);
                         $q=$d->select("emp_type_master","society_id=0","");
                         while ($row=mysqli_fetch_array($q)) {
                       ?>
                        <option <?php if($emp_type_id==0) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
                        <?php }?>
                       </select>
                       <?php } else { ?>
                        <select class="form-control check emp_type_id" name="emp_type_id" required="" id="emp_type_id" >
                         <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                         <?php
                         error_reporting(0);
                         $q=$d->select("emp_type_master","society_id='$society_id' AND society_id!=0","");
                         while ($row=mysqli_fetch_array($q)) {
                       ?>
                        <option <?php if($emp_type_id==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
                        <?php }?>
                       </select>
                    <?php } }  else {?>
                      <select class="form-control check emp_type_id" name="emp_type_id" required=""  id="emp_type_id"  >
                       <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                       <?php
                       error_reporting(0);
                      $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0","");
                       while ($row=mysqli_fetch_array($q)) {
                     ?>
                      <option <?php if($emp_type_id==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
                      <?php }?>
                     </select>
                    <?php } ?>
                  </div>
                  <div class="col-sm-4" id="userType">
                      <select class="form-control check emp_type_id11" name="emp_type_id_user" required="" id="emp_type_id">
                       <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                       <?php
                       error_reporting(0);
                      $q=$d->select("emp_type_master","society_id='$society_id' AND society_id!=0","");
                       while ($row=mysqli_fetch_array($q)) {
                     ?>
                      <option <?php if($emp_type_id==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
                      <?php }?>
                     </select>
                  </div>
                  <label for="input-dd" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input maxlength="50" required type="text" class="form-control text-capitalize onlyName" id="input-dd" name="emp_name" value="<?php if(isset($addemployee)){$emp_name='';}else{echo $emp_name;} ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="empNumber" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->mobile_no; ?> <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $country_code; ?>" id="country_code_get" name="">
                    <select name="country_code" class="form-control single-select" id="country_code" required="">
                      <?php include 'country_code_option_list.php'; ?>
                    </select>
                  </div>
                  <div class="col-sm-2">
                    <?php if(isset($updateemployee)){
                    ?>
                     <input required="" type="text" maxlength="15" minlength="8" class="form-control preventStartWithZero" id="empNumber" inputmode="numeric" name="emp_mobile" value="<?php if(isset($addemployee)){$emp_mobile='';}else{echo $emp_mobile;} ?>">
                     <input required="" type="hidden" maxlength="10" class="form-control" id="empNumberOld"  value="<?php if(isset($addemployee)){$emp_mobile='';}else{echo $emp_mobile;} ?>">
                   <?php } else { ?>
                     <input required="" type="text" maxlength="15" minlength="8" class="form-control preventStartWithZero" id="empNumber" inputmode="numeric" name="emp_mobile" value="<?php if(isset($addemployee)){$emp_mobile='';}else{echo $emp_mobile;} ?>">
                     <!-- <i class="hideAutoButton"><?php echo $xml->string->auto_generated; ?>? <a onclick="autoNumberGenerate();" href="#"><?php echo $xml->string->click_here; ?></a></i> -->
                   <?php } ?>
                </div>
                  <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->email_contact_finca; ?></label>
                  <div class="col-sm-4">
                    <input type="email" maxlength="80" class="form-control" id="input-12" name="emp_email" value="<?php if(isset($addemployee)){$emp_email='';}else{echo $emp_email;} ?>">
                  </div>
                </div>
                <div class="form-group row">
               <label for="input-13" class="col-sm-2 col-form-label"><?php echo $xml->string->employee; ?> <?php echo $xml->string->address; ?></label>
                  <div class="col-sm-4">
                    <textarea class="form-control" name="emp_address"><?php if(isset($addemployee)){$emp_address='';}else{echo $emp_address;} ?></textarea>
                  </div>
                  <!--  <label for="input-11" class="col-sm-2 col-form-label">Employee Profile</label>
                  <div class="col-sm-4">
                   <input class="form-control-file border photoOnly" accept="image/*" type="file" name="emp_profile" accept="image/x-png,image/gif,image/jpeg">
                  </div> -->
                  <label for="input-15" class="col-sm-2 col-form-label"><?php echo $xml->string->date_of_join; ?></label>
                  <div class="col-sm-4">
                    <input type="text"  readonly="" class="form-control" id="autoclose-datepicker1"  name="emp_date_of_joing" value="<?php if(isset($addemployee)){$emp_date_of_joing='';}else{echo $emp_date_of_joing;} ?>">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label"><?php echo $xml->string->resource_id_proof; ?> </label>
                  <div class="col-sm-4 table-responsive" style="display: inherit;">
                    <?php if(isset($updateemployee)){ ?>
                      <input type="hidden" name="emp_id_proof_old" value="<?php echo $emp_id_proof;?>">
                      <input type="hidden" name="emp_id_proof_old1" value="<?php echo $emp_id_proof1;?>">
                    <?php }?>
                   <!--  <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"   type="file" class="form-control-file idProof border"  name="emp_id_proof">
                    (Photo,Doc, Pdf & No more than 4 mb) -->

                    <label for="imgInpId">
                       <img id="idproof" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
                     <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="idInput idProof" id="imgInpId" type="file" name="emp_id_proof"><br>
                     <?php echo $xml->string->front_side; ?><?php if($emp_id_proof!='') { ?> <a target="_blank" href="../img/emp_id/<?php echo $emp_id_proof;?>"><?php echo $xml->string->view; ?></a> <?php } ?>
                   </label>
                     <label for="imgInpId1">
                       <img id="idproof1" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
                     <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="photoInput idProof" id="imgInpId1" type="file" name="emp_id_proof1">
                     <br>
                     <?php echo $xml->string->back_side; ?> <?php if($emp_id_proof1!='') { ?> <a target="_blank" href="../img/emp_id/<?php echo $emp_id_proof1;?>"><?php echo $xml->string->view; ?></a> <?php } ?>
                   </label>
                  </div>
                    <label id="empSalary" for="input-14" class="col-sm-2 col-form-label"><?php echo $xml->string->employee_salary; ?> <?php echo $xml->string->per; ?> <?php echo $xml->string->month; ?> </label>
                    <div class="col-sm-4">
                      <input type="text" autocomplete="off" class="form-control onlyNumber" inputmode="numeric" id="empSalary1" id="emp_sallary" name="emp_sallary" value="<?php if(isset($addemployee)){$emp_sallary='';}else if($emp_sallary!=0){echo $emp_sallary;} ?>">
                    </div>
                </div>
                 <div class="form-group row" id="passwordDiv">
                  <label for="password-field" class="col-sm-2 col-form-label"> <?php echo $xml->string->access; ?> <?php echo $xml->string->blocks; ?> <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                      <select  type="text" required="" class="form-control multiple-select" name="block_id[]" multiple="multiple">
                        <option value="">-- <?php echo $xml->string->select; ?> --</option>
                        <?php
                          $q3=$d->select("block_master","society_id='$society_id'","");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                          if(isset($updateemployee)){
                         ?>
                          <option <?php if(in_array($blockRow['block_id'], $blockAry)){ echo 'selected'; } ?>   value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> </option>
                        <?php }  else {?>
                          <option selected value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> </option>
                          <?php } }?>
                        </select>
                  </div>
                </div>
                    <?php if(isset($updateemployee))
                  {
                    ?>
                    <input type="hidden" name="emp_profile_old"  value="<?php echo $emp_profile; ?>">
                    <div class="form-footer text-center">
                    <input type="hidden" name="updateemployee" value="updateemployee">
                    <input type="Hidden" name="emp_id" value="<?php echo $emp_id;?>">
                    <input type="submit" id="socAddBtn" class="btn btn-primary" name="" value="<?php echo $xml->string->update; ?>">
                  </div>
                  <?php }else{ ?>
                <div class="form-footer text-center">
                    <input type="hidden" name="addemployee" value="addemployee">
                    <button type="submit" id="socAddBtn" value="" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> <?php echo $xml->string->reset; ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->
    </div><?php }?>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->

    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
      var fileExtension = ['jpeg', 'jpg', 'png', 'jpeg'];

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

    function readURLId(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#idproof').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  function readURLId1(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#idproof1').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#imgInp").change(function() {
    var fileExtension = ['jpeg', 'jpg', 'png'];
    var fileExt= $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(fileExt, fileExtension) == -1) {
    } else {
      readURL(this);
    }
  });

  $("#imgInpId").change(function() {
    var fileExt= $(this).val().split('.').pop().toLowerCase();
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      if(fileExt=='pdf') {
        $('#idproof').attr('src', 'img/pdf.png');
      } else  if(fileExt=='doc' || fileExt=='docx')  {
        $('#idproof').attr('src', 'img/doc.png');
      }
    } else {
      readURLId(this);
    }
    // readURLId(this);
    // alert('afd');
  });

  $("#imgInpId1").change(function() {
    var fileExt= $(this).val().split('.').pop().toLowerCase();
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      if(fileExt=='pdf') {
        $('#idproof1').attr('src', 'img/pdf.png');
      } else  if(fileExt=='doc' || fileExt=='docx')  {
        $('#idproof1').attr('src', 'img/doc.png');
      }
    } else {
      readURLId1(this);
    }
    // alert('afd');
  });
</script>


<script type="text/javascript">

      
$('#emp_type_id').on('change', function() {
//  alert( this.value );

  if(this.value=="1" || this.value=="0"){
     $('#gate_number_details_lable').css('display','block');
     $('#gate_number_details_div').css('display','block');
      
     
    
  } else {
    $('#gate_number_details_lable').css('display','none');
    $('#gate_number_details_div').css('display','none');
    
  }
});

 

  

  </script>
    <?php
     if ($emp_type==1) { ?>
      <script type="text/javascript">

      $(document).ready(function() {

          $('#passwordDiv').hide();
          // $('#empUsers').hide();
          $('#empSalary').hide();
          $('#empSalary1').hide();
          // $('#empUsers').show();
        
      });

  </script>
  
  <?php } else if ($emp_type==0) { ?>
      <script type="text/javascript">
      $(document).ready(function() {
          $('#empUsers').hide();
          $('#passwordDiv').hide();
      });

  </script>
  
  <?php }
    
     if ($emp_type_id!=0) { ?>
      <script type="text/javascript">
      $(document).ready(function() {

          $('#passwordDiv').hide();
          $('#userType').hide();
          $('.check').change(function(){
            var data= $(this).val();

             if(data==1) {
              $('#passwordDiv').show();
             } else {
              $('#passwordDiv').hide();
             }          
          });
        
      });

  </script>
  
  <?php }  else if ($emp_type_id==0 && isset($updateemployee)) { ?>
    <script type="text/javascript">

    $(document).ready(function() {
       $('#userType').hide();
       $('#passwordDiv').show();
      $('.check').change(function(){
        var data= $(this).val();

         if(data==1) {
          $('#passwordDiv').show();
         } else {
          $('#passwordDiv').hide();
         }          
      });
    

       $('.check1').change(function(){
        var data= $(this).val();

         if(data==1) {
          $('#passwordDiv').hide();
          $('#empSalary').hide();
          $('#empSalary1').hide();
          $('#gate_number_details_lable').hide();
          $('#gate_number_details_div').hide();
          $('#userType').show();
          $('#societyType').hide();
          $('#empUsers').show();
         } else {
          $('#emp_type_id').prop('selectedIndex', 0);
          $('#societyType').show();
          $('#userType').hide();
          $('#empSalary').show();
          $('#empSalary1').show();
          $('#empUsers').hide();
         }          
      });
  });

  </script>
  <?php } else { ?>
    <script type="text/javascript">

    $(document).ready(function() {

       $('#userType').hide();
       $('#passwordDiv').hide();
      $('.check').change(function(){
        var data= $(this).val();
         if(data==1) {
          $('#passwordDiv').show();
         } else {
          $('#passwordDiv').hide();
         }          
      });
    

       $('.check1').change(function(){
        var data= $(this).val();

         if(data==1) {
          $('#passwordDiv').hide();
          $('#empSalary').hide();
          $('#empSalary1').hide();
          $('#userType').show();
          $('#societyType').hide();
         } else {

          $('#societyType').show();
          $('#userType').hide();
          $('#empSalary').show();
          $('#empSalary1').show();
          $('#emp_type_id').prop('selectedIndex', 0);
         }          
      });
  });

  </script>
  <?php } ?>

  <script type="text/javascript">
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
  </script>
  <?php } 
//IS_1498?>

<div class="modal fade" id="mpinModelChange">
        <div class="modal-dialog">
          <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
              <h5 class="modal-title text-white">Change Login Mpin</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div >
                <form class="d-inline" id="mpinForm" action="controller/employeeController.php" method="post">
                  <input type="hidden" value="<?php echo $emp_id; ?>" class="emp_id" id="emp_id" name="emp_id">
                  <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">New Mpin<span class="required">*</span></label>
                    <div class="col-sm-8" >
                      <input type="text" required=""  id="mpin" class="form-control onlyNumber" inputmode="numeric" name="mpin"  maxlength="4">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Confirm Mpin<span class="required">*</span></label>
                    <div class="col-sm-8" >
                      <input type="text" required="" id="cmpin" class="form-control onlyNumber" inputmode="numeric" name="cmpin"  maxlength="4">
                    </div>
                  </div>
                  <div class="text-center">
                    <input type="hidden" value="setMpinNew" name="setMpinNew">
                      <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save</button>
                  </div>
                  </form>
                  
              </div>

            </div>

          </div>
        </div>
      </div>