<?php
if((isset($_GET['PCId']) && !empty($_GET['PCId']) && $_GET['PCId'] != 0) || (isset($_GET['PSCId']) && !empty($_GET['PSCId']) && $_GET['PSCId'] != 0))
{
    $product_category_id = $_GET['PCId'];
    $product_sub_category_id = $_GET['PSCId'];
}
elseif((isset($_COOKIE['PCId']) && $_COOKIE['PCId'] != "" && $_COOKIE['PCId'] != 0) || (isset($_COOKIE['PSCId']) && $_COOKIE['PSCId'] != "" && $_COOKIE['PSCId'] != 0))
{
    if(isset($_COOKIE['PCId']))
    {
        $product_category_id = $_COOKIE['PCId'];
    }
    if(isset($_COOKIE['PSCId']))
    {
        $product_sub_category_id = $_COOKIE['PSCId'];
    }
}

if(isset($product_category_id) && !empty($product_category_id) && !ctype_digit($product_category_id))
{
    $_SESSION['msg1'] = "Invalid Category Id!";
    ?>
    <script>
        window.location = "manageProduct";
    </script>
<?php
exit;
}

if(isset($product_sub_category_id) && !empty($product_sub_category_id) && !ctype_digit($product_sub_category_id))
{
    $_SESSION['msg1'] = "Invalid Sub Category Id!";
    ?>
    <script>
        window.location = "manageProduct";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Product</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addProduct" method="post" id="addProductBtnForm">
                        <input type="hidden" name="addProductBtn" value="addProductBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteProducts');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormProduct">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Category </label>
                    <select class="form-control single-select" id="PCId" name="PCId" required onchange="getSubCatList(this.value);">
                        <option value="">-- Select --</option>
                        <?php
                            $qc = $d->selectRow("pcm.product_category_id,pcm.category_name","product_category_master AS pcm","pcm.product_category_delete = 0");
                            while ($qd = $qc->fetch_assoc())
                            {
                        ?>
                        <option <?php if($product_category_id == $qd['product_category_id']) { echo 'selected'; } ?> value="<?php echo $qd['product_category_id'];?>"><?php echo $qd['category_name'];?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Sub Category </label>
                    <select class="form-control single-select" id="PSCId" name="PSCId">
                        <option value="">-- Select --</option>
                        <?php
                        if(isset($product_category_id) && $product_category_id != 0 && $product_category_id != "")
                        {
                            $qa = $d->selectRow("product_sub_category_id,sub_category_name","product_sub_category_master","product_category_id = '$product_category_id' AND product_sub_category_delete = 0");
                            while ($qda = $qa->fetch_assoc())
                            {
                        ?>
                        <option <?php if($product_sub_category_id == $qda['product_sub_category_id']) { echo 'selected'; } ?> value="<?php echo $qda['product_sub_category_id']; ?>"><?php echo $qda['sub_category_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Product Name</th>
                                        <th>Category Name</th>
                                        <th>Sub Category Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $append = "";
                                    if(isset($product_category_id) && $product_category_id != 0 && $product_category_id != "")
                                    {
                                        $append .= " AND rpm.product_category_id = '$product_category_id'";
                                    }
                                    if(isset($product_sub_category_id) && $product_sub_category_id != 0 && $product_sub_category_id != "")
                                    {
                                        $append .= " AND rpm.product_sub_category_id = '$product_sub_category_id'";
                                    }
                                    $q = $d->selectRow("rpm.*,pcm.category_name,pscm.sub_category_name,ropm.order_product_id","retailer_product_master AS rpm JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id LEFT JOIN product_sub_category_master AS pscm ON pscm.product_sub_category_id = rpm.product_sub_category_id LEFT JOIN retailer_order_product_master AS ropm ON ropm.product_id = rpm.product_id","1=1 ".$append,"GROUP BY rpm.product_id ORDER BY product_id DESC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php
                                            if(empty($order_product_id) && $order_product_id == "")
                                            {
                                            ?>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['product_id']; ?>">
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input type="hidden">
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form action="manageProductVariant">
                                                    <input type="hidden" name="PId" value="<?php echo $product_id; ?>">
                                                    <input type="hidden" name="PCId" value="<?php echo $product_category_id; ?>">
                                                    <button type="submit" class="btn btn-sm btn-primary" Title="View Variants"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </div>
                                            <div style="display: inline-block;">
                                                <form action="addProduct" method="post">
                                                    <input type="hidden" name="product_id" value="<?php echo $row['product_id']; ?>">
                                                    <input type="hidden" name="editProduct" value="editProduct">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if(empty($order_product_id) && $order_product_id == "")
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form action="controller/orderProductController.php" method="POST">
                                                    <input type="hidden" name="product_id" value="<?php echo $row['product_id']; ?>">
                                                    <input type="hidden" name="product_category_id" value="<?php echo $product_category_id; ?>">
                                                    <input type="hidden" name="product_sub_category_id" value="<?php echo $product_sub_category_id; ?>">
                                                    <input type="hidden" name="product_image" value="<?php echo $product_image; ?>">
                                                    <input type="hidden" name="deleteProduct" value="deleteProduct">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $product_name; ?></td>
                                        <td><?php echo $category_name; ?></td>
                                        <td><?php echo $sub_category_name; ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>
function getSubCatList(product_category_id)
{
    $.ajax({
        url: 'controller/orderProductController.php',
        data: {getSubCatList:"getSubCatList",product_category_id:product_category_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#PSCId').empty();
            response = JSON.parse(response);
            $('#PSCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#PSCId').append("<option value='"+value.product_sub_category_id+"'>"+value.sub_category_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var product_category_id = $('#PCId').val();
    var product_sub_category_id = $('#PSCId').val();
    if(product_category_id != "" && product_category_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'product_category_id',
            value: product_category_id
        }).appendTo('#addProductBtnForm');
    }
    if(product_sub_category_id != "" && product_sub_category_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'product_sub_category_id',
            value: product_sub_category_id
        }).appendTo('#addProductBtnForm');
    }
    $("#addProductBtnForm").submit()
}
</script>