<?php
extract(array_map("test_input" , $_REQUEST));
error_reporting(0);

if (isset($view) && $view == "trash") { ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2">
      <div class="col-sm-8 col-12  pb-2">
        <h4 class="page-title">Deleted <?php echo $xml->string->employees; ?></h4>
      </div>

      <div class="col-sm-4 col-7  pb-2">
        <div class="btn-group float-sm-right">
          <a href="employee" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <?php
          if (!isset($emp_type_id)) {
          $emp_type_id='All';
          }
          ?>

          <a href="employees" class="btn btn-danger btn-sm"><i class="fa fa-angle-double-left"></i> View Active</a>

        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row pt-2">
      <div class="col-sm-5 col-12  pb-2">
        <form class="form-inline" action="#">
          <input type="hidden" name="emp_type" value="<?php echo $emp_type; ?>">
          <input type="hidden" name="emp_type_id" value="<?php echo $emp_type_id; ?>">
          <input type="hidden" name="view" value="<?php echo 'trash'; ?>">
        <input required="" type="text" style="width: 88%;"  class="form-control" autocomplete="off" name="searchEmployee" placeholder="Search Name Or Mobile" value="<?php echo $_REQUEST['searchEmployee']; ?>">
        <button type="submit" style="padding: 10px 12px;margin-left: 5px;" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
        </form>
      </div>


    </div>
    <div class="row" id="searchedEmployees">
      <?php
      if (isset($searchEmployee) && $searchEmployee!='') {
        $searchEmployee=mysqli_real_escape_string($con, $searchEmployee);
        $appendQueryAll = "emp_status=0 AND society_id='$society_id' $appendQueryNew AND emp_name LIKE '%$searchEmployee%' OR emp_status=0 AND society_id='$society_id' $appendQueryNew AND emp_mobile LIKE '%$searchEmployee%' ";
      } else {
        $appendQueryAll = "emp_status=0 AND society_id='$society_id'";
      }

      $q=$d->select("employee_master","  $appendQueryAll","ORDER BY emp_name ASC");


      $icardAry = array();
      $icardAryName = array();
      $i=0;
      if(mysqli_num_rows($q)>0) {
      while ($row=mysqli_fetch_array($q)) {
      extract($row);

      $today = date("Y-m-d");
      $last_time = date("Y-m-d", strtotime($in_out_time));

      $earlier = new DateTime($today);
      $later = new DateTime($last_time);
      $diff = $later->diff($earlier)->format("%a");

      if ($diff>29) {
          $color_status='red-border';
      } else if ($diff>15) {
          $color_status='yellow-border';
      } else {
           $color_status='green-border';
      }


      array_push($icardAry, $emp_id);
      $EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
      $empTypeData=mysqli_fetch_array($EW);
      if ($empTypeData['emp_type_name']!='') {
                  $type= $empTypeData['emp_type_name'];
                  } else {
                  $type= "Security Guard";
                  } 
      array_push($icardAryName, $emp_name.'-'.$type);

      ?>
      <div class="col-lg-3 col-6">
        <div class="card radius-15 <?php echo $color_status; ?>">
          <div class="p-2 text-center">
            <div class="p-2 border radius-15">
              <a href="employee?manageEmployeeUnit=yes&emp_id=<?php echo $emp_id; ?>">
              <img onerror="this.src='img/user.png'"  src="img/user.png"  data-src="../img/emp/<?php echo $emp_profile; ?>" width="110" height="110" class="rounded-circle shadow lazyload" alt="">
              </a>
              <h5 class="mb-0 mt-3"><?php echo custom_echo($emp_name,25); ?></h5>
              <p class="mb-1"><?php echo $row['role_name']; if($row['user_type']==1) { echo " (Representative)";} ?></p>
              <p class="mb-1"><?php if ($empTypeData['emp_type_name']!='') {
                  echo custom_echo($empTypeData['emp_type_name'],10);
                  } else {
                  echo "Security Guard";
                  }  ?> </p>
              <p class="mb-1"><?php echo $row['country_code'] .' '.$row['emp_mobile'] ?> <?php if($emp_token !='' && $emp_type==0 && $device_lock=="0"){ echo "<i class='fa fa-lock'></i>";  } if($emp_token !='' && $emp_type==0 && $device_lock=="1") { echo "<i class='fa fa-unlock'></i>"; } ?></p>

              <!-- <div class="list-inline contacts-social mt-3 mb-3"> <a href="javascript:;" class="list-inline-item bg-facebook text-white border-0"><i class="bx bxl-facebook"></i></a>

              </div> -->
              <div class="d-grid"> <a href="employee?manageEmployeeUnit=yes&emp_id=<?php echo $emp_id; ?>" class="btn btn-sm btn-outline-danger radius-15">View Details</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } } else {
      echo "<img src='img/no_data_found.png'>";
      }

      ?>
    </div>
  </div>
</div>
<?php }  else {

if ($emp_type!='') {
  $emp_type=(int)$emp_type;
}
$emp_type_id = (int)$_REQUEST['emp_type_id'];
$notiArrayId= array();
$qn = $d->select("employee_in_out_notification","admin_id='$_COOKIE[bms_admin_id]' AND empType=0");
while ($notiData=mysqli_fetch_array($qn)) {
  array_push($notiArrayId, $notiData['emp_id']);
}

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2">
      <div class="col-sm-3 col-6  pb-2">
        <h4 class="page-title"><?php echo $xml->string->employees; ?></h4>
      </div>
      <div class="col-sm-4 col-6  pb-2 ">
          <form class="form-inline" method="post" action="controller/employeeController.php">
             <label for="email">Sort By : </label>
            <input type="hidden" name="changeSortBy" value="<?php echo 'changeSortBy'; ?>">
            <input type="hidden" name="emp_type" value="<?php echo $emp_type; ?>">
            <input type="hidden" name="emp_type_id" value="<?php echo $emp_type_id; ?>">
            <select name="sortby" onchange="this.form.submit()">
              <option <?php if(0==$_COOKIE['sortby']) { echo 'selected'; } ?> value="0">Name (A-Z)</option>
              <option <?php if(1==$_COOKIE['sortby']) { echo 'selected'; } ?> value="1">Date (Desc)</option>
            </select>
          </form>
      </div>
      <div class="col-sm-5 col-7  pb-2">
        <div class="btn-group float-sm-right">
          <a href="employee" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <?php
          if (!isset($emp_type_id)) {
          $emp_type_id='All';
          }
          ?>
          <a href="#" data-toggle="modal" data-target="#printIcard"  class="btn btn-warning btn-sm"><i class="fa fa-address-card-o"></i> I-Card</a>
          <a href="employees?view=trash" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> View Deleted</a>

        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row pt-2">
      <div class="col-sm-5 col-12  pb-2">
        <form class="form-inline" action="#">
          <input type="hidden" name="emp_type" value="<?php echo $emp_type; ?>">
          <input type="hidden" name="emp_type_id" value="<?php echo $emp_type_id; ?>">
        <input required="" type="text" style="width: 88%;"  class="form-control" autocomplete="off" name="searchEmployee" placeholder="Search Name Or Mobile" value="<?php echo $_REQUEST['searchEmployee']; ?>">
        <button type="submit" style="padding: 10px 12px;margin-left: 5px;" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <!-- <div class="col-sm-3 col-6  pb-2">
        <form class="form-inline" action="" method="get">
          <input type="hidden" name="emp_type_id" value="<?php echo $emp_type_id; ?>">
           <input type="hidden" name="searchEmployee" value="<?php echo $_REQUEST['searchEmployee']; ?>">
          <select class="form-control select-sm single-select" name="emp_type" id="empType2" onchange="this.form.submit()">
            <option <?php if($emp_type=='') { echo 'selected';} ?>  value=""> -- <?php echo $xml->string->all; ?> --</option>
            <option  <?php if($emp_type=='0') { echo 'selected';} ?> value="0"><?php echo $xml->string->society; ?></option>
            <option  <?php if($emp_type=='1') { echo 'selected';} ?> value="1"><?php echo $xml->string->resident; ?></option>
          </select>
        </form>
      </div> -->
      <div class="col-sm-4 col-6  pb-2">
        <form class="form-inline" action="" method="get">
          <input type="hidden" name="emp_type" value="<?php echo $emp_type; ?>">
          <input type="hidden" name="searchEmployee" value="<?php echo $_REQUEST['searchEmployee']; ?>">
          <select class="form-control select-sm single-select" name="emp_type_id" id="empType" onchange="this.form.submit()">
            <option value=""> -- <?php echo $xml->string->all; ?> --</option>
            <?php
            if ($emp_type==1) {
              $appendTest= " AND emp_type_id!=1";
            }

            $q=$d->select("emp_type_master","society_id='$society_id' $appendTest OR society_id=0 $appendTest","");
            while ($row=mysqli_fetch_array($q)) {
            ?>
            <option <?php if($emp_type_id==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
            <?php }?>
          </select>
        </form>
      </div>
    </div>
    <div class="row" id="searchedEmployees">
      <?php
      $i++;
        $emp_type_idIcard= $emp_type_id;
        $queryAry=array();
        if ($emp_type!='' || $emp_type=='0') {
          $append_query="emp_type='$emp_type'";
          array_push($queryAry, $append_query);
        }

        if ($emp_type_id != "" && $emp_type_id != "0") {
          if ($emp_type_id ==1) {
            $append_query1 ="emp_type_id=0";
          } else {
            $append_query1 ="emp_type_id=$emp_type_id";

          }
          array_push($queryAry, $append_query1);

        }


        $appendQuery=  implode(" AND ", $queryAry);
         if ($appendQuery!="") {
          $appendQueryNew = " AND $appendQuery";
        }

        if (isset($_COOKIE['sortby']) && $_COOKIE['sortby']==0) {
           $orderByQuery = "ORDER BY emp_name ASC";
        }  else if (isset($_COOKIE['sortby']) && $_COOKIE['sortby']==1) {
           $orderByQuery = "ORDER BY emp_id DESC";
        }else {
           $orderByQuery = "ORDER BY emp_name ASC";
        }
      if (isset($searchEmployee) && $searchEmployee!='') {
        $searchEmployee=mysqli_real_escape_string($con, $searchEmployee);
        $appendQueryAll = "emp_status=1 AND society_id='$society_id' $appendQueryNew AND emp_name LIKE '%$searchEmployee%' OR emp_status=1 AND society_id='$society_id' $appendQueryNew AND emp_mobile LIKE '%$searchEmployee%' ";
      } else {
        $appendQueryAll = "emp_status=1 AND society_id='$society_id' $appendQueryNew";
      }

      $q=$d->select("employee_master"," $appendQueryAll","$orderByQuery");

      $icardAry = array();
      $icardAryName = array();
      $i=0;
      if(mysqli_num_rows($q)>0) {
      while ($row=mysqli_fetch_array($q)) {
      extract($row);

      $today = date("Y-m-d");
      $last_time = date("Y-m-d", strtotime($in_out_time));

      $earlier = new DateTime($today);
      $later = new DateTime($last_time);
      $diff = $later->diff($earlier)->format("%a");

      if ($diff>29) {
          $color_status='red-border';
      } else if ($diff>15) {
          $color_status='yellow-border';
      } else {
           $color_status='green-border';
      }


      array_push($icardAry, $emp_id);
      $EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
      $empTypeData=mysqli_fetch_array($EW);
      if ($empTypeData['emp_type_name']!='') {
                  $type= $empTypeData['emp_type_name'];
                  } else {
                  $type= "Security Guard";
                  }
      array_push($icardAryName, $emp_name.'-'.$type);

      ?>
      <div class="col-lg-3 col-6">
        <div class="card radius-15 <?php echo $color_status; ?>">
          <div class="p-2 text-center">
            <div class="p-2 border radius-15">
              <a href="employee?manageEmployeeUnit=yes&emp_id=<?php echo $emp_id; ?>">
              <img onerror="this.src='img/user.png'"  src="img/user.png"  data-src="../img/emp/<?php echo $emp_profile; ?>" width="110" height="110" class="rounded-circle shadow lazyload" alt="">
              </a>
              <h6 class="mb-0 mt-3"><?php echo custom_echo($emp_name,25); ?></h6>
              <p class="mb-1"><?php echo $row['role_name']; if($row['user_type']==1) { echo " (Representative)";} ?></p>
              <p class="mb-1"><?php if ($empTypeData['emp_type_name']!='') {
                  echo custom_echo($empTypeData['emp_type_name'],10);
                  } else {
                  echo "Security Guard ";
                   if($emp_token !='' && $emp_type==0 && $device_lock=="0"){ echo "<i class='fa fa-lock'></i>";  } if($emp_token !='' && $emp_type==0 && $device_lock=="1") { echo "<i class='fa fa-unlock'></i>"; }
                  }   ?> <?php
                    echo "<br>";
                   if ($entry_status==1) {
                    echo "<span class='badge badge-success m-1'>Inside</span>";
                    } elseif ($entry_status==2) {
                    echo "<span class='badge badge-danger  m-1'>Outside</span>";
                    } else {
                      echo "<Br>";
                    } ?></p>
              <p class="mb-1"><?php echo $xml->string->in_out; ?> <?php echo $xml->string->notifications; ?>
                <span class='empSpan<?php echo $emp_id;?>'>
                <?php
                 if(in_array($emp_id, $notiArrayId)){  ?>
                 <a href="javascript:void(0)"  onclick="notGetEmpNotification('<?php echo $emp_id;?>');" > <i class="fa fa-bell" aria-hidden="true"></i> </a>
                <?php } else { ?>
                  <a href="javascript:void(0)"  onclick="getEmpNotification('<?php echo $emp_id;?>');" > <i class="fa fa-bell-slash" aria-hidden="true"></i> </a>
                <?php } ?>
                </span>
              </p>
              <!-- <div class="list-inline contacts-social mt-3 mb-3"> <a href="javascript:;" class="list-inline-item bg-facebook text-white border-0"><i class="bx bxl-facebook"></i></a>

              </div> -->
              <div class="d-grid"> <a href="employee?manageEmployeeUnit=yes&emp_id=<?php echo $emp_id; ?>" class="btn btn-sm btn-outline-danger radius-15">View Details</a>
              </div>
            </div>
          </div>
        </div>

      </div>
      <?php } } else {
      echo "<img src='img/no_data_found.png'>";
      }

      ?>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
function getSearchEmployee(argument) {
var empType = $('#empType').val();
var empType2 = $('#empType2').val();

$.ajax({
url: "getEmployees.php",
cache: false,
type: "POST",
data: {argument : argument,emp_type_id:empType,emp_type:empType2},
success: function(response){
$('#searchedEmployees').html(response);
}
});
}


</script>

<div class="modal fade" id="printIcard">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->select; ?> <?php echo $xml->string->employees; ?> <?php echo $xml->string->for; ?> <?php echo $xml->string->print_i_card; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="eyeModelDiv">
        <li style="list-style: none;">
        <input type="checkbox" checked="checked" class="chkparent" id="selectAll"><label for="option"><?php echo $xml->string->check_uncheck; ?> <?php echo $xml->string->all; ?> </label><br>
        <ul style="list-style: none;">

        <?php

        for ($i1=0; $i1 <count($icardAry) ; $i1++) {

          ?>
          <li><label><input class="unit_id" value="<?php echo $icardAry[$i1]; ?>" id="unit_id<?php echo $i1; ?>"  checked="" type="checkbox" name="emp_id[]"> <?php echo $icardAryName[$i1]; ?>
            </label></li>
          <!-- <br> -->
        <?php    } ?>
        </ul>

        <div class="text-center">
          <?php if (count($icardAry)>0) {

            if ($emp_type_idIcard=='' || $emp_type_idIcard=="All" ) {
              $emp_type_id="All";
            } else {
              $emp_type_id = $emp_type_idIcard;
            }

          ?>
          <a href="#" onclick="printIcard()" class="btn btn-warning btn-sm"><i class="fa fa-address-card-o"></i> <?php echo $xml->string->print_i_card; ?></a>
        <?php }?>
        </div>
      </div>

    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>

<script>
    $("#selectAll").click(function() {
        $("input.unit_id").prop("checked", $(this).prop("checked"));
        // $('.unit_id:checkbox:checked')
    });

    $("input[type=checkbox]").click(function() {
        if (!$(this).prop("checked")) {
            $("#selectAll").prop("checked", false);
        }
    });

    function printIcard()  {
      var val = [];
          $(".unit_id:checked").each(function(i){
            val[i] = $(this).val();
          });
      window.open('printIcard.php?emp_id='+val+'&emp_type_id=<?php echo $emp_type_id; ?>', '_self');
    }
</script>
