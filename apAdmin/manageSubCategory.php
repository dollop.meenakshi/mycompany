<?php
if(isset($_GET['PCId']) && !empty($_GET))
{
    $product_category_id = $_GET['PCId'];
}
elseif(isset($_COOKIE['PCId']) && $_COOKIE['PCId'] != "" && $_COOKIE['PCId'] != 0)
{
    $product_category_id = $_COOKIE['PCId'];
}

if(isset($product_category_id) && !empty($product_category_id) && !ctype_digit($product_category_id))
{
    $_SESSION['msg1'] = "Invalid Category Id!";
    ?>
    <script>
        window.location = "manageSubCategory";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Sub Category</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addSubCategory" method="post" id="addSubCategoryBtnForm">
                        <input type="hidden" name="addSubCategoryBtn" value="addSubCategoryBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                </div>
            </div>
        </div>
        <form id="filterFormArea">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">Category </label>
                    <select name="PCId" id="PCId" class="form-control single-select" required>
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->select("product_category_master","product_category_delete = 0");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($product_category_id == $cd['product_category_id']) { echo 'selected';} ?> value="<?php echo $cd['product_category_id']; ?>"><?php echo $cd['category_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if(isset($product_category_id) && $product_category_id != 0)
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Sub Category Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $q = $d->selectRow("pscm.*,ropm.order_product_id","product_sub_category_master AS pscm LEFT JOIN retailer_product_master AS rpm ON rpm.product_sub_category_id = pscm.product_sub_category_id LEFT JOIN retailer_order_product_master AS ropm ON ropm.product_id = rpm.product_id","pscm.product_sub_category_delete = 0 AND pscm.product_category_id = '$product_category_id'","GROUP BY pscm.product_sub_category_id");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form action="addSubCategory" method="post">
                                                    <input type="hidden" name="product_sub_category_id" value="<?php echo $row['product_sub_category_id']; ?>">
                                                    <input type="hidden" name="editSubCategory" value="editSubCategory">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if(empty($order_product_id) && $order_product_id == "")
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form action="controller/categorySubCategoryController.php" method="POST">
                                                    <input type="hidden" name="product_category_id" value="<?php echo $row['product_category_id']; ?>">
                                                    <input type="hidden" name="product_sub_category_id" value="<?php echo $row['product_sub_category_id']; ?>">
                                                    <input type="hidden" name="deleteSubCategory" value="deleteSubCategory">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $sub_category_name; ?></td>
                                        <td><?php if($sub_category_description != ""){ echo $sub_category_description; } ?></td>
                                        <td>
                                            <?php
                                            if($product_sub_category_status == 0)
                                            {
                                            ?>
                                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['product_sub_category_id']; ?>','subCategoryDeactive');" data-size="small"/>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input type="checkbox" class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['product_sub_category_id']; ?>','subCategoryActive');" data-size="small"/>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Category</span>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>
function submitAddBtnForm()
{
    var product_category_id = $('#PCId').val();
    if(product_category_id != "" && product_category_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'product_category_id',
            value: product_category_id
        }).appendTo('#addSubCategoryBtnForm');
    }
    $("#addSubCategoryBtnForm").submit()
}
</script>