<?php error_reporting(0);
$id = $_REQUEST['id'];
$q=$d->select("meeting_master,users_master","meeting_master.society_id='$society_id' AND meeting_master.meeting_host_by=users_master.user_id AND meeting_master.meeting_id='$id' $blockAppendQueryUser");
if(mysqli_num_rows($q)>0){
$data=mysqli_fetch_array($q);
?>
<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row pt-2 pb-2">
			<div class="col-sm-12 col-md-12">
				<h4 class="page-title">Meeting Detail</h4>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex align-items-center">
						<h5 class="card-title"><?php echo $data['meeting_title'] ?></h5>
							<div class="ms-auto">
								<span class="mb-1 text-primary small badge bg-default"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d M Y", strtotime($data['meeting_date']));?></span>
								<span class="mb-1 text-primary small badge bg-default"> <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date("h:i A", strtotime($data['meeting_time']));?></span>
							</div>
						</div>
						<div class="d-flex align-items-center">
						<p class="card-text" >Host: <?php echo $data['user_full_name'] ?> (<?php echo $data['user_designation'] ?>)</p>
						<?php if($data['meeting_status'] == 0){
                                    $status = "Upcoming";
                                    $statusClass = "badge-primary";
                                }
                                else if($data['meeting_status'] == 1){
                                    $status = "Canceled";
                                    $statusClass = "badge-danger";
                                }else if($data['meeting_status'] == 2){
                                    $status = "Ongoing";
                                    $statusClass = "badge-info";
                                }else{
                                    $status = "Completed";
                                    $statusClass = "badge-success";
                                }
                            ?>
							<div class="ms-auto">
							<p class="card-title badge mb-20 <?php echo $statusClass; ?>" style=""><?php echo $status; ?></p>
							</div>
						</div>
						<p class="card-text"><b>Meeting Description:</b> <?php echo $data['meeting_description'] ?></p>
						
						<?php if($data['meeting_discussion'] != ''){ ?>
						<p class="card-text"><b>Meeting Discussion:</b> <?php echo $data['meeting_discussion'] ?></p>
						<?php } ?>
						<?php if($data['meeting_spend_time'] != ''){ ?>
						<p class="card-text"><b>Spend Time:</b> <?php echo $data['meeting_spend_time'] ?></p>
						<?php } ?>
						<?php 
						$userq=$d->select("meeting_members_master,users_master,floors_master,block_master","meeting_members_master.society_id='$society_id' AND meeting_members_master.user_id=users_master.user_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND meeting_members_master.meeting_id='$data[meeting_id]'","");
						if(mysqli_num_rows($userq)>0){
						?>
						<div class="row m-0">
						    <?php while ($userData=mysqli_fetch_array($userq)) { ?>
							<div class="col-6 col-12 col-lg-3 col-xl-3 pt-3">
								<div class="card gradient-scooter  no-bottom-margin">
									<div class="card-body text-center">
										<div class="row">
											<div class="col-3">
												<img id="blah" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $userData['user_profile_pic']; ?>" width="60" height="60" alt="your image" class="rounded-circle p-1 lazyload">
											</div>
											<div class="col-9">
												<h6 class="text-white mt-2 text-capitalize"><b><?php echo $userData['user_full_name']; ?></b> <?php if($userData['meeting_join']!=0){ ?><i title="Meeting Join" class="fa fa-check-circle text-dark" aria-hidden="true"></i><?php } ?></h6>
												<p class="text-white mb-0" style="font-size: 11px;font-style: normal;"><?php echo $userData['block_name']; ?> (<?php echo $userData['floor_name']; ?>) </p>
												<i class="text-white" style="font-size: 11px;font-style: normal;"><i class="fa fa-building"></i> <?php echo $userData['user_designation']; ?> </i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<?php 
						$dq=$d->select("meeting_documents_master","society_id='$society_id' AND meeting_id='$data[meeting_id]'","");
						if(mysqli_num_rows($dq)>0){ ?>
						<div class="row m-0 pt-3">
							<?php while ($docData=mysqli_fetch_array($dq)) { ?>
							<div class="col-6 col-lg-2 col-xl-2">
								<div class="card border-1px">
									<div class="card-body text-center p-1">
										<a href="../img/meeting_documents/<?php echo $docData['meeting_document'];?>" target="_blank">
											<?php
												$ext = pathinfo($docData['meeting_document'], PATHINFO_EXTENSION);
												if ($ext == 'pdf' || $ext == 'PDF') {
												$imgIcon = 'img/pdf.png';
												} else if ($ext == 'jpg' || $ext == 'jpeg') {
												$imgIcon = 'img/jpg.png';
												}else if ($ext == 'png') {
													$imgIcon = 'img/png.png';
												} else if($ext == 'png'){
												$imgIcon = 'img/png.png';
												}else if ($ext == 'doc' || $ext == 'docx') {
												$imgIcon = 'img/doc.png';
												}else if ($ext == 'xlsx') {
												$imgIcon = 'img/excel.png';
												}else if ($ext == 'pptx') {
													$imgIcon = 'img/ppt.png';
												}
												else{
												$imgIcon = 'img/doc.png';
												}
											?>
											<img width="70" height="70" src="<?php echo $imgIcon;?>" alt="">
											<p class="small pt-2"><?php echo $docData['meeting_document'];?></p>
										</a>
									</div>
								</div>
							</div>
							<?php } ?>		
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- Meeting Document Section -->
		</div><!-- End Row-->
		
</div>
<!-- End container-fluid-->
<?php } ?>


