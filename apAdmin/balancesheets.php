
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
      <div class="col-sm-3">
        <h4 class="page-title"><?php echo $xml->string->balancesheets; ?></h4>
     </div>
     <div class="col-sm-6">
       <div class=""><span ><?php echo $xml->string->cash_on_hand; ?> <?php echo $currency; ?> <?php
          $totalIncome12=0;
          $sum=0;
          $countev=$d->sum_data("recived_amount - transaction_charges","event_attend_list","society_id='$society_id' ");
          $rowev=mysqli_fetch_array($countev);
          $asif=$rowev['SUM(recived_amount - transaction_charges)'];
          $totalEventBooking=number_format($asif,2,'.','');
          $count2=$d->sum_data("receive_amount - transaction_charges","facilitybooking_master","payment_status=0 AND society_id='$society_id' ");
          $row2=mysqli_fetch_array($count2);
          $asif2=$row2['SUM(receive_amount - transaction_charges)'];
          $totalFac=number_format($asif2,2,'.','');

          $count7111=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' ");
          $row=mysqli_fetch_array($count7111);
          $asif1111=$row['SUM(penalty_amount)'];
          $totalPenalaty111=number_format($asif1111,2,'.','');

          $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' ");
          $row2=mysqli_fetch_array($icnome);
          $asif5=$row2['SUM(income_amount)'];
          $totalIncome=number_format($asif5,2,'.','');

          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0");
          $row=mysqli_fetch_array($count6);
          $totalWalletAmount=$row['SUM(credit_amount-debit_amount)'];
          $totalWalletAmount= number_format($totalWalletAmount,2,'.','');

          $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty111+$totalWalletAmount;

          $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' ");
          $row5=mysqli_fetch_array($count4);
          $asif5=$row5['SUM(expenses_amount)'];
          $totalIncome11=$totalIncome-$totalExp=number_format($asif5,2,'.','');
           echo  number_format($totalIncome11,2);
           ?> </span>
      </div>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="balancesheet" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
         <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteBalance');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>  -->
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->balancesheet; ?></th>
                        <th><?php echo $xml->string->current_balance; ?></th>
                        <th><?php echo $xml->string->online; ?> <?php echo $xml->string->payment; ?></th>
                        <th><?php echo $xml->string->action; ?></th>
                        <th><?php echo $xml->string->block_type; ?></th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  $q=$d->select("balancesheet_master","society_id='$society_id'","ORDER BY balancesheet_id DESC");
                  while ($empData=mysqli_fetch_array($q)) {
                    if ($adminData['admin_type']==1 || $empData['block_id']==0 ||  count($blockAryAccess)==0 || in_array($empData['block_id'], $blockAryAccess)) {

                          $countev=$d->sum_data("recived_amount - transaction_charges","event_attend_list","society_id='$society_id' AND balancesheet_id='$empData[balancesheet_id]'");
                          $rowev=mysqli_fetch_array($countev);
                          $asif=$rowev['SUM(recived_amount - transaction_charges)'];
                          $totalEventBooking=number_format($asif,2,'.','');
                          $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$empData[balancesheet_id]' ");
                          $row=mysqli_fetch_array($count7);
                          $asif=$row['SUM(penalty_amount)'];
                          $totalPenalaty=number_format($asif,2,'.','');
                          $count2=$d->sum_data("receive_amount - transaction_charges","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$empData[balancesheet_id]'");
                          $row2=mysqli_fetch_array($count2);
                            $asif2=$row2['SUM(receive_amount - transaction_charges)'];
                          $totalFac=number_format($asif2,2,'.','');

                          $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$empData[balancesheet_id]'");
                          $row2=mysqli_fetch_array($icnome);
                              $asif5=$row2['SUM(income_amount)'];
                          $totalIncome=number_format($asif5,2,'.','');

                          $totalIncome=  $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

                          $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$empData[balancesheet_id]'");
                              $row5=mysqli_fetch_array($count4);
                                $asif5=$row5['SUM(expenses_amount)'];

                          //IS_588 cmt below line
                         //  $totalIncome-$totalExp=number_format($asif5,2,'.','');
                          $ExpAmount = $row5['SUM(expenses_amount)'];
                           $totalIncome=number_format(($totalIncome -$ExpAmount ),2,'.','');
                           //IS_588
                         ?>
                    <tr>
                      <!--  <td class="text-center">
                        <?php if ($totalIncome==0): ?>
                                  <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $empData['balancesheet_id']; ?>">
                        <?php endif ?>
                        </td> -->
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $empData['balancesheet_name']; ?></td>
                        <td><?php echo number_format($totalIncome,2);?></td>
                        <td><?php if ($empData['society_payment_getway_id']!=0 || $empData['society_payment_getway_id_upi']!=0) {
                          echo "<span class='badge badge-success '>Accept </span>";
                        } else {
                          echo "<span class='badge badge-danger'>Not Accept</span>";
                        };?></td>
                    <td >
                      <form action="viewBalancesheet" method="post" style="display: inline-block;" >
                        <input type="hidden" name="balancesheet_id" value="<?php echo $empData['balancesheet_id']; ?>">
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View</button>
                      </form>
                      <?php
                        if ($adminData['admin_type']==1 || $empData['balncesheet_created_by']==$_COOKIE['bms_admin_id']) {
                       ?>
                      <form action="balancesheet" method="post" style="display: inline-block;">
                        <input type="hidden" name="balancesheet_id_edit" value="<?php echo $empData['balancesheet_id']; ?>">
                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> </button>
                      </form>
                     <?php //IS_585  </td><td> removed?>
                     <?php //IS_585  style="display: inline-block;" added
                     $balancesheet_used_event = $d->count_data("events_day_id","event_days_master","balancesheet_id = '$empData[balancesheet_id]'");
                     $cnt = $balancesheet_used_event->fetch_assoc();

                     $balancesheet_used_facility = $d->count_data("booking_id","facilitybooking_master","balancesheet_id = '$empData[balancesheet_id]'");
                     $cnt_facility = $balancesheet_used_facility->fetch_assoc();

                     $balancesheet_used_penalty = $d->count_data("booking_id","facilitybooking_master","balancesheet_id = '$empData[balancesheet_id]'");
                     $cnt_penalty = $balancesheet_used_penalty->fetch_assoc();

                     if(($cnt['COUNT(*)'] == 0 || $cnt_facility['COUNT(*)'] == 0 || $cnt_penalty['COUNT(*)'] == 0) && $totalIncome == 0.00)
                     {
                     ?>
                      <form action="controller/balancesheetController.php" method="post" style="display: inline-block;" >
                        <input type="hidden" name="balancesheet_id_delete" value="<?php echo $empData['balancesheet_id']; ?>">
                        <input type="hidden" name="balancesheet_name_delete" value="<?php echo $empData['balancesheet_name']; ?>">
                        <button type="submit" class="btn btn-danger form-btn btn-sm"><i class="fa fa-trash-o"></i> </button>
                      </form>
                    <?php }
                      else
                      {
                      ?>
                        <button type="button" class="btn btn-danger btn-sm btnErrorBalance"><i class="fa fa-trash-o"></i></button>
                        <?php
                      }
                    } ?>
                      </td>
                      <td>
                         <?php
                        if ($empData['block_id']==0){
                          echo $xml->string->general;
                        } else {
                          $qs=$d->selectRow("block_name","block_master","block_id='$empData[block_id]'");
                          $blockData=mysqli_fetch_array($qs);
                          echo $blockData['block_name'].'-'.$xml->string->block;
                        } ?>
                      </td>
                    </tr>
                  <?php } } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



