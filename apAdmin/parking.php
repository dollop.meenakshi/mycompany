<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="parkingAdd" action="controller/parkingController.php" method="post">
              <h4 class="form-header text-uppercase">
                <i class="fa fa-car"></i>
                <?php echo $xml->string->add; ?>/<?php echo $xml->string->edit; ?> <?php echo $xml->string->parking; ?>
              </h4>
              <div class="form-group row">
                <?php //IS_600 <span class="required">*</span> ?>
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->parking; ?> <?php echo $xml->string->name; ?> <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input maxlength="40" required="" type="text" class="form-control  txtNumeric" id="input-10" name="socieaty_parking_name_add" placeholder="Ex: Basement 1">
                </div>
              </div>

             
              <div class="form-group row">
                <?php //IS_600 <span class="required">*</span> ?>
                <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->number_of; ?> <?php echo $xml->string->car_parking; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" maxlength="3" type="text" class="form-control" id="noofCar" name="total_car_parking">
                </div>
                <?php //IS_600 <span class="required">*</span> ?>
                <label for="input-13" class="col-sm-2 col-form-label"><?php echo $xml->string->number_of; ?> <?php echo $xml->string->bike_parking; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" maxlength="3" type="text" class="form-control" id="noofBike" name="total_bike_parking">
                </div>
              </div>

               <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label"><?php echo $xml->string->block_type; ?>  <i class="text-danger">*</i></label>
                  <div class="col-sm-10">
                    <div class="icheck-inline">
                      <input  required="" checked="" type="radio" id="inline-radio-info11 parkingType" value="0" name="parkingType">
                      <label for="inline-radio-info11"><?php echo $xml->string->mapping_with_name; ?></label>
                    </div>
                    <div class="icheck-inline">
                      <input  required="" type="radio" id="inline-radio-success11 parkingType " value="1" name="parkingType">
                      <label for="inline-radio-success11"> <?php echo $xml->string->without_mapping; ?></label>
                    </div>
                  </div>
                </div>

              <div class="form-group row" id="parkingNameDiv">
                <?php //IS_600 <span class="required">*</span> ?>
                <label for="input-16" class="col-sm-2 col-form-label"> <?php echo $xml->string->car_parking; ?>  <?php echo $xml->string->name; ?>  <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" maxlength="4"  type="text" class="form-control txtNumeric" id="input-16" name="car_parking_name" required="" placeholder="Ex: C" value="C">
                </div>
                <?php //IS_600 <span class="required">*</span> ?>
                <label for="input-17" class="col-sm-2 col-form-label"><?php echo $xml->string->bike_parking; ?>  <?php echo $xml->string->name; ?>  <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" maxlength="4" type="text" class="form-control txtNumeric" id="input-17" required="" name="bike_parking_name" placeholder="Ex: B" value="B"> 
                </div>
              </div>


              <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?> </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  $('[name="total_bike_parking"]').on('change blur keyup', function() {
    $('[name="total_car_parking"]').valid();
  });
  $('[name="total_car_parking"]').on('change blur keyup', function() {
    $('[name="total_bike_parking"]').valid();
  });
</script>