<?php
class firebase_admin
{   
    function  noti_new($society_id,$image,$registrationIds, $title, $body, $activity)
    {   

        $title = html_entity_decode($title);
        $body = html_entity_decode($body);

        if (is_array($registrationIds)) {
            $registrationIds=$registrationIds;
            $registrationIds = array_unique($registrationIds);
            $registrationIds = array_values($registrationIds);  
        } else {
            $registrationIds = array($registrationIds);
        }
        if ($image=='') {
            $image = $base_url."img/logo.png";
        }
        if ($base_url != '') {
            $small_icon = $base_url."img/logo.png";
        } else {
            $small_icon = "https://master.my-company.app/img/logo.png";
        }
        define('API_ACCESS_KEY_NEW', 'AAAAGMDWJJs:APA91bFIIkctvEO1lgnVTfpAkL7lexiWxkFCpwxPvm3Y-QJOH207VK45TsiOYnoP725QuYxi6rF-pKGwV8Fgi1GcVrldn1QX2LORMXr-VWVGHf0gW-FoNNFCIUvx-b4hHUftfwwn0_Zc');
        #prep the bundle
        // $msg = array(
        //     'click_action'     => $activity,
        //     'icon'    => 'myicon', /*Default Icon*/
        //     '    sound' => 'mySound' /*Default sound*/
        // );

        $msg_id = date("YmdHis");
        $msg_id = (int)$msg_id;

        $data = array(
            "image" => $image,
            'small_icon' => $small_icon,
            'body'     => $body,
            'click_action'     => $activity,
            'title'    => $title,
            'sound'=>'default',
            'msg_id'=>$msg_id,
            'society_id'=>$society_id,
        );
        $fields = array(
            // 'to'        => $registrationIds,
             'registration_ids' =>  $registrationIds,
            // 'notification'    => $data,
             'time_to_live' => 14400,
            'data'    => $data
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY_NEW,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $re = $result['success'];
        if ($re == 1) {
            // header("location:$url");
            return true;
        } else {
            // header("location:$url");
            return false;
        }
    }

    function  noti_ios_new($society_id,$image,$registrationIds, $title, $body, $activity)
    {
        
        $title = html_entity_decode($title);
        $body = html_entity_decode($body);
        
        if (is_array($registrationIds)) {
            $registrationIds=$registrationIds;
            $registrationIds = array_unique($registrationIds);
            $registrationIds = array_values($registrationIds);  
        } else {
            $registrationIds = array($registrationIds);
        }
       
        if ($image=='') {
            $image = $base_url."img/logo.png";
        }
        if ($base_url != '') {
            $small_icon = $base_url."img/logo.png";
        } else {
            $small_icon = "https://master.my-company.app/img/logo.png";
        }
        define('API_ACCESS_KEY_IOS', 'AAAAGMDWJJs:APA91bFIIkctvEO1lgnVTfpAkL7lexiWxkFCpwxPvm3Y-QJOH207VK45TsiOYnoP725QuYxi6rF-pKGwV8Fgi1GcVrldn1QX2LORMXr-VWVGHf0gW-FoNNFCIUvx-b4hHUftfwwn0_Zc');
        #prep the bundle
        // $msg = array(
        //     'click_action'     => $activity,
        //     'icon'    => 'myicon', /*Default Icon*/
        //     '    sound' => 'mySound' /*Default sound*/
        // );
         if ($title=='sos') {
            $alert = array(
                'body'  => $body,
                'click_action'     => $activity,
                'title'    => $title,
                'society_id' => $society_id,
                'sound'=>'beep_beep_beep.caf',
                'image' => $image,
                'small_icon' => $small_icon,
                'style' => 'picture',
                'picture' => $image,
            );
        }  else {
            $alert = array(
                'body'  => $body,
                'click_action'     => $activity,
                'title'    => $title,
                'society_id' => $society_id,
                'sound'=>'just-saying.caf',
                'image' => $image,
                'small_icon' => $small_icon,
                'style' => 'picture',
                'picture' => $image,
            );
        }   


        $data = array(
            "image" => $image,
            'body'     => $body,
            'click_action'     => $activity,
            'title'    => $title,
            'image' => $image,
            'style' => 'picture',
            'picture' => $image,
        );

        
        $fields = array(
            // 'to'        => $registrationIds,
            'registration_ids' =>  $registrationIds,
            'notification'    => $data,
            'notification' => $alert,
            'priority' => 'high',
            'data'    => $data
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY_IOS,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $re = $result['success'];
        if ($re == 1) {
            // header("location:$url");
            return true;
        } else {
            // header("location:$url");
            return false;
        }
    }


    
}
