<?php

class firebase_resident
{
    
    function  noti($menuClick,$image,$society_id,$registrationIds, $title, $body, $activity)
    {

        $title = html_entity_decode($title);
        $body = html_entity_decode($body);

        if (is_array($registrationIds)) {
            $registrationIds=$registrationIds;
            $registrationIds = array_unique($registrationIds);
             $registrationIds = array_values($registrationIds);  
        } else {
            $registrationIds = array($registrationIds);
        }
        
        if ($image=='') {
            $image= "";
        }

        if (is_array($activity) &&  $activity['userProfile']!='') {
            $small_icon=$activity['userProfile'];
        }elseif ($base_url != '') {
            $small_icon = $base_url."img/logo.png";
        } else {
            $small_icon = "https://master.my-company.app/img/logo.png";
        }
        define('API_ACCESS_KEY', 'AAAAGMDWJJs:APA91bFIIkctvEO1lgnVTfpAkL7lexiWxkFCpwxPvm3Y-QJOH207VK45TsiOYnoP725QuYxi6rF-pKGwV8Fgi1GcVrldn1QX2LORMXr-VWVGHf0gW-FoNNFCIUvx-b4hHUftfwwn0_Zc');

        $msg_id = date("YmdHis");
        $msg_id = (int)$msg_id;
           
        $alert = array(
             'body'  => $body,
            'click_action'     => $activity,
            'title'    => $title,
            'society_id' => $society_id,
            'sound'=>'default',
        );
        $data = array(
            'menuClick' => $menuClick,
            'image' => $image,
            'small_icon' => $small_icon,
            'body'     => $body,
            'click_action'     => $activity,
            'title'    => $title,
            'society_id' => $society_id,
            'sound'=>'default',
            'msg_id'=>$msg_id,

        );
      
        $fields = array(
            'title'=> $title,
            'registration_ids' =>  $registrationIds,
             // 'notification'     => $alert,
             'priority' => 'high',
             'time_to_live' => 14400,  
             'data'    => $data
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server	
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        //print_r($result);
        $re = $result['success'];
        if ($re == 1) {
            // header("location:$url");
            return true;
        } else {
            // header("location:$url");
            return false;
        }
    }
    function  noti_ios($menuClick,$image,$society_id,$registrationIds, $title, $body, $activity)
    {
        
        $title = html_entity_decode($title);
        $body = html_entity_decode($body);
        if (is_array($registrationIds)) {
            $registrationIds=$registrationIds;
            $registrationIds = array_unique($registrationIds);
            $registrationIds = array_values($registrationIds);  
        } else {
            $registrationIds = array($registrationIds);
        }
        if (is_array($activity) &&  $activity['userProfile']!='') {
            $small_icon=$activity['userProfile'];
        }elseif ($base_url != '') {
            $small_icon = $base_url."img/logo.png";
        } else {
            $small_icon = "https://master.my-company.app/img/logo.png";
        }
        define('API_ACCESS_KEY', 'AAAAGMDWJJs:APA91bFIIkctvEO1lgnVTfpAkL7lexiWxkFCpwxPvm3Y-QJOH207VK45TsiOYnoP725QuYxi6rF-pKGwV8Fgi1GcVrldn1QX2LORMXr-VWVGHf0gW-FoNNFCIUvx-b4hHUftfwwn0_Zc');
          
        if ($title=='sos') {
            $alert = array(
                'body'  => $body,
                'click_action'     => $activity,
                'title'    => $title,
                'society_id' => $society_id,
                'sound'=>'beep_beep_beep.caf',
                'image' => $image,
                'menuClick' => $menuClick,
                'style' => 'picture',
                'picture' => $image,
            );
        } else if (is_array($activity) && $activity['title']=='newVisitorApprove' || is_array($activity) && $activity['title']=='newChildApprove') {
            $alert = array(
                'body'  => $body,
                'click_action'     => $activity,
                'title'    => $title,
                'society_id' => $society_id,
                'sound'=>'Doorbell.caf',
                'image' => $image,
                'menuClick' => $menuClick,
                'style' => 'picture',
                'picture' => $image,
            );

        } else {
            $alert = array(
                'body'  => $body,
                'click_action'     => $activity,
                'title'    => $title,
                'society_id' => $society_id,
                'sound'=>'just-saying.caf',
                'image' => $image,
                'menuClick' => $menuClick,
                'style' => 'picture',
                'picture' => $image,
            );
        }   

        $data = array(
            'image' =>$image,
            'body'     => $body,
            'click_action'     => $activity,
            'title'    => $title,
            'society_id' => $society_id,
            'menuClick' => $menuClick,
        );
      
        $fields = array(
             'title'=> $title,
             'registration_ids' =>  $registrationIds,
             'priority' => 'high',
             'notification' => $alert,
             'data'    => $data
        );

        
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $re = $result['success'];
        // print_r($result);
        if ($re == 1) {
            // header("location:$url");
            return true;
        } else {
            // header("location:$url");
            return false;
        }
    }
}
