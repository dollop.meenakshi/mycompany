<?php error_reporting(0);

$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$_GET['month_year'] = $_REQUEST['month_year'];


?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row ">
            <div class="col-md-12 ">
                <h4 class="page-title">Document Read Status</h4>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if (isset($floor_id) && $floor_id > 0) {
                                $deptFilterQuery = " AND users_master.floor_id='$floor_id'";
                              }
                
                            if (isset($block_id) && $block_id > 0) {
                                $blockFilterQuery = " AND users_master.block_id='$block_id'";
                              }
                            if (isset($user_id) && $user_id > 0) {
                                $userFilterQuery = " AND users_master.user_id='$user_id'";
                              }
                            $i = 1;
                            $crDate = date('Y-m-d');
                            $curnTime  = date('H:i:s');
                            $q = $d->select("document_read_status LEFT JOIN hr_document_master ON hr_document_master.hr_document_id=document_read_status.hr_document_id, users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id", "users_master.user_id=document_read_status.user_id AND document_read_status.society_id='$society_id' AND document_read_status.hr_document_id='$hr_document_id' $deptFilterQuery $blockFilterQuery $userFilterQuery", "ORDER BY document_read_status.read_id DESC");
                            $counter = 1;
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Read Date</th>
                                        <th>Terms Conditions Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?><?php if ($data['user_designation'] != "") { echo "(" . $data['user_designation'] . ")"; } ?></td>
                                        <td><?php echo $data['floor_name'] . "(" . $data['block_name'] . ")"; ?></td>
                                        <td><?php if ($data['read_time'] != '0000-00-00' && $data['read_time'] != null) { echo date("d M Y", strtotime($data['read_time'])); } ?></td>
                                        <td><?php if ($data['terms_conditions_status'] != '' && $data['terms_conditions_status'] != null) {
                                                if ($data['terms_conditions_status'] == 0) { echo "No"; } else { echo "Yes ";
                                                    if ($data['terms_condition_accept_date'] != '0000-00-00' && $data['terms_condition_accept_date'] != null) {
                                                        echo "(".date("d M Y", strtotime($data['terms_condition_accept_date'])).")";
                                                    }
                                                }
                                            } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

        <div class="row ">
            <div class="col-md-12 ">
                <h4 class="page-title">Left To Read</h4>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php

                            if (isset($floor_id) && $floor_id > 0) {
                                $deptFilterQuery = " AND users_master.floor_id='$floor_id'";
                              }
                
                            if (isset($block_id) && $block_id > 0) {
                                $blockFilterQuery = " AND users_master.block_id='$block_id'";
                              }
                            if (isset($user_id) && $user_id > 0) {
                                $userFilterQuery = " AND users_master.user_id='$user_id'";
                              }
                            $userQuery = " AND users_master.user_id NOT IN(SELECT user_id FROM `document_read_status` WHERE hr_document_id=$hr_document_id)";
                            $i = 1;
                            $crDate = date('Y-m-d');
                            $curnTime  = date('H:i:s');
                            $q = $d->select("users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id", "users_master.society_id='$society_id' AND users_master.active_status=0 AND users_master.delete_status=0 $userQuery $deptFilterQuery $blockFilterQuery $userFilterQuery", "ORDER BY users_master.user_id DESC");
                            $counter = 1;
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?><?php if ($data['user_designation'] != "") { echo "(" . $data['user_designation'] . ")"; } ?></td>
                                        <td><?php echo $data['floor_name'] . "(" . $data['block_name'] . ")"; ?></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->



