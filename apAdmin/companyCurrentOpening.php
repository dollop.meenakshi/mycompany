<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Current Opening</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <a href="addCompanyCurrentOpening"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        
        <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyCurrentOpening');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>    
                        <th>Title</th>                      
                        <th>Position</th>                      
                        <th>Timing</th>                                               
                        <th>Address</th>                                                  
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      $q=$d->select("company_current_opening_master","society_id='$society_id' ","ORDER BY company_current_opening_id DESC");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                          <?php $totalApply = $d->count_data_direct("company_current_opening_apply_id","company_current_opening_apply_master","company_current_opening_id='$data[company_current_opening_id]'");
                          $totalRefer = $d->count_data_direct("company_opening_refer_id","company_opening_refer_master","company_current_opening_id='$data[company_current_opening_id]'");
                            if($totalApply==0 && $totalRefer==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['company_current_opening_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_current_opening_id']; ?>">                      
                          <?php } ?>
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_current_opening_title']; ?> </td>
                       <td><?php echo $data['company_current_opening_position']; ?></td>
                       <td><?php echo $data['company_current_opening_timing']; ?></td>
                       <td><?php echo $data['company_current_opening_address']; ?></td>
                       <td>
                         <div class="d-flex align-items-center">
                            <form action="addCompanyCurrentOpening" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="company_current_opening_id" value="<?php echo $data['company_current_opening_id']; ?>">
                              <input type="hidden" name="edit_company_current_opening" value="edit_company_current_opening">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                            </form>
                            <?php if($data['company_current_opening_status']=="0"){
                                  $status = "currentOpeningStatusDeactive";  
                                  $active="checked";                     
                              } else { 
                                  $status = "currentOpeningStatusActive";
                                  $active="";  
                              } ?>
                              <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['company_current_opening_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                              <button type="submit" class="btn btn-sm btn-primary ml-2" onclick="getCompanyCurrentOpening(<?php echo $data['company_current_opening_id']; ?>)"> <i class="fa fa-eye"></i></button>
                          </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
<div class="modal fade" id="companyCurrentOpeningModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Get In Touch Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="row col-md-12"  id="showCompanyCurrentOpening">
          </div>

        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
