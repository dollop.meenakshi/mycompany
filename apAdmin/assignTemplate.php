<?php 
extract($_POST);
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
if (isset($edit_assign)) {
    $q = $d->selectRow('template_assign_master.*',"template_assign_master","template_assign_id='$template_assign_id' AND society_id='$society_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
    if($data['template_assign_for']=="1"){
        $access_for_id = explode(',', $data['template_assign_ids']);
    }
    else if($data['template_assign_for']=="2"){
        $template_assign_ids = explode(',', $data['template_assign_ids']);
        $dept_ids = implode("','", $template_assign_ids);
        //$dept_ids = explode("','", $dept_ids);
        $q1 = $d->selectRow('*',"floors_master","floor_id IN ('$dept_ids') AND society_id='$society_id'");	
        $blockIds = array();
        while($data1 = mysqli_fetch_array($q1)){
            array_push($blockIds,$data1['block_id']);
        }
        $blockIds = array_unique($blockIds);
        $blockIds = array_values($blockIds);
        $block_ids = implode("','", $blockIds);
    }
    else if($data['template_assign_for']=="3"){
        $template_assign_ids = explode(',', $data['template_assign_ids']);
        $user_ids = implode("','", $template_assign_ids);
        //$dept_ids = explode("','", $dept_ids);
        $q2 = $d->selectRow('*',"users_master","user_id IN ('$user_ids') AND society_id='$society_id'");
        $blockIds = array();
        $floorIds = array();
        while($data2 = mysqli_fetch_array($q2)){
            array_push($blockIds,$data2['block_id']);
            array_push($floorIds,$data2['floor_id']);
        }
        $blockIds = array_unique($blockIds);
        $blockIds = array_values($blockIds);
        $block_ids = implode("','", $blockIds);
        $floorIds = array_unique($floorIds);
        $floorIds = array_values($floorIds);
        $floor_ids = implode("','", $floorIds);
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Assign Template</h4>
     </div>
     </div>
    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addEmployeeAccessFrom" action="controller/WorkReportTemplateController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Template <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select name="template_id" class="form-control single-select" required <?php if (isset($edit_assign)) { echo 'disabled'; }?>  >
                                        <option value="">-- Select Template --</option> 
                                        <?php 
                                        $tq=$d->select("template_master","society_id='$society_id' AND template_status=0");  
                                        while ($templateData=mysqli_fetch_array($tq)) {
                                        ?>
                                        <option  <?php if($template_id==$templateData['template_id']) { echo 'selected';} ?> value="<?php echo  $templateData['template_id'];?>" ><?php echo $templateData['template_name'];?></option>
                                        <?php } ?>

                                    </select>                
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Access For <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="template_assign_for" id="access_for" onchange="accessType(this.value);">
                                        <option value="">-- Select Access For --</option> 
                                        <?php if ($access_branchs=="") { ?>
                                        <option <?php if(isset($edit_assign) && $template_assign_for == 0){echo 'selected';} ?> value="0">All </option> 
                                        <?php } ?>
                                        <option <?php if(isset($edit_assign) && $template_assign_for == 1){echo 'selected';} ?> value="1">Branch Wise</option> 
                                        <option <?php if(isset($edit_assign) && $template_assign_for == 2){echo 'selected';} ?> value="2">Department Wise</option> 
                                        <option <?php if(isset($edit_assign) && $template_assign_for == 3){echo 'selected';} ?> value="3">Employee Wise</option> 
                                    </select>          
                                </div>
                            </div> 
                        </div>
                        
                    </div>    
                    <div class="row all-access branch-wise <?php if($data['template_assign_for']==1){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" >
                                    <select type="text" required="" id="template_assign_ids" class="form-control multiple-select taskAccessForBranchMultiSelectCls access_for_id" name="template_assign_ids[]" multiple>
                                        <?php if ($access_branchs=="") { ?>
                                        <option value="0">All Branch</option>
                                        <?php }
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($template_assign_ids) && in_array($blockData['block_id'],$template_assign_ids)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                    <div class="row all-access department-wise <?php if($data['template_assign_for']==2){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select dwblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" id="dwblockId" onchange="getTaskAccessForDepartment();">
                                        <!-- <option value="0">All Branch</option> -->
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockIds) && in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" id="dwfloorId" class="form-control multiple-select dwfloorId taskAccessForDepartmentMultiSelectCls template_assign_ids" multiple name="template_assign_ids[]">
                                        <option value="0">All Department</option>
                                        <?php 
                                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                            while ($depaData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(in_array($depaData['floor_id'],$template_assign_ids)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                    <div class="row all-access employee-wise <?php if($data['template_assign_for']==3){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" id="ewblockId" onchange="getTaskAccessForDepartment();">
                                        <!-- <option value="0">All Branch</option> -->
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockIds) && in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewfloorId taskAccessForDepartmentMultiSelectCls" multiple onchange="getTaskAccessForEmployee();" name="floorId[]">
                                        <!-- <option value="0">All Department</option> -->
                                        <?php 
                                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                            while ($depaData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(in_array($depaData['floor_id'],$floorIds)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewempId taskAccessForEmployeeMultiSelectCls access_for_id" multiple name="template_assign_ids[]" id="ewempId"> 
                                        <option value="0">All Employee</option>
                                        <?php 
                                            $user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id IN ('$floor_ids')$blockAppendQueryUser");  
                                            while ($userdata=mysqli_fetch_array($user)) {
                                        ?>
                                        <option <?php if(in_array($userdata['user_id'],$template_assign_ids)){ echo "selected"; } ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                
                    <div class="form-footer text-center">              
                        <?php if (isset($edit_assign)) { ?>      
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                        <input type="hidden" name="assignWorkReportTemplate"  value="assignWorkReportTemplate">
                        <input type="hidden" name="template_id"  value=" <?php echo $template_id; ?>">
                        <input type="hidden" name="template_assign_id" id="template_assign_id"  value="<?php echo $template_assign_id;?>">
                        <?php }else{ ?>
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                        <input type="hidden" name="assignWorkReportTemplate"  value="assignWorkReportTemplate">
                        <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function accessType(value) {
		$('.multiple-select').val('').trigger('change');
		if(value == "1"){
			$('.blockId').val('').trigger('change');
			$('.all-access').addClass('d-none');
			$('.branch-wise').removeClass('d-none');
		}
		else if(value == "2"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.department-wise').removeClass('d-none');
		}
		else if(value == "3"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.employee-wise').removeClass('d-none');
		}
        else{
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
		}

        $('#dwblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#ewblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#template_assign_ids').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#ewempId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Employee",
                }
            });
        $('#dwfloorId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Department",
                }
            });
    }
</script>  
  