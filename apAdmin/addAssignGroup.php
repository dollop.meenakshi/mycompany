<?php
extract(array_map("test_input", $_POST));
if (isset($editGroupAssign))
{
    $q = $d->select("holiday_group_assign_master", "holiday_group_assign_id = '$holiday_group_assign_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $common_assign_id_arr = explode(",",$common_assign_id);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title"> Add Assign Group</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="assignGroupAddForm" action="controller/HolidayController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Holiday Group <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select class="form-control single-select" name="holiday_group_id" id="holiday_group_id" required onchange="getBranches();">
                                        <option value="">--SELECT--</option>
                                        <?php
                                        $q = $d->selectRow("holiday_group_id,holiday_group_name","holiday_group_master","holiday_group_active_status = 0");
                                        while($hg_data = $q->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if(isset($editGroupAssign) && $holiday_group_id == $hg_data['holiday_group_id']){ echo "selected"; } ?> value="<?php echo $hg_data['holiday_group_id']; ?>"><?php echo $hg_data['holiday_group_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select id="block_id_assign" name="block_id_assign[]" multiple class="form-control multiple-select" onchange="getFloorByBlockIdAddGroup();" required>
                                        <option value="0">All Branch</option>
                                        <?php
                                        if(isset($editGroupAssign))
                                        {
                                            if($common_assign_type == 0)
                                            {
                                                $q = $d->selectRow("block_id,block_name","block_master AS bm","");
                                            }
                                            elseif($common_assign_type == 1)
                                            {
                                                $q = $d->selectRow("bm.block_id,bm.block_name","block_master AS bm JOIN floors_master AS fm ON fm.block_id = bm.block_id","fm.floor_id IN ($common_assign_id)","GROUP BY fm.block_id");
                                            }
                                            elseif($common_assign_type == 2)
                                            {
                                                $q = $d->selectRow("bm.block_id,bm.block_name","block_master AS bm LEFT JOIN users_master AS um ON um.block_id = bm.block_id","um.level_id IN ($common_assign_id)","GROUP BY um.block_id");
                                            }
                                            else
                                            {

                                            }
                                            while($data = $q->fetch_assoc())
                                            {
                                            ?>
                                                <option <?php if (in_array($data['block_id'], $common_assign_id_arr) || $common_assign_type == 1 || $common_assign_type == 2) { echo 'selected';} ?> value="<?php echo  $data['block_id']; ?>"><?php echo $data['block_name']; ?> </option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label departmentDiv <?php if(isset($editGroupAssign) && $common_assign_type == 0){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?>">Department</label>
                                <div class="col-lg-4 col-md-4 departmentDiv <?php if(isset($editGroupAssign) && $common_assign_type == 0){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?> ">
                                    <select id="floor_id" multiple onchange="getAllLevel();" class="form-control multiple-select" name="floor_id[]">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if (isset($editGroupAssign))
                                        {
                                            if($common_assign_type == 0)
                                            {
                                                $q = $d->selectRow("floor_id,floor_name,block_name","floors_master,block_master", "block_master.block_id = floors_master.block_id AND floors_master.floor_id IN ($common_assign_id)");
                                            }
                                            elseif($common_assign_type == 1)
                                            {
                                                $q = $d->selectRow("floor_id,floor_name,block_name","floors_master,block_master", "block_master.block_id = floors_master.block_id AND floors_master.floor_id IN ($common_assign_id)");
                                            }
                                            elseif($common_assign_type == 2)
                                            {
                                                $q = $d->selectRow("fm.floor_id,fm.floor_name,bm.block_name","floors_master AS fm JOIN block_master AS bm ON bm.block_id = fm.block_id LEFT JOIN users_master AS um ON um.floor_id = fm.floor_id LEFT JOIN employee_level_master AS elm ON elm.level_id = um.level_id","um.level_id IN ($common_assign_id)","GROUP BY fm.floor_id");
                                            }
                                            else
                                            {

                                            }
                                            while ($data = $q->fetch_assoc())
                                            {
                                            ?>
                                                <option <?php if (in_array($data['floor_id'],$common_assign_id_arr) || $common_assign_type == 2) { echo "selected";} ?> value="<?php echo $data['floor_id']; ?>"> <?php if (isset($data['floor_name']) && $data['floor_name'] != "") { echo $data['floor_name'] . "(" . $data['block_name'] . ")";} ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label levelDiv <?php if(isset($editGroupAssign) && ($common_assign_type == 0 || $common_assign_type == 1)){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?>">Level</label>
                                <div class="col-lg-4 col-md-4 levelDiv <?php if(isset($editGroupAssign) && ($common_assign_type == 0 || $common_assign_type == 1)){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?>">
                                    <select name="level_id[]" multiple id="level_id" class="form-control multiple-select" onchange="getEmp();">
                                        <option value="">-- Select Level --</option>
                                        <?php
                                        if (isset($editGroupAssign))
                                        {
                                            $q = $d->selectRow("elm.level_id,elm.level_name","employee_level_master AS elm","elm.level_id IN ($common_assign_id)","GROUP BY elm.level_id");
                                            while ($data = $q->fetch_assoc())
                                            {
                                            ?>
                                            <option <?php if (in_array($data['level_id'],$common_assign_id_arr)) {
                                            echo 'selected'; } ?> value="<?php echo $data['level_id']; ?>"><?php echo $data['level_name']; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label empDiv <?php if(isset($editGroupAssign) && ($common_assign_type == 0 || $common_assign_type == 1 || $common_assign_type == 2)){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?>">Employees</label>
                                <div class="col-lg-4 col-md-4 empDiv <?php if(isset($editGroupAssign) && ($common_assign_type == 0 || $common_assign_type == 1 || $common_assign_type == 2)){ echo "d-none"; }elseif(!isset($editGroupAssign)){ echo "d-none"; } ?>">
                                    <select id="user_id" multiple onchange="validateEmpDrop();" class="form-control multiple-select" name="user_id[]">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if (isset($editGroupAssign))
                                        {
                                            $all = [];
                                            $floor_id_str = "0";
                                            $q1 = $d->selectRow("common_assign_id","holiday_group_assign_master","common_assign_type = 3 AND holiday_group_id = '$holiday_group_id'");
                                            if(mysqli_num_rows($q1) > 0)
                                            {
                                                $da = $q1->fetch_assoc();
                                                $all_ids = $da['common_assign_id'];
                                                $q = $d->selectRow("user_id,user_full_name","users_master","level_id IN ($common_assign_id) AND floor_id IN ($floor_id_str) AND user_id NOT IN ($all_ids)");
                                            }
                                            else
                                            {
                                                $q = $d->selectRow("user_id,user_full_name","users_master","level_id IN ($common_assign_id) AND floor_id IN ($floor_id_str)");
                                            }
                                            while($data = $q->fetch_assoc())
                                            {
                                            ?>
                                                <option <?php if (in_array($data['user_id'],$common_assign_id)) { echo 'selected'; } ?> value="<?php echo $data['user_id']; ?>"><?php echo $data['user_full_name']; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <?php
                                if (isset($editGroupAssign))
                                {
                                ?>
                                    <input type="hidden" name="holiday_group_assign_id" value="<?php echo $holiday_group_assign_id; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                                    <input type="hidden" name="editAssignGroup" value="editAssignGroup">
                                    <input type="hidden" name="addAssignGroup" value="addAssignGroup">
                                <?php
                                }
                                else
                                {
                                ?>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                    <input type="hidden" name="addAssignGroup" value="addAssignGroup">
                                    <button type="reset" class="btn btn-danger cancel" onclick="resetFrm('assignGroupAddForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper--------->