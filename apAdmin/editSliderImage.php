<?php
  if(!($_GET['id'])){
    $_SESSION['msg1']="Invalid Request.";
  }
    $slider_id = $_GET['id'];
  $q = $d->select("app_slider_master","app_slider_id='$slider_id'");
  if(mysqli_num_rows($q)==0){
    $_SESSION['msg1']="Invalid Slider Details.";
  }
  $sliderdetails = mysqli_fetch_array($q);

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">App Banner</h4>
       
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if(mysqli_num_rows($q)==0) { ?>
              <h4 class="form-header text-uppercase">
              Invalid Banner Details.
              </h4>
            <?php }else{
             ?>
            <form id="sliderImageAddValidation" method="post" action="controller/sliderController.php" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-address-book-o"></i>
              Edit Banner Details 
              </h4>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Banner Image</label>
                <div class="col-sm-6">
                  <input type="hidden" name="slider_id" value="<?php echo $slider_id; ?>">
                  <input type="hidden" id="old_slider_image" value="<?php echo $sliderdetails['slider_image_name']; ?>">
                  <input class="form-control-file border" type="file" accept="image/*" name="slider_image">
                </div>
                <div class="col-sm-4">
                  <a href="../img/sliders/<?php echo $sliderdetails['slider_image_name']; ?>" target="_blank"><img src="../img/sliders/<?php echo $sliderdetails['slider_image_name']; ?>" width="250"></a>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Youtube Video Id </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"  name="youtube_url" minlength="5" maxlength="40" value="<?=$sliderdetails['youtube_url']?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">URL </label>
                <div class="col-sm-10">
                  <input class="form-control" maxlength="100" type="url"  name="page_url" value="<?=$sliderdetails['page_url']?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Phone Number </label>
                <div class="col-sm-10">
                  <input class="form-control" id="trlDays" type="text" maxlength="12"  name="page_mobile" value="<?php if($sliderdetails['page_mobile']!=0){ echo $sliderdetails['page_mobile'];}?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">About Offer </label>
                <div class="col-sm-10">
                  <textarea class="form-control"  type="text" maxlength="250"  name="about_offer"> <?=$sliderdetails['about_offer']?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Order By</label>
                <div class="col-sm-10">
                  <label><input type="radio" class="order_by" <?php if($sliderdetails['order_by']==0){echo 'checked';} ?> name="order_by" value="0" > Random</label>
                  <label><input type="radio" class="order_by" <?php if($sliderdetails['order_by']==1){echo 'checked';} ?> name="order_by" value="1"> Ordered</label>
                </div>
              </div>
              <div class="form-group row <?php if($sliderdetails['order_by']==0){echo 'd-none';} ?>" id="banner_order_div">
                <label for="input-12" class="col-sm-2 col-form-label">Order Value <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control onlyNumber" required name="order_value" value="<?=$sliderdetails['order_value']?>">
                </div>
              </div>
              <div class="form-footer text-center">
                <button type="submit" class="btn btn-success" name = "editSliderImage"><i class="fa fa-check-square-o"></i> UPDATE</button>
               
              </div>
            </form>
          <?php } ?>
          </div>
        </div>
      </div>
      </div><!--End Row-->
      </div><!--End Row-->
    </div>
  </div>
</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<script src="assets/js/jquery.min.js"></script>

<script>
  $(document).on('change', '.order_by', function(){
    value = $(this).val();
    if(value == 1){
      $('#banner_order_div').removeClass('d-none');
    }else{
      $('#banner_order_div').addClass('d-none');
    }
  });
</script>