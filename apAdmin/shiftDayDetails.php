<style>
    .separator {
      display: flex;
      align-items: center;
      text-align: center;
    }

    .separator::before,
    .separator::after {
      content: '';
      flex: 1;
      border-bottom: 1px solid #000;
    }

    .separator:not(:empty)::before {
      margin-right: .25em;
    }

    .separator:not(:empty)::after {
      margin-left: .25em;
    }
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
<?php
if(isset($_GET) && !empty($_GET))
{
    extract($_GET);
    if($_GET['edit_shift_timing'] != "edit_shift_timing")
    {
        $_SESSION['msg1'] = "Invalid Request";
    ?>
        <script>
            window.location = "manageShift";
        </script>
    <?php
        exit;
    }
    if(!ctype_digit($_GET['sti']))
    {
        $_SESSION['msg1'] = "Invalid Request";
    ?>
        <script>
            window.location = "manageShift";
        </script>
    <?php
        exit;
    }
    $shift_time_id = $_GET['sti'];
    if (isset($edit_shift_timing))
    {
        $q = $d->selectRow("stm.shift_name,stm.week_off_days,stm.maximum_in_out,stm.late_in_reason,stm.early_out_reason,stm.has_altenate_week_off,stm.alternate_week_off,stm.alternate_weekoff_days,stm.is_multiple_punch_in,stm.take_out_of_range_reason,sdm.*","shift_timing_master AS stm JOIN shift_day_master AS sdm ON sdm.shift_time_id = stm.shift_time_id", "stm.shift_time_id = '$shift_time_id'","GROUP BY sdm.shift_group_name ORDER BY sdm.shift_day ASC");
        while($row = $q->fetch_assoc())
        {
            $data_s[] = $row;
        }
        $week_off_days_arr = [];
        if($data_s[0]['week_off_days'] != "")
        {
            $week_off_days_arr = explode(",",$data_s[0]['week_off_days']);
        }

        $alt_week_off_days_arr = [];
        if($data_s[0]['alternate_weekoff_days'] != "")
        {
            $alt_week_off_days_arr = explode(",",$data_s[0]['alternate_weekoff_days']);
        }
    }
    function getFormatedTime($time)
    {
        $timeData = explode(':', $time);
        $hours = $timeData[0];
        $minutes = $timeData[1];
        if ($hours > 0 && $minutes > 0) {
            return sprintf('%02d hr %02d min', $hours, $minutes);
        }else if ($hours > 0 && $minutes <= 0) {
            if ($hours>1) {
                return sprintf('%02d Hours', $hours);
            }else{
                return sprintf('%02d Hour', $hours);
            }
        }else if ($hours <= 0 && $minutes > 0) {
            if ($minutes>1) {
                return sprintf('%02d Minutes', $minutes);
            }else{
                return sprintf('%02d Minute', $minutes);
            }
        }else{
            return "00:00";
        }
    }
}
else
{
    $_SESSION['msg1'] = "Invalid Request";
    ?>
        <script>
            window.location = "manageShift";
        </script>
    <?php
    exit;
}
$week_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$weeks = array('Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5');
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title"> Shift Timing</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label class="col-lg-12 col-md-12 col-form-label col-12">Shift Name : <?php if (isset($_GET['edit_shift_timing']) && $data_s[0]['shift_name'] != "") { echo $data_s[0]['shift_name'];} ?></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Week off Days :
                                    <?php
                                        $wod = "";
                                        for ($i = 0; $i < count($week_days); $i++)
                                        {
                                            if (isset($_GET['edit_shift_timing']) && $data_s[0]['week_off_days'] != "" && isset($data_s[0]['week_off_days']) && in_array($i, $week_off_days_arr))
                                            {
                                                $wod .= $week_days[$i] . ", ";
                                            }
                                        }
                                        echo rtrim($wod," ,");
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Has Alternate Week off :
                                    <?php
                                        if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 0)
                                        {
                                            echo "No";
                                        }
                                        else
                                        {
                                            echo "Yes";
                                        }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 has_altenate_week_off" <?php if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 1) { echo "style='display: block;'"; }else{ echo "style='display: none;'"; } ?>>
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off :
                                    <?php
                                        if ($data_s[0]['alternate_week_off'] != "")
                                        {
                                            $alternate_week_off = explode(',', $data_s[0]['alternate_week_off']);
                                        }
                                        $awo = "";
                                        for ($j = 0; $j < count($weeks); $j++)
                                        {
                                            if (isset($_GET['edit_shift_timing']) && $data_s[0]['alternate_week_off'] != "" && isset($data_s[0]['alternate_week_off']) && in_array($j + 1, $alternate_week_off))
                                            {
                                                $awo .= $weeks[$j] . ", ";
                                            }
                                        }
                                        echo rtrim($awo," ,");
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 has_altenate_week_off" <?php if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['has_altenate_week_off']) && $data_s[0]['has_altenate_week_off'] == 1) { echo "style='display: block;'"; }else{ echo "style='display: none;'"; } ?>>
                                <input type="hidden" name="hasAlrtWeekDays" id="hasAlrtWeekDays" value="<?php if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['alternate_weekoff_days']) && $data_s[0]['alternate_weekoff_days'] != "") {echo $data_s[0]['alternate_weekoff_days'];} ?>">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off Days :
                                    <?php
                                        $awod = "";
                                        for ($i = 0; $i < count($week_days); $i++)
                                        {
                                            if (isset($_GET['edit_shift_timing']) && $data_s[0]['alternate_weekoff_days'] != "" && isset($data_s[0]['alternate_weekoff_days']) && in_array($i, $alt_week_off_days_arr))
                                            {
                                                $awod .= $week_days[$i] . ", ";
                                            }
                                        }
                                        echo rtrim($awod," ,");
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Maximum In/Out : <?php if (isset($_GET['edit_shift_timing']) && $data_s[0]['maximum_in_out'] != "") {echo $data_s[0]['maximum_in_out'];} ?></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Late in :
                                    <?php
                                        if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['late_in_reason']) && $data_s[0]['late_in_reason'] == 0)
                                        {
                                            echo "No";
                                        }
                                        else
                                        {
                                            echo "Yes";
                                        }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Early Out :
                                    <?php
                                        if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['early_out_reason']) && $data_s[0]['early_out_reason'] == 0)
                                        {
                                            echo "No";
                                        }
                                        else
                                        {
                                            echo "Yes";
                                        }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Multiple Punch In/Out Allow :
                                    <?php
                                        if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['is_multiple_punch_in']) && $data_s[0]['is_multiple_punch_in'] == 0)
                                        {
                                            echo "No";
                                        }
                                        else
                                        {
                                            echo "Yes";
                                        }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Out Of Range Reason :
                                    <?php
                                        if (isset($_GET['edit_shift_timing']) && isset($data_s[0]['take_out_of_range_reason']) && $data_s[0]['take_out_of_range_reason'] == 1)
                                        {
                                            echo "Yes";
                                        }
                                        else
                                        {
                                            echo "No";
                                        }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" name="checkChnageAltFunction" id="checkChnageAltFunction" value=" <?php if ($data_s[0]['week_off_days'] != "") {echo 1;} else {echo 0;} ?>">
                        </div>
                    </div>
                </div>
                <?php
                    foreach($data_s AS $key => $value)
                    {
                        extract($value);
                        if($lunch_break_start_time != "" && $lunch_break_start_time != "00:00:00" && $lunch_break_end_time != "" && $lunch_break_end_time != "00:00:00")
                        {
                            $dateTimeObject1 = date_create('2019-05-18 '.$lunch_break_start_time.":00");
                            $dateTimeObject2 = date_create('2019-05-18 '.$lunch_break_end_time.":00");
                            $interval = date_diff($dateTimeObject1, $dateTimeObject2);
                            $lunch_min = $interval->days * 24 * 60;
                            $lunch_min += $interval->h * 60;
                            $lunch_min += $interval->i;
                            $lunch_hours = floor($lunch_min / 60);
                            $lunch_min = $lunch_min - ($lunch_hours * 60);
                            if($lunch_hours == 0)
                            {
                                $total_lunch_time = $lunch_min." Minutes";
                            }
                            else
                            {
                                if($lunch_hours > 1)
                                {
                                    $total_lunch_time = $lunch_hours ." Hours " .$lunch_min." Minutes";
                                }
                                else
                                {
                                    $total_lunch_time = $lunch_hours ." Hour " .$lunch_min." Minutes";
                                }
                            }
                        }

                        if($tea_break_start_time != "" && $tea_break_start_time != "00:00:00" && $tea_break_end_time != "" && $tea_break_end_time != "00:00:00")
                        {
                            $dateTimeObject1 = date_create('2019-05-18 '.$tea_break_start_time.":00");
                            $dateTimeObject2 = date_create('2019-05-18 '.$tea_break_end_time.":00");
                            $interval = date_diff($dateTimeObject1, $dateTimeObject2);
                            $tea_min = $interval->days * 24 * 60;
                            $tea_min += $interval->h * 60;
                            $tea_min += $interval->i;
                            $tea_hours = floor($tea_min / 60);
                            $tea_min = $tea_min - ($tea_hours * 60);
                            if($tea_hours == 0)
                            {
                                $total_tea_time = $tea_min." Minutes";
                            }
                            else
                            {
                                if($tea_hours > 1)
                                {
                                    $total_tea_time = $tea_hours ." Hours " .$tea_min." Minutes";
                                }
                                else
                                {
                                    $total_tea_time = $tea_hours ." Hour " .$tea_min." Minutes";
                                }
                            }
                        }
                ?>
                <div class="card">
                    <div class="card-header">
                    <?php
                        echo preg_replace("/,([^,]+)$/", " & $1", $shift_group_name);
                    ?>
                    </div>
                    <div class="card-body">
                        <div class="row container">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-lg-1 col-md-1 col-sm-12 text-lg-right text-md-right">
                                        <?php echo date("g:i A", strtotime($shift_start_time)); ?>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-12">
                                        <div class="separator">
                                            <h5><span class="badge badge-info"><?php echo $per_day_hour; ?> Hours</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-12">
                                        <?php echo date("g:i A", strtotime($shift_end_time)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                                <div class="row">
                                <?php
                                    if($lunch_break_start_time != "" && $lunch_break_start_time != "00:00:00" && $lunch_break_end_time != "" && $lunch_break_end_time != "00:00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Lunch Break(<?php echo date("g:i A", strtotime($lunch_break_start_time)) . " - " . date("g:i A", strtotime($lunch_break_end_time)) ; ?>)
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $total_lunch_time; ?>
                                    </div>
                                <?php
                                    }
                                    if($tea_break_start_time != "" && $tea_break_start_time != "00:00:00" && $tea_break_end_time != "" && $tea_break_end_time != "00:00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Tea Break(<?php echo date("g:i A", strtotime($tea_break_start_time)) . " - " . date("g:i A", strtotime($tea_break_end_time)) ; ?>)
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $total_tea_time; ?>
                                    </div>
                                <?php
                                    }
                                    if($late_time_start != "" && $late_time_start != "00:00:00" && $late_time_start != "00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Late In Relaxation
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $late_time_start . " Minutes"; ?>
                                    </div>
                                <?php
                                    }
                                    if($early_out_time != "" && $early_out_time != "00:00:00" && $early_out_time != "00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Early Out Relaxation
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $early_out_time . " Minutes"; ?>
                                    </div>
                                <?php
                                    }
                                    if($maximum_halfday_hours != "" && $maximum_halfday_hours != "00:00:00" && $maximum_halfday_hours != "00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Mininum working hours for half day
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo getFormatedTime($maximum_halfday_hours); ?>
                                    </div>
                                <?php
                                    }
                                    if($minimum_hours_for_full_day != "" && $minimum_hours_for_full_day != "00:00:00" && $minimum_hours_for_full_day != "00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Mininum working hours for full day
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo getFormatedTime($minimum_hours_for_full_day); ?>
                                    </div>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                                <div class="row">
                                <?php
                                    if($shift_type != "")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Shift Type
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $shift_type; ?>
                                    </div>
                                <?php
                                    }
                                    if($max_punch_out_time != "" && $max_punch_out_time != "00:00:00" && $max_punch_out_time != "00:00")
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Max Punch Out Time
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo date("g:i A", strtotime($max_punch_out_time)); ?>
                                    </div>
                                <?php
                                    }
                                    if($max_tea_break != "" && $max_tea_break > 0)
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Max Tea Break
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $max_tea_break; ?>
                                    </div>
                                <?php
                                    }
                                    if($max_lunch_break != "" && $max_lunch_break > 0 )
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Max Lunch Break
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $max_lunch_break; ?>
                                    </div>
                                <?php
                                    }
                                    if($max_personal_break != "" && $max_personal_break > 0)
                                    {
                                ?>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                        Max Personal Break
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 text-right">
                                    <?php echo $max_personal_break; ?>
                                    </div>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper-->