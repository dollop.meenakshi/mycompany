<?php error_reporting(0);
$create_group = $sData['create_group'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Auto Created Chat Group</h4>
          <i class="text-warning">Create Branch, Department, Zone & Level wise Group</i>
        </div>
        <div class="col-sm-3 col-md-3 col-3">
          <li class="list-group-item d-flex justify-content-between align-items-center">
           Create New Group from app  
          <span>
            <?php
              if($create_group=="1"){
              ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $create_group; ?>','CreateGroupActive');" data-size="small"/>
                <?php } else { ?>
               <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $create_group; ?>','CreateGroupDeactive');" data-size="small"/>
              <?php } ?>
          </span>
          </li>
        </div>
        <div class="col-sm-3 col-md-3 col-3">
          <div class="btn-group float-sm-right">
             <a data-toggle="modal" data-target="#addAutoGroup" href="javascript:void(0)"  class="btn  btn-sm btn-primary pull-right"><i class="fa fa-plus fa-lg"></i> Create Auto Group </a>
          </div>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <ul class="nav nav-tabs nav-tabs-info nav-justified">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabe-Branch"> Branch</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link  show" data-toggle="tab" href="#tabe-Department"> Department</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link  show" data-toggle="tab" href="#tabe-Zone"> Zone</span></a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link  show" data-toggle="tab" href="#tabe-Level"> Level</span></a>
                </li>
                
              </ul>

              <div class="tab-content">
                <div id="tabe-Branch" class="container-fluid tab-pane active show">
                  <div class="">
                    <div class="">
                      <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                      <div class="">
                        <div class="table-responsive">
                          <table id="default-datatable1" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>                        
                                    <th>Branch</th>
                                    <th>Group Icon</th>                    
                                    <th>Created Date</th>                      
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                                $i=1;
                                 $q2=$d->selectRow("block_master.*,chat_group_master.*","block_master,chat_group_master","block_master.block_id = chat_group_master.group_common_id AND  chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 1","");
                                  $counter = 1;
                                while ($data=mysqli_fetch_array($q2)) {
                                 ?>
                                <tr>
                                  
                                   <td><?php echo $counter++; ?></td>
                                   <td><?php echo $data['block_name'].' Branch'; ?></td>
                                   <td>
                                       <a href="../img/users/<?php echo $data['group_icon']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['group_icon']; ?>"><img width="50" height="50" onerror="this.src='../img/group_palceholder.png'" src="../img/group_palceholder.png" data-src="../img/users/<?php echo $data['group_icon']; ?>"  href="#divForm<?php echo $data['group_id']; ?>" class="btnForm lazyload" ></a>
                                    </td>
                                  <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                                   <td>
                                   <div class="d-flex align-items-center">
                                      <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="chatGroupDataSet(<?php echo $data['group_id']; ?>,<?php echo $data['group_type']; ?>)"> <i class="fa fa-pencil"></i></button>

                                      <?php if($data['active_status']==0){
                                          $status = "chatGroupStatusDeactive";  
                                          $active="checked";                     
                                      } else { 
                                          $status = "chatGroupStatusActive";
                                          $active="";  
                                      } ?>
                                      <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>

                                      <form  class="ml-2" action="controller/ChatGroupController.php" method="post">
                                        <input type="hidden" name="group_id" value="<?php echo $data['group_id']; ?>">
                                        <input type="hidden" name="deleteAutoGroup" value="deleteAutoGroup">
                                        <button type="submit" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> </button>
                                      </form>
                                   </div>
                                  </td>
                                    
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="tabe-Department" class="container-fluid tab-pane">
                  <div class="">
                    <div class="">
                      <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                      <div class="">
                        <div class="table-responsive">
                          <table id="default-datatable2" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>                        
                                    <th>Branch </th>
                                    <th>Deapartment </th>
                                    <th>Group Icon</th>                    
                                    <th>Created Date</th>                      
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                                $i=1;
                                $q3=$d->selectRow("block_master.block_name,floors_master.*,chat_group_master.*","block_master,floors_master,chat_group_master","block_master.block_id=floors_master.block_id AND chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 2 AND floors_master.floor_id = chat_group_master.group_common_id ","");
                                  $counter = 1;
                                while ($data=mysqli_fetch_array($q3)) {
                                 ?>
                                <tr>
                                   
                                    <td><?php echo $counter++; ?></td>
                                   <td><?php echo $data['block_name']; ?></td>
                                   <td><?php echo $data['floor_name'].' Department'; ?></td>
                                   <td>
                                       <a href="../img/users/<?php echo $data['group_icon']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['group_icon']; ?>"><img width="50" height="50" onerror="this.src='../img/group_palceholder.png'" src="../img/group_palceholder.png" data-src="../img/users/<?php echo $data['group_icon']; ?>"  href="#divForm<?php echo $data['group_id']; ?>" class="btnForm lazyload" ></a>
                                    </td>
                                  <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                                   <td>
                                   <div class="d-flex align-items-center">
                                    <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="chatGroupDataSet(<?php echo $data['group_id']; ?>,<?php echo $data['group_type']; ?>)"> <i class="fa fa-pencil"></i></button>

                                      <?php if($data['active_status']==0){
                                          $status = "chatGroupStatusDeactive";  
                                          $active="checked";                     
                                      } else { 
                                          $status = "chatGroupStatusActive";
                                          $active="";  
                                      } ?>
                                      <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>

                                      <form  class="ml-2" action="controller/ChatGroupController.php" method="post">
                                        <input type="hidden" name="group_id" value="<?php echo $data['group_id']; ?>">
                                        <input type="hidden" name="deleteAutoGroup" value="deleteAutoGroup">
                                        <button type="submit" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> </button>
                                      </form>
                                   </div>
                                  </td>
                                    
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="tabe-Zone" class="container-fluid tab-pane">
                  <div class="">
                    <div class="">
                      <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                      <div class="">
                        <div class="table-responsive">
                          <table id="default-datatable3" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>                        
                                    <th>Zone</th>
                                    <th>Group Icon</th>                    
                                    <th>Created Date</th>                      
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                                $i=1;
                                 $q2=$d->selectRow("zone_master.*,chat_group_master.*","zone_master,chat_group_master","chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 3 AND zone_master.zone_id = chat_group_master.group_common_id","");
                                  $counter = 1;
                                while ($data=mysqli_fetch_array($q2)) {
                                 ?>
                                <tr>
                                  
                                    <td><?php echo $counter++; ?></td>
                                   <td><?php echo $data['zone_name'].' Zone'; ?></td>
                                   <td>
                                       <a href="../img/users/<?php echo $data['group_icon']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['group_icon']; ?>"><img width="50" height="50" onerror="this.src='../img/group_palceholder.png'" src="../img/group_palceholder.png" data-src="../img/users/<?php echo $data['group_icon']; ?>"  href="#divForm<?php echo $data['group_id']; ?>" class="btnForm lazyload" ></a>
                                    </td>
                                  <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                                   <td>
                                   <div class="d-flex align-items-center">
                                    <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="chatGroupDataSet(<?php echo $data['group_id']; ?>,<?php echo $data['group_type']; ?>)"> <i class="fa fa-pencil"></i></button>

                                      <?php if($data['active_status']==0){
                                          $status = "chatGroupStatusDeactive";  
                                          $active="checked";                     
                                      } else { 
                                          $status = "chatGroupStatusActive";
                                          $active="";  
                                      } ?>
                                      <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                      <form  class="ml-2" action="controller/ChatGroupController.php" method="post">
                                        <input type="hidden" name="group_id" value="<?php echo $data['group_id']; ?>">
                                        <input type="hidden" name="deleteAutoGroup" value="deleteAutoGroup">
                                        <button type="submit" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> </button>
                                      </form>
                                   </div>
                                  </td>
                                    
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="tabe-Level" class="container-fluid tab-pane">
                  <div class="">
                    <div class="">
                      <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                      <div class="">
                        <div class="table-responsive">
                          <table id="default-datatable4" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>                        
                                    <th>Level</th>
                                    <th>Group Icon</th>                    
                                    <th>Created Date</th>                      
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                                $i=1;
                                $q2=$d->selectRow("employee_level_master.*,chat_group_master.*","employee_level_master,chat_group_master","chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 4 AND employee_level_master.level_id = chat_group_master.group_common_id ","");
                                  $counter = 1;
                                while ($data=mysqli_fetch_array($q2)) {
                                 ?>
                                <tr>
                                   
                                    <td><?php echo $counter++; ?></td>
                                   <td><?php echo $data['level_name'].' Level'; ?></td>
                                   <td>
                                       <a href="../img/users/<?php echo $data['group_icon']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['group_icon']; ?>"><img width="50" height="50" onerror="this.src='../img/group_palceholder.png'" src="../img/group_palceholder.png" data-src="../img/users/<?php echo $data['group_icon']; ?>"  href="#divForm<?php echo $data['group_id']; ?>" class="btnForm lazyload" ></a>
                                    </td>
                                  <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                                   <td>
                                   <div class="d-flex align-items-center">
                                    <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="chatGroupDataSet(<?php echo $data['group_id']; ?>,<?php echo $data['group_type']; ?>)"> <i class="fa fa-pencil"></i></button>

                                      <?php if($data['active_status']==0){
                                          $status = "chatGroupStatusDeactive";  
                                          $active="checked";                     
                                      } else { 
                                          $status = "chatGroupStatusActive";
                                          $active="";  
                                      } ?>
                                      <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                      <form  class="ml-2" action="controller/ChatGroupController.php" method="post">
                                        <input type="hidden" name="group_id" value="<?php echo $data['group_id']; ?>">
                                        <input type="hidden" name="deleteAutoGroup" value="deleteAutoGroup">
                                        <button type="submit" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> </button>
                                      </form>
                                   </div>
                                  </td>
                                    
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div><!-- End Row-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Employee Created Chat Group</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroup');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Group Name</th>
                        <th>Group Icon</th>
                        <th>Admin</th>                      
                        <th>Created Date</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->selectRow("chat_group_master.*, users_master.user_full_name, users_master.user_id,(SELECT COUNT(*) FROM users_master,chat_group_member_master WHERE chat_group_master.group_id=chat_group_member_master.group_id AND chat_group_member_master.user_id=users_master.user_id AND chat_group_member_master.user_id = users_master.user_id AND user_status!=0 AND delete_status=0 ) AS total_group_members","chat_group_master,users_master","chat_group_master.society_id='$society_id' AND chat_group_master.created_by=users_master.user_id");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                        <?php  
                             
                              if( $data['total_group_members']==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['group_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['group_id']; ?>">                      
                          <?php } ?>    
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['group_name']; ?><a href="chatGroupMember?gId=<?php echo $data['group_id']; ?>"><span class="badge badge-pill badge-info m-1">Members (<?php echo $data['total_group_members']; ?>)</span></a></td>
                       <td>
                           <a href="../img/users/<?php echo $data['group_icon']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['group_icon']; ?>"><img width="50" height="50" onerror="this.src='../img/group_palceholder.png'" src="../img/group_palceholder.png" data-src="../img/users/<?php echo $data['group_icon']; ?>"  href="#divForm<?php echo $data['group_id']; ?>" class="btnForm lazyload" ></a>
                        </td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="chatGroupDataSet(<?php echo $data['group_id']; ?>,<?php echo $data['group_type']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['active_status']==0){
                              $status = "chatGroupStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "chatGroupStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="chatGroupModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Chat Group</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body" id="chatGroupData">  
          <form id="chatGroupForm" action="controller/ChatGroupController.php" enctype="multipart/form-data" method="post" class="chatGroupData"> 
          </form>
        </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="addAutoGroup">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Create Auto Group</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="expense" action="controller/ChatGroupController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="createAutoGroup" value="createAutoGroup">
          <div class="form-group row">
            <label for="expenses_title" class="col-sm-5 col-form-label">Group Type <span class="required">*</span></label>
            <div class="col-sm-7">
              <select required=""  class="form-control group_type_change" name="group_type" id="">
                <option value=""> -- Select--</option>
                <option value="1"> Branch Wise</option>
                <option value="2"> Department Wise</option>
                <option value="3"> Zone Wise</option>
                <option value="4"> Level Wise</option>
              </select>
            </div>
            
          </div>
          <div id="getAutoGroupDiv">
            
          </div>
         
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addExpenses"></i> Create </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
