  <?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Leave Types</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteLeaveType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Date</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                        <?php  $totalAssign = $d->count_data_direct("user_leave_id","leave_assign_master","leave_type_id='$data[leave_type_id]'");
                              $totalDefault = $d->count_data_direct("leave_default_count_id","leave_default_count_master","leave_type_id='$data[leave_type_id]'");
                              $totalLeave = $d->count_data_direct("leave_id","leave_master","leave_type_id='$data[leave_type_id]'");
                              if( $totalAssign==0 &&  $totalDefault==0 &&  $totalLeave==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['leave_type_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['leave_type_id']; ?>">                      
                          <?php } ?>    
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['leave_type_name']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['leave_type_created_date'])); ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="leaveTypeDataSet(<?php echo $data['leave_type_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['leave_type_active_status']=="0"){
                              $status = "leaveTypeStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "leaveTypeStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['leave_type_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Type</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addLeaveTypeAdd" action="controller/LeaveTypeController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Leave Type Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="text" class="form-control" placeholder="Leave Type Name" id="leave_type_name" name="leave_type_name" value="">
              </div>  
           </div>                    
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Leave Apply On Date <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="radio" id="leaveApplyOnDateNo" name="leave_apply_on_date" value="0" checked>
                  <label for="leaveApplyOnDateNo">No</label><br>
                  <input type="radio" id="leaveApplyOnDateYes" name="leave_apply_on_date" value="1">
                  <label for="leaveApplyOnDateYes">Yes (Birthday & Anniversary Leave)</label><br>
              </div>  
           </div>                    
           <div class="form-footer text-center">
             <input type="hidden" id="leave_type_id" name="leave_type_id" value="" >
             <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addLeaveType"  value="addLeaveType">
             <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveTypeAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
