<?php error_reporting(0);
extract($_REQUEST);
$survey_id = (int)$survey_id;

if (isset($viewResult) && $viewResult == "yes") {
?>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/plugins/Chart.js/Chart.min.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row pt-2 pb-2">
        <div class="col-sm-9">
          <h4 class="page-title">Survey Result</h4>

        </div>
        <div class="col-sm-3">
          <a href="surveyResultPrint.php?id=<?php echo $survey_id ?>&society_id=<?php echo $_COOKIE['society_id'] ?>" class="btn btn-sm btn-success float-right"><i class="fa fa-print"></i> Print</a>
        </div>
      </div>
      <!-- End Breadcrumb-->
      <script type="text/javascript" src="assets/js/loader.js"></script>
      <div class="row">
        <div class="col-lg-12">
          <div class="card profile-card-2">
            <?php $survey_master_qry = $d->select("survey_master", "society_id='$society_id' AND survey_id='$survey_id'");
            $survey_master_result = mysqli_fetch_assoc($survey_master_qry);
            ?><div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <h5>Survey Title: <?php echo $survey_master_result['survey_title']; ?></h5>
                </div>

                <div class="col-md-12">
                  <?php if (trim($survey_master_result['survey_desciption']) != "") { ?>
                    <p>Description: <?php echo $survey_master_result['survey_desciption']; ?></p>
                  <?php } ?>
                  <p><b>Survey For:</b> <?php
                                        $election_for_string = $xml->string->election_for;
                                        $electionArray = explode("~", $election_for_string);
                                        $survey_for = $survey_master_result['survey_for'];
                                        if ($survey_for == 0) {
                                          echo  $all = $xml->string->all . ' ' . $xml->string->floors;
                                        } else {
                                          $qf = $d->selectRow("floor_name,block_name", "floors_master,block_master", "floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floor_id='$survey_master_result[survey_for]'");
                                          $floorData = mysqli_fetch_array($qf);
                                          echo $floorData['floor_name'] . " (" . $floorData['block_name'] . ")";
                                        }
                                        ?>
                  </p>
                  <p><b>Survey Date: </b><?php echo date("d M Y", strtotime($survey_master_result['survey_date'])); ?></p>
                </div>
              </div>

            </div>
            <?php
            $survey_question_master_qry = $d->select("survey_question_master", "society_id='$society_id' AND survey_id='$survey_id'");
            //   $survey_question_master_result=mysqli_fetch_array($survey_question_master_qry);
            //  echo "<pre>";print_r( $row);
            //   extract($survey_question_master_result);
            while ($survey_question_master_result = mysqli_fetch_array($survey_question_master_qry)) {
              //  echo "<pre>";print_r( $survey_question_master_result);echo "</pre>";
            ?>
              <div class="card-body ">
                <h5 class="card-title">Question: <?php echo $survey_question_master_result['survey_question']; ?></h5>
                <p><b>Survey Option: </b>
                <ul class="list-unstyled">
                  <?php
                  $i11 = 1;
                  $sq = $d->select("survey_option_master", "survey_id = '$survey_id' AND society_id = '$society_id' AND survey_question_id = '$survey_question_master_result[survey_question_id]' ");
                  while ($sData = mysqli_fetch_array($sq)) { ?>
                    <li><?php echo $i11++; ?>. <?php echo $sData['survey_option_name']; ?></li>
                  <?php  } ?>
                </ul>
                </p>
              </div>
              <?php
              $sq = $d->select("survey_result_master", "survey_id = '$survey_id' AND society_id = '$society_id' and survey_question_id = '$survey_question_master_result[survey_question_id]'");
              if (mysqli_num_rows($sq) > 0) {
              ?>
                <?php
                $optionArray = array();
                $optionArrayResult = array();
                ?>
                <div class="card-body border-top">
                  <script>
                    google.charts.load('current', {
                      'packages': ['corechart']
                    });
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                        ['Options', 'Count'],
                        <?php
                        $sq = $d->select("survey_option_master", "survey_id = '$survey_id' AND society_id = '$society_id' and survey_question_id = '$survey_question_master_result[survey_question_id]' ");
                        while ($sData = mysqli_fetch_array($sq)) {
                          $voteCount = $d->count_data_direct("survey_result_id", "survey_result_master", "survey_id = '$survey_id' AND survey_option_id='$sData[survey_option_id]'");
                          $voteCOunt = (int)$voteCount;
                          $survey_option_name = $sData['survey_option_name'] . '';
                          echo "['" . $sData['survey_option_name'] . "', " . $voteCOunt . "],";
                        } ?>
                      ]);
                      var options = {
                        titleTextStyle: {
                          fontSize: 16,
                        },
                        'title': 'Poll Result',
                        'width': 400,
                        'height': 400,
                        'colors': ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                        'is3D': true,
                        //IS_875
                        'sliceVisibilityThreshold': 0
                      };

                      // Display the chart inside the <div> element with id="piechart"
                      var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo $survey_question_master_result['survey_question_id']; ?>'));
                      chart.draw(data, options);
                    }
                  </script>
                  <div id="piechart<?php echo $survey_question_master_result['survey_question_id']; ?>" align='center'></div>
                </div>
              <?php } else {
                echo '<div class="card-body border-top"><center><b>No Result</b></center></div>';
              }
              if ($survey_master_result['is_username_required'] == 1) { ?>
                <div class="table-responsive">
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee -Branch</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      $q = $d->select("survey_result_master,unit_master,block_master,users_master", "users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=survey_result_master.unit_id AND survey_result_master.user_id=users_master.user_id AND survey_result_master.survey_id = '$survey_id' AND survey_result_master.survey_question_id = '$survey_question_master_result[survey_question_id]'");
                      while ($empData = mysqli_fetch_array($q)) {
                      ?>
                        <tr>

                          <td><?php echo $i++; ?></td>
                          <td><?php

                              if (!empty($empData['user_full_name'])) {
                                echo $empData['user_full_name'] . " - ";
                              }
                              if (!empty($empData['user_designation'])) {
                                echo $empData['user_designation'] . " - ";
                              } ?>
                            (<?php echo $empData['block_name']; ?>)</td>
                        </tr>
                      <?php } ?>
                    </tbody>

                  </table>
                </div>
            <?php
              }
            } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=800, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }
  </script>
<?php
} else {
  $q = $d->select("survey_question_master", "survey_id='$survey_id'", "ORDER BY survey_id DESC");
  $survey_master_qry = $d->select("survey_master", "society_id='$society_id' AND survey_id='$survey_id'");
  $survey_master_result = mysqli_fetch_array($survey_master_qry);

?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-6 col-6">
          <h4 class="page-title">Survey Questions</h4>
        </div>
        <div class="col-sm-3 col-6">

        </div>
        <div class="col-sm-3 col-6">
          <div class="btn-group float-sm-right">
            <?php
            $sq = $d->select("survey_result_master", "survey_id = '$survey_id' AND society_id = '$society_id' ");
            if (mysqli_num_rows($sq) == 0) { ?>
              <a href="addSurvey?addQuestion=true&survey_id=<?php echo $survey_id; ?>" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Question</a>
              <a href="javascript:void(0)" onclick="DeleteAll('deleteSurveyQue');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            <?php  } ?>
          </div>
        </div>
      </div>
      <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th>
                      <th>Question</th>
                      <th>Image</th>
                      <th>No. of Options</th>
                      <th>Action</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php

                    $i = 0;
                    while ($row = mysqli_fetch_array($q)) {
                      extract($row);
                      $i++;
                    ?>
                      <tr>
                        <td class='text-center'>
                          <?php
                          $survey_master_qry = $d->select("survey_result_master", "survey_question_id ='$row[survey_question_id]' AND society_id='$society_id'");
                          $result_found = mysqli_num_rows($survey_master_qry);
                          if ($result_found == 0  && mysqli_num_rows($sq) == 0) {
                          ?>
                            <input type="checkbox" class="multiDelteCheckbox" value="<?php echo $row['survey_question_id']; ?>">
                          <?php } ?>
                        </td>
                        <td><?php echo $i; ?></td>
                        <td><?php echo custom_echo($row['survey_question'], 50); ?></td>
                        <td>
                          <?php echo $row['question_image'];
                          if ($row['question_image'] != "") { ?>
                            <a title="<?php echo custom_echo($row['survey_question'], 50); ?>" target="_blank" href="../img/survey/<?php echo $row['question_image'] ?>">
                              <img alt='image' width="30" height="30" src="../img/survey/<?php echo $row['question_image']; ?>">
                            </a>
                          <?php } ?>
                        </td>
                        <td><?php $survey_option_master_qry = $d->select("survey_option_master", "survey_question_id ='$row[survey_question_id]' AND society_id='$society_id'");
                            echo mysqli_num_rows($survey_option_master_qry);
                            ?></td>
                        <td>
                          <?php if ($result_found == 0  && mysqli_num_rows($sq) == 0) { ?>
                            <div style="display: inline-block;">
                              <form class="mr-2" action="addSurvey" method="get">
                                <input type="hidden" name="survey_id" value="<?php echo $row['survey_id']; ?>">
                                <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                                <input type="hidden" name="survey_question_id" value="<?php echo $row['survey_question_id']; ?>">
                                <input type="hidden" name="editQuestion" value="true">
                                <button type="submit" class="btn btn-sm btn-primary"> Edit</button>
                              </form>
                            </div>
                            <div style="display: inline-block;">
                              <form class="mr-2" action="controller/surveyController.php" method="POST">
                                <input type="hidden" name="survey_id" value="<?php echo $row['survey_id']; ?>">
                                <input type="hidden" name="survey_question_id" value="<?php echo $row['survey_question_id']; ?>">
                                <input type="hidden" name="deleteSurveyQuestion" value="deleteSurveyQuestion">
                                <button type="submit" class="btn btn-sm btn-danger form-btn"> Delete</button>
                              </form>
                            </div>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
              </div>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  <!--Start Back To Top Button-->
<?php } ?>