<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />

<?php
extract(array_map("test_input", $_POST));
if (isset($edit_shift_timing)) {
    $q = $d->select("shift_timing_master", "shift_time_id='$shift_time_id'");
    $data = mysqli_fetch_array($q);
?>
<?php extract($data);
}
if (isset($_GET['shftId']) && $_GET['shftId'] != "") {
    $shift_time_id = $_GET['shftId'];
    $q = $d->select("shift_timing_master", "shift_time_id='$shift_time_id'");
    $data = mysqli_fetch_array($q);
    $data['floor_id'] = 0;
}
?>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title"> Shift Timing</h4>

            </div>
            
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php
                        $week_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                        $weeks = array('Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5');
                        ?>

                        <form id="addShiftTiming" action="controller/ShiftTimingController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Shift Name <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input class="form-control" required type="text" name="shift_name" value="<?php if ($data['shift_name'] != "") { echo $data['shift_name'];} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label shiftEndStart">Shift Start Time <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input onchange="changeShiftTIme(this.value)" type="text" id="shift_start_time" class="form-control  changeShiftInTIme" readonly name="shift_start_time" value="<?php if ($data['shift_start_time'] != "") { echo date("H:i", strtotime($data['shift_start_time']));}  ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Shift End Time <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <!-- <input type="text" class="form-control  changeShiftOutTIme shiftEndStart" onchange="changeShiftOutTIme(this.value)" readonly id="shift_end_time" name="shift_end_time" value="<?php if ($data['shift_end_time'] != "") {echo date("H:i", strtotime($data['shift_end_time']));} ?>"> -->
                                            <input type="text" onchange="changeShiftOutTIme(this.value)" class="form-control  changeShiftOutTIme shiftEndStart" readonly id="shift_end_time" name="shift_end_time" value="<?php if ($data['shift_end_time'] != "") {echo date("H:i", strtotime($data['shift_end_time'])); }  ?>">
                                             <i id="showAlert"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Lunch Start Time </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input onchange="changeLunchStartTime(this.value)" type="text" class="form-control lunch_break_start_time" readonly name="lunch_break_start_time" value="<?php if ($data['lunch_break_start_time'] != "" ) { echo date("H:i", strtotime($data['lunch_break_start_time']));}?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Lunch End Time </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" onchange="changeLunchEndTime(this.value)" class="form-control lunch_break_end_time" readonly name="lunch_break_end_time" value="<?php if ($data['lunch_break_end_time'] != "" ) {echo date("H:i", strtotime($data['lunch_break_end_time']));} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Tea Break Start Time </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input onchange="changeTeaBrkStart(this.value)" type="text" class="form-control tea_break_start_time" readonly name="tea_break_start_time" value="<?php if ($data['tea_break_start_time'] != "" ) {echo date("H:i", strtotime($data['tea_break_start_time']));} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Tea Break End Time </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" onchange="changeTeaBrkEndTime(this.value)" class="form-control tea_break_end_time" readonly name="tea_break_end_time" value="<?php if ($data['tea_break_end_time'] != "" ) {echo date("H:i", strtotime($data['tea_break_end_time']));} ?>">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Week off Days </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="week_off_days" type="text" class="form-control  week_off_days" multiple name="week_off_days[]">
                                                <?php
                                                if ($data['week_off_days'] != "") { 
                                                    $week_off_days = explode(',', $data['week_off_days']);
                                                }
                                                for ($i = 0; $i < count($week_days); $i++) {
                                                ?>
                                                    <option <?php if ($data['week_off_days'] != "" && isset($data['week_off_days']) && in_array($i, $week_off_days)) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $i; ?>"><?php echo $week_days[$i]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="checkChnageAltFunction" id="checkChnageAltFunction" value=" <?php if ($data['week_off_days'] != "") {echo 1;} else {echo 0;} ?>">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Half-Day After Time  </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                           
                                            <input type="text" class="form-control  half_day_time_starts " readonly name="half_day_time_start" value="<?php if (isset($data['half_day_time_start']) && $data['half_day_time_start'] != "") {echo date("H:i", strtotime($data['half_day_time_start']));} ?>">
                                            <span class="er" id="half_day_time_start_er"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Half-Day Before Time </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control halfday_before_time" readonly name="halfday_before_time" value="<?php if (isset($data['halfday_before_time']) &&  $data['halfday_before_time'] != "") { echo date("H:i", strtotime($data['halfday_before_time'])); } ?>">
                                            <span class="er" id="halfday_before_time_er"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Mininum Half-Day Hours </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">

                                            <input type="text" autocomplete="off" readonly class="form-control numberEclass maximum_halfday_hours" name="maximum_halfday_hours" value="<?php if (isset($data['maximum_halfday_hours']) && $data['maximum_halfday_hours'] != "") {echo date("h:i", strtotime($data['maximum_halfday_hours']));} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Mininum Full Day Hours </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" autocomplete="off" readonly class="form-control numberEclass minimum_hours_for_full_day" name="minimum_hours_for_full_day" value="<?php if (isset($data['minimum_hours_for_full_day']) && $data['minimum_hours_for_full_day'] != "") { echo date("h:i", strtotime($data['minimum_hours_for_full_day']));
                                         } ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Maximum Late In/Out </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control onlyNumber" max="31" name="maximum_in_out" value="<?php if ($data['maximum_in_out'] != "") {echo $data['maximum_in_out'];} ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">LATE IN COUNT AFTER MINUTES </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <?php 
                                            if($data['late_time_start'] !="" && $data['late_time_start'] !=
                                            "00:00:00"){

                                            $hours = floor($data['late_time_start'] / 60);
                                            $minutes = $data['late_time_start'] % 60;
                                           
                                           $timestr = $data['late_time_start'];
                                            //$timestr = "01:30:00";
                                            $parts = explode(':', $timestr);
                                            $min = ($parts[0] * 60 ) + ($parts[1]) + $parts[2];
                                            }else{
                                                $min = 00;
                                            }
                                            ?>
                                            <input type="text" onkeyup="lateInCountAfter(this.value)" class="form-control numberEClass" min="0" placeholder="15 min" name="late_time_start" value="<?php $to_time1 = strtotime($data['shift_start_time']);
                                            $from_time1 = strtotime($data['late_time_start']);         $minutes1 = round(abs($to_time1 - $from_time1) / 60, 2);
                                            if ($data['late_time_start'] != "" && $min>0) {
                                                echo  $min; } ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php if(isset($data['late_time_start']) && $data['late_time_start'] !=""){ $k = ""; } else { $k = "d-none"; } ?>
                                <div class="col-md-4 <?php echo $k; ?>" id="lateCheckInReasonBlock">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Late in </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <div class="form-check-inline">
                                                <input <?php if (isset($data['late_in_reason']) && $data['late_in_reason'] == 0) { echo "checked";} else {echo "checked";} ?>  class="form-check-input " checked type="radio" name="late_in_reason"  value="0" >
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" >
                                                    No
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input <?php if (isset($data['late_in_reason']) && $data['late_in_reason'] == 1) { echo "checked";} else {echo "";} ?> class="form-check-input " type="radio" name="late_in_reason"   value="1">
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" >
                                                    Yes
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">EARLY OUT COUNT BEFORE MINUTES </label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <?php 
                                                if($data['early_out_time'] !="" && $data['early_out_time'] !=
                                                "00:00:00"){
                                               $timestr2 = $data['early_out_time'];
                                                $parts2 = explode(':', $timestr2);
                                                $min2 = ($parts2[0] * 60 ) + ($parts2[1]) + $parts2[2];
                                                }else{
                                                    $min2 = 00;
                                                }
                                            ?>
                                            <input onkeyup="ealryOutCountAfter(this.value)" type="text" pattern="[0-9]+" class="form-control numberEClass onlyNumber" min="0" placeholder="15 min" name="early_out_time" value="<?php $to_time = strtotime($data['shift_end_time']);
                                        $from_time = strtotime($data['early_out_time']);
                                        $minutes = round(abs($to_time - $from_time) / 60, 2);
                                        if ($data['early_out_time'] != "" && $min2>0) {echo $min2;}
                                       ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php if(isset($data['early_out_time']) &&  $data['early_out_time'] !=""){ $vv =""; } else { $vv = "d-none"; }?>
                                <div class="col-md-4 <?php echo $vv; ?>" id="earlyOutReason">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Reason of Early Out</label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <div class="form-check-inline">
                                                <input class="form-check-input " <?php if (isset($data['early_out_reason']) && $data['early_out_reason'] == 0) { echo "checked";} else {echo "checked";} ?> type="radio" name="early_out_reason"  value="0" >
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" >
                                                    No
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input <?php if (isset($data['early_out_reason']) && $data['early_out_reason'] == 1) { echo "checked";} else {echo "";} ?> class="form-check-input " type="radio" name="early_out_reason"   value="1">
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" >
                                                    Yes
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Has Alternate Week off <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <div class="form-check-inline">
                                                <input class="form-check-input radio_week_off" type="radio" name="has_altenate_week_off" id="exampleRadios1" value="0" <?php if (isset($data['has_altenate_week_off']) && $data['has_altenate_week_off'] == 0) { echo "checked";} else {echo "checked";} ?>>
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" for="exampleRadios1">
                                                    No
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="form-check-input radio_week_off" type="radio" name="has_altenate_week_off" id="exampleRadios2" value="1" <?php if (isset($data['has_altenate_week_off']) && $data['has_altenate_week_off'] == 1) {echo "checked";} ?>>
                                                <span class="checkmark"></span>
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Yes
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 has_altenate_week_off">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select type="text" class="form-control single-select alternate_week_off" multiple name="alternate_week_off[]">
                                                <?php
                                                if ($data['alternate_week_off'] != "") {
                                                    $alternate_week_off = explode(',', $data['alternate_week_off']);
                                                }
                                                for ($j = 0; $j < count($weeks); $j++) {
                                                ?>
                                                    <option <?php if ($data['alternate_week_off'] != "" && isset($data['alternate_week_off']) && in_array($j + 1, $alternate_week_off)) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $j + 1; ?>"><?php echo $weeks[$j]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 has_altenate_week_off">
                                    <input type="hidden" name="hasAlrtWeekDays" id="hasAlrtWeekDays" value="<?php if (isset($data['alternate_weekoff_days']) && $data['alternate_weekoff_days'] != "") {echo $data['alternate_weekoff_days'];} ?>">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alternate Week off Days <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="has_altenate_week_off_opt" type="text" class="form-control single-select alternate_weekoff_days" multiple name="alternate_weekoff_days[]">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Multiple Punch In/Out Allow</label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="radio" name="is_multiple_punch_in" id="MultiplePunch1" value="0" <?php if (isset($data['is_multiple_punch_in']) && $data['is_multiple_punch_in'] == 0) { echo "checked";} else {echo "checked";}  ?>>
                                                <label class="form-check-label" for="MultiplePunch1">
                                                    No
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="radio" name="is_multiple_punch_in" id="MultiplePunch1" value="1" <?php if (isset($data['is_multiple_punch_in']) && $data['is_multiple_punch_in'] == 1) {echo "checked";}  ?>>
                                                <label class="form-check-label" for="MultiplePunch1">
                                                    Yes
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Required Out Of Range Reason</label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="radio" name="take_out_of_range_reason" id="outOfRange2" value="1" <?php if (isset($data['take_out_of_range_reason']) && $data['take_out_of_range_reason'] == 1) {echo "checked";} else {echo "checked";}  ?>>
                                                <label class="form-check-label" for="outOfRange2">
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="form-check-input" type="radio" name="take_out_of_range_reason" id="outRange1" value="0" <?php if (isset($data['take_out_of_range_reason']) && $data['take_out_of_range_reason'] == 0) { echo "checked";} ?>>
                                                <label class="form-check-label" for="outRange1">
                                                    No
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Maximum Time for Punch Out Allow</label>
                                        <div class="col-lg-5 col-md-5 col-5 " id="">
                                            <select onchange="chekMiniShiftHoursForPunchOut();" class="form-control max_shift_hour11" name="max_shift_hour">
                                                <?php if ($data['max_shift_hour'] == "") { ?>
                                                <option value="0">Select Hours</option>
                                                <?php } else  { ?>
                                                <option value="0">Remove Hours</option>
                                                <?php } $min=array("00","15","30","45");
                                                    for($i=1;$i<48.15;$i++) {  
                                                      foreach ($min as $v) { ?>
                                                        <option <?php if ($data['max_shift_hour'] == "$i:$v:00") { echo 'selected';} ?>  value="<?php echo "$i:$v:00";?>"><?php echo "$i:$v:00";?></option>
                                                       <?php } }
                                            
                                                    ?>
                                            </select>

                                        </div>
                                        <div class="col-lg-2 col-md-2 col-2 text-center" id="">
                                            OR
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-5" id="">
                                            <input  onchange="checkaMaxPunchOutTime(this.value)" type="text" class="form-control max_punch_out_time" id="max_punch_out_time" name="max_punch_out_time" placeholder="Set Time"  value="<?php if ($data['max_punch_out_time'] != "" ) { echo $data['max_punch_out_time'];} ?>">
                                            <span>After Shift End Time</span>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="is_Edit" id="is_Edit" value="<?php if (isset($data['has_altenate_week_off']) == 1) { echo 1; } else {echo 0;} ?>">
                                <input type="hidden" name="shiftDiffrance" id="shiftDiffrance">
                                <?php //IS_1019 addPenaltiesBtn 
                                     if (isset($edit_shift_timing)) {
                                ?>
                                    <input type="hidden" id="shift_time_id" name="shift_time_id" value="<?php if ($data['shift_time_id'] != "") {echo $data['shift_time_id'];} ?>">
                                    <button id="addShiftTimingBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                                    <input type="hidden" name="addShiftTiming" value="addShiftTiming">
                                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" 
                                    ?>
                                    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addShiftTiming');"><i class="fa fa-check-square-o"></i> Reset</button>
                                <?php } else {
                                ?>
                                    <button id="addShiftTimingBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                    <input type="hidden" name="addShiftTiming" value="addShiftTiming">

                                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" 
                                    ?>
                                    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addShiftTiming');"><i class="fa fa-check-square-o"></i> Reset</button>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function lateInCountAfter(value){
        if(parseInt(value)>0){
            $('#lateCheckInReasonBlock').removeClass('d-none');
        }
        else
        {
            $('#lateCheckInReasonBlock').addClass('d-none');
        }
    }
    function ealryOutCountAfter(value){
        if(parseInt(value)>0){
            $('#earlyOutReason').removeClass('d-none');
        }
        else
        {
            $('#earlyOutReason').addClass('d-none');
        }
    }
    document.querySelector(".numberEClass").addEventListener("keypress", function(evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    });

    <?php if (isset($data['per_day_hour']) && $data['has_altenate_week_off'] == 1) { ?>
        $(".has_altenate_week_off").show();
    <?php } else { ?>
        $(".has_altenate_week_off").hide();
    <?php } ?>
    <?php if (isset($data['late_time_start']) && $data['late_time_start'] != "" && $data['late_time_start'] != "00:00:00") { ?>
        $('#lateCheckInReasonBlock').removeClass('d-none');
    <?php } else { ?>
        $('#lateCheckInReasonBlock').addClass('d-none');
    <?php } ?>
    <?php if (isset($data['early_out_time']) && $data['early_out_time'] != "" && $data['early_out_time'] != "00:00:00") { ?>
        $('#earlyOutReason').removeClass('d-none');
    <?php } else { ?>
        $('#earlyOutReason').addClass('d-none');
    <?php } ?>

    function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
    }

    function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
    }

    function changeLunchStartTime(value) {
        $('input[name="lunch_break_end_time"]').val('');
    }

    function changeLunchEndTime(value) {
        if ($('input[name="lunch_break_start_time"]').val()=='') {
            swal("Please set lunch start time", {
                icon: "warning",
            });
            $('input[name="lunch_break_end_time"]').val('');
        } else if (value == $('input[name="lunch_break_start_time"]').val()) {
            swal("Lunch Break start & End Time Not equal", {
                icon: "warning",
            });
            $('input[name="lunch_break_end_time"]').val('');
        }

    }

    function checkaMaxPunchOutTime(value) {
        shift_start_time =   $('#shift_start_time').val();
        shift_end_time = $('#shift_end_time').val();

        max_shift_hour11 = $(".max_shift_hour11").val().split(':')[0], 10;

        if (shift_end_time != "" && shift_end_time!= "") {

            var totaShiftHours = parseFloat(shift_end_time.split(':')[0], 10) - parseFloat(shift_start_time.split(':')[0], 10);
            if (shift_start_time > shift_end_time) {
                totaShiftHours = parseFloat(totaShiftHours)+24;
            }

            if (shift_start_time > shift_end_time) {

            } else {
                if (value >= shift_start_time && value<shift_end_time) {
                    swal("Please Select After Shift Hours Time", {
                      icon: "warning",
                    });
                    $('input[name="max_punch_out_time"]').val('');
                } 
            }


            $('.max_shift_hour11').prop('selectedIndex',0);

        } else {
            swal("Please Select Shift Time First", {
              icon: "warning",
            });
            $('.max_shift_hour11').prop('selectedIndex',0);
            $('input[name="max_punch_out_time"]').val('');
        }

    }

    function chekMiniShiftHoursForPunchOut() {
        shift_start_time =   $('#shift_start_time').val();
        shift_end_time = $('#shift_end_time').val();

        var a11 = $(".max_shift_hour11").val().split(':'); // split it at the colons
        var max_shift_hour11minutes = (+a11[0]) * 60 + (+a11[1]);
        // console.log(max_shift_hour11minutes);

        max_shift_hour11 = $(".max_shift_hour11").val().split(':')[0], 10;


        if (shift_end_time != "" && shift_end_time!= "") {


            var totaShiftHours = parseFloat(shift_end_time.split(':')[0], 10) - parseFloat(shift_start_time.split(':')[0], 10);
            if (shift_start_time > shift_end_time) {
                totaShiftHours = parseFloat(totaShiftHours)+24;
                var diff = Math.abs(new Date('2011/10/09 '+shift_start_time) - new Date('2011/10/10 '+shift_end_time));
                var shiftMinutues = Math.floor((diff/1000)/60);
            } else {

                var diff = Math.abs(new Date('2011/10/09 '+shift_start_time) - new Date('2011/10/09 '+shift_end_time));
                var shiftMinutues = Math.floor((diff/1000)/60);
            }
            


            if (max_shift_hour11minutes <= shiftMinutues) {
                swal("Please set hours greater than shift hours ", {
                  icon: "warning",
                });
                $('.max_shift_hour11').prop('selectedIndex',0);
                $('input[name="max_punch_out_time"]').val('');  
            } else {
                $('input[name="max_punch_out_time"]').val('');  
            }

        } else {
            swal("Please Select Shift Time First", {
              icon: "warning",
            });
            $('.max_shift_hour11').prop('selectedIndex',0);
            $('input[name="max_punch_out_time"]').val('');

        }

    }

    function changeShiftTIme(value) {
        
        $('input[name="shift_end_time"]').val('');
        $('input[name="max_punch_out_time"]').val('');

        $('.max_shift_hour11').prop('selectedIndex',0);

        // if (value > shift_end_time) {
        //     swal("Its Night Shift", {
        //         icon: "warning",
        //     });
        // }
        if (shift_end_time != "") {

            var diff = (new Date("1970-1-1 " + shift_end_time) - new Date("1970-1-1 " + value)) / 1000 / 60 / 60;
            $('#shiftDiffrance').val(diff);
        }
        shift_start_time = value;
        lunch_break_start_time = $('.lunch_break_start_time').val();
        tea_break_start_time = $('.tea_break_start_time').val();
        halfday_before_time = $('.halfday_before_time').val();
        half_day_time_starts = $('.half_day_time_starts ').val();
        if(lunch_break_start_time !=""){
            if (lunch_break_start_time < shift_start_time) {
                $('.lunch_break_start_time').val('');
                }
                if (lunch_break_start_time > shift_end_time) {

                $('.lunch_break_start_time').val('');
                }
        }
        if(tea_break_start_time !=""){

            if (tea_break_start_time < shift_start_time) {
            $('.tea_break_start_time').val('');
            }
            if (tea_break_start_time > shift_end_time) {
                $('.tea_break_start_time').val('');
            }
        }
        if(halfday_before_time !=""){
            if (halfday_before_time < shift_start_time) {
                $('.halfday_before_time').val('');
            }
            if (halfday_before_time > shift_end_time) {
                $('.halfday_before_time').val('');
            }
        }
        if(half_day_time_starts !=""){
            if (half_day_time_starts < shift_start_time) {
                 $('.half_day_time_starts').val('');
            }
            if (half_day_time_starts > shift_end_time) {
                $('.half_day_time_starts').val('');
            }
        }

        <?php if (isset($edit_shift_timing)) { ?>
            if (shift_start_time < shift_end_time) {
                if (lunch_break_start_time < shift_start_time) {

                    $('.lunch_break_start_time').val('');
                }
                if (lunch_break_start_time > shift_end_time) {

                    $('.lunch_break_start_time').val('');
                }
                if (tea_break_start_time < shift_start_time) {

                    $('.tea_break_start_time').val('');
                }
                if (tea_break_start_time > shift_end_time) {

                    $('.tea_break_start_time').val('');
                }
                if (halfday_before_time < shift_start_time) {

                    $('.halfday_before_time').val('');
                }
                if (halfday_before_time > shift_end_time) {

                    $('.halfday_before_time').val('');
                }
                if (half_day_time_starts < shift_start_time) {

                    $('.half_day_time_starts').val('');
                }
                if (half_day_time_starts > shift_end_time) {

                    $('.half_day_time_starts').val('');
                }
            } else {
                if (shift_end_time < lunch_break_start_time && shift_start_time > lunch_break_start_time) {
                    $('.lunch_break_start_time').val('');
                }
                if (shift_end_time < tea_break_start_time && shift_start_time > tea_break_start_time) {
                    $('.tea_break_start_time').val('');
                }
                if (shift_end_time < halfday_before_time && shift_start_time > halfday_before_time) {
                    $('.halfday_before_time').val('');
                }
                if (shift_end_time < half_day_time_starts && shift_start_time > half_day_time_starts) {
                    $('.half_day_time_starts').val('');
                }
            }

            shift_start_time = $('#shift_start_time').val();
            shift_end_time = $('#shift_end_time').val();
            var startTime = moment(shift_start_time, 'hh:mm:ss');
            var endTime = moment(shift_end_time, 'hh:mm:ss');
            var totalMinutes = endTime.diff(startTime, 'minutes');
            ////////minutes to hours calculation 
            var num = totalMinutes;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            shift_diffrance_hours = (rhours + ":" + rminutes);
            /////////////////////minutes to hours calculation 
            minimum_hours = $('.minimum_hours_for_full_day').val();
            maximum_halfday_hours = $('.maximum_halfday_hours').val();

            //shift_diffrance_hours = parseFloat(shift_diffrance_hours);
            /* minimum_hours = parseFloat(minimum_hours);
            maximum_halfday_hours = parseFloat(maximum_halfday_hours); */
            if (shift_diffrance_hours < minimum_hours) {
                $('.minimum_hours_for_full_day').val("");
            }
            if (shift_diffrance_hours < maximum_halfday_hours) {
                $('.maximum_halfday_hours').val("");
            }

        <?php } ?>
    }

    function changeTeaBrkStart(value) {
        $('input[name="tea_break_end_time"]').val('');
    }

    function changeTeaBrkEndTime(value) {
        if ($('input[name="tea_break_start_time"]').val()=='') {
            swal("Please set tea break start time ", {
                icon: "warning",
            });
            $('input[name="tea_break_end_time"]').val('');
        } else 
        if (value == $('input[name="tea_break_start_time"]').val()) {
            swal("Tea Break start & End Time Not equal", {
                icon: "warning",
            });
            $('input[name="tea_break_end_time"]').val('');
        }

    }

    function changeShiftOutTIme(value) {
        var shift_end_time = value;
        var shift_start_time = $('#shift_start_time').val();
        half_day_time_start = $('#half_day_time_start').val();
        halfday_before_time = $('#halfday_before_time').val();

        $('input[name="max_punch_out_time"]').val('');
        $('.max_shift_hour11').prop('selectedIndex',0);

        if (shift_start_time == shift_end_time) {
            swal("Shift End Not Equal to Start", {
                icon: "warning",
            });
            $('input[name="shift_end_time"]').val('');

        }
        if(shift_end_time !=""){

            if (shift_start_time > shift_end_time) {
           
                showAlert = `<h6 class="text-danger " role="alert">
                                 Night shift (Next day end)
                             </h6>`;
                             $('#showAlert').html(showAlert);
            

            } else {
                 $('#showAlert').html("");
            }
        }
        if (shift_start_time > half_day_time_start) {
            if (half_day_time_start != "") {
                $('#half_day_time_starts').val("");
            }
        } else {
            $('#half_day_time_start_er').hide();
        }
        if (halfday_before_time > shift_end_time) {
            $('#halfday_before_time').val("");
        } else {
            $('#halfday_before_time_er').hide();
        }
        var diff = (new Date("1970-1-1 " + shift_end_time) - new Date("1970-1-1 " + shift_start_time)) / 1000 / 60 / 60;
        $('#shiftDiffrance').val(diff);

        <?php if (isset($edit_shift_timing)) { ?>
            shift_start_time = $('#shift_start_time').val();
            shift_end_time = $('#shift_end_time').val();
            var startTime = moment(shift_start_time, 'hh:mm:ss');
            var endTime = moment(shift_end_time, 'hh:mm:ss');
            if (shift_start_time > shift_end_time) { 
            var timeStart = new Date("01/01/2022 " + shift_start_time).getHours();
            var timeEnd = new Date("02/01/2022 " + shift_end_time).getHours();
            var timeStartMi = new Date("01/01/2022 " + shift_start_time).getMinutes();
            var timeEndMin = new Date("02/01/2022 " + shift_end_time).getMinutes();
            var hourDiff = timeEnd - timeStart;  
            var MinDiff = timeEndMin - timeStartMi;  
            hourDiff = parseInt(hourDiff);
            MinDiff = parseInt(MinDiff);
            if (MinDiff == 0) {
                MinDiff = "00";
            }
            else
            {
                if (MinDiff < 10) {
                MinDiff = "0" + MinDiff;
                }
                else
                {
                MinDiff = MinDiff;
                }
            }
            shift_diffrance_hours = hourDiff + parseInt(24)+":" + MinDiff;;
            }
            else
            {
            var totalMinutes = endTime.diff(startTime, 'minutes');
            var num = totalMinutes;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            if (rminutes == 0) {
                rminutes = "00";
            } else
            {
                rminutes = parseInt(rminutes);
            }
            if (rminutes < 10) {
                rminutes = "0" + rminutes;
            }
            shift_diffrance_hours = (rhours + ":" + rminutes);
            }
            /////////////////////minutes to hours calculation 
            minimum_hours_for_full_day = $('.minimum_hours_for_full_day').val();
            maximum_halfday_hours = $('.maximum_halfday_hours').val();
            if(parseFloat(shift_diffrance_hours)<=parseFloat(minimum_hours_for_full_day))
            {
                if (parseFloat(minimum_hours_for_full_day) == parseFloat(shift_diffrance_hours)) {
                    shift_diffrance_hours = shift_diffrance_hours.split(':');
                    minimum_hours_for_full_day = minimum_hours_for_full_day.split(':');
                  
                    if (minimum_hours_for_full_day[1] > shift_diffrance_hours[1])
                    {
                    $('.minimum_hours_for_full_day').val("");
                    swal("No More Then Shift Total Hours ", {
                        icon: "warning",
                    });
                    }
                }
                else {
                    $('.minimum_hours_for_full_day').val("");
                    swal("No More Then Shift Total Hours ", {
                    icon: "warning",
                    });
                }
            }
            if(parseFloat(shift_diffrance_hours)<=parseFloat(maximum_halfday_hours))
            {
                if (parseFloat(maximum_halfday_hours) == parseFloat(shift_diffrance_hours)) {
                    shift_diffrance_hours = shift_diffrance_hours.split(':');
                    maximum_halfday_hours = maximum_halfday_hours.split(':');
                 
                    if (maximum_halfday_hours[1] > shift_diffrance_hours[1])
                    {
                    $('.maximum_halfday_hours').val("");
                    swal("No More Then Shift Total Hours ", {
                        icon: "warning",
                    });
                    }
                }
                else {
                    $('.maximum_halfday_hours').val("");
                    swal("No More Then Shift Total Hours ", {
                    icon: "warning",
                    });
                }
            }
            
        <?php } ?>

    }
    
</script>
<style>
    .er {
        color: red;
    }
</style>