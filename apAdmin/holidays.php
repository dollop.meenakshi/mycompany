<?php
error_reporting(0);
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$laYear = (int)$_REQUEST['laYear'];
$gd = $d->selectRow("holiday_group_id,holiday_group_name","holiday_group_master","holiday_group_active_status = 0");
$all = [];
while($data = $gd->fetch_assoc())
{
    $all[] = $data;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-4 col-4">
                <h4 class="page-title">Holiday</h4>
            </div>
            <div class="col-sm-3 col-md-4 col-4">
                <form action=""  class="">
                    <div class=" form-group">
                        <select name="laYear" class="form-control" onchange="this.form.submit()">
                            <option <?php if($laYear==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                            <option <?php if($laYear==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                            <option <?php if($laYear==$currentYear) { echo 'selected';}?> <?php if(isset($laYear) && $laYear=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                            <option <?php if($laYear==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="col-sm-3 col-md-4 col-4">
                <div class="btn-group float-sm-right">
                    <a href="holiday"  class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteHoliday');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                    <!-- <a href="controller/BulkHolidayController.php"  class="btn btn-sm btn-warning" >Add Last Year</a> -->
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php //IS_675  example to examplePenaltiesTbl?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Start Date</th>
                                        <th>Image</th>
                                        <th>Day</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $idArray = array();
                                    if($laYear !="" AND $laYear>0)
                                    {
                                    $yearFilter = "AND DATE_FORMAT(holiday_master.holiday_start_date, '%Y')='$laYear'";
                                    }else{
                                    $yearFilter = "AND DATE_FORMAT(holiday_master.holiday_start_date, '%Y')='$currentYear'";
                                    }
                                    $q = $d->select("holiday_master LEFT JOIN floors_master ON floors_master.floor_id= holiday_master.floor_id ", " holiday_master.society_id='$society_id' $yearFilter", "ORDER BY holiday_master.holiday_start_date ASC");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q))
                                    {
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                        <?php if( $data['holiday_start_date'] > date('Y-m-d'))
                                                { ?>
                                            <input type="hidden" name="id" id="id"  value="<?php echo $data['holiday_id']; ?>">
                                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['holiday_id']; ?>">
                                        <?php } ?>
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['holiday_name']; ?></td>
                                        <td><?php if($data['floor_id']==0){echo "All Department"; }else { echo $data['floor_name']; } ?></td>
                                        <td><?php echo $data['holiday_start_date']; ?></td>
                                        
                                        <td><?php if ($data['holiday_image'] !=""){ ?>
                                            <img width="50px" height="50px" src="../img/holiday/<?php echo $data['holiday_image']; ?>" >
                                        <?php } ?></td>
                                        <td><?php echo date('l', strtotime($data['holiday_start_date'])); ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <?php
                                                if( $data['holiday_start_date'] < date('Y-m-d'))
                                                {
                                                    $disabled = "disabled";
                                                }
                                                else
                                                {
                                                    $disabled = "";
                                                }
                                                ?>
                                                <form action="holiday" method="post" accept-charset="utf-8">
                                                    <input type="hidden" name="holiday_id" value="<?php echo $data['holiday_id']; ?>">
                                                    <input type="hidden" name="edit_holiday" value="edit_holiday">
                                                    <button <?php echo $disabled; ?> type="submit" class="btn btn-sm btn-primary mr-1"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                                <?php
                                                if ($data['holiday_status'] == "0")
                                                {
                                                    $status = "holidayStatusDeactive";
                                                    $active = "checked";
                                                }
                                                else
                                                {
                                                    $status = "holidayStatusActive";
                                                    $active = "";
                                                }
                                                ?>
                                                <input <?php echo $disabled; ?> type="checkbox" <?php echo $active; ?>  class="js-switch mr-1" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['holiday_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                                <!--  <a  href="#" data-toggle="modal" data-target="#payBill" onclick="holidayModal(<?php echo $data['holiday_id']; ?>)" >
                                                    <button class="btn btn-sm btn-secondary mr-1">View</button>
                                                </a> -->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="payBill">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Holiday Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="holiday" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function popitup(url)
{
newwindow=window.open(url,'name','height=800,width=900, location=0');
if (window.focus) {newwindow.focus()}
return false;
}
</script>