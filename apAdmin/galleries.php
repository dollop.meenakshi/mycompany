<?php
extract($_REQUEST);
$bId = $_REQUEST['bId'];
$dId = $_REQUEST['dId'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
   
    <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">Photo Gallery</h4>
        
      </div>
      <div class="col-sm-6">
        <div class="btn-group float-right">
          <?php
                if (isset($gallery_album_id)) { ?>
          <form action="controller/galleryController.php" class="mr-2"  method="POST" >
            <?php //IS_570 ?>
            <input type="hidden" name="gallery_album_id" value="<?php echo $gallery_album_id; ?>">
            <input type="hidden" name="eName" value="<?php echo $eName; ?>">
           
            <input type="hidden" value="deleteGalleryAlbum" name="deleteGalleryAlbum">
          <button type="submit" class="form-btn btn btn-sm btn-danger" name="" ><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Album</button>
          </form>
        <?php } if (isset($gallery_album_id)) { ?>
          <a href="galleries?gallery_album_id=<?php echo $gallery_album_id;?>&addNew=yes" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Photo</a>
        <?php } else { ?>
          <a href="gallery" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Photo/Album</a>

        <?php } ?>
        </div>
      </div>
    </div>
    <form id="">
      <div class="row pt-2 pb-2">
        <?php  if(!isset($gallery_album_id)){ ?>

            <div class="col-md-3 form-group">
            <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" >
              <option value="">-- Select Branch --</option> 
              <?php 
              $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
              while ($blockData=mysqli_fetch_array($qb)) {
              ?>
              <option   <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
              <?php } ?>

            </select>         
            </div>

            <div class="col-md-3 form-group">
            <select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" >
            <option value="">-- Select Department --</option> 
            <?php 
              $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
              while ($depaData=mysqli_fetch_array($qd)) {
            ?>
            <option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
            <?php } ?>

            </select>
            </div>
        
       
        <div class="col-md-3 form-group mt-auto">
            <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>
          <?php } ?>
      </div>
      
    </form>
    <!-- End Breadcrumb-->
    <?php if (isset($addNew)) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php $q = $d->selectRow("block_id","gallery_album_master","gallery_album_id='$gallery_album_id'");
            $data = mysqli_fetch_array($q) ?>
            <form id="addGallary" method="post" action="controller/galleryController.php" enctype="multipart/form-data">
              <input type="hidden" name="gallery_album_id" value="<?php echo $gallery_album_id;?>">
              <input type="hidden" name="block_id" value="<?php echo $data['block_id'];?>">
              <input type="hidden" name="floor_id" value="<?php echo $data['floor_id'];?>">
               <input type="hidden" name="backUrl" value="backUrl">

              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label"> Photo <span class="required">*</span></label>
                <div class="col-sm-10">
                   
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" required="" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <?php //IS_988 galary_photos ?>
                  <div id="galary_photos_div"   ></div>
                  <br> <?php //IS_653 , 20 Files at a time added?> 
                  <!-- <h6>Note: Press CTRL key to Upload Multiple Photos(Max Size 2MB Per Photo, 20 Files at a time)</h6> -->
                </div>
                
              </div>
              
              <input type="hidden" name="society_id" value="<?php echo $society_id ?>">
              <div class="form-footer text-center">
                <input type="hidden" name="addGallery" value="addGallery">
                <button type="submit" class="btn btn-success" name = ""><i class="fa fa-check-square-o"></i> SAVE</button>
              </div>
            </form>
           

          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-12">
       
        <div id="photo_id">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <?php
                if (!isset($gallery_album_id)) {
                  
                  if(isset($bId) && $bId>0){
                    $branchFltr = " AND block_id=$bId";
                  }
                  if(isset($dId) && $dId>0){
                    $dptFlt = " AND FIND_IN_SET($dId,gallery_album_master.floor_id)";
                  }

                error_reporting(0);
                $q = $d->select("gallery_album_master","society_id='$society_id' $branchFltr $dptFlt","ORDER BY  gallery_album_id DESC");
                
                if (mysqli_num_rows($q)>0) {
                while($data = mysqli_fetch_array($q)){
                  $album_title = str_replace("'", "\'", $data['album_title']);
                 
                ?>
                <div class="col-md-2 col-6 text-center">
                  <a href="galleries?gallery_album_id=<?php echo $data['gallery_album_id'] ?>">
                  <div class="folder" style="background-image: url(img/folder.png);background-repeat: no-repeat;background-size: cover;height: 175px;width: 100%;text-align: center;text-transform: capitalize;">
                  </div>
                  <?php echo $data['album_title']; ?> (<?php echo $d->count_data_direct("gallery_id","gallery_master","society_id='$society_id' AND gallery_album_id='$data[gallery_album_id]'",""); ?>)
                  </a>
                </div>
                <?php }
              } else {
                 echo "<img width='250' src='img/no_data_found.png'>";
              }

                 } ?>
              </div>
              <div class="row">
                <?php   if (isset($gallery_album_id)) {
                  // /IS_1002
                if($gallery_album_id!=0) {
                $eqm = $d->select("gallery_master,gallery_album_master","gallery_master.gallery_album_id=gallery_album_master.gallery_album_id AND gallery_master.gallery_album_id='$gallery_album_id' AND gallery_master.society_id = '$society_id'");

                } else {
                //$eqm = $d->select("gallery_master","gallery_title='$_GET[eName]' AND society_id = '$society_id'");

                }
                $gallaryData = mysqli_fetch_assoc($eqm);
                 ?>
                 
                 

                 <div class="col-md-3">
                  <a style="margin-bottom: 20px;" href="galleries" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back To Folder</a>
                  
                </div>
                <!-- <div class="col-md-3"></div> -->
                <div class="col-md-9">
                  <?php  //IS_1046
                  if (mysqli_num_rows($eqm) > 0){ ?>
                  <lable class="card-title text-primary text-center"> <span class="text-center"><?php echo  $gallaryData['album_title'];?>  </span> </lable> 
                <?php } ?>
                </div>
                <!-- <div class="col-md-3"></div> -->



               
                <?php
               if($gallery_album_id!=0) {
                $eq = $d->select("gallery_master,gallery_album_master","gallery_master.gallery_album_id=gallery_album_master.gallery_album_id AND gallery_master.gallery_album_id='$gallery_album_id' AND gallery_master.society_id = '$society_id'");
              } else {
               // $eq = $d->select("gallery_master","gallery_title='$_GET[eName]' AND society_id = '$society_id'");
                
              }
              
                if(mysqli_num_rows($eq)>0) {
                while ($pData = mysqli_fetch_array($eq)) {
                  
                ?>

                <div class="col-md-6 col-6 col-lg-2 col-xl-2">
                  <a href="../img/gallery/<?php echo $pData['gallery_photo'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $pData["album_title"]; ?>">
                    <img src="../img/ajax-loader.gif" data-src="../img/gallery/<?php echo $pData['gallery_photo'] ?>" alt="<?php echo $pData['album_title'] ?>" class="lightbox-thumb img-thumbnail lazyload" style="height:210px;object-fit: cover;">
                  </a>
                  <div class="row pl-1 pb-2 text-center">
                   <div class="col-md-6 col-6">
                    <a href="../img/gallery/<?php echo $pData['gallery_photo'] ?>" download="<?php echo $pData['gallery_photo'] ?>">
                    <button class="btn btn-sm btn-primary" style="margin-right: 5px;"><i class="fa fa-download" aria-hidden="true"></i></button></a>
                  </div>
                  <div class="col-md-6 col-6">
                    <form action="controller/galleryController.php" method="POST" >
                      <input type="hidden" name="gallery_id" value="<?php echo $pData['gallery_id']; ?>">
                      <input type="hidden" name="gallery_path" value="../../img/gallery/<?php echo $pData['gallery_photo'] ?>">
                      <?php //IS_570 ?>
                      <input type="hidden" name="gallery_album_id" value="<?php echo $pData['gallery_album_id']; ?>">
                      <input type="hidden" name="eName" value="<?php echo $eName; ?>">

                      <input type="hidden" value="deleteGalleryPhoto" name="deleteGalleryPhoto">
                    <button type="submit" class="form-btn btn btn-sm btn-danger" name="" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                   </div>
                  </div>
                  
                </div>
                <?php
                  }  } else {
                  echo "<img width='250' src='img/no_data_found.png'>";
                } 
              }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div><!--End Row-->
      
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->