<?php 
 
extract(array_map("test_input" , $_REQUEST));

if(isset($id) && isset($viewDetails) && $viewDetails=="yes") { 
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) != true || (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) != true)) {
  $_SESSION['msg1']="Please select admin";
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='companyAdmins';
      </script>");
  exit(); 
}
  $q=$d->selectRow("bms_admin_master.*,role_master.role_name,role_master.admin_type as role_admin_type","role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.admin_id='$id'","");

if (mysqli_num_rows($q)==0) {
  $_SESSION['msg1']="Invalid request";
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='companyAdmins';
      </script>");
  exit(); 
}

  $data=mysqli_fetch_array($q);
  
?>
<link href="assets/plugins/fullcalendar/css/fullcalendar.min.css" rel='stylesheet'/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row ">
        <div class="col-sm-9">
        <h4 class="page-title"> Manage <?php echo $xml->string->committee; ?> </h4>
        </div>
     </div>
    <!-- End Breadcrumb-->
    
      <div class="row">
        <div class="card-body">
          <div class="row">
            <div class="col-sm-3 text-center">
              <div class="card">
                <div class="card-body">
                  <a href="../img/society/<?php echo $data['admin_profile']; ?>" data-fancybox="images" data-caption="Photo Name : Profile Photo"> 
                  <img  onerror="this.src='img/user.png'" height="180" width="180" src="img/user.png"  data-src="../img/society/<?php echo $data['admin_profile']; ?>" class="rounded-circle shadow lazyload" alt="">
                  </a>
                  <h5 class="text-capitalize pt-2"><?php echo $data['admin_name'];?></h5>
                  <p class="text-capitalize pt-2"><?php echo $data['role_name'];?></p>
                  <ul  class="list-inline text-center mt-3">

                     <li class="list-inline-item">
                      <?php if ($adminData['admin_type']==1 || $adminData['role_id']==2){?>
                      <form title="Edit"  style="float: left;" class="mr-2" action="companyAdmin" method="post">
                        <input type="hidden" name="admin_id_edit"  value="<?php echo $data['admin_id']; ?>">
                        <input type="hidden" name="role_id_edit"  value="<?php echo $data['role_id']; ?>">
                        <input type="hidden" name="editEvent" value="editEvent" placeholder="">
                        <button type="submit" name=""  value="" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>
                      </form>
                      <?php } if ($data['admin_type']!=1 && $data['admin_active_status']=="0") { ?>
                      <form  title="Deactive Account" style="float: left;" class="mr-2"  action="controller/buildingController.php" method="post">
                        <input type="hidden" name="adminNa"  value="<?php echo $data['admin_name']; ?>">
                        <input type="hidden" name="adminMobile"  value="<?php echo $data['admin_mobile']; ?>">
                        <input type="hidden" name="device"  value="<?php echo $data['device']; ?>">
                        <input type="hidden" name="token"  value="<?php echo $data['token']; ?>">
                        <input type="hidden" name="admin_id_delete"  value="<?php echo $data['admin_id']; ?>">
                        <input type="hidden" name="role_id_delete"  value="<?php echo $data['role_id']; ?>">
                        <input type="hidden" name="admin_profile" value="<?php if($data['admin_profile']!="user.png") { echo $data['admin_profile']; } ?>">
                        <button type="submit" name=""  value="" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                      </form>
                      <?php } else if($data['admin_active_status']=="1"){ ?>
                      <form title="Delete" style="float: left;" class="mr-2" action="controller/buildingController.php" method="post"  >
                        <input type="hidden" name="admin_id_restore" value="<?php echo $id; ?>">

                        <input type="hidden" value="restoreAdmin" name="restoreAdmin">
                        <input type="hidden" name="restoreAdminName" value="<?php echo $data['admin_name'];?>">
                        <button type="submit"  class="btn btn-warning btn-sm form-btn"><i class="fa fa-undo"></i> Restore</button>
                      </form>
                      <?php } ?>
                    </li>
                    
                  </ul>
                </div>
              </div>
            </div>  
            <div  class="col-sm-9 ">
              <div class="card">
                <div class="card-body">
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0"><?php echo $xml->string->mobile_no; ?> :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php echo $data['country_code'].' '.$data['admin_mobile'];?>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0"><?php echo $xml->string->members; ?> <?php echo $xml->string->block_type; ?> :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php if($data['user_type']=='0'){ echo $xml->string->committee;  }  else { echo $xml->string->representative;  } ?>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0"><?php echo $xml->string->email_id; ?> :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php echo $data['admin_email'];?>
                    </div>
                  </div>
                  <?php if($data['app_version']!='') { ?>

                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">App Version & Phone :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php echo $data['app_version'];?> - (<?php echo $data['phone_model'];?>-<?php echo $data['phone_brand'];?>)
                    </div>
                  </div>
                  <?php } ?>
                  <?php if($data['token']!='') { ?>
                    
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">App <?php echo $xml->string->logout; ?> :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <form  style="float: left;"   class="mr-2"  title="Logout From App" action="controller/buildingController.php" method="post" >
                        <input type="hidden" name="admin_id_logout" value="<?php echo $data['admin_id']; ?>">
                        <input type="hidden" name="device" value="<?php echo $data['device']; ?>">
                        <input type="hidden" name="token" value="<?php echo $data['token']; ?>">
                        <input type="hidden" name="admin_logout" value="admin_logout">
                        <input type="hidden" name="logoutAdminName" value="<?php echo $data['admin_name']; ?>">
                        <button type="submit" class="btn btn-sm form-btn btn-danger" ><i class="fa fa-power-off" aria-hidden="true"></i> <?php echo $xml->string->logout; ?></button> (<?php echo $data['device'];?>)
                      </form>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">Send Welcome Mail :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <form style="float: left;"  title="Send Welcome Mail" action="controller/buildingController.php" method="post" >
                        <input type="hidden" name="admin_id_mail" value="<?php echo $data['admin_id']; ?>">
                        <input type="hidden" name="device" value="<?php echo $data['device']; ?>">
                        <input type="hidden" name="token" value="<?php echo $data['token']; ?>">
                        <button type="submit" class="btn btn-sm form-btn btn-info" ><i class="fa fa-envelope" aria-hidden="true"></i> </button>
                      </form>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">Send Password Reset Link :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <form  style="float: left;" class="mr-2" title="Send Password <?php echo $xml->string->reset; ?> Link "  action="controller/buildingController.php" method="post">
                        <input type="hidden" name="forgot_email_admin_committee" value="<?php echo $data['admin_email'] ?>">
                        <button type="submit"  value="View" class="btn btn-sm btn-warning"><i class="fa fa-key" aria-hidden="true"></i> </button>
                      </form>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">Branch Access :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php  
                       $access_branchs=$data['access_branchs'];
                       $blockAryAccess = explode(",", $access_branchs);
                       $block_ids = join("','",$blockAryAccess); 
                       
                       $blockAry = array();
                        $duCheck=$d->select("block_master","block_id IN ('$block_ids')");
                        while ($oldBlock=mysqli_fetch_array($duCheck)) {
                            array_push($blockAry , $oldBlock['block_name']);
                        }
                        if (count($blockAry)>0) {
                          echo implode(",", $blockAry);
                        } else {
                          echo "All";
                        }
                        ?>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0">Department Access:</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php  
                       $access_departments=$data['access_departments'];
                       $departmentAry = explode(",", $access_departments);
                       $department_ids = join("','",$departmentAry); 
                       
                       $deparArray = array();
                        $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') AND floors_master.floor_id IN ('$department_ids')");  
                        while ($oldBlock=mysqli_fetch_array($qd)) {
                            array_push($deparArray , $oldBlock['floor_name'].'-'.$oldBlock['block_name']);
                        }
                        if (count($deparArray)>0) {
                          echo implode(",", $deparArray);
                        } else {
                          echo "All";
                        }
                        ?>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-3 col-4">
                      <h6 class="mb-0"><?php echo $xml->string->complaint; ?> <?php echo $xml->string->category; ?> :</h6>
                    </div>
                    <div class="col-sm-9 col-8">
                      <?php  
                         $comCatIdAry = explode(",", $data['complaint_category_id']);
                        $q3=$d->select("complaint_category","active_status=0","");
                        while ($blockRow=mysqli_fetch_array($q3)) {
                          if(in_array($blockRow['complaint_category_id'], $comCatIdAry)){
                            echo  "<span class='badge text-white mr-1 bg-primary'>$blockRow[category_name]</span>";
                          }
                        }
                        ?>
                    </div>
                  </div>
                    
              </div>
            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
    
    </div>
    
  </div><!--End content-wrapper-->
<?php } else { ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-md-4 col-4">
          <h4 class="page-title"><?php echo $xml->string->committee; ?></h4>
        </div>
        <div class="col-md-4 d-none d-lg-block">
           <?php if ($_GET['changeOrder']=='yes') {
            $dragId = "admin-image-list";
           ?>
          <span class="text-warning"><b>Drag to change order </b></span>
          <?php } else { ?>
          <a  href="companyAdmins?changeOrder=yes" class="btn  btn-info btn-sm waves-effect waves-light"><i class="fa fa-swap mr-1"></i> Change Order</a>
          <?php } ?>
          
          
        </div>
        <div class="col-md-4 col-8 text-right">
          <div class="float-sm-right">
          <?php if ($adminData['admin_type']==1 || $adminData['role_id']==2) { ?>
          <a href="companyAdmin" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
          <?php  if (isset($view) && $view=='trash') { ?>
          <a href="companyAdmins" class="btn btn-warning btn-sm"><i class="fa fa-check"></i> View Active</a>
          <?php } else { ?>
          <a href="companyAdmins?view=trash" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> View Deleted</a>
          <?php } ?>
          <?php }?>
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
     
    <div class="row" id="<?php echo $dragId; ?>">
           <?php
           if ($adminData['role_id']==1) {
            $q=$d->select("bms_admin_master,society_master","society_master.society_id=bms_admin_master.society_id ","","ORDER BY order_number ASC");
            } else {
              if (isset($view) && $view=='trash') {
                $apendqueryActive = " AND bms_admin_master.admin_active_status=1";
              } else {
                $apendqueryActive = " AND bms_admin_master.admin_active_status=0";
              }
              $q=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 $apendqueryActive ","ORDER BY bms_admin_master.order_number ASC");
            }
            $i = 1;
          if(mysqli_num_rows($q)>0) {
            while($row=mysqli_fetch_array($q))
            {
              $user_full_name= $row['admin_name'];
              $shortChar=$d->get_sort_name($user_full_name); 
            ?>
          <div class="col-lg-2 col-md-6 col-6 adminBox" data-post-id="<?php echo $row["admin_id"]; ?>">
            
            <div class="card radius-15">
              <div class="card-body text-center">
                <div class="">
                  <a href="companyAdmins?id=<?php echo $row['admin_id'];?>&viewDetails=yes">
                  <?php if(file_exists('../img/society/'.$row['admin_profile']) && $row['admin_profile']!="user_default.png" && $row['admin_profile']!="user.png") { ?> 
                  <img onerror="this.src='img/user.png'"  src="img/user.png"  data-src="../img/society/<?php echo $row['admin_profile']; ?>" width="70" height="70" class="rounded-circle shadow lazyload" alt="">
                  <?php } else { ?>
                  <div style="line-height: 70px; margin: auto !important;font-size: 30px;width: 70px;height: 70px;" class="profile-image-name">
                    <?php echo trim($shortChar); ?>
                  </div>
                  <?php } ?>
                  </a>
                  <h5 class="mb-0 mt-5"><?php echo $row['admin_name'] ?></h5>
                  <?php echo $row['role_name']; if($row['user_type']==1) { ?>
                  <p class="mb-1"><i>Representative</i></p>
                  <?php } else { ?>
                  <p class="mb-1">&nbsp;</p>
                  <?php } ?>
                  <p class="mb-1"><?php echo $row['country_code'] .' '.$row['admin_mobile'] ?></p>
                 
                  <div class="d-grid"> <a href="companyAdmins?id=<?php echo $row['admin_id'];?>&viewDetails=yes" class="btn btn-sm btn-outline-danger radius-15">View Details</a>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        <?php } } else {
          echo "<img src='img/no_data_found.png'>";
        }  ?>
      </div>


    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

<?php } ?>

