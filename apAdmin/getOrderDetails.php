<?php
if(isset($_GET['getOrderDetails']) && !empty($_GET['getOrderDetails']) && $_GET['getOrderDetails'] == 'getOrderDetails' && isset($_GET['retailer_order_id']) && !empty($_GET['retailer_order_id']) && $_GET['retailer_order_id'] != 0 && (int)$_GET['retailer_order_id'] == $_GET['retailer_order_id'])
{
    $retailer_order_id = $_GET['retailer_order_id'];
    $q = $d->selectRow("rdvtm.out_of_range,rdvtm.out_of_range_reason,rpvm.sku,ropm.product_price,ropm.order_packing_type,ropm.no_of_box,ropm.order_per_box_piece,ropm.product_total_price,ropm.product_quantity,rpm.product_name,rom.*,rm.retailer_latitude,rm.retailer_geofence_range,rm.retailer_longitude,rm.retailer_name,rm.retailer_contact_person,rm.retailer_contact_person_number,rm.retailer_contact_person_country_code,rm.retailer_address,distributor_latitude,dm.distributor_longitude,dm.distributor_contact_person,dm.distributor_contact_person_number,dm.distributor_contact_person_country_code,dm.distributor_name,dm.distributor_address,um.user_full_name,um.user_mobile,um.country_code,rpvm.product_variant_name","retailer_order_master AS rom JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id JOIN distributor_master AS dm ON dm.distributor_id = rom.distributor_id JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_order_product_master AS ropm ON ropm.retailer_order_id = rom.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN retailer_product_variant_master AS rpvm ON rpvm.product_variant_id = ropm.product_variant_id JOIN retailer_daily_visit_timeline_master AS rdvtm ON rdvtm.retailer_daily_visit_timeline_id = rom.retailer_visit_id","rom.retailer_order_id = '$retailer_order_id'");
    $all_data = [];
    while($data = $q->fetch_assoc())
    {
        $all_data[] = $data;
    }
    $retailer_order_id = $all_data[0]['retailer_order_id'];
    $order_status = $all_data[0]['order_status'];
}
else
{
    $_SESSION['msg1'] = 'Invalid Request!';
?>
    <script>
        window.location = "manageOrders";
    </script>
<?php
exit;
}
?>
<style type="text/css">
    @media print
    {
        body * {
            visibility: hidden;
        }
        #section-to-print, #section-to-print * {
            visibility: visible;
        }
        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Order Details</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <!-- <button type="button" class="btn btn-primary btn-sm waves-effect waves-light mr-1" id="btn-generate"><i class="fa fa-plus mr-1"></i> PDF</button> -->
                    <a href="javascript:void(0)" onClick="window.print()" class="btn btn-info btn-sm waves-effect waves-light"><i class="fa fa-print fa-lg"></i> Print </a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row" id="section-to-print">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row container">
                            <h5>Order : <?php echo "#".$all_data[0]['retailer_order_id']; ?></h5>
                        </div>
                        <div class="row container">
                            Order Date : <?php echo $all_data[0]['order_date']; ?>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-6">
                                <h6>Placed By : <?php echo $all_data[0]['user_full_name']; ?> </h6>
                                <h6>Order Placed Via : <?php echo $all_data[0]['order_placed_via']; ?></h6>
                                <h6>Status : <?php
                                    if($all_data[0]['order_status'] == 0)
                                    {
                                    ?>
                                        <label class="badge badge-warning">Pending</label>
                                    <?php
                                    }
                                    elseif($all_data[0]['order_status'] == 1)
                                    {
                                    ?>
                                        <label class="badge badge-success">Approved</label>
                                    <?php
                                    }
                                    elseif($all_data[0]['order_status'] == 2)
                                    {
                                    ?>
                                        <label class="badge badge-danger">Rejected</label>
                                    <?php
                                    }
                                    elseif($all_data[0]['order_status'] == 3)
                                    {
                                    ?>
                                        <label class="badge  badge-danger">Cancelled</label>
                                    <?php
                                    }
                                    ?>
                                    <a href="javascript:void(0);" onclick="changeOrderStatus(<?php echo $retailer_order_id; ?>,'<?php echo $order_status; ?>');" class="btn btn-sm btn-primary ml-2">Change Status</a>
                                </h6>
                                <?php
                                if($all_data[0]['order_remark'] != "")
                                {
                                ?>
                                <label class="text-capitalize"><h6>Remarks : <?php echo $all_data[0]['order_remark']; ?></h6></label>
                                <?php
                                }
                                ?>
                                <?php
                                if($all_data[0]['out_of_range'] == 1)
                                {
                                ?>
                                <label class="text-capitalize"><h6>Out Of Range Reason : <?php echo $all_data[0]['out_of_range_reason']; ?></h6></label>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <label> <h6> From : <?php echo $all_data[0]['distributor_name']; ?></h6></label><br>
                                <label><h6><?php echo $all_data[0]['distributor_contact_person'] . " (" . $all_data[0]['distributor_contact_person_country_code'] . " " . $all_data[0]['distributor_contact_person_number']; ?>)</h6></label><br>
                                <label class="text-capitalize"><h6><?php echo $all_data[0]['distributor_address']; ?></h6></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-6">
                                <label><h6>To : <?php echo $all_data[0]['retailer_name']; ?></h6></label><br>
                                <label><h6><?php echo $all_data[0]['retailer_contact_person'] . " (" . $all_data[0]['retailer_contact_person_country_code'] . " " .$all_data[0]['retailer_contact_person_number']; ?>)</h6></label><br>
                                <label class="text-capitalize"><h6><?php echo $all_data[0]['retailer_address']; ?></h6></label>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>SKU</th>
                                        <th>Price</th>
                                        <th>Case</th>
                                        <th>Unit</th>
                                        <th>Total Unit</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($all_data AS $key => $value)
                                    {
                                        extract($value);
                                    ?>
                                    <tr>
                                        <td><?php echo $product_name . " " . $product_variant_name; ?></td>
                                        <td><?php echo $sku; ?></td>
                                        <td><?php echo $product_price; ?></td>
                                        <td><?php echo $no_of_box . " (".$order_per_box_piece.")"; ?></td>
                                        <td>
                                        <?php
                                            if($order_packing_type == 0)
                                            {
                                                echo $product_quantity;
                                            }
                                            else
                                            {
                                                $mul = $no_of_box * $order_per_box_piece;
                                                $pie = $product_quantity - $mul;
                                                echo $pie;
                                            }
                                        ?>
                                        </td>
                                        <td class="pqc"><?php echo $product_quantity;?></td>
                                        <td><?php echo $product_total_price; ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    <tr class="font-weight-bold">
                                       <td colspan="5">Total</td>
                                       <td id="unitSum"></td>
                                       <td><?php echo $all_data[0]['order_total_amount']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <?php
        if(($all_data[0]['order_latitude'] != "" && $all_data[0]['order_longitude'] != "" && !empty($all_data[0]['order_latitude']) && !empty($all_data[0]['order_longitude'])) || ($all_data[0]['retailer_longitude'] != "" && $all_data[0]['retailer_latitude'] != "") || ($all_data[0]['distributor_latitude'] != "" && $all_data[0]['distributor_longitude'] != ""))
        {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row ">
                             <div class="mb-1"><img src="../img/green-dot.png"> Retailer <img src="../img/red-dot.png"> Distributor <img src="../img/person.png"> Employee</div>
                            <div class="map" id="map2" style="width: 100%; height: 600px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <?php
        }
        ?>
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="changeOrderStatusModal">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Change Order Status</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="changeOrderStatusForm" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-3 col-form-label">Status <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <select type="text" required="" class="form-control single-select" name="order_status" id="order_status">
                                <option value="">-- Select --</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="retailer_order_id" id="retailer_order_id">
                        <input type="hidden" name="changeOrderStatus" value="changeOrderStatus">
                        <button type="button" onclick="submitStatusForm();" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key(); ?>&libraries=geometry"></script>
<?php
if(($all_data[0]['order_latitude'] != "" && $all_data[0]['order_longitude'] != "" && !empty($all_data[0]['order_latitude']) && !empty($all_data[0]['order_longitude'])) || ($all_data[0]['retailer_longitude'] != "" && $all_data[0]['retailer_latitude'] != "") || ($all_data[0]['distributor_latitude'] != "" && $all_data[0]['distributor_longitude'] != ""))
{
?>
<script>
function initMap()
{
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map2"), mapOptions);
    map.setTilt(50);

    <?php
    if($all_data[0]['order_latitude'] != "" && $all_data[0]['order_longitude'] != "")
    {
    ?>
    var infoWindow = new google.maps.InfoWindow(), marker;
    // Multiple markers location, latitude, and longitude
    var markers = [
        ["<?php echo $all_data[0]['user_full_name'] ?>","<?php echo $all_data[0]['order_latitude'] ?>","<?php echo $all_data[0]['order_longitude'] ?>"]
    ];

    // Info window content
    var infoWindowContent = [
        ["<div class='info_content'><h6><?php echo $all_data[0]['user_full_name']; ?></h6><p><?php echo $all_data[0]['country_code'] . " " . $all_data[0]['user_mobile']; ?></p></div>"]
    ];
    var position = new google.maps.LatLng(markers[0][1], markers[0][2]);
    bounds.extend(position);
    marker = new google.maps.Marker({
        position: position,
        map: map,
        icon: "../img/person.png",
        title: markers[0][0]
    });

    // Add info window to marker
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infoWindow.setContent(infoWindowContent[0][0]);
            infoWindow.open(map, marker);
        }
    })(marker));

    // Center the map to fit all markers on the screen
    map.fitBounds(bounds);
    <?php
    }
    ?>

    var infoWindowContentd = [
        ["<div class='info_content'><h6><?php echo $all_data[0]['distributor_name']; ?> - (Distributor)</h6><p><?php echo $all_data[0]['distributor_contact_person']; ?></p><p><?php echo $all_data[0]['distributor_contact_person_country_code'] . " " . $all_data[0]['distributor_contact_person_number']; ?></p></div>"]
    ];

    var infoWindowContentr = [
        ["<div class='info_content'><h6><?php echo $all_data[0]['retailer_name']; ?> - (Retailer)</h6><p><?php echo $all_data[0]['retailer_contact_person']; ?></p><p><?php echo $all_data[0]['retailer_contact_person_country_code'] . " " . $all_data[0]['retailer_contact_person_number']; ?></p></div>"]
    ];

    // Add multiple markers to map
    var infoWindowd = new google.maps.InfoWindow(), markerd;
    var infoWindowr = new google.maps.InfoWindow(), markerr;

    <?php
    if($all_data[0]['retailer_latitude'] != "" && $all_data[0]['retailer_longitude'] != "" && $all_data[0]['distributor_latitude'] != "" && $all_data[0]['distributor_longitude'] != "")
    {
    ?>
    // var bounds = new google.maps.LatLngBounds();
    const flightPlanCoordinates = [
        { lat: <?php echo $all_data[0]['distributor_latitude'] ?>, lng: <?php echo $all_data[0]['distributor_longitude'] ?> },
        { lat: <?php echo $all_data[0]['retailer_latitude'] ?>, lng: <?php echo $all_data[0]['retailer_longitude'] ?> },
    ];
    var iconsetngs = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    };
    const flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2,
        icons: [{
            icon: iconsetngs,
            offset: '35%'
        },
        {
            icon: iconsetngs,
            offset: '60%'
        },
        {
            icon: iconsetngs,
            offset: '90%'
        }
        ]
    });
    flightPath.setMap(map);

    markerd = new google.maps.Marker(
    {
        position: new google.maps.LatLng(<?php echo $all_data[0]['distributor_latitude'] ?>, <?php echo $all_data[0]['distributor_longitude'] ?>),
        map: map,
        icon: "../img/red-dot.png"
    });

    bounds.extend(markerd.position);

    google.maps.event.addListener(markerd, 'click', (function(markerd)
    {
        return function()
        {
            infoWindowd.setContent(infoWindowContentd[0][0]);
            infoWindowd.open(map, markerd);
        }
    })(markerd));
   

    markerr = new google.maps.Marker(
    {
        position: new google.maps.LatLng(<?php echo $all_data[0]['retailer_latitude'] ?>, <?php echo $all_data[0]['retailer_longitude'] ?>),
        map: map,
        icon: "../img/green-dot.png"
    });

    ////retailer circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(<?php echo $all_data[0]['retailer_geofence_range'] ?>), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', markerr, 'position');
      var citymap = {
        newyork: {
          center: {
            lat: <?php echo $all_data[0]['retailer_latitude'] ?>,
            lng: <?php echo $all_data[0]['retailer_longitude'] ?>
          }
        }
      };

    bounds.extend(markerr.position);

    google.maps.event.addListener(markerr, 'click', (function(markerr)
    {
        return function()
        {
            infoWindowd.setContent(infoWindowContentr[0][0]);
            infoWindowd.open(map, markerr);
        }
    })(markerr));
    map.fitBounds(bounds);

    <?php
    }
    elseif($all_data[0]['retailer_latitude'] != "" && $all_data[0]['retailer_longitude'] != "" && $all_data[0]['distributor_latitude'] == "" && $all_data[0]['distributor_longitude'] == "")
    {
    ?>
    // var bounds = new google.maps.LatLngBounds();
    markerr = new google.maps.Marker(
    {
        position: new google.maps.LatLng(<?php echo $all_data[0]['retailer_latitude'] ?>, <?php echo $all_data[0]['retailer_longitude'] ?>),
        map: map,
        icon: "../img/green-dot.png"
    });

    ////retailer circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(<?php echo $all_data[0]['retailer_geofence_range'] ?>), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', markerr, 'position');
      var citymap = {
        newyork: {
          center: {
            lat: <?php echo $all_data[0]['retailer_latitude'] ?>,
            lng: <?php echo $all_data[0]['retailer_longitude'] ?>
          }
        }
      };

    google.maps.event.addListener(markerr, 'click', (function(markerr)
    {
        return function()
        {
            infoWindowd.setContent(infoWindowContentr[0][0]);
            infoWindowd.open(map, markerr);
        }
    })(markerr));

    var position = new google.maps.LatLng(<?php echo $all_data[0]['retailer_latitude'] ?>, <?php echo $all_data[0]['retailer_longitude'] ?>);
    bounds.extend(position);
    map.fitBounds(bounds);
    <?php
    }
    elseif($all_data[0]['retailer_latitude'] == "" && $all_data[0]['retailer_longitude'] == "" && $all_data[0]['distributor_latitude'] != "" && $all_data[0]['distributor_longitude'] != "")
    {
    ?>
    var bounds = new google.maps.LatLngBounds();
    markerd = new google.maps.Marker(
    {
        position: new google.maps.LatLng(<?php echo $all_data[0]['distributor_latitude'] ?>, <?php echo $all_data[0]['distributor_longitude'] ?>),
        map: map,
        icon: "../img/red-dot.png"
    });

    google.maps.event.addListener(markerd, 'click', (function(markerd)
    {
        return function()
        {
            infoWindowd.setContent(infoWindowContentd[0][0]);
            infoWindowd.open(map, markerd);
        }
    })(markerd));

    var position = new google.maps.LatLng(<?php echo $all_data[0]['distributor_latitude'] ?>, <?php echo $all_data[0]['distributor_longitude'] ?>);
    bounds.extend(position);
    map.fitBounds(bounds);
    <?php
    }
    ?>

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event)
    {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
}
google.maps.event.addDomListener(window, 'load', initMap);
</script>
<?php
}
?>
<script>
function changeOrderStatus(retailer_order_id,order_status)
{
    $('#retailer_order_id').val(retailer_order_id);
    $('#order_status').empty();
    $('#order_status').append($("<option></option>").attr("value", '').text('--Select--'));
    if(order_status == 0)
    {
        $('#order_status').append($("<option></option>").attr("value", 1).text('Approved'));
        $('#order_status').append($("<option></option>").attr("value", 2).text('Rejected'));
    }
    else if(order_status == 1)
    {
        $('#order_status').append($("<option></option>").attr("value", 3).text('Cancelled'));

    }
    else if(order_status == 2)
    {
        $('#order_status').append($("<option></option>").attr("value", 3).text('Cancelled'));
    }
    $('#changeOrderStatusModal').modal();
}

function submitStatusForm()
{
    if($("#changeOrderStatusForm").valid())
    {
        $.ajax({
            type: 'post',
            url: 'controller/orderProductController.php',
            data: $('#changeOrderStatusForm').serialize(),
            success: function (response)
            {
                window.location.reload();
            }
        });
    }
    else
    {
        $("#changeOrderStatusForm").validate();
    }
}

$(document).ready(function()
{
    var sum = 0;
    $(".pqc").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#unitSum').text(sum);
});
/*var retailer_order_id = <?php echo $retailer_order_id; ?>;
$("#sendMailDistributor").click(function()
{
    $.ajax({
    url: 'controller/orderProductController.php',
    data: {sendMailDis:"sendMailDis",retailer_order_id:retailer_order_id,csrf:csrf},
    type: "post",
    success: function(response)
    {
      console.log(response);
    },
    error: function(data)
    {
    }
  });
});*/
</script>