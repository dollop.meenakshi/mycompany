<?php
error_reporting(0);
/* $currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year')); */

$uId = (int) $_REQUEST['uId'];
$dId = (int) $_REQUEST['dId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-3">
          <h4 class="page-title">Salary</h4>
     </div>
     <div class="col-sm-3 col-9">
       <div class="btn-group float-sm-right">
        <a href="addSalary"   onclick="buttonSetting()" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deletesalary');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
      </div>
     </div>
     </div>
     <form action="" >
        <div class="row pt-2 pb-2">

          <div class="col-lg-6 col-6">
          <select name="dId" class="form-control single-select" onchange="this.form.submit()">
            <option value="">All Department</option>
              <?php
                $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");
                while ($depaData = mysqli_fetch_array($qd)) {
                    ?>
             <option  <?php if ($dId == $depaData['floor_id']) {echo 'selected';}?> value="<?php echo $depaData['floor_id']; ?>" ><?php echo $depaData['floor_name']; ?></option>
             <?php }?>

            </select>
        </div>
        <div class="col-lg-6 col-6">
            <select name="uId" class="form-control single-select" onchange="this.form.submit()">
              <option value="">All Employer </option>
                <?php
                  $user = $d->select("users_master", "society_id='$society_id'");
                  while ($userdata = mysqli_fetch_array($user)) {
                      ?>
                <option <?php if ($uId == $userdata['user_id']) {echo 'selected';}?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?></option>
                <?php }?>

              </select>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Department</th>
                        <th>Employer</th>
                        <th>Basic Salary</th>
                        <th>Professional Tex</th>
                        <th>Date</th>
                        <th>View</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                   <?php
                      $i = 1;
                      if (isset($dId) && $dId > 0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }
                      if (isset($uId) && $uId > 0) {
                          $UserFilterQuery = "AND salary.user_id='$user_id'";
                      }
                      $q = $d->select("salary,users_master,floors_master", "salary.user_id=users_master.user_id AND salary.floor_id=floors_master.floor_id AND salary.society_id='$society_id'  AND salary.is_delete='0' $deptFilterQuery  $UserFilterQuery", "ORDER BY salary.salary_id ASC");
                      $counter = 1;

                      while ($data = mysqli_fetch_array($q)) {

                          ?>
                    <tr>
                       <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['salary_id']; ?>">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['salary_id']; ?>">
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['floor_name']; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['basic_salary']; ?></td>
                       <td><?php echo $data['professional_tex']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['created_at'])); ?></td>
                       <td> <button type="button" class="btn btn-primary" onclick="salarySlipGeneration(<?php  echo $data['user_id']; ?>);">View</button></td>
                       <td>
                       <div class="d-flex align-items-center">
                       <?php
                            $increment_date = strtotime($data['increment_date']);
                                $crntDate = strtotime(date('Y-m-d'));

                                if ($crntDate > $increment_date) {?>
                           <a href="addSalary"><button type="submit" class="btn btn-sm btn-primary mr-2">Add Salary</button>
                                              <?php } else {
                        ?>
                         <a href="addSalary?sid=<?php echo $data['salary_id']; ?>">
                          <button type="submit" class="btn btn-sm btn-primary mr-2"><i class="fa fa-pencil"></i></button>
                          </a>
                          <?php }?>
                            <?php if ($data['is_active'] == "0") {
                                $status = "salaryStatusDeactive";
                                $active = "checked";
                            } else {
                                $status = "salaryStatusActive";
                                $active = "";
                            }?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_id']; ?>','<?php echo $status; ?>');" data-size="small"/>

                        </div>
                        </td>

                    </tr>
                    <?php }?>
                </tbody>

            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    </div>

    <div class="modal fade" id="salary_slip_model">
      <div class="modal-dialog ">
         <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Salary Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
      <div class="modal-body" id="salarySlipData" style="align-content: center;">

      </div>

    </div>
  </div>
</div>


<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
//////////////////////holiday data
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }





</script>
