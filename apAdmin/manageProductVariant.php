<?php
if((isset($_GET['PId']) && !empty($_GET['PId']) && $_GET['PId'] != 0) || (isset($_GET['PCId']) && !empty($_GET['PCId']) && $_GET['PCId'] != 0))
{
    $product_id = $_GET['PId'];
    $product_category_id = $_GET['PCId'];
}
elseif((isset($_COOKIE['PId']) && $_COOKIE['PId'] != "" && $_COOKIE['PId'] != 0) || (isset($_COOKIE['PCId']) && $_COOKIE['PCId'] != "" && $_COOKIE['PCId'] != 0))
{
    $product_id = $_COOKIE['PId'];
    $product_category_id = $_COOKIE['PCId'];
}

if(isset($product_category_id) && !empty($product_category_id) && !ctype_digit($product_category_id))
{
    $_SESSION['msg1'] = "Invalid Category Id!";
    ?>
    <script>
        window.location = "manageProductVariant";
    </script>
<?php
exit;
}

if(isset($product_id) && !empty($product_id) && !ctype_digit($product_id))
{
    $_SESSION['msg1'] = "Invalid Product Id!";
    ?>
    <script>
        window.location = "manageProductVariant";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Product Variant</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addProductVariant" method="post" id="addProductVariantBtnForm">
                        <input type="hidden" name="addProductVariantBtn" value="addProductVariantBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteProductVariant');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormProduct">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Category </label>
                    <select class="form-control single-select" id="PCId" name="PCId" required onchange="getProductList(this.value);">
                        <option value="">-- Select --</option>
                        <?php
                            $qp = $d->selectRow("product_category_id,category_name","product_category_master","product_category_status = 0 AND product_category_delete = 0");
                            while ($qd = $qp->fetch_assoc())
                            {
                        ?>
                        <option <?php if($product_category_id == $qd['product_category_id']) { echo 'selected'; } ?> value="<?php echo $qd['product_category_id'];?>"><?php echo $qd['category_name'];?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Product </label>
                    <select class="form-control single-select" id="PId" name="PId">
                        <option value="">-- Select --</option>
                        <?php
                        if(isset($product_category_id) && !empty($product_category_id))
                        {
                            $qp = $d->selectRow("rpm.product_id,rpm.product_name","retailer_product_master AS rpm","rpm.product_status = 0 AND rpm.product_delete = 0 AND rpm.product_category_id = '$product_category_id'");
                            while ($qd = $qp->fetch_assoc())
                            {
                        ?>
                        <option <?php if($product_id == $qd['product_id']) { echo 'selected'; } ?> value="<?php echo $qd['product_id'];?>"><?php echo $qd['product_name'];?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                        <th>Product Name</th>
                                        <th>Variant Name</th>
                                        <th>SKU</th>
                                        <th>Bulk Type</th>
                                        <th>Per Box Piece</th>
                                        <th>Retailer Selling Price</th>
                                        <th>MRP</th>
                                        <th>Menufacturing Cost</th>
                                        <th>Unit</th>
                                        <th>Photo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if((isset($product_id) && !empty($product_id) && $product_id != 0) || (isset($product_category_id) && !empty($product_category_id) && $product_category_id != 0))
                                    {
                                        $append = "";
                                        if($product_id > 0)
                                        {
                                            $append .= " AND rpvm.product_id = '$product_id'";
                                        }

                                        if($product_category_id > 0)
                                        {
                                            $append .= " AND rpm.product_category_id = '$product_category_id'";
                                        }
                                    }
                                    $q = $d->selectRow("rpvm.*,umm.unit_measurement_name,rpm.product_category_id,rpm.product_name,ropm.order_product_id","retailer_product_variant_master AS rpvm JOIN unit_measurement_master AS umm ON umm.unit_measurement_id = rpvm.unit_measurement_id JOIN retailer_product_master AS rpm ON rpm.product_id = rpvm.product_id LEFT JOIN retailer_order_product_master AS ropm ON ropm.product_variant_id = rpvm.product_variant_id","1=1".$append,"GROUP BY rpvm.product_variant_id ORDER BY product_variant_id DESC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php
                                            if($order_product_id == "" && empty($order_product_id))
                                            {
                                            ?>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['product_variant_id']; ?>">
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input type="hidden">
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form action="addProductVariant" method="post">
                                                    <input type="hidden" name="product_variant_id" value="<?php echo $row['product_variant_id']; ?>">
                                                    <input type="hidden" name="editProductVariant" value="editProductVariant">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if($order_product_id == "" && empty($order_product_id))
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form action="controller/orderProductController.php" method="POST">
                                                    <input type="hidden" name="product_variant_id" value="<?php echo $row['product_variant_id']; ?>">
                                                    <input type="hidden" name="product_id" value="<?php echo $row['product_id']; ?>">
                                                    <input type="hidden" name="variant_photo" value="<?php echo $variant_photo; ?>">
                                                    <input type="hidden" name="product_category_id" value="<?php echo $product_category_id; ?>">
                                                    <input type="hidden" name="deleteProductVariant" value="deleteProductVariant">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                        if($variant_active_status == 0)
                                        {
                                        ?>
                                            <button title="Deactivate Variant" type="button" class="btn btn-success btn-sm mmw-80 bg-success" onclick="changeStatus('<?php echo $product_variant_id; ?>','variantDeactive')">Active</button>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button title="Activate Variant" type="button" class="btn btn-danger btn-sm mmw-80" onclick="changeStatus('<?php echo $product_variant_id; ?>','variantActive')"/>Deactive</button>
                                        <?php
                                        }
                                        ?>
                                        </td>
                                        <td><?php echo $product_name; ?></td>
                                        <td><?php echo $product_variant_name; ?></td>
                                        <td><?php echo $sku; ?></td>
                                        <td><?php echo ($variant_bulk_type == 0) ? "Piece wise" : "Box wise "; ?></td>
                                        <td><?php echo ($variant_per_box_piece != 0) ? $variant_per_box_piece : ""; ?></td>
                                        <td><?php echo $retailer_selling_price; ?></td>
                                        <td><?php echo $maximum_retail_price; ?></td>
                                        <td><?php echo $menufacturing_cost; ?></td>
                                        <td><?php echo $unit_measurement_name; ?></td>
                                        <td>
                                            <?php
                                            if($variant_photo != "" && file_exists("../img/vendor_product/".$variant_photo))
                                            {
                                            ?>
                                            <a href="../img/vendor_product/<?php echo $variant_photo; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $product_variant_name; ?>">
                                                <img src="../img/vendor_product/<?php echo $variant_photo; ?>" width="100" height="100"  alt="Product Variant Image"/>
                                            </a>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            // }
                            // else
                            // {
                            ?>
                            <!-- <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Product</span>
                            </div> -->
                            <?php
                            // }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>

function getProductList(product_category_id)
{
    $.ajax({
        url: 'controller/orderProductController.php',
        data: {getProductList:"getProductList",product_category_id:product_category_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#PId').empty();
            response = JSON.parse(response);
            $('#PId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#PId').append("<option value='"+value.product_id+"'>"+value.product_name+"</option>");
            });
        }
    });
}


function submitAddBtnForm()
{
    var product_id = $('#PId').val();
    if(product_id != "" && product_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'product_id',
            value: product_id
        }).appendTo('#addProductVariantBtnForm');
    }
    $("#addProductVariantBtnForm").submit()
}
</script>