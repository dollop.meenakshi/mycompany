
<?php
    //include connection file 
    include_once("lib/dao.php");
    
    $db=new DbConnect();
    $conn=$db->connect();
    $society_id = $_COOKIE['society_id'];
    
    // count time 
    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    extract($_REQUEST);

     
    // initilize all variable
    $params = $columns = $totalRecords = $data = array();

    $params = $_REQUEST;
    //define index of column
    $columns = array( 
        0 =>  'block_sort',
        1 =>  'block_name',
        2 =>  'user_full_name',
        3 =>  'user_mobile',
        4 =>  'user_email',
        5 =>  'unit_name',
        6 =>  'blood_group',
        7 =>  'device',
        8 => 'app_version_code',
        9 => 'phone_model',
        10 => 'last_login',
        
    );

    $where = $sqlTot = $sqlRec = "";

    // codition
    $blockFilterQuery = "";
    $deptFilterQuery = "";
    $deviceFilterQuery = "";

    if (isset($bId) && $bId > 0) {
      $blockFilterQuery = " AND users_master.block_id='$bId' ";
    }
    if (isset($dId) && $dId > 0) {
      $deptFilterQuery = " AND users_master.floor_id='$dId' ";
    }
    if (isset($device) && $device != '') {
      $deviceFilterQuery = " AND users_master.device='$device'";
    }
    
    $where .= " WHERE users_master.delete_status=0 AND users_master.society_id='$society_id' AND users_master.user_status=1 $blockFilterQuery $deptFilterQuery  $deviceFilterQuery ";//$blockAppendQueryUser $blockAppendQuery
    // check search value exist
    if( !empty($params['search']['value']) ) {   
        //$where .=" WHERE ";
        $where .=" AND ( CONCAT(floors_master.floor_name,' ',block_master.block_name) LIKE '%".$params['search']['value']."%' 
                         OR users_master.user_full_name LIKE '%".$params['search']['value']."%' 
                         OR CONCAT(users_master.country_code,' ',users_master.user_mobile) LIKE '%".$params['search']['value']."%' 
                         OR users_master.user_email LIKE '%".$params['search']['value']."%' 
                         OR unit_master.unit_name LIKE '%".$params['search']['value']."%' 
                         OR users_master.blood_group LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN users_master.user_status=1 THEN users_master.device ELSE '' END) LIKE '%".$params['search']['value']."%' 
                         OR users_master.app_version_code LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN users_master.phone_brand!='' AND users_master.phone_model!='' THEN CONCAT(users_master.phone_model,'(',users_master.phone_brand,')') ELSE '' END ) LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN users_master.last_login!='0000-00-00 00:00:00' THEN DATE_FORMAT(users_master.last_login,'%Y-%m-%d %h:%i %p') ELSE 'Not Login' END) LIKE '%".$params['search']['value']."%' 
                         
                     )";

    }

    

    // getting total number records without any search
    $sql = "SELECT block_master.block_sort,
                CONCAT(floors_master.floor_name,' ',block_master.block_name) AS block_name,
                users_master.user_full_name,
                CONCAT(users_master.country_code,' ',users_master.user_mobile) AS user_mobile,
                users_master.user_email,unit_master.unit_name,users_master.blood_group,
                (CASE WHEN users_master.user_status=1 THEN users_master.device ELSE '' END) AS device,
                users_master.app_version_code,
                (CASE WHEN users_master.phone_brand!='' AND users_master.phone_model!='' THEN CONCAT(users_master.phone_model,'(',users_master.phone_brand,')') ELSE '' END ) AS phone_model,
                (CASE WHEN users_master.last_login!='0000-00-00 00:00:00' THEN DATE_FORMAT(users_master.last_login,'%Y-%m-%d %h:%i %p') ELSE 'Not Login' END) AS last_login,users_master.last_login AS new_last_login
            FROM users_master
            INNER JOIN block_master ON block_master.block_id = users_master.block_id
            INNER JOIN floors_master ON floors_master.block_id = block_master.block_id
            INNER JOIN unit_master ON unit_master.unit_id = users_master.unit_id
             ";

    
    $sqlTot .= $sql;
    $sqlRec .= $sql;
    //concatenate search sql if value exist
    if(isset($where) && $where != '') {

        $sqlTot .= $where;
        $sqlRec .= $where;
    }


    $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir'];//."  LIMIT ".$params['start']." ,".$params['length']." ";
    //print_r($sqlRec);

    mysqli_set_charset($conn,"utf8mb4");
    
    $queryTot = mysqli_query($conn, $sqlTot) or die("database error:". mysqli_error($conn));


    $totalRecords = mysqli_num_rows($queryTot);
    // print_r($sqlRec);die();
    $queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employee report data");

    //iterate on results row and create new index array of data
    $i = 1;
    while( $row = mysqli_fetch_array($queryRecords) ) { 
        
        $data_temp = array();

        // 0-position
        
        // $data_temp[] = $i++;
        $data_temp[] = $i++;

        // 1-position
        
        $data_temp[] = $row['block_name'];

        // 2-position
        
        $data_temp[] = $row['user_full_name'];

        // 3-position
        
        $data_temp[] = $row['user_mobile'];

        // 4-position
        $data_temp[] = $row['user_email'];

        // 5-position
        $data_temp[] = $row['unit_name'];

        // 6-position
        
        $data_temp[] = $row['blood_group'];

        // 7-position
        
        $data_temp[] = $row['device'];

        // 8-position
        $data_temp[] = $row['app_version_code']; 

        // 9-position
        $data_temp[] = $row['phone_model']; 

        // 10-position
        
        $data_temp[] = $row['last_login'].' ('.time_elapsed_string($row['new_last_login']).')';
                          
        array_push($data, $data_temp);
    
    }   
    
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );

    echo json_encode($json_data);  // send data as json format
?>
    