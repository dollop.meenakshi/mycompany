<?php error_reporting(0);
$tId = $_REQUEST['tId'];
$eId = $_REQUEST['eId'];
$date = $_REQUEST['date'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Workfolio Employess</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

        <form class="workfolioFilterForm" action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <select name="tId" class="form-control single-select" required="" onchange="getWorkfolioEmployees(this.value)">
                        <option value="">-- Select Team --</option> 
                        <?php 
                            $qt=$d->select("workfolio_team","");  
                            while ($teamData=mysqli_fetch_array($qt)) {
                        ?>
                        <option  <?php if($tId==$teamData['workfolio_team_id']) { echo 'selected';} ?> value="<?php echo $teamData['workfolio_team_id'];?>" ><?php echo $teamData['teamName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="col-md-3 form-group">
                    <select name="eId" id="eId" class="form-control single-select" required="">
                        <option value="">-- Select Team Employee --</option> 
                        <?php 
                            $qte=$d->select("workfolio_employees,workfolio_team","workfolio_employees.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$tId'");  
                            while ($teamEmpData=mysqli_fetch_array($qte)) {
                        ?>
                        <option  <?php if($eId==$teamEmpData['workfolio_employee_id']) { echo 'selected';} ?> value="<?php echo $teamEmpData['workfolio_employee_id'];?>" ><?php echo $teamEmpData['displayName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="col-md-3 form-group">
					<input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" readonly name="date" value="<?php if ($date!='') { echo $date;} else{echo date('Y-m-d');} ?>">
                </div>
                
                <div class="col-md-3 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
		<div class="row">
			<?php
				if (isset($tId) && $tId>0 && isset($eId) && $eId>0) {

			  	$q = $d->select("workfolio_screenshot, workfolio_employees", "workfolio_screenshot.email=workfolio_employees.email AND workfolio_employees.workfolio_employee_id='$eId' AND DATE_FORMAT(workfolio_screenshot.date,'%Y-%m-%d') = '$date'", "ORDER BY workfolio_screenshot.date ASC");
			 	
				if(mysqli_num_rows($q)>0){
					$counter = 1;
			  	while ($data = mysqli_fetch_array($q)) {
			?>
				<div class="col-lg-3 col-md-4">
					<div class="card">
						<div class="card-body">
							<div class="d-flex">
								<img class="rounded" width="8%" height="8%" src="<?php echo $data['app_icon']; ?>">
								<h6 class="ml-2"><?php custom_echo($data['app_title'],20	); ?></h6>
							</div>
							<a href="<?php echo $data['image_url']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data['app_title']; ?>"><img class="card-img-top" src="<?php echo $data['thumbnail_url']; ?>" ></a>
							<div class="pt-2 row" style="font-size: 12px;">
								<div class="col pl-3">
									<span><?php echo $data['displayName']; ?></span>
								</div>
								<div class="col pr-3">
									<span class=" float-right"><i class="fa fa-clock-o"></i> <?php echo date("h:i A", strtotime($data['date'])); ?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } }else{ ?>
				<div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> No Data Found!</span>
					</div>
				</div>
			 <?php } } else {  ?>
				<div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> Please Select Team Employee</span>
					</div>
				</div>
			<?php } ?>
		</div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
