<?php //error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Courses</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <div class="btn-group float-sm-right">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSettingForLms()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
        <a href="javascript:void(0)" onclick="DeleteAll('deleteCourse');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg "></i> Delete </a>
      </div>
     </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Course Name</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                    $i = 1;
                    
                    $q = $d->select("course_master", "society_id='$society_id' AND course_is_deleted=0 ");
                    $counter = 1;
                    while ($data = mysqli_fetch_array($q)) {

                        ?>
                      <tr>
                      <td class="text-center">
                          <?php $totalDocuemnt = $d->count_data_direct("course_chapter_id", "course_chapter_master", "course_id='$data[course_id]'");
                        if ($totalDocuemnt == 0) {
                            ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['course_id']; ?>">
                          <?php }?>
                        </td>
                       <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['course_title']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['course_created_date'])); ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                          <form  method="post" accept-charset="utf-8">
                              <input type="hidden" name="course_id" value="<?php echo $data['course_id']; ?>">
                              <button type="button" onclick="courseDataSet(<?php echo $data['course_id']; ?>)" data-toggle="modal" data-target="#addModal" class="btn btn-sm btn-primary mr-1"><i class="fa fa-pencil"></i></button>
                            </form>
                          <?php if ($data['course_is_active'] == "0") {
                                    $status = "courseStatusDeactive";
                                    $active = "checked";
                                } else {
                                    $status = "courseStatusActive";
                                    $active = "";
                                }?>
                          <input  type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['course_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                          <!-- <input type="button" onclick="courseDetailFunction(<?php echo $data['course_id']; ?>)" style="margin:3px" class="btn btn-sm btn-secondary mr-1 pd-1" data-size="small" Value="View"> -->
                          <a href="courseChapter?cId=<?php echo $data['course_id']; ?>"   style="margin:3px" class="btn btn-sm btn-secondary mr-1 pd-1" data-size="small" Value="View">
                          Chapters
                          </a>
                        </div>
                      </td>
                    </tr>
                    <?php }?>
                </tbody>
             </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- //////////////Modal//////////////////// -->
<div class="modal fade" id="mainModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Course Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewModal" style="align-content: center;">

      </div>

    </div>
  </div>
</div>

<!-- FOr add edit hr doc -->
<div class="modal fade" id="addModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Course</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">
           <form id="addCourseForm" action="controller/CourseController.php" enctype="multipart/form-data" method="post">
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Course Title <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
                   <input type="text" class="form-control restIn" id="course_title" placeholder="Course Title" name="course_title" value="<?php if ($data['course_title'] != "") {echo $data['course_title'];}?>">
               </div>
           </div>
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Course Description <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
                   <textarea class="form-control restIn" id="course_description" placeholder="Course Description" name="course_description" value="<?php if ($data['course_description'] != "") {echo $data['course_description'];}?>"><?php if ($data['course_description'] != "") {echo $data['course_description'];}?></textarea>
               </div>
           </div>
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Course Image </label>
               <div class="col-lg-8 col-md-8" id="">
                   <input type="file" class="restIn course_image" name="course_image" id="course_image" onchange="readURL(this);">
                   <input type="hidden" name="course_image_old" id="course_image_old" >
                   <img  id="blah" >
               </div>
           </div>
           <div class="form-footer text-center">
             <input type="hidden" name="course_id" id="course_id" value="<?php if ($data['course_id'] != "") {echo $data['course_id'];}?>" >
             <button id="addCourse" type="submit"  class="btn btn-success addCourseBtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                <input type="hidden" name="addCourse"  value="addCourse">
              <button id="" type="submit"  class="btn btn-success addCourseBtn  hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger  cancel" onclick="resethRFrm();"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<script type="text/javascript">


/* $("document").ready(function() {

$(".course_image").change(function() {
     fileName = this.value;
    fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
    extensionResume = ["jpeg", "jpg", "png", "JPG", "JPEG", "PNG"];
    
    if((jQuery.inArray( fileExtension, extensionResume))== -1 ){
      $("#course_image").val("");
      $('#course_image').rules("add", {
            required: true,
            messages: {
                    required: "Invalid File type you Can Select  jpeg ,  jpg ,  png ,  JPG ,  JPEG ,  PNG ",
                }
        });
    }
  
});
}); */

  function resethRFrm() {
    $('.restIn').val('');
    $('#blah').attr('src', 'img/no_image.png');
    $('#blah').attr('widht', "75px");
    $('#blah').attr('height', "75px");
  }
//////////////////////////preview image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
            $('#blah').attr('widht', "75px");
            $('#blah').attr('height', "75px");
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function buttonSettingForLms(){
  $('.hideupdate').hide();
  $('.hideAdd').show();
  $('.restIn').val('');
  $('#blah').attr('src', 'img/no_image.png');
  $('#blah').attr('widht', "75px");
  $('#blah').attr('height', "75px");
}
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
<style>
.hideupdate{
  display:none;
}

</style>