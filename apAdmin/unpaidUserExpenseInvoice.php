<script src="assets/js/jquery.min.js"></script>

<?php include 'lib/dao.php'; ?>


<?php

error_reporting(0);
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
$con = $d->dbCon();
error_reporting(0);
$dId = $_REQUEST['dId'];
$ueYear = $_REQUEST['ueYear'];
$ueMonth = $_REQUEST['ueMonth'];
$bId = $_REQUEST['bId'];
$uId = $_REQUEST['uId'];
//print_r();
if (isset($dId) && $dId > 0) {
  $deptFilterQuery = " AND user_expenses.floor_id='$dId'";
}
if (isset($uId) && $uId > 0) {
  $userFilterQuery = " AND user_expenses.user_id='$uId'";
  $q3 = $d->selectRow("users_master.*,floors_master.*,block_master.*", "users_master,floors_master,block_master", "users_master.user_id=$uId AND block_master.block_id = users_master.block_id AND floors_master.floor_id = users_master.floor_id");
  $userData =  mysqli_fetch_assoc($q3);
}
if (isset($ueYear) && $ueYear > 0) {
  $yearFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y') = '$ueYear'";
}
if (isset($ueMonth) && $ueMonth > 0) {
  $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
} else {
  $ueMonth = date('m');
  $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
}
$month_name = $ueMonth . "-" . $ueYear;

$q = $d->selectRow("user_expenses.*,floors_master.*,users_master.*,paid_by_user.user_full_name AS paid_user_name,paid_by_admin.admin_name AS paid_by_admin_name ", "user_expenses LEFT JOIN users_master AS paid_by_user ON paid_by_user.user_id=user_expenses.expense_paid_by_id LEFT JOIN bms_admin_master AS paid_by_admin ON paid_by_admin.admin_id=user_expenses.expense_paid_by_id,floors_master,users_master ", "users_master.user_id=user_expenses.user_id AND user_expenses.floor_id=floors_master.floor_id  AND user_expenses.expense_paid_status=1 AND users_master.delete_status=0 $userFilterQuery $deptFilterQuery $yearFilterQuery $monthFilterQuery ", "ORDER BY user_expenses.user_expense_id ASC");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Paid Employee Expense <?php echo $month_name; ?>-<?php echo $salary_slip_data['user_full_name']; ?></title>
  <link rel="icon" href="../img/fav.png" type="image/png">
  <meta name="author" content="harnishdesign.net">
  <meta name="viewport" content="width=1024">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/app-style9.css" rel="stylesheet" />
  <?php include 'common/colours.php'; ?>
</head>

<body id="printableArea" class="bg-white">

  <div class="container invoice-container">
    <div class="border border-primary">

      <div class="row">
        <div class="col-md-12 mt-4 text-center">
          <h2>Unpaid Expense</h2>
        </div>
        <?php if (isset($uId) && $uId > 0) { ?>
        <div class="col-md-6 pl-4 ">
          <lable><b>Name:</b></lable>
          <?php echo $userData['user_full_name'] ?> <?php if (isset($userData['user_designation']) && $userData['user_designation'] != "") { echo "(" . $userData['user_designation'] . ")";} ?><br>
          <lable><b>Department:</b></lable>
          <?php echo $userData['user_full_name'] ?> <?php echo $userData['floor_name'] . "(" . $userData['block_name'] . ")"; ?><br>
          <lable><b>Mobile No:</b></lable>
          <?php echo $userData['user_mobile']; ?><br>
          <lable><b>Generated Date:</b></lable>
          <?php echo date('d-M-Y H:i A') ?><br>
          <lable><b>Month Of:</b></lable>
          <?php echo date("F-Y", strtotime('01-' . $month_name)); ?>
        </div>
        <?php } ?>
        <div class="col-md-12 p-4">
        
            
              <table class="w-100 table-bordered">
                
                <thead class="border-0 head_row bg-primary text-white">
                  <tr style="background: #2f648e !important;">
                      <th class="border-0">Sr. No.</th>
                      <?php if (!isset($uId) || $uId == 0) { ?>
                        <th class="border-0">Employee</th>
                      <?php } ?>
                      <th class="border-0">Expense For</th>
                      <th class="border-0">Date</th>
                    
                      <th class="border-0">Amount</th>
                  </tr>
                  
                </thead>
                <tbody class="border-top-0 ">
                  <?php $i = 1;
                  $total_amount = 0;
                  while ($data = mysqli_fetch_array($q)) {
                    //print_r($data);
                    $total_amount += $data['amount'];
                  ?>
                    <tr>
                      <td class=""><?php echo $i++; ?></td>
                      <?php if (!isset($uId) || $uId == 0) { ?>
                        <td><?php echo $data['user_full_name']; ?> </td>
                  <?php } ?>
                  <td><?php echo $data['expense_title']; ?> </td>
                      <td><?php if ($data['date'] != "0000-00-00" && $data['date'] != "") {
                            echo date('d-M-Y', strtotime($data['date']));
                          } ?> </td>
                      
                      <td><?php echo $data['amount']; ?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td colspan="4" class="no-border pb-4"></td>

                  </tr>
                  <tr class="">
                    <td class="no-border text-white bg-primary" ></td>
                    <?php if (!isset($uId) || $uId == 0) { ?>
                      <td class="no-border text-white bg-primary"> </td>
                      <?php } ?>
                    <td class="no-border text-white bg-primary"> </td>
                    <td class=" text-end no-border border-left text-white bg-primary">Total Amount </td>
                    <td class=" no-border text-white bg-primary"><?php echo number_format($total_amount,2); ?></td>
                  </tr>
                </tbody>
              </table>
           
          
        </div>
      </div>
    </div>
  </div>
</body>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

    window.onload = function() { printDiv('printableArea'); }

  Android.print(printContents);
</script>

</html>
<style>


</style>