<?php error_reporting(0);
  ?>
  <?php if(isset($_GET) && $_GET['id']>0){ ?>
    <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company About Us </h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <a href="companyHomeSlider"  class="btn btn-sm btn-primary waves-effect waves-light"> Back </a>
      </div>
     </div>
     </div>
     
    




      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <?php 
                $q1 = $d->selectRow('*',"company_home_slider_master","company_home_slider_id='$_GET[id]'");
                $resultData = mysqli_fetch_assoc($q1); 
               ?>
              <div class="row m-2">
                <div class="col-md-12 form-group">
                  <div class="">
                    <a href="../img/front_slider/<?php echo $resultData['company_home_slider_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $resultData["company_home_slider_image"]; ?>"><img width="50%" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/front_slider/<?php echo $resultData['company_home_slider_image']; ?>"  href="#divForm<?php echo $resultData['company_home_slider_id'];?>" class="btnForm lazyload" ></a>
                  </div>
                
                </div>
                <div class="col-md-12 form-group">
                    <label>Slider title</label>
                  <h6><?php echo $resultData['company_home_slider_title']; ?></h6>
                </div>
                <div class="col-md-12 form-group">
                <label>Slider description</label>
                  <p><?php echo $resultData['company_home_slider_description']; ?></p>
                </div>
                <div class="col-md-6 form-group">
                    <label>Slider URL</label>
                   <p><?php echo $resultData['company_home_slider_url']; ?></p>
                </div>
                <div class="col-md-6 form-group">
                    <label>Slider Mobile No</label>
                    <p><?php echo $resultData['company_home_slider_mobile']; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <?php }else{ ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Home Slider</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addCompanyHomeSlider"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
      </div>
     </div>
     </div>
      <?php 
        $i=1;
        $q=$d->select("company_home_slider_master","society_id='$society_id' ");
        $counter = 1;
        $carouselIds = array();
        $carouselImages = array();
        while ($data=mysqli_fetch_array($q)) {
            array_push($carouselIds, $data['company_home_slider_id']);
            array_push($carouselImages, $data['company_home_slider_image']);
        ?>
      <?php } ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                  <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                      <?php for($a=0; sizeof($carouselIds)>$a; $a++){ ?>
                        <li data-target="#demo" data-slide-to="<?php echo $a; ?>" class="<?php if($a == 0){ echo "active"; } ?>"></li>
                      <?php } ?>
                      
                      
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                    <?php for($b=0; sizeof($carouselImages)>$b; $b++){ ?>
                      <div class="carousel-item <?php if($b == 0){ echo "active"; } ?>">
                        <img src="../img/front_slider/<?php echo $carouselImages[$b]; ?>" alt="" width="100%" height="300px">
                      </div>
                      <?php } ?>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                      <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                      <span class="carousel-control-next-icon"></span>
                    </a>
                  </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      
                      
                      <th>#</th>
                      <th>Slider Image</th>
                       <th>Action</th>
                     
                    </tr>
                </thead>
                <tbody>
                    <?php 
                      $i=1;
                      $q=$d->select("company_home_slider_master","society_id='$society_id' ");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                     <tr>
                  
                  <td><?php echo $i++; ?></td>
                  <td><a href="../img/front_slider/<?php echo $data['company_home_slider_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["company_home_slider_image"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  src="../img/ajax-loader.gif" data-src="../img/front_slider/<?php echo $data['company_home_slider_image']; ?>"  href="#divForm<?php echo $data['company_home_slider_id'];?>" class="btnForm lazyload" ></a></td>
                  <td>
                    <div class="d-flex align-items-center">
                        <form action="addCompanyHomeSlider" method="post" accept-charset="utf-8" class="mr-2">
                            <input type="hidden" name="company_home_slider_id" value="<?php echo $data['company_home_slider_id']; ?>">
                            <input type="hidden" name="edit_company_home_slider" value="edit_company_home_slider">
                            <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                        </form>
                        <form action="controller/CompanyHomeSliderController.php" method="post" accept-charset="utf-8" class="mr-2">
                            <input type="hidden" name="company_home_slider_id" value="<?php echo $data['company_home_slider_id']; ?>">
                            <input type="hidden" name="delete_company_home_slider" value="delete_company_home_slider">
                            <button type="submit" class="form-btn btn btn-sm btn-danger" name="" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </form>
                        <a href="companyHomeSlider?id=<?php echo $data['company_home_slider_id']; ?>" class="btn btn-sm btn-primary mr-2"><i class="fa fa-eye"></i></a>
                    </div>
                  </td>
                </tr>
                  
                  <?php } ?>
                  </tbody>
            </table>
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <?php } ?>



<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
