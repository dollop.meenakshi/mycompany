<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title"> Elections</h4>

      </div>
      <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="election?action=Add" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteElection');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>


        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Election</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Participants</th>
                    <th>Result</th>
                    <th>For</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $q=$d->select("election_master","society_id='$society_id'","ORDER BY election_id DESC");
                    $i = 0;
                    while($row=mysqli_fetch_array($q)) {
                      $i++;
                      $qEle=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id=".$row['election_id'],"");
                      $totalUsers= mysqli_num_rows($qEle);
                  ?>
                    <tr>
                      <td class='text-center'>
                       <?php if($totalUsers==0){ ?>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['election_id']; ?>">
                      <?php }?>
                      </td>
                      <td><?php echo $i; ?></td>
                      <td class="tableWidth"><?php echo $row['election_name']; ?></td>
                      <td><?php echo $row['election_date']; ?></td>
                      <td>
                        <?php //IS_470
                          $approvedUsers = 0;
                          $qNota=$d->select("election_users"," is_nota = '1' and  election_id='$row[election_id]'","");
                          $notaCount= mysqli_num_rows($qNota);
                          $approvedUsers = $approvedUsers+$notaCount;
                          while($rowData=mysqli_fetch_array($qEle)) {
                            if($rowData['election_user_status']==1){  
                              $approvedUsers++; 
                            }   
                          }  
                        ?> 
                            <form action="controller/pullingController.php" method="post" id="statusForm">
                              <input type="hidden" name="election_id" value="<?php echo $row['election_id']; ?>">
                              <input type="hidden" name="election_for" value="<?php echo $row['election_for']; ?>">
                              <?php if($row['election_status']==0){?>
                                <input type="hidden" name="election_status" value="1" >
                                <input type="hidden" name="electionStart" value="electionStart" >
                                <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                                <label class="label label-danger">Nomination Open</label>
                                <?php echo $rowData['election_date']; if($approvedUsers>1 && $row['election_date']==date("Y-m-d")) { //IS_470?>
                                  <input type="submit" name="" class="btn form-btn  btn-danger btn-sm" value="Start Election ?" >
                                <?php } else if($approvedUsers>1 && $row['election_date']!=date("Y-m-d")) {?> 
                               <input type="button" name=""  onclick="swal('In Valid Election Start Date!');" class="btn btn-danger  btn-sm" value="Start Election ?" >
                              <?php }}else if($row['election_status']==1){?>
                                <input type="hidden" name="election_status" value="3">
                                <input type="hidden" name="electionEnd" value="Close">
                                <input type="hidden" name="election_name" value="<?php echo $row['election_name']; ?>" >
                                <label class="label label-danger">Election Running</label>
                                
                                  <input type="submit" name="electionEnd" class="btn btn-danger btn-sm form-btn" value="Close ?" >
                                  <input type="hidden" name="election_name" value="<?php echo $row['election_name']; ?>" >
                                
                              <?php } else if($row['election_status']==3){?>
                                <input type="hidden" name="election_status" value="3">
                                <input type="hidden" name="election_name" value="<?php echo $row['election_name']; ?>" >
                                <input type="button" name="electionEnd" class="btn btn-warning btn-sm" value="Election Closed" onclick="return  swal('This Election is Closed & Please Publish the Result', {icon: 'info', });">
                              <?php }  else if($row['election_status']==2){?>
                                <input type="hidden" name="election_status" value="3">
                                <input type="hidden" name="election_name" value="<?php echo $row['election_name']; ?>" >
                                <input type="button" name="electionEnd" class="btn btn-warning btn-sm" value="Election Closed" onclick="return  swal('This Election is Closed & Result Declare', {icon: 'info', });">
                              <?php } ?>
                            </form>
                      </td>
                      <td>
                        <div style="display: inline-block;">
                        <a href="electionParticipateUsers?election_id=<?php echo $row['election_id'] ?>" class="btn btn-primary btn-sm" name="pullingReport">View</a>
                      <span style="width: 10px !important;"></span>
                      </div>

                      <?php 
                      $today = date("Y-m-d");
                   
                      if($row['election_status']==0){
                       ?>
                      
                      <div style="display: inline-block;">
                         <form   class="mr-2" action="election" method="post">
                <input type="hidden" name="election_id" value="<?php echo $row['election_id']; ?>">
                 <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                <input type="hidden" name="action" value="Edit">
                <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
              </form>
            </div>
          <?php } ?>
                      </td>
                      <td>
                        <?php if ($row['election_status']==2 OR $row['election_status']==3  ) { ?>
                          <form action="electionResult" method="post">
                            <input type="hidden" name="election_id" id="electionData" value="<?php echo $row['election_id'] ?>">
                            <button type="submit" class="btn btn-success btn-sm" name="pullingReport">View Result</button>
                          </form>

                        <?php } ?>
                      </td>
                      <td>
                        <?php
                       
                        if ($row['election_for']==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 

                        } else {
                          $qf=$d->selectRow("floor_name","floors_master","floor_id='$row[election_for]'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'];
                        }
                        ?>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>