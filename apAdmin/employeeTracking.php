<?php error_reporting(0);
$uId = (int)$_REQUEST['uId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6 col-md-6 col-12">
        <h4 class="page-title">View Employee Tracking History</h4>

      </div>
      <div class="col-sm-6 col-md-6 col-12 text-right">

        <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#addTrackUser" onclick="buttonSettingForTrackUser()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light">Add</a> -->
      </div>
    </div>
    <form action="" class="branchDeptFilter">
      <div class="row ">
        <?php include('selectBranchDeptForFilter.php'); ?>
        <div class="col-md-3 form-group ">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <?php //IS_675  example to examplePenaltiesTbl
              if (isset($bId) && $bId > 0) {
              ?>
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>Sr.No</th>
                    <th>Employee Name</th>
                    <th>Department</th>
                    <th>Log</th>
                    <th>MaP</th>
                    <th>Accessibility</th>
                    <th>phone model </th>
                    <th>App Version </th>
                    <th>OS </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (isset($uId) && $uId > 0) {
                    $userFilterQuery = " AND users_master.user_id='$uId'";
                  }
                  if (isset($dId) && $dId > 0) {
                    $dptFilterQuery = " AND users_master.floor_id='$dId'";
                  }
                  if (isset($bId) && $bId > 0) {
                    $blkFilterQuery = " AND users_master.block_id='$bId'";
                  }
                  $q = $d->selectRow('users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name', "users_master LEFT JOIN block_master ON block_master.block_id = users_master.block_id LEFT JOIN floors_master ON floors_master.floor_id = users_master.floor_id", "users_master.delete_status=0 AND users_master.society_id = $society_id AND users_master.track_user_location=1 $userFilterQuery $dptFilterQuery $blkFilterQuery $blockAppendQuery");

                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                  ?>
                    <tr>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['user_full_name']; ?></td>
                      <td><?php echo $data['floor_name'] . " (" . $data['block_name'] . ")"; ?></td>
                      <td>
                          <form  action="controller/userController.php" method="post">
                            <input type="hidden" name="bId" value="<?php echo $data['block_id']; ?>">
                            <input type="hidden" name="dId" value="<?php echo $data['floor_id']; ?>">
                            <input type="hidden" name="uId" value="<?php echo $data['user_id']; ?>">
                            <input type="hidden" name="viewLocationLog" value="viewLocationLog">
                            <button type="submit" class="btn btn-sm btn-info ml-1">Location Log </button>
                          </form>
                      </td>
                      <td>

                          <form  action="controller/userController.php" method="post">
                            <input type="hidden" name="bId" value="<?php echo $data['block_id']; ?>">
                            <input type="hidden" name="dId" value="<?php echo $data['floor_id']; ?>">
                            <input type="hidden" name="uId" value="<?php echo $data['user_id']; ?>">
                            <input type="hidden" name="viewLocationLog" value="viewLocationLog">
                            <input type="hidden" name="mapView" value="mapView">
                            <button type="submit" class="btn btn-sm btn-primary ml-1">Drive Route </button>
                          </form>
                          
                      </td>
                      <td>
                        <?php if($data['track_user_location']==1 && $data['device']!='ios'){ ?>
                        
                        <?php if ($data['take_accessibility_permission'] == "1" && $data['accessibility_permision_on']==1) {
                            echo " Granted";
                         } else if ($data['take_accessibility_permission'] == "1" && $data['accessibility_permision_on']==0) {
                            echo " Not Granted";
                         }
                         } ?>
                      </td>
                      <td><?php if($data['phone_model']!="") { echo $data['phone_model'] . " (" . $data['phone_brand'] . ")"; }?></td>
                      <td><?php echo $data['app_version_code']; ?></td>
                      <td><?php echo $data['device'].' '.$data['mobile_os_version']; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php }else { ?>
                 Note : Please Select Branch And Department !!!!!
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div>

<div class="modal fade" id="addTrackUser">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Track User</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row ">
            <div class="col-md-12">
              <form id="trackUser" action="controller/trackUserController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12" id="">
                    <select name="block_id" id="block_id" class="form-control single-select inputSl" onchange="getFloorByBlockIdForTrack(this.value)" required>
                      <option value="">-- Select Branch --</option>
                      <?php
                      $qb2 = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                      while ($blockData2 = mysqli_fetch_array($qb2)) {
                      ?>
                        <option value="<?php echo  $blockData2['block_id']; ?>"><?php echo $blockData2['block_name']; ?></option>
                      <?php } ?>

                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Department <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12" id="">
                    <select name="floor_id" id="track_floor_id" class=" inputSl form-control single-select" onchange="getUserForTrack(this.value);" required>
                      <option value="">--Select Department --</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Employee <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12" id="">
                    <select name="user_id[]" id="track_user_id" multiple class="inputSl form-control single-select">
                      <option>-- Select Employee --</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Track Time <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12" id="">
                    <input type="number" class="form-control inputCls" id="track_user_time" name="track_user_time" placeholder="minute">
                  </div>
                </div>
                <div class="form-footer text-center">
                  <input type="hidden" name="addTrackUser" value="addTrackUser">
                 
                 
                  <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <span class="btnSpan"></span> </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="EditTrackUser">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Track User</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row ">
            <div class="col-md-12">
              <form id="updateTrackUser" action="controller/trackUserController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Track Time <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12" id="">
                    <input type="number" class="form-control inputCls track_user_time" id="" name="track_user_time" min="5" placeholder="minute">
                  </div>
                </div>
                <div class="form-footer text-center">
                  <input type="hidden" id="editTrackerUser" name="editTrackerUser" value="editTrackerUser">
                  <input type="hidden" name="trackUserId" class="trackUserId" value="">
                  <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                  <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                  <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i><span class="btnSpan"></span></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style>
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type=number] {
    -moz-appearance: textfield;
  }
</style>

<script type="text/javascript">
  function buttonSettingForTrackUser() {
    $('.btnSpan').text("Add");
    $('.inputCls').val("");
    $('.inputSl').val("");
    $('.inputSl').select2();

  }
</script>.