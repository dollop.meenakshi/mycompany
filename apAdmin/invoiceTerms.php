<?php //IS_1581 whole new file
extract(array_map("test_input" , $_POST));
           unset($_SESSION['post']); 

?>
<style type="text/css" media="screen">
 .card-body img {
  width: 280px !important;
 }  
</style>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-12">
        <h4 class="page-title">Invoice Terms & Conditions</h4>
     </div>
     <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <?php 
          $q=$d->select(" invoice_term_conditions_master","society_id='$society_id'  ","ORDER BY invoice_term_condition_id DESC limit 0,1");
           if(mysqli_num_rows($q) <= 0) {
           ?>
         <button class="btn  btn-sm btn-success " data-toggle="modal" data-target="#addModal"><i class="fa fa-plus mr-1"></i> Add Invoice Terms and Conditions</button>  
       <?php } ?> 
        </div>
      </div>
     </div>
    <!-- End Breadcrumb-->
     <div class="row">
        <?php 
         if(mysqli_num_rows($q)>0) {
          while($row=mysqli_fetch_array($q)){ ?> 
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header bg-primary text-white"  ><i class=" fa fa-tasks" aria-hidden="true"></i> Invoice Term & Conditions</div>
            <div class="card-body">
               <?php //IS_569 <div class="cls-notice-info"> 
               ?>
              <p class="card-text"><div class="cls-notice-info"><?php echo stripslashes($row['condition_desc']);?></div></p>
            </div>
             <div  class="card-footer">
              

               <?php
                                if($row['status']=="0"){
                                ?>
                                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['invoice_term_condition_id']; ?>','TermConditionDeactive');" data-size="small"/>
                                  <?php } else { ?>
                                 <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['invoice_term_condition_id']; ?>','TermConditionActive');" data-size="small"/>
                                <?php } ?>

                           <button class="btn  btn-sm btn-success " data-toggle="modal" data-target="#editModal"> <i class="fa fa-pencil"></i>Edit </button>       
              <div class="modal fade" id="editModal"><!-- Edit Modal -->
      <div class="modal-dialog">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Edit Term and Conditions</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="editTermFrm" action="controller/termConditionController.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="invoice_term_condition_id" value="<?php echo $row['invoice_term_condition_id'];?>">
              
              <div class="form-group row">
                <label for="visitor_name" class="col-sm-12 col-form-label">Terms and Conditions <span class="required">*</span></label>
                <div class="col-sm-12" id="">

                 <textarea required="" id="summernoteImgage" name="condition_desc">
             <?php echo $row['condition_desc'];?>
          </textarea>
                </div>
              </div>

             
               
               
              <div class="form-footer text-center">
                <input type="hidden" name="editTermBtn" value="editTermBtn">
                <button type="submit" name="" value="addDailyVisitorBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--end modal -->
              
            </div>
          </div>
        </div>
         <?php } } else {
      echo "<img src='img/no_data_found.png'>";
    } ?>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  <div class="modal fade" id="addModal"><!-- Edit Modal -->
      <div class="modal-dialog">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Add Term and Conditions</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="addTermFrm" action="controller/termConditionController.php" method="post" enctype="multipart/form-data">
              
              <div class="form-group row">
                <label for="visitor_name" class="col-sm-12 col-form-label">Terms and Conditions <span class="required">*</span></label>
                <div class="col-sm-12" id="">

                 <textarea required="" id="summernoteImgage" name="condition_desc">
             
          </textarea>
                </div>
              </div>

             
               
               
              <div class="form-footer text-center">
                      <input type="hidden" name="addTermBtn" value="addTermBtn"> 
                <button type="submit" name="" value="addDailyVisitorBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--end modal -->
