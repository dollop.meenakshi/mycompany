<?php 
extract(array_map("test_input" , $_POST));

$q=$d->select("transection_master","transection_id='$transection_id'");
$data = mysqli_fetch_array($q);
extract($data);
// print_r($_REQUEST);

$qp=$d->select("package_master","package_id='$package_id'");
$pkData=mysqli_fetch_array($qp);

$sp=$d->select("society_master","society_id='$society_id'");
$socData=mysqli_fetch_array($sp);
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Payment Details</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Payment Details</li>
        </ol>
        
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card" id="printableArea">
          <div class="card-body">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h3>
                Invoice
                <small>#<?php echo $invoice_no; ?></small>
              </h3>
            </section>

            <!-- Main content -->
            <section class="invoice">
              <!-- title row -->
              <div class="row mt-3">
                <div class="col-lg-6">
                  <h4><i class="fa fa-building"></i> MyCo</h4>
                   <address>
                    <strong>MyCo</strong><br>
                    F-502, Patligram apartment, Bajrangpuri,Patna-800007, Patna, BIHAR,<br> India - 800007
                  </address>
                  <span><?php echo $xml->string->tax;?>  No: </span>
                </div>
                <div class="col-lg-6 text-right">
                  <!-- <h5 class="">Date: <?php echo $traDate; ?></h5> -->
                  <b>Invoice #<?php echo $invoice_no; ?></b><br>
                  <b>Order ID:</b> <?php echo $payuMoneyId ?><br>
                  <b>Payment Date:</b> <?php echo $transection_date; ?><br>
                </div>
              </div>

              <hr>
              <div class="row invoice-info">
                
                <div class="col-sm-12 invoice-col">
                  Customer Details
                  <address>
                    <strong><?php echo $socData['society_name']; ?></strong><br>
                    <?php echo $socData['society_address']; ?>
                  </address>
                  <p><?php echo $xml->string->tax;?>  No: </p>

                </div><!-- /.col -->

              </div><!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Qty</th>
                        <th>Package</th>
                        <th>No Of Month #</th>
                        <th>Description</th>
                        <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><?php echo $package_name; ?></td>
                        <td><?php echo $pkData['no_of_month'];?></td>
                        <td><?php echo $pkData['packaage_description'];?></td>
                        <td><?php echo $currency; ?> <?php echo $transection_amount; ?></td>
                      </tr> 
                      
                    </tbody>
                  </table>
                </div><!-- /.col -->
              </div><!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-lg-6 payment-icons">
                  <p class="lead">Payment Methods:</p>
                  <img src="img/PayUmoney_Logo.jpg" alt="PayUmoney">
                  
                </div><!-- /.col -->
                <div class="col-lg-6">
                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <th style="width:50%">Subtotal:</th>
                          <td><?php echo $currency; ?> <?php echo $transection_amount; ?></td>
                        </tr>
                      
                        <tr>
                          <th>Total:</th>
                          <td><?php echo $currency; ?> <?php echo $transection_amount; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->

              <!-- this row will not appear when printing -->
              <hr>
              <div class="row no-print" id="printPageButton">
                <div class="col-lg-3">
                  <a href="#" onclick="printDiv('printableArea')"  class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="col-lg-9">
                  <div class="float-sm-right">
                    <button class="btn btn-primary m-1"><i class="fa fa-envelope"></i> Receive on Mail</button>
                  </div>
                </div>
              </div>
            </section><!-- /.content -->
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->

 <script type="text/javascript">
 	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
 </script>

 <style type="text/css">
 	@media print {
	  #printPageButton {
	    display: none;
	  }
	}
 </style>