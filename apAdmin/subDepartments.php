<?php
if($_GET['f'] == "subDepartments" && isset($_GET['changeSort']) && $_GET['changeSort'] == "yes" && isset($_GET['floor_id']) && $_GET['floor_id'] != '' && !empty($_GET['floor_id']))
{
  $floor_id = $_GET['floor_id'];
  $floor = $d->select("floors_master,sub_department","sub_department.floor_id = floors_master.floor_id AND floors_master.floor_id = '$floor_id'","ORDER BY sub_department_sort");
?>
  <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-5">
        <h4 class="page-title"><?php echo $_GET['floor_name']; ?></h4>
      </div>
      <div class="col-sm-3 col-7 text-right">
        <div class="btn-group float-sm-right">
        <a class="btn btn-sm btn-primary" href="subDepartments"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row" id="sub-dept-list">
      <?php
        while ($floor_details = $floor->fetch_assoc())
        {
        ?>
          <div class="col-lg-3 col-md-6 col-12 subDeptBox" data-post-id="<?php echo $floor_details["sub_department_id"]; ?>">
            <div class="card">
              <div class="card-header text-uppercase">
                <?php echo $floor_details['floor_name']; ?>
              </div>
              <div class="card-body">
                <?php echo $floor_details['sub_department_name']; ?>
              </div>
            </div>
          </div>
      <?php
        }
      ?>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
<?php
}
else
{
?>
 <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-5">
        <h4 class="page-title">Sub <?php echo $xml->string->floors; ?></h4>
      </div>
      <div class="col-sm-3 col-7 text-right">
        <div class="btn-group float-sm-right">
          <a href="subDepartment" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->multiple; ?> Sub <?php echo $xml->string->floors; ?></a>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <?php 
    $q3 = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","");
    if(mysqli_num_rows($q3)>0)
    {
    while ($data=mysqli_fetch_array($q3))
    {
        $block_id=$data['block_id'];
        $block_name=$data['block_name'];
        $floorCount= $d->count_data_direct("floor_id","floors_master","society_id='$society_id' AND  block_id='$block_id'");
    ?>
    <div class="row " > <!-- style="background-color: #00000047;" -->
    <div class="col-12 col-lg-12">
    <div class="card border border-info">
      <div class="row m-0 pt-3 p-2">
    <div class="col-lg-12">
        <h5 class=""><?php echo $block_name ?> (<?php echo $floorCount; ?>)</h5>
    </div>
        <!-- <div class="row"> -->
            <?php
                $fq=$d->select("floors_master","society_id='$society_id' AND block_id='$block_id' $blockAppendQueryFloor","ORDER BY floor_sort ASC");
                if(mysqli_num_rows($fq)>0)
                {
                while ($floorData=mysqli_fetch_array($fq))
                {
                    $floor_id=$floorData['floor_id'];
                    $floor_name=$floorData['floor_name'];
                    $subDeptCount= $d->count_data_direct("sub_department_id","sub_department","society_id='$society_id' AND  floor_id='$floor_id'");
            ?>
                <div class="col-lg-4 ">
                    <form action="addSingleSubDepartment" method="post">
                    <div class="card gradient-scooter  no-bottom-margin ">
                        <div class="card-body text-uppercase">
                        <a href="#"><?php echo $floor_name; ?> (<?php echo $subDeptCount; ?>) <?php if($subDeptCount>0) { ?>
                            <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                            <input type="hidden" name="blockName" value="<?php echo $block_name; ?>">
                            <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                            <input type="hidden" name="floorName" value="<?php echo $floor_name; ?>">
                            <input type="hidden" name="floor_id" value="<?php echo $floor_id; ?>">
                            <input type="hidden" name="subDeptCount" value="<?php echo $subDeptCount; ?>">
                            <input type="submit" class="pull-right btn btn-sm btn-primary fa fa-plus" value="&#xf067" name="singleSubDept"><?php } ?>
                        </a>
                        <a class="btn btn-sm bg-warning text-white fa fa-sort float-sm-right" href="subDepartments?changeSort=yes&floor_name=<?php echo $floor_name; ?>&floor_id=<?php echo $floor_id; ?>" title="Change Order"></a>
                        </div>
                        <div class="">
                            <ul class="list-group maxCard">
                            <?php
                                $sdq=$d->select("sub_department","society_id='$society_id' AND floor_id='$floor_id'","ORDER BY sub_department_sort ASC");
                                if(mysqli_num_rows($sdq)>0)
                                {
                                while ($subDeptData=mysqli_fetch_array($sdq))
                                {
                            ?>
                                <li class="list-group-item node-treeview1" data-nodeid="4" style="color:undefined;background-color:undefined;"><span class="indent"></span><span class="icon glyphicon"></span><span class="icon node-icon"></span><?php echo $subDeptData['sub_department_name'] ?>                                                 
                                <i title="Delete Department" class="fa fa-trash-o pull-right" onclick="DeleteSubDepartment('<?php echo $subDeptData['sub_department_id']; ?>');"></i>
                                <i title="Edit Department Name" class="fa fa-pencil pull-right" data-toggle="modal" data-target="#editSubDepartment" onclick="editSubDepartment('<?php echo $subDeptData['sub_department_id']; ?>','<?php echo $subDeptData['sub_department_name']; ?>');"></i>
                                </li>
                            <?php
                                }
                                }
                                else
                                { ?>
                                    <li class="list-group-item node-treeview1">No Sub Department Added</li>
                                <?php }
                            ?>
                            </ul>
                        </div>
                    </div>
                        <input type="hidden" name="csrf" value="dbcd371e6daebdec7a9d18bba000b5a0">
                    </form>
                </div> 
            <?php
                }
                }
                else
                {
                    echo "No ".$xml->string->floor." Added";
                }
            ?>     
       <!--  </div> -->
    </div>
    </div>
    </div>
    </div> 
    <?php }
    }
    else
    {
      echo "<img src='img/no_data_found.png'>";
    } ?>   
    </div> 
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
<?php
}
?>



  <div class="modal fade" id="editSubDepartment">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> Sub <?php echo $xml->string->floor; ?> <?php echo $xml->string->name; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="subDepartmentForm" action="controller/subDepartmentController.php" method="post">
            <input type="hidden" id="subDepartmentId" name="sub_department_id">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Sub <?php echo $xml->string->floor; ?> <?php echo $xml->string->name; ?>  <span class="text-danger">*</span> </label>
                    <div class="col-sm-8" id="PaybleAmount">
                      <input  type="text" id="oldSubDepartmentName" class="form-control" name="sub_department_name" required="" maxlength="30">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <input type="hidden" name="updateSubDept" value="updateSubDept">
                  <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
                </div>

          </form>
      </div>
     
    </div>
  </div>
</div><!--End Modal -->