<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id=$_COOKIE['society_id'];
extract(array_map("test_input" , $_POST));
if (isset($request_id)) {
    $q=$d->select("request_master","society_id='$society_id' AND request_id='$request_id'");
    $data=mysqli_fetch_array($q);
?>
    <form id="requestFrm" action="controller/requestController.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
        <input type="hidden" value="<?php echo $request_id; ?>" name="request_id">
        <input type="hidden" value="<?php echo $data['request_unit_id']; ?>" name="unit_id">
        <input type="hidden" value="<?php echo $data['request_user_id']; ?>" name="user_id">
        <input type="hidden" value="<?php echo $data['request_title']; ?>" name="request_title">
            <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">From </label>
                <div class="col-sm-8">
                    <?php 
                        $userDa=$d->selectArray("users_master","user_id='$data[request_user_id]'");
                       
                        $blockDa=$d->selectArray("block_master","block_id='$userDa[block_id]'");
                        echo $userDa['user_full_name']." - ".$userDa['user_designation']." (".$blockDa['block_name'].")"; ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Request </label>
                <div class="col-sm-8">
                     <?php echo $data['request_title'] ?>
                </div>
            </div>
             <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Description </label>
                <div class="col-sm-8">
                     <?php echo $data['request_description'] ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Date </label>
                <div class="col-sm-8">
                <?php echo date("d M Y h:i A", strtotime($data['request_date'])); ?>
                </div>
            </div>
            <?php if($data['request_image']!='')  {?>
             <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Image </label>
                <div class="col-sm-8">
                    <img  src="../img/request/<?php echo $data['request_image']; ?>" height="100" width="100">
                </div>
            </div>
            <?php }?>
            <?php if($data['request_audio']!='')  {?>
             <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Audio File </label>
                <div class="col-sm-8">
                    <audio controls>
                      <source src="../img/request/<?php echo $data['request_audio']; ?>" type="audio/mp3">
                    </audio> 
                </div>
            </div>
            <?php }?>
            <?php if ($data['request_closed_by']!=0) { ?>
                <div class="form-group row" >
                    <label for="input-10" class="col-sm-4 col-form-label">Closed By: </label>
                    <div class="col-sm-8">
                     <?php $adminData = $d->selectArray("bms_admin_master","admin_id='$data[request_closed_by]' AND society_id='$society_id'"); 
                        echo $adminData['admin_name'];
                     ?> 
                    </div>
                </div>   
            <?php  } ?>
          
            <div class="form-group row" >
                <label for="request_status" class="col-sm-4 col-form-label">Status <span class="required">*</span></label>
                <div class="col-sm-8">
                    <select required="" name="request_status" class="form-control">
                        <option <?php if($data['request_status']==3) {echo 'selected';} ?> value="3">Reply</option>
                        <option <?php if($data['request_status']==2) {echo 'selected';} ?> value="2">In Progress</option>
                        <option <?php if($data['request_status']==1) {echo 'selected';} ?> value="1">Close</option>
                    </select>
                </div>
            </div>
            <div class="form-group row" >
                <label for="input-10" class="col-sm-4 col-form-label">Message </label>
                <div class="col-sm-8">
                    <textarea maxlength="300" class="form-control" name="request_track_msg"></textarea>
                </div>
            </div>
            <div class="form-group row" >
                <label for="request_track_img" class="col-sm-4 col-form-label">Image </label>
                <div class="col-sm-8">
                    <input type="file" accept="image/*" name="request_track_img" class="form-control-file border photoOnly" >
                </div>
            </div>
            <div class="form-group row" >
                <label for="request_track_voice" class="col-sm-4 col-form-label">Audio </label>
                <div class="col-sm-8">
                    <input type="file" accept="audio/*" name="request_track_voice" class="form-control-file border mp3Only" accept=".mp3">
                    (Max File Size 4MB)
                </div>
            </div>

     
            <div class="form-footer text-center">
              <button type="submit" name="changeStausComp" value="changeStausComp" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
            </div>
    </form>
<?php } ?>

<script>
    $(".photoOnly").change(function () {
     // alert(this.files[0].size);
     var fileExtension = ['jpeg', 'jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.photoOnly').val('');
    }
});

$(".mp3Only").change(function () {
  // alert(this.files[0].size);
  var fileExtension = ['mp3','wav','flac','ogg','m3u','acc','wma','midi','aif','m4a','mpa','pls'];
 if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
     // alert("Only formats are allowed : "+fileExtension.join(', '));
     swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
     $('.mp3Only').val('');
 }
});
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));


$("#requestFrm").validate({
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                error.insertAfter(element);               // default
              }
            },
            rules: {
              request_status: {
                required: true 
              } ,
               
              request_track_voice: {
                filesize4MB:true,
              } 

        },
        messages: {
          request_status: {
            required : "Please select request status" 
          } 
      },
      submitHandler: function(form) {
          $(':input[type="submit"]').prop('disabled', true);
          form.submit(); 
      }
    });
</script>