<?php 
error_reporting(0);
extract($_REQUEST);
$userMobileAray = array();
$qq=$d->selectRow("user_mobile","user_wallet_master","","GROUP BY user_mobile");
while ($wData=mysqli_fetch_array($qq)) {
  array_push($userMobileAray, $wData['user_mobile']);
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Wallet Report (<?php echo $currency;?> <?php $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0");
            $row=mysqli_fetch_array($count6);
            $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];
             echo number_format($totalDebitAmount1,2,'.','');
             ?>)</h4>
        </div>
     
     </div>

    
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
                <table id="exampleReportBalancesheet" class="table table-bordered auto-index">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th><?php echo $xml->string->unit; ?></th>
                            <th>Avl. Balance</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $i=1;
                          // echo $balancesheet_id;
                           $ids = join("','",$userMobileAray);   
                            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 AND users_master.user_mobile IN ('$ids') $blockAppendQuery","GROUP BY users_master.user_mobile ORDER BY block_master.block_sort ASC ");

                          while ($data=mysqli_fetch_array($q3)) {
                           
                            $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$data[user_mobile]'");
                          $row=mysqli_fetch_array($count6);
                          $totalWalletAmount=$row['SUM(credit_amount-debit_amount)'];
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $data['user_full_name']; ?> (<?php 
                              if($data['user_type']=="0" && $data['member_status']=="0" ){
                                  echo "Owner";
                              } else  if($data['user_type']=="0" && $data['member_status']=="1" ){
                                  echo "Owner Family";
                              } else  if($data['user_type']=="1" && $data['member_status']=="0"  ){
                                  echo "Tenant";
                              }else  if($data['user_type']=="1" && $data['member_status']=="1"   ){
                                  echo "Tenant Family";
                              } else   {
                                echo "Owner";
                              } 

                               if ($data['member_status']==1) {
                                 echo '-'.$data['member_relation_name'];
                               }
                               ?>)</td>
                            <td>
                              <?php  if ($adminData['role_id']==2) {
                                echo $data['user_mobile']; 
                              } else {
                                  echo "".substr($data['user_mobile'], 0, 2) . '*****' . substr($data['user_mobile'],  -3);
                              } ?>
                            </td>
                            <td><?php echo $data['block_name']; ?>-<?php echo $data['unit_name']; ?></td>
                             <td><?php  echo number_format($totalWalletAmount,2,'.',''); ?></td>
                             <td><a class="btn btn-sm btn-warning" href="userWallet?id=<?php echo $data['user_mobile'];?>">View </a></td>
                          </tr>
                         
                          <?php } ?>
                           
                        </tbody>
                        
                      </table>
            
              </div>

           

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->