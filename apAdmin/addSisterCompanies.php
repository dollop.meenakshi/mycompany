<?php
$soc = $d->selectRow("society_latitude,society_longitude","society_master");
$data = $soc->fetch_assoc();
extract($data);
if(isset($_POST) && !empty($_POST) && isset($_POST['editSisterCompany']))
{
    extract($_POST);
    $q = $d->select("sister_company_master","sister_company_id = '$sister_company_id'");
    $bData = $q->fetch_assoc();
    extract($bData);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="sisterCompanyAddForm" action="controller/sisterCompanyController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <i class="fa fa-building"></i>
                            <?php echo (isset($editSisterCompany)) ? "Edit" : "Add"; ?> Sister Company
                            </h4>
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6  text-center">
                                    <label for="soceityLogo">
                                        <img onerror="this.src='../img/defaultSocityLogo.jpg'" id="societyLogoView" style="border: 1px solid gray; width: 250px; height: auto; border-radius: 15px;cursor: pointer;" src="<?php if(isset($editSisterCompany) && file_exists("../img/society/$sister_company_logo")){ echo '../img/society/'.$sister_company_logo; }else{ echo '../img/defaultSocityLogo.jpg'; } ?>"  src="#" alt="your image" class='' />
                                        <input accept="image/*" class="photoInput photoOnly" id="soceityLogo" type="file" name="sister_company_logo">
                                        <br>
                                        Sister <?php echo $xml->string->society; ?> <?php echo $xml->string->logo; ?> (300 PX * 200 PX)
                                    </label>
                                </div>
                                <div class="col-lg-6 col-md-6  text-center">
                                    <label for="soceityCover">
                                        <img onerror="this.src='../img/defaultSocityLogo.jpg'" id="societyCoverView" style="border: 1px solid gray; width: 250px; height: auto; border-radius: 15px;cursor: pointer;" src="<?php if(isset($editSisterCompany) && file_exists("../img/society/$sister_company_stamp")){ echo '../img/society/'.$sister_company_stamp; }else{ echo '../img/defaultSocityLogo.jpg'; } ?>"  src="#" alt="your image" class='' />
                                        <input accept="image/*" class="photoInput photoOnly" id="soceityCover" type="file" name="sister_company_stamp">
                                        <br>
                                        Sister <?php echo $xml->string->society; ?> Stamp (300 PX & 200 PX)
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sister_company_name" class="col-sm-2 col-form-label">Sister <?php echo $xml->string->society; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input maxlength="50" autocomplete="off" required value="<?php if(isset($editSisterCompany)){ echo $sister_company_name; } ?>" type="text" class="form-control" id="sister_company_name" name="sister_company_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sister_company_address" class="col-sm-2 col-form-label"><?php echo $xml->string->address; ?>  <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <textarea maxlength="255" required name="sister_company_address" class="form-control" rows="4" id="sister_company_address"><?php if(isset($editSisterCompany)){ echo $sister_company_address; } ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sister_company_pincode" class="col-sm-2 col-form-label"><?php echo $xml->string->pincode; ?> <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" autocomplete="off" id="sister_company_pincode" maxlength="6" value="<?php if(isset($editSisterCompany) && $sister_company_pincode != '0'){ echo $sister_company_pincode; } ?>" required class="form-control onlyNumber" inputmode="numeric" name="sister_company_pincode">
                                </div>
                                <label for="sister_company_phone" class="col-sm-2 col-form-label">Company Contact <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" value="<?php if(isset($editSisterCompany)){ echo $sister_company_phone; } ?>" required type="text" class="form-control" id="sister_company_phone" name="sister_company_phone" maxlength="15" minlength="7">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sister_company_email" class="col-sm-2 col-form-label">Company Email <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" value="<?php if(isset($editSisterCompany)){ echo $sister_company_email; } ?>" required type="email" class="form-control" id="sister_company_email" name="sister_company_email">
                                </div>
                                <label for="sister_company_website" class="col-sm-2 col-form-label"><?php echo $xml->string->company_website; ?></label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" maxlength="100" value="<?php if(isset($editSisterCompany) && $sister_company_website != ""){ echo $sister_company_website; } ?>" type="url" placeholder="http://www.example.com" class="form-control" id="sister_company_website" name="sister_company_website">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sister_company_gst_no" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_number; ?> </label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" minlength="15" maxlength="15" value="<?php if(isset($editSisterCompany) && $sister_company_gst_no != ""){ echo $sister_company_gst_no; } ?>" type="text" class="form-control alphanumeric" onkeydown="upperCaseF(this)" id="sister_company_gst_no" name="sister_company_gst_no">
                                </div>
                                <label for="sister_company_pan" class="col-sm-2 col-form-label"><?php echo $xml->string->pan_number; ?></label>
                                <div class="col-sm-4">
                                    <input type="text" autocomplete="off" id="sister_company_pan" maxlength="10" value="<?php if(isset($editSisterCompany) && $sister_company_pan != ""){ echo $sister_company_pan; } ?>" class="form-control" onkeydown="upperCaseF(this)" name="sister_company_pan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <input id="searchInput5" class="form-control" type="text" placeholder="Enter a Google location" autocomplete="off">
                                <div class="map" id="map" style="width: 100%; height: 400px;"></div>
                                <input type="hidden" class="form-control" id="lat" name="sister_company_latitude" placeholder="Lattitude" value="<?php if(isset($editSisterCompany) && $sister_company_latitude != ""){ echo $sister_company_latitude; } ?>">
                                <input type="hidden" class="form-control" id="lng" name="sister_company_longitude" placeholder="sociaty_longitude" value="<?php if(isset($editSisterCompany) && $sister_company_longitude != ""){ echo $sister_company_longitude; } ?>">
                            </div>
                            <div class="form-footer text-center">
                            <?php
                                if(isset($editSisterCompany))
                                {
                            ?>
                                <input type="hidden" name="sister_company_logo_old" value="<?php echo $sister_company_logo; ?>">
                                <input type="hidden" name="sister_company_stamp_old" value="<?php echo $sister_company_stamp; ?>">
                                <input type="hidden" name="sister_company_id" value="<?php echo $sister_company_id; ?>">
                                <input type="hidden" name="updateSisterCompany" value="updateSisterCompany">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                            <?php
                                }
                                else
                                {
                            ?>
                                <input type="hidden" name="addSisterCompany" value="addSisterCompany">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                            <?php
                                }
                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $d->map_key();?>"></script>
<script>
    function upperCaseF(a)
    {
        setTimeout(function(){
            a.value = a.value.toUpperCase();
        }, 1);
    }

    function initialize()
    {
        // var latlng = new google.maps.LatLng(23.05669,72.50606);
        var latitute = document.getElementById('lat').value;
        var longitute = document.getElementById('lng').value;
        if(latitute == "" && longitute == "")
        {
            latt = '<?php echo $society_latitude; ?>';
            lonn = '<?php echo $society_longitude; ?>';
            if(latt == "" && lonn == "")
            {
                latitute = "23.037738";
                longitute = "72.512139";
            }
            else
            {
                latitute = latt;
                longitute = lonn;
            }
        }
        var latlng = new google.maps.LatLng(latitute,longitute);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 15
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
            // icon:'img/direction/'+dirction+'.png'
        });
        var parkingRadition = 5;
        var citymap = {
            newyork: {
                center: {lat: latitute, lng: longitute},
                population: parkingRadition
            }
        };
        var input = document.getElementById('searchInput5');
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        autocomplete10.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place5 = autocomplete10.getPlace();
            if (!place5.geometry)
            {
                window.alert("Autocomplete's returned place5 contains no geometry");
                return;
            }
            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport)
            {
                map.fitBounds(place5.geometry.viewport);
            }
            else
            {
                map.setCenter(place5.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);
            var pincode="";
            for (var i = 0; i < place5.address_components.length; i++)
            {
                for (var j = 0; j < place5.address_components[i].types.length; j++)
                {
                    if (place5.address_components[i].types[j] == "postal_code")
                    {
                        pincode = place5.address_components[i].long_name;
                    }
                }
            }
            bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);
        });
        // this function will work on marker move event into map
        google.maps.event.addListener(marker, 'dragend', function()
        {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    if (results[0])
                    {
                        var places = results[0];
                        var pincode="";
                        var serviceable_area_locality= places.address_components[4].long_name;
                        for (var i = 0; i < places.address_components.length; i++)
                        {
                            for (var j = 0; j < places.address_components[i].types.length; j++)
                            {
                                if(places.address_components[i].types[j] == "postal_code")
                                {
                                    pincode = places.address_components[i].long_name;
                                }
                            }
                        }
                        bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
                    }
                }
            });
        });
    }

    function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
    {
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>