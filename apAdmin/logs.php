  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3">
          <h4 class="page-title"><?php echo $xml->string->other_logs; ?></h4>
        </div>
        <div class="col-lg-3 col-md-4 col-6">
            <form method="get">
              <input id="logrange" class="logrange form-control" name="e1" >
            </form>
          </div>
        <div class="col-lg-6 col-sm-3">
          <div class="btn-group float-sm-right">
           
            
          </div>
       </div>
     </div>

     
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body" id="logData">
              
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->


   <script src="assets/js/jquery.min.js"></script>
   <script type="text/javascript">
    $(function() {

        var start = moment().subtract(6, 'days');
        var end = moment();

        function cb(start, end) {
          
            
            $('#logrange').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

              var startDate = start.format('YYYY-MM-D');
              var endDate = end.format('YYYY-MM-D');
              $(".ajax-loader").show();
              $.ajax({
                url: "getLog.php",
                cache: false,
                type: "POST",
                data: {startDate:startDate,endDate:endDate,csrf:csrf},
                success: function(response){
                    $("#logData").html(response);
                    $(".ajax-loader").hide();
                }
             });
        }

        $('#logrange').daterangepicker({
            startDate: start,
            endDate: end,
            locale: {
              format: 'MMMM D, YYYY'
            },
            maxDate: new Date(),
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            
        }, cb);

        cb(start, end);

    });
   </script>



