<?php
error_reporting(0);
extract($_REQUEST);
if (isset($_GET['from']) && $_GET['id']!='') {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1 || filter_var($_GET['id'], FILTER_VALIDATE_INT) != true){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='welcome';
        </script>");
  }
  $balancesheet_id= $_GET['id'];
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Balancesheet Report</h4>
        </div>
     </div>
    <form action="" id="personal-info" method="get">
     <div class="row pt-2 pb-2">
            <div class="col-lg-2 col-6">
              <input placeholder="From Date" type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>">
            </div>
            <div class="col-lg-2 col-6">
              <input placeholder="To Date"  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" >
            </div>
            <div class="col-lg-4 col-6">
             <select type="text" required="" class="form-control single-select" name="id">
                <option value=""> Select </option>
                 <?php
                  $q=$d->select("balancesheet_master","society_id='$society_id'","");
                   while ($row11=mysqli_fetch_array($q)) {
                    if ($adminData['admin_type']==1 || $row11['block_id']==0 ||  count($blockAryAccess)==0 ||  in_array($row11['block_id'], $blockAryAccess)) {
                 ?>
                  <option <?php if($_GET['id']==$row11['balancesheet_id']) { echo 'selected';} ?> value="<?php echo $row11['balancesheet_id'];?>"><?php echo $row11['balancesheet_name'];?></option>
                  <?php } } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"></label>
              <input  class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <?php
                extract(array_map("test_input" , $_GET));
                $date=date_create($from);
                    $dateTo=date_create($toDate);
                    $from= date_format($date,"Y-m-d 00:00:00");
                    $toDate= date_format($dateTo,"Y-m-d 23:59:59");
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                  $i=1;
                if (isset($_GET['from'])) {
              ?>
              <div class="table-responsive">
                <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportBalancesheetWithoutBtn'; } else { echo 'exampleReportBalancesheet'; }?>" class="table table-bordered auto-index">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Income</th>
                            <th>Expense</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $i=1;
                           $q4=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id' AND income_amount!=0 AND expenses_add_date BETWEEN '$from' AND '$toDate' ","ORDER BY expenses_balance_sheet_id DESC");

                           $q5=$d->select("event_attend_list,unit_master,block_master,users_master,floors_master","floors_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=event_attend_list.user_id AND unit_master.unit_id=event_attend_list.unit_id AND unit_master.block_id=block_master.block_id AND  event_attend_list.recived_amount!='0.00' AND event_attend_list.society_id='$society_id' AND  event_attend_list.balancesheet_id='$balancesheet_id' AND  event_attend_list.payment_received_date BETWEEN '$from' AND '$toDate'","");

                           $q6=$d->select("penalty_master,unit_master,users_master,block_master,floors_master","floors_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND block_master.block_id=unit_master.block_id and unit_master.unit_id=penalty_master.unit_id AND users_master.user_id=penalty_master.user_id AND  penalty_master.penalty_amount!='0.00' AND penalty_master.society_id='$society_id' AND  penalty_master.balancesheet_id='$balancesheet_id' AND penalty_master.paid_status=1 AND penalty_master.penalty_receive_date BETWEEN '$from' AND '$toDate' ","");

                           $qe=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id' AND expenses_amount!=0 AND expenses_add_date BETWEEN '$from' AND '$toDate' ","");

                          if (mysqli_num_rows($q5) > 0) {
                          while ($eventData=mysqli_fetch_array($q5)) {
                            $eq=$d->select("event_master","event_id='$eventData[event_id]'");
                            $evenName=mysqli_fetch_array($eq);
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $evenName['event_title']; ?> Event Booking (<?php echo $eventData['floor_name']; ?>-<?php echo $eventData['block_name']; ?>)</td>
                            <td><?php echo date("Y-m-d", strtotime($eventData['payment_received_date'])); ?></td>
                            <td><?php echo number_format($eventData['recived_amount']-$eventData['transaction_charges'],2,'.',''); ?></td>
                             <td>-</td>
                          </tr>
                          <?php } }
                          $q3=$d->select("block_master,facilities_master,facilitybooking_master,users_master,floors_master","floors_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=facilitybooking_master.user_id AND facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.balancesheet_id='$balancesheet_id' AND facilitybooking_master.payment_status=0 AND facilitybooking_master.payment_received_date BETWEEN '$from' AND '$toDate' ","ORDER BY facilitybooking_master.booking_id DESC");
                           if (mysqli_num_rows($q3) > 0) {
                          while ($fData=mysqli_fetch_array($q3)) {
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $fData['facility_name']; ?> Facility (<?php echo $fData['floor_name'].'-'.$fData['block_name']; ?>)</td>
                            <td><?php echo date("Y-m-d", strtotime($fData['payment_received_date'])); ?></td>
                            <td><?php echo number_format($fData['receive_amount']-$fData['transaction_charges'],2,'.',''); ?></td>
                             <td>-</td>
                          </tr>
                          <?php  }}
                           if (mysqli_num_rows($q6) > 0) {
                          while ($penaltyData=mysqli_fetch_array($q6)) {
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $penaltyData['penalty_name']; ?> Penalty (<?php echo $penaltyData['floor_name'].'-'.$penaltyData['block_name']; ?>)</td>
                            <td><?php echo date("Y-m-d", strtotime($penaltyData['penalty_receive_date'])); ?></td>
                            <td><?php echo $penaltyData['penalty_amount']; ?></td>
                             <td>-</td>
                          </tr>
                          <?php  }}
                           if (mysqli_num_rows($q4) > 0) {
                          while ($incomeData=mysqli_fetch_array($q4)) {
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $incomeData['expenses_title']; ?> Income</td>
                            <td><?php echo date("Y-m-d", strtotime($incomeData['expenses_add_date'])); ?></td>
                            <td><?php echo $incomeData['income_amount']; ?></td>
                             <td>-</td>
                          </tr>
                          <?php  }}
                           while ($sData=mysqli_fetch_array($qe)) {
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $sData['expenses_title']; ?></td>
                            <td><?php echo date("Y-m-d", strtotime($sData['expenses_add_date'])); ?></td>
                             <td>-</td>
                            <td><?php echo $sData['expenses_amount'];; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
              </div>
            <?php } else { ?>
                 <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select Date</span>
                </div>
            <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->