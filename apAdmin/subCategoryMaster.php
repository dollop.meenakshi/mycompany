  <?php error_reporting(0);
  $vId = (int)$_REQUEST['vId'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-6 col-12">
          <h4 class="page-title">Sub Category Master</h4>
        </div>
        <div class="col-sm-6 col-12 ">
            <div class="btn-group float-sm-right">
                <a href="addSubCategoryBtn" data-toggle="modal" data-target="#addModal" onclick="buttonSettingForSubCat()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                  <a href="javascript:void(0)" onclick="DeleteAll('deleteSubCategory');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#bulkUpload" class="btn  mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Bulk Add </a>
            </div>
        </div>
      </div>
      <form action="">
        <div class="row pt-2 pb-2">
        <!--   <div class="col-lg-4 col-6">
            <label>Vendor</label>
            <select type="text" class="form-control single-select " onchange="this.form.submit()" id="vId" name="vId">
              <option value="">-- Select --</option>
              <?php
              $floor = $d->select("local_service_provider_users", " service_provider_status=0 AND service_provider_delete_status=0 AND society_id = $society_id");
              while ($floorData = mysqli_fetch_array($floor)) {
              ?>
                <option <?php if (isset($vId) && $vId == $floorData['service_provider_users_id']) {
                          echo "selected";
                        } ?> value="<?php if (isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] != "") { echo $floorData['service_provider_users_id'];} ?>"><?php if (isset($floorData['service_provider_name']) && $floorData['service_provider_name'] != "") {echo $floorData['service_provider_name'];} ?></option>
              <?php } ?>
            </select>
          </div> -->
        </div>
      </form>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">

              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sr.No</th>
                    <th> Category Name</th>
                    <th>Sub Category Name</th>
                    <th>Vendors</th>
                    <th>Date</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody id="showFilterData">
                  <?php
                  $i = 1;
                  if (isset($vId) && $vId > 0) {
                    $vendorFilterQuery = " AND product_sub_category_master.vendor_id='$vId'";
                  }
                  $q = $d->selectRow('product_sub_category_master.*,product_category_master.product_category_id,product_category_master.category_name', "product_sub_category_master,product_category_master", "product_sub_category_master.product_category_id = product_category_master.product_category_id AND product_sub_category_master.society_id='$society_id'  AND product_sub_category_delete= 0 ");
                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                  ?>
                    <tr>
                      <td class="text-center">
                        <?php $totalVariant = $d->count_data_direct("product_sub_category_vendor_id", "product_sub_category_vendor_master", "product_sub_category_id='$data[product_sub_category_id]'");
                        if ($totalVariant == 0) { ?>
                          <input type="hidden" name="id" id="id" value="<?php echo $data['product_sub_category_id']; ?>">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['product_sub_category_id']; ?>">
                        <?php } ?>
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['category_name']; ?></td>
                      <td><?php echo $data['sub_category_name']; ?></td>
                      <td>
                        <a href="addproductCategoryBtn" data-toggle="modal" data-target="#addVendorSubCatModal" onclick="addSubCatvendorModal(<?php echo $data['product_category_id']; ?>)" class="btn mr-1 btn-sm btn-info waves-effect waves-light"><i class="fa fa-plus mr-1"></i> 
                        </a>  
                        <?php if( $totalVariant>0) {?>
                          <a href="productVendorSubCatMaster?scId=<?php echo $data['product_sub_category_id']; ?>"><?php echo $totalVariant; ?></a>
                          <?php } ?>
                           <!--
                          else{ ?><a class="btn btn-sm btn-info" href="productVendorSubCatMaster?scId=<?php echo $data['product_sub_category_id']; ?>"><i class="fa fa-plus"></i></a><?php //} ?> -->
                      </td>

                      <td><?php echo date("d M Y h:i A", strtotime($data['product_sub_category_created_date'])); ?></td>
                      <td>
                        <div class="d-flex align-items-center">
                          <form method="post" accept-charset="utf-8">
                            <input type="hidden" name="product_sub_category_id" value="<?php echo $data['product_sub_category_id']; ?>">
                            <input type="hidden" name="edit_product_sub_category" value="edit_product_sub_category">
                            <button type="button" class="btn btn-sm btn-primary mr-1" onclick="subCategorySetData(<?php echo $data['product_sub_category_id']; ?>)" data-toggle="modal" data-target="#addModal"> <i class="fa fa-pencil"></i></button>
                          </form>
                          <?php if ($data['product_sub_category_status'] == "0") {
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['product_sub_category_id']; ?>','subCategoryStatusDeactive');" data-size="small" />
                          <?php } else { ?>
                          <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['product_sub_category_id']; ?>','subCategoryStatusActive');" data-size="small" />
                          <?php } ?>
                          <button type="button" class="btn btn-sm btn-primary ml-1" onclick="subCategoryShowDetails(<?php echo $data['product_sub_category_id']; ?>)" data-toggle="modal" data-target="#subCategoryDetailModel"><i class="fa fa-eye"></i></button>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->

  </div>
  <!--End content-wrapper-->

  <div class="modal fade" id="subCategoryDetailModel">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Sub Category Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="subCategoryDetailModelDiv" style="align-content: center;">
        </div>
      </div>
    </div>
  </div>



  <div class="modal fade" id="addModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Sub Category</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="billPayDiv" style="align-content: center;">
          <div class="card-body">
            <form id="addsubCategoryForm" action="controller/subCategoryMasterController.php" enctype="multipart/form-data" method="post">
             
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Category <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <select name="product_category_id" id="product_category_id" class="form-control single-select frmInputSl">
                  <option value="">Select Category</option>
                    <?php 
                     $q = $d->selectRow('product_category_master.*', "product_category_master", "product_category_master.society_id='$society_id'  AND product_category_master.product_category_delete= 0 ");
                     $counter = 1;
                     while ($data = mysqli_fetch_array($q)) {
                    ?>
                    <option value="<?php echo $data['product_category_id']; ?>"><?php ?><?php echo $data['category_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Sub Category Name <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <input type="text" name="sub_category_name" id="sub_category_name" class="form-control restFrm">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Sub-Category Description</label>
                <div class="col-lg-8 col-md-8" id="">
                  <textarea name="sub_category_description" id="sub_category_description" class="form-control restFrm"></textarea>
                </div>
              </div>
              <div class="form-footer text-center">
                <input type="hidden" id="product_sub_category_id" name="product_sub_category_id" value="">
                <input type="hidden" id="ven_id" name="ven_id" value="">
                <input type="hidden" id="cat_id" name="cat_id" value="">
                <button id="addSubCategoryBtn" name="addSubCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                <input type="hidden" name="addSubCategory" value="addSubCategory">

                <button id="addSubCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

                <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addsubCategoryForm');"><i class="fa fa-check-square-o"></i> Reset</button>

              </div>

            </form>

          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="addVendorSubCatModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Sub Category</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="billPayDiv" style="align-content: center;">
          <div class="card-body">
          <form id="addproductVendorCategoryForm" action="controller/subCategoryMasterController.php" enctype="multipart/form-data" method="post">
              <div class="addSubCatvendor"></div>

            </form>

          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="bulkUpload">
    <div class="modal-dialog">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white"><?php echo $xml->string->import; ?> <?php echo $xml->string->bulk; ?> <?php echo $xml->string->parking; ?></h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="" action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 1 -> <?php echo $xml->string->formatted_csv; ?> <a href="controller/bulkUploadController.php?ExportProductSubCategory=ExportProductSubCategory&&csrf=<?php echo $_SESSION["token"]; ?>" name="ExportProductSubCategory" value="ExportProductSubCategory" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</a></label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 2 -> <?php echo $xml->string->fill_your_data; ?> </label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 3 -> <?php echo $xml->string->import_file; ?></label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 4 -> <?php echo $xml->string->click_upload_btn; ?></label>
              <label for="input-10" class="col-sm-12 col-form-label text-danger"> NOTE: DON'T CHANGE COLUMN FROM CSV </label>
            </div>
          </form>
          <form id="importValidation" action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->import; ?> CSV <?php echo $xml->string->file; ?> <span class="required">*</span></label>
              <div class="col-sm-8" id="PaybleAmount">
                <input required="" type="file" name="file" accept=".csv" class="form-control-file border">
              </div>
            </div>

            <div class="form-footer text-center">
              <input type="hidden" name="importSubProductCategory" value="importSubProductCategory">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }

    function buttonSettingForSubCat() {
      $('.hideupdate').hide();
      $('.hideAdd').show();
      $('#product_sub_category_id').val('');
      $('#product_category_id').attr('disabled',false);
      $('#cat_id').val('');
      $('.restFrm').val('');
      $('.frmInputSl').val('');
      $('.frmInputSl').select2();

    }
  </script>
  <style>
    .hideupdate {
      display: none;
    }
  </style>