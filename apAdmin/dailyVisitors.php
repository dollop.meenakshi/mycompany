<?php 
 
extract(array_map("test_input" , $_REQUEST));

if(isset($visitor_id) && isset($manageVisitor) && $manageVisitor=="yes") { 
if (filter_var($_GET['visitor_id'], FILTER_VALIDATE_INT) != true || (isset($_GET['visitor_id']) && filter_var($_GET['visitor_id'], FILTER_VALIDATE_INT) != true)) {
  $_SESSION['msg1']="Please Select Visitor";
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='dailyVisitors';
      </script>");
  exit(); 
}
$id= (int)$visitor_id;
  $q=$d->select("daily_visitors_master","visitor_id=$id");

if (mysqli_num_rows($q)==0) {
  $_SESSION['msg1']="Invalid Url";
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='dailyVisitors';
      </script>");
  exit(); 
}

  $data=mysqli_fetch_array($q);

?>
<link href="assets/plugins/fullcalendar/css/fullcalendar.min.css" rel='stylesheet'/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Manage Daily Visitor Working For <?php echo $xml->string->units; ?></h4>
      
     </div>
     
     </div>
    <!-- End Breadcrumb-->
    
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
               
     		  <?php 
                $edit_id =  $visitor_id;
                   $q=$d->select("daily_visitor_unit_master","daily_visitor_id='$visitor_id'");
                   $selected_user_ids = array();
                  while ( $daily_visitor_data=mysqli_fetch_array($q)) {
                    $selected_user_ids[] = $daily_visitor_data['user_id'];
                  }
                 ?>
                  
                <div class="form-group row">
                  <div class="col-sm-3 text-center">
                    <h5 class="text-capitalize"><?php echo $data['visitor_name'];?></h5>
                    <a href="../img/visitor/<?php echo $data['visitor_profile'];?>" data-fancybox="images" data-caption="Photo Name : Profile Photo"> 
                    <img  onerror="this.src='img/user.png'" height="180" width="180" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile'];?>" class="lazyload" alt="">
                    </a>

                    <ul  class="list-inline text-center mt-3">

                       <li class="list-inline-item">
                           <a title="Edit"  onclick="getDailyVisitorEdit('<?php echo $visitor_id; ?>');" data-toggle="modal" data-target="#updateDailyVisitorModal" href="javascript:void();" > <span class="btn btn-primary btn-sm" ><i class="fa fa-pencil"></i></span></a>
                      </li>
                      <li class="list-inline-item">
                          <?php   $cnt= $d->count_data_direct("daily_visitor_id","daily_visitor_unit_master,users_master"," daily_visitor_unit_master.daily_visitor_id='$visitor_id' and daily_visitor_unit_master.society_id='$society_id' AND daily_visitor_unit_master.user_id =users_master.user_id"); 
                          if( $cnt == 0 ) {?>
                            <form action="controller/dailyVisitorController.php" method="post">    
                              <input type="hidden" name="visitor_id" value="<?php echo $visitor_id; ?>">    
                              <input type="hidden" name="deletDailyVisitor" value="deletDailyVisitor">                 
                              <input type="hidden" name="deleteVisitorName" value="<?php echo $data['visitor_name'];?>">                 
                              <button type="submit" name="" class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o fa-lg"> </i></button>
                            </form>

                          <?php } else {?>  <button type="butto" name="" class="btn btn-danger btn-sm " onclick="swal('Please Delete <?php echo $xml->string->unit; ?> First...!');" ><i class="fa fa-trash-o fa-lg"> </i></button> <?php }?>
                      </li>
                    </ul>
                  
                  </div>  
                  <div  class="col-sm-9 ">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                     
                      <tbody>
                        <tr>
                          <th>Name</th>
                          <td><?php echo $data['visitor_name'];?></td>
                        </tr>
                       
                        <tr>
                          <th><?php echo $xml->string->mobile_no; ?></th>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile'];?></td>
                        </tr>
                       
                        <tr>
                          <th>Working <?php echo $xml->string->units; ?>:</th>
                          <td><?php 
                          $uq=$d->select("unit_master,block_master,users_master,daily_visitor_unit_master","users_master.delete_status=0 AND daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
                      daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$id' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");
                       echo mysqli_num_rows($uq); ?></td>
                        </tr>
                       
                        <?php if( $data['visitor_id_proof']!='' || $data['visitor_id_proof_back']!='') { ?>
                         <tr>
                          <th>View ID Proof</th>
                          <td>
                            <?php 

                            if( $data['visitor_id_proof']!='' && $data['visitor_id_proof_back']!='') { ?>
                             <a title="View ID Proof" target="_blank" href="../img/visitor/<?php echo $data['visitor_id_proof'];?>" name="Id Proof" class="btn btn-warning btn-sm">Front Side</a>
                             <a title="View ID Proof" target="_blank" href="../img/visitor/<?php echo $data['visitor_id_proof_back'];?>" name="Id Proof" class="btn btn-warning btn-sm">Back Side</a>
                             
                            <?php } else  if( $data['visitor_id_proof']!='') { ?>
                              <a title="View ID Proof" target="_blank" href="../img/visitor/<?php echo $data['visitor_id_proof'];?>" name="Id Proof" class="btn btn-warning btn-sm">View</a>
                               <?php } else  if( $data['visitor_id_proof_back']!='') { ?>
                               <a title="View ID Proof" target="_blank" href="../img/visitor/<?php echo $data['visitor_id_proof_back'];?>" name="Id Proof" class="btn btn-warning btn-sm">View</a>
                              <?php } ?>
                            

                          </td>
                        </tr>
                      <?php  } ?>
                        <tr>
                          <th>Print I-Card</th>
                          <td> 
                          <a title="Print I-Card" href="printIcardSingle.php?visitor_id=<?php echo $visitor_id; ?>" class="btn btn-info btn-sm"><i class="fa fa-print"></i> Print I-Card</a>
                          </td>
                        </tr>

                         <tr>
                          <th>Valid Till</th>
                          <td> 
                            <?php echo date("d-m-Y", strtotime($data['valid_till']));?>
                          </td>
                        </tr>
                          <tr>
                          <th>Visit Company</th>
                          <td> 
                            <?php $fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                              $vistLogo=mysqli_fetch_array($fd);
                              if ($vistLogo['visitor_sub_type_name']=="Other") {
                                echo $vistLogo['visitor_sub_type_name'].'-'.$data['visit_from'];
                              }else {
                                echo $vistLogo['visitor_sub_type_name'];
                              }
                              if ($vistLogo['visitor_sub_image']!='') { ?>
                                <img width="50"  src="../img/visitor_company/<?php echo $vistLogo['visitor_sub_image'];?>" alt="">
                                 
                              <?php }
                              ?>
                          </td>
                        </tr>
                       
                        
                        
                      </tbody>
                    </table>

                    </div>
                  </div>
                 
                   
                </div>


              
                 
               <form  id="personal-info" action="controller/dailyVisitorController.php" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="daily_visitor_id" value="<?php echo $visitor_id;?>">
                 <input type="hidden" name="visitor_name" value="<?php echo $data['visitor_name'];?>">
                 <input type="hidden" name="valid_till" value="<?php echo $data['valid_till'];?>">
                 <input type="hidden" name="in_time" value="<?php echo $data['in_time'];?>">
                 <input type="hidden" name="out_time" value="<?php echo $data['out_time'];?>">
                 <input type="hidden" name="week_days" value="<?php echo $data['week_days'];?>">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label  class="col-form-label">Manage <?php echo $xml->string->units; ?> For This Daily Visitor</label>
                  </div>
                  <div class="col-sm-3">
                    <select name="block_id" id="block_id" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
                      <option value="">-- Select Branch --</option>
                      <?php 
                          $bd=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                          while ($blockData=mysqli_fetch_array($bd)) {
                        ?>
                      <option value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?> </option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <select name="floor_id" id="floor_id" class="form-control single-select" required="" onchange="getVisitorUserByFloor(this.value)">
                      <option value="">-- Select Department --</option>
                   		
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <select  multiple="multiple" name="user_data[]" id="user_id" class="form-control multiple-select" required="" >
                      <option value="">-- Select User --</option>
                       <?php 
                   		$ids = join("','",$selected_user_ids);   
                     
                      /* $q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  users_master.user_id  NOT IN ('$ids') $blockAppendQueryUser AND users_master.member_status=0"," ORDER BY block_master.block_sort ASC");
                    while ($blockRow=mysqli_fetch_array($q3)) {
                    	 */
                      ?>
                      <!-- <option value="<?php echo $blockRow['unit_id'].'~'.$blockRow['user_id'].'~'.$blockRow['user_type'];?>"> <?php echo $blockRow['user_full_name'];?> (<?php echo $blockRow['user_designation'];?>-<?php echo $blockRow['block_name'];?> ) </option> -->
                    <?php //}?>


                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <button type="submit" id="dailyVisitiorUserBtn" class="btn btn-success" name="dailyVisitiorUserBtn"><i class="fa fa-check-square-o"></i> Add</button>
                  </div>
                </div>
          			</form>


               
               
                   
                  

             
            </div>
          </div>
        </div>
      </div><!--End Row-->
       <?php 
       
        if(mysqli_num_rows($uq)>0) { ?>

     <div class="row">
     	 <div class="col-12 col-lg-12">
		    <div class="card border border-info floorList">
		      <div class="card-body">
		        <div class="media align-items-center">
		          
		         
		        <div class="media-body p-1">
		          <div class="row">
		           <?php
		            while ($unitData=mysqli_fetch_array($uq)) { 
		                extract($unitData);
		                 $qqname= $d->selectRow("user_full_name,user_type,user_designation","users_master,daily_visitor_unit_master","users_master.delete_status=0 AND daily_visitor_unit_master.user_id=users_master.user_id AND daily_visitor_unit_master.unit_id='$unitData[unit_id]' AND users_master.member_status=0","ORDER BY users_master.user_id DESC");
                    $userData=mysqli_fetch_array($qqname);
                    $primaryName= $userData['user_full_name'];
		              ?>
		            <div class="col-6 col-lg-2 col-xl-2">
		              <div class="card gradient-scooter no-bottom-margin">
		                <div class="card-body text-center">
		                <h6 class="text-white mt-2"><?php echo $block_name; ?>
		                <!-- <i title="Delete Unit" class="fa fa-trash-o pull-right" onclick="DeleteUnit('<?php echo $unitData['unit_id']; ?>');"></i> -->

		                 <br><?php if ($primaryName!="") {
		                        echo $primaryName;
		                      } else {
		                        echo $unitData['user_full_name']; 
		                      } ?><br>
		                    <?php echo '('.$unitData['user_designation'].')';  ?></h6>
		                    <div class="row">
		                    		<?php
		                    	  	if ($adminData['admin_type']==1 || count($blockAryAccess)==0 || in_array($unitData['block_id'], $blockAryAccess) && count($blockAryAccess)>0) { ?>
		                    	  <div class="col-6">
		                    	  	   <a title="Notification Users"  onclick="getNotificationDataDaily('<?php echo $unit_id; ?>','<?php echo $user_id; ?>','<?php echo $visitor_id; ?>');" data-toggle="modal" data-target="#notificationModal" href="javascript:void();" > <span class="btn btn-warning btn-sm  "  ><i class="fa fa-bell"></i></span></a>
		                    	  </div>
		                    	  <div class="col-6">
		                    	  	 <form action="controller/dailyVisitorController.php" method="post" >
                                  <input type="hidden" name="daily_visitor_unit_id" value="<?php echo $daily_visitor_unit_id; ?>">
				                        	<input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
				                         <input type="hidden" id="daily_visitor_id" name="visitor_id" value="<?php echo $daily_visitor_id; ?>">
				                          <input type="hidden" name="manageVisitor" value="yes">
                                <input type="hidden" name="DailyVisitorDelete" value="DailyVisitorDelete">
				                        <input type="hidden" name="deleteVisitorName" value="<?php echo $data['visitor_name'];?>">
                                <input type="hidden" name="unitRemoveName" value="<?php echo $block_name; ?>-<?php echo $unitData['unit_name']; ?> ">
				                        <button type="submit" class="form-btn btn btn-sm btn-danger " title="Delete"> <i class="fa fa-trash-o"></i> </button>
				                        
				                      </form>
		                    	  </div>
		                    	  <?php } else if(count($blockAryAccess)==0) { ?>
		                    	  	<div class="col-6">
		                    	  	   <a title="Notification Members"  onclick="getNotificationDataDaily('<?php echo $unit_id; ?>','<?php echo $user_id; ?>','<?php echo $visitor_id; ?>');" data-toggle="modal" data-target="#notificationModal" href="javascript:void();" > <span class="btn btn-warning btn-sm  "  ><i class="fa fa-bell"></i></span></a>
		                    	  </div>
		                    	  <div class="col-6">
		                    	  	 <form action="controller/dailyVisitorController.php" method="post" >
				                        	<input type="hidden" name="daily_visitor_unit_id" value="<?php echo $daily_visitor_unit_id; ?>">
                                  <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
				                         <input type="hidden" name="visitor_id" value="<?php echo $daily_visitor_id; ?>">
				                          <input type="hidden" name="manageVisitor" value="yes">
				                        <input type="hidden" name="DailyVisitorDelete" value="DailyVisitorDelete">
                                 <input type="hidden" name="deleteVisitorName" value="<?php echo $data['visitor_name'];?>">
                                 <input type="hidden" name="unitRemoveName" value="<?php echo $block_name; ?>-<?php echo $unitData['unit_name']; ?> ">
				                        <button type="submit" class="form-btn btn btn-sm btn-danger " title="Delete"> <i class="fa fa-trash-o"></i> </button>
				                        
				                      </form>
		                    	  </div>
		                    	  <?php } ?>
		                    </div>
		                   
		              </div>
		              </div>
		            </div>
		           
		            <?php }   ?>
		            
		          </div>

		        </div>
		      </div>
		     </div>
		   </div>
		  </div>
     </div>
	 <?php }  ?>
      <input type="hidden" id="todayDate" value="<?php echo date("Y-m-d"); ?>" name="">
       <?php 
       $statrDate =  date("Y-m-d", strtotime("-3 month"));
       $startMonth =  date("Y-m", strtotime("-3 month"));
       $viewEmployeeAttandace = 'viewEmployeeAttandace'; ?>
      <?php 
      if ($_GET['monthFilter'] !="") {
        $monthFilter = $_GET['monthFilter'];
        $timestamp    = strtotime($monthFilter);
        $from = date('Y-m-01', $timestamp);
        $toDate  = date('Y-m-t', $timestamp);
      } else {
        $from = date("Y-m-")."01";
        $toDate = date("Y-m-d");
        $cMonth = date("F-Y");
        $monthFilter = date("F-Y");
      }
      // $visitorAttandaceArray =array();
      // $q = $d->select("visitors_master","daily_visitor_id='$id' AND visit_date>$statrDate","GROUP BY visit_date ORDER BY visitor_id DESC ");
      // while ($data=mysqli_fetch_array($q)) {
      //   array_push($visitorAttandaceArray, $data);
      // }
      ?>
      <form action="" method="get" >
      <h6>Attendance Month 
        <input type="hidden" name="manageVisitor" value="yes">
        <input type="hidden" name="visitor_id" value="<?php echo $visitor_id; ?>">
        <select onchange="this.form.submit()" name="monthFilter">
            <?php $i = 1;
              $month = strtotime($statrDate);
              while($i <= 4)
              {
                $month_name = date('F-Y', $month);
                $monthOnlty = date('m', $month);
                $totalDaysMonth =   date('t', strtotime($month_name));
                $firstDate = date('Y-m-01', strtotime($month_name));
                $lastDate = date('Y-m-t', strtotime($month_name));
                ?>
                <option <?php if($monthFilter=="" && $cMonth==$month_name) { echo 'selected'; } else if($monthFilter==$month_name){  echo 'selected'; } ?> value="<?php echo $month_name; ?>"><?php echo $month_name; ?></option>
               <?php
                $month = strtotime('+1 month', $month);
                $i++; 

                }?>
        </select>
      </h6> 
      </form>
    
     <div class="row">
      <div class="col-lg-12 pt-4">
        <div class="card border border-info floorList">
          <div class="card-body table-responsive">
            <?php 
             $totalDaysinMonth = $lastDate = date('t', strtotime($monthFilter));
             if ($monthFilter=="") {
               $monthFilter = date('F-Y');
             }
             ?>
          <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>In Out Time</th>
                </tr>
            </thead>
            <tbody>
              <?php 
              $i3= 1;
             for ($iMonth=1; $iMonth <= $totalDaysinMonth; $iMonth++) { 
                $viDate = date("Y-m-d", strtotime($iMonth.'-'.$monthFilter)); 
               ?>
                <tr>
                   
                    <td><?php echo $i3++; ?></td>
                    <td><?php echo $iMonth.'-'.date("M-Y", strtotime($iMonth.'-'.$monthFilter)) ?></td>
                    <td><?php 
                      $entryI= 1;
                      $qs = $d->select("visitors_master","daily_visitor_id='$id' AND visit_date='$viDate'","ORDER BY visitor_id ASC ");
                      
                      while ($visitData=mysqli_fetch_array($qs)){
                        echo "(".$entryI++.")";
                          if ($visitData['visit_date']!="") {
                            echo "<i class='text-success'>IN :".  date("h:i A", strtotime($visitData['visit_time'])).'</i>';
                            echo "<br>";
                          }

                          if ($visitData['exit_date']!="") { 
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;<i class='text-danger'>OUT :". date("h:i A", strtotime($visitData['exit_time'])); 
                            if (date("Y-m-d", strtotime($visitData['exit_date'])) != $viDate) {
                              echo " (". date("d-M", strtotime($visitData['exit_date'])).')'; 
                            }
                            echo "</i><br>";
                          }
                      }

                    ?></td>
                   
                    
                </tr>
              <?php } ?>
            </tbody>  
            
        </table>
        </div>
      </div>
      </div>
    </div>
      
    <!-- End container-fluid-->
    <div class="row">
      <div class="col-lg-12 pt-4">
        <div class="card table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Month</th>
                <th>Working</th>
                <th>Absent</th>
                <th>Total Days</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 1;
              $month = strtotime($statrDate);
              $curnetMonthName = date("F-Y");
              while($i <= 4)
              {
                  $monthOnlty = date('m', $month);
                  $month_name = date('F-Y', $month);
                  if ($month_name==$curnetMonthName) {
                    $totalDaysMonth =   date('d');
                  } else {
                    $totalDaysMonth =   date('t', strtotime($month_name));
                  }
                  $firstDate = date('Y-m-01', strtotime($month_name));
                  $lastDate = date('Y-m-t', strtotime($month_name));
               ?>
              <tr>
                <td><?php echo $month_name;
                  $month = strtotime('+1 month', $month);
                  $i++; ?></td>
                <td><?php 
                $vCount = $d->select("visitors_master","daily_visitor_id='$id' AND visit_date BETWEEN '$firstDate' AND '$lastDate' ","GROUP BY visit_date ORDER BY visitor_id DESC ");
                echo $totalWorkingdays=  mysqli_num_rows($vCount);
                  ?>
                </td>
                <td><?php echo $totalDaysMonth-$totalWorkingdays; ?></td>
                <td><?php  echo $totalDaysMonth; ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>


    </div>
    <!-- End container-fluid-->
     <div class="modal fade" id="notificationModal">
      <div class="modal-dialog ">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Manage In/Out Notification</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="NotificationListDiv" style="align-content: center;">
            

          </div>

        </div>
      </div>
    </div>
    </div><!--End content-wrapper-->
 <?php } else {
?>
<style type="text/css" media="screen">
  .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 35px;
  width: 25px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
</style>

<?php 
$notiArrayId= array();
$qn = $d->select("employee_in_out_notification","admin_id='$_COOKIE[bms_admin_id]' AND empType=1");
while ($notiData=mysqli_fetch_array($qn)) {
  array_push($notiArrayId, $notiData['emp_id']);
}

?>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row pt-2  pb-2">
			<div class="col-sm-9 col-5">
				<h4 class="page-title">Daily Visitors</h4>
			</div>
			<div class="col-sm-3 col-7 text-right">
				<div class=" float-sm-right">
					<button class="btn  btn-sm btn-success " data-toggle="modal" data-target="#addModal"><i class="fa fa-plus mr-1"></i> Add New</button>	
					<a href="printIcard.php?allDaily=allDaily" class="btn btn-warning btn-sm"><i class="fa fa-address-card-o"></i> I-Card</a>
				</div>
			</div>
		</div>


		<div class="row"><!-- start view -->
			<?php 
			$query=$d->select("daily_visitors_master"," society_id='$society_id' and (active_status = 0 or active_status = 1 ) order by visitor_name ASC ","");
      if (mysqli_num_rows($query) >0) {
			while ($response=mysqli_fetch_array($query)) {
				extract($response); 

				 $fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$response[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            $visit_company = $vistLogo['visitor_sub_type_name'].'';
                        } else {
                            $visit_logo="";
                        }
				?>
				<div class="col-md-3 col-6">
					<div class="card radius-15 <?php echo $color_status; ?>">
          <div class="p-2 text-center">
            <div class="p-2 border radius-15">
              <a href="dailyVisitors?visitor_id=<?php echo $visitor_id; ?>&manageVisitor=yes">
              <img onerror="this.src='img/user.png'"  src="img/user.png"  data-src="../img/visitor/<?php echo $visitor_profile; ?>" width="110" height="110" class="rounded-circle shadow lazyload" alt="">
              </a>
              <h6 class="mb-0 mt-3"><?php echo custom_echo($response['visitor_name'],25); ?></h6>
              <p class="mb-1"><span class='badge badge-success m-1'><?php echo $visit_company; ?></span></p>

              <p class="mb-1"><?php echo $xml->string->in_out; ?> <?php echo $xml->string->notifications; ?>
                <span class='empSpan<?php echo $visitor_id;?>'>
                <?php
                 if(in_array($visitor_id, $notiArrayId)){  ?>
                 <a href="javascript:void(0)"  onclick="notGetEmpNotificationDaily('<?php echo $visitor_id;?>');" > <i class="fa fa-bell" aria-hidden="true"></i> </a>
                <?php } else { ?>
                  <a href="javascript:void(0)"  onclick="getEmpNotificationDaily('<?php echo $visitor_id;?>');" > <i class="fa fa-bell-slash" aria-hidden="true"></i> </a>
                <?php } ?>
                </span>
              </p>
              <!-- <div class="list-inline contacts-social mt-3 mb-3"> <a href="javascript:;" class="list-inline-item bg-facebook text-white border-0"><i class="bx bxl-facebook"></i></a>
               
              </div> -->
              <div class="d-grid"> <a href="dailyVisitors?visitor_id=<?php echo $visitor_id; ?>&manageVisitor=yes" class="btn btn-sm btn-outline-danger radius-15">View Details</a>
              </div>
            </div>
          </div>
        </div>
				</div>
			<?php } } else {
        echo "<img src='img/no_data_found.png'>";

      } ?>
		</div><!-- end view -->



 
 

		
<?php } ?>


<div class="modal fade" id="updateDailyVisitorModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Edit Daily Visitor</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"  style="align-content: center;">
             <form id="editDailyVisitorFrm" action="controller/dailyVisitorController.php" method="post" enctype="multipart/form-data">
              
              <span id="updateDailyVisitorModalDiv"></span>
              <div class="form-footer text-center">
                <input type="hidden" name="editDailyVisitorBtn" value="editDailyVisitorBtn">
              <button type="submit" name="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
              </div>
             </form>

          </div>

        </div>
      </div>
    </div>



    <div class="modal fade" id="addModal"><!-- Edit Modal -->
      <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Add Daily Visitors</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="addDailyVisitorFrm" action="controller/dailyVisitorController.php" method="post" enctype="multipart/form-data">
              
               <div class="form-group row">
                   <div class="col-sm-12 text-center">
                    <label for="imgInp">
                       <img id="blah" style="border: 1px solid gray;" src="<?php if(isset($emp_profile)) { echo '../img/emp/'.$emp_profile; }  else { echo 'img/user.png'; } ?>"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="imgInp" type="file" name="visitor_profile">
                   </label>
                   </div>
              </div>
              <div class="form-group row">
                <label for="visitor_name" class="col-sm-2 col-form-label">Visitor Name <span class="required">*</span></label>
                <div class="col-sm-4" id="">

                  <input type="text" class="form-control text-capitalize" name="visitor_name" id="visitor_name" required="" >
                </div>
             
                <label for="visitor_mobile" class="col-sm-2 col-form-label">Visitor Mobile <span class="required">*</span></label>
                <div class="col-sm-2" id="">
                  <select name="country_code" class="form-control single-select" id="country_code_add1" required="">
                  <?php include 'country_code_option_list.php'; ?>
                  </select>
                </div>
                <div class="col-sm-2" id="">

                  <input type="text" maxlength="15" minlength="8" class="form-control onlyNumber" inputmode="numeric" name="visitor_mobile" id="visitor_mobile"     >
                </div>
              </div>
              
              <div class="form-group row">
                <label for="visit_from" class="col-sm-2 col-form-label">Visit Till <span class="required">*</span></label>
                <div class="col-sm-4" id=" ">
                  <input type="text" class="form-control valid-till" name="valid_till" id="" readonly="" value="<?php echo  date('Y-m-d', strtotime('+1 month'));?>" required="">
                </div>
             
                <label for="visit_from" class="col-sm-2 col-form-label">In/Out Time <span class="required">*</span></label>
                <div class="col-sm-2" id="">
                  <input type="text" class="form-control time-picker" name="in_time" id="sun"  placeholder="In Time" required="">
                </div>
                <div class="col-sm-2" id="">
                  <input type="text" class="form-control time-picker1" name="out_time" id="out_time" placeholder="Out Time" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="visit_from" class="col-sm-2 col-form-label">Week Days <span class="required">*</span></label>
                <div class="col-sm-4" id=" ">
                  <div class="weekDays-selector">
                    <input name="week_days[]" type="checkbox" id="weekday-mon" value="MON" class="weekday chk-day" />
                    <label for="weekday-mon">M</label>
                    <input name="week_days[]" type="checkbox" id="weekday-tue" value="TUE" class="weekday chk-day" />
                    <label for="weekday-tue">T</label>
                    <input name="week_days[]" type="checkbox" id="weekday-wed" value="WED" class="weekday chk-day" />
                    <label for="weekday-wed">W</label>
                    <input name="week_days[]" type="checkbox" id="weekday-thu" value="THU" class="weekday chk-day" />
                    <label for="weekday-thu">T</label>
                    <input name="week_days[]" type="checkbox" id="weekday-fri" value="FRI" class="weekday chk-day" />
                    <label for="weekday-fri">F</label>
                    <input name="week_days[]" type="checkbox" id="weekday-sat" value="SAT" class="weekday chk-day" />
                    <label for="weekday-sat">S</label>
                    <input name="week_days[]" type="checkbox" id="weekday-sun" value="SUN" class="weekday chk-day" />
                    <label for="weekday-sun">S</label>
                     <span id="chk_error"></span>
                  </div>
                </div>
            
                <label for="visit_from" class="col-sm-2 col-form-label">Visit Company <span class="required">*</span></label>
                <div class="col-sm-4" id=" ">
                   <select onchange="otherDailyCheck();" id="visitor_sub_type_id"  required="" class="form-control complain-select" name="visitor_sub_type_id" >
                          <option value="">Select Visitor Type</option>
                          <?php
                          $visitorMainType_qry = $d->select("visitorSubType","active_status=0 AND visitor_main_type_id=4");
                            //isset($complaint_category) &&
                          while($visitorMainType_data = mysqli_fetch_array($visitorMainType_qry)){ ?>
                            <option  value="<?php echo $visitorMainType_data['visitor_sub_type_id']; ?>"><?php echo $visitorMainType_data['visitor_sub_type_name']; ?></option>
                          <?php } ?>
                        </select>
                </div>
              </div>
              <div class="form-group row" id="otherCompany" style="display: none;">
                <label for="visit_from" class="col-sm-2 col-form-label">Other Company Name <span class="required">*</span></label>
                <div class="col-sm-4" id=" ">
                  <input type="text" class="form-control text-capitalize" name="visit_from" id="visit_from">
                </div>
              
              </div>
              <div class="form-group row">
                <label for="vehicle_number" class="col-sm-2 col-form-label">Vehicle Number</label>
                <div class="col-sm-4" id=" ">
                  <input type="text" class="form-control text-uppercase txtNumeric" name="vehicle_number" id="vehicle_number">
                </div>

              </div>

              <div class="form-group row">
                <label for="visitor_profile" class="col-sm-2 col-form-label">ID Proof </label>
                <div class="col-sm-10 table-responsive" style="display: inherit;">
                 <!--  <input type="file" class="form-control-file idProof" name="id_proof"   id="id_proof" >
                  (Max file size 4MB) -->
                    <label for="imgInpId">
                       <img id="idproof" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
                     <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="idInput idProof" id="imgInpId" type="file" name="id_proof"><br>
                     Front Side 
                   </label>
                     <label for="imgInpId1">
                       <img id="idproof1" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
                     <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="photoInput idProof" id="imgInpId1" type="file" name="visitor_id_proof_back">
                     <br>
                     Back Side 
                   </label> 
                </div>
              </div>

              <div class="form-footer text-center">
                <input type="hidden" name="addDailyVisitorBtn" value="addDailyVisitorBtn">
                <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--end modal -->
  </div>
</div>
    <script src="assets/js/jquery.min.js"></script>

 <script type="text/javascript">
      var fileExtension = ['jpeg', 'jpg', 'png', 'jpeg'];

  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

  function readURLId(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#idproof').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function readURLId1(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#idproof1').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#imgInp").change(function() {
  readURL(this);
});

$("#imgInpId").change(function() {
  var fileExt= $(this).val().split('.').pop().toLowerCase();
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    if(fileExt=='pdf') {
      $('#idproof').attr('src', 'img/pdf.png');
    } else  if(fileExt=='doc' || fileExt=='docx')  {
      $('#idproof').attr('src', 'img/doc.png');
    }
  } else {
    readURLId(this);
  }
  // readURLId(this);
  // alert('afd');
});

$("#imgInpId1").change(function() {
  var fileExt= $(this).val().split('.').pop().toLowerCase();
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    if(fileExt=='pdf') {
      $('#idproof1').attr('src', 'img/pdf.png');
    } else  if(fileExt=='doc' || fileExt=='docx')  {
      $('#idproof1').attr('src', 'img/doc.png');
    }
  } else {
    readURLId1(this);
  }
  // alert('afd');
});
</script>