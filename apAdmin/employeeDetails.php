<?php  
extract($_REQUEST);
$week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

if(filter_var($id, FILTER_VALIDATE_INT) != true){
  $_SESSION['msg1']='Invalid User';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='companyEmployees';
      </script>");
}
$user_id = $id;
$qq=$d->select("users_master,unit_master,block_master,floors_master ","users_master.user_id='$id' AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=floors_master.block_id AND floors_master.floor_id=unit_master.floor_id AND users_master.society_id='$society_id' $blockAppendQuery ");

$userData = $qq->fetch_assoc();
extract($userData);

$q1=$d->select("user_employment_details","user_id='$id'");
$proData=mysqli_fetch_array($q1);
                        
$empe_exp2=$d->select("employee_experience","user_id='$id' AND society_id='$society_id'");

$eaec=$d->select("employee_achievements_education","user_id='$id' AND society_id='$society_id'");
$expDataar = array();

if(mysqli_num_rows($empe_exp2)>0){
  while($tData=mysqli_fetch_assoc($empe_exp2)){
    array_push($expDataar,$tData);
  }
  array_multisort(array_column($expDataar, 'work_from'), SORT_ASC, $expDataar);
 
  $sdate = $expDataar[0]['work_from'];
  $edate = date('Y-m-d');
  $getExperianseYears = $d->getMonthDays($sdate,$edate);
  
}else
{
  if($proData['joining_date'] !="" && $proData['joining_date'] !="0000-00-00"){

    $sdate = $proData['joining_date'];
    $edate = date('Y-m-d');
    $getExperianseYears = $d->getMonthDays($sdate,$edate);
  }
 
}

error_reporting(0);
$userType =$unit_status;
$user_type =$user_type;
if ($member_status==0) {
  $userFamilyType = "Primary";
}else {
  $userFamilyType = $member_relation_name;

}


if ($user_status==0) {
  echo ("<script LANGUAGE='JavaScript'>
    window.location.href='pendingUser?id=$id';
    </script>");
  exit();
}

if ($user_type==1) {
  $qqexpire=$d->select("users_master","user_id='$id' AND society_id='$society_id' AND date(tenant_agreement_end_date) < CURDATE() AND tenant_agreement_end_date!='0000-00-00' ");
}

 $familyCount=$d->select("users_master","delete_status=0 AND user_status!=0 AND unit_id='$unit_id' AND member_status=1 AND society_id='$society_id' AND user_type='$user_type' AND parent_id='$user_id' AND users_master.user_id!='$id'");


 $iaq=$d->select("assets_item_detail_master,assets_category_master,assets_detail_inventory_master","assets_item_detail_master.assets_id=assets_detail_inventory_master.assets_id AND assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_detail_inventory_master.user_id='$id' AND assets_detail_inventory_master.society_id='$society_id' AND assets_detail_inventory_master.end_date='0000-00-00'");

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <?php if(mysqli_num_rows($qq)>0) { ?>
      <div class="row ">
        <div class="col-sm-9">
          <h4 class="page-title"><?php echo $floor_name; ?>-<?php echo $block_name; ?></h4>
        </div>
       
        <div class="col-sm-3">
          <div class="btn-group float-sm-right">
               <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#changeDepartmentModal" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-pencil-square-o"></i> Change Department </a> -->
               <a href="viewOwnerHistory?id=<?php echo $id; ?>"  class="btn btn-sm btn-warning waves-effect waves-light"><i class="fa fa-history mr-1"></i> <?php echo $xml->string->history; ?></a>

              
          </div>
        </div>
      </div>
      <div class="row ">
         <div class="col-sm-6">
         <?php if($proData['joining_date'] !="0000-00-00" && $proData['joining_date'] !=""){ ?>
           <label>employee Joined since :</label>
           <?php 
                    
                    $sdate = date('Y-m-d');
                   $edate = $proData['joining_date'];
                    $getMonthDays = $d->getMonthDays($sdate,$edate);
                    echo $getMonthDays;
                }
                  ?>
         
        </div>
        <?php if(isset($getExperianseYears) && $getExperianseYears !=""){ ?>
         <div class="col-sm-6">
         <label>Total Experience :</label>
         <?php 
          echo $getExperianseYears;
         ?>
        
        </div>
        <?php } ?>
      </div>
     
      <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="card profile-card-2">
            <div class="card-img-block">
              <?php if ($socieaty_cover_photo!="") { ?>
              <img onerror="this.src='img/31.jpg'"  class="img-fluid" src="../img/society/<?php echo $socieaty_cover_photo;?>" alt="Card image cap">
            <?php } else { ?>
              <img class="img-fluid" src="img/31.jpg" alt="Card image cap">
            <?php } ?>
            </div>
            <div class="card-body pt-5">
              <?php if(file_exists('../img/users/recident_profile/'.$user_profile_pic) && $user_profile_pic!="user_default.png" && $user_profile_pic!="user.png") { ?>
              <a href="../img/users/recident_profile/<?php echo $user_profile_pic;?>" data-fancybox="images" data-caption="Photo Name : Profile Photo"> 
              <img id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $user_profile_pic; ?>"  width="75" height="75"   src="#" alt="your image" class='profile img-thumbnail' /></a>
              <?php } else { 
                $shortChar=$d->get_sort_name($user_full_name); 
                ?>
              <div style="line-height: 60px; margin-left: .25rem !important;font-size: 25px;width: 75px;height: 75px;position: absolute;top: -36px;left: 15%;max-width: 75px;-webkit-transform: translate(-50%, 0%);transform: translate(-50%, 0%);border: 3px solid rgba(255, 255, 255, 1);" class="profile-image-name">
                <?php echo trim($shortChar); ?>
              </div>

              <?php }  ?>
             
              <h5 class="card-title"><?php echo $user_full_name; ?></h5>
              <h5 class="card-title"><?php echo $user_designation; ?></h5>
              <?php 
                $profile_progress = 0;
                if ($user_full_name!="") {
                  $profile_progress +="10";
                }
                if ($user_profile_pic!="" && $user_profile_pic!="user_default.png") {
                  $profile_progress +="10";
                }
                if ($user_mobile!="") {
                  $profile_progress +="10";
                }
                if ($gender!="") {
                  $profile_progress +="5";
                }
                if ($last_address!="") {
                  $profile_progress +="5";
                }
                if ($user_email!="") {
                  $profile_progress +="5";
                }
                if ($whatsapp_number>0) {
                  $profile_progress +="5";
                }
                if ($emergency_number>0) {
                  $profile_progress +="5";
                }
                if ($permanent_address!="") {
                  $profile_progress +="3";
                }
                if ($member_date_of_birth!="") {
                  $profile_progress +="3";
                }
                if ($nationality!="") {
                  $profile_progress +="3";
                }
                if ($alt_mobile>0) {
                  $profile_progress +="3";
                }
                if ($blood_group!="") {
                  $profile_progress +="3";
                }
                if ($user_designation!="") {
                  $profile_progress +="3";
                }
                if ($marital_status>0) {
                  $profile_progress +="3";
                }
                if ($total_family_members>0) {
                  $profile_progress +="2";
                }
                if ($facebook!="") {
                  $profile_progress +="2";
                }
                if ($instagram!="") {
                  $profile_progress +="2";
                }
                if ($linkedin!="") {
                  $profile_progress +="2";
                }
                if ($twitter!="") {
                  $profile_progress +="2";
                }
                if ($proData['job_location']!="") {
                  $profile_progress +="2";
                }
                if ($proData['intrest_hobbies']!="") {
                  $profile_progress +="2";
                }
                if ($proData['professional_skills']!="") {
                  $profile_progress +="4";
                }
                if ($proData['special_skills']!="") {
                  $profile_progress +="2";
                }
                if ($proData['language_known']!="") {
                  $profile_progress +="2";
                }
                if (mysqli_num_rows($eaec)>0) {
                  $profile_progress +="2";
                }
                
                if ($profile_progress>100) {
                  $profile_progress = "100";
                }
              ?>
              <div class="progress-wrapper">
                <div class="progress-content">
                  <span class="">Profile Completeness</span>
                  <span class="progress-percentage"><?php echo $profile_progress; ?>%</span>
                </div>
                <div class="progress mb-4">
                  <div class="progress-bar bg-primary" style="width:<?php echo $profile_progress; ?>%"></div>
                </div>
              </div>
              
              <!-- <div class="media align-items-center text-center">
                <?php if ($facebook!='' && filter_var($facebook, FILTER_VALIDATE_URL)){ ?>
                       <a target="_blank" href="<?php echo $facebook;?>"><i class="fa fa-facebook" title="Facebook"></i></a>
                <?php  } else if($facebook!='' ) { ?>
                   <a target="_blank" href="https://www.facebook.com/<?php echo $facebook;?>"><i class="fa fa-facebook" title="Facebook"></i></a>
                <?php } 
                if ($instagram!='' && filter_var($instagram, FILTER_VALIDATE_URL)){ ?>
                  <a target="_blank" href="<?php echo $instagram;?>"><i class="fa fa-instagram" title="Instagram"></i></a>
                <?php } else if($instagram!='') { ?>
                  <a target="_blank" href="https://www.instagram.com/<?php echo $instagram;?>"><i class="fa fa-instagram" title="Instagram"></i></a>
                <?php }
                 if ($linkedin!='' && filter_var($linkedin, FILTER_VALIDATE_URL)){ ?>
                  <a target="_blank" href="<?php echo $linkedin;?>"><i class="fa fa-linkedin" title="Linkedin"></i></a>
                <?php } else if($linkedin!="") { ?>
                  <a target="_blank" href="https://in.linkedin.com/<?php echo $linkedin;?>"><i class="fa fa-linkedin" title="Linkedin"></i></a>
                <?php } if ($twitter!='' && filter_var($linkedin, FILTER_VALIDATE_URL)){ ?>
                  <a target="_blank" href="<?php echo $twitter;?>"><i class="fa fa-twitter" title="Twitter"></i></a>
                <?php } else if ($twitter!="") { ?>
                 <a target="_blank" href="https://twitter.com/<?php echo $twitter;?>"><i class="fa fa-twitter" title="Twitter"></i></a>
                <?php } ?>
              </div> -->

              <div class="list-inline contacts-social mt-3 mb-3">
            <?php if ($facebook != '' && filter_var($facebook, FILTER_VALIDATE_URL)) { ?>
              <a target="_blank" href="<?php echo $facebook; ?>" class="list-inline-item bg-facebook text-white border-0"><i class="fa fa-facebook"></i></a>
            <?php  } else if ($facebook != '') { ?>
              <a target="_blank" href="https://www.facebook.com/<?php echo $facebook; ?>" class="list-inline-item bg-facebook text-white border-0"><i class="fa fa-facebook"></i></a>
            <?php } else { ?>
              <a href="javascript:;" class="list-inline-item bg-gray text-white border-0"><i class="fa fa-facebook"></i></a>
            <?php }
            if ($twitter != '' && filter_var($twitter, FILTER_VALIDATE_URL)) { ?>
              <a target="_blank" href="<?php echo $twitter; ?>" class="list-inline-item bg-twitter text-white border-0"><i class="fa fa-twitter"></i></a>
            <?php } else if ($twitter != '') { ?>
              <a target="_blank" href="https://www.twitter.com/<?php echo $twitter; ?>" class="list-inline-item bg-twitter text-white border-0"><i class="fa fa-twitter"></i></a>
            <?php } else { ?>
              <a href="javascript:;" class="list-inline-item bg-gray text-white border-0"><i class="fa fa-twitter"></i></a>
            <?php }
            if ($instagram != '' && filter_var($instagram, FILTER_VALIDATE_URL)) { ?>
              <a target="_blank" href="<?php echo $instagram; ?>" class="list-inline-item bg-instagram text-white border-0"><i class="fa fa-instagram"></i></a>
            <?php } else if ($instagram != "") { ?>
              <a target="_blank" href="https://www.instagram.com/<?php echo $instagram; ?>" class="list-inline-item bg-instagram text-white border-0"><i class="fa fa-instagram"></i></a>
            <?php } else { ?>
              <a href="javascript:;" class="list-inline-item bg-gray text-white border-0"><i class="fa fa-instagram"></i></a>
            <?php }
            if ($linkedin != '' && filter_var($linkedin, FILTER_VALIDATE_URL)) { ?>
              <a target="_blank" href="<?php echo $linkedin; ?>" class="list-inline-item bg-linkedin text-white border-0"><i class="fa fa-linkedin"></i></a>
            <?php } else if ($linkedin != '') { ?>
              <a target="_blank" href="https://in.linkedin.com/<?php echo $linkedin; ?>" class="list-inline-item bg-linkedin text-white border-0"><i class="fa fa-linkedin"></i></a>
            <?php } else { ?>
              <a href="javascript:;" class="list-inline-item bg-gray text-white border-0"><i class="fa fa-linkedin"></i></a>
            <?php } ?>
          </div>
            </div>
            <div class="card-body border-top">
              <?php
              $count5=$d->sum_data("credit_amount-debit_amount","user_wallet_master","user_mobile='$user_mobile' AND active_status=0");
                $row11=mysqli_fetch_array($count5);
                $totalDebitAmount=$row11['SUM(credit_amount-debit_amount)'];
               
              if ($totalDebitAmount>1) { ?>
              <div class="media align-items-center">
                <div>
                  <i class="fa fa-credit-card"></i>
                </div>
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                    <?php  
                     echo $currency.' '. number_format($totalDebitAmount,2);
                  ?>
                  </div>                   
                </div>
              </div>
              <hr>
            <?php } ?>

              <div class="media align-items-center">
                <div>
                  <i class="fa fa-mobile"></i>
                </div>
                <div class="media-body text-left">
                  <div class="progress-wrapper no_copy">
                    <?php if($user_mobile!=0) {  echo $country_code.' '.$user_mobile; } else {
                      echo "Not Available";
                    } ?>
                  </div>                   
                </div>
              </div>
              <?php if ($user_email!='') { ?>
                <hr>
                <div class="media align-items-center">
                  <div>
                    <i class="fa fa-envelope"></i>
                  </div>
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                      <a href="mailto:<?php echo $user_email; ?>"><?php echo $user_email; ?></a>
                    </div>                   
                  </div>
                </div>
              <?php }?>
              <?php if ($gender!='') { ?>
                <hr>
                <div class="media align-items-center">
                  <div>
                    <i class="fa fa-user"></i>
                  </div>
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                      <?php echo $gender; ?>
                    </div>                   
                  </div>
                </div>
              <?php }?>
              <hr>
              <div class="media align-items-center">
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                   Device : <?php echo $device; ?>
                  </div>                   
                </div>
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                   App Version :  <?php echo $app_version_code; ?>
                  </div>                   
                </div>
              </div>
              <div class="media align-items-center mt-1">
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                  Model : <?php echo $phone_model; ?>
                  </div>                   
                </div>
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                  OS Version : <?php echo $mobile_os_version .' '.$android_os_name; ?>
                  </div>                   
                </div>
                    
              </div>
              
              <div class="media align-items-center  mt-1">
                <div class="media-body text-left">
                        <div class="progress-wrapper">
                          Brand :  <?php echo $phone_brand; ?>
                        </div>                   
                </div>
              </div>
              <div class="media align-items-center mt-1">
                    <div class="media-body text-left">
                      <div class="progress-wrapper">
                      Last App Access : <?php if($last_login !="0000-00-00 00:00:00"){ echo date('d-M-Y H:i A',strtotime($last_login)); } ?>
                      </div>                   
                    </div>
                    
              </div>
              <?php if($user_mac_address != ''){ ?>
              <hr>
              <div class="media align-items-center">
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                   MAC Address : <?php echo $user_mac_address; ?>
                   <a href="macAddress?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo 
                   $id; ?>"><i class="fa fa-history bg-warning text-white"></i></a>
                  </div>   
                </div>              
              </div>
              <?php } ?>
              <?php
              
                if ($delete_status==0) { ?>
              <!-- <hr>
              <div class="media align-items-center">
                <div>
                </div>
                <div class="media-body text-left">
                  <div class="progress-wrapper">
                    <?php  echo "Office Closed :";  ?>

                    <?php  
                        if($unit_status=="5"){
                        ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $unit_id; ?>','openAppartemnt');" data-size="small"/>
                          <?php } else { ?>
                         <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $unit_id; ?>','closeAppartment');" data-size="small"/>
                        <?php } ?>
                  </div>                   
                </div>
                <div>
                </div>
                 
              </div> -->
              <?php 
              
              if ($adminData['user_type']==0) { ?>
              <hr>
              <div class="media align-items-center">
                <div>
                </div>
                
                  <div class="media-body text-left ">
                    <div class="progress-wrapper">
                    <?php echo $xml->string->app_active_status; ?>  :  
                      <?php  
                          if($active_status=="0"){
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','UserStatusDeactive');" data-size="small"/>
                            <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','UserStatusActive');" data-size="small"/>
                          <?php } ?>
                    </div>                   
                  </div>
                  <div>
                </div>
              </div>
              <hr>
              <div class="media align-items-center">
                <div>
                </div>
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                    Attendance Mandatory  :  
                      <?php  
                          if($is_attendance_mandatory=="0"){
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','UserAttendanceMandatoryNo');" data-size="small"/>
                            <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','UserAttendanceMandatoryYes');" data-size="small"/>
                          <?php } ?>
                    </div>                   
                  </div>
                
                <div>
                </div>
                <hr>
              </div>
              <hr>
              <div class="media align-items-center">
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                    VPN Restrictions In App   :  
                      <?php  
                          if($VPNCheck=="0"){
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','setVPNOn');" data-size="small"/>
                            <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','setVPNOff');" data-size="small"/>
                          <?php } ?>
                    </div>                   
                  </div>
              </div>
              <hr>
              <div class="media align-items-center">
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                    Developer Mode Restrictions In App   :  
                      <?php  
                          if($attendance_with_developer_mode_on=="1"){
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','AllowInDevelperMode');" data-size="small"/>
                            <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $user_id; ?>','NotAllowInDevelperMode');" data-size="small"/>
                          <?php } ?>
                    </div>                   
                  </div>
              </div>
              <hr>
              <div class="media align-items-center">
                <div>
                </div>
                  <div class="media-body text-left">
                    <div class="progress-wrapper">
                    Front Desk Portal Login  :  
                    <?php  
                          if($allow_front_desk_portal_login=="1"){
                          ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $id; ?>','AllowFrontDeskPortalLoginNo');" data-size="small"/>
                            <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $id; ?>','AllowFrontDeskPortalLoginYes');" data-size="small"/>
                          <?php } ?>
                    </div>                   
                  </div>
                
                <div>
                </div>
                <hr>
              </div>
              <?php } if ($adminData['admin_type']==1 || $adminData['role_id']==2) { ?> 
              <hr>
               <div class="row">
                <div class="col-xs-6 col-12">
                 <form action="companyAdmin" method="post">
                  <input type="hidden" name="user_full_name" value="<?php echo $user_full_name;?>">
                  <input type="hidden" name="user_mobile" value="<?php echo $user_mobile;?>">
                  <input type="hidden" name="user_email" value="<?php echo $user_email;?>">
                  <input type="hidden" name="uId" value="<?php echo $id;?>">
                   <button type="submit" class="btn btn-sm btn-warning" ><?php echo $xml->string->add_to_committee_member; ?></button>
                 </form>
                </div>
              </div>
              <?php }  ?>
              <hr>
              <?php 

              if ($member_status==0 && $delete_status==0) { ?>
              <div class="row">
                <div class="col-xs-6 col-12">
                  <form action="controller/userController.php" method="post">
                    <input type="hidden" name="userType" value="<?php echo $userType; ?>">
                    <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                    <input type="hidden" name="unit_name" value="<?php echo $user_full_name." (".$user_designation.") "; ?><?php echo $floor_name; ?>-<?php echo $block_name; ?>">
                    <input type="hidden" name="user_fcm" value="<?php echo $user_token; ?>">
                    <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                    <input type="hidden" name="assets_count" value="<?php echo mysqli_num_rows($iaq) ; ?>">
                     <?php if ($adminData['role_id']==2 && $totalUnpadiDue==0 || $adminData['admin_type']==1 && $totalUnpadiDue==0) {  ?>
                       <input type="hidden" name="delete_user_id" value="<?php echo $id; ?>">
                      <button type="submit" class="btn  btn-sm form-btn btn-danger mb-2" >Mark As a Ex Employee </button>
                    <?php }  ?>
                  </form>
                </div>
                <div class="col-xs-6 col-12">
                  
                  <?php if ($user_token!='') { ?>
                    <form  action="controller/userController.php" method="post" class="mt-4">
                      <input type="hidden" name="user_token" value="<?php echo $user_token; ?>">
                      <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                      <input type="hidden" name="bId" value="<?php echo $block_id; ?>">
                      <input type="hidden" name="device" value="<?php echo $device; ?>">
                      <input type="hidden" name="logoutDevice" value="logoutDevice">
                      <button type="submit" class="btn  btn-sm form-btn btn-warning mb-2" ><?php echo $xml->string->logout_from_app; ?></button>
                    </form>
                      
                      <button data-toggle="modal" data-target="#notificationSingle"  type="button" class="btn mt-4 btn-sm  btn-danger" ><?php echo $xml->string->send; ?> <?php echo $xml->string->notifications; ?> </button>
                  <?php } ?>
                </div>
              </div>
            <?php }   } else  if ( $delete_status==1) {
              echo $xml->string->account_deleted_on.' : '. $deleted_date;  
            ?>
            <button type="button" class="btn btn-info" onclick="UserDeleteActive(<?php echo $id; ?>)" >Active User</button>
           <?php }  ?>

            </div>
          </div>
          <?php 
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  users_master.user_mobile!=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_mobile='$user_mobile' AND users_master.country_code='$country_code' AND users_master.user_id!='$id'","");

            $q4=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=1 AND  users_master.user_mobile!=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.unit_id='$unit_id' AND users_master.user_id!='$id' AND users_master.member_status=0","");
            if(mysqli_num_rows($q3)>0) {
          ?>
            <div class="row p-2">
              <div class="col-12 col-lg-12 col-xl-12">
                <?php echo $xml->string->multiple; ?> <?php echo $xml->string->units; ?> <?php echo $xml->string->register_on_same_number; ?>
              </div>
              <?php while ($multiData=mysqli_fetch_array($q3)) {
                if($multiData['user_type']==0) {
              ?>
                <div class="col-12 col-lg-2 col-xl-6">
                  <div class="card gradient-scooter no-bottom-margin">
                    <div class="card-body text-center">
                      <a   href="employeeDetails?id=<?php echo $multiData['user_id']; ?>"><h6 class="text-white mt-2 text-capitalize"><?php echo $multiData['block_name']; ?>-<?php echo $multiData['floor_name']; ?> <br><?php echo $multiData['user_full_name']; ?> </h6></a>
                    </div>
                  </div>
                </div>
              <?php }   
              }?>
            </div>
          <?php }
           if(mysqli_num_rows($q4)>0) {
          ?>
            <div class="row p-2">
              <div class="col-12 col-lg-12 col-xl-12">
                <?php echo $xml->string->previous_owners_tenants_of_this; ?> <?php echo $xml->string->unit; ?>
              </div>
              <?php while ($multiData=mysqli_fetch_array($q4)) {

                if($multiData['user_type']==0) {
              ?>
                <div class="col-6 col-lg-2 col-xl-6">
                  <div class="card gradient-scooter no-bottom-margin">
                    <div class="card-body text-center">
                      <a   href="employeeDetails?id=<?php echo $multiData['user_id']; ?>"><h6 class="text-white mt-2 text-capitalize"><?php echo $multiData['user_full_name']; ?><Br><?php   echo $xml->string->owner; ?> </h6></a>
                    </div>
                  </div>
                </div>
              <?php } elseif ($multiData['user_type']==1) { ?>
                <div class="col-6 col-lg-2 col-xl-6">
                  <div class="card bg-warning  no-bottom-margin">
                    <div class="card-body text-center">
                      <a  href="employeeDetails?id=<?php echo $multiData['user_id']; ?>"><h6 class="text-white mt-2 text-capitalize"><?php echo $multiData['user_full_name']; ?><Br><?php echo $xml->string->tenant;; ?> </h6></a>
                    </div>
                  </div>
                </div>
              <?php }  
              }?>
            </div>
          <?php } ?>
        </div>
        <div class="col-lg-8 col-md-8">
          <div class="card">
            <div class="card-body">
              <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#Professional" data-toggle="pill" class="nav-link active"><i class="fa fa-graduation-cap"></i> <span class="hidden-xs">Job Information </span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#contact_detail" data-toggle="pill" class="nav-link"><i class="fa fa-address-book-o"></i> <span class="hidden-xs">Contact Detail</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#personal_info" data-toggle="pill" class="nav-link"><i class="icon-note"></i> <span class="hidden-xs">Personal Info</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#social_links" data-toggle="pill" class="nav-link"><i class="fa fa-connectdevelop"></i> <span class="hidden-xs">Social Links</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getUserTeamMembers(<?php echo $id; ?>,<?php echo $level_id; ?>,<?php echo $society_id; ?>)"  data-target="#team" data-toggle="pill" class="nav-link"><i class="fa fa-users"></i>
                     <span class="hidden-xs"> Team </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getUserIdProof(<?php echo $id; ?>,<?php echo $society_id; ?>)"  data-target="#idProof" data-toggle="pill" class="nav-link"><i class="fa fa-id-card-o"></i>
                     <span class="hidden-xs"> ID Proof </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getUserAssets(<?php echo $id; ?>,<?php echo $society_id; ?>)"  data-target="#assets" data-toggle="pill" class="nav-link"><i class="fa fa-cubes"></i>
                     <span class="hidden-xs"> Assets </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#quickLinks" data-toggle="pill" class="nav-link "><i class="fa fa-link"></i> <span class="hidden-xs">Quick Links</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getVisitingCardData(<?php echo $id; ?>,<?php echo $society_id; ?>)" data-target="#visting_cards" data-toggle="pill" class="nav-link "><i class="fa fa-credit-card"></i> <span class="hidden-xs">Visiting Card</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getUserBankData(<?php echo $id; ?>,<?php echo $society_id; ?>)" data-target="#bank_details" data-toggle="pill" class="nav-link "><i class="fa fa-bank"></i> <span class="hidden-xs">Bank Accounts</span></a>
                </li>
                
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getEmployeeHistory(<?php echo $id; ?>,<?php echo $society_id; ?>)" data-target="#experience_details" data-toggle="pill" class="nav-link "><i class="fa fa-briefcase"></i> <span class="hidden-xs"><?php echo $xml->string->experience ; ?></span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getEducationAndAchivements(<?php echo $id ?>)" data-target="#education_details" data-toggle="pill" class="nav-link "><i class="fa fa-graduation-cap"></i> <span class="hidden-xs"><?php echo $xml->string->education . " & " . $xml->string->achievements; ?></span></a>
                </li>
               
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getPendingDues(<?php echo $id ?>)" data-target="#pendiAndDues" data-toggle="pill" class="nav-link "><i class="fa fa-graduation-cap"></i> <span class="hidden-xs">Pending Due</span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" onclick="getNotes(<?php echo $id ?>)" data-target="#notes" data-toggle="pill" class="nav-link "><i class="fa fa-sticky-note-o"></i> <span class="hidden-xs"><?php echo $xml->string->notes; ?> </span></a>
                </li>
              </ul>
              <div class="tab-content p-3">
                <div class="tab-pane" id="assets">
                  
                  <?php 
                 // echo $user_id;
                 ?>
                    <div class="table-responsive assets">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Category</th>
                          <th>Item Name</th>
                          <th>Brand</th>
                          <th>Item Image</th>
                          <th>Handover Date</th>
                          <th>Handover Image</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="assetsData">
                      </tbody>
                    </table>
                  </div>
                  <?php  ?>
                </div>
                <div class="tab-pane" id="emergency">
                  <?php  $fq11=$d->select("user_emergencycontact_master","user_id='$user_id' AND society_id='$society_id'");
                  if(mysqli_num_rows($fq11)>0) { ?>
                  <div class="table-responsive">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo $xml->string->name; ?> </th>
                          <th><?php echo $xml->string->emergency_numbers; ?> </th>
                          <th><?php echo $xml->string->relation; ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $i=1;
                        while ($fData=mysqli_fetch_array($fq11)) {
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $fData['person_name'] ?></td>
                            <td><?php echo $fData['person_mobile']; ?></td>
                            <td><?php echo $fData['relation']; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } else {
                    echo "<img width='250' src='img/no_data_found.png'>";
                  } ?>
                </div>
                <div class="tab-pane" id="notes">
                  <a class="btn btn-warning pull-right btn-sm" href="#" data-toggle="modal" data-target="#addNote"  > Add Note</a>
                  <br>
                    <div class="table-responsive notsData">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo $xml->string->title_add_note; ?></th>
                          <th><?php echo $xml->string->description_contact_finca_fragment; ?></th>
                          <th><?php echo $xml->string->date; ?></th>
                          <th><?php echo $xml->string->action; ?></th>
                        </tr>
                      </thead>
                      <tbody id="notesTableData">
                       
                      </tbody>
                    </table>
                  </div>
                  <?php  ?>
                </div>

                <div class="tab-pane" id="idProof">
                  <div class="row">
                    <div class="col-md-12 col-12 text-center">
                      <a class="btn btn-warning pull-right btn-sm mb-4" href="#" data-toggle="modal" data-target="#addIdProofModal"  > Add Id Proof</a>
                    </div>
                  </div>
                    <div class="row idProofData">
                        
                    </div>
                  <?php  ?>
                </div>

                <div class="tab-pane" id="education_details">
                  <a class="btn btn-warning pull-right btn-sm" href="#" data-toggle="modal" data-target="#addEducation">Add Education Details</a>
                  <br>
                  <?php  
                  if(mysqli_num_rows($eaec)>0) { ?>
                    <div class="table-responsive education_details">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo "Achievement/Class Name"; ?></th>
                          <th><?php echo "University/Asso Name"; ?></th>
                          <th><?php echo "Date"; ?></th>
                          <th><?php echo $xml->string->action; ?></th>
                        </tr>
                      </thead>
                      <tbody id="educationData">
                      </tbody>
                    </table>
                  </div>
                  <?php } else {
                    echo "<img width='250' src='img/no_data_found.png'>";
                  } ?>
                </div>
                <div class="tab-pane" id="experience_details">
                  <a class="btn btn-danger pull-right btn-sm mb-1" href="#" data-toggle="modal" data-target="#addExperience">Add Past Experience</a>
                  <a class="btn btn-warning pull-right btn-sm mx-1 mb-1" href="#" data-toggle="modal" data-target="#addCurrentExperience">Add Promotion</a>
                  <br>
                  <div class="table-responsive empe_exp">
                    <h4>Past Experience </h4>
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo $xml->string->company_name; ?></th>
                          <th><?php echo $xml->string->designation; ?></th>
                          <th><?php echo "Work From"; ?></th>
                          <th><?php echo "Work To"; ?></th>
                          <th><?php echo "Company Location"; ?></th>
                          <th><?php echo $xml->string->action; ?></th>
                        </tr>
                      </thead>
                      <tbody id="empe_exp_data">
                      </tbody>
                    </table>
                  </div>
                    <div class="table-responsive mt-3 expHistory">
                      <h4>Promotions </h4>
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo $xml->string->designation; ?></th>
                          <th><?php echo "Work From"; ?></th>
                          <th><?php echo "Work To"; ?></th>
                          <th><?php echo $xml->string->remark;  ?></th>
                          <th>Salary Increment</th>
                          <th><?php echo $xml->string->action; ?></th>
                        </tr>
                      </thead>
                      <tbody id="expHistoryData">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="visting_cards">
                  
                  
                  <div class="row vistingCardMainDiv">
                   
                  </div>
                 
                </div>


                <div class="tab-pane" id="bank_details">
                  <?php
                  ?>
                  <a class="btn btn-warning pull-left mb-1 addBankModal btn-sm" href="#" data-toggle="modal" data-target="#addBankModal" onclick="buttonSettings()" > Add Bank</a>
                 
                  <br>
                  <?php  
                   ?>
                    <div class="table-responsive bank">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>#</th>
                           <th><?php echo $xml->string->action; ?></th>
                          <th><?php echo "Account Holder's Name"; ?></th>
                          <th><?php echo $xml->string->bank_name; ?></th>
                          <th><?php echo $xml->string->account_number; ?></th>
                          <th><?php echo $xml->string->ifsc_code; ?></th>
                          <th>CRN No</th>
                          <th>Pan Card Number</th>
                          <th><?php echo $xml->string->bank_branch; ?></th>
                          <th><?php echo $xml->string->account_type; ?></th>
                          <th>Date</th>
                        
                        </tr>
                      </thead>
                      <tbody id="userBankData">
                      </tbody>
                    </table>
                  </div>
                 
                </div>
                <div class="tab-pane " id="contact_detail">
                  <form id="addUser" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                      
                        <input type="hidden" name="user_fcm" value="<?php echo $user_token; ?>">
                        <input type="hidden" name="user_status" value="<?php echo $user_status; ?>">
                        <?php  if($member_status==0) { ?>
                        <input type="hidden" name="edit_user_id" value="<?php echo $id; ?>">
                        <input type="hidden" name="contact_detail" value="contact_detail">
                        <?php } else { ?>
                        <input type="hidden" name="edit_user_id_family" value="<?php echo $user_id; ?>">
                        <?php }?>
                        <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                        <input type="hidden" name="user_type" value="<?php echo $user_type; ?>">
                        <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->mobile_no; ?> <span class="required">*</span></label>
                        <div class="row mr-0 ml-0">
                          <div class="col-lg-4 pr-0 pl-0">
                            <input type="hidden" value="<?php echo $country_code; ?>" id="country_code_get" name="country_code_get">
                            <select name="country_code" class="form-control single-select" id="country_code" required="">
                              <?php include 'country_code_option_list.php'; ?>
                            </select>
                          </div>
                          <div class="col-lg-8 pr-0 pl-0">
                            <input required="" class="form-control no_copy" name="user_mobile"  maxlength="15" minlength="8" type="text" onblur="checkMobileUserEdit()"  value="<?php echo $user_mobile; ?>" inputmode="numeric" id="userMobile">
                            <input class="form-control" name="user_mobile_old" maxlength="15"  type="hidden" value="<?php echo $user_mobile; ?>"  id="userMobileOld">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->emergency_number; ?></label>
                        <div class="row mr-0 ml-0">
                          <div class="col-lg-4 pr-0 pl-0">
                            <input type="hidden" value="<?php echo $country_code_emergency; ?>" id="country_code_emergency_get" name="">
                            <select name="country_code_emergency" class="form-control single-select" id="country_code_emergency">
                              <?php include 'country_code_option_list.php'; ?>
                            </select>
                          </div>
                          <div class="col-lg-8 pr-0 pl-0">
                            <input class="form-control no_copy onlyNumber" inputmode="numeric" name="emergency_number"  maxlength="15" minlength="8"  type="text" value="<?php if($emergency_number!=0) { echo $emergency_number; } ?>" id="emergency_number">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->alternate_phone_number; ?></label>
                        <div class="row mr-0 ml-0">
                          <div class="col-lg-4 pr-0 pl-0">
                            <input type="hidden" value="<?php echo $country_code_alt; ?>" id="country_code_get_alt" name="">
                            <select name="country_code_alt" class="form-control single-select" id="country_code_alt" >
                              <?php include 'country_code_option_list.php'; ?>
                            </select>
                          </div>
                          <div class="col-lg-8 pr-0 pl-0">
                            <input class="form-control onlyNumber" inputmode="numeric" name="alt_mobile"  maxlength="15" minlength="8"  type="text" value="<?php if($alt_mobile!=0) { echo $alt_mobile; } ?>" id="alt_mobile">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->whatsapp; ?> Number</label>
                        <div class="row mr-0 ml-0">
                          <div class="col-lg-4 pr-0 pl-0">
                            <input type="hidden" value="<?php echo $country_code_whatsapp; ?>" id="country_code_whatsapp_get" name="">
                            <select name="country_code_whatsapp" class="form-control single-select" id="country_code_whatsapp">
                              <?php include 'country_code_option_list.php'; ?>
                            </select>
                          </div>
                          <div class="col-lg-8 pr-0 pl-0">
                            <input class="form-control no_copy onlyNumber" inputmode="numeric" name="whatsapp_number"  maxlength="15" minlength="8"  type="text" value="<?php if($whatsapp_number!=0) { echo $whatsapp_number; } ?>" id="whatsapp_number">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Current Address</label>
                        <textarea maxlength="200" value="<?php echo $last_address; ?>" class="form-control" type="text" name="last_address" value=""><?php  echo $last_address; ?></textarea>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->permanent_address; ?></label>
                        <textarea maxlength="200" value="<?php echo $permanent_address; ?>" class="form-control" type="text" name="permanent_address" value=""><?php  echo $permanent_address;   ?></textarea>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->email_id; ?> </label>
                        <input class="form-control" type="email" name="user_email"  value="<?php echo $user_email; ?>" id="userEmail">
                        <input class="form-control" type="hidden" name="user_email_old" value="<?php echo $user_email; ?>"  id="userEmailOld">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Personal Email </label>
                        <input class="form-control" type="email" name="personal_email"  value="<?php echo $personal_email; ?>" id="personal_email">
                        <input class="form-control" type="hidden" name="personal_email_old" value="<?php echo $user_email; ?>"  id="personal_email_old">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Contact Number Privacy</label>
                        <select class="form-control" name="public_mobile"  type="text"  id="public_mobile">
                          <option <?php if($public_mobile=='0') { echo 'selected';} ?> value="0">Public</option>
                          <option <?php if($public_mobile=='1') { echo 'selected';} ?> value="1">Private</option>
                        </select>
                      </div>
                    </div>
                    <?php if ( $delete_status==0) { ?>
                    <div class="form-group row">
                      <div class="col-lg-12 text-center">
                        <input type="submit" id="socAddBtn" class="btn btn-primary" name="updateUserProfile"  value="Update">
                      </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>
                <div class="tab-pane " id="personal_info">
                  <form id="addUser" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->first_name; ?> <span class="required">*</span></label>
                        <input type="hidden" name="user_fcm" value="<?php echo $user_token; ?>">
                        <input type="hidden" name="user_status" value="<?php echo $user_status; ?>">
                        <?php  if($member_status==0) { ?>
                        <input type="hidden" name="edit_user_id" value="<?php echo $id; ?>">
                        <input type="hidden" name="personal_info" value="personal_info">
                        <?php } else { ?>
                        <input type="hidden" name="edit_user_id_family" value="<?php echo $user_id; ?>">
                        <?php }?>
                        <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                        <input type="hidden" name="user_type" value="<?php echo $user_type; ?>">
                        <input maxlength="30" class="form-control" name="user_first_name" type="text" value="<?php echo $user_first_name; ?>" required="">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Middle Name </label>
                        <input maxlength="30" class="form-control" name="user_middle_name" type="text" value="<?php echo $user_middle_name; ?>" >
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->last_name; ?> <span class="required">*</span></label>
                        <input maxlength="30" class="form-control" name="user_last_name" type="text" value="<?php echo $user_last_name; ?>" required="">
                      </div>
                    
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->date_of_birth; ?>   </label>
                        <input class="form-control" readonly="" id="autoclose-datepicker-dob" name="member_date_of_birth" type="text" value="<?php if($member_date_of_birth!='0000-00-00' && $member_date_of_birth!='null') { echo $member_date_of_birth; }?>" >
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->blood_group; ?></label>
                        <select name="blood_group" class="form-control single-select" >
                          <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                          <option <?php if($blood_group== "A+") { echo 'selected';} ?> value="A+">A+</option>
                          <option <?php if($blood_group== "A-") { echo 'selected';} ?> value="A-">A-</option>
                          <option <?php if($blood_group== "B+") { echo 'selected';} ?> value="B+">B+</option>
                          <option <?php if($blood_group== "B-") { echo 'selected';} ?> value="B-">B-</option>
                          <option <?php if($blood_group== "AB+") { echo 'selected';} ?> value="AB+">AB+</option>
                          <option <?php if($blood_group== "AB-") { echo 'selected';} ?> value="AB-">AB- </option>
                          <option <?php if($blood_group== "O+") { echo 'selected';} ?> value="O+">O+</option>
                          <option <?php if($blood_group== "O-") { echo 'selected';} ?> value="O-">O-</option>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->marital_status; ?></label>
                        <?php 
                          $marital_status_string= $xml->string->marital_status_array; 
                          $marital_status_array = explode("~", $marital_status_string);
                        ?>
                        <select name="marital_status" class="form-control" onchange="change_marital_status(this.value)">
                          <?php for ($iR=0; $iR < count($marital_status_array) ; $iR++) {  ?>
                            <option <?php if(isset($marital_status) && $marital_status==$iR  ) { echo "selected";} ?> value="<?php echo $iR;?>"><?php echo $marital_status_array[$iR];?></option>
                          <?php  }?>
                        </select>
                      </div>
                       
                      <div class="col-lg-6 hide_wedding_anniversary_date <?php echo $dNone; ?>">
                        <label class="form-control-label">Wedding Anniversary</label>
                        <input class="form-control valid" readonly="" value="<?php if(isset($wedding_anniversary_date) && $wedding_anniversary_date !="0000-00-00"){ echo $wedding_anniversary_date; } ?>" id="wedding_anniversary_date" name="wedding_anniversary_date" type="text"  aria-invalid="false">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->family_members; ?></label>
                        <input maxlength="30" class="form-control onlyNumber" name="total_family_members" type="text" value="<?php echo $total_family_members; ?>">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->nationality; ?></label>
                        <select name="nationality_drop" id="nationality_drop" class="form-control" >
                          <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                          <option <?php if($nationality== "indian") { echo 'selected';} ?> value="indian">Indian</option>
                          <option <?php if($nationality != "indian" && $nationality != "") { echo 'selected';} ?> value="other">Other</option>
                        </select>
                      </div>
                      <div class="col-lg-6 <?php echo ($nationality == "indian") ? "d-none" : ""; ?>" id="nationality_div">
                        <label class="form-control-label">Enter <?php echo $xml->string->nationality; ?></label>
                        <input maxlength="30" class="form-control" name="nationality" type="text" value="<?php echo $nationality; ?>">
                      </div>
                      <?php if ($adminData['role_id']==2) {  ?>
                        <div class="col-lg-6">
                          <label class="form-control-label"> <?php echo $xml->string->demo; ?> <?php echo $xml->string->account; ?> </label>
                          <select class="form-control" name="is_demo"  type="text"  id="is_demo">
                            <option <?php if($is_demo=='0') { echo 'selected';} ?> value="0"><?php echo $xml->string->no; ?></option>
                            <option <?php if($is_demo=='1') { echo 'selected';} ?> value="1"><?php echo $xml->string->yes; ?></option>
                          </select>
                        </div>
                      <?php } ?>
                      <div class="col-lg-6">
                        <label class="form-control-label w-100"><?php echo $xml->string->gender; ?></label>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" <?php if($gender=='Male'){echo "checked";} ?>  class="form-check-input" value="Male" name="gender"> <?php echo $xml->string->male; ?>
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio"  <?php if($gender=='Female'){echo "checked";} ?>  class="form-check-input" value="Female" name="gender"> <?php echo $xml->string->female; ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->hobbies; ?> & INTERESTS</label>
                        <textarea maxlength="300" class="form-control" name="intrest_hobbies" type="text" ><?php echo $proData['intrest_hobbies']; ?></textarea>
                      </div>
                  
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->professional_skills; ?></label>
                        <textarea maxlength="300" class="form-control" name="professional_skills" type="text" ><?php echo $proData['professional_skills']; ?></textarea>
                      </div>
                    
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->language_known; ?></label>
                        <textarea maxlength="300" class="form-control" name="language_known" type="text" value=""><?php echo $proData['language_known']; ?></textarea>
                      </div>
                    
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->special_skills; ?></label>
                        <textarea maxlength="300" class="form-control" name="special_skills" type="text" value=""><?php echo $proData['special_skills']; ?></textarea>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->change_profile; ?></label>
                        <input accept="image/*" class="form-control-file border" id="imgInp" name="user_profile_pic" type="file">
                        <input class="form-control" name="user_profile_pic_old" type="hidden" value="<?php echo $user_profile_pic; ?>">
                      </div>
                    
                      <div class="col-lg-6">
                        <label class="form-control-label">Resume/CV</label>
                        <input  class="form-control-file border docOnly"  name="resume_cv_doc" type="file">
                        <input class="form-control" name="resume_cv_doc_old" type="hidden" value="<?php echo $resume_cv_doc; ?>">
                        <?php if ($resume_cv_doc != '') {
                              $ext = pathinfo($resume_cv_doc, PATHINFO_EXTENSION);
                              $singleView= 0;
                              if ($ext == 'pdf' || $ext == 'PDF') {
                                $imgIcon = 'img/pdf.png';
                                $singleView= 1;
                              } else if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                                $imgIcon = 'img/jpg.png';
                                 $singleView= 1;
                              } else if ($ext == 'png') {
                                $imgIcon = 'img/png.png';
                                 $singleView= 1;
                              } else if ($ext == 'doc' || $ext == 'docx') {
                                $imgIcon = 'img/doc.png';
                                 $singleView= 0;
                              } else {
                                $imgIcon = 'img/doc.png';
                                 $singleView= 0;
                              }
                              ?>
                              <a href="../img/documents/<?php echo $resume_cv_doc; ?>" <?php if($singleView==1) { ?> data-fancybox="images" data-caption="Photo Name : Resume/CV" <?php } ?>>
                                <div style="height: 30px;width: 30px;background-image: url(<?php echo $imgIcon ?>);background-repeat: no-repeat;background-size: contain;" class="d-block text-break"></div>
                              </a>
                            <?php } ?>

                      </div>
                      <?php  if($member_status==1) { ?>
                        <div class="col-sm-6">
                          <label for="input-14" class="form-control-label"><?php echo $xml->string->app_access; ?><span class="text-danger">*</span></label>
                          <select class="form-control single-select" required="" id="user_status"  name="user_status">
                            <option value=""> -- Select --</option>
                            <option <?php if($user_status==1) { echo 'selected'; } ?> value="1"><?php echo $xml->string->yes; ?></option>
                            <option  <?php if($user_status==2) { echo 'selected'; } ?> value="2"><?php echo $xml->string->no; ?></option>
                          </select>
                        </div>
                       <?php } //IS_3241
  
                       if ($user_type==1 && $member_status==0){ ?>
                          <div class="col-lg-6">
                          <label class="form-control-label"><?php echo $xml->string->rent_agreement; ?></label>
                             <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"  class="form-control-file border idProof" id="tenant_doc" type="file" name="tenant_doc">
                             <input class="form-control" name="tenant_doc_old" type="hidden" value="<?php echo $tenant_doc; ?>">
                        </div>
                       <?php } ?> 
                      <?php if ($user_type==1 && $member_status==0){ ?>
                        <div class="col-lg-6">
                            <label class="form-control-label"><?php echo $xml->string->police_verification_document; ?> </label>
                          <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"  class="form-control-file border idProof" id="prv_doc" type="file" name="prv_doc">
                          <input class="form-control" name="prv_doc_old" type="hidden" value="<?php echo $prv_doc; ?>">
                      </div>
                       <?php } ?>
                    </div>
                    <?php if ( $delete_status==0) { ?>
                    <div class="form-group row">
                      <div class="col-lg-12 text-center">
                        <input type="submit" id="socAddBtn" class="btn btn-primary" name="updateUserProfile"  value="Update">
                      </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>
                <div class="tab-pane " id="social_links">
                  <form id="addUser" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <input type="hidden" name="user_fcm" value="<?php echo $user_token; ?>">
                        <input type="hidden" name="user_status" value="<?php echo $user_status; ?>">
                        <?php  if($member_status==0) { ?>
                        <input type="hidden" name="edit_user_id" value="<?php echo $id; ?>">
                        <input type="hidden" name="social_links" value="social_links">
                        <?php } else { ?>
                        <input type="hidden" name="edit_user_id_family" value="<?php echo $user_id; ?>">
                        <?php }?>
                        <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                        <input type="hidden" name="user_type" value="<?php echo $user_type; ?>">
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->facebook; ?> </label>
                        <input class="form-control" type="text" name="facebook" value="<?php echo $facebook; ?>" id="facebook">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->linkedin; ?> </label>
                        <input class="form-control" type="text" name="linkedin" value="<?php echo $linkedin; ?>" id="linkedin">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->twitter; ?> </label>
                        <input class="form-control" type="text" name="twitter" value="<?php echo $twitter; ?>" id="twitter">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->instagram; ?> </label>
                        <input class="form-control" type="text" name="instagram" value="<?php echo $instagram; ?>" id="instagram">
                      </div>
                    </div>
                    <?php if ( $delete_status==0) { ?>
                    <div class="form-group row">
                      <div class="col-lg-12 text-center">
                        <input type="submit" id="socAddBtn" class="btn btn-primary" name="updateUserProfile"  value="Update">
                      </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>
                <div class="tab-pane  " id="location">
                  <form id="updateUserLocationForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                      <div class="col-lg-12">
                        <input id="searchInput5" name="location_name" class="form-control" type="text" placeholder="Enter location" >
                        <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-lg-12 text-center">
                        <input type="hidden" name="user_latitude" id="user_latitude">
                        <input type="hidden" name="user_longitude" id="user_longitude">
                        <input type="hidden" name="old_user_latitude" value="<?php echo $user_latitude; ?>">
                        <input type="hidden" name="old_user_longitude" value="<?php echo $user_longitude; ?>">
                        <input type="hidden" name="updateUserLocation" value="updateUserLocation">
                        <input type="hidden" name="user_id" value="<?php echo $_GET['id']; ?>">
                        <input type="submit" class="btn btn-primary" name="UpdateLocation" value="Update">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="tab-pane active" id="Professional">
                  <form id="personal-info3" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                      <input type="hidden" name="user_fcm" value="<?php echo $user_token; ?>">
                      <input type="hidden" name="prof_user_id" value="<?php echo $id; ?>">
                      <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                      <input type="hidden" name="user_full_name" value="<?php echo $user_full_name; ?>">
                      <input type="hidden" name="user_phone" value="<?php echo $user_mobile; ?>">
                      <input type="hidden" name="user_email" value="<?php echo $user_email; ?>">
                      <input type="hidden" name="member_status" value="<?php echo $member_status; ?>">
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->designation; ?> <span class="text-danger">*</span></label>
                        <input maxlength="100" required=""  class="form-control" type="text" name="designation" value="<?php echo $user_designation; ?>" id="designation">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->employment_type; ?></label>
                        <!-- <input maxlength="100" class="form-control" name="employment_type" type="text" value="<?php echo $proData['employment_type']; ?>"> -->
                        <select name="employment_type" class="form-control single-select" id="employment_type">
                          <option  value="">--Select Type--</option>
                          <option <?php if($proData['employment_type']=="1"){ echo 'selected'; } ?>  value="1">Full Time</option>
                          <option <?php if($proData['employment_type']=="2"){ echo 'selected'; } ?> value="2" >Part Time</option>
                          <option <?php if($proData['employment_type']=="3"){ echo 'selected'; } ?> value="3" >Seasonal</option>
                          <option <?php if($proData['employment_type']=="4"){ echo 'selected'; } ?> value="4" >Temporary</option>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->employee_id; ?> </label>
                        <input class="form-control" type="text" name="company_employee_id"  value="<?php echo $company_employee_id; ?>" id="company_employee_id">
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->date_of_join; ?> </label>
                        <input  class="form-control" id="datepicker-doj" autocomplete="off" name="joining_date" type="text" value="<?php if($proData['joining_date']!='0000-00-00' && $proData['joining_date']!='null') { echo $proData['joining_date']; }?>" >
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Branch</label>
                        <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                          <option value="">-- Select Branch --</option> 
                            <?php 
                              $qb=$d->select("block_master","society_id='$society_id'");  
                              while ($blockData=mysqli_fetch_array($qb)) { ?>
                          <option  <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                          <?php } ?>
                          </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Department</label>
                        <select name="floor_id" id="floor_id" class="form-control single-select floor_id" onchange="getSubDepartmentByFloorId(this.value);" required>
                          <option value="">-- Select Department --</option> 
                            <?php 
                              $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND block_master.block_id='$block_id'");  
                              while ($depaData=mysqli_fetch_array($qd)) { ?>
                          <option  <?php if($floor_id==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                          <?php } ?>
                          </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label">Sub Department</label>
                        <select name="sub_department_id" class="form-control single-select" >
                        <option value="">-- <?php echo $xml->string->select; ?> Sub <?php echo $xml->string->floor; ?> --</option>
                        <?php
                            $sdq=$d->select("sub_department","society_id='$society_id' AND floor_id='$floor_id'");
                            while ($sdqData=mysqli_fetch_array($sdq)) { ?>
                              <option <?php if($sub_department_id== $sdqData['sub_department_id']) { echo 'selected';} ?>  value="<?php echo $sdqData['sub_department_id'];?>"> <?php echo $sdqData['sub_department_name'];?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label"><?php echo $xml->string->shift; ?> <?php echo $xml->string->timing; ?> </label>
                        <input type="hidden" name="hidden_block" value="<?php echo $block_id; ?>">
                        <input type="hidden" name="hidden_floor_id" value="<?php echo $floor_id; ?>">
                        <!-- <input maxlength="100" class="form-control" name="shift_timing" type="text" value="<?php echo $proData['shift_timing']; ?>"> -->
                        <select type="text" class="form-control single-select" id="shift_time_id" name="shift_time_id">
                          <option value="">-- <?php echo $xml->string->select; ?> <?php echo $xml->string->shift; ?> --</option>
                          <?php

                          $shiftq=$d->select("shift_timing_master","society_id='$_COOKIE[society_id]' AND is_deleted = 0");
                        
                          while ($shiftData=mysqli_fetch_array($shiftq)) { 
                            $arData = array();
                            for($i=0; $i<count($week_days); $i++){
                              if($shiftData['week_off_days'] !="")
                              {
                                $week_off_days = explode(',',$shiftData['week_off_days']);
                                if(in_array($i, $week_off_days)){
                                  array_push($arData,$week_days[$i]);
                                } 
                              }
                            }
                            if(!empty($arData)){
                              $showOff = implode(',',$arData);
                            }
                            else
                            {
                              $showOff = "";
                            }
                            ?>
                            <option <?php if($shift_time_id == $shiftData['shift_time_id']){echo "selected";} ?> value="<?php echo $shiftData['shift_time_id'];?>"> <?php echo $shiftData['shift_name'];?> <?php echo "(S".$shiftData['shift_time_id'];?>) <?php if($showOff !="") {echo $showOff; } ?></option>
                          <?php } ?>
                        </select>    
                      </div>
                      <?php $totalZone = $d->count_data_direct("zone_id","zone_master","society_id='$society_id' AND zone_status = 0");
                      if( $totalZone > 0) { ?>
                      <div class="col-lg-6">
                        <label class="form-control-label">Zone</label>
                        <select name="zone_id" class="form-control single-select" >
                        <option value="">-- <?php echo $xml->string->select; ?> Zone --</option>
                          <?php
                            $zq=$d->select("zone_master","society_id='$society_id' AND zone_status='0'");
                            while ($zqData=mysqli_fetch_array($zq)) { ?>
                              <option <?php if($zone_id== $zqData['zone_id']) { echo 'selected';} ?>  value="<?php echo $zqData['zone_id'];?>"> <?php echo $zqData['zone_name'];?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <?php
                      }
                      $totalLevel = $d->count_data_direct("level_id","employee_level_master","society_id='$society_id' AND level_status = 0");
                      if( $totalLevel > 0)
                      {
                      ?>
                      <div class="col-lg-6">
                        <label class="form-control-label">Employee Level</label>
                        <select name="level_id" class="form-control single-select" >
                        <option value="">-- <?php echo $xml->string->select; ?> Employee Level --</option>
                        <?php
                            $lq=$d->select("employee_level_master","society_id='$society_id' AND level_status='0'");
                            while ($lqData=mysqli_fetch_array($lq)) { ?>
                              <option <?php if($level_id== $lqData['level_id']) { echo 'selected';} ?>  value="<?php echo $lqData['level_id'];?>"> <?php echo $lqData['level_name'];?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <?php
                      }
                      $check_sc = $d->selectRow("sister_company_id,sister_company_name","sister_company_master","society_id = '$society_id' AND status = 1");
                      if(mysqli_num_rows($check_sc) > 0)
                      {
                      ?>
                      <div class="col-lg-6">
                        <label class="form-control-label">Sister Company</label>
                          <select class="form-control single-select" id="sister_company_id" name="sister_company_id">
                            <option>Select</option>
                            <?php
                            while($row = $check_sc->fetch_assoc())
                            {
                            ?>
                            <option <?php if($sister_company_id == $row['sister_company_id']){ echo "selected"; } ?> value="<?php echo $row['sister_company_id']; ?>"><?php echo $row['sister_company_name']; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                      </div>
                      <?php
                      }
                      if ($member_status==0){ ?>
                      <div class="col-lg-6">
                        <label class="form-control-label w-100"><?php echo $xml->string->members; ?> <?php echo $xml->string->view_access; ?> </label>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" <?php if($member_access_denied==0) { echo 'checked'; }?> class="form-check-input" value="0" name="member_access_denied"> <?php echo $xml->string->yes; ?>
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio"  <?php if($member_access_denied==1) { echo 'checked'; }?> class="form-check-input" value="1" name="member_access_denied"> <?php echo $xml->string->no; ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label w-100"><?php echo $xml->string->resident_chat_access; ?></label>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" <?php if($chat_access_denied==0) { echo 'checked'; }?> class="form-check-input" value="0" name="chat_access_denied"> <?php echo $xml->string->yes; ?>
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" <?php if($chat_access_denied==1) { echo 'checked'; }?> class="form-check-input" value="1" name="chat_access_denied"> <?php echo $xml->string->no; ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label class="form-control-label w-100"><?php echo $xml->string->timeline; ?> <?php echo $xml->string->access; ?></label>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio"  <?php if($timline_access_denied==0) { echo 'checked'; }?> checked="" class="form-check-input" value="0" name="timline_access_denied"> <?php echo $xml->string->yes; ?>
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio"  <?php if($timline_access_denied==1) { echo 'checked'; }?> class="form-check-input" value="1" name="timline_access_denied"> <?php echo $xml->string->no; ?>
                          </label>
                        </div>
                      </div>
                      <?php } 
                      if ($user_face_data!='') {
                      ?>
                      <div class="col-lg-12">
                        <label class="form-control-label w-100">Attendance Face Data</label>
                            <?php if ($face_data_image!='') { ?>
                            <a href="../img/attendance_face_image/<?php echo $face_data_image; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image"]; ?>"><img width="100" height="100" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $face_data_image; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>

                            <a href="../img/attendance_face_image/<?php echo $face_data_image_two; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image_two"]; ?>"><img width="100" height="100" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $face_data_image_two; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>
                            <?php } 
                            echo  "<br>Face updated on : ".date("d M Y h:i A", strtotime($face_added_date));
                            ?>

                      </div>
                    <?php } ?>
                    </div>
                    <?php if ( $delete_status==0) { ?>
                    <div class="form-group row">
                      <div class="col-lg-12 text-center">
                        <input type="hidden" name="updateProfessional" value="updateProfessional">
                        <input type="submit" id="socAddBtn" class="btn btn-primary" name=""  value="Update">
                      </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>
                <div  class="tab-pane " id="quickLinks"> 
                  <div class="row">
                    <?php
                    $crMonth = date('m');
                    $crYear = date('Y');
                    $crDate = date('Y-m-d');
                    $sDate = date('Y-m-1');
                    $mLastDate = date("Y-m-t", strtotime($crDate)); 
                    ?>
                    <div class="col-md-3 text-center">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="attendances?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&from=<?php echo $sDate; ?>&toDate=<?php echo $mLastDate; ?>" data-toggle="toolip" title="Attendance"><img class="myIcon lazyload" src="assets/images/ic_present.png" data-src="assets/images/ic_present.png"><br>
                        <label>Attendance</label>
                       </a>
                      
                      </div>
                      
                    </div>
                    <div class="col-md-3 text-center">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="attendanceCalendar?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&month=<?php echo $crMonth; ?>&laYear=<?php echo $crYear; ?>" data-toggle="toolip" title="Month Attendance"><img class="myIcon lazyload" src="assets/images/ic_calendar_working_days.png" data-src="assets/images/ic_calendar_working_days.png">
                        <br>
                        <label>Month Atten.</label>
                      </a>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="pendingAttendance?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&from=<?php echo $sDate; ?>&toDate=<?php echo $mLastDate; ?>" data-toggle="toolip" title="Pending Attendance"><img class="myIcon lazyload" src="assets/images/pending_attendance.png" data-src="assets/images/pending_attendance.png">
                        <br>
                        <label>Pending Atten.</label>
                      </a>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="punchMissing?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&from=<?php echo $sDate; ?>&toDate=<?php echo $mLastDate; ?>" data-toggle="toolip" title="Punch Out Missing"><img class="myIcon lazyload" src="assets/images/time_missing.png" data-src="assets/images/time_missing.png">
                        <br>
                        <label>Punch Out Miss</label>
                      </a>
                      </div>
                    </div>
                    <div class="col-md-3 text-center mt-2">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="leaves?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&month=<?php echo $crMonth; ?>&year=<?php echo $crYear; ?>" data-toggle="toolip" title="Leave Request"><img class="myIcon lazyload" src="assets/images/leave_re.png" data-src="assets/images/leave_re.png">
                        <br>
                        <label>Leave Request</label>
                      </a>
                      </div>
                    </div>
                    <div class="col-md-3 text-center mt-2">
                        <div class=" bg-light-success text-success p-2 rounded">
                        <a href="leaveAssign?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&ltId=&year=<?php echo $crYear; ?>" data-toggle="toolip" title="Leave Balance"><img class="myIcon lazyload" src="assets/images/leave.png" data-src="assets/images/leave.png">
                        <br>
                        <label>Leave Balance</label>
                      </a>
                      </div>
                    </div>
                    <?php if ($track_user_location==1) { ?>
                    <div class="col-md-3 text-center mt-2">
                      <div class=" bg-light-success text-success p-2 rounded">
                          

                        
                        <form  action="controller/userController.php" method="post">
                            <input type="hidden" name="bId" value="<?php echo $block_id; ?>">
                            <input type="hidden" name="dId" value="<?php echo $floor_id; ?>">
                            <input type="hidden" name="uId" value="<?php echo $id; ?>">
                            <input type="hidden" name="viewLocationLog" value="viewLocationLog">
                            <button type="submit" class="btn btn-sm btn-link ml-1"><label class="mb-0"><img class="myIcon lazyload" src="assets/images/live_tracker_png.png" data-src="assets/images/live_tracker_png.png">
                        <br>Track User </label></button>
                        </form>
                      
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-3 text-center mt-2">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="salaryPublished?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>" data-toggle="toolip" title="Salary Slip"><img class="myIcon lazyload" src="assets/images/salary_quick_link.png" data-src="assets/images/salary_quick_link.png">
                        <br>
                        <label>Salary Slip</label>
                      </a>
                        </div>
                    </div>
                    <div class="col-md-3 text-center mt-2">
                      <div class=" bg-light-success text-success p-2 rounded">
                        <a href="viewWorkReport?bId=<?php echo $block_id; ?>&dId=<?php echo $floor_id; ?>&uId=<?php echo $id; ?>&from=<?php echo $sDate; ?>&toDate=<?php echo $mLastDate; ?>" data-toggle="toolip" title="Work Report"><img class="myIcon lazyload" src="assets/images/work_report.png" data-src="assets/images/work_report.png">
                        <br>
                        <label>Work Report</label>
                      </a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane "id="pendiAndDues" >
                <div class="table-responsive pendiAndDues">
                    <table class="table align-items-center table-bordered ">
                      <thead>
                        <tr>
                          <th>Title</th>
                          <th>Due Amount</th>
                        </tr>
                      </thead>
                      <tbody id="pendiAndDuesData">
                        
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="team">
                  <div class="row m-0" id="teamMembersData">
                    
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <!-- Agreement Section -->
       
<?php  } else {
  echo "<img width='250' src='img/no_data_found.png'>";
} ?>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInp").change(function() {
    readURL(this);
  });
  
</script>




<div class="modal fade" id="addCommercialUser">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->commercial_entry_portal; ?> <?php echo $xml->string->user; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addUserCommercial" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="addComercialUser" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $unit_id;?>" name="unit_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_id;?>" name="user_id" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->name; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="name" id="name" class="form-control">
                    </div>
                    
                </div>
             </div> 
            <div class="col-md-12">
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->mobile_no; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" name="phone"  id="phone" class="form-control number" required="" autocomplete="off" minlength="8" maxlength="15">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                   <label class="col-sm-3 text-uppercase"><?php echo $xml->string->password; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="password" name="password" id="password" class="form-control" required="" autocomplete="off">
                    </div>
                </div>
            </div>
            
            
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?></button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="addNote">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->note; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addNoteForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="addNote" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $unit_id;?>" name="unit_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_id;?>" name="user_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $device;?>" name="device" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_token;?>" name="user_token" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->add_comment_title; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="note_title" id="note_title" class="form-control">
                    </div>
                    
                </div>
             </div> 
            <div class="col-md-12">
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->description; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <textarea name="note_description"  id="note_description" class="form-control" required="" autocomplete="off" minlength="8" maxlength="500"></textarea>
                    </div>
                </div>
            </div>
          
            
            
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="addEducation">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add . " " . $xml->string->education . " & ". $xml->string->achievements . " " . $xml->string->detail; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addEducationForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="addEducation" value="addEducation">
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_id;?>" name="user_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_token;?>" name="user_token" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>">
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->select; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <select name="status_type_select" id="status_type_select" class="form-control EducationTypeChange">
                          <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                          <option selected value="1">Education</option>
                          <option value="2">Achievement</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase" id="class_name0"><?php echo "Class"; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text"  autocomplete="off" name="class_name" id="class_name" class="form-control">
                    </div>
                  </div>
                </div> 
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase" id="uni_achi_name"><?php echo "University Name"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                            <input type="text" required="" autocomplete="off" name="university_name" id="university_name" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase" id="pass_achi_date"><?php echo "Passed date"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                            <input class="form-control pass_year" id="datepicker-doa" autocomplete="off" name="pass_year" type="text">
                        </div>
                    </div>
                </div>
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addExperience">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add . " " . $xml->string->experience; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addExperienceForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="addExperience" value="addExperience">
                <input type="hidden" required="" autocomplete="off" value="<?php echo $id;?>" name="user_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_token;?>" name="user_token" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->company_name; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="company_name" id="company_name" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->designation; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="designation" id="designation" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo "Work From"; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input readonly="" class="form-control" onchange="changeWfDate(this.value);" id="new_datepicker-wf-2" autocomplete="off" name="work_from" type="text">
                    </div>
                  </div>
                </div> 
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase"><?php echo "Work TO"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                          
                          <input type="hidden" name="joining_date_validate" id="joining_date_validate" value="<?php if($proData['joining_date'] !="0000-00-00"){echo $proData['joining_date'];} else{ echo date('Y-m-d'); } ?>">
                            <input readonly="" class="form-control" id="datepicker-wt" autocomplete="off" name="work_to" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase"><?php echo "Company Location"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                            <input class="form-control" id="company_location" autocomplete="off" name="company_location" type="text">
                        </div>
                    </div>
                </div>
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addCurrentExperience">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add . " " . $xml->string->experience; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addNewExperienceForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="add_letest_experience" value="add_letest_experience">
                <input type="hidden" required="" autocomplete="off" value="<?php echo $id;?>" name="user_id" >
                <input type="hidden" required="" autocomplete="off" value="<?php echo $user_token;?>" name="user_token" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->designation; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="designation"  class="form-control">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo "Work From"; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input readonly="" class="form-control" id="new_datepicker-wf" autocomplete="off" name="joining_date" id="joining_date" type="text">
                        <input readonly="" class="form-control" id="new_datepicker-wf" autocomplete="off" name="joining_date_v" value="<?php if($proData['joining_date'] !="0000-00-00"){echo $proData['joining_date'];} else{ echo date('Y-m-d'); } ?>" id="joining_date_v" type="hidden">
                    </div>
                  </div>
                </div> 
                <!-- <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase"><?php echo "Work TO"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                           <input readonly="" class="form-control" id="new_datepicker-wt" autocomplete="off" name="end_date" type="text">
                        </div>
                    </div>
                </div> -->
                <div class="col-md-12">
                    <div class="row">
                    <label class="col-sm-6 text-uppercase">Salary Increment<span class="required">*</span></label>
                      <input type="radio" name="salary_incrment" value="0">
                        <label class="col-sm-2 text-uppercase">Yes</label>
                      <input type="radio" checked name="salary_incrment" value="1">
                        <label class="col-sm-2 text-uppercase">No</label>
                       
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase">Remark </label>
                        <div class="form-group col-sm-9">
                            <textarea  class="form-control" autocomplete="off" name="remark"></textarea>
                        </div>
                    </div>
                </div>
                
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="notificationSingle">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->send; ?> <?php echo $xml->string->notifications; ?>  <?php echo $xml->string->to; ?> <?php echo $user_full_name; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="notificationValidationSingle" action="controller/notificationController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="send_to" value="3">
          <input type="hidden" name="user_name" value="<?php echo $user_full_name; ?>">
          <input type="hidden" name="user_token" value="<?php echo $user_token; ?>">
          <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
          <input type="hidden" name="bId" value="<?php echo $block_id; ?>">
          <input type="hidden" name="device" value="<?php echo $device; ?>">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->add_comment_title; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input required="" maxlength="200" id="title" type="text" name="title"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->description; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <textarea maxlength="500" required=""  name="description"  class="form-control"></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->photo; ?> </label>
            <div class="col-sm-8">
              <input type="file" accept="image/*" maxlength="500" name="notiUrl"  class="form-control-file border"></textarea>
            </div>
          </div>
        
          
          <div class="form-footer text-center">
            <input type="hidden" name="blockAppendQueryOnly" value="<?php echo $blockAppendQueryOnly; ?>">
            <button type="submit" name="sendNotiSingleUser" value="sendNotiSingleUser" class="btn  btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->send; ?></button>
          </div>

        </form> 
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="changeDepartmentModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Change Department</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
              <!-- mahi -->
                <div class="col-md-12">
                <form id="changeDepartmentForm" action="controller/userController.php" name="changeDepartmentForm"  enctype="multipart/form-data" method="post">
                    <!-- <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                        <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                          <option value="">-- Select Branch --</option> 
                            <?php 
                              $qb=$d->select("block_master","society_id='$society_id'");  
                              while ($blockData=mysqli_fetch_array($qb)) { ?>
                          <option  <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                          <?php } ?>
                          </select>
                        </div>                   
                    </div>  
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Department <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                        <select name="floor_id"  class="form-control single-select floor_id" onchange="getSubDepartmentByFloorId(this.value); getShiftByFloorId(this.value);" required>
                          <option value="">-- Select Department --</option> 
                            <?php 
                              $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND block_master.block_id='$block_id'");  
                              while ($depaData=mysqli_fetch_array($qd)) { ?>
                          <option  <?php if($floor_id==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                          <?php } ?>
                          </select>
                        </div>                   
                    </div> 
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Shift </label>
                        <div class="col-lg-12 col-md-12" id="">
                        <select name="shift_time_id" id="shift_time_id"  class="form-control single-select" >
                          <option value="">-- <?php echo $xml->string->select; ?> Shift --</option>
                          <?php
                              $sq=$d->select("shift_timing_master","society_id='$society_id' AND floor_id='$floor_id'");
                              while ($shiftData=mysqli_fetch_array($sq)) {
                                $arData = array();
                                for($i=0; $i<count($week_days); $i++){
                                  if($shiftData['week_off_days'] !="")
                                  {
                                    $week_off_days = explode(',',$shiftData['week_off_days']);
                                    if(in_array($i, $week_off_days)){
                                      array_push($arData,$week_days[$i]);
                                    } 
                                  }
                                }
                                if(!empty($arData)){
                                  $showOff = implode(',',$arData);
                                }
                                else
                                {
                                  $showOff = "";
                                }
                                ?>
                                <option <?php if($shift_time_id == $shiftData['shift_time_id']){echo "selected";} ?> value="<?php echo $shiftData['shift_time_id'];?>"> <?php echo $shiftData['shift_start_time'];?> - <?php echo $shiftData['shift_end_time'];?> <?php if($showOff !="") {echo "("  .$showOff." )"; } ?></option>
                              <?php } ?>
                          </select>
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Sub Department </label>
                        <div class="col-lg-12 col-md-12" id="">
                        <select name="sub_department_id" id="sub_department_id" class="form-control single-select" >
                          <option value="">-- <?php echo $xml->string->select; ?> Sub <?php echo $xml->string->floor; ?> --</option>
                          <?php
                              $sdq=$d->select("sub_department","society_id='$society_id' AND floor_id='$floor_id'");
                              while ($sdqData=mysqli_fetch_array($sdq)) { ?>
                                <option <?php if($sub_department_id== $sdqData['sub_department_id']) { echo 'selected';} ?>  value="<?php echo $sdqData['sub_department_id'];?>"> <?php echo $sdqData['sub_department_name'];?></option>
                              <?php } ?>
                          </select>
                        </div>                   
                    </div>               
                    <div class="form-footer text-center">
                      <input type="hidden" name="changeDepartment"  value="changeDepartment">
                      <input type="hidden" name="user_id" value="<?php echo $id; ?>">
                      <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update </button> 
                    </div> -->
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="addBankModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">User Bank</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">
          <form id="addUserBank" name="addUserBank" action="controller/UserBankController.php" enctype="multipart/form-data" method="post">
          <input type="hidden" name="block_id" id="block_id" value="<?php echo $userData['block_id']; ?>" class="addRest">
          <input type="hidden" name="floor_id" id="floor_id" value="<?php echo $userData['floor_id']; ?>" class="addRest">
          <input type="hidden" id="user_id" value="<?php echo $id; ?>" type="text" required=""  name="user_id">
            <input type="hidden" name="user_id_old" id="user_id_old">
            <input type="hidden" name="floor_id_old" id="floor_id_old">

            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account Holder's Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="account_holders_name" id="account_holders_name" value="<?php echo $userData['user_full_name']; ?>" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="bank_name" id="bank_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Branch Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="bank_branch_name" id="bank_branch_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account Type<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
               <select name="account_type" id="account_type" class="form-control" >
                 <option value="Current">Current</option>
                 <option value="Saving">Saving</option>
               </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account No <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="account_no" id="account_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">IFSC Code <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="11" style="text-transform:uppercase;" name="ifsc_code" id="ifsc_code" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Customer Id/CRN No. </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="crn_no" id="crn_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">ESIC No </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="10" name="esic_no" id="esic_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Pan Card No </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="10" style="text-transform:uppercase;" name="pan_card_no" id="pan_card_no" class="form-control">
              </div>
            </div>
            <div class="form-footer text-center">
              <input type="hidden" id="bank_id" name="bank_id" value="<?php if ($data['bank_id'] != "") {
                                                                        echo $data['bank_id'];
                                                                      } ?>">
              <button  type="submit" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addEmployeeBank" value="addEmployeeBank">

              <button id="addHrDocumentBtn" type="submit" class="btn addUserBank btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

              <button type="reset" value="add" class="btn btn-danger  cancel hideAdd" onclick="resetFrm('addHrDocumentAdd');"><i class="fa fa-check-square-o"></i> Reset</button>

            </div>

          </form>

        </div>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="addIdProofModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->note; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addIdProofForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            
            <div class="col-md-12">
                <input type="hidden" name="addIdProof" value="addIdProof">
                <input type="hidden" value="<?php echo $user_id;?>" name="user_id" >
                <input type="hidden" value="" name="id_proof_pages" id="id_proof_pages">
                <div class="row">
                    <label class="col-sm-3 text-uppercase">ID Proof Type <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <select name="id_proof_id" id="id_proof_id" class="form-control single-select" onchange="selectIdProofDocument(this.value)" required>
                            <option value="">-- Select ID Proof Type --</option> 
                            <?php 
                            $idpq=$d->select("id_proof_master","society_id='$society_id' AND id_proof_status=0");  
                            while ($idProofData=mysqli_fetch_array($idpq)) {
                            ?>
                            <option data-id="<?php echo  $idProofData['id_proof_pages'];?>" value="<?php echo  $idProofData['id_proof_id'];?>" ><?php echo $idProofData['id_proof_name'];?></option>
                            <?php } ?>
                        </select>  
                    </div>
                    
                </div>
             </div> 
            <div class="col-md-12">
                <div class="row" id="id_prrof_document_div">
                    
                </div>
            </div>
          
            
            
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>

<script type="text/javascript">

function buttonSettings() {
    $('#addUserBank').trigger('reset');
    $('.hideupdate').hide();
    $('.hideAdd').show();
   
    $('#bank_id').val("");
    $('.addRest').val("");
    //$('.floor_id').val(response.bank.floor_id).trigger("change", true);

   
    //$('.addRest').val("").trigger("change", true);

  }

  $().ready(function() 
  {
    var latitude = '<?php echo $user_latitude; ?>';
    var longitude = '<?php echo $user_longitude; ?>';

    initialize(latitude,longitude);
  });
  
  function initialize(d_lat,d_long) 
  {
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };
       
    var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    autocomplete10.addListener('place_changed', function() 
    {
      infowindow.close();
      marker.setVisible(false);
      var place5 = autocomplete10.getPlace();
      if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
      }
    
      // If the place5 has a geometry, then present it on a map.
      if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
      } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
      }
     
      marker.setPosition(place5.geometry.location);
      marker.setVisible(true);          
      
      var pincode="";
      for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
          if (place5.address_components[i].types[j] == "postal_code") {
            pincode = place5.address_components[i].long_name;
          }
        }
      }
      bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
      infowindow.setContent(place5.formatted_address);
      infowindow.open(map, marker);
       
    });
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() 
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) 
      {
        if (status == google.maps.GeocoderStatus.OK) 
        {
          if (results[0]) 
          { 
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++) 
            {
              for (var j = 0; j < places.address_components[i].types.length; j++) 
              {
                if (places.address_components[i].types[j] == "postal_code") 
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }

  function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
  {
    document.getElementById('user_latitude').value = lat;
    document.getElementById('user_longitude').value = lng;
  }

 
function change_marital_status(value) {
  if(value==2){
    $('.hide_wedding_anniversary_date').removeClass('d-none');
  }else
  {
    $('.hide_wedding_anniversary_date').addClass('d-none');
  }
}

function selectIdProofDocument(value){
  var option = $('option:selected', '#id_proof_id').attr('data-id');
  var doc_content = ``;
  for (var i = 0; i < option; i++) {
    var indexCount = '';
    if(option > 1){
      indexCount = 'Page '+ (i + 1);
    }
    doc_content += 
    `<label class="col-sm-3 text-uppercase">ID Proof `+indexCount+` <span class="required">*</span></label>
    <div class="form-group col-sm-9">
        <input name="id_proof[`+i+`]" id="id_proof_`+i+`" accept=".png, .jpg, .jpeg, .doc, .docx, .pdf" class="idProof form-control id_proof_validation" type="file"> 
        <i>Only Photo & Pdf formats are allowed </i>
    </div>`;
  }
  $('#id_proof_pages').val(option);
  $('#id_prrof_document_div').html(doc_content);
  addRule();
}


function addRule() {
    $(".id_proof_validation").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            messages: {
                    required: "Please Select File ",
                }
        });
    });
}
</script>
