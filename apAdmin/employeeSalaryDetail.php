<?php
error_reporting(0);
/* $currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year')); */

$uId = (int)$_REQUEST['uId'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
$sId = (int)$_REQUEST['sId'];
$increment = (int) $_REQUEST['increment'];
  if(isset($sId) && $sId>0){
  $q2 = $d->selectRow("salary.*,floors_master.floor_name,users_master.*,user_bank_master.*,user_employment_details.joining_date","salary,floors_master,users_master LEFT JOIN user_bank_master ON user_bank_master.user_id=users_master.user_id LEFT JOIN user_employment_details ON  user_employment_details.user_id = users_master.user_id", "salary.user_id=users_master.user_id AND salary.floor_id=floors_master.floor_id AND salary.society_id='$society_id' AND salary.salary_id='$sId' AND salary.is_delete='0' AND users_master.delete_status=0  $deptFilterQuery  $UserFilterQuery $blockAppendQueryUser", "ORDER BY salary.salary_id DESC");
    $checkSalaryData = mysqli_fetch_assoc($q2);
   

  }

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title">Employee CTC Detail</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        
      </div>
    </div>
    
    <?php if (isset($sId) && $sId > 0) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">

          <div class="row ">
          
          <div class="col-md-6  "  style="width: 50%;">
            <h4><?php echo $checkSalaryData['user_full_name']; ?></h4>
            <label>Employee Code</label> : <?php if(isset($checkSalaryData['company_employee_id']) && $checkSalaryData['company_employee_id'] !=""){ echo $checkSalaryData['company_employee_id']; }  ?> <br>
           
            <label>Contact No. </label> :<?php if(isset($checkSalaryData['user_mobile']) && $checkSalaryData['user_mobile'] !=""){ echo $checkSalaryData['country_code']." ".$checkSalaryData['user_mobile']; }  ?> <br>
            <label>Department</label> :<?php if(isset($checkSalaryData['floor_name']) && $checkSalaryData['floor_name'] !=""){ echo $checkSalaryData['floor_name']; }  ?> <br>
            <label>Designation</label> :<?php if(isset($checkSalaryData['user_designation']) && $checkSalaryData['user_designation'] !=""){ echo $checkSalaryData['user_designation']; }  ?> <br>
          </div>
          <div class="col-md-6  text-left" style="width: 50%;">
            <label> Email Id. </label>:<?php if(isset($checkSalaryData['user_email']) && $checkSalaryData['user_email'] !=""){ echo $checkSalaryData['user_email']; }  ?> <br>
            <label> Date of Joining</label> :<?php  if(isset($checkSalaryData['joining_date']) && $checkSalaryData['joining_date'] !="" && $checkSalaryData['joining_date'] !="0000-00-00"){ echo date('d M Y',strtotime($checkSalaryData['joining_date'])); }  ?> <br>

          </div>
        </div>
      </div>
       					

          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
              <div class="table-responsive">
              <table id="" class="table table-bordered">
                <thead>
                  <tr>
                    <th >Earnings</th>
                    <th colspan="2" class="text-center">CTC </th>
                    <th colspan="2" class="text-center">salary </th>
                    <th colspan="3" class="text-center">Deduction </th>
                  </tr>
                  <tr>
                    <th >Head Name</th>
                    <th class="text-right"> Per Month</th>
                    <th class="text-right"> Per Annum</th>
                    <th class="text-right"> Per Month</th>
                    <th class="text-right">Per Annum</th>
                    <th>Head Name</th>
                    <th class="text-right"> Per Month</th>
                    <th class="text-right">Per Annum</th>                   
                    
                  </tr>
                </thead>
                  <tbody>
                    <?php
                    $i = 1;

                    
                    if (isset($sId) && $sId > 0) {
                      $salaryFilter = " AND salary.salary_id='$sId'";
                    }

                    $q = $d->selectRow("salary_master.*,salary.*,salary_earning_deduction_type_master.earning_deduction_name,salary_earning_deduction_type_master.earning_deduction_type","salary_master LEFT JOIN salary ON salary.salary_id = salary_master.salary_id LEFT JOIN salary_earning_deduction_type_master ON salary_earning_deduction_type_master.salary_earning_deduction_id=salary_master.salary_earning_deduction_id", "salary.society_id='$society_id'  AND salary.is_delete='0'   $salaryFilter ", "ORDER BY salary_master.salary_id DESC");
                    $counter = 1;
                    $earnAr = array();
                    $newEarnAr = array();
                    $deductAr = array();
                    $contribution = array();
                    $deduct = 0;
                    while ($data = mysqli_fetch_array($q)) {
                      $total_ctc= $data['total_ctc'];
                      $gross_salary= $data['gross_salary'];
                      $net_salary= $data['net_salary'];
                      if($data['earning_deduction_type']==0){
                        $data['contribution']=0;
                        array_push($earnAr,$data);
                      }else{
                          $deduct = $deduct+$data['salary_earning_deduction_value'];
                        $newQ  =$d->selectRow('salary_common_value_master.*','salary_common_value_master',"salary_common_value_master.salary_group_id=$data[salary_group_id] AND salary_common_value_master.salary_earning_deduction_id=$data[salary_earning_deduction_id]",'');
                        $newData = mysqli_fetch_assoc($newQ);
                       
                        if($newData){
                        
                          if((float)$newData['amount_value_employeer']>0){
                           
                            if($newData['amount_type']==0){
                              $newArData =array();
                              $newArData['earning_deduction_name'] = $data['earning_deduction_name'];
                              $newArData['contribution'] = 1;
                              $newArData['salary_earning_deduction_value'] =($data['gross_salary']*$newData['amount_value_employeer'])/100;
                              array_push($contribution,$newArData);
                            }
                          }
                        }
                        array_push($deductAr,$data);
                      }
                    
                    }
                    $newEarnAr = array_merge($earnAr,$contribution);
                      $earncount = count($newEarnAr);
                      $deductcount = count($deductAr);
                      if($earncount>$deductcount){
                        $count =$earncount; 
                      }else{
                        $count =$deductcount; 
                      }
                  ///  foreach($earnAr as $val){
                    for ($i=0; $i <$count ; $i++) { 
                     
                    ?>

                      <tr>
                        <td >
                        <?php if($newEarnAr[$i]['contribution']==0){ 
                           echo $newEarnAr[$i]['earning_deduction_name']; 
                          }else{
                            echo $newEarnAr[$i]['earning_deduction_name']." Employer's Contribution"; 
                          }
                           ?>                         
                        </td>
                        
                        <td class="text-right"><?php echo number_format((float)$newEarnAr[$i]['salary_earning_deduction_value'], 2, '.', '');  ?></td>
                        <td class="text-right"><?php echo number_format((float)($newEarnAr[$i]['salary_earning_deduction_value']*12), 2, '.', '');  ?></td>
                        <?php if($newEarnAr[$i]['contribution']==0){ ?>
                        <td class="text-right"><?php echo $newEarnAr[$i]['salary_earning_deduction_value']; ?></td>
                        <td class="text-right"><?php echo number_format((float)($newEarnAr[$i]['salary_earning_deduction_value']*12), 2, '.', '');  ?></td>
                        <?php }else{ ?>
                          <td></td>
                          <?php } ?>

                        <td ><?php echo $deductAr[$i]['earning_deduction_name']; ?></td>
                        <td class="text-right"><?php echo $deductAr[$i]['salary_earning_deduction_value']; ?></td>
                        <td class="text-right"><?php echo $deductAr[$i]['salary_earning_deduction_value']*12; ?></td>
                                        
                                            </tr>
                    <?php } 
                   
                    ?>

                      <tr>
                        <td ><b>Total</b></td>
                        <td class="text-right"><?php echo number_format((float)($total_ctc), 2, '.', ''); ?></td>
                        <td class="text-right"><?php echo number_format((float)($total_ctc*12), 2, '.', ''); ?></td>
                        <td class="text-right"><?php echo number_format((float)($gross_salary), 2, '.', ''); ?></td>
                        <td class="text-right"><?php echo number_format((float)($gross_salary*12), 2, '.', ''); ?></td>
                        <td ><b>Total</b></td>
                        <td class="text-right"><?php echo number_format((float)($deduct), 2, '.', ''); ?></td>
                        <td class="text-right"><?php echo number_format((float)($deduct*12), 2, '.', ''); ?></td>
                      </tr>
                      <tr>
                        <td colspan="5"><b>Net Salary Monthly In Hand (<?php  $netsalaryinWord = changeAmountInWords($net_salary);
                              echo ucfirst($netsalaryinWord);
                            ?>)</b></td>
                        <td colspan="3" class="text-right"><b><?php echo number_format((float)($net_salary), 2, '.', ''); ?></b></td>
                      </tr>
                      <tr>
                        <td colspan="5"><b>Net Salary Annum In Hand (<?php  $netsalaryinWord = changeAmountInWords($net_salary*12);
                              echo ucfirst($netsalaryinWord);
                            ?>)</b></td>
                        <td colspan="3" class="text-right"><b><?php echo number_format((float)($net_salary*12), 2, '.', ''); ?></b></td>
                      </tr>
              
                  </tbody>
              </table>

             
            </div>
              </div>
              
            </div>
           
          </div>
        </div>
      </div>
    </div><!-- End Row-->
    <?php } else { 
      ?>
       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <span> NOTE : Please select salary group</span>
            </div>
          </div>
        </div>
       </div>
      <?php } ?>
  </div>
</div>



<div class="modal fade" id="salaryModal">
  <div class="modal-dialog" style="max-width: 90%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary CTC Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div id="salaryData"></div>
        <div class="col-md-12 row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Earning</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="earnData">
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th> Deduction Name</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="deductData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
                  <label for="exampleInputEmail1">Net Salary : </label>
                    <span id="netSalary"></span>
                  </div>
      </div>
    </div>
  </div>
</div>

<?php
function changeAmountInWords($number){

  $no = floor($number);
  $point = round($number - $no, 2) * 100;
  $hundred = null;
  $digits_1 = strlen($no);
  $i = 0;
  $str = array();
  $words = array('0' => '', '1' => 'one', '2' => 'two',
   '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
   '7' => 'seven', '8' => 'eight', '9' => 'nine',
   '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
   '13' => 'thirteen', '14' => 'fourteen',
   '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
   '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
   '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
   '60' => 'sixty', '70' => 'seventy',
   '80' => 'eighty', '90' => 'ninety');
  $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
  while ($i < $digits_1) {
    $divider = ($i == 2) ? 10 : 100;
    $number = floor($no % $divider);
    $no = floor($no / $divider);
    $i += ($divider == 10) ? 1 : 2;
    if ($number) {
       $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
       $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
       $str [] = ($number < 21) ? $words[$number] .
           " " . $digits[$counter] . $plural . " " . $hundred
           :
           $words[floor($number / 10) * 10]
           . " " . $words[$number % 10] . " "
           . $digits[$counter] . $plural . " " . $hundred;
    } else $str[] = null;
 }
 $str = array_reverse($str);
 $result = implode('', $str);
 $points = ($point) ?
   "." . $words[$point / 10] . " " . 
         $words[$point = $point % 10] : '';
 return $result ."only.";
}
?>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  //////////////////////holiday data
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
</script>