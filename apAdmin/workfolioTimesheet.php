<?php error_reporting(0);
$tId = $_REQUEST['tId'];
$eId = $_REQUEST['eId'];
$date = $_REQUEST['date'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Workfolio Employess</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
      </div>

        <form class="workfolioFilterForm" action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <select name="tId" class="form-control single-select" required="" onchange="getWorkfolioEmployees(this.value)">
                        <option value="">-- Select Team --</option> 
                        <?php 
                            $qt=$d->select("workfolio_team","");  
                            while ($teamData=mysqli_fetch_array($qt)) {
                        ?>
                        <option  <?php if($tId==$teamData['workfolio_team_id']) { echo 'selected';} ?> value="<?php echo $teamData['workfolio_team_id'];?>" ><?php echo $teamData['teamName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				        <div class="col-md-3 form-group">
                    <select name="eId" id="eId" class="form-control single-select" required="">
                        <option value="">-- Select Team Employee --</option> 
                        <?php 
                            $qte=$d->select("workfolio_employees,workfolio_team","workfolio_employees.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$tId'");  
                            while ($teamEmpData=mysqli_fetch_array($qte)) {
                        ?>
                        <option  <?php if($eId==$teamEmpData['workfolio_employee_id']) { echo 'selected';} ?> value="<?php echo $teamEmpData['workfolio_employee_id'];?>" ><?php echo $teamEmpData['displayName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				        <div class="col-md-3 form-group">
					        <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" readonly name="date" value="<?php if ($date!='') { echo $date;} else{echo date('Y-m-d');} ?>">
                </div>
                
                <div class="col-md-3 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
		<div class="row">
      <?php
				if (isset($tId) && $tId>0 && isset($eId) && $eId>0) {

			  	$q = $d->selectRow("workfolio_timesheet.*, workfolio_employees.displayName,workfolio_employees.profilePictureUrl","workfolio_timesheet, workfolio_employees", "workfolio_timesheet.email=workfolio_employees.email AND workfolio_employees.workfolio_employee_id='$eId' AND workfolio_timesheet.date = '$date'", "");
			 	
				if(mysqli_num_rows($q)>0){
			  	$data = mysqli_fetch_array($q);
			?>                        
        <div class="col-12 col-lg-6 col-xl-6">
          <div class="card gradient-scooter no-bottom-margin radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div class="text-success">
                    <img width="60" height="60" class="rounded-circle shadow lazyload" onerror="this.src='img/user.png'" src="<?php echo $data['profilePictureUrl']; ?>">
                </div>
                <div class="pl-3">
                  <h4 class="mb-0 text-white"><?php echo $data['displayName']; ?></h4>
                  <marquee scrollamount="3" class="mb-0 text-white"><?php echo $data['email']; ?></marquee>
                  <!-- <p class="mb-0 text-white">shubhamdubey.dollop@gmail.com</p> -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Total Worked Hours</p>
                    <?php
                      $init = $data['workedSec'];
                      $hours = floor($init / 3600);
                      $minutes = floor(($init / 60) % 60);
                      $seconds = $init % 60;
                    ?>
                  <h4 class="my-1"><?php echo sprintf('%02d', $hours).'h:'.sprintf('%02d', $minutes).'m'; ?></h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Active Time</p>
                    <?php
                      $init = $data['activeSec'];
                      $hours = floor($init / 3600);
                      $minutes = floor(($init / 60) % 60);
                      $seconds = $init % 60;
                    ?>
                  <h4 class="my-1"><?php echo sprintf('%02d', $hours).'h:'.sprintf('%02d', $minutes).'m'; ?></h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Productive Apps %</p>
                    <?php if($data['productiveSec']>0){
                      $productive_pre = round((($data['productiveSec']+$data['untrackedSec'])/$data['workedSec'])*100);
                    }else{
                      $productive_pre = 0;
                    }  
                    ?>
                  <h4 class="my-1"><?php echo $productive_pre; ?>%</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Idle Time Spent %</p>
                    <?php if($data['idleSec']>0){
                      $idle_pre = round(($data['idleSec']/$data['workedSec'])*100);
                    }else{
                      $idle_pre = 0;
                    }  
                    ?>
                  <h4 class="my-1"><?php echo $idle_pre; ?>%</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Check-in Time</p>
                  <h4 class="my-1"><?php if($data['in_time'] != '0000-00-00 00:00:00'){ echo date("h:i A", strtotime($data['in_time']));}else{echo '-';} ?></h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-6 col-xl-3">
          <div class="card radius-10">
            <div class="p-2">
              <div class="d-flex align-items-center">
                <div>
                  <p class="mb-0">Check-Out Time</p>
                  <h4 class="my-1"><?php if($data['out_time'] != '0000-00-00 00:00:00'){echo date("h:i A", strtotime($data['out_time']));}else{ echo '-';} ?></h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }else{ ?>
        <div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> No Data Found!</span>
					</div>
				</div>
      <?php } } else{ ?>
        <div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> Please Select Team Employee</span>
					</div>
				</div>
      <?php } ?>
	  </div><!-- End Row-->

  </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
