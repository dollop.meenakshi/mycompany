<?php
// error_reporting(0);
//$month_year= $_REQUEST['month_year'];
//$laYear = $_REQUEST['laYear'];
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];

$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
if (!isset($_GET['month_year'])) {
  $month_year = $currentMonth;
}
if (!isset($_GET['laYear'])) {
  $laYear = $currentYear;
}

if (!isset($_GET['laYear'])) {
  $monthYear = $currentYear.'-'.$currentMonth;
} else {
  $monthYear = $_GET['laYear'].'-'.$_GET['month_year'];
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Attendance Summary</h4>
      </div>

    </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php');?>
        
        <div class="col-md-3 col-6">
          <select name="laYear" class="form-control" >
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') { echo 'selected';} ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-3 col-6">
          <select class="form-control month_year single-select" name="month_year">
                <option <?php if($_GET['month_year']=="0") { echo 'selected'; } ?> value="0"> Month</option>
                <option <?php if($_GET['month_year']=="01") { echo 'selected'; } ?> <?php if($_GET['month_year'] == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                <option <?php if($_GET['month_year']=="02") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                <option <?php if($_GET['month_year']=="03") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                <option <?php if($_GET['month_year']=="04") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                <option <?php if($_GET['month_year']=="05") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                <option <?php if($_GET['month_year']=="06") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                <option <?php if($_GET['month_year']=="07") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                <option <?php if($_GET['month_year']=="08") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                <option <?php if($_GET['month_year']=="09") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                <option <?php if($_GET['month_year']=="10") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                <option <?php if($_GET['month_year']=="11") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                <option <?php if($_GET['month_year']=="12") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option> 
            </select> 
        </div>
        

        
        <input type="hidden" name="">
        
        <div class="col-lg-2 from-group col-6">
          <input  class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
        </div>


      </div>
    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {echo $_GET['dId']; } ?>">

    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">

            <div class="table-responsive">
              <?php
              extract(array_map("test_input", $_GET));


              if (isset($dId) && $dId > 0) {
                $deptFilterQuery = " AND users_master.floor_id='$dId'";
              }

               if (isset($bId) && $bId > 0) {
                $blockFilterQuery = " AND users_master.block_id='$bId'";
              }
              if (isset($uId) && $uId > 0) {
                $userFilterQuery = " AND users_master.user_id='$uId'";
              }
              if(isset($_GET['laYear']) && $_GET['laYear']>0) {
                $YearFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y')='$_GET[laYear]'";
              }
              
              echo $filterDate; 

            // echo  $monthYear = $_GET['month_year']."-".$_GET['laYear'];

              
              $q = $d->selectRow("*,shift_timing_master.shift_start_time,shift_timing_master.shift_end_time,
              (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
              AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$monthYear'  AND leave_day_type=0 AND leave_status=1) AS total_full_leave ,(SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
              AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$monthYear' AND leave_day_type=1 AND leave_status=1) AS total_half_leave ,(SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
              AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$monthYear' AND leave_type_id!=0 AND paid_unpaid=0 AND leave_day_type=0) AS total_paid_leave_full ,(SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
              AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$monthYear' AND leave_type_id!=0 AND paid_unpaid=0 AND leave_day_type=1) AS total_paid_leave_half,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.punch_in_request=1) AS total_out_missing ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.attendance_date_end='0000-00-00') AS total_out_pending ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id  AND attendance_status=1 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.punch_in_in_range=1) AS total_out_of_range ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id  AND attendance_status=1 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.punch_out_in_range=1) AS total_out_of_range_out ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id  AND attendance_status=1 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_date_end != '0000-00-00' AND attendance_status = '1' AND is_leave!=1) AS total_present ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id  AND attendance_status=1  
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.late_in=1 ) AS total_late_in ,
              (SELECT COUNT(*) FROM attendance_master WHERE attendance_master.user_id = users_master.user_id   AND attendance_status=1 
              AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$monthYear' AND attendance_master.early_out=1 ) AS early_out,
              (SELECT COUNT(*) FROM attendance_punch_out_missing_request WHERE attendance_punch_out_missing_request.user_id = users_master.user_id 
              AND DATE_FORMAT(attendance_punch_out_missing_request.attendance_date,'%Y-%m')='$monthYear'  ) AS out_miss
              ","block_master,floors_master,users_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id =users_master.shift_time_id ","users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id  AND  users_master.user_status=1 AND  users_master.active_status =0 AND  users_master.delete_status=0 AND users_master.shift_time_id!=0 $blockFilterQuery $deptFilterQuery $userFilterQuery $blockAppendQueryUser",'');

              $i = 1;
              ?>

                <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Employee</th>
                      <th>Branch </th>
                      <th>Department</th>
                      <th>Shift in</th>
                      <th>Shift Out</th>
                      <th>Month</th>
                      <th>Present</th>
                      <th>In Out of Range</th>
                      <th>Out Out of Range</th>
                      <th>Late In</th>
                      <th>Early Out</th>
                      <th>Full Day Leave</th>
                      <th>Half Day Leave</th>
                      <th>Paid Full Day</th>
                      <th>Paid Half Day</th>
                      <th>Att. Missing Req.</th>
                      <th>Total Out Req.</th>
                      <th>Pending Out Req</th>
                      <!-- <th>Avg. W.H</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($data = mysqli_fetch_array($q)) {
                      $totalHr = [];
                      $totalSec = [];
                      $totalMin = [];
                      $halfDayLess = $data['total_half_leave']/2;
                     // $total_auto_leave= $data['total_auto_leave']+($total_auto_leave_half/2);
                    ?>
                      <tr>

                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo date('h:i A',strtotime($data['shift_start_time'])); ?></td>
                        <td><?php echo date('h:i A',strtotime($data['shift_end_time'])); ?></td>
                        <td><?php echo date('m-Y',strtotime("01-$month_year-$laYear")); ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'present')"><?php echo $data['total_present']-$halfDayLess; ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_out_of_range')"><?php echo $data['total_out_of_range']; ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_out_of_range_out')"><?php echo $data['total_out_of_range_out']; ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'late_in')"><?php echo $data['total_late_in']; ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'ealry_out')" ><?php echo $data['early_out']; ?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_full_leave')"><?php echo $data['total_full_leave'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_half_leave')"><?php echo $data['total_half_leave'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_paid_leave_full')"><?php echo $data['total_paid_leave_full'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_paid_leave_half')"><?php echo $data['total_paid_leave_half'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'total_out_missing')" ><?php echo $data['total_out_missing'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'out_miss')" ><?php echo $data['out_miss'];?></td>
                        <td style="cursor: pointer;" onclick="showDateDetailsModal('<?php echo $monthYear; ?>',<?php echo $data['user_id']; ?>,'out_miss_pending')" ><?php echo $data['total_out_pending'];?></td>
                        <!-- <td><?php echo $avgWorking ;?></td> -->
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
             

            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="attnSmryDateDataMdl">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Date Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">
          <div class="row col-md-12 " >
          <table class="table table-bordered">
          <thead>
              <tr>
                <th>#</th>
                <th>Date</th>
              </tr>
          </thead>
          <tbody id="attnSmryDateData" >

          </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
