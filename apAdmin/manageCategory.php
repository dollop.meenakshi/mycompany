<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Category</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <a href="addCategory" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Category Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $q = $d->selectRow("pcm.*,pscm.product_sub_category_id,ropm.order_product_id,rpm.product_id","product_category_master AS pcm LEFT JOIN product_sub_category_master AS pscm ON pscm.product_category_id = pcm.product_category_id LEFT JOIN retailer_product_master AS rpm ON rpm.product_category_id = pcm.product_category_id LEFT JOIN retailer_order_product_master AS ropm ON ropm.product_id = rpm.product_id","pcm.product_category_delete = 0","GROUP BY pcm.product_category_id ORDER BY pcm.product_category_id DESC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form action="addCategory" method="post">
                                                    <input type="hidden" name="product_category_id" value="<?php echo $row['product_category_id']; ?>">
                                                    <input type="hidden" name="editCategory" value="editCategory">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if($product_sub_category_id == '' && empty($product_sub_category_id) && $order_product_id == '' && empty($order_product_id) && $product_id == "" && empty($product_id))
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form action="controller/categorySubCategoryController.php" method="POST">
                                                    <input type="hidden" name="product_category_id" value="<?php echo $row['product_category_id']; ?>">
                                                    <input type="hidden" name="deleteCategory" value="deleteCategory">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $category_name; ?></td>
                                        <td><?php if($category_description != ""){ echo $category_description; } ?></td>
                                        <td>
                                            <?php
                                            if($product_category_status == 0)
                                            {
                                            ?>
                                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['product_category_id']; ?>','categoryDeactive');" data-size="small"/>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input type="checkbox" class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['product_category_id']; ?>','categoryActive');" data-size="small"/>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->