
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title">Manage Poll</h4>
      </div>
      <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="addPolling" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deletePoll');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
   
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
           
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Question</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th> For</th>
                    <th>Status</th>
                    <th>Reports</th>
                    <!-- <th>Action</th> -->
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $q=$d->select("voting_master","society_id='$society_id'","ORDER BY voting_id DESC");
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  extract($row);
                  $i++;
                  
                  ?>
                  <tr>
                     <td class='text-center'>
                      <?php

                       $today = date("Y-m-d H:i:s");
                    $voting_option_master_qry=$d->select("voting_option_master","society_id='$society_id' AND voting_id=".$row['voting_id']);
                    $option_count_total=0;
                      while ($voting_option_master_data = mysqli_fetch_array($voting_option_master_qry)) {

                        if($voting_option_master_data['option_count'] >0){
                          $option_count_total++;
                        }
                      }


                       //IS_1041

                      $past_10_days_date=Date('Y-m-d', strtotime("-10 days"));
                      if( (strtotime($past_10_days_date) > strtotime($row['voting_start_date'])) || $option_count_total==0 ) {?>

                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['voting_id']; ?>">
                    <?php }  //IS_1041?>
                    </td>
                    <td><?php echo $i; ?></td>
                    <td><?php echo custom_echo($row['voting_question'],50); ?></td>
                    <td><?php echo  date("d M Y h:i A", strtotime($row['voting_start_date'])); ?></td>
                    <td><?php  echo date("d M Y h:i A", strtotime($row['voting_end_date']));?></td>
                    <td><?php
                       if ($poll_for==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 

                        } else {
                          $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.floor_id='$poll_for'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'].' ('.$floorData['block_name'].')' ;
                        }
                        ?>
                    <td>
                      <?php
                       $cTime= date("H:i:s");
                      $today = strtotime("today $cTime");
                      $expire = strtotime($row['voting_end_date']);
                      if($today > $expire){   ?>
                          Auto Closed
                      <?php }  else {  

                       if($row['voting_status']=="0"){
                      ?>
                        <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['voting_id']; ?>','pollClose');" data-size="small"/>
                        <?php } else { ?>
                       <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['voting_id']; ?>','pollActive');" data-size="small"/>
                      <?php }  }?>
                      
                    </td>
                    <td>
                      <div style="display: inline-block;">
                      <form action="pollResult" method="get">
                          <input type="hidden" name="vId" id="votingData" value="<?php echo $row['voting_id'] ?>">
                        <button type="submit" class="btn btn-primary btn-sm" name="pullingReport">View</button>
                        
                      </form>
                    </div>
                      <?php 
                     //&& (strtotime($row['voting_start_date']) >strtotime($today) || strtotime($row['voting_end_date']) >strtotime($today)   )

                     
                      if($option_count_total==0  && $today < $expire ){
                       ?>
                      
                      <div style="display: inline-block;">
                         <form   class="mr-2" action="addPolling" method="post">
                <input type="hidden" name="voting_id" value="<?php echo $row['voting_id']; ?>">
                 <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                <input type="hidden" name="action" value="Edit">
                <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
              </form>
            </div>
          <?php  } ?>


                      </td>
                   
                    
                    
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->

<script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        function getVogingDetails(voting_id) {
           $('#pullingBox').modal('show');
             $.ajax({
              url: "getPullingReport.php",
              cache: false,
              type: "POST",
              data: {voting_id : voting_id},
              success: function(response){
                  $('#addMainDiv').html(response);
              }
           });
        }

</script>

  <div class="modal fade" id="pullingBox">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Polling Reports</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="addMainDiv">

      </div>
    </div>
  </div>
</div>