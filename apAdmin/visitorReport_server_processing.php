
<?php
    //include connection file 
    include_once("lib/dao.php");
    
    $db=new DbConnect();
    $conn=$db->connect();
    $society_id = $_COOKIE['society_id'];
    

    function custom_echo($x, $length)
    {
      if(strlen($x)<=$length)
      {
        return $x;
      }
      else
      {
        $y=mb_substr($x, 0, $length, 'UTF-8').'...';
        return $y;
      }
    }
 
    extract($_REQUEST);

     
    // initilize all variable
    $params = $columns = $totalRecords = $data = array();

    $params = $_REQUEST;
    //define index of column
    $columns = array( 
        0 =>  'visitor_id',
        1 =>  'visitor_name',
        2 =>  'visitor_mobile',
        3 =>  'block_name',
        4 =>  'user_full_name',
        5 =>  'visit_date',
        6 =>  'exit_date',
        7 => 'visitor_type',
        8 => 'vehicle_no',
        9 => 'visiting_reason',
        10 => 'visitor_status',
        11 => 'entry_time',
        12 => 'entry_by',
        13 => 'exit_by_name',
        
    );

    $where = $sqlTot = $sqlRec = "";

    // codition
    if($block_id =="All"){
        $query = "";
    }else{
        $query = "  AND visitors_master.block_id ='$block_id'";
    }
    $where .= " WHERE visitors_master.society_id='$society_id' $query AND visitors_master.visit_date BETWEEN '$from' AND '$toDate' ";
    // check search value exist
    if( !empty($params['search']['value']) ) {   
        //$where .=" WHERE ";
        $where .=" AND ( visitors_master.visitor_name LIKE '%".$params['search']['value']."%' 
                         OR CONCAT(visitors_master.country_code,' ',visitors_master.visitor_mobile) LIKE '%".$params['search']['value']."%' 
                         OR CONCAT(block_master.block_name,'-',unit_master.unit_name) LIKE '%".$params['search']['value']."%' 
                         OR users_master.user_full_name LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN visitors_master.visitor_status='2' OR visitors_master.visitor_status='3' THEN DATE_FORMAT(CONCAT(visitors_master.visit_date,' ',visitors_master.visit_time),'%d %M %Y %h:%i %p') ELSE '' END ) LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN visitors_master.visitor_status='2' OR visitors_master.visitor_status='3' THEN DATE_FORMAT(CONCAT(visitors_master.exit_date,' ',visitors_master.exit_time),'%d %M %Y %h:%i %p') ELSE '' END ) LIKE '%".$params['search']['value']."%' 
                         OR (CASE 
                                WHEN visitors_master.visitor_type=0 THEN 'Guest'
                                WHEN visitors_master.visitor_type=1 THEN (CASE 
                                                                            WHEN expected_type=2 THEN 'Exp. Delivery'
                                                                            WHEN expected_type=3 THEN 'Exp. Cab'
                                                                            ELSE 'Exp. Guest' END
                                                                          )
                                WHEN visitors_master.visitor_type=2 THEN 'Delivery'
                                WHEN visitors_master.visitor_type=3 THEN 'Cab' 
                                ELSE '' END
                            ) LIKE '%".$params['search']['value']."%' 
                         OR visitors_master.vehicle_no LIKE '%".$params['search']['value']."%' 
                         OR visitors_master.visiting_reason LIKE '%".$params['search']['value']."%' 
                         OR (CASE 
                                WHEN visitors_master.visitor_status=0 THEN 'Pending'
                                WHEN visitors_master.visitor_status=1 THEN 'Approved'
                                WHEN visitors_master.visitor_status=2 THEN 'Entered'
                                WHEN visitors_master.visitor_status=3 THEN (CASE 
                                                                                WHEN exit_date!='' THEN 'Exit'
                                                                                ELSE 'Auto Exit' END
                                                                            )
                                WHEN visitors_master.visitor_status=4 THEN 'Rejected' 
                                WHEN visitors_master.visitor_status=5 THEN 'Deleted' 
                                WHEN visitors_master.visitor_status=6 THEN 'Hold' 
                                ELSE '' END
                            ) LIKE '%".$params['search']['value']."%' 
                         OR DATE_FORMAT(visitors_master.entry_time,'%Y-%m-%d %h:%i %p') LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN em_entry.emp_name!='' THEN CONCAT(em_entry.emp_name,'-Guard') ELSE '' END) LIKE '%".$params['search']['value']."%' 
                         OR (CASE WHEN em_exit.emp_name!='' THEN CONCAT(em_exit.emp_name,'-Guard') ELSE '' END) LIKE '%".$params['search']['value']."%' 
                     )";

    }

    

    // getting total number records without any search
    $sql = "SELECT visitors_master.visitor_id,visitors_master.visitor_name,
            CONCAT(visitors_master.country_code,' ',visitors_master.visitor_mobile) AS visitor_mobile,
            CONCAT(block_master.block_name,'-',unit_master.unit_name) AS block_name,users_master.user_full_name,
            (CASE WHEN visitors_master.visitor_status='2' OR visitors_master.visitor_status='3' THEN DATE_FORMAT(CONCAT(visitors_master.visit_date,' ',visitors_master.visit_time),'%d %M %Y %h:%i %p') ELSE '' END ) AS visit_date,
            (CASE WHEN visitors_master.visitor_status='2' OR visitors_master.visitor_status='3' THEN DATE_FORMAT(CONCAT(visitors_master.exit_date,' ',visitors_master.exit_time),'%d %M %Y %h:%i %p') ELSE '' END ) AS exit_date,
            (CASE 
                WHEN visitors_master.visitor_type=0 THEN 'Guest'
                WHEN visitors_master.visitor_type=1 THEN (CASE 
                                                            WHEN expected_type=2 THEN 'Exp. Delivery'
                                                            WHEN expected_type=3 THEN 'Exp. Cab'
                                                            ELSE 'Exp. Guest' END
                                                          )
                WHEN visitors_master.visitor_type=2 THEN 'Delivery'
                WHEN visitors_master.visitor_type=3 THEN 'Cab' 
                ELSE '' END
            ) AS visitor_type,
            visitors_master.vehicle_no,visitors_master.visiting_reason,
            (CASE 
                WHEN visitors_master.visitor_status=0 THEN 'Pending'
                WHEN visitors_master.visitor_status=1 THEN 'Approved'
                WHEN visitors_master.visitor_status=2 THEN 'Entered'
                WHEN visitors_master.visitor_status=3 THEN (CASE 
                                                                WHEN exit_date!='' THEN 'Exit'
                                                                ELSE 'Auto Exit' END
                                                            )
                WHEN visitors_master.visitor_status=4 THEN 'Rejected' 
                WHEN visitors_master.visitor_status=5 THEN 'Deleted' 
                WHEN visitors_master.visitor_status=6 THEN 'Hold' 
                ELSE '' END
            ) AS visitor_status,
            DATE_FORMAT(visitors_master.entry_time,'%Y-%m-%d %h:%i %p') AS entry_time,
            (CASE WHEN em_entry.emp_name!='' THEN CONCAT(em_entry.emp_name,'-Guard') ELSE '' END) AS entry_by,
            (CASE WHEN em_exit.emp_name!='' THEN CONCAT(em_exit.emp_name,'-Guard') ELSE '' END) AS exit_by_name
            FROM visitors_master
            INNER JOIN users_master ON users_master.user_id = visitors_master.user_id
            INNER JOIN unit_master ON unit_master.unit_id = users_master.unit_id
            INNER JOIN block_master ON block_master.block_id = unit_master.block_id
            LEFT JOIN employee_master AS em_entry ON em_entry.emp_id=visitors_master.emp_id
            LEFT JOIN employee_master AS em_exit ON em_exit.emp_id=visitors_master.exit_by
             ";
    
    $sqlTot .= $sql;
    $sqlRec .= $sql;
    //concatenate search sql if value exist
    if(isset($where) && $where != '') {

        $sqlTot .= $where;
        $sqlRec .= $where;
    }


    $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir'];//."  LIMIT ".$params['start']." ,".$params['length']." ";

    mysqli_set_charset($conn,"utf8mb4");
    
    $queryTot = mysqli_query($conn, $sqlTot) or die("database error:". mysqli_error($conn));


    $totalRecords = mysqli_num_rows($queryTot);
    // print_r($sqlRec);die();
    $queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch visitor report data");

    //iterate on results row and create new index array of data
    $i = 1;
    while( $row = mysqli_fetch_array($queryRecords) ) { 
        
        $data_temp = array();

        // 0-position
        
        $data_temp[] = $i++;

        // 1-position
        
        $data_temp[] = custom_echo($row['visitor_name'],15);
        

        // 2-position
        $mobile="";
        if ($role_id==2 && $row['visitor_mobile']!=0) {
            $mobile =  $row['visitor_mobile']; 
            
        } else if ($row['visitor_mobile']!=0) {
           $mobile =  substr($row['visitor_mobile'], 0, 6) . '*****' . substr($row['visitor_mobile'],  -3);
        }
        $data_temp[] = $mobile;

        // 3-position
        $data_temp[] = $row['block_name'];

        // 4-position
        $data_temp[] = $row['user_full_name'];

        // 5-position
        
        $visit_date = $row['visit_date'];
        $data_temp[] = $visit_date;

        // 6-position
        
        $exit_date = $row['exit_date'];
        $data_temp[] = $exit_date;

        // 7-position
        $visitor_type = $row['visitor_type'];
        $data_temp[] = $visitor_type; 

        // 8-position
        
        $data_temp[] = $row['vehicle_no'];
                          
        // 9-position
        
        $data_temp[] = $row['visiting_reason'];
                          
        // 10-position
        $visitor_status = $row['visitor_status'];
        $data_temp[] = $visitor_status;
                          
        // 11-position
        $entry_time = $row['entry_time'];
        $data_temp[] = $entry_time;
                          
        // 12-position
        $entry_by = $row['entry_by'];
        $data_temp[] = $entry_by;
                          
        // 13-position
        $exit_by_name=$row['exit_by_name'];
        $data_temp[] = $exit_by_name;
        
        array_push($data, $data_temp);
    
    }   
    
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );

    echo json_encode($json_data);  // send data as json format
?>
    