
<?php error_reporting(0);
  
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $bId= $_REQUEST['bId'];
  $dId= $_REQUEST['dId'];
  $key= $_REQUEST['key'];
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-12">
            <?php if(isset($key)){
                if($key=="late_in"){
                    $showKey = "Late";
                }
                else if($key=="present"){
                    $showKey = "Present";
                }
                else if($key=="absent"){
                    $showKey = "Absent";
                }
                else if($key=="early_out"){
                  $showKey = "Early Out";
                }
            } ?>
          <h4 class="page-title"><?php echo $showKey; ?> Attendances :  <?php echo date('d-M-Y');?> </h4>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row pt-2 pb-2">
          <?php // include('selectBranchDeptForFilter.php'); ?>
          
          <div class="col-md-3 form-group">
              <select class="form-control single-status"   name="key">
                  <option>--Status--</option>
                  <option <?php if(isset($key) && $key=="present") {echo "selected"; }?> value="present">Present</option>
                  <option <?php if(isset($key) && $key=="late_in") {echo "selected"; }?>  value="late_in" >Late In</option>
                  <option <?php if(isset($key) && $key=="absent") {echo "selected"; }?> value="absent">Absent</option>
                  <option <?php if(isset($key) && $key=="early_out") {echo "selected"; }?> value="early_out">Early  Out</option>
              </select>
          </div>
          <div class="col-md-3 form-group ">
              <input class="btn btn-success btn-sm " type="submit" name="getReport" class="form-control" value="Get Data">
          </div>    
      </div>
     </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                        $i=1;
                        
                        $crDate = date('Y-m-d');
                        $userIds = array();
                        $absent = array();
                        $precent = array();
                        $q = $d->select("attendance_master LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master", "users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND users_master.delete_status=0 AND attendance_master.attendance_date_start='$crDate' $blockAppendQueryUser", "ORDER BY attendance_master.attendance_id DESC");
                           
                        while ($data2=mysqli_fetch_array($q)) {
                          
                          array_push($userIds,$data2['user_id']);
                          if(isset($key) && $key=="present"){
                            array_push($precent,$data2);
                          }
                          if(isset($key) && $key=="late_in"){
                              if($data2['late_in']==1){

                                  array_push($precent,$data2);
                              }
                          }
                          if(isset($key) && $key=="early_out"){
                            if($data2['early_out']==1){

                                array_push($precent,$data2);
                            }
                          }

                        }
                        $userIds = join("','",$userIds); 
                        $q2=$d->select("unit_master,block_master,floors_master,users_master 
                        LEFT JOIN leave_master ON leave_master.user_id=users_master.user_id AND leave_master.leave_status=1 AND leave_start_date = '$crDate'",
                        "users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND 
                         users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.is_attendance_mandatory=0  AND users_master.user_id NOT IN ('$userIds')   $blockAppendQueryUser","ORDER BY block_master.block_sort ASC");
                        while ($data3=mysqli_fetch_array($q2)) {
                         if(isset($key) && $key=="absent"){
                         
                            array_push($precent,$data3);
                            }                            
                        }
                       
                        $counter = 1;
                        ?>
                        <table id="example" class="table table-bordered">
                          <thead>
                              <tr>
                                <th>Sr.No</th>                        
                                <th>Employee Name</th>
                                <?php if(isset($key) && ($key=="present" || $key=="late_in" || $key=="early_out")){ ?> 
                                  <th>Punch In</th>
                                  <?php } ?>
                                <?php if(isset($key) && ($key=="early_out")){ ?> 
                                  <th>Punch out</th>
                                  <?php } ?>
                                <th>On Leave</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                           if(!empty($precent)){
                               foreach($precent as $data){
                                ?>
                              <tr>
                                <td><?php echo $counter++; ?></td>
                                <td><?php echo $data['user_full_name']; ?></td>
                                <?php if(isset($key) && ($key=="present" || $key=="late_in" || $key=="early_out")){ ?> 
                                  <td><?php echo date('h:i A',strtotime($data['punch_in_time'])); ?></td>
                                  <?php } ?>
                                <?php if(isset($key) && $key=="early_out"){ ?> 
                                  <td><?php echo date('h:i A',strtotime($data['punch_out_time'])); ?></td>
                                  <?php } ?>
                                <td><?php if($data['leave_id']>0){ echo "Yes"; } ?></td>
                              </tr>
                              <?php 
                              }
                             } ?>
                          </tbody>
                      </table>
                      <?php //} else {  ?>
                        <!-- <div class="" role="alert">
                          <?php if(isset($dId)) { ?> 
                            <span><strong>Note :</strong> No Data !!!!!</span>
                            <?php } else { ?>
                              <span><strong>Note :</strong> Please Select Department !!!</span>
                               <?php }?>
                         
                        </div> -->
                      <?php //} ?>
                </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->
    </div>
  </div>
 
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">
<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth; ?>)
<?php } ?>
function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {
    newwindow.focus()
    }
    return false;
  }
</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
