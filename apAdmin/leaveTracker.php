<?php 
  error_reporting(0);
  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $year = (int)$_REQUEST['year'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-md-6">
          <h4 class="page-title">Assign Leaves</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
          <?php  if(isset($dId) && $dId > 0 && $year >0){ 
            ?>
            <a href="javascript:void(0)" onclick="assignLeaveToAllUsers(<?php echo $year ?>);" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Assign Leave To All Users </a>
            <?php }?>
          </div>
        </div>
    </div>
    <form class="branchDeptFilter" action="" >
      <div class="row pt-2 pb-2">
        <?php include 'selectBranchDeptForFilter.php' ?>
        <?php include 'selectYearForFilter.php' ?>
        
        <div class="col-md-3 form-group">
            <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
        </div>
      </div>
    </form>     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php  if(isset($dId) && $dId>0){ ?>
                
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="" class="selectAll" value=""></th>  
                        <th>Sr.No</th>                        
                        <th>Total</th>
                        <th>Employer</th>  
                        <?php 
                          $leaveTypeArray = array();
                         $qLeaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");
                          while ($TypeData=mysqli_fetch_array($qLeaveType)) { 
                            array_push($leaveTypeArray, $TypeData['leave_type_id']);
                            ?>                 
                        <th><?php echo $TypeData['leave_type_name']; ?></th>
                          <?php } ?>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      if(isset($dId) && $dId>0) {
                        $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }

                      
                      $q=$d->select("block_master,floors_master,users_master","users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status=0 $deptFilterQuery $blockAppendQueryUser","ORDER BY users_master.user_id ASC");
                      $counter = 1;

                    while ($data=mysqli_fetch_array($q)) {
                      if(isset($year) && $year>0) {
                        $assignYearFilterQuery = " AND assign_leave_year='$year'";
                      }
                      $totalLeaveAssingYear = $d->count_data_direct("user_leave_id","leave_assign_master","user_id='$data[user_id]' $assignYearFilterQuery ");
                     ?>
                    <tr>
                       <td>
                         <?php if ($totalLeaveAssingYear==0) { ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['user_id']; ?>"> 
                          <?php } ?>
                       </td>
                       <td><?php echo $counter++; ?></td>
                       <td>
                      
                        <?php  
                        
                        //$totalLeaveAssingYear = $d->count_data_direct("user_leave_id","leave_assign_master","user_id='$data[user_id]' $assignYearFilterQuery ");
                        if ($totalLeaveAssingYear==0) {
                        ?>
                        <form action="leaveAssignBulk" method="post">
                          <input type="hidden" name="user_id" value="<?php echo $data['user_id'];?>">
                          <input type="hidden" name="floor_id" value="<?php echo $data['floor_id'];?>">
                          <input type="hidden" name="leave_year" value="<?php echo $year;?>">
                          <button class="btn btn-success btn-sm">Assing Bulk</button>
                        </form>
                        <?php }else{ 
                          $totalQ = $d->selectRow('SUM(user_total_leave) AS total_leave', "leave_assign_master", "user_id='$data[user_id]' AND assign_leave_year = '$year'");
                          $totalData = mysqli_fetch_assoc($totalQ);
                          echo $totalData['total_leave']; ?> 
                          <form action="editLeaveAssignBulk" method="post" class="float-right ml-1">
                            <input type="hidden" name="user_id" value="<?php echo $data['user_id'];?>">
                            <input type="hidden" name="floor_id" value="<?php echo $data['floor_id'];?>">
                            <input type="hidden" name="leave_year" value="<?php echo $year;?>">
                          <button class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit Bulk</button>
                        </form>
                          <?php } ?>
                      </td>
                       <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                       <?php 
                       for ($il=0; $il < count($leaveTypeArray) ; $il++) { 
                        $toatlLeave = "";
                          
                        $leave_type_id = $leaveTypeArray[$il];
                        ?>
                      <td>
                        <?php   
                        $tq= $d->selectRow("user_total_leave","leave_assign_master","leave_type_id='$leave_type_id' AND user_id='$data[user_id]' $assignYearFilterQuery") ;
                        $totalLeaveData=mysqli_fetch_assoc($tq);
                        
                         $toatlLeave = $totalLeaveData['user_total_leave'];
                         echo $toatlLeave;
                        ?>
                      </td>
                       <?php  } ?>
                      
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            
            <?php }else{ ?>
                    <div class="" role="alert">
                    <span><strong>Note :</strong> Please Select Department</span>
                  </div>
                <?php  } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Assign</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="leaveAssignAdd" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post"> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Leave Type <span class="required">*</span></label>
                  <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="leave_type_id" name="leave_type_id">
                    <option value="">-- Select --</option> 
                      <?php 
                          $leaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_active_status = 0");  
                          while ($leaveTypeData=mysqli_fetch_array($leaveType)) {
                      ?>
                      <option value="<?php if(isset($leaveTypeData['leave_type_id']) && $leaveTypeData['leave_type_id'] !=""){ echo $leaveTypeData['leave_type_id']; } ?>"><?php if(isset($leaveTypeData['leave_type_name']) && $leaveTypeData['leave_type_name'] !=""){ echo $leaveTypeData['leave_type_name']; } ?></option> 
                      <?php } ?>
                    </select>                   
                </div> 
            </div> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Employee <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <select  type="text" required="" class="form-control single-select" id="user_id" name="user_id">
                    <option value="">-- Select --</option>
                    <?php
                    $userData=$d->select("users_master","society_id='$society_id'");
                    while ($user=mysqli_fetch_array($userData)) {
                      ?>
                      <option  value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                      <?php }?>
                  </select>
                </div>                   
            </div> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">User Total Leave <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control" placeholder="User Total Leave" id="user_total_leave" name="user_total_leave" value="">
                </div> 
            </div>                     
           <div class="form-footer text-center">
             <input type="hidden" id="user_leave_id" name="user_leave_id" value="" >
             <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addLeaveAssign"  value="addLeaveAssign">
             <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="bulkAssignLeaveGroupModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"> Assign Leave To All Users</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="leaveAssignAdd" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post"> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Leave Group <span class="required">*</span></label>
                  <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="leave_group_id" name="leave_group_id">
                    <option value="">-- Select --</option> 
                      <?php 
                          $leaveGroup=$d->select("leave_group_master","society_id='$society_id' AND leave_group_active_status = 0");  
                          while ($leaveGroupData=mysqli_fetch_array($leaveGroup)) {
                      ?>
                      <option value="<?php if(isset($leaveGroupData['leave_group_id']) && $leaveGroupData['leave_group_id'] !=""){ echo $leaveGroupData['leave_group_id']; } ?>"><?php if(isset($leaveGroupData['leave_group_name']) && $leaveGroupData['leave_group_name'] !=""){ echo $leaveGroupData['leave_group_name']; } ?></option> 
                      <?php } ?>
                    </select>                   
                </div> 
            </div> 
            <div id="show-all-group-leave">

            </div>
           <div class="form-footer text-center">
             <input type="hidden" name="action" value="assignUserBulkLeave" >
             <input type="hidden" name="year" value="<?php echo $year ?>" >
             <input type="hidden" id="userIds" name="userIds" value="" >
             <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>






<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">



//////////////////////holiday data 

function dataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getLeaveAssign",
                user_leave_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#user_leave_id').val(response.leave_assign.user_leave_id); 
              $('#leave_type_id').val(response.leave_assign.leave_type_id); 
              $('#leave_type_id').select2();
              $('#user_id').val(response.leave_assign.user_id); 
              $('#user_id').select2();
              $('#user_total_leave').val(response.leave_assign.user_total_leave);
            }
    })
}

function buttonSetting()
{
  $('.hideupdate').hide();
  $('.hideAdd').show();
}






  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }





</script>.
