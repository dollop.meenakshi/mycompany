<?php
error_reporting(0);

?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-7">
        <h4 class="page-title">Employee Report</h4>

        </div>
        <div class="col-sm-3 col-5 text-right">
          
        </div>
      
     </div>

       
    <!-- End Breadcrumb-->
    <form action="" method="get" id="getEmployeeReportForm">
      <div class="row pt-2 pb-2">
        <div class="col-md-3 form-group">

          <label class="form-control-label">Branch </label>
          <select name="bId" id="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
            <option value="">All Branch</option>
            <?php
            $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQuery");
            while ($depaData = mysqli_fetch_array($qd)) {
            ?>
              <option value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
            <?php } ?>

          </select>

        </div>
        <div class="col-md-3 form-group">
          
          <label class="form-control-label">Department </label>
          <select name="dId" id="dId" class="form-control single-select floor_id" onchange="getUserForSalary(this.value)">
            <option value="">All Department</option>
            <?php
                  $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$_GET[bId]' $blockAppendQuery");
                  while ($depaData = mysqli_fetch_array($qd)) {
                  ?>
              <option value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
            <?php } ?>

          </select>
        </div>
        <div class="col-md-3 form-group">
          
          <label class="form-control-label">Device </label>
          <select name="device" id="device" class="form-control">
            <option value="">All</option>
            <option <?php if ($_GET['device'] == "android") {echo 'selected';} ?> value="android">Android</option>
            <option <?php if ($_GET['device'] == "ios") {echo 'selected';} ?> value="ios">IOS</option>
          </select>
        </div>
        <div class="col-lg-2 col-6">
          <label class="form-control-label"> </label>
          <input type="hidden" name="report_download_access" value="<?php echo $adminData['report_download_access'];?>">
          <input type="hidden" name="role_id" value="<?php echo $adminData['role_id'];?>">

          <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getEmployeeReport" id="getEmployeeReport" class="form-control" value="Get Report">
        </div>
      </div>
    </form>
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                if (isset($_GET['bId']) && $_GET['bId'] > 0) {
                  $blockFilterQuery = " AND users_master.block_id='$_GET[bId]'";
                }
                if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                  $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                }
                if (isset($_GET['device']) && $_GET['device'] != '') {
                  $deviceFilterQuery = " AND users_master.device='$_GET[device]'";
                }

                 $q3=$d->select("unit_master,block_master,users_master,floors_master,user_employment_details","user_employment_details.user_id=users_master.user_id AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_status=1 $blockFilterQuery $deptFilterQuery $blockAppendQueryUser $blockAppendQuery $deviceFilterQuery","ORDER BY block_master.block_sort ASC");
                 // $q3=$d->select("users_master","society_id='$society_id'   ","ORDER BY user_id ASC");

                  $i=1;
                  if (isset($_GET['dId']) && isset($_GET['bId'])) {
               ?>
              <div id="employeeReport" >  
                
                <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Branch</th>
                          <th>Department</th>
                          <th>Mobile</th>
                          <th>Email</th>
                          <th>Joining Date</th>
                          <th>Company Experience</th>
                          <th>Emp Id</th>
                          <th>Blood Group</th>
                          <th>Device</th>
                          <th>App Version</th>
                          <th>Phone Model</th>
                          <th>Last App Access</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php 


                    while ($data=mysqli_fetch_array($q3)) {

                    
                     ?>
                      <tr>
                         
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $data['user_full_name'];  ?></td>
                          <td><?php echo $data['block_name']; ?></td>
                          <td><?php echo $data['floor_name']; ?></td>
                         
                          <td><?php  if ($adminData['role_id']==2) {
                            echo $data['country_code'] . " " . $data['user_mobile']; 
                          } else {
                              echo $data['country_code'] . " " . substr($data['user_mobile'], 0, 2) . '*****' . substr($data['user_mobile'],  -3);
                          } ?></td>
                          <td>
                            <?php 
                            echo $data['user_email']; 
                           ?>
                          </td>
                          <td><?php
                          if($data['joining_date'] !="" && $data['joining_date'] !="0000-00-00"){
                            echo  $sdate = $data['joining_date'];
                            $edate = date('Y-m-d');
                            $getExperianseYears = $d->getMonthDays($sdate,$edate);
                          }
                          ?></td>
                          <td>
                             <?php if(isset($getExperianseYears) && $getExperianseYears !=""){ 
                               echo $getExperianseYears;
                              } else {
                                echo "";
                              } ?>


                          </td>
                           <td><?php
                        if ($data['unit_name']!="No Number") {
                          echo $data['unit_name']; 
                          }
                          // code...
                          ?></td>
                           <td><?php
                       echo $data['blood_group']; 
                          ?></td>
                          <td> <?php if ($data['user_status']==1) {
                            echo $data['device']; 
                          } ?></td>
                          <td>
                            <?php 
                            echo $data['app_version_code']; 
                           ?>
                          </td>
                          <td><?php echo $data['phone_model']; ?> <?php if($data['phone_brand']!='') { echo '('.$data['phone_brand'].')'; } ?></td>
                          <td> <?php  if($data['last_login']!="0000-00-00 00:00:00") { echo  date('Y-m-d h:i A', strtotime($data['last_login'])).' ('.time_elapsed_string($data['last_login']).')';  } else { echo 'Not Login';}?></td>
                      </tr>
                    <?php }  ?>
                  </tbody>  
                  
                </table>
              </div>
             <?php } else {  ?>
              <div id="employeeReportNoData" style="display: block;">
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select branch and department !!!</span>
                </div>
              </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->