  <?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-3">
          <h4 class="page-title">Penalty</h4>
         
     </div>
     <div class="col-sm-3 col-9">
       <div class="btn-group float-sm-right">
        <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a>
        <a href="penalty"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
       
        
         <a href="javascript:void(0)" onclick="DeleteAll('deletePenalty');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>

     <div class="row ">
      <div class="col-sm-12 col-12">
        <ol class="breadcrumb">
          <li>
              <a href ="penalties?type=0"><span class="badge badge-pill badge-danger m-1">Unpaid (<?php echo $d->count_data_direct("penalty_id","penalty_master,unit_master,users_master","penalty_master.society_id='$society_id' AND penalty_master.paid_status=0 AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id $blockAppendQueryUser"); ?>)</span></a>
              <a href ="penalties?type=1" ><span class="badge badge-pill badge-success
               m-1">Paid (<?php echo $d->count_data_direct("penalty_id","penalty_master,unit_master,users_master","penalty_master.society_id='$society_id' AND penalty_master.paid_status=1 AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id $blockAppendQueryUser"); ?>)</span></a>

               <a href ="penalties" ><span class="badge badge-pill badge-warning
               m-1">All (<?php echo $d->count_data_direct("penalty_id","penalty_master,unit_master,users_master","penalty_master.society_id='$society_id' AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id $blockAppendQueryUser "); ?>)</span></a>

                <a href ="penalties?type=2" ><span class="badge badge-pill badge-info
               m-1">Paid Request (<?php echo $d->count_data_direct("penalty_id","penalty_master,unit_master,users_master,payment_paid_request","payment_paid_request.penalty_id=penalty_master.penalty_id AND penalty_master.society_id='$society_id' AND penalty_master.user_id=users_master.user_id AND penalty_master.paid_status=0 AND unit_master.unit_id=penalty_master.unit_id $blockAppendQueryUser "); ?>)</span></a>
             
        </ol>
      </div>

     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                       <th>#</th>
                        <th>Status</th>
                        <th>Mail</th>
                        <th>Amount</th>
                        <th><?php echo $xml->string->unit; ?></th>
                        <th>Penalty</th>
                        <th>Penalty Date</th>
                        <?php //IS_595?>
                        <th>Receive Date</th>
                        <th>Created Date</th>
                        <th>Photo</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                    // IS_675 penalty_date to  penalty_id
                    $queryAry=array();
                    if (isset($_GET['type']) && $_GET['type']!=2) {
                      $type = (int)$_GET['type'];
                      $append_query="penalty_master.paid_status='$type'";
                      array_push($queryAry, $append_query);
                    } 
                   
                   
                    
       
                    $appendQuery=  implode(" AND ", $queryAry);
                    if ($appendQuery!="") {
                      $appendQueryNew = " AND $appendQuery";
                    }
                
                    if ($_GET['type']==2) {
                        $q=$d->select("penalty_master,unit_master,users_master,block_master,payment_paid_request","payment_paid_request.penalty_id=penalty_master.penalty_id AND block_master.block_id=unit_master.block_id AND  penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id'  AND penalty_master.paid_status=0 $appendQueryNew $blockAppendQuery ","ORDER BY penalty_master.penalty_id DESC");
                    } else {

                        $q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=unit_master.block_id AND  penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id'  $appendQueryNew $blockAppendQuery ","ORDER BY penalty_master.penalty_id DESC");
                    }
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                      if ($data['member_status']==0) {
                            $uType = "Primary";
                          } else {
                            $uType= "Team";
                          }
                     ?>
                    <tr>
                         <td><?php echo $counter++; ?></td>
                       <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['penalty_id']; ?>">

                        <?php if ($data['paid_status']==0): ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['penalty_id']; ?>">
                        <?php endif ?>

                        </td>
                        <td><?php
                         if ($data['paid_status']==0) {  
                             $qcq= $d->select("payment_paid_request","unit_id='$data[unit_id]' AND penalty_id='$data[penalty_id]' ");
                              if (mysqli_num_rows($qcq)>0) {  ?>
                          <a onclick="getPenaltyRequest('<?php echo $data['penalty_id']; ?>');" data-toggle="modal" data-target="#payBill" href="javascript:void();">
                          <span class="badge badge-info m-1 pointerCursor">Paid Request <i class="fa fa-pencil"></i></span>
                           </a>
                            <?php  } else {
                          ?>
                          <form action="penalty" method="post" accept-charset="utf-8">
                            <input type="hidden" name="penalty_id" value="<?php echo $data['penalty_id']; ?>">
                            <input type="hidden" name="edit_penaly" value="edit_penaly">
                            <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button>
                             
              
                           <a onclick="getPenalty('<?php echo $data['penalty_id']; ?>');" data-toggle="modal" data-target="#payBill" href="javascript:void();">
                          <span class="badge badge-danger m-1 pointerCursor">Collect <i class="fa fa-pencil"></i></span>
                           </a>
                           <a title="Proforma Invoice"  href="proformaInvoice.php?user_id=<?php echo $data['user_id'] ?>&unit_id=<?php echo $data['unit_id'] ?>&type=P&societyid=<?php echo $data['society_id'] ?>&id=<?php echo $data['penalty_id'] ?>" class="btn btn-sm btn-info"><i class="fa fa-file"></i> </a>
                          </form>
                          
                         
                        <?php } } elseif ($data['paid_status']==1) { ?>
                         
                          <a   href="invoice.php?user_id=<?php echo $data['user_id'] ?>&unit_id=<?php echo $data['unit_id'] ?>&type=P&societyid=<?php echo $data['society_id'] ?>&id=<?php echo $data['penalty_id'] ?>" class="badge badge-success"><i class="fa fa-print"></i> Paid</a>
                         
                        <?php  }  ?></td>
                        <td>
                          <?php if ($data['paid_status']==0 && $data['user_email']!="") {   ?>
                          <form  action="controller/penaltyController.php" method="post" accept-charset="utf-8">
                             <input type="hidden" name="sendMail" value="sendMail">
                             <input type="hidden" name="user_full_name" value="<?php echo $data['user_full_name']; ?>">
                             <input type="hidden" name="to" value="<?php echo $data['user_email']; ?>">
                             <input type="hidden" name="final_penalty_amount" value="<?php echo number_format($data['penalty_amount'],2); ?>">
                             <input type="hidden" name="penalty_name_email" value="<?php echo $data['penalty_name']; ?>">
                            <button type="submit" class="btn form-btn btn-sm btn-warning"> <i class="fa fa-envelope"></i></button>
                          </form>
                          <?php } ?>
                        </td>
                        <td><?php echo number_format($data['penalty_amount'],2);?></td>
                        <td><?php echo $data['user_full_name'];?> (<?php echo $data['user_designation'];?>)</td>
                        <td><?php echo $data['penalty_name']; ?></td>
                        <?php //IS_595 penalty_date to penalty_datetime?>
                        <td><?php echo date("d M Y h:i A", strtotime($data['penalty_datetime'])); ?></td>
                         <?php //IS_595?>
                         <td><?php if($data['paid_status']==1) { 
                          if($data['penalty_receive_date']=="0000-00-00 00:00:00") {

                             $paneltyDataQry=$d->select("penalty_master,unit_master,users_master,expenses_balance_sheet_master","
                      expenses_balance_sheet_master.society_id=penalty_master.society_id AND 
                      expenses_balance_sheet_master.balancesheet_id=penalty_master.balancesheet_id AND 
                      penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id=".$society_id ." AND penalty_master.penalty_id=".$data['penalty_id']."  AND penalty_master.penalty_amount =expenses_balance_sheet_master.income_amount   ","");

                      if(mysqli_num_rows($paneltyDataQry) > 0 ){
                         $paneltyDataQData=mysqli_fetch_array($paneltyDataQry);  

                       
                         if( trim($paneltyDataQData['expenses_add_date'])==""){
                          echo "Not Available";
                           }else{
                            echo date("d-m-Y h:i A", strtotime($paneltyDataQData['expenses_add_date']));
                           }
                          
                       } else {
                          echo "Not Available";
                       }
                          
                          } else {
                             echo date("d M Y h:i A", strtotime($data['penalty_receive_date'])); 
                             if ($data['collected_by']!=0) {
                             echo "<br>Received By ";
                             $qn=$d->selectRow("admin_name","bms_admin_master","admin_id='$data[collected_by]'");
                             $admintData= mysqli_fetch_array($qn);
                             echo $admintData['admin_name'];
                             }
                          }
                           } else { echo "Not Available";} ?></td>
                           <td  data-order="<?php echo date("U",strtotime($data['created_at'])); ?>" ><?php echo date("d M Y h:i A", strtotime($data['created_at'])); ?></td>
                          


                        <td><?php if ($data['penalty_photo']!='') { ?>

                           <a href="../img/billReceipt/<?php echo $data['penalty_photo']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["penalty_photo"]; ?>"><img width="50" height="50" onerror="ImageErrrorSet(<?php echo $data['penalty_id'];?>)" src="../img/ajax-loader.gif" data-src="../img/billReceipt/<?php echo $data['penalty_photo']; ?>"  href="#divForm<?php echo $data['penalty_id'];?>" class="btnForm lazyload <?php echo $data['penalty_id'];?>" ></a>
                        <?php } else {
                          echo "Not Uploaded";
                        } ?></td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<script type="text/javascript">
  function ImageErrrorSet(cls){
   
    $('.'+cls).attr('src','../img/pnlty.png');
  }
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>




<div class="modal fade" id="payBill">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Pay Penalty</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>





<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
