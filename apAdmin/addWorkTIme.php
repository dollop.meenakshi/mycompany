<?php
extract(array_map("test_input", $_POST));
if (isset($edit_work_time)) {
    $q = $d->select("working_days_master","working_day_id='$working_day_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Shift Timing</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
           
                <form id="addWorkTime" action="controller/WorkTimeController.php" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Department <span class="required">*</span></label>
                            <div class="col-lg-7 col-md-8 col-12" id="">
                                <select class="form-control" name="floor_id">
                                <option value="">-- Select --</option> 
                                    <?php 
                                        $floor=$d->select("floors_master","society_id='$society_id' AND floor_status = 0");  
                                        while ($floorData=mysqli_fetch_array($floor)) {
                                    ?>
                                    <option <?php if(isset($data['floor_id']) && $data['floor_id'] ==$floorData['floor_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['floor_id']) && $floorData['floor_id'] !=""){ echo $floorData['floor_id']; } ?>"><?php if(isset($floorData['floor_name']) && $floorData['floor_name'] !=""){ echo $floorData['floor_name']; } ?></option> 
                                    <?php } ?>
                                </select>                   
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Shift <span class="required">*</span></label>
                            <div class="col-lg-7 col-md-8 col-12" id="">
                                <select class="form-control" name="shift_id">
                                <option value="">-- Select --</option> 
                                    <?php 
                                        $shift=$d->select("shift_timing_master","society_id='$society_id'");  
                                        while ($shiftData=mysqli_fetch_array($shift)) {
                                    ?>
                                    <option <?php if(isset($data['shift_time_id']) && $data['shift_time_id'] ==$shiftData['shift_time_id']){ echo "selected"; } ?> value="<?php if(isset($shiftData['shift_time_id']) && $shiftData['shift_time_id'] !=""){ echo $shiftData['shift_time_id']; } ?>"><?php if(isset($shiftData['shift_start_time']) && $shiftData['shift_start_time'] !=""){ echo $shiftData['shift_start_time'].'-'.$shiftData['shift_end_time']; } ?></option> 
                                    <?php } ?>
                                </select>                   
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label">Month Year <span class="required">*</span></label>
                            <div class="col-lg-7 col-md-8 col-12" id="">
                            <select class="form-control month_year" name="month_year">
                                <option value="">-- Select --</option> 
                                <?php 
                                
                                for ($m=1; $m<=12; $m++) {
                                    $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
                                    $days=cal_days_in_month(CAL_GREGORIAN, $m,date('Y'));                             
                                   ?>

                                <option data-id="<?php echo $days; ?>" value="<?php echo $m.'-'.date('Y'); ?>"><?php echo $month.'-'.date('Y'); ?></option> 
                                   <?php }
                                  // print_r($workdays);

                                ?>
                            </select>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label">Month Days <span class="required">*</span></label>
                            <div class="col-lg-7 col-md-8 col-12" id="">
                                <input type="text" readonly class="form-control" placeholder="Month Days" id="month_days" name="month_days" value="<?php if($data['month_days'] !=""){ echo $data['month_days']; } ?>">
                            </div>
                        </div> 
                    </div>   
                  
                </div>
                               
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn 
                  if (isset($edit_work_time)) {                    
                  ?>
                  <input type="hidden" id="working_day_id" name="working_day_id" value="<?php if($data['working_day_id'] !=""){ echo $data['working_day_id']; } ?>" >
                  <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                 <input type="hidden" name="addWorkTime"  value="addWorkTime">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveTypeAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                      ?>
                 
                  <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                 <input type="hidden" name="addWorkTime"  value="addWorkTime">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveTypeAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                  <?php } ?>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

    <script type="text/javascript">
       function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
      }

       function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
      }

      

    </script>