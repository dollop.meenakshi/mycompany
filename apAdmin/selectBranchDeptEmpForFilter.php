<div class="col-md-3 form-group col-6">
	<select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
		<option value="">-- Select Branch --</option> 
		<?php 
		$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
		while ($blockData=mysqli_fetch_array($qb)) {
		?>
		<option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
		<?php } ?>

	</select>         
</div>

<div class="col-md-3 form-group col-6">
	<select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);">
		<option value="">--Select Department --</option> 
		<?php 
			$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
			while ($depaData=mysqli_fetch_array($qd)) {
		?>
		<option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
		<?php } ?>
		
	</select>
</div>
<div class="col-md-3 form-group col-6">
	<select name="uId" id="uId" class="form-control single-select">
		<option value="">-- Select Employee --</option> 
		<?php 
			$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$dId' $blockAppendQueryUser","ORDER BY user_full_name ASC");  
			while ($userdata=mysqli_fetch_array($user)) {
		?>
		<option <?php if($uId==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
		<?php } ?>
	</select>
</div>
