<?php
    error_reporting(0);
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Manage Letter Template</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h6>Letter Setting</h6>
                        </div>
                        <hr>
                        <?php
                            $ls = $d->select("letter_setting_master");
                            $rec = mysqli_num_rows($ls);
                            if($rec > 0)
                            {
                                $lsd = $ls->fetch_assoc();
                                extract($lsd);
                                $form_id = "letterSettingFormUpdate";
                            }
                            else
                            {
                                $form_id = "letterSettingFormAdd";
                            }
                        ?>
                        <form action="controller/letterController.php" id="<?php echo $form_id; ?>" enctype="multipart/form-data" method="post">
                            <div class="row form-group">
                                <div class="col-12">
                                    <label class="text-danger">Best resolution for header image is -> 1200(w) * 200(h) px</label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-2">Header Image</div>
                                <div class="col-3">
                                    <input type="file" id="header_image" name="header_image" class="form-control-file border imageOnly" accept="image/*">
                                </div>
                                <div class="col-6">
                                <?php
                                    if(!empty($header_image) && $header_image != "" && file_exists("../img/".$header_image))
                                    {
                                ?>
                                    <a href="../img/<?php echo $header_image; ?>" data-fancybox="images" data-caption="Photo Name : Header Image"><img class="img-fluid" alt="Header Image" src="../img/<?php echo $header_image; ?>"/></a>
                                <?php
                                    }
                                    else
                                    {
                                        echo "Header image is not uploaded";
                                    }
                                ?>
                                </div>
                                <div class="col-1 text-center">
                                <?php
                                    if(!empty($header_image) && $header_image != "" && file_exists("../img/".$header_image))
                                    {
                                ?>
                                    <a onclick="deleteHeaderImage('<?php echo $header_image; ?>');" class="btn btn-sm btn-danger" title="Delete Header Image"><i class="fa fa-trash"></i></a>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12">
                                    <label class="text-danger">Best resolution for footer image is -> 1200(w) * 230(h) px</label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-2">Footer Image</div>
                                <div class="col-3">
                                    <input type="file" id="footer_image" name="footer_image" class="form-control-file border imageOnly" accept="image/*">
                                </div>
                                <div class="col-6">
                                <?php
                                    if(!empty($footer_image) && $footer_image != "" && file_exists("../img/".$footer_image))
                                    {
                                ?>
                                    <a href="../img/<?php echo $footer_image; ?>" data-fancybox="images" data-caption="Photo Name : Footer Image"><img class="img-fluid" alt="Footer image" src="../img/<?php echo $footer_image; ?>"/></a>
                                <?php
                                    }
                                    else
                                    {
                                        echo "Footer image is not uploaded";
                                    }
                                ?>
                                </div>
                                <div class="col-1 text-center">
                                <?php
                                    if(!empty($footer_image) && $footer_image != "" && file_exists("../img/".$footer_image))
                                    {
                                ?>
                                    <a onclick="deleteFooterImage('<?php echo $footer_image; ?>');" class="btn btn-sm btn-danger" title="Delete Footer Image"><i class="fa fa-trash"></i></a>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-2">Add Logo In Watermark?</div>
                                <div class="col-4">
                                <?php
                                    if($add_watermark == 1)
                                    {
                                ?>
                                    <input type="checkbox" checked class="js-switch letterStatus" data-color="#15ca20" onchange ="changeStatusLetterSetting(<?php echo $add_watermark; ?>,'Inactive');" data-size="small"/>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                    <input type="checkbox" class="js-switch letterStatus" data-color="#15ca20" onchange ="changeStatusLetterSetting(<?php echo $add_watermark; ?>,'Active');" data-size="small"/>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="saveLetterImages" value="saveLetterImages"/>
                            <?php
                                if($rec > 0)
                                {
                            ?>
                                <input type="hidden" name="update"/>
                                <input type="hidden" name="old_header_image" value="<?php echo $header_image; ?>"/>
                                <input type="hidden" name="old_footer_image" value="<?php echo $footer_image; ?>"/>
                                <button type="submit" class="btn btn-success">Update</button>
                            <?php
                                }
                                else
                                {
                            ?>
                                <input type="hidden" name="insert"/>
                                <input type="hidden" name="old_header_image" value=""/>
                                <input type="hidden" name="old_footer_image" value=""/>
                                <button type="submit" class="btn btn-success">Save</button>
                            <?php
                                }
                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Letter Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $q = $d->select("letter_master AS lm");
                                    $counter = 1;
                                    while ($data = $q->fetch_assoc())
                                    {
                                        extract($data);
                                    ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['letter_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="generateDocuments?letter_id=<?php echo $letter_id; ?>&letter_name=<?php echo $letter_name; ?>" class="btn btn-sm btn-info mr-2" title="Generate Documents"> <i class="fa fa-gear"></i> Generate Letter</a>
                                                <form method="post" action="controller/letterController.php">
                                                    <input type="hidden" name="generateDocuments" value="generateDocuments"/>
                                                    <input type="hidden" name="preview" value="preview"/>
                                                    <input type="hidden" name="letter_id" value="<?php echo $letter_id; ?>"/>
                                                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['token']; ?>"/>
                                                    <button type="submit" class="btn btn-sm btn-danger mr-2"><i class="fa fa-eye"></i> Preview</button>
                                                </form>
                                                <a href="addLetterTemplate?letter_id=<?php echo $letter_id; ?>" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></a>
                                                <?php
                                                    if($data['active_status'] == "0")
                                                    {
                                                ?>
                                                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['letter_id']; ?>','letterDeactive');" data-size="small"/>
                                                <?php
                                                    }
                                                    else
                                                    {
                                                ?>
                                                <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['letter_id']; ?>','letterActive');" data-size="small"/>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>