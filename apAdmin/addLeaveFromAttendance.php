<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

$currentYear = date('Y');

$user_id = $_POST['user_id'];
$date = $_POST['date'];
$year = date('Y', strtotime($date));

$society_id = $_COOKIE['society_id'];
if (isset($_POST['leave_id']) && $_POST['leave_id'] > 0) {
    $Ldata = $d->selectRow('*', "leave_master", "leave_id='$_POST[leave_id]'");
    $Leavedata = mysqli_fetch_assoc($Ldata);
    //print_r($Leavedata);die;
}

?>
<div class="row">
    <div class="col-md-12">
        <?php if($Leavedata['leave_reason']!="") { ?>
            <p class="col-sm-12">Leave Reason: <?php echo $Leavedata['leave_reason'];?><span id="leave_reason"></p>
        <?php } ?>
        <div class="leaveBalanceResponse">
            <p class="col-sm-12 text-danger">Total Leave Balance (Year): <span class="balance_leave"></span></p>
            <p class="col-sm-12 text-danger">Applicable Leave Till Date <span class="leave_date"></span>: <span class="applicable_leave"></span></p>
        </div>
        <input type="hidden" class="balance_leave_temp" name="">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Type <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12">
                <select name="leave_type_id" class="form-control single-select leave_type" required>
                    <option value="">-- Leave Type --</option>
                    <?php
                    $qLeaveType = $d->select("leave_type_master,leave_assign_master", "leave_type_master.leave_type_id=leave_assign_master.leave_type_id AND leave_type_master.society_id='$society_id' AND leave_assign_master.user_id='$user_id' AND leave_type_master.leave_type_delete=0  AND leave_assign_master.assign_leave_year='$year'");
                    //$qLeaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");  
                    while ($qLeaveTypeData = mysqli_fetch_array($qLeaveType)) {

                        $userLeaveq = $d->selectRow('user_total_leave', "leave_assign_master", "user_id='$user_id' AND leave_type_id='$qLeaveTypeData[leave_type_id]' AND assign_leave_year= '$currentYear'");
                        $userLeaveData = mysqli_fetch_assoc($userLeaveq);

                        $approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$qLeaveTypeData[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$currentYear'");

                        $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);

                        $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$qLeaveTypeData[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$currentYear'");
                        $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
                        $payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$user_id' AND leave_type_id='$qLeaveTypeData[leave_type_id]' AND leave_payout_year = '$currentYear'");
                        $payOutLeaveData = mysqli_fetch_assoc($payOutLeave);
                        $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave'] / 2;
                        $remaining_leave = $userLeaveData['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'];
                        /*  print_r('userLeaveData');
                            print_r($remaining_leave); */
                    ?>
                        <option <?php if (isset($Leavedata) && $Leavedata['leave_type_id'] == $qLeaveTypeData['leave_type_id']) {
                                    echo "selected";
                                } ?> value="<?php echo  $qLeaveTypeData['leave_type_id']; ?>" remaining_leave="<?php echo $remaining_leave; ?>"> <?php echo $qLeaveTypeData['leave_type_name'] . ' (Leave Balance=' . $remaining_leave . ')'; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group row w-100 mx-0">
           
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Day Type <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12">
                <select name="leave_day_type" class="form-control leave_day_type" id="leave_day_type" required>
                    <option value="">-- Day Type --</option>
                    <option <?php if (isset($Leavedata) && $Leavedata['leave_day_type'] == 0) {
                                echo "selected";
                            } ?> value="0">Full Day</option>
                    <option <?php if (isset($Leavedata) && $Leavedata['leave_day_type'] == 1) {
                                echo "selected";
                            } ?> value="1">Half Day</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Paid Or Unpaid<span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12">
                <select name="paid_unpaid" class="form-control paid_unpaid" id="paid_unpaid" required>
                    <option value="">-- Mode --</option>
                    <option <?php if (isset($Leavedata) && $Leavedata['paid_unpaid'] == 0) {
                                echo "selected";
                            } ?> value="0">Paid Leave</option>
                    <option <?php if (isset($Leavedata) && $Leavedata['paid_unpaid'] == 1) {
                                echo "selected";
                            } ?> value="1">Unpaid Leave</option>
                </select>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
     $(".leaveBalanceResponse").hide();
    $(".leave_type").change(function(){
            var leave_type_id = $(this).val();
            var leave_date = '<?php echo $date; ?>';
            var user_id = <?php echo $user_id;?>;
            $.ajax({
                url: "../residentApiNew/commonController.php",
                cache: false,
                type: "POST",
                data: {
                    action:'getLeaveBalanceForAutoLeave',
                    leave_type_id:leave_type_id,
                    leave_date:leave_date,
                    user_id:user_id,
                },
                success: function(response){
                    $(".leaveBalanceResponse").show();
                    $(".ajax-loader").hide();
                    $('.applicable_leave').html(response.leave.available_paid_leave);
                    $('.balance_leave_temp').val(response.leave.available_paid_leave);
                    $('.use_leave').html(response.leave.use_leave);
                    $('.balance_leave').html(response.leave.remaining_leave);
                    if(response.leave.remaining_leave <=0){
                        var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                        $('#paid_unpaid').html(paidUnpaidValue);
                    }else if(parseFloat(response.leave.applicable_leaves_in_month) <= parseFloat(response.leave.use_leave)){
                        var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                        $('#paid_unpaid').html(paidUnpaidValue);
                    }
                    else{
                        var paidUnpaidValue = `<option value="">-- Mode --</option>
                                <option value="0">Paid Leave</option>
                                <option value="1">Unpaid Leave</option> `;
                        $('#paid_unpaid').html(paidUnpaidValue);
                        }
                }
            })
        });
        $("#leave_day_type").change(function(){
            var leave_day_type = $('#leave_day_type').val();
            var remaining_leave = $('.balance_leave_temp').val();
            var applicable_leave = parseFloat($('.applicable_leave').val());
            var use_leave = parseFloat($('.use_leave').val());
            leave_diff = applicable_leave - use_leave;

            if(remaining_leave == 0.5 && leave_day_type <= 0){
                var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }else if(applicable_leave <= use_leave){
                var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }else if(leave_diff == 0.5 && leave_day_type <= 0){
                var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }else if(remaining_leave == 0){
                var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }else{
                var paidUnpaidValue = `<option value="">-- Mode --</option>
                            <option value="0">Paid</option>
                            <option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }
        });

</script>