<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));

    if ($key==$keydb) {
        
            if($_POST['getVotingList']=="getVotingList" && $society_id!=''){


                $qvoting=$d->select("voting_master","society_id='$society_id'" );
                if(mysqli_num_rows($qvoting)>0){
                        $response["voting"] = array();

                    while($data_voting_list=mysqli_fetch_array($qvoting)) {

                            $voting = array(); 

                            $voting["voting_id"]=$data_voting_list['voting_id'];
                            $voting["society_id"]=$data_voting_list['society_id'];
                            $voting["voting_question"]=$data_voting_list['voting_question'];
                            $voting["voting_description"]=$data_voting_list['voting_description'];
                            $voting["voting_start_date"]=$data_voting_list['voting_start_date'];
                            $voting["voting_end_date"]=$data_voting_list['voting_end_date'];
                            $voting["voting_status"]=$data_voting_list['voting_status'];
                            
                            array_push($response["voting"], $voting);


                    }
                    $response["message"]="Get voting success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No voting Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getVotingOptionList']=="getVotingOptionList"){


                   $count5=$d->sum_data("option_count","voting_option_master","voting_id='$voting_id'");
                   $row=mysqli_fetch_array($count5);
                   $totalVoting=$row['SUM(option_count)'];
                   

                $voting_option_data=$d->select("voting_option_master","voting_id ='$voting_id'");

                if(mysqli_num_rows($voting_option_data)>0){

                    $response["option"] = array();
            
                    while($data_voting_option_list=mysqli_fetch_array($voting_option_data)) {

                        $sv=$d->sum_data("option_count","voting_option_master","voting_id='$voting_id' AND voting_option_id='$data_voting_option_list[voting_option_id]'");
                        $row1=mysqli_fetch_array($sv);
                        $singleVoting=$row1['SUM(option_count)'];

                        $singleVoting = $singleVoting *100;
                        $votingPer= number_format($singleVoting /  $totalVoting,2);

                        $option = array();
                        $option["voting_option_id"]=$data_voting_option_list['voting_option_id'];
                        $option["society_id"]=$data_voting_option_list['society_id'];
                        $option["voting_id"]=$data_voting_option_list['voting_id'];
                        $option["option_name"]=$data_voting_option_list['option_name'];
                        $option["votingPer"]=$votingPer;
                   
                        array_push($response["option"], $option); 
                    }

                 $voting_result_data=$d->select("voting_result_master","voting_id='$voting_id' AND unit_id ='$unit_id' AND user_id ='$user_id' AND user_id!=0");

                if(mysqli_num_rows($voting_result_data)>0){
                    $response["voting_submitted"]="200";
                }else{
                    $response["voting_submitted"]="201";
                }
                  
                    
                    $response["message"]="Get Voting Option success.";
                    $response["status"]="200";
                    echo json_encode($response);
        
                }else{

                    $response["message"]="No Voting Option Found.";
                    $response["status"]="201";
                    echo json_encode($response);
        
                }
   }else if($_POST['addVote']=="addVote" && $society_id!='' && $unit_id!=''){

        $qqq=$d->select("voting_result_master","unit_id='$unit_id' AND user_id='$user_id' AND voting_id='$voting_id'");
        $voteData=mysqli_fetch_array($qqq);
        if ($voteData>0) {
          $response["status"]="201";
          $response["message"]="You have already voted for this poll..!";
          echo json_encode($response);
          exit();
        }

        $m->set_data('voting_id',$voting_id);
        $m->set_data('unit_id',$unit_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('admin_id',$admin_id);
        $m->set_data('voting_option_id',$voting_option_id);

        $a= array (
          'voting_id'=> $m->get_data('voting_id'),
          'unit_id'=> $m->get_data('unit_id'),
          'user_id'=> $m->get_data('user_id'),
          'society_id'=> $m->get_data('society_id'),
          'admin_id'=> $m->get_data('admin_id'),
          'voting_option_id'=> $m->get_data('voting_option_id')
        );

        $q=$d->insert('voting_result_master',$a);

        $voting_option_data2=$d->select("voting_option_master","voting_option_id ='$voting_option_id'");
        if(mysqli_num_rows($voting_option_data2)>0){
             $data=mysqli_fetch_array($voting_option_data2);
            $option_count=$data['option_count'];
            
        }
        $option_count=$option_count+1;
    
        $a1= array (
          'option_count'=> $option_count
         );

         $q1=$d->update('voting_option_master',$a1,"voting_option_id ='$voting_option_id'");

       if ($q==true) {
          $response["status"]="200";
          $response["message"]="Thank you for Vote..!";
          echo json_encode($response);
        }else{
          $response["status"]="201";
          $response["message"]="Something Wrong...!";
          echo json_encode($response);
       }
   }else if($_POST['getVotingResult']=="getVotingResult" && $voting_id!=''){

                $voting_option_data=$d->select("voting_option_master","voting_id ='$voting_id'","ORDER BY option_count DESC");

                if(mysqli_num_rows($voting_option_data)>0){

                    $response["result"] = array();
            
                    while($data_voting_option_list=mysqli_fetch_array($voting_option_data)) {

                        $result = array();
                        $result["given_vote"]=$data_voting_option_list['option_count'];
                        $result["option_name"]=$data_voting_option_list['option_name'];
                        array_push($response["result"], $result); 
                    }

                 $voting_result_data=$d->select("voting_result_master","voting_id='voting_id'AND unit_id ='$unit_id'");

                if(mysqli_num_rows($voting_result_data)>0){
                    $response["voting_submitted"]="200";
                }else{
                    $response["voting_submitted"]="201";
                }
                  
                    
                    $response["message"]="Get Voting Option success.";
                    $response["status"]="200";
                    echo json_encode($response);
        
                }else{

                    $response["message"]="No Voting Option Found.";
                    $response["status"]="201";
                    echo json_encode($response);
        
                }
   }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
      }

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>