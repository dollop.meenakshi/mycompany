<?php
  error_reporting(0);
  $complaint_category_id=$adminData['complaint_category_id'];
  $compCtgAry = explode(",",$complaint_category_id);
  $ids = join("','",$compCtgAry);
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Complaints Report</h4>
        </div>
     </div>
    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required="" autocomplete="off"  class="form-control" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required="" autocomplete="off"  class="form-control" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Select Status </label>
             <select required="" class="form-control single-select" name="type">
                <option value="All" <?= ($_GET) ? (($_GET['type']=='All') ? 'selected' : '' ) : ''  ?> >All</option>
                <option value="0" <?= ($_GET) ? (($_GET['type']=='0') ? 'selected' : '' ) : ''  ?> >Open</option>
                <option value="1" <?= ($_GET) ? (($_GET['type']=='1') ? 'selected' : '' ) : ''  ?> >Close</option>
                <option value="2" <?= ($_GET) ? (($_GET['type']=='2') ? 'selected' : '' ) : ''  ?> >Re Open</option>
                <option value="3" <?= ($_GET) ? (($_GET['type']=='3') ? 'selected' : '' ) : ''  ?> >In Progress</option>
                <option value="4" <?= ($_GET) ? (($_GET['type']=='4') ? 'selected' : '' ) : ''  ?> >Deleted</option>
             </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Select Category </label>
             <select required="" class="form-control single-select" name="category">
                <option value="All">All</option>
                  <?php
                  if ($ids!="" ) {
                      $append_temp="complaint_category_id IN ('$ids')";
                    }
                   $q=$d->select("complaint_category","$append_temp","");
                        while ($mData=mysqli_fetch_array($q)) { ?>
                          <option <?php if($mData['complaint_category_id']==$_GET['category']) { echo 'selected';} ?> value="<?php echo $mData['complaint_category_id'] ?>"><?php echo $mData['category_name']; ?></option>
                  <?php } ?>
             </select>
          </div>
          <div class="col-lg-3 col-6">
            <label  class="form-control-label">Employee </label>
             <select required="" class="form-control single-select" name="unit">
                <option value="All">All</option>
                   <?php
                    $q3=$d->select("unit_master,block_master,users_master,floors_master","floors_master.floor_id = users_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.member_status=0 $blockAppendQuery","group by unit_master.unit_id");
                     while ($blockRow=mysqli_fetch_array($q3)) {
                   ?>
                    <option <?php if($blockRow['unit_id']==$_GET['unit']) { echo 'selected';} ?> value="<?php echo $blockRow['unit_id'];?>"><?php echo $blockRow['user_full_name'];?>-<?php echo $blockRow['floor_name'];?>  </option>
                    <?php }?>
             </select>
          </div>
          <div class="col-lg-1 col-6">
            <label  class="form-control-label"> </label>
              <!-- <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report"> -->
              <button type="submit" class="btn btn-success mt-4" name="getReport" ><i class="fa fa-search"></i></button>
          </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <?php
                extract(array_map("test_input" , $_GET));
                $i=1;
                if (isset($_GET['from'])) {
               ?>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Status</th>
                        <th>No</th>
                        <th>Employee</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Last Status</th>
                        <th>Category</th>
                        <th>Feedback</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $from = date('Y-m-d 00:00:00', strtotime($from));
                    $toDate = date('Y-m-d 23:59:00', strtotime($toDate));
                    $queryAry=array();
                    if ($_GET['type']!='All' && $_GET['type']!='') {
                      if ($type==4) {
                      $append_query="complains_master.flag_delete IN ('1','2')";
                      }else if ($type==0) {
                      $append_query="complains_master.complain_status='$type' AND complains_master.flag_delete = '0'";
                      }
                      else {
                      $append_query="complains_master.flag_delete='0' AND complains_master.complain_status='$type'";
                      }
                      array_push($queryAry, $append_query);
                    }
                    if ($_GET['unit']!="All") {
                      $unit=(int)$unit;
                      $append_query1 ="complains_master.unit_id='$unit'";
                      array_push($queryAry, $append_query1);
                    }
                    if ($_GET['category']!="All" ) {
                      $append_query13="complains_master.complaint_category='$category'";
                      array_push($queryAry, $append_query13);
                    }
                    if ($ids!="" ) {
                      $append_query4="complains_master.complaint_category IN ('$ids')";
                      array_push($queryAry, $append_query4);
                    }
                   $appendQuery=  implode(" AND ", $queryAry);
                    if ($appendQuery!="") {
                      $appendQueryNew = " AND $appendQuery";
                    }
                    $sr=1;
                        $q=$d->select("complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.complain_date BETWEEN '$from' AND '$toDate' $appendQueryNew  $blockAppendQueryUnit","ORDER BY complains_master.complain_id DESC");

                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                         <td><?php echo $sr++; ?></td>
                        <td><?php
                        if ($data['flag_delete']==0) {
                         if ($data['complain_status']==0) {  ?>
                          <span class="badge badge-warning m-1 ">OPEN</span>
                        <?php } elseif ($data['complain_status']==1) { ?>
                          <span class="badge badge-success  m-1 ">CLOSE</span>
                        <?php  } elseif ($data['complain_status']==2) { ?>
                          <span class="badge badge-info  m-1 ">REOPEN</span>
                        <?php } elseif ($data['complain_status']==3) { ?>
                          <span class="badge badge-primary  m-1 ">In Progress</span>
                        <?php } } else {
                          echo "Deleted by User";
                        }?></td>
                        <td>
                         <form action="complaintHistory" method="get">
                            <input type="hidden" name="id" value="<?php echo $data['complain_id']; ?>">
                            <button title="View History" class="btn btn-link p-0">CN<?php echo $data['complain_id']; ?> <i class="fa  fa-history" aria-hidden="true"></i></button>
                          </form>
                        </td>
                        <td><?php
                          $qu=$d->select("users_master","user_id='$data[user_id]'");
                          $userData=mysqli_fetch_array($qu);
                          echo $userData['user_full_name'];
                         ?></td>
                        <td class="tableWidth"><?php echo $data['compalain_title']; ?></td>
                        <td><?php echo $data['complain_date']; ?></td>
                        <td class="tableWidth"><?php echo $data['complain_review_msg']; ?></td>
                        <td><?php
                            $q11=$d->selectRow("category_name","complaint_category","complaint_category_id='$data[complaint_category]'");
                            $catData=mysqli_fetch_array($q11);
                            echo $catData['category_name']; ?></td>
                        <td><?php echo $data['feedback_msg']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } else {  ?>
                <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->