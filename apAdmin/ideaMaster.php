  <?php 
  error_reporting(0);
  $status = $_GET['status'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Manage Ideas</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <div class="float-sm-right">
          <ol class="breadcrumb">
            <li>
                <a href="ideaMaster"><span class="badge badge-pill badge-warning m-1">All (<?php echo $d->count_data_direct("idea_id","idea_master","society_id='$society_id'"); ?>)</span></a>
                <a href="ideaMaster?status=0"><span class="badge badge-pill badge-primary m-1">Pending (<?php echo $d->count_data_direct("idea_id","idea_master","society_id='$society_id' AND idea_status=0"); ?>)</span></a>
                <a href="ideaMaster?status=1"><span class="badge badge-pill badge-success m-1">Approved (<?php echo $d->count_data_direct("idea_id","idea_master","society_id='$society_id' AND idea_status=1"); ?>)</span></a>
                <a href="ideaMaster?status=2"><span class="badge badge-pill badge-danger m-1">Declined (<?php echo $d->count_data_direct("idea_id","idea_master","society_id='$society_id' AND idea_status=2"); ?>)</span></a>
            </li>
          </ol>
      </div>
     </div>
    </div>

     

      <div class="row">
        <div class="col-lg-12">
          <div class="idea">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body row ">
            <?php 
              $i=1;
              if(isset($status) && $status != '') {
                $statusFilterQuery = " AND idea_master.idea_status = '$status'";
              } 
              $q=$d->select("idea_master,idea_category_master,users_master","users_master.user_id=idea_master.user_id AND idea_master.idea_category_id=idea_category_master.idea_category_id AND idea_master.society_id='$society_id' AND users_master.delete_status=0 $statusFilterQuery $blockAppendQueryUser", "ORDER BY idea_master.idea_id DESC");
              $counter = 1;
              while ($data=mysqli_fetch_array($q)) {
              ?>
            <div class="col-12 col-lg-6 col-xl-6">
              <div class="card profile-card-2">
              <?php if($data['idea_status'] == 0){
                  $status_class = "badge-primary";
                  $status_value = "Pending";
                } 
                else if($data['idea_status'] == 1){
                  $status_class = "badge-success";
                  $status_value = "Approved";
                }else{
                  $status_class = "badge-danger";
                  $status_value = "Declined";
                }
                
                ?>
               
              
              <div style="width: 70px; position: relative;z-index: 950;left: 0;margin: 5px;" class="badge <?php echo $status_class; ?>"><?php echo $status_value; ?></div>             

                <div class="card-body pt-2">
                  <div class="col-6  float-left">
                  <lable class="card-title" >Idea Title:<lable>
                  </div>
                  <div class="col-6 float-left">
                  <h6 class=""><?php echo $data['idea_title']; ?></h6> 
                  </div>
                  <div class="col-6  float-left">
                  <lable class="card-title" >Idea Category :<lable>
                  </div>
                  <div class="col-6 float-left">
                  <h6 class=""><?php echo $data['idea_category_name']; ?></h6>
                  </div>
                  <div class="col-6  float-left">
                      <lable class="card-title" >Name: <lable>
                  </div>
                    <div class="col-6 float-left">
                        <h6 class=""><?php echo $data['user_full_name']; ?></h6>          
                  </div>
                  <div class="col-6  float-left">
                      <lable class="card-title" >Date: <lable>
                  </div>
                    <div class="col-6 float-left">
                    <h6 class=""><?php echo date("d M Y h:i A", strtotime($data['idea_created_date'])); ?></h6>
                  </div>
                 
                  <div class="col-6  float-left">
                      <lable class="card-title" >Attachment: <lable>
                  </div>
                    <div class="col-6 float-left">
                      <?php if($data['idea_attachment'] != ''){?>
                      <h6 class=""><a href="../img/idea/<?php echo $data['idea_attachment']; ?>" target="_blank"><?php echo $data['idea_attachment']; ?></a></h6>
                    <?php }else{ ?>
                      <h6 class="">No attachment available!</h6>
                    <?php } ?> 
                   </div>
                   <div class="col-6  float-left">
                      <lable class="card-title" >Description: <lable>
                  </div>
                    <div class="col-6 float-left">
                    <p class="text-primary" style="height: 66px;overflow: hidden;"><?php echo $data['idea_description']; ?></p>
                  </div>
                  <div class="col-6  float-left">
                      <lable class="card-title" >Likes/Comments: <lable>
                  </div>
                    <div class="col-6 float-left">
                    <?php
                        $totalComment = $d->count_data_direct("idea_comment","idea_comment_master","idea_id='$data[idea_id]' AND idea_parent_comment_id=0");
                        $totalLike = $d->count_data_direct("idea_like_id","idea_like_master","idea_id='$data[idea_id]' ");
                    ?>
                    <div ><a href="javascript:void(0)" onclick="ideaBoxLike(<?php echo $data['idea_id']; ?>);"><i class="fa fa-thumbs-up"></i><?php echo $totalLike; ?></a></div>
                    <div class=""><a href="javascript:void(0)" onclick="ideaBoxComment(<?php echo $data['idea_id']; ?>);"><i class="fa fa-commenting"></i><?php echo $totalComment; ?></a></div>
                 
                  </div>
                 
                  
                  <div class="icon-block mt-2 row text-center">
                    <div class="col-6 col-lg-6 col-xl-6">
                    <select class="form-control valid" onchange="ideaStatusChange(this.value, '<?php echo $data['idea_id']; ?>', 'ideaMasterStatus')">
                      <option value="0" <?php if($data['idea_status'] == 0){echo "selected";} ?>>Pending</option>
                      <option value="1" <?php if($data['idea_status'] == 1){echo "selected";} ?>>Approved</option>
                      <option value="2" <?php if($data['idea_status'] == 2){echo "selected";} ?>>Declined</option>
                    </select>
                    </div>
                    <div class="col-2 col-lg-2 col-xl-2">
                       <a href="ideaMasterDetail?id=<?php echo $data['idea_id']; ?>" class="btn btn-sm btn-primary ml-2 mr-2">View</a>
                    </div>
                    <div class="col-2 col-lg-2 col-xl-2 text-right">
                    <button class="btn btn-sm btn-warning ml-3  DeleteIdeaMaster" onclick="DeleteIdeaMaster('<?php echo $data['idea_id']; ?>', 'deleteIdea');"> Delete</button>
                    <!-- <input type="hidden" name="csrf" value="f1127fa29b311f8d7c3b0871e86f3d6d"> -->
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <?php } ?>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<div class="modal fade" id="likedUserModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Likes</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="like_user_detail">

        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="usersCommentModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Comments</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="comment_detail">

        </div>
      </div>
    </div>
  </div>
</div>
<style>
   .user-like-list{
     padding-left:45px !important;
   }
  .user-like-list .liked_user-avtar{
    position: absolute;
      top:7px;
      left:7px;
      width:35px;
      height:35px;
      border-radius:50px;
      overflow:hidden;
  }
  .user-like-list .liked_user-avtar img{
    width:100%;
    height:100%;
    object-fit:cover;
  }
  .user-like-list p{
    font-weight:bold;
    color:#000
  }
  .user-sub-comment-list{
    background-color:#f6f6f6;
    margin-bottom:5px;
    border-radius:5px;
  }
  </style>

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
    
</script>
