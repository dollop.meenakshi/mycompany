  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
      <div class="col-sm-6 col-6">
        <h4 class="page-title"><?php echo $xml->string->balance_sheet_pdf; ?></h4>
     </div>
    
     <div class="col-sm-6 col-6">
       <div class="btn-group float-sm-right">
       
        <a data-toggle="modal" data-target="#addFile" href="#" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> </a>   
         <a href="javascript:void(0)" onclick="DeleteAll('deleteBalanceFile');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> <?php echo $xml->string->delete; ?> </a> 
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th class='deleteTh'>#</th>
                        <th>#</th>
                        <th><?php echo $xml->string->name; ?></th>
                        <th><?php echo $xml->string->action; ?></th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $i=1;
                  $q=$d->select("balancesheet_pdf_master","society_id='$society_id'");
                  while ($empData=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       <td class="text-center">
                        
                                  <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $empData['balancesheet_file_id']; ?>">
                        </td>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $empData['balancesheet_name']; ?></td>
                        
                       
                    <td>
                      
                        <a target="_blank" href="../img/balancesheet/<?php echo $empData['file_name']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o"></i> <?php echo $xml->string->view_file; ?> </a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->




<div class="modal fade" id="addFile">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->balance_sheet_pdf; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php //IS_574 personal-info to BalancesheetFileFrm ?>
          <form id="BalancesheetFileFrm" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
               <label for="input-10" class="col-sm-2 col-form-label"> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-10" id="PaybleAmount">
                  <?php //IS_574 id="balancesheet_name"  ?>
                  <input required="" type="text" name="balancesheet_name"  id="balancesheet_name"  class="form-control" maxlength="30">
                </div>
            </div> 
            <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->file; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-10" id="PaybleAmount">
                  <?php //IS_574 id="balancesheet_name"  ?>
                  <input required="" type="file" accept="application/pdf" name="file_name" id="file_name"   class="form-control-file border pdfOnly">
                </div>
            </div>
         
                <div class="form-footer text-center"> 
                  <input type="hidden" name="addBalaceFile" value="addBalaceFile"> 
                  <button type="submit" name=""  class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->