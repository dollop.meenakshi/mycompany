<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Stationery Vendor</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addStationaryVendor"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            
            <a href="javascript:void(0)" onclick="DeleteAll('deleteStationaryVendor');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

          </div>
        </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>       
                        <th>Vendor Name</th>                      
                        <th>Mobile No</th>                                               
                        <th>Action</th>
                        <th>Logo</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      $q=$d->select("vendor_master","society_id='$society_id' AND vendor_category_id=2 AND vendor_master_delete=0 ");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                          <?php $totalCategory = $d->count_data_direct("vendor_product_category_id","vendor_product_category_master","vendor_id='$data[vendor_id]'");
                            if( $totalCategory==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['vendor_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['vendor_id']; ?>">                      
                          <?php } ?>
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['vendor_name']; ?></td>
                       <td><?php echo $data['vendor_mobile']; ?></td>
                       <td>
                         <div class="d-flex align-items-center">
                            <form action="addStationaryVendor" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="vendor_id" value="<?php echo $data['vendor_id']; ?>">
                              <input type="hidden" name="edit_stationary_vendor" value="edit_stationary_vendor">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                            </form>
                            <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="productVendorDetailModal(<?php echo $data['vendor_id']; ?>)"> <i class="fa fa-eye"></i></button>
                            <?php if($data['vendor_active_status']=="0"){
                                  $status = "stationaryVendorStatusDeactive";  
                                  $active="checked";                     
                              } else { 
                                  $status = "stationaryVendorStatusActive";
                                  $active="";  
                              } ?>
                              <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                          </div>
                        </td>
                        <td><?php if ($data['vendor_logo']!='') { ?>
                            <a href="../img/vendor_logo/<?php echo $data['vendor_logo']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["vendor_logo"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif" data-src="../img/vendor_logo/<?php echo $data['vendor_logo']; ?>"  href="#divForm<?php echo $data['vendor_id'];?>" class="btnForm lazyload" ></a>
                            <?php } else {
                            echo "Not Uploaded";
                            } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
<div class="modal fade" id="productVendorDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Stationery Vendor Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="product_vendor_detail">

        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
