<?php  
include_once '../residentApiNew/SalaryDaysCalculation.php';


// error_reporting(E_ALL);
// ini_set('display_errors', '1');




$dId = (int) $_REQUEST['floor_id'];
$bId = (int) $_REQUEST['block_id'];

$user_id = (int)$_GET['user_id'];
if(isset($_GET['year']) && isset($_GET['month'])){
   $month  = (int)$_GET['month'];
   $year  = (int)$_GET['year'];
   $startDate = date("Y-m-d", strtotime("$year-$month"));
}
else
{
   $month  =  date('n',strtotime('last month'));
   $year  = date('Y');
   $startDate = date("Y-m-01");
}
$month_end_date = date("Y-m-t", strtotime($startDate)); 
$sId = (int)$_REQUEST['sId'];
if(isset($sId) && $sId>0 ) {
   $desabledKey = "disabled";
}
else
{
   $desabledKey = "";
}

$checkSalarySlip = $d->selectRow('*', 'salary_slip_master', "salary_start_date = '$startDate' AND salary_end_date='$month_end_date' AND user_id = '$user_id'");
$checkSalarySlipData = mysqli_fetch_assoc($checkSalarySlip);
   if($checkSalarySlipData)
   {
      $sId = $checkSalarySlipData['salary_slip_id'];
      if(!isset($_REQUEST['sId'])){
         $desabledKey = "";
      }
   }
      
   $q1 = $d->selectRow('*', "floors_master,users_master LEFT JOIN salary ON salary.user_id = users_master.user_id AND is_delete=0 AND is_preivous_salary=0", "users_master.user_id='$user_id' AND users_master.floor_id=floors_master.floor_id");
   $data = mysqli_fetch_assoc($q1);
   

   if($data['salary_type']==0){
      $salary_type = "Fixed";
   }elseif ($data['salary_type']==1) {
      $salary_type = "Per-Day";
   } else {
      $salary_type = "Per-Hour";
   }

   if (isset($_GET['month']) && $_GET['month']>0) {
      $days = cal_days_in_month(CAL_GREGORIAN, $_GET['month'], $_GET['year']);
   }


   $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL,$base_url."residentApiNew/monthAttendance.php");
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS,
                     "getMonthlyAttendanceHistoryNew=getMonthlyAttendanceHistoryNew&society_id=$society_id&user_id=$user_id&month_end_date=$month_end_date&month_start_date=$startDate");

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

   $server_output = curl_exec($ch);
  
         curl_close ($ch);

   $server_output=json_decode($server_output,true);
  // print_r('$server_output');
   $totalPresent = $server_output['total_present_days'];
  // print_r('$totalPresent');
   // print_r($server_output);
  $toatlWorkingHours = $server_output['total_month_hours'];
  $unpaid_leave_hours = $server_output['unpaid_leave_hours'];
  //$totalworkingdays = $server_output['total_working_days'];
  $totalworkingdays = $days;
  $tHours = $server_output['total_month_hour_spent'];
  $totalExtraPresent = $server_output['total_extra_days'];
  $extra_days = $server_output['total_extra_days'];
  $holidayWeekDate_temp = $server_output['total_holidays'];
  $total_holiday_extra_hrs = $server_output['total_holiday_extra_hrs'];
  $total_holiday_hrs = $server_output['total_holiday_hrs'];
  $week_off_hours = $server_output['week_off_hours'];
  $week_off_extra_hours = $server_output['week_off_extra_hours'];
  $total_leave = $server_output['total_leave'];
  $extraWorkingHoursTemp = ($server_output['total_extra_minutes']/60);
  $shift_weekoff_date_temp = $server_output['total_week_off'];
  $paid_half_day_leave = $server_output['paid_half_day_leave'];
  $total_paid_leave_count = $server_output['paid_leave'];
  $absent= $server_output['total_absent'];
  $un_paid_leave= $server_output['un_paid_leave'];
  $un_paid_half_day_leave= $server_output['un_paid_half_day_leave'];
  $total_holiday_hrs = $total_holiday_hrs- $total_holiday_extra_hrs;
  $week_off_hours = $week_off_hours- $week_off_extra_hours;
                     /////////////////emp working days calculation
            $arrayEarning= array();
            $arraDedicution= array();
            if(isset($user_id) && isset($_GET['month']) && $user_id>0)
            {
               $qs = $d->selectRow('salary.*,salary_group_master.working_day_calculation,salary_group_master.hourly_salary_extra_hours_payout,salary_group_master.extra_day_payout_type_per_day,salary_group_master.allow_extra_day_payout_per_day,salary_group_master.fixed_amount_for_per_day,salary_group_master.allow_extra_day_payout_per_hour,salary_group_master.extra_day_payout_type_per_hour,salary_group_master.fixed_amount_for_per_hour,salary_group_master.allow_extra_day_payout_fixed,salary_group_master.extra_day_payout_type_fixed,salary_group_master.fixed_amount_for_fixed,salary_group_master.extra_hour_calculation_minutes,salary_group_master.extra_hour_calculation','salary,salary_group_master',"salary.user_id ='$user_id' AND  salary.gross_salary>0 AND salary_start_date<='$startDate' AND salary_group_master.salary_group_id=salary.salary_group_id","ORDER BY salary_id DESC LIMIT 1");
               $salaryData =mysqli_fetch_array($qs);
             
               if ($salaryData['working_day_calculation']==1) {
                  // 1 for (- weekoff & + holidays)
                   $totalworkingdays = $days - $shift_weekoff_date_temp;
                   $totalworkingdays = $totalworkingdays;
                  $total_paid_holiday = $holidayWeekDate_temp; 
                  $total_paid_week_off = "0";
               } else if ($salaryData['working_day_calculation']==2) {
                  
                  // 2 for (+ weekoff & + holidays) 
                  $totalworkingdays = $days;
                  $total_paid_holiday = $holidayWeekDate_temp; 
                  $total_paid_week_off = $shift_weekoff_date_temp;
               } else {
                  // 0 for (- weekoff & - holidays),
                 $totalworkingdays = $days - $shift_weekoff_date_temp;
                  $totalworkingdays = $totalworkingdays - $holidayWeekDate_temp;
                  $total_paid_holiday = "0"; 
                  $total_paid_week_off = "0";
               }

               /* salary calculation for per day and monthlly */
               $flatAmount = $salaryData['total_fixed_amount'];////10000
               $gross_salary = $salaryData['gross_salary']-$flatAmount;////10000
               $perDaySalary = (float)$gross_salary/((float)$totalworkingdays);///  526.31578947368
               $salary_start_date = $salaryData['salary_start_date']; 
               $salary_group_id = $salaryData['salary_group_id']; 
               if ($toatlWorkingHours>0) {
                  $perHourSalary = (float)$gross_salary/((float)$toatlWorkingHours);
               }
              
              
               if($total_paid_leave_count !="" && $total_paid_leave_count >0){
                 
                  $total_paid_leave_sal = $total_paid_leave_count*$perDaySalary;
               }

               if (isset($un_paid_leave) && COUNT($un_paid_leave)>0) {
                 $un_paid_leave_count = COUNT($un_paid_leave);
               }

               if (isset($un_paid_half_day_leave) && COUNT($un_paid_half_day_leave)>0) {
                 $un_paid_half_day_leave_count = COUNT($un_paid_half_day_leave);
               }
               
               $total_unpaid_leave_count =  $absent+$un_paid_leave_count+$un_paid_half_day_leave_count;  

               if ($total_unpaid_leave_count <0) {
                  $total_unpaid_leave_count  =0;
               }

               if($data['salary_type']==0){
                 // $MonthNetSalary  = (float)$salaryData['gross_salary'];
                  $MonthNetSalary  = (float)$salaryData['gross_salary']-(float)$salaryData['total_fixed_amount'];
                  if($totalExtraPresent>0){
                     // $totalPresent  = $totalPresent -$totalExtraPresent;
                      if($salaryData){
                         if($salaryData['allow_extra_day_payout_fixed']=="1" && $salaryData['extra_day_payout_type_fixed']=="1"){
                            
                            if($salaryData['hourly_salary_extra_hours_payout']>0){
    
                              $extraPerDaySalary=((float)$salaryData['fixed_amount_for_fixed'])*$salaryData['hourly_salary_extra_hours_payout'];
                           }else{
                              $extraPerDaySalary=$salaryData['fixed_amount_for_fixed'];
                           }
                         }else if($salaryData['allow_extra_day_payout_fixed']=="1"){
 
                            if($salaryData['hourly_salary_extra_hours_payout']>0){
    
                               $extraPerDaySalary=((float)$perDaySalary)*$salaryData['hourly_salary_extra_hours_payout'];
                            }
                         }
                         $extrDaysSalary = $extraPerDaySalary*$totalExtraPresent;
                       ///  $MonthNetSalary = ((float)$perDaySalary)*((float)($totalPresent)-$totalExtraPresent);
                        // $MonthNetSalary = $extrDaysSalary+$MonthNetSalary;
                         
                      }else
                      {
                        /// $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                      }
                   }

               } else if($data['salary_type']==1) {
                  // per day salay
                  if($totalExtraPresent>0){
                    // $totalPresent  = $totalPresent -$totalExtraPresent;
                     if($salaryData){
                        if($salaryData['allow_extra_day_payout_per_day']=="1" && $salaryData['extra_day_payout_type_per_day']=="1"){
                           if($salaryData['hourly_salary_extra_hours_payout']>0){
    
                              $extraPerDaySalary=((float)$salaryData['fixed_amount_for_per_day'])*$salaryData['hourly_salary_extra_hours_payout'];
                           }else{

                              $extraPerDaySalary=$salaryData['fixed_amount_for_per_day'];
                           }
                        }else if($salaryData['allow_extra_day_payout_per_day']=="1"){

                           if($salaryData['hourly_salary_extra_hours_payout']>0){
   
                              $extraPerDaySalary=((float)$perDaySalary)*$salaryData['hourly_salary_extra_hours_payout'];
                           }
                        }
                        $extrDaysSalary = $extraPerDaySalary*$totalExtraPresent;
                        $MonthNetSalary = ((float)$perDaySalary)*((float)($totalPresent));
                       
                     }else
                     {
                        
                        $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                     }
                  }else
                  {
                     $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                   
                  }
                  $MonthNetSalary  = (float)$MonthNetSalary;
               } else {
                 // per hour salary
                  $extraWorkingHoursAvgTemp = 0;
                  $extraWorkingHoursDayByDayTemp = 0;

                  if($tHours>$toatlWorkingHours){
                     $extraWorkingHoursAvgTemp =  $tHours-$toatlWorkingHours;
                  }
                  $extraWorkingHoursDayByDayTemp = $extraWorkingHoursTemp;
                  
                 
                  if($extraWorkingHoursDayByDayTemp>0 || $extraWorkingHoursAvgTemp>0 ){
                     if($salaryData){
                        if($salaryData['extra_hour_calculation']=="1"){
                           if($salaryData['extra_hour_calculation_minutes']>0){
                              if($salaryData['extra_hour_calculation_minutes']>=$extraWorkingHoursDayByDayTemp){
                                 $extraWorkingHoursTemp =0;
                              }
                           }else{

                              $extraWorkingHoursTemp = $extraWorkingHoursDayByDayTemp;
                           }
                        }else{
                           $extraWorkingHoursTemp = $extraWorkingHoursAvgTemp;    
                        }
                       
                        if($salaryData['allow_extra_day_payout_per_hour']=="1" && $salaryData['extra_day_payout_type_per_hour']=="1"){
                           if($salaryData['hourly_salary_extra_hours_payout']>0){
                             // if($salaryData['extra_hour_calculation'])
                              $extra_hour_day_salary=((float)$salaryData['fixed_amount_for_per_hour'])*$salaryData['hourly_salary_extra_hours_payout'];
                           }else{
                              $extra_hour_day_salary=$salaryData['fixed_amount_for_per_hour'];
                           }

                        }else if($salaryData['allow_extra_day_payout_per_hour']=="1"){

                           if($salaryData['hourly_salary_extra_hours_payout']>0){
                              $extra_hour_day_salary=((float)$perHourSalary)*$salaryData['hourly_salary_extra_hours_payout'];
                           }
                        }
                        echo '$total_holiday_extra_hrs'.$total_holiday_extra_hrs;
                        echo '$week_off_extra_hours'.$week_off_extra_hours;
                        $extraWorkingHoursTemp =$extraWorkingHoursTemp+$total_holiday_extra_hrs+$week_off_extra_hours;
                        $extrDaysSalary = $extra_hour_day_salary*$extraWorkingHoursTemp;
                        $MonthNetSalary = ((float)$perHourSalary)*((float)($tHours));
                           
                       // echo $tHours;
                       /// $MonthNetSalary = $extrDaysSalary+$MonthNetSalary;
                        
                     }else
                     {
                        $MonthNetSalary = ((float)$perHourSalary)*(float)($tHours);
                     }
                  }else
                  {
                     $MonthNetSalary = ((float)$perHourSalary)*(float)($tHours);
                  }
                  $MonthNetSalary  = (float)$MonthNetSalary;
               }
              
              

               if ($salaryData['working_day_calculation']==1 && $totalPresent>0) {
                  // 1 for (- weekoff & + holidays)
                  $totalPaidWeekOff = "0";
                  $totalPaidHoliday = $holidayWeekDate_temp*$perDaySalary;
               } else if ($salaryData['working_day_calculation']==2 && $totalPresent>0) {
                  // 2 for (+ weekoff & + holidays) 
                  $totalPaidWeekOff = $shift_weekoff_date_temp*$perDaySalary;
                  $totalPaidHoliday = $holidayWeekDate_temp*$perDaySalary;
               } else {
                  $totalPaidWeekOff = "0";
                  $totalPaidHoliday = "0";
               }

               if($salaryData)
               {

                  
                  $q=$d->selectRow("salary_master.*,salary_earning_deduction_type_master.earning_deduction_type,salary_earning_deduction_type_master.earning_deduction_name","salary_master LEFT JOIN salary_earning_deduction_type_master ON salary_master.salary_earning_deduction_id =salary_earning_deduction_type_master.salary_earning_deduction_id","salary_master.salary_id='$salaryData[salary_id]'","GROUP BY salary_master.salary_master_id");
               }

            }
           
            $counter = 1;
            $totalEarns = 0;
            $totalEarn = 0;
            $totalDeductAmnt = 0;
            $totalContribution = 0;
            $totalDAr = array();
            $totalDeduct = array();
            $salaryDeductForPrecent = array();
            $earningArray["earning"] = array();
            $earningArray["deduction"] = array();
           
            $MonthNetSalary +=$total_paid_leave_sal;
            $MonthNetSalary +=$totalPaidWeekOff;
            $MonthNetSalary +=$totalPaidHoliday;
            
            //$flatAmount = 0;
            if (isset($q) && mysqli_num_rows($q)>0) {
            
               while ($data3=mysqli_fetch_assoc($q)) {
                  
                 
                  $asalComn = $d->selectRow('salary_common_value_master.salary_common_value_id,salary_common_value_master.salary_earning_deduction_id,salary_common_value_master.salary_common_value_remark,salary_common_value_master.amount_value,salary_common_value_master.slab_json,salary_common_value_master.salary_common_value_earn_deduction,salary_common_value_master.percent_deduction_max_amount,salary_common_value_master.percent_min_max'," salary_common_value_master", "salary_common_value_master.salary_earning_deduction_id = '$data3[salary_earning_deduction_id]' AND salary_common_value_master.salary_group_id='$salary_group_id'");
                  $commnVal = mysqli_fetch_assoc($asalComn);
                  $data3['salary_common_value_id'] =$commnVal['salary_common_value_id'];
                  $data3['percent_min_max'] =$commnVal['percent_min_max'];
                  $data3['percent_deduction_max_amount'] =$commnVal['percent_deduction_max_amount'];
                  $data3['salary_earning_deduction_id'] =$commnVal['salary_earning_deduction_id'];
                  $data3['salary_common_value_remark'] =$commnVal['salary_common_value_remark'];
                  $data3['slab_json'] =$commnVal['slab_json'];
                  $data3['amount_value'] =$commnVal['amount_value'];
                  $data3['salary_common_value_earn_deduction'] =$commnVal['salary_common_value_earn_deduction'];
                  // if($data3['earning_deduction_type']=='0' && $data3['amount_type']=="1"){
                     
                  //    $flatAmount = $flatAmount + $data3['amount_value'];
                  //    $MonthNetSalary = $MonthNetSalary-$flatAmount;
                  // }
                 ////////////////Earning calculation
                  if($data3['earning_deduction_type']==0)
                  { 
                    
                     $earning = array();
                     if ($data3['amount_type']==1) {
                        $Deamnt = $data3['salary_earning_deduction_value'];
                     } else {
                       // $Deamnt =((float)$MonthNetSalary*(float)$data3['amount_percentage'])/100;
                       if($data3['salary_common_value_earn_deduction'] !=""){
                        $ernOnErnIdsAr = explode(',',$data3['salary_common_value_earn_deduction']);
                        $baseAmount = 0;
                        foreach ($ernOnErnIdsAr as $u => $n) {
                          
                           $getSub = $d->selectRow('salary_master.*,salary.salary_group_id,salary_common_value_master.amount_value AS salary_common_amount_value','salary_master LEFT JOIN salary ON  salary.salary_id = salary_master.salary_id
                           LEFT JOIN salary_common_value_master ON  salary_common_value_master.salary_group_id = salary.salary_group_id',"salary_master.salary_id=$data3[salary_id] AND salary_master.salary_earning_deduction_id =$n AND salary_common_value_master.salary_earning_deduction_id=$n");
                           $getSubData =mysqli_fetch_assoc($getSub);
                           $baseAmount = (float)$baseAmount+((float)$MonthNetSalary*(float)$getSubData['salary_common_amount_value'])/100;
                        }
                        $Deamnt = ((float)$baseAmount*(float)$data3['amount_value'])/100;
                       }else{

                          $Deamnt =((float)$MonthNetSalary*(float)$data3['amount_value'])/100;
                       }
                     }
                     $basicArray = array('salary_earning_deduction_id'=>$data3['salary_earning_deduction_id'],'amount'=>$Deamnt);
                     $salaryDeductForPrecent[$data3['salary_earning_deduction_id']]= $Deamnt;
                     $data3['amount']=$Deamnt;
                     $totalEarn +=(float)$Deamnt;

                     $earning["amount"] = $Deamnt;
                     $earning["salary_earning_deduction_id"] = $data3['salary_earning_deduction_id'];
                     $earning["earning_percentage"] = number_format((float)$data3['amount_percentage'], 2, '.', '');;
                     $earning["earning_percentage_show"] = number_format((float)$data3['amount_value'], 2, '.', '');
                     $earning["earning_deduction_name"] = $data3['earning_deduction_name'];
                     $earning["salary_common_value_remark"] = $data3['salary_common_value_remark'];
                     $earning["amount_value"] = $data3['amount_value'];
                     $earning["amount_type"] = $data3['amount_type'];
                     if($data3['salary_common_value_earn_deduction'] !=""){
                        $subEarnValue = $d->selectRow('GROUP_CONCAT(earning_deduction_name) AS earning_deduction_name','salary_earning_deduction_type_master',"salary_earning_deduction_id IN ($data3[salary_common_value_earn_deduction])");

                        $subEarnValueData =  mysqli_fetch_assoc($subEarnValue);
                       $earning["earn_show_name"] = $subEarnValueData['earning_deduction_name'];
                     }else{
                        $earning["earn_show_name"] = '';
                     }

                     array_push($earningArray["earning"], $earning);
                      array_push($arrayEarning,$data3);
                  }
                  else
                  {
                     $deduction = array();
                     //////////////amount_type =1 flat,0 percent
                     $totalDeducts=0;
                     if($data3['amount_type']==1)
                     {
                        $totalDeducts = $data3['salary_earning_deduction_value'];
                        $data3['amount']=$totalDeducts;
                        $data3['contribution_amount']=$data3['contribution_amount'];
                        $contribution_amount=$data3['contribution_amount'];
                        $data3['contribution_precent']=$data3['contribution_precent'];
                        $showNameForApply = "";
                        $totalContribution = $totalContribution +$contribution_amount;
                        array_push($totalDeduct,(float)$totalDeducts);
                     } else if($data3['amount_type']==0) {
                        
                        $tempMultiDeductionArray = array();
                        $dedecution_precent = $data3['amount_percentage'];
                        $salary_common_value_id=$data3['salary_common_value_id'];
                        $salary_common_value_earn_deduction=$data3['salary_common_value_earn_deduction'];
                       
                       if($data3['salary_common_value_earn_deduction'] !=""){
                        $ernOnErnIdsArNew = explode(',',$data3['salary_common_value_earn_deduction']);
                        $contributionEarning = 0;
                        
                           $totalEarnContri = 0;
                           for ($newCOntri=0; $newCOntri <COUNT($earningArray['earning']) ; $newCOntri++) { 
                              $kContri = array_search($ernOnErnIdsArNew[$newCOntri], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                             
                              if ($kContri === 0 || $kContri > 0) {

                                 $totalEarnContri += $earningArray['earning'][$kContri]['amount'];
                              }
                           }
                           $contributionEarning = (float)$contributionEarning+$totalEarnContri;
                           $contribution_amount = ((float)$contributionEarning*(float)$data3['contribution_precent'])/100;
                       }else{
                          $contribution_amount = $MonthNetSalary*$data3['contribution_precent']/100;
                       }
                      
                       $totalContribution = $totalContribution +$contribution_amount;
                       
                        $salaryCvED = explode(',',$salary_common_value_earn_deduction);
                        $tempValueDedcution = 0;
                       
                           $showNameForApply = array();
                           for ($iD=0; $iD <count($earningArray['earning']) ; $iD++) { 
                             
                              $ks = array_search($salaryCvED[$iD], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                             
                              if ($ks === 0 || $ks > 0) {
                                
                                 $tempValueDedcution += $earningArray['earning'][$ks]['amount'];
                                 array_push($tempMultiDeductionArray,$salaryCvED[$iD]);
                                 array_push($showNameForApply,$earningArray['earning'][$ks]['earning_deduction_name']);

                              }
                           }
                          
                       
                           $totalCombineDeduction = $tempValueDedcution;
                           $perstageDedcutionValue = $totalCombineDeduction *  $dedecution_precent /100;
                          
                        $x = 1;
                       
                     if($data3['percent_min_max'] !=""){
                        $percent_min_max = json_decode($data3['percent_min_max'],true);
                        if($gross_salary>=$percent_min_max['percent_min'] && $gross_salary<=$percent_min_max['percent_max']){
                           if($data3['percent_deduction_max_amount']>0){
                              if($perstageDedcutionValue<= $data3['percent_deduction_max_amount'])
                              {
                                 $totalDeducts = $perstageDedcutionValue;
                              }else
                              {
                                 $totalDeducts=$data3['percent_deduction_max_amount'];
                              }
                            }
                              else
                              {
                                 $totalDeducts=$perstageDedcutionValue;
                              }
                            if($data3['percent_deduction_max_amount']>0){
                              if($contribution_amount<= $data3['percent_deduction_max_amount'])
                              {
                                 $contribution_amount = $contribution_amount;
                              }else
                              {
                                 $contribution_amount=$data3['percent_deduction_max_amount'];
                              }
                            }
                            else
                           {
                              $contribution_amount=$contribution_amount;
                           }
                        }else{
                           $totalDeducts = 0;
                           $contribution_amount = 0;
                        }
                     }else{
                       
                        if($data3['percent_deduction_max_amount']>0){
                          if($perstageDedcutionValue<= $data3['percent_deduction_max_amount'])
                          {
                             $totalDeducts = $perstageDedcutionValue;
                          }else
                          {
                             $totalDeducts=$data3['percent_deduction_max_amount'];
                          }
                        }
                          else
                          {
                             $totalDeducts=$perstageDedcutionValue;
                          }
                        if($data3['percent_deduction_max_amount']>0){
                          if($contribution_amount<= $data3['percent_deduction_max_amount'])
                          {
                             $contribution_amount = $contribution_amount;
                          }else
                          {
                             $contribution_amount=$data3['percent_deduction_max_amount'];
                          }
                        }
                        else
                       {
                          $contribution_amount=$contribution_amount;
                       }
                     }
                      
                     $data3['contribution_amount']=$contribution_amount;
                        array_push($totalDeduct,(float)$totalDeducts);
                      
                     }
                     else
                     {
                        /////////////slab calculation (deduction)
                       
                        $slabArray = explode('~',$data3['slab_json']);


                        $tempMultiDeductionArray = array();
                        $dedecution_precent = $data3['amount_percentage'];
                        $salary_common_value_id=$data3['salary_common_value_id'];
                        $salary_common_value_earn_deduction=$data3['salary_common_value_earn_deduction'];
                        $salaryCvED = explode(',',$salary_common_value_earn_deduction);
                       
                        $tempValueDedcution = 0;
                           $showNameForApply = array();
                           for ($iD=0; $iD <count($earningArray['earning']) ; $iD++) { 
                              
                              $ks = array_search($salaryCvED[$iD], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                              if ($ks === 0 || $ks > 0) {
                             
                                 $tempValueDedcution += $earningArray['earning'][$ks]['amount'];
                                 array_push($tempMultiDeductionArray,$salaryCvED[$iD]);
                                 array_push($showNameForApply,$earningArray['earning'][$ks]['earning_deduction_name']);

                              }
                           }
                          
                       $totalCombineDeduction = $tempValueDedcution;
                        
                       $perstageDedcutionValue = $totalCombineDeduction;
                        $x = 1;
                        $totalDeducts = $perstageDedcutionValue;

                       for ($k=0; $k <count($slabArray) ; $k++) { 
                          
                           $slabDecodedJson = json_decode($slabArray[$k],true);
                         
                          if((float)$slabDecodedJson['min']<=$totalDeducts &&(float)$slabDecodedJson['max']>=$totalDeducts){

                             $totalDeducts = $slabDecodedJson['value'];
                             
                             array_push($totalDeduct,(float)$totalDeducts);
                             
                          } else {
                             $totalDeducts = 0;
                          }
                        }
                        
                     }
                    
                     $deduction["amount"] = $totalDeducts;
                     $deduction["show_name_apply_form"] = $showNameForApply;
                     $deduction["contribution_precent"] = $data3['contribution_precent'];
                     $deduction["salary_earning_deduction_id"] = $data3['salary_earning_deduction_id'];
                     $deduction["contribution_amount"] = $data3['contribution_amount'];
                     $deduction["earning_deduction_name"] = $data3['earning_deduction_name'];
                     $deduction["deduction_percentage"] = number_format((float)$data3['amount_percentage'], 2, '.', '');
                     $deduction["salary_common_value_remark"] = $data3['salary_common_value_remark'];
                     $deduction["amount_value"] = $data3['amount_value'];
                     $deduction["amount_type"] = $data3['amount_type'];
                     array_push($earningArray["deduction"], $deduction);
                     array_push($arraDedicution,$data3);
                  }
                  
               }
            }
          
             if(isset($sId) && $sId>0){
               $salary_slip=$d->selectRow('*','salary_slip_master',"salary_slip_id=$sId");
               $salary_slip_data= mysqli_fetch_assoc($salary_slip);
             }
          
         

         

         


        if($data['salary_type']==0){
         $final_emp_salary =  $totalEarn -array_sum($totalDeduct);
        }
        else if($data['salary_type']==2)
        {
         $final_emp_salary =$totalEarn- array_sum($totalDeduct);
        }else
        {
         $final_emp_salary = $totalEarn -array_sum($totalDeduct);
        }
       
        /* expense calcualtion */
        
        $advanceAmount = 0;
        $expanceAmnt = 0;
         if( $sId>0 && $salary_slip_data['expense_amount']>0) {
          
            $expanceAmnt=$salary_slip_data['expense_amount'];
         } else {
            $expanceAmnt = 0;
          
            $expQ = $d->selectRow("user_expenses.*,(SELECT SUM(amount) FROM `user_expenses` WHERE user_expenses.user_id=$user_id AND user_expenses.expense_status=1  AND user_expenses.expense_paid_status=0 AND  user_expenses.date  BETWEEN '$startDate' AND '$month_end_date') AS total_expense_amount",'user_expenses',"user_expenses.user_id=$user_id AND user_expenses.expense_status=1  AND user_expenses.expense_paid_status=0 AND  user_expenses.date  BETWEEN '$startDate' AND '$month_end_date'");
            $expanceData = mysqli_fetch_assoc($expQ);
        
            /* while ($expanceData= mysqli_fetch_array($expQ)) {
               if(isset($expanceData['amount'])){
                  $expanceAmnt +=(float)$expanceData['amount'];
               }
            } */
            if($expanceData['total_expense_amount'] !=""){

               $expanceAmnt = $expanceData['total_expense_amount'];
            }
           
           
         }

         if( $sId>0 && $salary_slip_data['advance_salary_paid_amount']>0) {
          
            $advanceAmount=$salary_slip_data['advance_salary_paid_amount'];
            $appylyReadOnlyAdvance = "readonly";
         } else {
             $advanceQ = $d->selectRow("advance_salary.*,(SELECT SUM(advance_salary_amount) FROM `advance_salary` WHERE advance_salary_date BETWEEN '$startDate' AND '$month_end_date' AND user_id = $user_id AND is_paid=0) AS advance_amount",'advance_salary',"advance_salary.user_id=$user_id AND advance_salary.is_paid=0 AND  advance_salary.advance_salary_date  BETWEEN '$startDate' AND '$month_end_date'");
           
            if(mysqli_num_rows($advanceQ)>0){
               $advanceData = mysqli_fetch_assoc($advanceQ);
              
               $advanceAmount  = $advanceData['advance_amount'];
               if($final_emp_salary>$advanceAmount){
                  $advanceAmount = $advanceAmount;
               }else{
                  $advanceAmount = 0;
               }
            }else
            {
               $advanceAmount = 0;
            }
         }

         if( $sId>0 && $salary_slip_data['emi_deduction']>0) {
          
            $emi_deduction=$salary_slip_data['emi_deduction'];
         } else {
            $loan_emi_ids = array();
            $loadAmount = array();
            $expQ = $d->selectRow('loan_emi_master.*','loan_emi_master',"loan_emi_master.user_id='$user_id'  AND loan_emi_master.is_emi_paid=0 AND loan_emi_master.emi_created_at  BETWEEN '$startDate' AND '$month_end_date'");
              while ($loanData= mysqli_fetch_array($expQ)) {
                  array_push($loan_emi_ids,$loanData['loan_emi_id']);
                  array_push($loadAmount,$loanData['emi_amount']);
              }

              $emi_deduction = array_sum($loadAmount);

              if ($emi_deduction<$final_emp_salary) {
                  $emi_deduction = number_format((float)$emi_deduction, 2, '.', ''); ;
              } else {
                  $emi_deduction = 0;
              }
         }

         $final_emp_salary = $final_emp_salary;
         $final_amn_with_Expense = $final_emp_salary+$expanceAmnt;
         $totalEarnWithExpense = $totalEarn;

         ?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-12 col-12">
            <h4 class="page-title">Generate Salary Slip Individual Employee </h4>
         </div>
    
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
            <form id="salaryBasicAdd" action="" enctype="multipart/form-data" method="get">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label for="input-10" class="col-md-12 col-form-label">Branch<span class="required">*</span></label>
                           <div class=" col-md-12" id="">
                              <select <?php echo $desabledKey; ?> id="block_id" name="block_id" class="form-control single-select" onchange="getFloorByBlockIdAddSalary(this.value)"  required="">
                                 <option value="">Select Branch</option> 
                                 <?php 
                                    $qd=$d->select("block_master","block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQuery");  
                                    while ($depaData=mysqli_fetch_array($qd)) {
                                 ?>
                                 <option <?php if($bId==$depaData['block_id']) { echo 'selected';} ?>  <?php if($_GET['block_id']==$depaData['block_id']) { echo 'selected';} ?> value="<?php echo  $depaData['block_id'];?>" ><?php echo $depaData['block_name'];?> </option>
                                 <?php } ?>
                                 
                                 </select>
                           </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <label for="input-10" class="col-md-12 col-form-label">Department <span class="required">*</span></label>
                        <div class="col-md-12" id="">
                           <select <?php echo $desabledKey; ?>  type="text" id="floor_id" required="" onchange="getUserForSalarySlip(this.value)" class="form-control single-select" name="floor_id" >
                              <option value="">-- Select --</option>
                               <?php
                                 $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id='$bId'");
                                 while ($floorsData = mysqli_fetch_array($floors)) {
                                    ?>
                              <option  <?php if (isset($floorsData['floor_id']) && $dId == $floorsData['floor_id']) {echo "selected"; }?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {echo $floorsData['floor_id'];}?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {echo $floorsData['floor_name']."(".$floorsData['block_name'].")";}?></option>
                              <?php }?> 
                           </select>
                        </div>
                     </div>
                  </div>
                  
                  <div class="col-md-3">
                     <div class="form-group" >
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year  <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                           <select <?php echo $desabledKey; ?>   class="form-control single-select year" onchange="yearChange(this.value)" name="year" id="year">
                              <option value="">-- Select --</option>
                                 <?php
                                    $currently_selected = date('Y');
                                    $earliest_year = date("Y", strtotime("-4 year"));
                                    $latest_year = date('Y');
                                    foreach (range($latest_year, $earliest_year) as $i) {
                                    ?>
                                     <option <?php if ($i == date("Y") || $i == $year) {echo "selected";}?> data-id="<?php echo $i; ?>" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                 <?php }

                                 ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group" >
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label"> Month <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                           <select <?php echo $desabledKey; ?>  onchange="submitForGetDetails();" class="form-control single-select month" name="month" id="month">
                              <option value="">-- Select --</option>
                              <?php
                                    if(isset($year) && $year !=""){
                                       if($year==date('Y')){
                                          $cntMonth = date('m');
                                         // $cntMonth = $cntMonth;
                                       }else if($year<date('Y')){
                                          $cntMonth = 13;
                                       }
                                    }else{
                                       $cntMonth = date('m');
                                    }
                                    for ($m = 1; $m <$cntMonth; $m++) {
                                       $monthView = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                       $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                                       ?>
                              <option  <?php if(isset($_GET['month']) && $_GET['month']== $m || $m==$month){ echo "selected"; }?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $monthView; ?></option>
                                 <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label ">Employee<span class="required">*</span></label>
                           <div class="col-lg-12 col-md-12" id="">
                           <select <?php echo $desabledKey; ?> onchange="submitForGetDetails()" id="user_id01"  type="text"   class="form-control single-select getUserGenratedSalary user_id01" name="user_id">
                              <option value="">-- Select --</option>

                              </select>
                           </div>
                     </div>
                  </div>
                  
               </div>
            </form>
            <?php
             if(isset($user_id) && isset($_GET['month']) && $user_id>0)
            {?>
         <form id="salarySlip" class="salaryAdd" action="controller/SalarySlipController.php" enctype="multipart/form-data" method="post">
            <div class="row">
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class=" col-lg-12 col-sm-12 col-form-label">Salary Type <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="salary_type" name="salary_type" value="<?php echo $salary_type; ?>" >
                     </div>
                  </div>
               </div>
               <?php 
               if($data['salary_type']!=2)
               {
             ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Month Working Days  <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="month_working_days" name="month_working_days" value="<?php echo $totalworkingdays; ?>" >
                           <input type="text" class="form-control "  readonly id="month_working_days1" name="month_working_days1" value="<?php echo round($totalworkingdays,2); ?>" >
                           <input type="hidden" class="form-control "  readonly  name="total_month_hours" value="<?php echo $toatlWorkingHours; ?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-lg-12 col-form-label">Employee Working Days <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="employee_working_days" name="employee_working_days" value="<?php echo $totalPresent; ?>" >
                           <input type="text" class="form-control "  readonly id="employee_working_days1" name="employee_working_days1" value="<?php echo $totalPresent; ?>" >
                           <input type="hidden"   readonly id="total_working_hours" name="total_working_hours" value="<?php echo $tHours ; ?>" >
                     </div>
                  </div>
               </div>
               <?php }else{ ?>
                  <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Month Working Hours <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly  name="total_month_hours" value="<?php echo $toatlWorkingHours; ?>" >
                           <input type="text" class="form-control "  readonly  name="total_month_hours1" value="<?php echo round($toatlWorkingHours,2); ?>" >
                           <?php echo sprintf('%02d hr %02d min', (int) $toatlWorkingHours, fmod($toatlWorkingHours, 1) * 60); ?>
                           <input type="hidden" class="form-control "  readonly  name="month_working_days" value="<?php echo $totalworkingdays; ?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-lg-12 col-form-label">Employee Working Hours <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly  name="total_working_hours" value="<?php echo $tHours ; ?>" >
                           <input type="text" class="form-control "  readonly  name="total_working_hoursssss" value="<?php echo round($tHours,2) ; ?>" >
                           <input type="hidden" readonly  id="employee_working_days" name="employee_working_days" value="<?php echo $totalPresent; ?>" >
                           <i><?php echo sprintf('%02d hr %02d min', (int) $tHours, fmod($tHours, 1) * 60); ?></i>
                     </div>
                  </div>
               </div>
               <?php } ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Paid Leave <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="paid_leave_days" name="paid_leave_days" value="<?php echo $total_paid_leave_count;?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Unpaid Leave /Absent<span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                        <input type="text" class="form-control "  readonly id="unpaid_leave_days" name="unpaid_leave_days" value="<?php if($total_unpaid_leave_count !=""){echo $total_unpaid_leave_count;} else { echo 0; }?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Total Leave <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="leave_days" name="leave_days" value="<?php echo $total_paid_leave_count+$total_unpaid_leave_count;?>" >
                     </div>
                  </div>
               </div>
                <?php 
               if($data['salary_type']!=2)
                {
               ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Extra Days <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="extra_days" name="extra_days" value="<?php echo $extra_days;?>" >
                     </div>
                  </div>
               </div>
              
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label"> Per Day Salary (Extra Days)<span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="extra_days_per_day_salary" name="extra_days_per_day_salary" value="<?php echo $extraPerDaySalary;?>" >
                     </div>
                  </div>
               </div>
               <?php } else { ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Extra Hours <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="extra_days" name="extra_days" value="<?php echo $extra_days;?>" >
                           <input type="hidden" class="form-control "  readonly id="extra_hours" name="extra_hours" value="<?php echo $extraWorkingHoursTemp;?>" >
                           <input readonly type="text" class="form-control" value="<?php echo round($extraWorkingHoursTemp,2);?>" >
                           <i><?php echo sprintf('%02d hr %02d min', (int) $extraWorkingHoursTemp, fmod($extraWorkingHoursTemp, 1) * 60); ?></i>
                     </div>
                  </div>
               </div>

               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label"> Per Hour Salary (Extra Days)<span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="extra_days_per_day_salary" name="extra_days_per_day_salary" value="<?php echo $extraPerDaySalary;?>" >
                           <input type="text" class="form-control "  readonly id="extra_per_hour_salary" name="extra_per_hour_salary" value="<?php echo round($extra_hour_day_salary,2);?>" >
                     </div>
                  </div>
               </div>
               <?php } ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Salary Mode <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <select class="form-control " required name="salary_mode" id="salary_mode">
                              <option  value="">-- Select --</option>
                              <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==0) { echo "selected"; }?> value="0">Bank Transfer</option>
                              <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==1){ echo "selected"; }?> value="1">Cash</option>
                              <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==2){ echo "selected"; }?> value="2">Cheque</option>
                           </select>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Salary Status <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                        <?php
                        
                        $checkMenu = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryChecked'");
                        $getMenu = mysqli_fetch_assoc($checkMenu);
                        $checkMenu2 = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryPublished'");
                        $getMenu2 = mysqli_fetch_assoc($checkMenu2);
                        ?>
                     <select class="form-control " required name="salary_slip_status" id="salary_slip_status" onclick="changeStatusAction(this.value)">
                        <option value="">-- Select --</option>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 0) {
                                 echo "selected";
                                 } ?> value="0">Generate</option>
                     <?php
                     if($getMenu){
                     if(in_array($getMenu['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 1) {
                                 echo "selected";
                                 } ?> value="1"> Generate & Check</option>
                     <?php } } ?>
                     <?php 
                     if($getMenu2){
                     if(in_array($getMenu2['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 2) {
                                 echo "selected";
                                 } ?> value="2">Generate & Publish</option>
                     <?php } } ?>
                     </select>
                     </div>
                  </div>
               </div>
               <?php            
               if (isset($salary_slip_data) && $salary_slip_data['share_with_user'] == 1 || isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 2) {
                  $dnClass = "";
               }else {
                  $dnClass = " d-none";
               }?>
               <div class="col-md-3 sharWUser <?php echo $dnClass; ?>">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Share With User <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                        <input type="radio" <?php if (isset($salary_slip_data) && $salary_slip_data['share_with_user'] == "1") {
                                 echo "checked"; } else { echo "" ;} ?> name="share_with_user" class="mr-2" value="1"><label> Yes</label>
                        <input type="radio" <?php if (isset($salary_slip_data)) {
                                 if($salary_slip_data['share_with_user'] == 0) 
                                 {echo "checked"; }else{ echo ""; } }
                                 else { echo "checked" ; } ?>  name="share_with_user"  class="mr-2" value="0"><label> No</label>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Joining Net Salary <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="total_net_salary" name="total_net_salary" value="<?php if (isset($salaryData) && $salaryData['net_salary'] != "") {echo $salaryData['net_salary'];}?>" >
                           <input type="text" class="form-control "  readonly id="total_net_salary1" name="total_net_salary1" value="<?php if (isset($salaryData) && $salaryData['net_salary'] != "") {echo round($salaryData['net_salary'],2);}?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Joining Gross Salary <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="gross_salary" name="gross_salary" value="<?php if (isset($salaryData) && $salaryData['gross_salary'] != "") {echo $salaryData['gross_salary'];}?>" >
                           <input type="text" class="form-control "  readonly id="gross_salary1" name="gross_salary1" value="<?php if (isset($salaryData) && $salaryData['gross_salary'] != "") {echo round($salaryData['gross_salary'],2);}?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">This Month Gross Salary <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="month_net_salary" name="month_net_salary" value="<?php echo $MonthNetSalary;?>" >
                           <input type="text" class="form-control "  readonly id="month_net_salarylll" name="month_net_salaryll" value="<?php echo round($MonthNetSalary,2);?>" >
                           <i class="text-danger">NOTE:Excluded Flat Amount And Extra Day/Hours Payout</i>
                     </div>
                  </div>
               </div>
               <?php if($data['salary_type']!=2){ ?>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">
                       Per Day Salary (Working Day)
                        <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly id="per_day_salary" name="per_day_salary" value="<?php echo $perDaySalary;?>" >
                           <input type="text" class="form-control "  readonly id="per_day_salarykkk" name="per_day_salarykkk" value="<?php echo round($perDaySalary,2);?>" >
                           <input type="hidden" class="form-control "  readonly  name="per_hour_salary" value="<?php echo $perHourSalary;?>" >
                     </div>
                  </div>
               </div>
               <?php
               } else { ?>
                  <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">
                        This Month Per Hour Salary
                        <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="hidden" class="form-control "  readonly name="per_hour_salary" value="<?php echo $perHourSalary;?>" >
                           <input type="text" class="form-control "  readonly name="per_hour_salaryhhh" value="<?php echo round($perHourSalary,2);?>" >
                           <input type="hidden" class="form-control "  readonly  name="per_day_salary" value="<?php echo $perDaySalary;?>" >

                        </div>
                  </div>
               </div>
               <?php } ?>
             
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Paid Holidays <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                        <input type="text" class="form-control "  readonly id="" name="total_paid_holiday" value="<?php if($total_paid_holiday=='') { echo "0";} else { echo $total_paid_holiday; } ?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Paid Week off <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                        <input type="text" class="form-control "  readonly id="" name="total_paid_week_off" value="<?php  if($total_paid_week_off=='') { echo "0";} else { echo $total_paid_week_off; } ?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group ">
                     <label for="input-10" class="col-sm-12 col-form-label">Description</label>
                     <div class="col-lg-12 col-md-12" id="">
                           <textarea class="form-control "   id="salary_mode_description" name="salary_mode_description" value="" ><?php echo $checkSalarySlipData['salary_mode_description'];?></textarea>
                     </div>
                  </div>
               </div>
              
            </div>
            <div class="form-group row">
                     <div class="col-md-6" >
                        <div class="row container-fluid">
                           <?php
                        $arrayEarning =$earningArray['earning'];
                           for ($iE=0; $iE < count($earningArray['earning']) ; $iE++) { 
                                 $symbol = "%";
                           ?> 
                           <div class="col-md-12">
                              <label for="input-10"><?php echo $arrayEarning[$iE]['earning_deduction_name']?> (<?php if($arrayEarning[$iE]['amount_type']==0) { echo $arrayEarning[$iE]['earning_percentage_show']."".$symbol; }else { echo "Flat"; } ?>)<?php if($arrayEarning[$iE]['salary_common_value_remark']!="") {?> <i class="fa fa-question-circle   " aria-hidden="true" data-toggle="tooltip" title="<?php echo $arrayEarning[$iE]['salary_common_value_remark']; ?>"></i><?php } if(isset($arrayEarning[$iE]['earn_show_name']) && $arrayEarning[$iE]['earn_show_name'] !="") { echo "( ".$arrayEarning[$iE]['earn_show_name']." )";} ?> </label>
                           </div>
                           <div class="col-md-12">
                              <input readonly value="<?php if(isset($salaryData['salary_id'])){echo  $arrayEarning[$iE]['amount']; } else {echo 0; }?>" id="earning_<?php echo $iE;?>" type="text" class="form-control earning"   name="salary_earning_deduction_id[<?php echo $arrayEarning[$iE]['salary_earning_deduction_id']?>]" >
                              <input type="hidden" name="earning_deduction_parcent[<?php echo $arrayEarning[$iE]['salary_earning_deduction_id']?>]" value="<?php echo $arrayEarning[$iE]['earning_percentage'];?>">
                           </div>
                           <?php 
                           }
                           ?>
                           <div class="col-md-12">
                              <label for="input-10">Other Earning </label>
                           </div>
                           <div class="col-md-12">
                              <input autocomplete="off" value="<?php if(isset($salary_slip_data) && $salary_slip_data['other_earning']!="") { echo $salary_slip_data['other_earning']; }?>" id="other_earning"   type="text" class="form-control onlyNumber " onkeyup="otherEarncalculation(this.value)"  name="other_earning" >
                           </div>
                           <?php if((isset($salary_slip_data) && $salary_slip_data['overtime_amount']!="" && $salary_slip_data['overtime_amount']>0) || $extrDaysSalary>0) {?>
                           <div class="col-md-12">
                              <label for="input-10">Overtime Allowance </label>
                           </div>
                           <div class="col-md-12">
                              <input autocomplete="off" readonly value="<?php if(isset($salary_slip_data) && $salary_slip_data['overtime_amount']!="") { echo $salary_slip_data['overtime_amount']; }else{
                                 echo $extrDaysSalary; } ?>" id="overtime_amount"   type="text" class="form-control onlyNumber" name="overtime_amount" >
                           </div>
                          <?php } ?>
                           <input  value="<?php if(isset($salary_slip_data) && $salary_slip_data['expense_amount']!="") { echo $salary_slip_data['expense_amount']; }?>" id="expense_amount" readonly  type="hidden" class="form-control onlyNumber "   name="expense_amount" >
                        </div>
                     </div>
                  <div class="col-md-6" >
                     <div class="row container-fluid">
                        <?php 
                        
                         $arraDedicution =$earningArray['deduction'];
                           for ($iD=0; $iD < count($arraDedicution) ; $iD++) { 
                              if($arraDedicution[$iD]['amount_type']==0){
                                 $symbol = $arraDedicution[$iD]['deduction_percentage']." %";
                              } else if($arraDedicution[$iD]['amount_type']==2){
                                 $symbol = $arraDedicution[$iD]['amount_value']."Slab";
                              }else{
                                 $symbol = $arraDedicution[$iD]['amount_value']."Fixed";
                              }
                             
                        ?>
                       
                        <input type="hidden" name="earning_deduction_parcent[<?php echo $arraDedicution[$iD]['salary_earning_deduction_id']?>]" value="<?php echo $arraDedicution[$iD]['deduction_percentage'];?>">
                        <div class="col-md-6">
                           <label for="input-10"><?php echo $arraDedicution[$iD]['earning_deduction_name']?> (<?php echo $symbol;?>) <?php if($arraDedicution[$iD]['salary_common_value_remark']!="") {?> <i class="fa fa-question-circle   " aria-hidden="true" data-toggle="tooltip" title="<?php echo $arraDedicution[$iD]['salary_common_value_remark']; ?>"></i> <?php } ?></label>
                           <span><?php if(!empty($arraDedicution[$iD]['show_name_apply_form'])){ echo "(". implode('+',$arraDedicution[$iD]['show_name_apply_form']).")";} ?></span>
                           <input readonly value="<?php if(isset($salaryData['salary_id'])){ echo $arraDedicution[$iD]['amount']; } else {echo 0; } ?>" id="deduct_<?php echo $iD;?>" type="text" class="form-control"   name="salary_earning_deduction_id[<?php echo $arraDedicution[$iD]['salary_earning_deduction_id']?>]" >
                        </div>
                        <?php if(isset($arraDedicution[$iD]['contribution_precent']) && $arraDedicution[$iD]['contribution_precent']>0){  ?>
                           <div class="col-md-6">
                              <label for="input-10"><?php echo $arraDedicution[$iD]['earning_deduction_name']." Employer"?> (<?php echo $arraDedicution[$iD]['contribution_precent']." %";?>) <?php if($arraDedicution[$iD]['salary_common_value_remark']!="") {?> <i class="fa fa-question-circle   " aria-hidden="true" data-toggle="tooltip" title="<?php echo $arraDedicution[$iD]['salary_common_value_remark']; ?>"></i> <?php } ?></label>
                              <?php if(!empty($arraDedicution[$iD]['show_name_apply_form'])){ echo "(". implode('+',$arraDedicution[$iD]['show_name_apply_form']).")";} ?>
                              <!-- <input type="hidden" name="contribution_amount"> -->
                              <input readonly value="<?php if(isset($salaryData['salary_id'])){ echo $arraDedicution[$iD]['contribution_amount']; } else {echo 0; } ?>"  type="text" class="form-control"  name="contribution_amount[<?php echo $arraDedicution[$iD]['salary_earning_deduction_id']?>]" >
                           </div>
                        <?php } else{?>
                        <div class="col-md-6">
                        
                        </div>
                       
                       
                        <?php }
                        }
                        ?>
                         <input type="hidden" name="totalContribution"  id="totalContribution" value="<?php if(isset($totalContribution)){echo $totalContribution;} ?>">
                        <div class="col-md-12">
                           <label for="input-10">Other Deduction  </label>
                        </div>
                        <div class="col-md-12">
                           <input autocomplete="off" value="<?php if(isset($salary_slip_data) && $salary_slip_data['other_deduction']!="") { echo $salary_slip_data['other_deduction']; }?>" id="other_deduction" type="text" class="form-control onlyNumber" onkeyup="otherDeductcalculation(this.value)"  name="other_deduction" >
                        </div>
                        <div class="col-md-12 advAmount <?php if( $sId>0 && $salary_slip_data['advance_salary_paid_amount']>0) { "";} else { echo "d-none";}?> ">
                              <label for="input-10">Advance Salary Deduction</label>
                        </div>
                        <div class="col-md-12 advAmount <?php if( $sId>0 && $salary_slip_data['advance_salary_paid_amount']>0) { "";} else { echo "d-none";}?>">
                           <input  value="<?php if(isset($salary_slip_data) && $salary_slip_data['advance_salary_paid_amount']!="") { echo $salary_slip_data['advance_salary_paid_amount']; } else{ echo $advanceAmount; } ?>" id="advance_salary_paid_amount" readonly  type="text" class="form-control onlyNumber "   name="advance_salary_paid_amount" >
                        </div>
                         <div class="col-md-12 emiAmount <?php if( $sId>0 && $salary_slip_data['emi_deduction']>0) { "";} else { echo "d-none";}?> ">
                              <label for="input-10">Loan E.M.I Deduction</label>
                        </div>
                        <div class="col-md-12 emiAmount <?php if( $sId>0 && $salary_slip_data['emi_deduction']>0) { "";} else { echo "d-none";}?>">
                           <input  value="<?php if(isset($salary_slip_data) && $salary_slip_data['emi_deduction']!="") { echo $salary_slip_data['emi_deduction']; } else{ echo $emi_deduction; } ?>" id="emi_deduction" readonly  type="text" class="form-control onlyNumber "   name="emi_deduction" >
                        </div>
                     </div>
                 </div>
                <div class="col-md-6">
                  <div class="row container-fluid ">
                     <label for="input-10" class="col-sm-12 col-form-label">Total Earning <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="total_earning_salary" name="total_earning_salary" value="<?php echo $totalEarn+$extrDaysSalary;?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="row container-fluid ">
                     <label for="input-10" class="col-sm-12 col-form-label">Total Deduction <span class="required">*</span></label>
                     
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control "  readonly id="total_deduction_salary" name="total_deduction_salary" value="<?php if( $sId>0 && $salary_slip_data['total_deduction_salary']>0) { echo $salary_slip_data['total_deduction_salary']; } else {  echo array_sum($totalDeduct); } ?>" >
                     </div>
                  </div>
               </div>
               <div class="col-md-12">
                  <?php if($expanceAmnt>0){ ?>
                  <div class="form-group mt-2">
                     <?php if( $sId>0 && $salary_slip_data['expense_amount']>0) { 
                        $labelNameAdvaceExpense = "Reimbursement/Expense Cleared";
                        $newExpenceClear = "0";
                     } else {
                        $labelNameAdvaceExpense = "Clear Reimbursement/Expense";
                        $newExpenceClear = "1";
                     } ?>
                      <input type="hidden" name="newExpenceClear" value="<?php echo $newExpenceClear; ?>">
                      <label class="col-sm-12 col-form-label" for="user-checkbox"><?php echo $labelNameAdvaceExpense;?>  <span class="text-danger"><?php echo $expanceAmnt;?></span> for this month </label>
                     <?php if($salary_slip_data['expense_amount']<=0) {  ?>
                     <div class="col-lg-12 col-md-12" id="">
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_expanse_clear" type="radio" name="is_expanse_clear"  <?php if(isset($salary_slip_data['expense_amount']) && $salary_slip_data['expense_amount'] >0){ echo ""; } else { echo "checked";  }?>  id="inlineRadio2" value="0">
                           <label class="form-check-label" for="inlineRadio2">No</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_expanse_clear" type="radio" name="is_expanse_clear" <?php if(isset($salary_slip_data['expense_amount']) && $salary_slip_data['expense_amount'] >0){ echo "checked"; } ?> id="inlineRadio1" value="1">
                           <label class="form-check-label" for="inlineRadio1">Yes</label>
                        </div>
                        
                     </div>
                     <?php } else if($salary_slip_data['expense_amount']>0){ ?>
                        <input type="hidden" class="alreadyPaidExpense" name="is_expanse_clear" value="1">
                     <?php } ?>
                  </div>
              <?php } ?>
               </div>

               <?php if($advanceAmount>0){ ?>
               <div class="col-md-12">
                  <div class="form-group ">
                     <?php if( $sId>0 && $salary_slip_data['advance_salary_paid_amount']>0) { 
                        $labelNameAdvaceSalary = "Advance Salary <span class='text-danger'> $advanceAmount</span> has been deducted for this month ";
                        $newAdvanceSalary = "0";
                     } else {
                        $labelNameAdvaceSalary = "Deduct Advance Salary <span class='text-danger'> $advanceAmount</span> for this month";
                        $newAdvanceSalary = "1";
                     } ?>
                     <input type="hidden" name="newAdvanceSalary" value="<?php echo $newAdvanceSalary; ?>">
                     <label for="input-10" class="col-sm-12 col-form-label"><?php echo $labelNameAdvaceSalary;?></label>
                     <?php if($salary_slip_data['advance_salary_paid_amount']==0 ) {  ?>
                     <div class="col-lg-12 col-md-12" id="">
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_advance_clear is_advance_clear1" type="radio" name="is_advance_clear"  <?php if(isset($salary_slip_data['advance_salary_paid_amount']) && $salary_slip_data['advance_salary_paid_amount'] >0){ echo ""; } else { echo "checked";  }?>  id="inlineRadio2" value="0">
                           <label class="form-check-label" for="inlineRadio2">No</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_advance_clear is_advance_clear2" type="radio" name="is_advance_clear" <?php if(isset($salary_slip_data['advance_salary_paid_amount']) && $salary_slip_data['advance_salary_paid_amount'] >0){ echo "checked"; } ?> id="inlineRadio1" value="1">
                           <label class="form-check-label" for="inlineRadio1">Yes</label>
                        </div>
                     </div>
                     <?php } else if($salary_slip_data['advance_salary_paid_amount']>0){ ?>
                        <input type="hidden" class="alreadyPaidAdvaceSalary" name="is_advance_clear" value="1">
                     <?php } ?>
                  </div>
               </div>
              <?php } ?>

              <?php if($emi_deduction>0){ ?>
               <div class="col-md-12">
                  <div class="form-group ">
                     <?php if( $sId>0 && $salary_slip_data['emi_deduction']>0) { 
                        $labelNameLoanEmi = "Loan E.M.I <span class='text-danger'> $emi_deduction</span> has been deducted for this month ";
                        $newEmiClear = "0";
                     } else {
                        $labelNameLoanEmi = "Deduct Loan E.M.I <span class='text-danger'> $emi_deduction</span> for this month";
                        $newEmiClear = "1";
                     } ?>
                     <input type="hidden" name="newEmiClear" value="<?php echo $newEmiClear; ?>">
                     <label for="input-10" class="col-sm-12 col-form-label"><?php echo $labelNameLoanEmi;?></label>
                     <?php if($salary_slip_data['emi_deduction']==0) {  ?>
                     <div class="col-lg-12 col-md-12" id="">
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_loan_clear is_loan_clear1" type="radio" name="loan_emi"  <?php if(isset($salary_slip_data['emi_deduction']) && $salary_slip_data['emi_deduction'] >0){ echo ""; } else { echo "checked";  }?>  id="LoaninlineRadio2" value="0">
                           <label class="form-check-label" for="LoaninlineRadio2">No</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input is_loan_clear is_loan_clear2" type="radio" name="loan_emi" <?php if(isset($salary_slip_data['emi_deduction']) && $salary_slip_data['emi_deduction'] >0){ echo "checked"; } ?> id="LoaninlineRadio1" value="1">
                           <label class="form-check-label" for="LoaninlineRadio1">Yes</label>
                        </div>
                     </div>
                     <?php } else if($salary_slip_data['emi_deduction']>0){ ?>
                        <input type="hidden" class="alreadyPaidEmi" name="loan_emi" value="1">
                     <?php } ?>
                  </div>
               </div>
              <?php } ?>

               <div class="col-md-4 offset-4 text-center">
                  <div class="row container-fluid ">
                     <label for="input-10" class="col-sm-12 col-form-label"> This Month Net Salary <span class="required">*</span></label>
                     <div class="col-lg-12 col-md-12" id="">
                           <input type="text" class="form-control employee_net_salary"  readonly id="employee_net_salary0" name="employee_net_salary0" value="<?php if($salary_slip_data['total_net_salary']>0){ echo $salary_slip_data['total_net_salary']; } else { echo round($final_emp_salary+$extrDaysSalary); }  ?>" >
                           <input type="hidden" class="form-control "  readonly id="employee_net_salary" name="employee_net_salary" value="<?php  if($salary_slip_data['total_net_salary']>0){ echo $salary_slip_data['total_net_salary']; } else {  echo $final_emp_salary+$extrDaysSalary; }  ?>" >
                           
                     </div>
                  </div>
               </div>
            </div>   
         </div>
           <div class="form-footer text-center">
           <input type="hidden" name="total_month_days" id="total_month_days"  value="<?php echo $monthDays; ?>">
           
           <input type="hidden" name="floor_id_old" id="floor_id_old"  value="<?php if(isset($_GET['floor_id']) && $_GET['floor_id'] !=""){ echo $_GET['floor_id']; }?>">
               <input type="hidden" name="salary_month" id="salary_month"  value="<?php if(isset($_GET['month']) && $_GET['month'] !=""){ echo $_GET['month']; }?>">
               <input type="hidden" name="salary_year" id="salary_year"  value="<?php if(isset($_GET['year']) && $_GET['year'] !=""){ echo $_GET['year']; }?>">
               <input type="hidden" name="block_id_old" id="block_id_old"  value="<?php if(isset($_GET['block_id']) && $_GET['block_id'] !=""){ echo $_GET['block_id']; }?>">
               <input type="hidden" name="user_id_old" id="user_id_old"  value="<?php if(isset($user_id) && $user_id !=""){ echo $user_id; }?>">
               
             <input type="hidden" id="salary_id" name="salary_id" value="<?php echo $salaryData['salary_id']; ?>">
             <?php if(isset($sId) && $sId>0 ) {?>
               <input type="hidden" id="editSalarySlip" name="editSalarySlip"  value="editSalarySlip" >
               <input type="hidden" id="salary_slip_id" name="salary_slip_id" value="<?php echo $sId; ?>">
             <?php } else { ?>
               <input type="hidden" id="addsalarySlip" name="addsalarySlip"  value="addsalarySlip" >
             <?php }  ?>
             <input type="hidden" id="society_id" name="society_id" value="<?php echo $society_id; ?>"  >
             <?php if($totalPresent>0) { ?>
               <a href="Salaryinvoice"> 
                  <button <?php if($final_emp_salary<=0){ echo "disabled"; }?> id="redirectToInvoice" type="submit"  class="btn btn-success mb-5"><i class="fa fa-check-square-o"></i><?php if(isset($sId) && $sId>0 ) { echo "Update"; } else {  echo "Genarate";  }?>  </button><a>
            <?php } else {
               echo "<h4 class='text-danger'>Attendance Not Found on selected month</h4>";
            } ?>
           </div>
         </form>
         <?php } ?>
            </div>
          </div>
        </div>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
 
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/custom17.js"></script>

    <script type="text/javascript">
     //  $(".year").on('change', function(event){
         function yearChange(yearValue){
        // alert("ldfj");
         // yearValue = $(this).val();
          crntYear = '<?php echo date('Y');?>';
          crntM = '<?php echo date('m');?>';
          monthHtml = "";
         var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         var d = new Date();
         var monthName=months[d.getMonth()];
         console.log(monthName)
         if(crntYear==yearValue)
         {
            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               if(index<crntM)
               {
                  monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
               }
            }
            $('#month').html(monthHtml);
         }
         else
         {
            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
            }
            $('#month').html(monthHtml);
         }
      };
<?php 
if (isset($salaryData['salary_id']) && $salaryData['salary_id'] != "") { ?>
   var floor_id = $('#floor_id').val();
   getUserForSalary(floor_id);
<?php } ?>
<?php if((isset($salary_slip_data['expense_amount']) && $salary_slip_data['expense_amount'] >0)){ ?>
   $('#total_earning_salary').val(<?php echo $totalEarnWithExpense; ?>);
   $('#employee_net_salary').val(<?php echo $salary_slip_data['total_net_salary']; ?>);
   $('.employee_net_salary').val(<?php echo $salary_slip_data['total_net_salary']; ?>);
   <?php } ?>
<?php 
if (isset($_GET['block_id']) && $_GET['block_id'] != "") {?>
   var block_id = $('#block_id').val();
   // getFloorByBlockIdAddSalary(block_id);

<?php }?>
<?php 
if (isset($_GET['floor_id']) && $_GET['floor_id'] != "") {?>
   var floor_id = '<?php echo $_GET['floor_id']; ?>';
   getUserForSalarySlip(floor_id);

<?php }?>
function removePhoto2() {
   $('#facility_photo_2').remove();
   $('#penalty_photo_old_2').val('');
}
function removePhoto3() {
   $('#facility_photo_3').remove();
   $('#penalty_photo_old_3').val('');
}

$('.pfClass').keyup(function(event){

});
emp_sal = '<?php echo $final_emp_salary; ?>';
function otherEarncalculation(val)
{
  
   total_earning_salary = '<?php echo $totalEarn;?>';
   var radioValue = $("input[name='is_expanse_clear']:checked").val();
   var alreadyPaidExpense = $(".alreadyPaidExpense").val();

      if(radioValue==1){
         expAmnt = <?php echo $expanceAmnt; ?>;
      }else if(alreadyPaidExpense==1){
         expAmnt = <?php echo $expanceAmnt; ?>;
      } else {
         expAmnt = 0;
      }
   if(val !="")
   {
      console.log(expAmnt);
      $('#total_earning_salary').val(parseFloat(total_earning_salary)+parseFloat(val));
   }else
   {
      $('#total_earning_salary').val(parseFloat(total_earning_salary));
   }
   cal();
}

function otherDeductcalculation(val)
{
   total_deduction_salary = '<?php echo array_sum($totalDeduct);?>';
   
   if(val !="")
   {
      advanceAmount = '<?php echo $advanceAmount; ?>';
      emiAmount = '<?php echo $emi_deduction; ?>';
      advanceAmountO = 0;
      advanceAmount1 = 0;
      var radioValue = $("input[name='is_advance_clear']:checked").val();
      var alreadyPaidExpense = $(".alreadyPaidAdvaceSalary").val();
      
      var radioValueEmi = $("input[name='loan_emi']:checked").val();
      var alreadyPaidEmi = $(".alreadyPaidEmi").val();

      if(radioValue==1){
         advanceAmountO = advanceAmount;
      }else if(alreadyPaidExpense==1){
         advanceAmountO = '<?php echo $advanceAmount; ?>;'
      } else
      {
         advanceAmountO = 0;
      }

      if(radioValueEmi==1){
         advanceAmount1 = emiAmount;
      }else if(alreadyPaidEmi==1){
         advanceAmount1 = '<?php echo $emiAmount; ?>;'
      } else
      {
         advanceAmount1 = 0;
      }

      total_deduction_salary = parseFloat(total_deduction_salary)+parseFloat(val);
      total_deduction_salaryNew = parseFloat(total_deduction_salary)+parseFloat(advanceAmountO);
      total_deduction_salaryNew1 = parseFloat(total_deduction_salaryNew)+parseFloat(advanceAmount1);

      $('#total_deduction_salary').val(total_deduction_salaryNew1);
   }else
   {  
      $('#total_deduction_salary').val(parseFloat(total_deduction_salary));
   }
   cal();
   
}
function cal()
{  

   var radioValue = $("input[name='is_expanse_clear']:checked").val();
   var alreadyPaidExpense = $(".alreadyPaidExpense").val();

   if(radioValue==1){
      expAmnt = <?php echo $expanceAmnt; ?>;
   }else if(alreadyPaidExpense==1){
      expAmnt = <?php echo $expanceAmnt; ?>;
   } else {
      expAmnt = 0;
   }
  
   total_earning_salary =   $('#total_earning_salary').val();
   total_deduction_salary =  $('#total_deduction_salary').val();
   overtime_amount =  $('#overtime_amount').val();
   total_earning_salaryTemp = parseFloat(total_earning_salary)+parseFloat(expAmnt);
   console.log(total_deduction_salary);
   var contribution_amount = $('#totalContribution').val();
   if(contribution_amount ==undefined){
      contribution_amount = 0;
   }
   if(overtime_amount ==undefined){
      overtime_amount = 0;
   }
   tAmount =  parseFloat(total_earning_salaryTemp)-parseFloat(total_deduction_salary)+parseFloat(overtime_amount)
   roundTAmount = Math.round(parseFloat(total_earning_salaryTemp)-parseFloat(total_deduction_salary)+parseFloat(overtime_amount));
      if (roundTAmount<1) {
         $("#redirectToInvoice").attr("disabled", true);
      } else {
         $("#redirectToInvoice").removeAttr("disabled");
      }
   $('#employee_net_salary').val(tAmount);
   $('.employee_net_salary').val(roundTAmount);
}

function submitForGetDetails(){
   mnth = $('#month').val();
   if(mnth !=""){
      $( "#salaryBasicAdd" ).submit();
   }else
   {
      swal('Please Select Month');
   }
}

//$(document).ready(function(){
      $('.is_expanse_clear').change(function(){
         advanceAmount = '<?php echo $advanceAmount; ?>';
         other_earning = $('#other_earning').val();
         other_deduction = $('#other_deduction').val();
        

         if(other_earning !=""){
            other_earning = other_earning;
         }else
         {
            other_earning = 0;
         }
        
         other_earning  = parseFloat(other_earning);
         advanceAmountO = 0;
         var radioValue = $("input[name='is_advance_clear']:checked").val();
         if(radioValue==1){
            advanceAmountO = advanceAmount;
         }else
         {
            advanceAmountOMes="";
            advanceAmountO = 0;
         }


         if(this.value==1){
            totalEarnWithExpense = '<?php echo $totalEarnWithExpense; ?>';
            totalEarnWithExpense = totalEarnWithExpense;
            final_amn_with_Expense = <?php echo $final_amn_with_Expense; ?>;
            final_amn_with_Expense_withround = <?php echo round($final_amn_with_Expense); ?>;
            final_amn_with_Expense_withround = final_amn_with_Expense_withround-advanceAmountO+other_earning;
            final_amn_with_Expense = final_amn_with_Expense-advanceAmountO+other_earning;
            console.log(final_amn_with_Expense);
              $('#expense_amount').val('<?php echo $expanceAmnt; ?>');
              $('#total_earning_salary').val(totalEarnWithExpense);
              $('#employee_net_salary').val(final_amn_with_Expense-advanceAmountO);
              $('.employee_net_salary').val(Math.round(final_amn_with_Expense_withround));

           } else {
            
            final_emp_salaryy = <?php echo $final_emp_salary; ?>;

            var  totalEarn = <?php echo $totalEarn; ?>;
            totalEarn = totalEarn+other_earning;
            final_emp_salaryy = other_earning+final_emp_salaryy;
            final_emp_salaryy_wth_r = <?php echo round($final_emp_salary); ?>;
            advanceAmountOTemp = advanceAmountO+other_deduction;
            final_emp_salaryy_wth_r = final_emp_salaryy_wth_r-advanceAmountOTemp+other_earning;
            
            console.log(other_deduction);
            
            $('#expense_amount').val(0);
            $('#total_earning_salary').val(totalEarn);
            $('#employee_net_salary').val(final_emp_salaryy-advanceAmountOTemp);
            $('.employee_net_salary').val(Math.round(final_emp_salaryy_wth_r));
           }
           
           cal();
        });

   function changeStatusAction(value) {
       if (value == 2) {
         $('.sharWUser').removeClass('d-none');
       } else {
         $('.sharWUser').addClass('d-none');
       }

   }

  $('.is_advance_clear').change(function(){
    if(this.value==1){

      $('.advAmount').removeClass('d-none');
      employee_net_salary = $('#employee_net_salary').val();
      total_deduction_salary = $('#total_deduction_salary').val();
      advanceAmount = '<?php echo $advanceAmount; ?>';
      advacneDuductSalary = parseFloat(employee_net_salary)-parseFloat(advanceAmount);
      $('#advance_salary_paid_amount').val(advanceAmount);
      if(employee_net_salary>parseFloat(advanceAmount)){
         var radioValue = $("input[name='is_advance_clear']:checked").val();
         
         salary_mode_description = $('textarea#salary_mode_description').val();
         
         $('#employee_net_salary').val(advacneDuductSalary);
         $('.employee_net_salary').val(advacneDuductSalary);
         $('#total_deduction_salary').val((parseFloat(total_deduction_salary))+parseFloat(advanceAmount));
      }else
      {
         $(".is_advance_clear1").prop("checked", true);
         $("textarea#salary_mode_description").val('');
         swal("Advance Salary Greater than Net Salary");
         $('#advance_salary_paid_amount').val(0);
      }
     
    }else
    {
      var radioValue = $("input[name='is_advance_clear']:checked").val();
      
      advanceAmount = '<?php echo $advanceAmount; ?>';
      employee_net_salary = $('#employee_net_salary').val();
      total_deduction_salary = $('#total_deduction_salary').val();
      advacneDuductSalary = parseFloat(employee_net_salary)+parseFloat(advanceAmount);

      advanceAmount = '<?php echo $advanceAmount; ?>';
      $('#employee_net_salary').val(advacneDuductSalary);
      $('.employee_net_salary').val(advacneDuductSalary);
      $('#total_deduction_salary').val((parseFloat(total_deduction_salary))-parseFloat(advanceAmount));
      $('.advAmount').addClass('d-none');
    }
  });

  $('.is_loan_clear').change(function(){
    if(this.value==1){

      $('.emiAmount').removeClass('d-none');
      employee_net_salary = $('#employee_net_salary').val();
      total_deduction_salary = $('#total_deduction_salary').val();
      emiAmount = '<?php echo $emi_deduction; ?>';
      $('#emi_deduction').val(emiAmount);
      emiDeductSalary = parseFloat(employee_net_salary)-parseFloat(emiAmount);
      // console.log(emiAmount);
      if(employee_net_salary>parseFloat(emiAmount)){
         var radioValue = $("input[name='is_loan_clear']:checked").val();
         
         
         $('#employee_net_salary').val(emiDeductSalary);
         $('.employee_net_salary').val(emiDeductSalary);
         $('#total_deduction_salary').val((parseFloat(total_deduction_salary))+parseFloat(emiAmount));
      }else
      {
         $(".is_loan_clear1").prop("checked", true);
         swal("Loan E.M.I Greater than Net Salary");
         $('#advance_salary_paid_amount').val(0);
      }
     
    }else
    {
      var radioValue = $("input[name='is_loan_clear']:checked").val();
      
      emiAmount = '<?php echo $emi_deduction; ?>';
      employee_net_salary = $('#employee_net_salary').val();
      total_deduction_salary = $('#total_deduction_salary').val();
      emiDeductSalary = parseFloat(employee_net_salary)+parseFloat(emiAmount);

      emiAmount = '<?php echo $emi_deduction; ?>';
      $('#employee_net_salary').val(emiDeductSalary);
      $('.employee_net_salary').val(emiDeductSalary);
      $('#total_deduction_salary').val((parseFloat(total_deduction_salary))-parseFloat(emiAmount));
      $('.emiAmount').addClass('d-none');
    }
  });
</script>
<?php

function shiftHoursCalculate($d,$shift_id)
{
   # code...
}

?>