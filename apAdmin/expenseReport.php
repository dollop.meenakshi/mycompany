<?php
error_reporting(0);
if (isset($_GET['from']) && $_GET['balancesheet_id']!='All') {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1 || filter_var($_GET['balancesheet_id'], FILTER_VALIDATE_INT) != true){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='expenseReport';
        </script>");
  }
} else if(isset($_GET['from']) && $_GET['balancesheet_id']=='All'){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='expenseReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Expense  Report</h4>
        </div>

     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
            <div class="col-lg-2 col-6">
            <div class="input-group mb-2">
              <input placeholder="From Date" type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>
              <!-- <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>  -->
            </div>
          
            </div>
            <div class="col-lg-2 col-6">
            <div class="input-group mb-2">
              <input placeholder="To Date"  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>
              <!-- <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>  -->
            </div>
            </div>
          <div class="col-lg-4 col-6">
             <select type="text" required="" class="form-control" name="balancesheet_id">
                <option value="All">All BalanceSheet</option>
                 <?php
                   error_reporting(0);
                  $q=$d->select("balancesheet_master","society_id='$society_id'","");
                   while ($row11=mysqli_fetch_array($q)) {
                 ?>
                  <option <?php if($_GET['balancesheet_id']==$row11['balancesheet_id']) { echo 'selected';} ?> value="<?php echo $row11['balancesheet_id'];?>"><?php echo $row11['balancesheet_name'];?></option>
                  <?php }?>
            </select>
          </div>

          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input  class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
         

     </div>
    </form>
    <!-- End Breadcrumb-->


      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">

              <div class="table-responsive">
              <?php
                extract(array_map("test_input" , $_GET));

                $date=date_create($from);
                    $dateTo=date_create($toDate);
                    $from= date_format($date,"Y-m-d 00:00:00");
                    $toDate= date_format($dateTo,"Y-m-d 23:59:59");

                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                  if ($_GET['balancesheet_id']=="All") {
                       $q=$d->select("balancesheet_master,expenses_balance_sheet_master","balancesheet_master.balancesheet_id =expenses_balance_sheet_master.balancesheet_id  AND expenses_balance_sheet_master.society_id='$society_id' AND  expenses_balance_sheet_master.expenses_add_date BETWEEN '$from' AND '$toDate' AND expenses_balance_sheet_master.expenses_amount!=0");
                    } else {
                       $q=$d->select("balancesheet_master,expenses_balance_sheet_master","balancesheet_master.balancesheet_id =expenses_balance_sheet_master.balancesheet_id  AND balancesheet_master.society_id='$society_id' AND balancesheet_master.balancesheet_id ='$_GET[balancesheet_id]' AND  expenses_balance_sheet_master.expenses_add_date BETWEEN '$from' AND '$toDate' AND expenses_balance_sheet_master.expenses_amount!=0");
                  }

                  $i=1;
                if (isset($_GET['from'])) {

               ?>
              <div class="row">
                <?php if ($_GET['balancesheet_id']=="All") { ?>
                <div class="col-md-3 ml-3">
                  Total Expense Amount:  <?php

                      $count522=$d->sum_data("expenses_amount","expenses_balance_sheet_master","expenses_add_date BETWEEN '$from' AND '$toDate'");
                      while($row11=mysqli_fetch_array($count522))
                     {
                          $asifee=$row11['SUM(expenses_amount)'];
                     echo   $totalMain=number_format($asifee,2,'.','');

                      }
                   ?>
                </div>

                <?php } else { ?>
                  <div class="col-md-3 ml-3">
                  Total Expense Amount:  <?php
                      $count545=$d->sum_data("expenses_amount","expenses_balance_sheet_master","balancesheet_id ='$_GET[balancesheet_id]' AND expenses_add_date BETWEEN '$from' AND '$toDate'");
                      while($row114=mysqli_fetch_array($count545))
                     {
                          $asif65=$row114['SUM(expenses_amount)'];
                     echo   $totalMainE=number_format($asif65,2,'.','');

                      }
                   ?>
                </div>

                <?php } ?>
              </div>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Expense</th>
                        <th> Date</th>
                        <th> Amount</th>
                        <th>Balancesheet</th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                  <?php


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>

                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['expenses_title']; ?></td>
                        <td><?php if($data['expenses_add_date']!="") { echo date('Y-m-d h:i A', strtotime($data['expenses_add_date'])); } ?></td>
                        <td><?php echo $data['expenses_amount']; ?></td>
                        <td><?php echo $data['balancesheet_name']; ?></td>
                        <td><?php  if($data['expense_category_id']!=0) {
                               $q11=$d->select("expense_category_master","active_status=0 AND expense_category_id='$data[expense_category_id]'","");
                               $row=mysqli_fetch_array($q11);
                               echo $row['expense_category_name'];
                            } ?></td>

                    </tr>
                  <?php } ?>
                </tbody>

            </table>


            <?php } else {  ?>

                 <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->