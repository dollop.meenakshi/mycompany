<?php 
 include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  // add main menu
  $cTime= date("H:i:s");
  $today  = date("Y-m-d");

  if (isset($addParticipatePlayer)) {
      $qh = $d->select("housie_room_master","room_id = '$room_id' AND housie_type=1");
      if (mysqli_num_rows($qh)==0) {
        $_SESSION['msg1']="Invalid Game";
        header("location:../manageHousie");
        exit();
      }
      $data = mysqli_fetch_array($qh);

     
      $m->set_data('room_id',$room_id);
      $m->set_data('user_id',$user_id);
      $m->set_data('society_id',$society_id);
      $m->set_data('join_time',date("Y-m-d H:i:s"));

      $a1 =array(
        'society_id'=> $m->get_data('society_id'),
        'room_id'=> $m->get_data('room_id'),
        'user_id'=> $m->get_data('user_id'),
        'join_time'=> $m->get_data('join_time'),
        
      );

        
      $qc=$d->select("housie_participate_player","user_id='$user_id' AND room_id='$room_id'");
      if (mysqli_num_rows($qc)>0) {
        $_SESSION['msg1']="Already Added";
        header("location:../addHousieGame?id=$room_id&managePlayer=yes");
        exit();
      }

      $q=$d->insert("housie_participate_player",$a1);

       if($q>0) {
            $game_date = date('d M Y h:i A',strtotime($data[game_date].' '.$data[game_time]));

            $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND user_id='$user_id'");
            $data_notification=mysqli_fetch_array($qUserToken);
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            $title = "Housie Invitation";
            $description = "You are invited to play Housie on $game_date by Admin ($created_by)";
            if ($device=='android') {
                $nResident->noti("HousieJoinGame","",$society_id,$sos_user_token,$title,$description,'housie');
            }  else if($device=='ios') {
               $nResident->noti_ios("HousieJoinGame","",$society_id,$sos_user_token,$title,$description,'housie');
            }

            $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$user_id,
              'notification_title'=>$title,
              'notification_desc'=>$description,    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'housie',
              'notification_logo'=>'housieNew.png',
            );
            $d->insert("user_notification",$notiAry);
      
        $_SESSION['msg']="Game Player Added For Housie";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Game Player Added For Housie");
        header("location:../addHousieGame?id=$room_id&managePlayer=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../addHousieGame?id=$room_id&managePlayer=yes");
      }

  }

  if (isset($deleteParticipatetPlayer)) {

      $q=$d->delete("housie_participate_player","room_id='$room_id' AND user_id='$user_id'");

       if($q>0) {

        $_SESSION['msg']="Game Player Deleted";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Game Player Deleted");
        header("location:../addHousieGame?id=$room_id&managePlayer=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../addHousieGame?id=$room_id&managePlayer=yes");
      }

  }
  if (isset($room_id_notification)) {

      $title= "Housie Hosted On $fulldateTime";
      $description= "Hosted By Admin $created_by";

      $qh = $d->select("housie_room_master","room_id = '$room_id_notification' ");
      $data = mysqli_fetch_array($qh);

      if ($data['housie_type']==1) {
          $unitALreadyBlock=array();
          $qjoin=$d->select("users_master,housie_participate_player,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_participate_player.user_id=users_master.user_id AND housie_participate_player.room_id='$room_id_notification'","");
           while($row=mysqli_fetch_array($qjoin)){ 
              array_push($unitALreadyBlock, $row['user_id']);
            }
           $ids = join("','",$unitALreadyBlock); 
           $appendQuery = " users_master.user_id IN ('$ids')";
           $appendQuery11 = " AND users_master.user_id IN ('$ids')";
          
          $d->insertUserNotification($society_id,$title,$description,"housie","housieNew.png","$appendQuery");
          $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $appendQuery11");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $appendQuery11");
         $nResident->noti("HousieJoinGame","",$society_id,$fcmArray,$title,$description,'housie');
         $nResident->noti_ios("HousieJoinGame","",$society_id,$fcmArrayIos,$title,$description,'housie');

      } else {


         $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'");
         $nResident->noti("HousieJoinGame","",$society_id,$fcmArray,$title,$description,'housie');
         $nResident->noti_ios("HousieJoinGame","",$society_id,$fcmArrayIos,$title,$description,'housie');
      } 

        $_SESSION['msg']="Game Notification Send";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Housie Game Notification Send ");
        header("location:../manageHousie");

  }

  if(isset($_POST['addHousieGame'])){
    $expire = strtotime($_POST['game_date'].' '.$_POST['game_time']);
    $today = strtotime("$today $cTime");
    if($today >  $expire) {
      $_SESSION['msg1']="Time is expire select another time !";
      header("location:../addHousieGame");
      exit();
    }
    $game_time= date('H:i:s',strtotime($game_time));
    $fulldateTime=  date('d M Y, h:i A',strtotime($game_date.' '.$game_time));

    $qt=$d->select("housie_room_master","game_date='$game_date' AND game_time='$game_time' AND society_id='$society_id'");
    if (mysqli_num_rows($qt)>o) {
      $_SESSION['msg1']="Game Already Added On Same Date Time !";
      header("location:../addHousieGame");
      exit();
    }



      $levelCount= count($_POST['master_rule_id']);
      if ($levelCount<1) {
        $_SESSION['msg1']="Minimum 1 Rules Required";
        header("location:../addHousieGame");
        exit();
      }

      $file = $_FILES['sponser_photo']['tmp_name'];
      if(file_exists($file)) {

        $extensionResume=array("png","jpg","jpeg","PNG","JPG","JPEG");
        $extId = pathinfo($_FILES['sponser_photo']['name'], PATHINFO_EXTENSION);

         $errors     = array();
          $maxsize    = 2097152;
          
          if(($_FILES['sponser_photo']['size'] >= $maxsize) || ($_FILES["sponser_photo"]["size"] == 0)) {
              $_SESSION['msg1']="Sponser Iamge. Must be less than 2 MB.";
              header("location:../kbg");
              exit();
          }
          if(!in_array($extId, $extensionResume) && (!empty($_FILES["sponser_photo"]["type"]))) {
               $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
              header("location:../kbg");
              exit();
          }
         if(count($errors) === 0) {
          $image_Arr = $_FILES['sponser_photo'];   
          $temp = explode(".", $_FILES["sponser_photo"]["name"]);
          $sponser_photo = $kbg_game_sponsor_name.'sponser_id_'.round(microtime(true)) . '.' . end($temp);
          move_uploaded_file($_FILES["sponser_photo"]["tmp_name"], "../../img/game/".$sponser_photo);
        } 
      } else{
        $sponser_photo="";
      }

      $que_interval11 = $que_interval*2;

      $m->set_data('society_id',$society_id);
      $m->set_data('game_name',$game_name);
      $m->set_data('que_id',$que_id);
      $m->set_data('game_date',$game_date);
      $m->set_data('game_time',$game_time);
      $m->set_data('sponser_name',$sponser_name);
      $m->set_data('sponser_photo',$sponser_photo);
      $m->set_data('sponser_url',$sponser_url);
      $m->set_data('que_interval',$que_interval11);
      $m->set_data('housie_type',$housie_type);

      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'game_name'=> $m->get_data('game_name'),
        'que_id'=> $m->get_data('que_id'),
        'game_date'=> $m->get_data('game_date'),
        'game_time'=> $m->get_data('game_time'),
        'sponser_name'=> $m->get_data('sponser_name'),
        'sponser_photo'=> $m->get_data('sponser_photo'),
        'que_interval'=> $m->get_data('que_interval'),
        'housie_type'=> $m->get_data('housie_type'),
      );
     
      $q=$d->insert("housie_room_master",$a);
      $room_id = $con->insert_id;
      $m->set_data('room_id',$room_id);
      if($q>0) {
            $allowUsers=array_filter($_POST['allow_users']);
            $rulePoints=array_filter($_POST['rule_points']);
            $allowUsers = implode("~", $allowUsers);
            $allowUsers = explode("~", (string)$allowUsers);
            $rulePoints = implode("~", $rulePoints);
            $rulePoints = explode("~", (string)$rulePoints);

         

              for ($i=0; $i <$levelCount ; $i++) { 
                  $m->set_data('master_rule_id',$_POST['master_rule_id'][$i]);
                  $m->set_data('room_no',$room_no);
                  $m->set_data('allow_users',$allowUsers[$i]);
                  $m->set_data('rulePoints',$rulePoints[$i]);

               $a3= array (
                'master_rule_id'=>$m->get_data('master_rule_id'),
                'society_id'=>$m->get_data('society_id'),
                'room_id'=>$m->get_data('room_id'),
                'allow_users_claim'=> $m->get_data('allow_users'),
                'total_claim_player'=> $m->get_data('allow_users'),
                'rule_points'=> $m->get_data('rulePoints'),
                );


              $cheRule=$d->select("housie_claim_rules_master","master_rule_id='$master_rule_id[$i]' AND room_id='$room_id'");

              $cheData=mysqli_fetch_array($cheRule);
              if($cheData>0){
                $q2=$d->update("housie_claim_rules_master",$a3,"master_rule_id='$master_rule_id[$i]' AND room_id='$room_id'");
              } else {
                $q2=$d->insert("housie_claim_rules_master",$a3);
              }
            }

         //    $title= "Housie Hosted On $fulldateTime";
         //    $description= "Hosted By Admin $created_by";
         //  $d->insertUserNotification($society_id,$title,$description,"housie","housieNew.png","");



         // $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");
         // $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
         // $nResident->noti("HosieInfoPageActivity","",$society_id,$fcmArray,$title,$description,'housie');
         // $nResident->noti_ios("HousieVC","",$society_id,$fcmArrayIos,$title,$description,'housie');

          
        $_SESSION['msg']="Game Added Please Add Question Now";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Housie Game Added ");
        header("location:../addHousieGame?id=$room_id&que=$que_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../addHousieGame");
      }
  }

   if(isset($_POST['rescheduleGame'])){

      $expire = strtotime($_POST['game_date'].' '.$_POST['game_time']);
      $today = strtotime("$today $cTime");
      if($today >  $expire) {
        $_SESSION['msg1']="Time is expire select another time !";
        header("location:../manageHousie");
        exit();
      }
       $game_time= date('H:i:s',strtotime($game_time));

      $fulldateTime=  date('d M Y, h:i A',strtotime($game_date.' '.$game_time));


       $qt=$d->select("housie_room_master","game_date='$game_date' AND game_time='$game_time' AND society_id='$society_id'");
        if (mysqli_num_rows($qt)>o) {
          $_SESSION['msg1']="Game Already Added On Same Date Time !";
          header("location:../manageHousie");
          exit();
        }

      $levelCount= count($_POST['master_rule_id']);
      if ($levelCount<1) {
        $_SESSION['msg1']="Minimum 1 Rules Required";
        header("location:../manageHousie");
        exit();
      }

      $file = $_FILES['sponser_photo']['tmp_name'];
      if(file_exists($file)) {

        $extensionResume=array("png","jpg","jpeg","PNG","JPG","JPEG");
        $extId = pathinfo($_FILES['sponser_photo']['name'], PATHINFO_EXTENSION);

         $errors     = array();
          $maxsize    = 2097152;
          
          if(($_FILES['sponser_photo']['size'] >= $maxsize) || ($_FILES["sponser_photo"]["size"] == 0)) {
              $_SESSION['msg1']="Sponser Iamge. Must be less than 2 MB.";
              header("location:../manageHousie");
              exit();
          }
          if(!in_array($extId, $extensionResume) && (!empty($_FILES["sponser_photo"]["type"]))) {
               $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
              header("location:../manageHousie");
              exit();
          }
         if(count($errors) === 0) {
          $image_Arr = $_FILES['sponser_photo'];   
          $temp = explode(".", $_FILES["sponser_photo"]["name"]);
          $sponser_photo = $kbg_game_sponsor_name.'sponser_id_'.round(microtime(true)) . '.' . end($temp);
          move_uploaded_file($_FILES["sponser_photo"]["tmp_name"], "../../img/game/".$sponser_photo);
        } 
      } else{
        $sponser_photo=$sponser_photo_old;
      }

     
      $que_interval11 = $que_interval*2;


      $m->set_data('society_id',$society_id);
      $m->set_data('game_name',$game_name);
      $m->set_data('que_id',$que_id);
      $m->set_data('game_date',$game_date);
      $m->set_data('game_time',$game_time);
      $m->set_data('sponser_name',$sponser_name);
      $m->set_data('sponser_photo',$sponser_photo);
      $m->set_data('sponser_url',$sponser_url);
      $m->set_data('que_interval',$que_interval11);
      $m->set_data('housie_type',$housie_type);

      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'game_name'=> $m->get_data('game_name'),
        'que_id'=> $m->get_data('que_id'),
        'game_date'=> $m->get_data('game_date'),
        'game_time'=> $m->get_data('game_time'),
        'sponser_photo'=> $m->get_data('sponser_photo'),
        'sponser_name'=> $m->get_data('sponser_name'),
        'sponser_url'=> $m->get_data('sponser_url'),
        'que_interval'=> $m->get_data('que_interval'),
        'housie_type'=> $m->get_data('housie_type'),
      );
      $q=$d->insert("housie_room_master",$a);
      $room_id = $con->insert_id;
      $m->set_data('room_id',$room_id);

    
      if($q>0) {
            $allowUsers=array_filter($_POST['allow_users']);
            $rulePoints=array_filter($_POST['rule_points']);
            $allowUsers = implode("~", $allowUsers);
            $allowUsers = explode("~", (string)$allowUsers);
            $rulePoints = implode("~", $rulePoints);
            $rulePoints = explode("~", (string)$rulePoints);

         

              for ($i=0; $i <$levelCount ; $i++) { 
                  $m->set_data('master_rule_id',$_POST['master_rule_id'][$i]);
                  $m->set_data('room_no',$room_no);
                  $m->set_data('allow_users',$allowUsers[$i]);
                  $m->set_data('rulePoints',$rulePoints[$i]);

               $a3= array (
                'master_rule_id'=>$m->get_data('master_rule_id'),
                'society_id'=>$m->get_data('society_id'),
                'room_id'=>$m->get_data('room_id'),
                'allow_users_claim'=> $m->get_data('allow_users'),
                'total_claim_player'=> $m->get_data('allow_users'),
                'rule_points'=> $m->get_data('rulePoints'),
                );


              $cheRule=$d->select("housie_claim_rules_master","master_rule_id='$master_rule_id[$i]' AND room_id='$room_id'");

              $cheData=mysqli_fetch_array($cheRule);
              if($cheData>0){
                $q2=$d->update("housie_claim_rules_master",$a3,"master_rule_id='$master_rule_id[$i]' AND room_id='$room_id'");
              } else {
                $q2=$d->insert("housie_claim_rules_master",$a3);
              }
            }

        
      for ($i11=0; $i11<count($_POST['question_name']); $i11++)  {


        $m->set_data('society_id',$society_id);
        $m->set_data('question_name',$_POST['question_name'][$i11]);
        $m->set_data('answer',$_POST['answer'][$i11]);
        $m->set_data('room_id',$room_id);

        $a1= array (
                    'society_id'=> $m->get_data('society_id'),
                    'question'=> $m->get_data('question_name'),
                    'answer'=> $m->get_data('answer'),
                    'room_id'=> $m->get_data('room_id'),
            );

           $q=$d->insert('housie_questions_master',$a1); 
      }
        
            $title= "Housie Hosted On $fulldateTime";
            $description= "Hosted By Admin $created_by";

         //  $d->insertUserNotification($society_id,$title,$description,"housie","housieNew.png","");


         // $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");
         // $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
         // $nResident->noti("HosieInfoPageActivity","",$society_id,$fcmArray,$title,$description,'housie');
         // $nResident->noti_ios("HousieVC","",$society_id,$fcmArrayIos,$title,$description,'housie');

        $_SESSION['msg']="Game Added Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Housie Game Added (Reschedule)");
        header("location:../manageHousie");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../manageHousie");
      }
  }


    if(isset($_POST["editHousieGame"]))
  {
    $expire = strtotime($_POST['game_date'].' '.$_POST['game_time']);
      $today = strtotime("$today $cTime");
      if($today >  $expire) {
        $_SESSION['msg1']="Time is expire select another time !";
        header("location:../manageHousie");
        exit();
      }

       $game_time= date('H:i:s',strtotime($game_time));


       $qt=$d->select("housie_room_master","game_date='$game_date' AND game_time='$game_time' AND society_id='$society_id' AND room_id!='$room_id'");
        if (mysqli_num_rows($qt)>o) {
          $_SESSION['msg1']="Game Already Added On Same Date Time !";
          header("location:../manageHousie");
          exit();
        }

    $file = $_FILES['sponser_photo']['tmp_name'];
      if(file_exists($file)) {

        $extensionResume=array("png","jpg","jpeg","PNG","JPG","JPEG");
        $extId = pathinfo($_FILES['sponser_photo']['name'], PATHINFO_EXTENSION);

         $errors     = array();
          $maxsize    = 2097152;
          
          if(($_FILES['sponser_photo']['size'] >= $maxsize) || ($_FILES["sponser_photo"]["size"] == 0)) {
              $_SESSION['msg1']="Sponser Iamge. Must be less than 2 MB.";
              header("location:../manageHousie");
              exit();
          }
          if(!in_array($extId, $extensionResume) && (!empty($_FILES["sponser_photo"]["type"]))) {
               $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
              header("location:../manageHousie");
              exit();
          }
         if(count($errors) === 0) {
          $image_Arr = $_FILES['sponser_photo'];   
          $temp = explode(".", $_FILES["sponser_photo"]["name"]);
          $sponser_photo = $kbg_game_sponsor_name.'sponser_id_'.round(microtime(true)) . '.' . end($temp);
          move_uploaded_file($_FILES["sponser_photo"]["tmp_name"], "../../img/game/".$sponser_photo);
        } 
      } else{
        $sponser_photo=$sponser_photo_old;
      }
      $fulldateTime=  date('d M Y, h:i A',strtotime($game_date.' '.$game_time));


      $que_interval11 = $que_interval*2;


     $game_time= date('H:i:s',strtotime($game_time));
        $m->set_data('game_name',$game_name);
        $m->set_data('game_date',$game_date);
        $m->set_data('game_time',$game_time);
        $m->set_data('sponser_name',$sponser_name);
      $m->set_data('sponser_photo',$sponser_photo);
      $m->set_data('sponser_url',$sponser_url);
      $m->set_data('que_interval',$que_interval11);
      $m->set_data('housie_type',$housie_type);
       
    $a1= array (
        'game_name'=>$m->get_data('game_name'),
        'game_date'=> $m->get_data('game_date'),
        'game_time'=> $m->get_data('game_time'),
        'sponser_name'=> $m->get_data('sponser_name'),
        'sponser_photo'=> $m->get_data('sponser_photo'),
        'sponser_url'=> $m->get_data('sponser_url'),
        'que_interval'=> $m->get_data('que_interval'),
        'housie_type'=> $m->get_data('housie_type'),
    );
          
          $qTemp=$d->update("housie_room_master",$a1,"room_id='$room_id'");
          if($qTemp > 0){
              $d->delete("housie_claim_rules_master","room_id='$room_id'");

              // print_r($a1);
              $masterRuleId=array_filter($_POST['master_rule_id']);
              $allowUsers=array_filter($_POST['allow_users']);
              $rulePoints=array_filter($_POST['rule_points']);
              $masterRuleId = implode("~", $masterRuleId);
              $masterRuleId = explode("~", (string)$masterRuleId);

              $allowUsers = implode("~", $allowUsers);
              $allowUsers = explode("~", (string)$allowUsers);

              $rulePoints = implode("~", $rulePoints);
              $rulePoints = explode("~", (string)$rulePoints);
                // print_r($master_rule_id);

               $levelCount= count($_POST['master_rule_id']);
                for ($i=0; $i <$levelCount ; $i++) { 
                 
                    $m->set_data('master_rule_id',$masterRuleId[$i]);
                    $m->set_data('allow_users',$allowUsers[$i]);
                    $m->set_data('rulePoints',$rulePoints[$i]);
                     $m->set_data('total_claim_player',$allowUsers[$i]);
                     $m->set_data('society_id',$society_id);
                     $m->set_data('room_id',$room_id);

                  $a3= array (
                  'master_rule_id'=>$m->get_data('master_rule_id'),
                  'society_id'=>$m->get_data('society_id'),
                  'room_id'=>$m->get_data('room_id'),
                  'allow_users_claim'=> $m->get_data('allow_users'),
                  'rule_points'=> $m->get_data('rulePoints'),
                   'total_claim_player'=> $m->get_data('total_claim_player'),
                  );
                  
                    $q2=$d->insert("housie_claim_rules_master",$a3);
                  
                }

           //  $title= "Housie Hosted On $fulldateTime";
           //  $description= "Data Updated By Admin $created_by";
           //  $d->insertUserNotification($society_id,$title,$description,"housie","housieNew.png","");


           // $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");
           // $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
           // $nResident->noti("HosieInfoPageActivity","",$society_id,$fcmArray,$title,$description,'housie');
           // $nResident->noti_ios("HousieVC","",$society_id,$fcmArrayIos,$title,$description,'housie');


             $_SESSION['msg']="Game Data Updated.";
             header("location:../manageHousie");  
          }
          else{
              // $_SESSION['msg2']="Something wrong";
              header("location:../manageHousie");  
          }
    }

   if (isset($editQue)) {
    $m->set_data('question_name',$question_name);
    $m->set_data('answer',$answer);


      $a1= array (
                'question'=> $m->get_data('question_name'),
                'answer'=> $m->get_data('answer'),
        );
         // print_r($a1);
      $q=$d->update('housie_questions_master',$a1,"que_id='$que_id' AND room_id='$room_id'"); 

      if($q>0) {
         $_SESSION['msg']="Questions Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Questions Updated ");
        header("location:../housieQuestions?room_id=$room_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../housieQuestions?room_id=$room_id");
      }
  }

  if (isset($addQuest)) {

        $question_name = implode("~", $_POST['question_name']);
        $qName    = explode('~', (string)$question_name);
        $answer = implode("~", $_POST['answer']);
        $ans    = explode('~', (string)$answer);

        // print_r($_FILES);
        $que_no= count($qName);
        for ($i=0; $i<$que_no; $i++)  {

        $question_name=  $qName[$i];
        $answer=  $ans[$i];

        $no=0;
        $m->set_data('society_id',$society_id);
        $m->set_data('question_name',$question_name);
        $m->set_data('answer',$answer);
        $m->set_data('room_id',$room_id);

        $a1= array (
                    'society_id'=> $m->get_data('society_id'),
                    'question'=> $m->get_data('question_name'),
                    'answer'=> $m->get_data('answer'),
                    'room_id'=> $m->get_data('room_id'),
            );
         // print_r($a1);
           $q=$d->insert('housie_questions_master',$a1); 
        }


      if($q>0) {
         $_SESSION['msg']="Questions Added";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Questions Added ");
        header("location:../housieQuestions?room_id=$room_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../housieQuestions?room_id=$room_id");
      }
  }



  
}
else{
  header('location:../login');
}
 ?>
