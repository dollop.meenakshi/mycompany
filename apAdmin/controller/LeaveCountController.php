<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['updateLeaveDefaultCount'])) {

        if ($_POST['leave_group_name'] != '' && $_POST['leave_group_name'] > 0) {

            $m->set_data('leave_group_name', $_POST['leave_group_name']);

            $a2 = array(
                'leave_group_name' => $m->get_data('leave_group_name'),
            );
            $q2 = $d->update("leave_group_master", $a2, "leave_group_id='$leave_group_id'");
        }

        $q1 = $d->delete("leave_default_count_master", "leave_group_id='$leave_group_id'");
        $isGroupActive = false;

        for ($i = 0; $i < count($_POST['leave_type_id']); $i++) {
            if ($_POST['number_of_leaves'][$i] != '' && $_POST['number_of_leaves'][$i] > 0) {
                $isGroupActive = true;
                $m->set_data('society_id', $society_id);
                $m->set_data('leave_group_id', $leave_group_id);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('number_of_leaves', $_POST['number_of_leaves'][$i]);
                $m->set_data('leaves_use_in_month', $_POST['leaves_use_in_month'][$i]);
                $m->set_data('carry_forward_to_next_year', $_POST['carry_forward_to_next_year'][$i]);
                $m->set_data('max_carry_forward', $_POST['max_carry_forward'][$i]);
                $m->set_data('min_carry_forward', $_POST['min_carry_forward'][$i]);
                $m->set_data('pay_out', $_POST['pay_out'][$i]);
                $m->set_data('pay_out_remark', $_POST['pay_out_remark'][$i]);
                $leave_calculation = $_POST['leave_calculation'][$i];
                if ($_POST['number_of_leaves'][$i] < 12) {
                    $leave_calculation = 0;
                }
                $m->set_data('leave_calculation', $leave_calculation);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'leave_group_id' => $m->get_data('leave_group_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'number_of_leaves' => $m->get_data('number_of_leaves'),
                    'leaves_use_in_month' => $m->get_data('leaves_use_in_month'),
                    'carry_forward_to_next_year' => $m->get_data('carry_forward_to_next_year'),
                    'max_carry_forward' => $m->get_data('max_carry_forward'),
                    'min_carry_forward' => $m->get_data('min_carry_forward'),
                    'pay_out' => $m->get_data('pay_out'),
                    'pay_out_remark' => $m->get_data('pay_out_remark'),
                    'leave_calculation' => $m->get_data('leave_calculation'),
                );

                $q = $d->insert("leave_default_count_master", $a1);
            }
        }

        if ($isGroupActive === false) {
            $q = $d->delete("leave_group_master", "leave_group_id='$leave_group_id'");
        }
        if ($q == true) {
            $_SESSION['msg'] = "Leave Count Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Count Updated Successfully");
            header("Location: ../leaveDefaultCount?lgId=$leave_group_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveDefaultCount");
        }
    }

    if (isset($_POST['addBulkLeaveDefaultCount'])) {
        mysqli_autocommit($con, FALSE);
        if ($_POST['leave_group_name'] != '' && $_POST['leave_group_name'] > 0) {
            $m->set_data('society_id', $society_id);
            $m->set_data('leave_group_name', $_POST['leave_group_name']);
            $m->set_data('leave_group_created_date', date("Y-m-d H:i:s"));
            $m->set_data('leave_group_created_by', $_COOKIE['bms_admin_id']);

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'leave_group_name' => $m->get_data('leave_group_name'),
                'leave_group_created_date' => $m->get_data('leave_group_created_date'),
                'leave_group_created_by' => $m->get_data('leave_group_created_by'),
                'leave_group_active_status' => '0',
            );

            $q = $d->insert("leave_group_master", $a);
        }
        $unit_id = $con->insert_id;

        for ($i = 0; $i < count($_POST['leave_type_id']); $i++) {

            if ($_POST['number_of_leaves'][$i] != '' && $_POST['number_of_leaves'][$i] > 0) {
                $m->set_data('society_id', $society_id);
                $m->set_data('leave_group_id', $unit_id);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('number_of_leaves', $_POST['number_of_leaves'][$i]);
                $m->set_data('leaves_use_in_month', $_POST['leaves_use_in_month'][$i]);
                $m->set_data('carry_forward_to_next_year', $_POST['carry_forward_to_next_year'][$i]);
                $m->set_data('max_carry_forward', $_POST['max_carry_forward'][$i]);
                $m->set_data('min_carry_forward', $_POST['min_carry_forward'][$i]);
                $m->set_data('pay_out', $_POST['pay_out'][$i]);
                $m->set_data('pay_out_remark', $_POST['pay_out_remark'][$i]);
                $leave_calculation = $_POST['leave_calculation'][$i];
                if ($_POST['number_of_leaves'][$i] < 12) {
                    $leave_calculation = 0;
                }
                $m->set_data('leave_calculation', $leave_calculation);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'leave_group_id' => $m->get_data('leave_group_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'number_of_leaves' => $m->get_data('number_of_leaves'),
                    'leaves_use_in_month' => $m->get_data('leaves_use_in_month'),
                    'carry_forward_to_next_year' => $m->get_data('carry_forward_to_next_year'),
                    'max_carry_forward' => $m->get_data('max_carry_forward'),
                    'min_carry_forward' => $m->get_data('min_carry_forward'),
                    'pay_out' => $m->get_data('pay_out'),
                    'pay_out_remark' => $m->get_data('pay_out_remark'),
                    'leave_calculation' => $m->get_data('leave_calculation'),
                );

                $q1 = $d->insert("leave_default_count_master", $a1);
            }
        }
        if ($q && $q1) {
            mysqli_commit($con);
            header("Location: ../leaveDefaultCount?lgId=$unit_id");
            $_SESSION['msg'] = "Leave Count Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Count Added Successfully");
        } else {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveDefaultCount");
        }
    }

    if (isset($_POST['addLeaveFromPreviousYear'])) {
        //print_r($_POST);
        $bId = $block_id;
        $dId = $floor_id;
        $laYear = $leave_year;

        for ($i = 0; $i < count($_POST['leave_type_id']); $i++) {

            if ($_POST['number_of_leaves'][$i] != '' && $_POST['number_of_leaves'][$i] > 0) {
                $leave_type_id = $_POST['leave_type_id'][$i];

                $m->set_data('society_id', $society_id);
                $m->set_data('floor_id', $floor_id);
                $m->set_data('leave_year', $leave_year);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('number_of_leaves', $_POST['number_of_leaves'][$i]);
                $m->set_data('leaves_use_in_month', $_POST['leaves_use_in_month'][$i]);
                $m->set_data('carry_forward_to_next_year', $_POST['carry_forward_to_next_year'][$i]);
                $m->set_data('max_carry_forward', $_POST['max_carry_forward'][$i]);
                $m->set_data('min_carry_forward', $_POST['min_carry_forward'][$i]);
                $m->set_data('pay_out', $_POST['pay_out'][$i]);
                $m->set_data('pay_out_remark', $_POST['pay_out_remark'][$i]);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'leave_year' => $m->get_data('leave_year'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'number_of_leaves' => $m->get_data('number_of_leaves'),
                    'leaves_use_in_month' => $m->get_data('leaves_use_in_month'),
                    'carry_forward_to_next_year' => $m->get_data('carry_forward_to_next_year'),
                    'max_carry_forward' => $m->get_data('max_carry_forward'),
                    'min_carry_forward' => $m->get_data('min_carry_forward'),
                    'pay_out' => $m->get_data('pay_out'),
                    'pay_out_remark' => $m->get_data('pay_out_remark'),
                );

                $q1 = $d->selectRow('leave_default_count_id', "leave_default_count_master", "leave_type_id='$leave_type_id' AND floor_id='$floor_id' AND leave_year='$leave_year'");
                $data = mysqli_fetch_assoc($q1);

                if (empty($data)) {
                    $q = $d->insert("leave_default_count_master", $a1);
                } else {
                    $q = $d->update('leave_default_count_master', $a1, "leave_default_count_id='$data[leave_default_count_id]'");
                }
            }
        }

        if ($q == true) {
            header("Location: ../leaveDefaultCount?bId=$bId&dId=$dId&year=$laYear");
            $_SESSION['msg'] = "Leave Count Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Count Added Successfully");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveDefaultCount");
        }
    }
}
