<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) ){ 
  if(isset($request_status)) {

    $m->set_data('request_status',$request_status);
    if ($request_status==1) {
    $m->set_data('request_closed_by',$_COOKIE['bms_admin_id']);
    } else{
    $m->set_data('request_closed_by',0);
    }


    $a1= array (
      'request_status'=> $m->get_data('request_status'),
      'request_closed_by'=> $m->get_data('request_closed_by'),
    );

    if ($request_status!=3) {
      $q=$d->update("request_master",$a1,"request_id='$request_id'");
    } 
     if ($request_status==0) { $cStatus="Open";} 
    elseif ($request_status==1) { $cStatus="Closed";  } 
    elseif ($request_status==2) { $cStatus="In Progress";}
    else { $cStatus="Reply";}

    
    if ($request_track_msg=='') {

      $request_track_msg = "Change Status";
    }

       $file11 = $_FILES["request_track_img"]["tmp_name"];
       $file22 = $_FILES["request_track_voice"]["tmp_name"];
      if(file_exists($file11)) {

        $temp = explode(".", $_FILES["request_track_img"]["name"]);
        $request_track_img = 'Request_'.round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["request_track_img"]["tmp_name"], "../../img/request/".$request_track_img);
      } 
      if (file_exists($file22)) {
        
        $temp = explode(".", $_FILES["request_track_voice"]["name"]);
        $request_track_voice = 'Request_'.round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["request_track_voice"]["tmp_name"], "../../img/request/".$request_track_voice);
      }

     


      $m->set_data('request_id',$request_id);
      $m->set_data('request_track_msg',$request_track_msg);
      $m->set_data('request_track_img',$request_track_img);
      $m->set_data('request_track_voice',$request_track_voice);
      $m->set_data('admin_id',$_COOKIE['bms_admin_id']);
      $m->set_data('request_track_date_time',date('Y-m-d H:i:s'));

      $a2= array (
        'request_track_by'=> 1,
        'request_id'=> $m->get_data('request_id'),
        'request_track_msg'=> $m->get_data('request_track_msg'),
        'request_track_img'=> $m->get_data('request_track_img'),
        'request_track_voice'=> $m->get_data('request_track_voice'),
        'admin_id'=> $m->get_data('admin_id'),
        'request_track_date_time'=> $m->get_data('request_track_date_time'),
        'request_status_view'=> $cStatus,
      );
      $q=$d->insert("request_track_master",$a2);
    
    if($request_status==TRUE) {

     
      $qUserToken=$d->select("users_master","society_id='$society_id' AND unit_id='$unit_id' AND user_id='$user_id'");
      $data_notification=mysqli_fetch_array($qUserToken);
      $sos_user_token=$data_notification['user_token'];
      $device=$data_notification['device'];
      if ($device=='android') {
         $nResident->noti("RequestFragment","",$society_id,$sos_user_token,"Your Request for $request_title ","is $cStatus by $created_by",'request');
      }  else if($device=='ios') {
        $nResident->noti_ios("RequestVC","",$society_id,$sos_user_token,"Your Request for $request_title ","is $cStatus by $created_by",'request');
      }

      $notiAry = array(
      'society_id'=>$society_id,
      'user_id'=>$data_notification['user_id'],
      'notification_title'=>"Your Request for $request_title ",
      'notification_desc'=>"is $cStatus by $created_by",    
      'notification_date'=>date('Y-m-d H:i'),
      'notification_action'=>'request',
      'notification_logo'=>'request.png',
      );
      $d->insert("user_notification",$notiAry);

      $_SESSION['msg']="Request Status Updated";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Request Status Updated");
    
      header("Location: ../requestHistory?request_id=$request_id");
    } else {
      header("Location: ../requestHistory?request_id=$request_id");
    }
  }
} ?>