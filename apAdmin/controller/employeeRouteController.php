<?php
include '../common/objectController.php';
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addEmployeeRoute']))
    {
        $route_assign_week_days_str = implode(",",$route_assign_week_days);
        $m->set_data('route_id', $route_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('route_assign_week_days', $route_assign_week_days_str);
        $a1 = array(
            'society_id' => $society_id,
            'route_id' => $m->get_data('route_id'),
            'user_id' => $m->get_data('user_id'),
            'route_assign_date' => date("Y-m-d H:i:s"),
            'route_assign_week_days' => $m->get_data('route_assign_week_days'),
            'route_assign_by' => $_COOKIE['bms_admin_id']
        );
        if(isset($route_employee_assign_id) && $route_employee_assign_id > 0)
        {
            $q = $d->update("route_employee_assign_master",$a1,"route_employee_assign_id = '$route_employee_assign_id'");
            $_SESSION['msg'] = "Employee Route Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Route Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Employee Route Successfully Updated.";
                header("Location: ../manageEmployeeRoute?RId=$route_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addEmployeeRoute");
            }
        }
        else
        {
            $q = $d->insert("route_employee_assign_master", $a1);
            $_SESSION['msg'] = "Employee Route Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Route Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Employee Route Successfully Added.";
                header("Location: ../manageEmployeeRoute?RId=$route_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addEmployeeRoute");
            }
        }
    }
    elseif(isset($_POST['deleteEmployeeRoute']))
    {
        $q = $d->delete("route_employee_assign_master","route_employee_assign_id = '$route_employee_assign_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Route deleted");
            $_SESSION['msg'] = "Employee Route Successfully Delete.";
            header("Location: ../manageEmployeeRoute?RId=$route_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageEmployeeRoute?RId=$route_id");exit;
        }
    }
    elseif(isset($_POST['checkEmployeeRouteAdded']))
    {
        $q = $d->selectRow("route_employee_assign_id","route_employee_assign_master","route_id = '$route_id' AND user_id = '$user_id'");
        if(mysqli_num_rows($q) > 0)
        {
            $response = 1;
            echo json_encode($response);exit;
        }
        else
        {
            $response = 0;
            echo json_encode($response);exit;
        }
    }
    elseif(isset($_POST['getUsersFromRoute']))
    {
        $q = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id JOIN route_employee_assign_master AS ream ON ream.user_id = um.user_id","um.user_status = 1 AND um.active_status = 0 AND um.delete_status = 0 AND ream.route_id = '$route_id'");
        $row = [];
        while($row = $q->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);exit;
    }
    elseif(isset($_POST['getRouteFromUser']))
    {
        $q = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id JOIN route_employee_assign_master AS ream ON ream.route_id = rm.route_id","ream.user_id = '$user_id'");
        $row = [];
        while($row = $q->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);exit;
    }
}
?>