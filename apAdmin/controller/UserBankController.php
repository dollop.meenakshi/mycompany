<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    
    if (isset($_POST['addUserBank'])) {

        $qqq = $d->selectRow('*', "user_bank_master", "user_id='$user_id' AND is_primary=1");

        if (mysqli_num_rows($qqq)==0) {
            $is_primary = 1;
        } else {
            $is_primary = 0;
        }



        $m->set_data('user_id', $user_id);
        $m->set_data('unit_id', $unit_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('account_holders_name', $account_holders_name);
        $m->set_data('bank_name', ucfirst($bank_name));
        $m->set_data('bank_branch_name', $bank_branch_name);
        $m->set_data('account_type', $account_type);
        $m->set_data('pan_card_no', strtoupper($pan_card_no));
        $m->set_data('esic_no', $esic_no);
        $m->set_data('user_id_old', $user_id_old);
        $m->set_data('floor_id_old', $floor_id_old);
        $m->set_data('ifsc_code', $ifsc_code);
        $m->set_data('crn_no', $crn_no);
        $m->set_data('bank_id', $bank_id);
        $m->set_data('bank_created_at', date("Y-m-d H:i:s"));
        $m->set_data('account_no', $account_no);
        $m->set_data('is_primary', $is_primary);

        $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'unit_id' => $maData['unit_id'],
            'block_id' => $m->get_data('block_id'),
            'user_id' => $m->get_data('user_id'),
            'account_holders_name' => $m->get_data('account_holders_name'),
            'bank_name' => $m->get_data('bank_name'),
            'account_type' => $m->get_data('account_type'),
            'bank_branch_name' => $m->get_data('bank_branch_name'),
            'floor_id' => $m->get_data('floor_id'),
            'account_no' => $m->get_data('account_no'),
            'ifsc_code' => strtoupper($m->get_data('ifsc_code')),
            'crn_no' => $m->get_data('crn_no'),
            'esic_no' => $m->get_data('esic_no'),
            'pan_card_no' => strtoupper($m->get_data('pan_card_no')),
            'bank_created_at' => $m->get_data('bank_created_at'),
            'is_primary' => $m->get_data('is_primary'),
        );

        if (isset($bank_id) && $bank_id > 0) {
            $a1['user_id'] = $user_id_old;
            $a1['floor_id'] = $floor_id_old;
            $userBank = $d->selectRow("user_bank_master.user_id,users_master.unit_id,users_master.society_id,users_master.user_id,users_master.block_id,users_master.floor_id", "user_bank_master LEFT JOIN users_master ON users_master.user_id = user_bank_master.user_id", "user_bank_master.bank_id='$bank_id'");
            $userBankData = mysqli_fetch_array($userBank);
            $a1['unit_id'] = $userBankData['unit_id'];
            $a1['block_id'] = $userBankData['block_id'];
            $a1['user_id'] = $userBankData['user_id'];
            $a1['floor_id'] = $userBankData['floor_id'];
            $a1['society_id'] = $userBankData['society_id'];
            $q = $d->update("user_bank_master", $a1, "bank_id ='$bank_id'");
            $_SESSION['msg'] = "User Bank Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Bank Updated Successfully");
        } else {
            $q = $d->insert("user_bank_master", $a1);
            $_SESSION['msg'] = "User Bank Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Bank Added Successfully");
        }

        if ($q == true) {
            header("Location: ../addUserBank");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addUserBank");
        }
    }
    if (isset($_POST['addEmployeeBank'])) {
      //  echo "<pre>";

        $qqq = $d->selectRow('*', "user_bank_master", "user_id='$user_id' AND is_primary=1");

        if (mysqli_num_rows($qqq)==0) {
            $is_primary = 1;
        } else {
            $is_primary = 0;
        }

        $m->set_data('user_id', $user_id);
        $m->set_data('unit_id', $unit_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('account_holders_name', $account_holders_name);
        $m->set_data('bank_name', ucfirst($bank_name));
        $m->set_data('bank_branch_name', $bank_branch_name);
        $m->set_data('account_type', $account_type);
        $m->set_data('pan_card_no', strtoupper($pan_card_no));
        $m->set_data('esic_no', $esic_no);
        $m->set_data('user_id_old', $user_id_old);
        $m->set_data('floor_id_old', $floor_id_old);
        $m->set_data('ifsc_code', $ifsc_code);
        $m->set_data('crn_no', $crn_no);
        $m->set_data('bank_id', $bank_id);
        $m->set_data('bank_created_at', date("Y-m-d H:i:s"));
        $m->set_data('account_no', $account_no);
        $m->set_data('is_primary', $is_primary);
        $user = $d->selectRow("users_master.*", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'unit_id' => $maData['unit_id'],
            'block_id' => $maData['block_id'],
            'bank_branch_name' => $m->get_data('bank_branch_name'),
            'account_holders_name' => $m->get_data('account_holders_name'),
            'account_type' => $m->get_data('account_type'),
            'user_id' => $m->get_data('user_id'),
            'bank_name' => $m->get_data('bank_name'),
            'floor_id' => $maData['floor_id'],
            'account_no' => $m->get_data('account_no'),
            'ifsc_code' => strtoupper($m->get_data('ifsc_code')),
            'crn_no' => $m->get_data('crn_no'),
            'esic_no' => $m->get_data('esic_no'),
            'pan_card_no' => strtoupper($m->get_data('pan_card_no')),
            'bank_created_at' => $m->get_data('bank_created_at'),
            'is_primary' => $m->get_data('is_primary'),
        );
       
        if (isset($bank_id) && $bank_id > 0) {
            $a1['user_id'] = $user_id;
            $a1['floor_id'] = $floor_id_old;
           
            $userBank = $d->selectRow("user_bank_master.user_id,users_master.unit_id,users_master.society_id,users_master.user_id,users_master.block_id,users_master.floor_id", "user_bank_master LEFT JOIN users_master ON users_master.user_id = user_bank_master.user_id", "user_bank_master.bank_id='$bank_id'");
            $userBankData = mysqli_fetch_array($userBank);
          //  print_r($userBankData);
            $a1['unit_id'] = $userBankData['unit_id'];
            $a1['block_id'] = $userBankData['block_id'];
            $a1['user_id'] = $userBankData['user_id'];
            $a1['floor_id'] = $userBankData['floor_id'];
            $a1['society_id'] = $userBankData['society_id'];
          //  print_r($a1);die;
            $q = $d->update("user_bank_master", $a1, "bank_id ='$bank_id'");
            
            $_SESSION['msg'] = "User Bank Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Bank Updated Successfully");
        } else {
            $q = $d->insert("user_bank_master", $a1);
            $_SESSION['msg'] = "User Bank Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Bank Added Successfully");
        }
        if ($q == true) {
            header("Location: ../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../employeeDetails?id=$user_id");
        }
    }
}
