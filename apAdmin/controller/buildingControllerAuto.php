<?php 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include_once '../lib/sms_api.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$smsObj = new sms_api();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
$base_url=$m->base_url();

header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

 //print_r($_POST);exit;
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
    $response = array();

  // add main menu
  if(isset($_POST['society_name']) && $society_id!=0 && $society_id!=''){
     $qcb=$d->select("society_master","");
     $dataqcb=mysqli_fetch_array($qcb);
     if ($dataqcb>0) {
        $response["message"]="Society is Already Created ";
        $response["status"]="201";
        echo json_encode($response);
        exit();
     } else if ($base_url!=$sub_domain) {
        $response["message"]="Invalid Base Url Set ($base_url)";
        $response["status"]="201";
        echo json_encode($response);
        exit();
     } else {



      if ($package_id==0) {
        $q=$d->select("role_master","role_id='2'","");
        $row=mysqli_fetch_array($q);
        $menu_id=$row['menu_id'];
        $pagePrivilege=$row['pagePrivilege'];
        $plan_expire_date= date('Y-m-d', strtotime(' +'.$trial_days.' day'));
      } else {

          $q=$d->select("package_master","package_id='$package_id'","");
          $row1=mysqli_fetch_array($q);
          $q=$d->select("role_master","role_id='2'","");
          $row=mysqli_fetch_array($q);
          $menu_id=$row['menu_id'];
          $pagePrivilege=$row['pagePrivilege'];
           
      }

     

      $m->set_data('society_name',$society_name);
      $m->set_data('society_address',$society_address);
      $m->set_data('society_pincode',$society_pincode);
      $m->set_data('soiciety_latitude',$soiciety_latitude);
      $m->set_data('sociaty_longitude',$sociaty_longitude);
      $m->set_data('secretary_email',$secretary_email);
      $m->set_data('secretary_mobile',$secretary_mobile);
      $m->set_data('socieaty_logo',$socieaty_logo);
      $m->set_data('builder_name',$builder_name);
      $m->set_data('country_id',$country_id);
      $m->set_data('state_id',$state_id);
      $m->set_data('city_id',$city_id);
      $m->set_data('city_name',$city_name);
      $m->set_data('state_id',$state_id);
      $m->set_data('sub_domain',$sub_domain);
      $m->set_data('builder_address',$builder_address);
      $m->set_data('builder_mobile',$builder_mobile);
      $m->set_data('package_id',$package_id);
      $m->set_data('trial_days',$trial_days);
      $m->set_data('plan_expire_date',$plan_expire_date);
      $m->set_data('last_renew_date',$last_renew_date);
      $m->set_data('api_key',$api_key);
      $m->set_data('currency',$currency);
      $m->set_data('society_type',$society_type);

      $a =array(
        'society_type'=> $m->get_data('society_type'),
        'society_name'=> $m->get_data('society_name'),
          'society_address'=> $m->get_data('society_address'),
          'society_pincode'=> $m->get_data('society_pincode'),
          'society_latitude'=> $m->get_data('soiciety_latitude'),
          'society_longitude'=> $m->get_data('sociaty_longitude'),
          'secretary_email'=>$m->get_data('secretary_email'),
          'secretary_mobile'=>$m->get_data('secretary_mobile'),
          'socieaty_logo'=>$m->get_data('socieaty_logo'),
          'builder_name'=>$m->get_data('builder_name'),
          'country_id'=>$m->get_data('country_id'),
          'state_id'=>$m->get_data('state_id'),
          'city_id'=>$m->get_data('city_id'),
          'city_name'=>$m->get_data('city_name'),
          'sub_domain'=>$m->get_data('sub_domain'),
          'api_key'=>$m->get_data('api_key'),
          'currency'=>$m->get_data('currency'),
          'builder_address'=>$m->get_data('builder_address'),
          'builder_mobile'=>$m->get_data('builder_mobile'),
          'package_id'=>$m->get_data('package_id'),
          'trial_days'=>$m->get_data('trial_days'),
          'plan_expire_date'=>$m->get_data('plan_expire_date'),
          'last_renew_date'=>$m->get_data('last_renew_date'),

      );
      $q=$d->insert("society_master",$a);

      if($q>0) {

        $bytes = openssl_random_pseudo_bytes(4);
        $admin_password = bin2hex($bytes);

        $q11=$d->select("role_master","role_id='2'","");
        $row111=mysqli_fetch_array($q11);
        $menu_id=$row111['menu_id'];
        $pagePrivilege=$row111['pagePrivilege'];

        $camCtrAry=array();
          $q33=$d->select("complaint_category","active_status=0","");
          while($rowsc=mysqli_fetch_array($q33)){
              $cat_ids=$rowsc['complaint_category_id'];
              array_push($camCtrAry, $cat_ids);
        }

        $complaint_category_id = implode(',', $camCtrAry);

        // MyCo Admin
        $aFin =array(
          'role_id'=>2,
          'society_id'=> $society_id,
          'admin_name'=>"MyCo Admin",
          'admin_email'=>"contact@my-company.app",
          'admin_mobile'=>"9687271071",
          'admin_password'=>$masterAuth,
          'created_date'=>date("Y-m-d"),
          'complaint_category_id'=>$complaint_category_id,
        );
        $d->insert("bms_admin_master",$aFin);
        $a7 =array(
          'society_id'=> $society_id,
        );
        $d->update("role_master",$a7,"role_id=2");
        $d->update("society_master",$a7,"");
        // add Role society admin
         $a5 =array(
          'society_id'=> $society_id,
          'role_name'=>"MyCo Admin",
          'menu_id'=> $menu_id,
          'pagePrivilege'=> $pagePrivilege,
          'adminAppPrivilege'=> "",
          'admin_type'=> 1,
        );
        $d->insert("role_master",$a5);
        $role_id = $con->insert_id;
         // society admin entry
        $a3 =array(
          'role_id'=>$role_id,
          'society_id'=> $society_id,
          'admin_name'=>$secretary_name,
          'admin_email'=>$m->get_data('secretary_email'),
          'admin_mobile'=>$m->get_data('secretary_mobile'),
          'admin_password'=>$admin_password,
          'created_date'=>date("Y-m-d"),
          'complaint_category_id'=>$complaint_category_id,
        );
        $d->insert("bms_admin_master",$a3);
        $admin_id= $con->insert_id;
        
        
        $a4 =array(
          'society_id'=> $society_id,
          'balancesheet_name'=>"Main BalanceSheet",
          'created_date'=> date("Y-m-d H:i:s"),
          'current_balance'=>0,
        );
       $d->insert("balancesheet_master",$a4);
       

        
        // create default sos 

         $qSos=$d->select("sos_events_common","");
         while ($sosRow=mysqli_fetch_array($qSos)) {
             
             $m->set_data('society_id',$society_id);
             $m->set_data('event_name',$sosRow['event_name']);
             $m->set_data('sos_image',$sosRow['sos_image']);
             $m->set_data('sos_for',$sosRow['sos_for']);

               $a1= array (
                  'society_id'=> $m->get_data('society_id'),
                  'event_name'=> $m->get_data('event_name'),
                  'sos_image'=> $m->get_data('sos_image'),
                  'sos_for'=> $m->get_data('sos_for'),
              );

              $q11=$d->select("sos_events_master","society_id='$society_id' AND event_name='$sosRow[event_name]'");
              if (mysqli_num_rows($q11)==0) {
                 $q11=$d->insert("sos_events_master",$a1);
              } 
           }

         $qEmp=$d->select("emp_type_common","");
         while ($empRow=mysqli_fetch_array($qEmp)) {
             
             $m->set_data('society_id',$society_id);
             $m->set_data('employee_type_name_add',$empRow['emp_type_name']);
             $m->set_data('emp_type_icon',$empRow['emp_type_icon']);

                 $a1= array (
                    
                    'society_id'=> $m->get_data('society_id'),
                    'emp_type_name'=> $m->get_data('employee_type_name_add'),
                    'emp_type_icon'=> $m->get_data('emp_type_icon'),
                );

              $q22=$d->select("emp_type_master","society_id='$society_id' AND emp_type_name='$empRow[emp_type_name]'");
              if (mysqli_num_rows($q22)==0) {
                 $q=$d->insert("emp_type_master",$a1);
              } 
           }
       

         $qNoti=$d->select("admin_notification_master","");
         while ($notiData=mysqli_fetch_array($qNoti)) {
             
             $m->set_data('society_id',$society_id);
             $m->set_data('admin_id',$admin_id);
             $m->set_data('admin_notification_id',$notiData['admin_notification_id']);

                 $aNoti= array (
                    
                    'society_id'=> $m->get_data('society_id'),
                    'admin_id'=> $m->get_data('admin_id'),
                    'admin_notification_id'=> $m->get_data('admin_notification_id'),
                );

              $q2N=$d->select("bms_admin_notification_master","admin_id='$admin_id' AND admin_notification_id='$notiData[admin_notification_id]'");
              if (mysqli_num_rows($q2N)==0) {
                 $q=$d->insert("bms_admin_notification_master",$aNoti);
              } 
           }


         
         
         $forgotLink= $base_url.'apAdmin/';
        
        if ($country_code=='') {
          $country_code= "+91";
        }
        $smsObj->send_welcome_message_admin($society_id,$secretary_name,'Main Admin',$society_name,$secretary_mobile,$secretary_email,$admin_password,$forgotLink,$country_code);
        $d->add_sms_log($secretary_mobile,"Admin Welcome Message",$society_id,$country_code,4);
        
       
        $to = $secretary_email;
        $admin_mobile= $secretary_mobile;
        $subject = "Account Created - MyCo";
        $admin_name = $secretary_name;
        $role_name = 'Admin';
        include '../mail/newAdminMail.php';
        include '../mail.php';
        
        // $d->insert_log("","0","$_COOKIE[bms_admin_id]","$created_by","Society Added .");
        $response["message"]="Society Successfully Created";
        $response["status"]="200";
        echo json_encode($response);
        exit();
      } else {
        $response["message"]="Something Went Wrong";
        $response["status"]="201";
        echo json_encode($response);
        exit();
      }
    }
  }
  


}
else{
  header('location:../login');
 }
 ?>
