<?php 
include '../common/objectController.php';
extract($_POST);


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605

	 if(isset($_POST['update_user_mac_seting']) && $_POST['update_user_mac_seting'] == "update_user_mac_seting")
    {
        $update_arr = [
            'mac_bind_on' => $mac_bind_on
        ];
        $q = $d->update("users_master",$update_arr,"user_id = '$user_id'");
        if($q)
        {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work report successfully updated");
            $_SESSION['msg'] = "Mac Device Bind Setting Changed successfully.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }

    if(isset($_POST['updateUsersDeviceBindMulti']) && $_POST['updateUsersDeviceBindMulti'] == "updateUsersDeviceBindMulti")
    {
        $update_arr = [
            'mac_bind_on' => $mac_bind_on
        ];
        foreach($user_id AS $key => $value)
        {
            $q = $d->update("users_master",$update_arr,"user_id = '$value'");
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Mac Device Bind Setting Changed");
        }
        if($q)
        {
            $_SESSION['msg'] = "Mac Device Bind Setting Changed successfully.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }

	if(isset($_POST['macAddressApprove'])){

		mysqli_autocommit($con,FALSE);

		$m->set_data('mac_address_change_status',$mac_address_change_status);
		$m->set_data('mac_address_status_change_date',date("Y-m-d"));
		$m->set_data('mac_address_status_change_by',$_COOKIE['bms_admin_id']);
		$m->set_data('user_mac_address',$mac_address);
		
		$a1 = array(
		'mac_address_change_status'=>$m->get_data('mac_address_change_status'),
		'mac_address_status_change_date'=>$m->get_data('mac_address_status_change_date'), 
		'mac_address_status_change_by_type'=>1,  
		'mac_address_status_change_by'=>$m->get_data('mac_address_status_change_by'), 
		);
		
		$q=$d->update("user_mac_address_master",$a1,"user_mac_address_id='$user_mac_address_id'");
		
		$a2 = array(
			'user_mac_address'=>$m->get_data('user_mac_address'),
		);

		$q1=$d->update("users_master",$a2,"user_id='$user_id'");

		if($q && $q1) {
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "MAC Address Approve Successfully");
			mysqli_commit($con);
			$_SESSION['msg'] = "MAC Address Approve Successfully";
			header("Location: ../macAddress");
		} else {
			mysqli_rollback($con);
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../macAddress");
		}
	}

	if(isset($_POST['macAddressReject'])){

		$m->set_data('mac_address_change_status',$mac_address_change_status);
		$m->set_data('mac_address_reject_reason',$mac_address_reject_reason);
		$m->set_data('mac_address_status_change_date',date("Y-m-d"));
		$m->set_data('mac_address_status_change_by',$_COOKIE['bms_admin_id']);
		
		$a1 = array(
		'mac_address_reject_reason'=>$m->get_data('mac_address_reject_reason'),
		'mac_address_change_status'=>$m->get_data('mac_address_change_status'),
		'mac_address_status_change_date'=>$m->get_data('mac_address_status_change_date'), 
		'mac_address_status_change_by_type'=>1,  
		'mac_address_status_change_by'=>$m->get_data('mac_address_status_change_by'), 
		);
		
		$q=$d->update("user_mac_address_master",$a1,"user_mac_address_id='$user_mac_address_id'");

		if($q) {
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "MAC Address Reject Successfully");
			$_SESSION['msg'] = "MAC Address Reject Successfully";
			header("Location: ../macAddress");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../macAddress");
		}
	}

}
?>