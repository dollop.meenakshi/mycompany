<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));

if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    if (isset($_POST['addAdvanceSalary'])) {
       
        $m->set_data('user_id', $user_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('advance_salary_date', $advance_salary_date);
        $m->set_data('advance_salary_remark', $advance_salary_remark);
        $m->set_data('advance_salary_mode', $advance_salary_mode);
        $m->set_data('advance_salary_amount', $advance_salary_amount);
        $m->set_data('created_date', date("Y-m-d H:i:s"));
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'floor_id' => $m->get_data('floor_id'),
            'block_id' => $m->get_data('block_id'),
            'user_id' => $m->get_data('user_id'),
            'advance_salary_date' => $m->get_data('advance_salary_date'),
            'advance_salary_remark' => $m->get_data('advance_salary_remark'),
            'advance_salary_mode' => $m->get_data('advance_salary_mode'),
            'advance_salary_amount' => $m->get_data('advance_salary_amount'),
            'created_date' => $m->get_data('created_date'),
            'created_by_type' => 0,
            'advance_salary_created_by' => $_COOKIE['bms_admin_id'],
            'created_date' => $m->get_data('created_date')
        );
        if($advance_salary_id && $advance_salary_id>0){
            $q = $d->update("advance_salary",$a1,"advance_salary_id='$advance_salary_id'");
            $_SESSION['msg'] = "Advance Salary Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Advance Salary Added Successfully");
        }else{
            $q = $d->insert("advance_salary", $a1);
            $_SESSION['msg'] = "Advance Salary Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Advance Salary Added Successfully");
        }
        $crMonthYr = date('Y-m',strtotime($advance_salary_date));
        $totalMonthAmount = 0;
        $mAdvSalry = $d->selectRow('advance_salary.*','advance_salary',"DATE_FORMAT(advance_salary_date,'%Y-%m')='$crMonthYr' AND user_id=$user_id");
        while($monthly = mysqli_fetch_assoc($mAdvSalry)){
            $totalMonthAmount +=$monthly['advance_salary_amount'];
        }
       $a2 = array('total_advance_amount'=>$totalMonthAmount,'month'=> $crMonthYr,'user_id'=>$user_id,'monthly_advance_salary_date'=>date('Y-m-d h:i:s'));
       $checkMonthDatasql =  $d->selectRow('monthly_advance_salary.*','monthly_advance_salary',"month='$crMonthYr' AND user_id=$user_id");
       if(mysqli_num_rows($checkMonthDatasql)>0){
           $checkMonthData = mysqli_fetch_assoc($checkMonthDatasql);
           $monthly_advance_salary_id = $checkMonthData['monthly_advance_salary_id'];
            $q = $d->update("monthly_advance_salary",$a2,"monthly_advance_salary_id='$monthly_advance_salary_id'");
        
    }else
       {
          $q = $d->insert("monthly_advance_salary", $a2);
       }
    }
    if ($q == true) {
        header("Location: ../advanceSalary");
    } else {
        $_SESSION['msg1'] = "Something Wrong";
        header("Location: ../advanceSalary");
    }
}
