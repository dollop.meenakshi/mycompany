<?php 
include '../common/objectController.php';
extract($_POST);

// add new Notice Board
if(isset($_POST) && !empty($_POST) )

{
  $title = test_input($title);
  $description = test_input($description); 

  if(isset($title) && $send_to==0 && $send_to !="All" ) {
    
    //IS_1209
    
    
    $qss=$d->select("block_master" ,"society_id='$society_id'  and block_status = 0 ","ORDER BY block_sort ASC");
     if(mysqli_num_rows($qss) <= 0){
      //header("Location: ../welcome");
       $_SESSION['msg1']="There are no blocks in this society, please add blocks first and then send notification to their users.";
      header('Location: ' . $_SERVER['HTTP_REFERER']);
      exit;
    }

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['notiUrl']['tmp_name'];
      $ext = pathinfo($_FILES['notiUrl']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand();
          $dirPath = "../../img/noticeBoard/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1600) {
            $newWidthPercentage= 1600*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }
          switch ($imageType) {
            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           
            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../welcome");
                exit;
                break;
            }
            $notiImgName= $newFileName."_user.".$ext;

            $notiUrl = $base_url.'img/noticeBoard/'.$notiImgName;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../welcome");
          exit();
         }
        } else {
          $notiImgName= "";
        }       

        
    //IS_1209
      $title = ucfirst($title);
      $description = $description; 

        $clickAray = array(
            'title' => $title,
            'description' => $description,
            'img_url' =>  $notiUrl,
            'notification_time' => date("d M Y h:i A"),
        );     
        if(isset($_POST['bId']) && $_POST['bId'] !="" ){
          if($_POST['bId'] >0 && $_POST['bId'] !="All")
          {
            $blkFltr = "AND block_id = '$_POST[bId]'";
          }
          
        }
        if(isset($_POST['dId']) && $_POST['dId'] !="" ){
          if($_POST['dId'] >0 && $_POST['dId'] !="All")
          {
            $floorFltr = "AND floor_id = '$_POST[dId]'";
          }
        }
         $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $blkFltr $floorFltr $blockAppendQueryOnly");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $blkFltr $floorFltr  $blockAppendQueryOnly");
         // print_r($fcmArray);
        $titleCheck = strtolower($title);
        if ($titleCheck!='logout') {
          $nResident->noti("custom_notification",$notiUrl,$society_id,$fcmArray,$title,$description,$clickAray);
          $nResident->noti_ios("custom_notification",$notiUrl,$society_id,$fcmArrayIos,$title,$description,$clickAray);
          $d->insertUserNotification($society_id,$title,$description,"custom_notification",$notiImgName,"");
        }  else {
          $nResident->noti("",$notiUrl,$society_id,$fcmArray,$title,$description,"");
          $nResident->noti_ios("",$notiUrl,$society_id,$fcmArrayIos,$title,$description,"");
        }

        
      $_SESSION['msg']="Notification Send";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Send");
      header("Location: ../welcome");
  
  }

  if(isset($title) && $send_to==1) {

     $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['notiUrl']['tmp_name'];
      $ext = pathinfo($_FILES['notiUrl']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand();
          $dirPath = "../../img/noticeBoard/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1600) {
            $newWidthPercentage= 1600*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../welcome");
                exit;
                break;
            }
            $notiImgName= $newFileName."_user.".$ext;

            $notiUrl = $base_url.'img/noticeBoard/'.$notiImgName;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../welcome");
          exit();
         }
        } else {
          $notiImgName= "";
        }       

    //IS_1209
      $title = ucfirst($title);
      $description = $description;

      $qa=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.admin_active_status=0","GROUP BY token");
      while($row=mysqli_fetch_array($qa))
      { 
        if ($row['device']=='android') {
          $fcm = $row['token'];
          
            $notiAry = array(
              'admin_id'=>$row['admin_id'],
              'society_id'=>$society_id,
              'notification_tittle'=>$title,
              'notification_description'=>$description,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>"custom",
              'admin_click_action '=>"welcome",
              'notification_logo'=>$notiImgName,
            );
                    
          $d->insert("admin_notification",$notiAry);
          $notification_id = $con->insert_id;

          $admin_click_action = "viewNotification?id=$notification_id";
          $nAdmin->noti_new($society_id,"$notiUrl", $fcm, $title,$description, "$admin_click_action");
          

        } else if($row['device']=='ios') {
          $fcmIos = $row['token'];
        
           $notiAry = array(
              'admin_id'=>$row['admin_id'],
              'society_id'=>$society_id,
              'notification_tittle'=>$title,
              'notification_description'=>$description,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>"custom",
              'admin_click_action '=>"welcome",
              'notification_logo'=>$notiImgName,
            );
                    
          $d->insert("admin_notification",$notiAry);
          $notification_id = $con->insert_id;

          $admin_click_action = "viewNotification?id=$notification_id";
         
          $nAdmin->noti_ios_new($society_id,"$notiUrl", $fcmIos, $title, $description, "$admin_click_action");
        }

      }

      $_SESSION['msg']="Notification Send to admin";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Send to Admin");
      header("Location: ../welcome");


  } 

  if(isset($title) && $send_to==2) {

      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['notiUrl']['tmp_name'];
      $ext = pathinfo($_FILES['notiUrl']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand();
          $dirPath = "../../img/noticeBoard/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1600) {
            $newWidthPercentage= 1600*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../welcome");
                exit;
                break;
            }
            $notiImgName= $newFileName."_user.".$ext;

            $notiUrl = $base_url.'img/noticeBoard/'.$notiImgName;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../welcome");
          exit();
         }
        } else {
          $notiImgName= "";
        }       

      $clickAray = array(
            'title' => $title,
            'description' => $description,
            'img_url' =>  $notiUrl,
            'notification_action'=>'custom_notification',
            'notification_time' => date("d M Y h:i A"),
        );   
      $fcmToken=$d->get_emp_fcm("employee_master","society_id='$society_id' AND emp_token!=''");
      
       $nGaurd->noti_new("custom_notification",$fcmToken,$title,$description,$clickAray);

       $d->insertGuardNotificationAll($notiImgName,$title,$description,"custom_notification",$society_id);

       $_SESSION['msg']="Notification Send to Security Guard";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Send to Security Guard");
      header("Location: ../welcome");

  }


  if(isset($title) && $send_to==3) {
    
   //print_r($_POST);die;

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['notiUrl']['tmp_name'];
      $ext = pathinfo($_FILES['notiUrl']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand();
          $dirPath = "../../img/noticeBoard/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1600) {
            $newWidthPercentage= 1600*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }

         switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../employeeDetails?id=$user_id");
                exit;
                break;
            }
            $notiImgName= $newFileName."_user.".$ext;

            $notiUrl = $base_url.'img/noticeBoard/'.$notiImgName;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("Location: ../employeeDetails?id=$user_id");
          exit();
         }
        } else {
          $notiImgName= "";
        }       

        
        $title = ucfirst($title);
        $description = $description;

        $clickAray = array(
            'title' => $title,
            'description' => $description,
            'img_url' =>  $notiUrl,
            'notification_time' => date("d M Y h:i A"),
        );     
       
       $titleCheck = strtolower($title);
       if ($titleCheck!='logout') {
         if ($device=='android') {
          $nResident->noti("custom_notification",$notiUrl,$society_id,$user_token,$title,$description,$clickAray);
         } else {
          $nResident->noti_ios("custom_notification",$notiUrl,$society_id,$user_token,$title,$description,$clickAray);
         }
       } else {
         if ($device=='android') {
          $nResident->noti("",$notiUrl,$society_id,$user_token,$title,$description,"");
         } else {
          $nResident->noti_ios("",$notiUrl,$society_id,$user_token,$title,$description,"");
         }
       }
        

        $notiAry = array(
          'society_id'=>$society_id,
          'user_id'=>$user_id,
          'notification_title'=>"$title",
          'notification_desc'=>"$description",    
          'notification_date'=>date('Y-m-d H:i'),
          'notification_action'=>'custom_notification',
          'notification_logo'=>$notiImgName,
        );
        $d->insert("user_notification",$notiAry);
        
      $_SESSION['msg']="Notification Send";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Send To $user_name");
     header("Location: ../employeeDetails?id=$user_id");
  }

   ////////////////date 24 nov 2021 Dollop

  if (isset($title) && $send_to == "birthDayNoti") {

      $title = ucfirst($title);
      $description = $description;
      $clickAray = array(
          'title' => $title,
          'description' => $description,
          'img_url' => "",
          'notification_time' => date("d M Y h:i A"),
      );
      $titleCheck = strtolower($title);
      if ($titleCheck != 'logout') {
          if ($device == 'android') {
              $nResident->noti("custom_notification", $notiUrl, $society_id, $user_token, $title, $description, $clickAray);
          } else {
              $nResident->noti_ios("custom_notification", $notiUrl, $society_id, $user_token, $title, $description, $clickAray);
          }
      } else {
          if ($device == 'android') {
              $nResident->noti("", $notiUrl, $society_id, $user_token, $title, $description, "");
          } else {
              $nResident->noti_ios("", $notiUrl, $society_id, $user_token, $title, $description, "");
          }
      }

      $notiAry = array(
          'society_id' => $society_id,
          'user_id' => $user_id,
          'notification_title' => "$title",
          'notification_desc' => "$description",
          'notification_date' => date('Y-m-d H:i'),
          'notification_action' => 'custom_notification',
          'notification_logo' => "",
      );
      $d->insert("user_notification", $notiAry);

      $_SESSION['msg'] = "Birthday Notification Send";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Notification Send To $user_name");
      header("Location: ../employeeDetails?id=$user_id");

  }

  if(isset($title) && $send_to == "All"){
   
    $qss=$d->select("block_master" ,"society_id='$society_id'  and block_status = 0 $blkFltr","ORDER BY block_sort ASC");
    if(mysqli_num_rows($qss) <= 0){
     //header("Location: ../welcome");
      $_SESSION['msg1']="There are no blocks in this society, please add blocks first and then send notification to their users.";
     header('Location: ' . $_SERVER['HTTP_REFERER']);
     exit;
   }

   $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
     $uploadedFile = $_FILES['notiUrl']['tmp_name'];
     $ext = pathinfo($_FILES['notiUrl']['name'], PATHINFO_EXTENSION);
     if(file_exists($uploadedFile)) {
       if(in_array($ext,$extension)) {

         $sourceProperties = getimagesize($uploadedFile);
         $newFileName = rand();
         $dirPath = "../../img/noticeBoard/";
         $imageType = $sourceProperties[2];
         $imageHeight = $sourceProperties[1];
         $imageWidth = $sourceProperties[0];
         if ($imageWidth>1600) {
           $newWidthPercentage= 1600*100 / $imageWidth;  //for maximum 400 widht
           $newImageWidth = $imageWidth * $newWidthPercentage /100;
           $newImageHeight = $imageHeight * $newWidthPercentage /100;
         } else {
           $newImageWidth = $imageWidth;
           $newImageHeight = $imageHeight;
         }
         switch ($imageType) {
           case IMAGETYPE_PNG:
               $imageSrc = imagecreatefrompng($uploadedFile); 
               $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
               imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
               break;           
           case IMAGETYPE_JPEG:
               $imageSrc = imagecreatefromjpeg($uploadedFile); 
               $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
               imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
               break;
           case IMAGETYPE_GIF:
               $imageSrc = imagecreatefromgif($uploadedFile); 
               $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
               imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
               break;

           default:
              $_SESSION['msg1']="Invalid Image";
               header("Location: ../welcome");
               exit;
               break;
           }
           $notiImgName= $newFileName."_user.".$ext;

           $notiUrl = $base_url.'img/noticeBoard/'.$notiImgName;
        } else {
         $_SESSION['msg1']="Invalid Photo";
         header("location:../welcome");
         exit();
        }
       } else {
         $notiImgName= "";
       }       

       
   //IS_1209
     $title = ucfirst($title);
     $description = $description; 

       $clickAray = array(
           'title' => $title,
           'description' => $description,
           'img_url' =>  $notiUrl,
           'notification_time' => date("d M Y h:i A"),
       );     
      
        $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'   $blockAppendQueryOnly");
        $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'$blockAppendQueryOnly");
        
       $titleCheck = strtolower($title);
       if ($titleCheck!='logout') {
         $nResident->noti("custom_notification",$notiUrl,$society_id,$fcmArray,$title,$description,$clickAray);
         $nResident->noti_ios("custom_notification",$notiUrl,$society_id,$fcmArrayIos,$title,$description,$clickAray);
         $d->insertUserNotification($society_id,$title,$description,"custom_notification",$notiImgName,"");
       }  else {
         $nResident->noti("",$notiUrl,$society_id,$fcmArray,$title,$description,"");
         $nResident->noti_ios("",$notiUrl,$society_id,$fcmArrayIos,$title,$description,"");
       }
       $clickAray = array(
        'title' => $title,
        'description' => $description,
        'img_url' =>  $notiUrl,
        'notification_action'=>'custom_notification',
        'notification_time' => date("d M Y h:i A"),
        );   
      $fcmTokens=$d->get_emp_fcm("employee_master","society_id='$society_id' AND emp_token!=''");
      
      $nGaurd->noti_new("custom_notification",$fcmTokens,$title,$description,$clickAray);

      $d->insertGuardNotificationAll($notiImgName,$title,$description,"custom_notification",$society_id);


      $qa=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.admin_active_status=0","GROUP BY token");
    while($row=mysqli_fetch_array($qa))
    { 
      if ($row['device']=='android') {
        $fcm = $row['token'];
        
          $notiAry = array(
            'admin_id'=>$row['admin_id'],
            'society_id'=>$society_id,
            'notification_tittle'=>$title,
            'notification_description'=>$description,
            'notifiaction_date'=>date('Y-m-d H:i'),
            'notification_action'=>"custom",
            'admin_click_action '=>"welcome",
            'notification_logo'=>$notiImgName,
          );
                  
        $d->insert("admin_notification",$notiAry);
        $notification_id = $con->insert_id;

        $admin_click_action = "viewNotification?id=$notification_id";
        $nAdmin->noti_new($society_id,"$notiUrl", $fcm, $title,$description, "$admin_click_action");
        

      } else if($row['device']=='ios') {
        $fcmIos = $row['token'];
      
         $notiAry = array(
            'admin_id'=>$row['admin_id'],
            'society_id'=>$society_id,
            'notification_tittle'=>$title,
            'notification_description'=>$description,
            'notifiaction_date'=>date('Y-m-d H:i'),
            'notification_action'=>"custom",
            'admin_click_action '=>"welcome",
            'notification_logo'=>$notiImgName,
          );
                  
        $d->insert("admin_notification",$notiAry);
        $notification_id = $con->insert_id;

        $admin_click_action = "viewNotification?id=$notification_id";
       
        $nAdmin->noti_ios_new($society_id,"$notiUrl", $fcmIos, $title, $description, "$admin_click_action");
      }

    }
       
     $_SESSION['msg']="Notification Send";
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Send");
     header("Location: ../welcome");
  }

    //20220728
  if(isset($notificationCount) && $notificationCount=='yes') {
    
  
    $sql = "SELECT COUNT(*) AS notification_count FROM (
              SELECT notification_id,read_status
              FROM `admin_notification`
              WHERE society_id='$society_id' AND (admin_id =0 OR admin_id ='$bms_admin_id')
              ORDER BY notification_id DESC 
              LIMIT 1000
            ) AS temp
            WHERE temp.read_status=0";
    $notificationData = $d->executeSql($sql,'row_array');
    $totalNotification = $notificationData['notification_count'];
    if ($totalNotification<100) {
      echo $totalNotification;
    } else {
      echo "99+";
    }
  }

    //20220728
}
 ?>