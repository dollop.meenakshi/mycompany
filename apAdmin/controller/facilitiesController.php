<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

extract($_POST);
//IS_1313
 
if ( isset($payment_bank) && !preg_match('/^[a-z\d\-_\s]+$/i',$payment_bank)){
    $_SESSION['msg1']="Special Characters are Not Allowed, please provide valid bank name";
     header('Location: ../facilityBook?id=$facility_id');
      exit;
} 


// delete user
if(isset($deletefacilities)) {
  $facility_id=$_POST['facility_id'];

  $q=$d->delete("facilities_master","facility_id='$facility_id'");
  $q=$d->delete("facilitybooking_master","facility_id='$facility_id'");
  $q=$d->delete("facility_schedule_master","facility_id='$facility_id'");
  if($q==TRUE) {
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$deletedFacilityName Facility Deleted");
     echo $_SESSION['msg']="Facility Deleted";
    header("Location: ../facilities");
  } else {
    header("Location: ../facilities");
  }
}


// add new user
if(isset($addfacilities)) {

  if($_POST['Sunday_status'] == 1 && $_POST['Monday_status'] == 1 && $_POST['Tuesday_status'] == 1 && $_POST['Wednesday_status'] == 1 && $_POST['Thursday_status'] == 1 && $_POST['Friday_status'] == 1 && $_POST['Saturday_status'] == 1) 
  {
    $_SESSION['msg1']="At Least Need 1 time slot to open facility";
    header("location:../facility");
    exit();
  }

       if($_POST['Monday_opening_time'][0]=='' && $_POST['Sunday_opening_time'][0]=='' && $_POST['Tuesday_opening_time'][0]=='' && $_POST['Wednesday_opening_time'][0]=='' && $_POST['Thursday_opening_time'][0]=='' && $_POST['Friday_opening_time'][0]=='' && $_POST['Saturday_opening_time'][0]=='') {
           echo $_SESSION['msg1']="At Least Need 1 time slot to open facility";
          header("location:../facility");
           exit();
       }
  

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['facility_photo']['tmp_name'];
    $ext = pathinfo($_FILES['facility_photo']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }



        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facility");
              exit;
              break;
        }
        $facility_photo= $newFileName."_facility.".$ext;
      } else{
        $_SESSION['msg1']="Invalid Facility Photo";
        header("location:../facility");
        exit();
      }
    } else {
      $facility_photo="default.png";
    }

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['facility_photo_2']['tmp_name'];
    $ext = pathinfo($_FILES['facility_photo_2']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }



        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facility");
              exit;
              break;
        }
        $facility_photo_2= $newFileName."_facility2.".$ext;
      } else{
        $_SESSION['msg1']="Invalid Facility Photo";
        header("location:../facility");
        exit();
      }
    } else {
      $facility_photo_2="";
    }


      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['facility_photo_3']['tmp_name'];
    $ext = pathinfo($_FILES['facility_photo_3']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }



        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facility");
              exit;
              break;
        }
        $facility_photo_3= $newFileName."_facility3.".$ext;
      } else{
        $_SESSION['msg1']="Invalid Facility Photo";
        header("location:../facility");
        exit();
      }
    } else {
      $facility_photo_3="";
    }

   
    if ($person_limit<1 && $facility_type!=2) {
        $_SESSION['msg1']="Please set Person limit Greater Than 0";
        header("Location: ../facility");
        exit();
    }  if ($facility_type!=2) {
          if ($facility_amount<1) {
              $_SESSION['msg1']="Invalid Facility Amount ";
               header("Location: ../facility");
               exit();
          }
        }

    if ($facility_type==1) {
      $booking_type=1;
    } else {
      $booking_type=0;
    }

    if ($is_taxble=='1' && $gst==1) {
       $gst_amount_adult =  $facility_amount *$tax_slab/100;  //only for Exclude
       $facility_amount = $facility_amount + $gst_amount_adult;

       $gst_amount_adult_tenant =  $facility_amount_tenant *$tax_slab/100;  //only for Exclude
       $facility_amount_tenant = $facility_amount_tenant + $gst_amount_adult_tenant;
    }

    $m->set_data('society_id',$society_id);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('facility_name',$facility_name);
    $m->set_data('facility_description',$facility_description);
    $m->set_data('facility_address',$facility_address);
    $m->set_data('facility_type',$facility_type);
    $m->set_data('amount_type',$amount_type);
    $m->set_data('booking_type',$booking_type);
    $m->set_data('facility_amount',$facility_amount);
    $m->set_data('facility_amount_tenant',$facility_amount_tenant);
    $m->set_data('person_limit',$person_limit);
    $m->set_data('facility_photo',$facility_photo);
    $m->set_data('facility_photo_2',$facility_photo_2);
    $m->set_data('facility_photo_3',$facility_photo_3);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('gst',$gst);
    $m->set_data('taxble_type',$taxble_type);
    $m->set_data('tax_slab',$tax_slab);
        if ($facility_type!=2) {
          if ($facility_amount<1 ) {
              $_SESSION['msg1']="Invalid Facility Amount ";
               header("Location: ../facility");
               exit();
          }
        }
         $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'balancesheet_id'=> $m->get_data('balancesheet_id'),
            'facility_name'=> $m->get_data('facility_name'),
            'facility_description'=> $m->get_data('facility_description'),
            'facility_address'=> $m->get_data('facility_address'),
            'facility_amount'=> $m->get_data('facility_amount'),
            'facility_amount_tenant'=> $m->get_data('facility_amount_tenant'),
            'facility_type'=> $m->get_data('facility_type'),
            'amount_type'=> $m->get_data('amount_type'),
            'booking_type'=> $m->get_data('booking_type'),
            'person_limit'=> $m->get_data('person_limit'),
            'facility_photo'=> $m->get_data('facility_photo'),
            'facility_photo_2'=> $m->get_data('facility_photo_2'),
            'facility_photo_3'=> $m->get_data('facility_photo_3'),
            'facility_added_date'=> date("Y-m-d H:i:s"),
            'is_taxble'=> $m->get_data('is_taxble'),
            'gst'=> $m->get_data('gst'),
            'taxble_type'=> $m->get_data('taxble_type'),
            'tax_slab'=> $m->get_data('tax_slab'),
        );
       

     
    $q=$d->insert("facilities_master",$a1);
    $facility_id = $con->insert_id;



    if($q==TRUE) {
       for ($i=0; $i <count($_POST['Sunday_opening_time']); $i++) { 
        if ($_POST['Sunday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Sunday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Sunday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Sunday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',0);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Sunday_status']);
        $aSunday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
       
          $d->insert("facility_schedule_master",$aSunday);
      }

      for ($i=0; $i <count($_POST['Monday_opening_time']); $i++) { 
        if ($_POST['Monday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Monday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Monday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Monday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',1);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Monday_status']);
        $aMondayo= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        $d->insert("facility_schedule_master",$aMondayo);
        
      }

      for ($i=0; $i <count($_POST['Tuesday_opening_time']); $i++) { 
        if ($_POST['Tuesday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Tuesday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Tuesday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Tuesday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',2);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Tuesday_status']);
        $aTuesday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        $d->insert("facility_schedule_master",$aTuesday);
        
      }
      for ($i=0; $i <count($_POST['Wednesday_opening_time']); $i++) { 
        if ($_POST['Wednesday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Wednesday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Wednesday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Wednesday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',3);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Wednesday_status']);
        $aWednesday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        $d->insert("facility_schedule_master",$aWednesday);
        
      }
      for ($i=0; $i <count($_POST['Thursday_opening_time']); $i++) { 
        if ($_POST['Thursday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Thursday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Thursday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Thursday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',4);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Thursday_status']);
        $aThursday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        $d->insert("facility_schedule_master",$aThursday);
        
      }
      for ($i=0; $i <count($_POST['Friday_opening_time']); $i++) { 
        if ($_POST['Friday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Friday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Friday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Friday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',5);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Friday_status']);
        $aFriday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        
        $d->insert("facility_schedule_master",$aFriday);
        
      }
      for ($i=0; $i <count($_POST['Saturday_opening_time']); $i++) { 
        if ($_POST['Saturday_opening_time'][$i] !='') {
          $opening_time = date('H:i:s',strtotime($_POST['Saturday_opening_time'][$i]));
        } else{
          $opening_time = '';
        }
        if ($_POST['Saturday_closing_time'][$i] !='') {
          $closing_time = date('H:i:s',strtotime($_POST['Saturday_closing_time'][$i]));
        }else{
          $closing_time = '';
        }
        $m->set_data('facility_id',$facility_id);
        $m->set_data('facility_day_id',6);
        $m->set_data('facility_start_time',$opening_time);
        $m->set_data('facility_end_time',$closing_time);
        $m->set_data('facility_status',$_POST['Saturday_status']);
        $aSaturday= array (
          'facility_id'=> $m->get_data('facility_id'),
          'facility_day_id'=> $m->get_data('facility_day_id'),
          'facility_start_time'=> $m->get_data('facility_start_time'),
          'facility_end_time'=> $m->get_data('facility_end_time'),
          'facility_status'=> $m->get_data('facility_status'),
        );
        $d->insert("facility_schedule_master",$aSaturday);
        
      }

      $_SESSION['msg']="Facility Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$facility_name Facility Added");
      header("Location: ../facilities");
    } else {
      $_SESSION['msg1']="Something Wrong ";
      header("Location: ../facilities");
    }
  
}



if(isset($updatefacilities)) {
  
        if ($facility_type!=2) {
          if ($facility_amount<1 ) {
              echo $_SESSION['msg1']="Invalid Facility Amount ";
               header("Location: ../facilities");
               exit();
          }
        }

      $counts = array_count_values($_POST['status']);
       if($counts['1']==7) {
           echo $_SESSION['msg1']="At Least Need 1 day to open facility";
          header("Location: ../facilities");
           exit();
       }

        if ($facility_type==1) {
          $booking_type=1;
        } else {
          $booking_type=0;
        }
    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['facility_photo']['tmp_name'];
      $ext = pathinfo($_FILES['facility_photo']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }

        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facilities");
              exit;
              break;
          }
          $facility_photo= $newFileName."_facility.".$ext;

        } else{
          $_SESSION['msg1']="Invalid Facility Photo";
          header("location:../facilities");
          exit();
        }
      } else {
        $facility_photo= $facility_photo_old;
      }

      if ($person_limit<1 && $facility_type!=2)  {
          $_SESSION['msg1']="Please set Person limit Greater Than 0";
          header("Location: ../facilities");
          exit();
      }


    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['facility_photo_2']['tmp_name'];
    $ext = pathinfo($_FILES['facility_photo_2']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }



        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility2.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facility");
              exit;
              break;
        }
        $facility_photo_2= $newFileName."_facility2.".$ext;
      } else{
        $_SESSION['msg1']="Invalid Facility Photo";
        header("location:../facility");
        exit();
      }
    } else {
      $facility_photo_2=$facility_photo_2_old;
    }


      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['facility_photo_3']['tmp_name'];
    $ext = pathinfo($_FILES['facility_photo_3']['name'], PATHINFO_EXTENSION);

    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/facility/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }



        switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_facility3.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Facility Photo";
              header("Location: ../facility");
              exit;
              break;
        }
        $facility_photo_3= $newFileName."_facility3.".$ext;
      } else{
        $_SESSION['msg1']="Invalid Facility Photo";
        header("location:../facility");
        exit();
      }
    } else {
      $facility_photo_3=$facility_photo_3_old;
    }

    if ($is_taxble=='1' && $gst==1) {
       $gst_amount_adult =  $facility_amount *$tax_slab/100;  //only for Exclude
       $facility_amount = $facility_amount + $gst_amount_adult;

       $gst_amount_adult_tenant =  $facility_amount_tenant *$tax_slab/100;  //only for Exclude
       $facility_amount_tenant = $facility_amount_tenant + $gst_amount_adult_tenant;
    }


    $m->set_data('society_id',$society_id);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('facility_name',$facility_name);
    $m->set_data('facility_amount',$facility_amount);
    $m->set_data('facility_amount_tenant',$facility_amount_tenant);
    $m->set_data('booking_type',$booking_type);
    $m->set_data('facility_type',$facility_type);
    $m->set_data('amount_type',$amount_type);
    $m->set_data('facility_description',$facility_description);
    $m->set_data('facility_address',$facility_address);
    $m->set_data('person_limit',$person_limit);
    $m->set_data('facility_photo',$facility_photo);
    $m->set_data('facility_photo_2',$facility_photo_2);
    $m->set_data('facility_photo_3',$facility_photo_3);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('gst',$gst);
    $m->set_data('taxble_type',$taxble_type);
    $m->set_data('tax_slab',$tax_slab);

         
          $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'balancesheet_id'=> $m->get_data('balancesheet_id'),
            'facility_name'=> $m->get_data('facility_name'),
            'facility_amount'=> $m->get_data('facility_amount'),
            'facility_amount_tenant'=> $m->get_data('facility_amount_tenant'),
            'facility_type'=> $m->get_data('facility_type'),
            'amount_type'=> $m->get_data('amount_type'),
            'booking_type'=> $m->get_data('booking_type'),
            'facility_description'=> $m->get_data('facility_description'),
            'facility_address'=> $m->get_data('facility_address'),
            'facility_photo'=> $m->get_data('facility_photo'),
            'facility_photo_2'=> $m->get_data('facility_photo_2'),
            'facility_photo_3'=> $m->get_data('facility_photo_3'),
            'person_limit'=> $m->get_data('person_limit'),
            'is_taxble'=> $m->get_data('is_taxble'),
            'gst'=> $m->get_data('gst'),
            'taxble_type'=> $m->get_data('taxble_type'),
            'tax_slab'=> $m->get_data('tax_slab'),
          ); 

       
    

    
    $schedule = explode(",",$_POST['scheduleString']);
    $schedule = array_filter($schedule);
    
    
    $q=$d->update("facilities_master",$a1,"facility_id='$facility_id' AND society_id='$society_id'");

    if($q==TRUE) {
      $_SESSION['msg']="Facility Updated";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$facility_name Facility Updated");
      header("Location: ../facilities");
    } else {
      header("Location: ../facilities");
    }
}


if(isset($deletedFacilityTimeSLot)) {

    $totalTimeSlot = $d->count_data_direct("facility_schedule_id","facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$facility_day_id'"); 
    if ($totalTimeSlot>1) {
      $q=$d->delete("facility_schedule_master","facility_id='$facility_id'  AND facility_schedule_id='$facility_schedule_id'");
    } else {
      $a2= array (
        'facility_start_time'=> '',
        'facility_end_time'=> '',
        'facility_status'=> 1,
      );
      $q=$d->update("facility_schedule_master",$a2,"facility_id='$facility_id'  AND facility_schedule_id='$facility_schedule_id'");
    }

    if($q==TRUE) {
      $_SESSION['msg']="Facility Timeslot Deleted";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Timeslot Deleted");
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    } else {
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    }


}

if (isset($addMoreTimeslotSingle)) {

    if ($opening_time=='' || $closing_time=='') {
      $_SESSION['msg1']="Please Enter Time";
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
      exit();
    }

    $totalTimeSlot = $d->count_data_direct("facility_schedule_id","facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$facility_day_id' AND facility_status=1 OR facility_id='$facility_id' AND facility_day_id='$facility_day_id' AND facility_start_time='00:00:00'"); 
    if ($totalTimeSlot==1) {
          $opening_time = date('H:i:s',strtotime($_POST['opening_time']));
          $closing_time = date('H:i:s',strtotime($_POST['closing_time']));
       $a2= array (
        'facility_start_time'=> $opening_time,
        'facility_end_time'=> $closing_time,
        'facility_status'=> 0,
      );
      $q=$d->update("facility_schedule_master",$a2,"facility_id='$facility_id'  AND facility_day_id='$facility_day_id'");
    } else {

      $opening_time = date('H:i:s',strtotime($_POST['opening_time']));
      $closing_time = date('H:i:s',strtotime($_POST['closing_time']));
      
      $a2= array (
        'facility_id'=> $facility_id,
        'facility_day_id'=> $facility_day_id,
        'facility_start_time'=> $opening_time,
        'facility_end_time'=> $closing_time,
        'facility_status'=> 0,
      );
      $q=$d->insert("facility_schedule_master",$a2);
    }


    if($q==TRUE) {
      $_SESSION['msg']="Facility Timeslot Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Timeslot Deleted");
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    } else {
       $_SESSION['msg1']="Something Wrong";
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    }

}


if (isset($UpdateMoreTimeslotSingle)) {

    if ($opening_time=='' || $closing_time=='') {
      $_SESSION['msg1']="Please Enter Time";
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
      exit();
    }

   
        $opening_time = date('H:i:s',strtotime($_POST['opening_time']));
          $closing_time = date('H:i:s',strtotime($_POST['closing_time']));
       $a2= array (
        'facility_start_time'=> $opening_time,
        'facility_end_time'=> $closing_time,
        'facility_status'=> 0,
      );
      $q=$d->update("facility_schedule_master",$a2,"facility_id='$facility_id'  AND facility_day_id='$facility_day_id' AND facility_schedule_id='$facility_schedule_id'");

   


    if($q==TRUE) {
      $_SESSION['msg']="Facility Timeslot Updated";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Timeslot Deleted");
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    } else {
       $_SESSION['msg1']="Something Wrong";
      header("Location: ../facility?manageWorkingHours=yes&facility_id=$facility_id");
    }

}


if(isset($bookFacility)) {

  $cq = $d->select("facilities_master","facility_id='$facility_id'");
  $cData = mysqli_fetch_array($cq);
  $person_count = $cData['person_count']+$no_of_person;

  if ($booking_start_time_days==$booking_end_time_days) {
    $_SESSION['msg1']= "Start Date & End Date Is Same";
    header("Location: ../facilityBook?id=$facility_id");
    exit();
  }
  $facility_amount= $cData['facility_amount'];
  if($facility_type==0 || $facility_type==3){
    // Day Wise
    $booked_date = $booked_date;
    $booking_start_time = $booking_start_time_days;
    $booking_end_time = $booking_end_time_days;
    // $effectiveDate = strtotime("+$no_of_days days", strtotime($booked_date)); // returns timestamp
    $booking_expire_date=$booked_date; // formatted version
    if ($amount_type==0) {
      $facility_amount= $facility_amount*$no_of_person;
    } else {
      $facility_amount= $facility_amount;
    }
  } else {

    $i11=1;
    $month = strtotime($booked_date);
    $montNameAry=array();
    while($i11 <= 12)
    {
        $month_name = date('F', $month);
        $year_name = date('Y', $month);
        $fullName= $month_name.'-'.$year_name;
        array_push($montNameAry, $fullName);
        $month = strtotime('+1 month', $month);
        $i11++;
    }

    $key = array_search($_POST['booking_month'], $montNameAry); // $key = 2;
    $no_of_month =$key+1;

    $booking_start_time = $booking_start_time_days;
    $booking_end_time = $booking_end_time_days;
    $getLastMont= $no_of_month-1;
    $lastValue=  $_POST['booking_month'];
    $booking_expire_date = $lastValue;
    $lastday = date('t',strtotime($booking_expire_date));
    $booking_expire_date= $lastday.' '.$booking_expire_date;
    $booking_expire_date=date('Y-m-d',strtotime($booking_expire_date));
     if ($no_of_month==0) {
        $_SESSION['msg1']= "Minimum 1 Month Required";
        header("Location: ../facilityBook?id=$facility_id");
        exit();
      }
  } 


    $ref_no="REF".date('Ymd').$unit_id.$facility_id;
    $payment_status = 0;
    $payment_received_date= date('Y-m-d H:i:s');



  $uq=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND unit_master.block_id=block_master.block_id AND users_master.user_id='$user_id' AND unit_master.society_id='$society_id'");
  $unitData=mysqli_fetch_array($uq);
  $unitName=  $unitData['block_name']."-".$unitData['unit_name'];
  $userType=  $unitData['user_type'];
  $unit_id_main=  $unitData['unit_id'];

  if ($facility_amount<$facility_amount_org) {
    $_SESSION['msg1']= "Invalid Booking Amount !";
    header("Location: ../facilityBook?id=$facility_id");
    exit();
  }
  $facility_name = html_entity_decode($facility_name);
  if ($facility_amount>$facility_amount_org_wallet) {
      $credit_amount = $facility_amount -$facility_amount_org_wallet;
      $qqq=$d->selectRow("user_mobile","users_master","user_id='$user_id' ");
        $userData= mysqli_fetch_array($qqq);
        $user_mobile = $userData['user_mobile'];
        $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
        $row=mysqli_fetch_array($count6);
        $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

        $remark= "Overpaid Payment of Facility Booking $facility_name";      

        $m->set_data('society_id',$society_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('unit_id',$unit_id);
        $m->set_data('remark',$remark);
        $m->set_data('credit_amount',$credit_amount);
        $m->set_data('avl_balance',$totalDebitAmount1+$credit_amount);


        $aWallet = array(
          'society_id'=>$m->get_data('society_id'),
          'user_mobile'=>$m->get_data('user_mobile'),
          'unit_id'=>$m->get_data('unit_id'),
          'remark'=>$m->get_data('remark'),
          'credit_amount'=>$m->get_data('credit_amount'),
          'avl_balance'=>$m->get_data('avl_balance'),
          'created_date'=>date("Y-m-d H:i:s"), 
          'admin_id'=>$_COOKIE['bms_admin_id'],
        );
        
       $wallet_amount_type = 0;
       $wallet_amount = $credit_amount;
       $mailRemark = "& $credit_amount Credited in wallet";
       
       $facility_amount = $facility_amount-$credit_amount;
    }

    

  $m->set_data('facility_id',$facility_id);
  $m->set_data('balancesheet_id',$balancesheet_id);
  $m->set_data('receive_amount',$facility_amount);
  $m->set_data('unit_name',$unitName);
  $m->set_data('unit_id',$unit_id_main);
  $m->set_data('booked_date',$booked_date);
  $m->set_data('booking_start_time',$booking_start_time);
  $m->set_data('booking_end_time',$booking_end_time);
  $m->set_data('payment_type',$payment_type);
  $m->set_data('payment_status',$payment_status);
  $m->set_data('payment_bank',$payment_bank);
  $m->set_data('payment_number',$payment_number);
  $m->set_data('payment_received_date', $payment_received_date);
  $m->set_data('no_of_person',$no_of_person);
  $m->set_data('booking_expire_date',$booking_expire_date);
  $m->set_data('no_of_month',$no_of_month);
  $m->set_data('user_id',$user_id);
  $m->set_data('userType',$userType);
  $m->set_data('booking_hours_type',$amount_type);
  $m->set_data('wallet_amount',$wallet_amount);
  $m->set_data('wallet_amount_type',$wallet_amount_type);

   $a1= array (
      'society_id'=> $society_id,
      'balancesheet_id'=> $balancesheet_id,
      'facility_id'=> $m->get_data('facility_id'),
      'receive_amount'=> $m->get_data('receive_amount'),
      'unit_name'=> $m->get_data('unit_name'),
      'unit_id'=> $m->get_data('unit_id'),
      'booked_date'=> $m->get_data('booked_date'),
      'booking_start_time'=> $m->get_data('booking_start_time'),
      'booking_end_time'=> $m->get_data('booking_end_time'),
      'book_request_date'=> $booked_date,
      'payment_type'=> $m->get_data('payment_type'),
      'payment_status'=> $m->get_data('payment_status'),
      'bank_name'=> $m->get_data('payment_bank'),
      'payment_ref_no'=> $m->get_data('payment_number'),
      'payment_received_date'=> $m->get_data('payment_received_date'),
      'no_of_person'=> $m->get_data('no_of_person'),
      'book_status'=>'1',
      'booking_expire_date'=> $m->get_data('booking_expire_date'),
      'no_of_month'=> $m->get_data('no_of_month'),
      'user_id'=> $m->get_data('user_id'),
      'userType'=> $m->get_data('userType'),
      'booking_hours_type'=> $m->get_data('booking_hours_type'),
      'wallet_amount'=> $m->get_data('wallet_amount'),
      'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
  );

  

  $q = $d->insert("facilitybooking_master",$a1);
  $booking_id = $con->insert_id;
  if($q==TRUE) {

     if ($wallet_amount>0) {
          $wq=$d->insert("user_wallet_master",$aWallet);
      }
      
    if($facility_type==1){
      for ($i=0; $i <$no_of_month ; $i++) { 
        $m->set_data('month_name',$montNameAry[$i]);

         $aMonth= array (
          'society_id'=> $society_id,
          'booking_id'=> $booking_id,
          'facility_id'=> $m->get_data('facility_id'),
          'unit_id'=> $m->get_data('unit_id'),
          'no_of_person'=> $m->get_data('no_of_person'),
          'month_name'=> $m->get_data('month_name'),
          'booking_start_time'=> $m->get_data('booking_start_time'),
          'booking_end_time'=> $m->get_data('booking_end_time'),
        );
         $d->insert("facility_booking_months",$aMonth);
      }

    }

    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=users_master.block_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id' ");
    $data_notification=mysqli_fetch_array($qUserToken);
    
    $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floor_id='$data_notification[floor_id]'");
    $floorData=mysqli_fetch_array($qf);
    
    $sos_user_token=$data_notification['user_token'];
    $user_mobile=$data_notification['user_mobile'];
    $user_email=$data_notification['user_email'];
    $user_first_name=$data_notification['user_full_name'];
    $unit_name=$floorData['floor_name'] . " (" . $floorData['block_name'] .")";
    $device=$data_notification['device'];
      if ($device=='android') {
         $nResident->noti("FacilityFragment","",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin $created_by ",'facility');
      }  else if($device=='ios') {
        $nResident->noti_ios("FacilityMainTabVC","",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin $created_by",'facility');
      }
      // $nResident->noti("",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin ".date('Y-m-d H:i'),'facility');

      $notiAry = array(
      'society_id'=>$society_id,
      'user_id'=>$data_notification['user_id'],
      'notification_title'=>$facility_name.' Facility Booked',
      'notification_desc'=>"by Admin $created_by ",    
      'notification_date'=>date('Y-m-d H:i'),
      'notification_action'=>'facility',
       'notification_logo'=>'Facilitiesxxxhdpi.png',
      );
      $d->insert("user_notification",$notiAry);
     
      
    $_SESSION['msg']= "Booking Confirm";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Booking Confirm");
    if ($user_email!='' && $payment_type != 4) {
      $to=$user_email;
      $invoice_number= "INVFAC".$booking_id;
      $paid_by= $user_first_name;
      $received_by= $created_by;
      $subject=$facility_name. ' Facility Booking Payment Acknowledgement #'.$invoice_number;
      if ($facility_type==0) {
        $description=$facility_name."  Booking Date : $booked_date ". $mailRemark;
      }else if ($facility_type==1) {
        $description=$facility_name." Booking Date : $booked_date to $booking_expire_date for $noPerson Person ". $mailRemark;
      }else if ($facility_type==3) {
        $description=$facility_name." (Booking Date : $booked_date Time: ". $booking_start_time.' - '. $booking_end_time.')'. $mailRemark;
      } else {
        $description=$facility_name." Booking Date : $booked_date  ". $mailRemark;
      }
      $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id_main&type=Fac&societyid=$society_id&id=$booking_id&facility_id=$facility_id";
      $received_amount=number_format($facility_amount,2,'.','');
      $receive_date = $payment_received_date;
      $category = "Facility";
      $txt_status= "Success";
      $txt_id = "NA";
      switch ($payment_type) {
        case '1':
          $payment_mode = 'Cheque';
          break;
        case '2':
          $payment_mode = 'Online Payment';
          break;
        default:
          $payment_mode = 'Cash';
          break;
      }


      include '../mail/paymentReceipt.php';
      include '../mail.php';
      // echo $message;
    }
    // exit();
    header("Location: ../facilityBook?id=$facility_id");
  } else {
    $_SESSION['msg1']= "Something Wrong";
    header("Location: ../facilityBook?id=$facility_id");
  }
}


if(isset($bookFacilityNew)) {
  $cq = $d->select("facilities_master","facility_id='$facility_id'");
  $cData = mysqli_fetch_array($cq);
  $person_limit = $cData['person_limit'];

  /*if (count($_POST['facility_schedule_id'])==0 && $facility_type!=1) {
    $_SESSION['msg1']= "Select At Least 1 Time Slot";
    header("Location: ../facilityBook?id=$facility_id");
    exit();
  }*/
// echo "pass";exit;

   if($user_type==1 && $cData['facility_amount_tenant']>0) {
      $facility_amount_db = $cData['facility_amount_tenant'];
   } else {
      $facility_amount_db = $cData['facility_amount'];
   }

   if($cData['amount_type']==0) {
      $facility_amount_db = ($facility_amount_db * $no_of_person);
   } else {
      $facility_amount_db = $facility_amount_db;
   } 



    $totalNewPrice = 0;
  if($facility_type==0 || $facility_type==3 || $facility_type==4){
    $ids = join("','",$_POST['facility_schedule_id']); 
    $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND  facility_schedule_id IN ('$ids')","");
    while ($sData= mysqli_fetch_array($sq)) {

      $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]' AND booking_start='$booked_date'");
      $row11=mysqli_fetch_array($count5);
      $booke_count=$row11['SUM(no_of_person)'];
      if ($booke_count=='') {
        $booke_count =0;
      }
      $avSeat = $person_limit - $booke_count;
      if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) {
        $_SESSION['msg1']= "During the transaction booking full !";
        header("Location: ../facilityBook?id=$facility_id");
        exit();
        }


      $time1 = strtotime($sData["facility_start_time"]);
      $time2 = strtotime($sData["facility_end_time"]);
      $difference_hours = abs($time2 - $time1) / 3600;
      if($difference_hours <1) {
         $difference_hours =1;
      }
      $difference_hours;
      if($facility_type==3) {
        $facilityDb  =$facility_amount_db*$difference_hours;
        $totalNewPrice+=$facilityDb;
      } else if($facility_type==4) {
        $facilityDb  =$facility_amount_db;
        $totalNewPrice+=$facilityDb;
      } else {
        $facility_amount_db  =$facility_amount_db;
        $totalNewPrice=$facility_amount_db;
      }


    }

   


    // Day Wise
    $booked_date = $booked_date;
    $booking_start_time = $booking_start_time_days;
    $booking_end_time = $booking_end_time_days;
    // $effectiveDate = strtotime("+$no_of_days days", strtotime($booked_date)); // returns timestamp
    $booking_expire_date=$booked_date; // formatted version
    
  } else {

    $no_of_month = count($_POST['booking_month']);
    $totalNewPrice=$facility_amount_db*$no_of_month;
    $lastBookMonth=  $_POST['booking_month'][$no_of_month-1];
    $firstBookMonth=  $_POST['booking_month'][0];
    $booked_date_check_temp = date("F-Y", strtotime($booked_date));
    $booking_expire_date = date("Y-m-t", strtotime($lastBookMonth));
    //Month Wise
    if ($firstBookMonth!=$booked_date_check_temp) {
       $booked_date =  date("Y-m-1", strtotime($firstBookMonth));
    }
   
    for ($i1check=0; $i1check <count($_POST['booking_month']) ; $i1check++) { 
      $monthName = $_POST['booking_month'][$i1check];
      $count5=$d->sum_data("no_of_person","facility_booking_months","(cancel_status=0 AND facility_id='$facility_id' AND month_name='$monthName')");
      $row11=mysqli_fetch_array($count5);
      $booke_count=$row11['SUM(no_of_person)'];
      if ($booke_count=='') {
        $booke_count =0;
      }
      $avSeat = $person_limit - $booke_count;
      if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) {
        $_SESSION['msg1']= "During the transaction booking full !";
        header("Location: ../facilityBook?id=$facility_id");
        exit();
      }
    }
    // $booking_expire_date=date('Y-m-d',strtotime($booking_end_time));

     if ($no_of_month==0) {
        $_SESSION['msg1']= "Minimum 1 Month Required";
        header("Location: ../facilityBook?id=$facility_id");
        exit();
      }
  }

  $ref_no="REF".date('Ymd').$unit_id.$facility_id;
  $payment_status = 0;
  $payment_received_date= date('Y-m-d H:i:s');


  $uq=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND unit_master.block_id=block_master.block_id AND users_master.user_id='$user_id' AND unit_master.society_id='$society_id'");
  $unitData=mysqli_fetch_array($uq);
  $unitName=  $unitData['block_name']."-".$unitData['unit_name'];
  $userType=  $unitData['user_type'];
  $unit_id_main=  $unitData['unit_id'];
  if ($facility_amount<$facility_amount_org) {
    $_SESSION['msg1']= "Invalid Booking Amount !";
    header("Location: ../facilityBook?id=$facility_id");
    exit();
  }



  $facility_name = html_entity_decode($facility_name);
  if ($facility_amount>$facility_amount_org_wallet) {
      $credit_amount = $facility_amount -$facility_amount_org_wallet;
      $qqq=$d->selectRow("user_mobile","users_master","user_id='$user_id' ");
        $userData= mysqli_fetch_array($qqq);
        $user_mobile = $userData['user_mobile'];
        $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
        $row=mysqli_fetch_array($count6);
        $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

        $remark= "Overpaid Payment of Facility Booking $facility_name";      

        $m->set_data('society_id',$society_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('unit_id',$unit_id);
        $m->set_data('remark',$remark);
        $m->set_data('credit_amount',$credit_amount);
        $m->set_data('avl_balance',$totalDebitAmount1+$credit_amount);


        $aWallet = array(
          'society_id'=>$m->get_data('society_id'),
          'user_mobile'=>$m->get_data('user_mobile'),
          'unit_id'=>$m->get_data('unit_id'),
          'remark'=>$m->get_data('remark'),
          'credit_amount'=>$m->get_data('credit_amount'),
          'avl_balance'=>$m->get_data('avl_balance'),
          'created_date'=>date("Y-m-d H:i:s"), 
          'admin_id'=>$_COOKIE['bms_admin_id'],
        );
        
       $wallet_amount_type = 0;
       $wallet_amount = $credit_amount;
       $mailRemark = "& $credit_amount Credited in wallet";
       
       $facility_amount = $facility_amount-$credit_amount;
    }

    
  $facility_amount = $totalNewPrice;

  $m->set_data('facility_id',$facility_id);
  $m->set_data('balancesheet_id',$balancesheet_id);
  $m->set_data('receive_amount',$facility_amount);
  $m->set_data('unit_name',$unitName);
  $m->set_data('unit_id',$unit_id_main);
  $m->set_data('booked_date',$booked_date);
  $m->set_data('booking_start_time',$booking_start_time);
  $m->set_data('booking_end_time',$booking_end_time);
  $m->set_data('payment_type',$payment_type);
  $m->set_data('payment_status',$payment_status);
  $m->set_data('bank_name',$bank_name);
  $m->set_data('payment_ref_no',$payment_ref_no);
  $m->set_data('payment_received_date', $payment_received_date);
  $m->set_data('no_of_person',$no_of_person);
  $m->set_data('booking_expire_date',$booking_expire_date);
  $m->set_data('no_of_month',$no_of_month);
  $m->set_data('user_id',$user_id);
  $m->set_data('userType',$userType);
  $m->set_data('booking_hours_type',$amount_type);
  $m->set_data('wallet_amount',$wallet_amount);
  $m->set_data('wallet_amount_type',$wallet_amount_type);

  $a1 = array (
      'society_id'=> $society_id,
      'balancesheet_id'=> $balancesheet_id,
      'facility_id'=> $m->get_data('facility_id'),
      'receive_amount'=> $m->get_data('receive_amount'),
      'unit_name'=> $m->get_data('unit_name'),
      'unit_id'=> $m->get_data('unit_id'),
      'booked_date'=> $m->get_data('booked_date'),
      'booking_start_time'=> $m->get_data('booking_start_time'),
      'booking_end_time'=> $m->get_data('booking_end_time'),
      'book_request_date'=> $booked_date,
      'payment_type'=> $m->get_data('payment_type'),
      'payment_status'=> $m->get_data('payment_status'),
      'bank_name'=> $m->get_data('bank_name'),
      'payment_ref_no'=> $m->get_data('payment_ref_no'),
      'payment_received_date'=> $m->get_data('payment_received_date'),
      'no_of_person'=> $m->get_data('no_of_person'),
      'book_status'=>'1',
      'booking_expire_date'=> $m->get_data('booking_expire_date'),
      'no_of_month'=> $m->get_data('no_of_month'),
      'user_id'=> $m->get_data('user_id'),
      'userType'=> $m->get_data('userType'),
      'booking_hours_type'=> $m->get_data('booking_hours_type'),
      'wallet_amount'=> $m->get_data('wallet_amount'),
      'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
  );
  

  $q = $d->insert("facilitybooking_master",$a1);
  $booking_id = $con->insert_id;
  if($q==TRUE) {

     if ($wallet_amount>0) {
          $wq=$d->insert("user_wallet_master",$aWallet);
      }
      
    if($facility_type==1){
      for ($i=0; $i <$no_of_month ; $i++) { 
        $m->set_data('month_name',$_POST['booking_month'][$i]);

         $aMonth= array (
          'society_id'=> $society_id,
          'booking_id'=> $booking_id,
          'facility_id'=> $m->get_data('facility_id'),
          'unit_id'=> $m->get_data('unit_id'),
          'no_of_person'=> $m->get_data('no_of_person'),
          'month_name'=> $m->get_data('month_name'),
          'booking_start  '=> $m->get_data('booked_date'),
          'booking_end'=> $m->get_data('booking_expire_date'),
        );
         $d->insert("facility_booking_months",$aMonth);
      }

    } else {
       for ($i1=0; $i1 <count($_POST['facility_schedule_id']) ; $i1++) { 
        $facility_schedule_id = $_POST['facility_schedule_id'][$i1];
        $m->set_data('facility_schedule_id',$facility_schedule_id);
        
        $sq1 = $d->select("facility_schedule_master","facility_schedule_id='$facility_schedule_id' ","");
        $sData= mysqli_fetch_array($sq1);
        $facility_start_time = $sData['facility_start_time'];
        $facility_end_time = $sData['facility_end_time'];

         $aMonth= array (
          'society_id'=> $society_id,
          'booking_id'=> $booking_id,
          'facility_id'=> $m->get_data('facility_id'),
          'unit_id'=> $m->get_data('unit_id'),
          'no_of_person'=> $m->get_data('no_of_person'),
          'month_name'=> 'Time Slot',
          'booking_start_time'=> $facility_start_time,
          'booking_end_time'=> $facility_end_time,
          'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
          'booking_start'=> $m->get_data('booked_date'),
          'booking_end'=> $m->get_data('booking_expire_date'),
        );
         $d->insert("facility_booking_months",$aMonth);
      }
    }


    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=users_master.block_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id' ");
    $data_notification=mysqli_fetch_array($qUserToken);
    
    $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floor_id='$data_notification[floor_id]'");
    $floorData=mysqli_fetch_array($qf);

    $sos_user_token=$data_notification['user_token'];
    $user_mobile=$data_notification['user_mobile'];
    $user_email=$data_notification['user_email'];
    $user_first_name=$data_notification['user_full_name'];
    $unit_name=$floorData['floor_name'] . " (" . $floorData['block_name'] .")";
    $device=$data_notification['device'];
      if ($device=='android') {
         $nResident->noti("FacilityFragment","",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin $created_by ",'facility');
      }  else if($device=='ios') {
        $nResident->noti_ios("FacilityMainTabVC","",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin $created_by",'facility');
      }
      // $nResident->noti("",$society_id,$sos_user_token,"$facility_name Facility Booked","by Admin ".date('Y-m-d H:i'),'facility');

      $notiAry = array(
      'society_id'=>$society_id,
      'user_id'=>$data_notification['user_id'],
      'notification_title'=>$facility_name.' Facility Booked',
      'notification_desc'=>"by Admin $created_by ",    
      'notification_date'=>date('Y-m-d H:i'),
      'notification_action'=>'facility',
       'notification_logo'=>'Facilitiesxxxhdpi.png',
      );
      $d->insert("user_notification",$notiAry);
      
      
    $_SESSION['msg']= "Booking Confirm";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Booking Confirm");
    if ($user_email!='' && $payment_type != 4) {
      $to=$user_email;
      $invoice_number= "INVFAC".$booking_id;
      $paid_by= $user_first_name;
      $received_by= $created_by;
      $subject=$facility_name. ' Facility Booking Payment Acknowledgement #'.$invoice_number;
      if ($facility_type==0) {
        $description=$facility_name."  Booking Date : $booked_date ". $mailRemark;
      }else if ($facility_type==1) {
        $description=$facility_name." Booking Date : $booked_date to $booking_expire_date for $noPerson Person ". $mailRemark;
      }else if ($facility_type==3) {
        $description=$facility_name." (Booking Date : $booked_date Time: ". $booking_start_time.' - '. $booking_end_time.')'. $mailRemark;
      } else {
        $description=$facility_name." Booking Date : $booked_date  ". $mailRemark;
      }
      $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id_main&type=Fac&societyid=$society_id&id=$booking_id&facility_id=$facility_id";
      $received_amount=number_format($facility_amount,2,'.','');
      $receive_date = $payment_received_date;
      $category = "Facility";
      $txt_status= "Success";
      $txt_id = "NA";
      switch ($payment_type) {
        case '1':
          $payment_mode = 'Cheque';
          break;
        case '2':
          $payment_mode = 'Online Payment';
          break;
        default:
          $payment_mode = 'Cash';
          break;
      }


      include '../mail/paymentReceipt.php';
      include '../mail.php';
      // echo $message;
    }
    // exit();
    header("Location: ../facilityBook?id=$facility_id");
  } else {
    $_SESSION['msg1']= "Something Wrong";
    header("Location: ../facilityBook?id=$facility_id");
  }
}

if(isset($_POST['approved'])) {

      $booked_date = date('Y-m-d');

      $cq = $d->select("facilities_master","facility_id='$facility_id'");
      $cData = mysqli_fetch_array($cq);
      $person_count = $cData['person_count']+1;
      $m->set_data('book_status',$book_status);


        $a = array ('booked_date'=> $booked_date,
          'book_status'=> $m->get_data('book_status')
        );
        $a2 = array (
          'person_count'=>$person_count
        );
        $q1 =$d->update("facilities_master",$a2,"facility_id='$facility_id' AND society_id='$society_id'");
        $q=$d->update("facilitybooking_master",$a,"facility_id='$facility_id' AND society_id='$society_id' AND unit_id='$unit_id'");
        if($q>0 && $q1) {

                    $qUserToken=$d->select("users_master","delete_status=0 AND  society_id='$society_id' AND unit_id='$unit_id'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    if ($device=='android') {
                      $nResident->noti("FacilityMainTabVC","",$society_id,$sos_user_token,"Facility Booking Request","Approved by Admin",'facility');
                    }  else if($device=='ios') {
                      $nResident->noti_ios("FacilityFragment","",$society_id,$sos_user_token,"Facility Booking Request","Approved by Admin",'facility');
                    }
                    

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$data_notification['user_id'],
                      'notification_title'=>'Facility Booking Request',
                      'notification_desc'=>"Approved by Admin",    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'facility',
                      'notification_logo'=>'Facilitiesxxxhdpi.png',
                    );
                    $d->insert("user_notification",$notiAry);


            $_SESSION['msg']="Request is Approved";
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Booking Request is Approved");

            header("location:../facilityBook?id=$facility_id");
        }
        else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../facilityBook?id=$facility_id");
        }
}

if(isset($deleteFacilityReq)) {
      $cq = $d->select("facilities_master","facility_id='$facility_id'");
      $cData = mysqli_fetch_array($cq);
      $person_count = $cData['person_count']-1;
      $a2 = array (
          'person_count'=>$person_count
        );
      $q=$d->delete("facilitybooking_master","booking_id='$booking_id'");
      $q1 =$d->update("facilities_master",$a2,"facility_id='$facility_id' AND society_id='$society_id'");
     if($q>0 && $q1) {
                    $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND unit_id='$unit_id'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    if ($device=='android') {
                        $nResident->noti("",$society_id,$sos_user_token,"Facility Booking Request ","Rejected by Admin",'facility');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("",$society_id,$sos_user_token,"Facility Booking Request ","Rejected by Admin",'facility');
                    }
                   

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$data_notification['user_id'],
                      'notification_title'=>'Facility Booking Request',
                      'notification_desc'=>"Rejected by Admin",    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'facility',
                      'notification_logo'=>'Facilitiesxxxhdpi.png',
                    );
                    $d->insert("user_notification",$notiAry);

            $_SESSION['msg']="Request is Deleted";
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility Booking Request is Rejected");
            header("location:../facilityBook?id=$facility_id");
        }
        else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../facilityBook?id=$facility_id");
        }
}

if(isset($cancelFacilityReq)) {
      $cq = $d->select("facilities_master","facility_id='$facility_id'");
      $cData = mysqli_fetch_array($cq);
      if ($refund_type==1) {
        $expenses_title  = $expenses_title ." (Refund in User Wallet)";
      }
      $a1 = array (
          'book_status'=>3,
          'refund_amount'=>$expenses_amount,
          'refund_remark'=>$expenses_title,
          'refund_by'=>$created_by,
        );
      
       
        $a3 = array (
          'cancel_status'=>1
        );

      $q=$d->update("facilitybooking_master",$a1,"booking_id='$booking_id'");
      $q1=$d->update("facility_booking_months",$a3,"booking_id='$booking_id'");
     if($q>0) {
      $facility_name = html_entity_decode($facility_name);

            $expenses_add_date = date('Y-m-d h:i:s');
            $expDateAry = explode("-", $expenses_add_date);
            $exYear = $expDateAry[0];
            $exMonth = $expDateAry[1];
            $dateObj   = DateTime::createFromFormat('!m', $exMonth);
            $monthName = $dateObj->format('F'); 
            $expenses_created_date = $monthName.'-'.$exYear;

            $m->set_data('expenses_amount',$expenses_amount);
            $m->set_data('expenses_add_date',$expenses_add_date);
            $m->set_data('balancesheet_id',$balancesheet_id);
            $m->set_data('expenses_created_date',$expenses_created_date);
            //IS_1686
            
            $m->set_data('expenses_title',$facility_name.' Facility Cancelation Invoice Number : INVFAC'.$booking_id. ' '.$expenses_title);
            $m->set_data('expenses_amount',$expenses_amount);

            
            $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND user_token!=''");
            $data_notification=mysqli_fetch_array($qUserToken);

            //IS_1686
            
              $aEx = array(
                'expenses_title'=>$m->get_data('expenses_title'),
                'expenses_amount'=>$m->get_data('expenses_amount'),
                'expenses_add_date'=>$m->get_data('expenses_add_date'),
                'balancesheet_id'=>$m->get_data('balancesheet_id'),
                'society_id'=>$society_id,
                'expenses_created_date'=>$m->get_data('expenses_created_date'),
                'amount_without_gst'=>$m->get_data('expenses_amount'), 
              );
                  
              $q = $d->insert("expenses_balance_sheet_master",$aEx);

            if ($refund_type==1) {
            
                $user_mobile=$data_notification['user_mobile'];
                $unit_id=$data_notification['unit_id'];

                $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
                $row=mysqli_fetch_array($count6);
                $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

                $remark = "$facility_name Facility Booking Cancel";
              
                $m->set_data('society_id',$society_id);
                $m->set_data('user_mobile',$user_mobile);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('remark',$remark);
                $m->set_data('credit_amount',$expenses_amount);
                $m->set_data('avl_balance',$totalDebitAmount1+$expenses_amount);


                $aEx = array(
                  'society_id'=>$m->get_data('society_id'),
                  'user_mobile'=>$m->get_data('user_mobile'),
                  'unit_id'=>$m->get_data('unit_id'),
                  'remark'=>$m->get_data('remark'),
                  'credit_amount'=>$m->get_data('credit_amount'),
                  'avl_balance'=>$m->get_data('avl_balance'),
                  'created_date'=>date("Y-m-d H:i:s"), 
                   'admin_id'=>$_COOKIE['bms_admin_id'],
                );

                 // print_r($a1);
                $d->insert("user_wallet_master",$aEx);

            }

           
            $sos_user_token=$data_notification['user_token'];
            $user_mobile=$data_notification['user_mobile'];
            $user_first_name=$data_notification['user_first_name'];
            $device=$data_notification['device'];
             $to=$data_notification['user_email'];
            $title= "Facility booking $facility_name cancelled";
            if ($device=='android') {
              $nResident->noti("FacilityFragment","",$society_id,$sos_user_token,$title,"By Admin $created_by",'facility');
            }  else if($device=='ios') {
              $nResident->noti_ios("FacilityMainTabVC","",$society_id,$sos_user_token,$title,"By Admin $created_by",'facility');
            }

            $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$data_notification['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>"By Admin $created_by",    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'facility',
              'notification_logo'=>'Facilitiesxxxhdpi.png'  
            );
            $d->insert("user_notification",$notiAry);

             if ($to!='') {
              $subject=$facility_name. ' Facility Booking  Cancelled';
              $message= "Hi $user_first_name\n$title\nBy Admin $created_by";
              include '../mail.php';
            }


            

          $_SESSION['msg']="Facility Booking Cancelled Successfully";
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Facility($facility_name) Booking Cancelled for $user_first_name");
            header("location:../facilityBook?id=$facility_id");
        }
        else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../facilityBook?id=$facility_id");
        }
}


if(isset($_POST['payRemAmount'])) {

          $payment_received_date= date('Y-m-d h:i:sa');
          $q5=$d->select("balancesheet_master","balancesheet_id='$balancesheet_id'");
          $data_balanceshee_list=mysqli_fetch_array($q5);
          $amount=$data_balanceshee_list['current_balance'];
          $amount=$amount+$receive_amount;
          $m->set_data('current_balance',$amount);
          $arrayName = array('current_balance'=>$m->get_data('current_balance'));
          // print_r($arrayName);
          $d->update("balancesheet_master",$arrayName,"balancesheet_id='$balancesheet_id'");

          $m->set_data('payment_received_date',$payment_received_date);
          $m->set_data('payment_type',$payment_type);
          $m->set_data('payment_bank',$payment_bank);
          $m->set_data('payment_number',$payment_number);
          $m->set_data('payment_status',0);

           $a1= array (
            
            'payment_type'=> $m->get_data('payment_type'),
            'bank_name'=> $m->get_data('payment_bank'),
            'payment_ref_no'=> $m->get_data('payment_number'),
            'payment_status'=> $m->get_data('payment_status'),
            'payment_received_date'=> $m->get_data('payment_received_date'),
          );
     
           $q=$d->update("facilitybooking_master",$a1,"booking_id='$booking_id'");

            if($q==TRUE) {

            $_SESSION['msg']= "Payment Received";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Received");
            header("Location: ../facilityBook?id=$facility_id");
            } else {
              $_SESSION['msg1']= "Something Wrong";
              header("Location: ../facilityBook?id=$facility_id");
            }
}

?>