<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{

  // add main menu
  if(isset($_POST['addSubDeptMulti'])){

    $q1 = $d->selectRow("block_id", "floors_master","society_id='$society_id' AND floor_id = '$floor_id'");
    $data1 = mysqli_fetch_array($q1);
    $block_id = $data1['block_id'];
  
   $sub_department_name = implode("~", $_POST['sub_department_name']);
   $sub_department_name1= explode('~', (string)$sub_department_name);
   $tt =1;
   for ($i=0; $i <$no_of_sub_dept ; $i++) { 
    
      $m->set_data('society_id',$society_id);
      $m->set_data('block_id',$block_id);
      $m->set_data('floor_id',$floor_id);
      $m->set_data('sub_department_name',$sub_department_name1[$i]);
      $m->set_data('sub_department_sort',$i+1);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_id'=> $m->get_data('block_id'),
        'floor_id'=> $m->get_data('floor_id'),
        'sub_department_name'=> $m->get_data('sub_department_name'),
        'sub_department_sort'=> $m->get_data('sub_department_sort'),
      );
      $q=$d->insert("sub_department",$a);
     
    }
      if($q>0) {
        $_SESSION['msg']="Sub Department Added Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Sub Department Added");
        header("location:../subDepartments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../subDepartments");
      }
  }
  
  if(isset($_POST['addSingleSubDept'])){
    
    $q1 = $d->select("sub_department","society_id='$society_id' AND floor_id = '$floor_id'","ORDER BY sub_department_sort DESC");
    $data1 = mysqli_fetch_array($q1);
    $nextShortNumber = $data1['sub_department_sort']+1;

      
      $m->set_data('society_id',$society_id);
      $m->set_data('block_id',$block_id);
      $m->set_data('floor_id',$floor_id);
      $m->set_data('sub_department_name',$sub_department_name);
      $m->set_data('sub_department_sort',$nextShortNumber);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_id'=> $m->get_data('block_id'),
        'floor_id'=> $m->get_data('floor_id'),
        'sub_department_name'=> $m->get_data('sub_department_name'),
        'sub_department_sort'=> $m->get_data('sub_department_sort'),
      );

      //print_r($a);die;
      $q=$d->insert("sub_department",$a);

      if($q>0) {
        $_SESSION['msg']="Sub Department Added Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Sub Department Added");

        header("location:../subDepartments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../subDepartments");
      }
  }


  if(isset($deleteSubDept)) {
     $q=$d->delete("sub_department","sub_department_id='$deleteSubDept' AND society_id='$society_id'");
     //$q=$d->delete("unit_master","floor_id='$deleteFloor' AND society_id='$society_id'");
     //$q=$d->delete("users_master","floor_id='$deleteFloor' AND society_id='$society_id'");
     if($q>0) {
      echo 1;
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Sub Department Deleted");

     } else {
      echo 0;
     }
  }
  if(isset($updateSubDept)) {
          $m->set_data('sub_department_name',$sub_department_name);
          
          $a5 =array(
            'sub_department_name'=> $m->get_data('sub_department_name'),
          );

     $q=$d->update("sub_department",$a5 ,"sub_department_id='$sub_department_id' AND society_id='$society_id'");
      if($q>0) {
        $_SESSION['msg']="Sub Department Name Updated";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Sub Department Name Updated");
        
        header("location:../subDepartments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../subDepartments");
      }
  }

}
else{
  header('location:../login');
 }
 ?>
