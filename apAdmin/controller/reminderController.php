<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));
 

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{ 

 
 if(isset($_POST['remAddBtn'])){
  
  
  $reminder_date = date("Y-m-d H:i:s", strtotime($reminder_date));
  $user_id = 0 ; 
  $m->set_data('reminder_text',$reminder_text);  
  $m->set_data('reminder_date',$reminder_date);  
  $m->set_data('user_id',$user_id);  
  $m->set_data('admin_id',$_COOKIE['bms_admin_id']);  
  $m->set_data('society_id',$society_id);  
  
  $a =array(
    'reminder_text'=> $m->get_data('reminder_text'),
    'reminder_date'=> $m->get_data('reminder_date'),
    'user_id'=> $m->get_data('user_id')  ,
    'society_id'=> $m->get_data('society_id')  ,
    'admin_id'=> $m->get_data('admin_id')  
  );

  
  $q=$d->insert("reminder_master",$a);
  $reminder_id = $con->insert_id;
  
  if($q>0) {

    $remAray =array(
      'reminder_text'=> $m->get_data('reminder_text'),
      'reminder_date'=> $m->get_data('reminder_date'),
      'reminder_id'=> $reminder_id  ,
      'society_id'=> $m->get_data('society_id')  ,
      'admin_id'=> $m->get_data('admin_id') ,
      'reminder_status'=> 0  
    );

     $title = "Reminder Set Successfully";
     $description = "Click here to set reminder in calendar";
     $fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $nAdmin->noti_new($society_id,"", $fcmArray, $title,$description, $remAray);
      $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, $title, $description, $remAray);
    $_SESSION['msg']="Reminder Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$_SESSION['msg']);
    header("location:../reminderList");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../reminderList");
  }

}  else if(isset($_POST['remEditBtn'])){
 
  
  $reminder_date = date("Y-m-d H:i:s", strtotime($reminder_date));
  $user_id = 0 ; 
  $m->set_data('reminder_text',$reminder_text);  
  $m->set_data('reminder_date',$reminder_date);  
  $m->set_data('user_id',$user_id);  
  $m->set_data('admin_id',$_COOKIE['bms_admin_id']);
  $m->set_data('society_id',$society_id); 

   $a =array(
    'reminder_text'=> $m->get_data('reminder_text'),
    'reminder_date'=> $m->get_data('reminder_date'),
    'user_id'=> $m->get_data('user_id')  ,
    'society_id'=> $m->get_data('society_id')  ,
    'admin_id'=> $m->get_data('admin_id')  
  );


  $q=$d->update("reminder_master",$a,"reminder_id = '$reminder_id' ");
  if($q>0 ) {
    $_SESSION['msg']="Reminder Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$_SESSION['msg']);
    
    header("location:../reminderList");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../reminderList");
  }


}  
else if(isset($_POST['DoneRem'])){
 
  $m->set_data('reminder_status','1'); 

   $a =array(
    'reminder_status'=> $m->get_data('reminder_status') 
  );
   $q=$d->update("reminder_master",$a,"reminder_id = '$reminder_id' ");
       
  if($q>0 ) {

    $remAray =array(
      'reminder_text'=> "Reminder",
      'reminder_date'=>"Completed Successfully",
      'reminder_id'=> $reminder_id  ,
      'society_id'=> $m->get_data('society_id')  ,
      'admin_id'=> $m->get_data('admin_id')   ,
      'reminder_status'=> 1
    );

     $title = "Reminder";
     $description = "Completed Successfully";
     $fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $nAdmin->noti_new($society_id,"", $fcmArray, $title,$description, "reminderList");
      $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, $title, $description, "reminderList");

    $_SESSION['msg']="Reminder Completed";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$_SESSION['msg']);
    header("location:../reminderList");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../reminderList");
  }


}  
 else if(isset($_POST['deleteRem'])){
 
    $remAray =array(
      'reminder_text'=> "Reminder",
      'reminder_date'=>"Deleted Successfully",
      'reminder_id'=> $reminder_id  ,
      'society_id'=> $m->get_data('society_id')  ,
      'admin_id'=> $m->get_data('admin_id')   ,
      'reminder_status'=> 2
    );

     $title = "Reminder";
     $description = "Deleted Successfully";
     $fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android' AND admin_id='$_COOKIE[bms_admin_id]' AND bms_admin_master.admin_active_status=0");
      $nAdmin->noti_new($society_id,"", $fcmArray, $title,$description, "reminderList");
      $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, $title, $description, "reminderList");


   $q=$d->delete("reminder_master","reminder_id='$reminder_id'  ");
       
  if($q>0 ) {
    $_SESSION['msg']="Reminder Deleted";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$_SESSION['msg']);
    header("location:../reminderList");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../reminderList");
  }


}  


}else{
  header('location:../login');
}
?>