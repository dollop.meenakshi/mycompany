<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));



 
if ( (isset($visit_from)  && trim($visit_from) !="" && !preg_match('/^[a-z\d\-_\s]+$/i',$visit_from))  || (isset($visitor_name) && trim($visitor_name) !="" && !preg_match('/^[a-z\d\-_\s]+$/i',$visitor_name)) || (isset($vehicle_number) && trim($vehicle_number) !="" && !preg_match('/^[a-z\d\-_\s]+$/i',$vehicle_number) ) ){
  $_SESSION['msg1']="Special Characters are Not Allowed, please provide Alphanumeric string";
  header("location:../dailyVisitors");
  exit;
} 

 

// delete user
if(isset($deletDailyVisitor)) {
  $q=$d->delete("daily_visitors_master","visitor_id='$visitor_id'");
  if($q==TRUE) {
         $visit_date = date("Y-m-d");
            $visit_time = date("H:i:s");

    $a = array(
        'exit_date' => $visit_date,
        'exit_time' => $visit_time,
        'visitor_status' => "3",
    );
  
   $d->update("visitors_master", $a,"daily_visitor_id='$visitor_id'  ORDER BY visitor_id DESC ");  

        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$deleteVisitorName Daily Visitor Deleted");

    $_SESSION['msg']="Deleted Successfully";
    header("Location: ../dailyVisitors");
  } else {
    $_SESSION['msg']="Something Wrong";
    header("Location: ../dailyVisitors");
  }
}

if(isset($RejectDailyVisitor)) {
$q=$d->delete("daily_visitors_master","visitor_id='$visitor_id'");
 
 if($q==TRUE) {

    $_SESSION['msg']="Daily Visitor Rejected";
    header("Location: ../dailyVisitors");
  } else {
    $_SESSION['msg']="Something Wrong";
    header("Location: ../dailyVisitors");
  }
}

if(isset($ApproveDailyVisitor)) {
   $a1= array (
   'active_status'=> '0' 
  );
  $q=$d->update("daily_visitors_master",$a1,"visitor_id='$visitor_id'");

  if($q==TRUE) {

    $_SESSION['msg']="Daily Visitor Rejected";
    header("Location: ../dailyVisitors");
  } else {
    $_SESSION['msg']="Something Wrong";
    header("Location: ../dailyVisitors");
  }
}

if(isset($DailyVisitorDelete)) {

   $q=$d->select("daily_visitor_unit_master","daily_visitor_unit_id='$daily_visitor_unit_id' ");
   $daily_visitor_unit_master_detail=mysqli_fetch_array($q);


  $q=$d->delete("daily_visitor_unit_master","unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_unit_master_detail[daily_visitor_id]'");
  if($q==TRUE) {
       
        $daily_visitors_master_qry=$d->select("daily_visitors_master","visitor_id='$daily_visitor_unit_master_detail[daily_visitor_id]' ");
    $daily_visitors_master_data=mysqli_fetch_array($daily_visitors_master_qry);
    $title =$daily_visitors_master_data['visitor_name']." - Daily Visitor Deleted";
    $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$daily_visitor_unit_master_detail['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'visitor',
              'notification_logo'=>'Visitors-mgmtxxxhdpi.png',
              );
              $d->insert("user_notification",$notiAry);
 
         $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$daily_visitor_unit_master_detail[user_id]' ");
         $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$daily_visitor_unit_master_detail[user_id]'  ");
         $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorFragment');
         $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorVC');


        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$deleteVisitorName Daily Visitor Unit ($unitRemoveName) Deleted");

    $_SESSION['msg']="Deleted Successfully";
    header("Location: ../dailyVisitors?visitor_id=".$visitor_id."&manageVisitor=yes");
  } else {
    $_SESSION['msg']="Something Wrong";
     header("Location: ../dailyVisitors?visitor_id=".$visitor_id."&manageVisitor=yes");
  }
}

// add new employee
if(isset($addDailyVisitorBtn)) {
  if ($country_code!="") {
      $appendQuery = " AND country_code='$country_code'";
  }

  $q=$d->select("daily_visitors_master","visitor_mobile='$visitor_mobile' and active_status=0 $appendQuery");
    $data=mysqli_fetch_array($q);
    if ($data>0) {
        $_SESSION['msg']="Mobile NUmber Already Used";
     header("Location: ../dailyVisitors?visitor_id=".$visitor_id."&manageVisitor=yes");
    }  

  $visitor_mobile= (int)$visitor_mobile;
    if (strlen($visitor_mobile)<8 ) {
        $_SESSION['msg1']="Invalid Mobile Number";
         header("Location: ../dailyVisitors");
        exit;
    }

  $extension=array("jpeg","jpg","png","gif","JPG","jpeg","JPEG","PNG");
  $uploadedFile = $_FILES['visitor_profile']['tmp_name'];
  $ext = pathinfo($_FILES['visitor_profile']['name'], PATHINFO_EXTENSION);

  if(file_exists($uploadedFile)) {
    if(in_array($ext,$extension)) {
      $sourceProperties = getimagesize($uploadedFile);
      $newFileName = rand().$user_id;
      $dirPath = "../../img/visitor/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];

          // less 30 % size 
      $newImageWidth = $imageWidth * 30 /100;
      $newImageHeight = $imageHeight * 30 /100;

      switch ($imageType) {

        case IMAGETYPE_PNG:
        $imageSrc = imagecreatefrompng($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagepng($tmp,$dirPath. $newFileName. ".". $ext);
        break;           

        case IMAGETYPE_JPEG:
        $imageSrc = imagecreatefromjpeg($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagejpeg($tmp,$dirPath. $newFileName. ".". $ext);
        break;

        case IMAGETYPE_GIF:
        $imageSrc = imagecreatefromgif($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagegif($tmp,$dirPath. $newFileName. ".". $ext);
        break;

        default:
        $_SESSION['msg1']="Invalid Image";
        header("Location: ../dailyVisitors");
        exit;
        break;
      }
      $visitor_profile= $newFileName.".".$ext;

    } else{
      $_SESSION['msg1']="Invalid Profile Photo";
      header("location:../dailyVisitors");
      exit();
    }
  } else {
    $visitor_profile="visitor_default.png";
  }


      $file = $_FILES['id_proof']['tmp_name'];
      $file11 = $_FILES['visitor_id_proof_back']['tmp_name'];
        if(file_exists($file)) {

          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['id_proof']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['id_proof']['size'] >= $maxsize) || ($_FILES["id_proof"]["size"] == 0)) {
                $_SESSION['msg1']="Proof too large. Must be less than 4 MB.";
                header("location:../dailyVisitors");
                
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["id_proof"]["type"]))) {
                 $_SESSION['msg1']="Invalid  File. Only  JPG, PNG,Doc & PDF are allowed.";
                header("location:../dailyVisitors");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['id_proof'];   
            $temp = explode(".", $_FILES["id_proof"]["name"]);
            $id_proof = $visitor_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["id_proof"]["tmp_name"], "../../img/visitor/".$id_proof);
          } 
        } else{
          $id_proof="";
        }

    if(file_exists($file11)) {

          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['visitor_id_proof_back']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['visitor_id_proof_back']['size'] >= $maxsize) || ($_FILES["visitor_id_proof_back"]["size"] == 0)) {
                $_SESSION['msg1']="Proof too large. Must be less than 4 MB.";
                header("location:../dailyVisitors");
                
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["visitor_id_proof_back"]["type"]))) {
                 $_SESSION['msg1']="Invalid  File. Only  JPG, PNG,Doc & PDF are allowed.";
                header("location:../dailyVisitors");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['visitor_id_proof_back'];   
            $temp = explode(".", $_FILES["visitor_id_proof_back"]["name"]);
            $visitor_id_proof_back = $visitor_mobile.'_idBack_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["visitor_id_proof_back"]["tmp_name"], "../../img/visitor/".$visitor_id_proof_back);
          } 
        } else{
          $visitor_id_proof_back="";
        }

   $visitorMainType_qry = $d->select("visitorSubType","active_status=0 AND visitor_sub_type_id='$visitor_sub_type_id'");
                        //isset($complaint_category) &&
   $visitorMainType_data = mysqli_fetch_array($visitorMainType_qry);
   $visit_fromCheck= $visitorMainType_data['visitor_sub_type_name'];

    if ($visit_fromCheck=='Other') {
        $visit_fromCheck= $visit_from;
    }
 

  $m->set_data('society_id',$_COOKIE['society_id']);
  $m->set_data('visitor_type','4');
  $m->set_data('visit_from',$visit_fromCheck);
  $m->set_data('visitor_name',$visitor_name);
  $m->set_data('visitor_mobile',$visitor_mobile);
  $m->set_data('country_code',$country_code);
  $m->set_data('visitor_profile',$visitor_profile);
  $m->set_data('id_proof',$id_proof);
  $m->set_data('visitor_id_proof_back',$visitor_id_proof_back);
  
  $m->set_data('number_of_visitor','1'); 
 
  $week_days_chk = implode(",", $_POST['week_days']);

  $vehicle_number = strtoupper($vehicle_number);

  $m->set_data('valid_till',$valid_till);
  $m->set_data('in_time',date("H:i A",strtotime($in_time)));
  $m->set_data('out_time',date("H:i A",strtotime($out_time))); 
  $m->set_data('week_days',$week_days_chk); 
  $m->set_data('visitor_sub_type_id',$visitor_sub_type_id); 
    $m->set_data('vehicle_number',$vehicle_number); 
  $a1= array (

    'society_id'=> $m->get_data('society_id'),
    'visitor_type'=> $m->get_data('visitor_type'),
    'visit_from'=> $m->get_data('visit_from'),
    'visitor_name'=> $m->get_data('visitor_name'),
    'visitor_mobile'=> $m->get_data('visitor_mobile'),
    'country_code'=> $m->get_data('country_code'),
    'visitor_profile'=> $m->get_data('visitor_profile'),
    'visitor_id_proof' =>$m->get_data('id_proof'),
    'visitor_id_proof_back' =>$m->get_data('visitor_id_proof_back'),
    'number_of_visitor'=> $m->get_data('number_of_visitor'),
    'valid_till'=> $m->get_data('valid_till'),
    'in_time'=> $m->get_data('in_time'),
    'out_time'=> $m->get_data('out_time'),
    'week_days'=> $m->get_data('week_days'),
     'vehicle_number'=> $m->get_data('vehicle_number'),
    'visitor_sub_type_id'=> $m->get_data('visitor_sub_type_id') 
  );

//echo "<pre>";print_r($a1);exit;
  $q=$d->insert("daily_visitors_master",$a1);

  if($q==TRUE) {
    $_SESSION['msg']="Daily Visitor Added";


    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$visitor_name Daily Visitor Added");

    header("Location: ../dailyVisitors");
  } else {
    header("Location: ../dailyVisitors");
  }
}


if(isset($editDailyVisitorBtn)) {

  if ($country_code!="") {
      $appendQuery = " AND country_code='$country_code'";
  }
  
 $q=$d->select("daily_visitors_master","visitor_mobile='$visitor_mobile' and active_status=0 and visitor_id!='$visitor_id' $appendQuery");
    $data=mysqli_fetch_array($q);
    if ($data>0) {
        $_SESSION['msg']="Mobile NUmber Already Used";
     header("Location: ../dailyVisitors?visitor_id=".$visitor_id."&manageVisitor=yes");
    }  

    $visitor_mobile= (int)$visitor_mobile;
    if (strlen($visitor_mobile) < 8 && strlen($visitor_mobile) > 15) {
        $_SESSION['msg1']="Invalid Mobile Number";
         header("Location: ../dailyVisitors");
        exit;
    }

  $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG","jpeg");
  $uploadedFile = $_FILES['visitor_profile']['tmp_name'];
  $ext = pathinfo($_FILES['visitor_profile']['name'], PATHINFO_EXTENSION);

  if(file_exists($uploadedFile)) {
    if(in_array($ext,$extension)) {
      $sourceProperties = getimagesize($uploadedFile);
      $newFileName = rand().$user_id;
      $dirPath = "../../img/visitor/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];

          // less 30 % size 
      $newImageWidth = $imageWidth * 30 /100;
      $newImageHeight = $imageHeight * 30 /100;

      switch ($imageType) {

        case IMAGETYPE_PNG:
        $imageSrc = imagecreatefrompng($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagepng($tmp,$dirPath. $newFileName. ".". $ext);
        break;           

        case IMAGETYPE_JPEG:
        $imageSrc = imagecreatefromjpeg($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagejpeg($tmp,$dirPath. $newFileName. ".". $ext);
        break;

        case IMAGETYPE_GIF:
        $imageSrc = imagecreatefromgif($uploadedFile); 
        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
        imagegif($tmp,$dirPath. $newFileName. ".". $ext);
        break;

        default:
        $_SESSION['msg1']="Invalid Image";
        header("Location: ../dailyVisitors");
        exit;
        break;
      }
      $visitor_profile= $newFileName.".".$ext;

    } else{
      $_SESSION['msg1']="Invalid Profile Photo";
      header("location:../dailyVisitors");
      exit();
    }
  } else {
    $visitor_profile=$visitor_profile_old;
  }


$file = $_FILES['id_proof']['tmp_name'];
$file11 = $_FILES['visitor_id_proof_back']['tmp_name'];
        if(file_exists($file)) {

          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['id_proof']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['id_proof']['size'] >= $maxsize) || ($_FILES["id_proof"]["size"] == 0)) {
                $_SESSION['msg1']="Id Proof too large. Must be less than 4 MB.";
                header("location:../dailyVisitors");
                exit();
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["id_proof"]["type"]))) {
                 $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
                header("location:../dailyVisitors");
                exit();
            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['id_proof'];   
            $temp = explode(".", $_FILES["id_proof"]["name"]);
            $id_proof = $visitor_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["id_proof"]["tmp_name"], "../../img/visitor/".$id_proof);
          } 
        } else{
          $id_proof=$id_proof_old;
        }

      if(file_exists($file11)) {

        $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
        $extId = pathinfo($_FILES['visitor_id_proof_back']['name'], PATHINFO_EXTENSION);

         $errors     = array();
          $maxsize    = 4097152;
          
          if(($_FILES['visitor_id_proof_back']['size'] >= $maxsize) || ($_FILES["visitor_id_proof_back"]["size"] == 0)) {
              $_SESSION['msg1']="Proof too large. Must be less than 4 MB.";
              header("location:../dailyVisitors");
              
          }
          if(!in_array($extId, $extensionResume) && (!empty($_FILES["visitor_id_proof_back"]["type"]))) {
               $_SESSION['msg1']="Invalid  File. Only  JPG, PNG,Doc & PDF are allowed.";
              header("location:../dailyVisitors");

          }
         if(count($errors) === 0) {
          $image_Arr = $_FILES['visitor_id_proof_back'];   
          $temp = explode(".", $_FILES["visitor_id_proof_back"]["name"]);
          $visitor_id_proof_back = $visitor_mobile.'_idBack_'.round(microtime(true)) . '.' . end($temp);
          move_uploaded_file($_FILES["visitor_id_proof_back"]["tmp_name"], "../../img/visitor/".$visitor_id_proof_back);
        } 
      } else{
        $visitor_id_proof_back=$visitor_id_proof_back_old;
      }


   $visitorMainType_qry = $d->select("visitorSubType","active_status=0 AND visitor_sub_type_id='$visitor_sub_type_id'");
                        //isset($complaint_category) &&
   $visitorMainType_data = mysqli_fetch_array($visitorMainType_qry);
   $visit_fromCheck= $visitorMainType_data['visitor_sub_type_name'];

    if ($visit_fromCheck=='Other') {
        $visit_fromCheck= $visit_from;
    }

   
  $m->set_data('society_id',$_COOKIE['society_id']);
  $m->set_data('visitor_type','4');
  $m->set_data('visit_from',$visit_fromCheck);
  $m->set_data('visitor_name',$visitor_name);
  $m->set_data('visitor_mobile',$visitor_mobile);
  $m->set_data('country_code',$country_code);
  $m->set_data('visitor_profile',$visitor_profile);
  $m->set_data('id_proof',$id_proof);
  $m->set_data('visitor_id_proof_back',$visitor_id_proof_back);
  $m->set_data('number_of_visitor','1'); 

   $week_days_chk = implode(",", $_POST['week_days']);
$vehicle_number = strtoupper($vehicle_number);
  $m->set_data('valid_till',$valid_till);
  $m->set_data('in_time',date("H:i A",strtotime($in_time)));
  $m->set_data('out_time',date("H:i A",strtotime($out_time))); 
  $m->set_data('week_days',$week_days_chk); 
  $m->set_data('visitor_sub_type_id',$visitor_sub_type_id); 
  $m->set_data('vehicle_number',$vehicle_number); 
  

  $a1= array (

    'society_id'=> $m->get_data('society_id'),
    'visitor_type'=> $m->get_data('visitor_type'),
    'visit_from'=> $m->get_data('visit_from'),
    'visitor_name'=> $m->get_data('visitor_name'),
    'visitor_mobile'=> $m->get_data('visitor_mobile'),
    'country_code'=> $m->get_data('country_code'),
    'visitor_profile'=> $m->get_data('visitor_profile'),
    'number_of_visitor'=> $m->get_data('number_of_visitor') ,
     'visitor_id_proof' =>$m->get_data('id_proof'), 
     'visitor_id_proof_back' =>$m->get_data('visitor_id_proof_back'), 
    'valid_till'=> $m->get_data('valid_till'),
    'in_time'=> $m->get_data('in_time'),
    'out_time'=> $m->get_data('out_time'),
    'week_days'=> $m->get_data('week_days'),
      'vehicle_number'=> $m->get_data('vehicle_number'),
    'visitor_sub_type_id'=> $m->get_data('visitor_sub_type_id') 
  );

//echo "<pre>";print_r($a1);exit;

  $q=$d->update("daily_visitors_master",$a1,"visitor_id='$visitor_id'");
  if($q==TRUE) {

    $_SESSION['msg']="Daily Visitor Details Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$visitor_name Daily Visitor Details Updated");
    header("Location: ../dailyVisitors?visitor_id=$visitor_id&manageVisitor=yes");
  } else {
    header("Location: ../dailyVisitors");
  }
}



// add new employee
if(isset($dailyVisitiorUserBtn)) {
 

if( (count($_POST['user_data']) + count($_POST['user_data'])) <= 0  ){
  $_SESSION['msg1']="Please Select Atleast One User";
        header("location:../dailyVisitors");
        exit;
}
 $daily_visitor_unit_master_Added = $d->select("daily_visitor_unit_master","  daily_visitor_id ='$daily_visitor_id' ");
   
   
  

 for ($i=0; $i < count($_POST['user_data']) ; $i++) {

  $user_id = $_POST['user_data'][$i];
  /* $user_details = explode("~", $user_data);
  $unit_id= $user_details[0];
  $user_id= $user_details[1];
  $user_type= $user_details[2]; */
 /*  echo "<pre>";
  print_r($user_id);die; */

  // find all family member
  $qf=$d->selectRow("unit_id","users_master","user_status=1 AND user_id='$user_id' AND delete_status=0");
  while ($userData=mysqli_fetch_array($qf)) {
      $unit_id= $userData['unit_id'];

     $m->set_data('unit_id',$unit_id);
     $m->set_data('user_id',$user_id);
     $m->set_data('society_id', $_COOKIE['society_id']);
     $m->set_data('daily_visitor_id', $daily_visitor_id);

      $a = array(
        'unit_id'=>$m->get_data('unit_id'),
        'user_id'=>$m->get_data('user_id') ,
        'society_id'=>$m->get_data('society_id') ,
        'daily_visitor_id'=>$m->get_data('daily_visitor_id'),
        'valid_till_user'=>$valid_till ,
        'in_time_user'=>$in_time  ,
        'out_time_user'=>$out_time ,
        'week_days_user'=>$week_days , 
      );
    
      $dc= $d->selectRow("user_id","daily_visitor_unit_master","unit_id='$unit_id' AND user_id='$user_id' AND  daily_visitor_id='$daily_visitor_id'");
      if (mysqli_num_rows($dc)) {
        
      } else {
        $q=$d->insert("daily_visitor_unit_master",$a);

     
       $title =$visitor_name." - Daily Visitor Added";
     
         $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$user_id,
              'notification_title'=>$title,
              'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'visitor',
              'notification_logo'=>'Visitors-mgmtxxxhdpi.png',
              );
              $d->insert("user_notification",$notiAry);
 
         $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$user_id' ");
         $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$user_id'  ");
         $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorFragment');
         $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorVC');
      }
    }
  }




  if($q){ 
    if(mysqli_num_rows($daily_visitor_unit_master_Added)>0) {
       $_SESSION['msg']="Added Successfully";
       $msg= "Added Successfully";
     }else {
      $_SESSION['msg']="Added Successfully";
      $msg= "Added Successfully";
     }
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$msg);
        header("Location: ../dailyVisitors?visitor_id=".$daily_visitor_id."&manageVisitor=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../dailyVisitors?visitor_id=".$daily_visitor_id."&manageVisitor=yes");
      } 
    
}



if(isset($dailyVisitiorUserBtnAll)) {
 

if( (count($_POST['user_data']) + count($_POST['user_data'])) <= 0  ){
  $_SESSION['msg1']="Please Select Atleast One User";
        header("location:../dailyVisitors");
        exit;
}
 $daily_visitor_unit_master_Added = $d->select("daily_visitor_unit_master","  daily_visitor_id ='$daily_visitor_id' ");
   
   
  

 for ($i=0; $i < count($_POST['user_data']) ; $i++) {

  $user_data = $_POST['user_data'][$i];
  $user_details = explode("~", $user_data);
  $unit_id= $user_details[0];
  $user_id= $user_details[1];
  $user_type= $user_details[2];

  // find all family member
  $qf=$d->selectRow("user_id","users_master","user_status=1 AND unit_id='$unit_id' AND user_type='$user_type' AND delete_status=0 AND user_id='$user_id'");
  while ($userData=mysqli_fetch_array($qf)) {
      $user_id= $userData['user_id'];

     $m->set_data('unit_id',$unit_id);
     $m->set_data('user_id',$user_id);
     $m->set_data('society_id', $_COOKIE['society_id']);
     $m->set_data('daily_visitor_id', $daily_visitor_id);

      $a = array(
        'unit_id'=>$m->get_data('unit_id'),
        'user_id'=>$m->get_data('user_id') ,
        'society_id'=>$m->get_data('society_id') ,
        'daily_visitor_id'=>$m->get_data('daily_visitor_id'),
        'valid_till_user'=>$valid_till ,
        'in_time_user'=>$in_time  ,
        'out_time_user'=>$out_time ,
        'week_days_user'=>$week_days , 
      );
    
      $dc= $d->selectRow("user_id","daily_visitor_unit_master","unit_id='$unit_id' AND user_id='$user_id' AND  daily_visitor_id='$daily_visitor_id'  AND user_id='$user_id'");
      if (mysqli_num_rows($dc)) {
        
      } else {
        $q=$d->insert("daily_visitor_unit_master",$a);

     
       $title =$visitor_name." - Daily Visitor Added";
     
         $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$user_id,
              'notification_title'=>$title,
              'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'visitor',
              'notification_logo'=>'Visitors-mgmtxxxhdpi.png',
              );
              $d->insert("user_notification",$notiAry);
 
         $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$user_id' ");
         $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$user_id'  ");
         $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorFragment');
         $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'VisitorVC');
      }
    }
  }




  if($q){ 
    if(mysqli_num_rows($daily_visitor_unit_master_Added)>0) {
       $_SESSION['msg']="Added Successfully";
       $msg= "Unit Added Successfully";
     }else {
      $_SESSION['msg']="Added Successfully";
      $msg= "Unit Added Successfully";
     }
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$msg);
        header("Location: ../dailyVisitors?visitor_id=".$daily_visitor_id."&manageVisitor=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../dailyVisitors?visitor_id=".$daily_visitor_id."&manageVisitor=yes");
      } 
    
}



if(isset($deleteSingleUser)) {

 
  $q=$d->delete("daily_visitor_unit_master","unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}


if(isset($muteDailyVisitorNotification)) {
    $a = array(
      'active_status'=>1,
      );
  $q=$d->update("daily_visitor_unit_master",$a,"unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}

if(isset($unmuteDailyVisitoreNotification)) {
    $a = array(
      'active_status'=>0,
      );
  $q=$d->update("daily_visitor_unit_master",$a,"unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}
 

if(isset($getEmpNotification)) {

    $admin_id = $_COOKIE['bms_admin_id'];
    $a = array(
      'society_id'=>$society_id,
      'emp_id'=>$emp_id,
      'admin_id'=>$admin_id,
      'empType'=>1,
      );

    $qc=$d->select("employee_in_out_notification","admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=1");
    if (mysqli_num_rows($qc)>0) {
      $q=$d->update("employee_in_out_notification",$a,"admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=1");
    } else {
      $q=$d->insert("employee_in_out_notification",$a);
    }

  if($q==TRUE) {
    echo "<a href='javascript:void(0)' onclick='notGetEmpNotificationDaily($emp_id);' > <i class='fa fa-bell' aria-hidden='true'></i> </a>";
  } else { 
    echo "1";
  }
}


if(isset($NotgetEmpNotification)) {
  $admin_id = $_COOKIE['bms_admin_id'];
  $q=$d->delete("employee_in_out_notification","admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=1");
    
  if($q==TRUE) {
    echo "<a href='javascript:void(0)' onclick='getEmpNotificationDaily($emp_id);' > <i class='fa fa-bell-slash' aria-hidden='true'></i> </a>";
  } else { 
    echo "1";
  }
}
?>