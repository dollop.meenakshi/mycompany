<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));


if (isset($_POST) && !empty($_POST)) {
    if (isset($_POST['addProductVerient'])) {
        $m->set_data('product_id', $pr_id);
        $m->set_data('product_variant_name', $product_variant_name);
        $m->set_data('created_date', date("Y-m-d H:i:s"));
        $a1 = array(
            'product_id' => $m->get_data('product_id'),
            'product_variant_name' => $m->get_data('product_variant_name'),
            'created_date' => $m->get_data('created_date'),
        );

        $checkProductVer = $d->selectRow('product_variant_master.*', 'product_variant_master', "product_variant_name='$product_variant_name' AND product_id= $pr_id");
        $getDta = mysqli_fetch_assoc($checkProductVer);

        if (isset($product_variant_id) && $product_variant_id > 0) {
            if ($getDta) {

                if ($getDta['product_variant_id'] == $product_variant_id) {
                    $q = $d->update("product_variant_master", $a1, "product_variant_id ='$product_variant_id'");
                    $_SESSION['msg'] = "Product variant Updated Successfully";
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Updated Successfully");
                    if ($q == true) {
                        header("Location: ../productDetail?pId=$pr_id");
                    } else {
                        $_SESSION['msg1'] = "Something Wrong";
                        header("Location: ../productDetail?pId=$pr_id");
                    }
                } else {
                    $_SESSION['msg1'] = "  Product variant Already Added";
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Added Successfully");
                    if ($q == true) {
                        header("Location: ../productDetail?pId=$pr_id");
                    } else {
                        $_SESSION['msg1'] = "Something Wrong";
                        header("Location: ../productDetail?pId=$pr_id");
                    }
                }
            } else {
                $q = $d->update("product_variant_master", $a1, "product_variant_id ='$product_variant_id'");
                $_SESSION['msg'] = "Product variant Updated Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Updated Successfully");
                if ($q == true) {
                    header("Location: ../productDetail?pId=$pr_id");
                } else {
                    $_SESSION['msg1'] = "Something Wrong";
                    header("Location: ../productDetail?pId=$pr_id");
                }
            }
        } else {

            if ($getDta) {
                $_SESSION['msg1'] = "  Product variant Already Added";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Added Successfully");
                header("Location: ../productDetail?pId=$pr_id");
            } else {

                $q = $d->insert("product_variant_master", $a1);
                $_SESSION['msg'] = "Product variant Added Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Added Successfully");
                if ($q == true) {
                    header("Location: ../productDetail?pId=$pr_id");
                } else {
                    $_SESSION['msg1'] = "Something Wrong";
                    header("Location: ../productDetail?pId=$pr_id");
                }
            }
        }
    }
}
