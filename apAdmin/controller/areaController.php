<?php
include '../common/objectController.php';

if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addArea']))
    {
        $ci_st_id = explode("-",$_POST['state_city_id']);
        $city_id = $ci_st_id[0];
        $state_id = $ci_st_id[1];
        $m->set_data('country_id', $country_id);
        $m->set_data('city_id', $city_id);
        $m->set_data('state_id', $state_id);
        $m->set_data('area_name', $area_name);
        $m->set_data('pincode', $pincode);
        $m->set_data('latitude', $latitude);
        $m->set_data('longitude', $longitude);
        $a1 = array(
            'country_id' => $m->get_data('country_id'),
            'state_id' => $m->get_data('state_id'),
            'city_id' => $m->get_data('city_id'),
            'area_name' => $m->get_data('area_name'),
            'pincode' => $m->get_data('pincode'),
            'latitude' => $m->get_data('latitude'),
            'longitude' => $m->get_data('longitude')
        );
        if(isset($area_id) && $area_id > 0)
        {
            $q = $d->update("area_master_new",$a1,"area_id = '$area_id'");
            $_SESSION['msg'] = "Area Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Area Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Area Successfully Updated.";
                header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addArea");
            }
        }
        else
        {
            $q = $d->insert("area_master_new", $a1);
            $_SESSION['msg'] = "Area Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Area Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Area Successfully Added.";
                header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addArea");
            }
        }
    }
    elseif(isset($_POST['getStateCityTag']))
    {
        $q = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['deleteArea']))
    {
        $check = $d->selectRow("rm.retailer_id","retailer_master AS rm","rm.area_id = '$area_id'");
        if(mysqli_num_rows($check) > 0)
        {
            $_SESSION['msg1'] = "Can't delete area, already in use!";
            header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");exit;
        }

        $check1 = $d->selectRow("dm.distributor_id","distributor_master AS dm","dm.distributor_area_id = '$area_id'");
        if(mysqli_num_rows($check1) > 0)
        {
            $_SESSION['msg1'] = "Can't delete area, already in use!";
            header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");exit;
        }

        $q = $d->delete("area_master_new","area_id = '$area_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Area deleted");
            $_SESSION['msg'] = "Area Successfully Delete.";
            header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageArea?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
    }
    elseif(isset($_POST['checkAreaName']))
    {
        if(isset($area_id))
        {
            $q = $d->selectRow("area_name","area_master_new","area_name = '$area_name' AND area_id != '$area_id'");
        }
        else
        {
            $q = $d->selectRow("area_name","area_master_new","area_name = '$area_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
}
?>