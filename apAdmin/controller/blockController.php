<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

$blockLngName = $xml->string->blocks;
$societyLngName = $xml->string->society;

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  // add main menu
  if(isset($_POST['no_of_blocks'])){

    $dataCount = count($_POST['block_name']);
    
    $block_name = implode("~", $_POST['block_name']);
    $block_name1= explode('~', (string)$block_name);
    
    for ($i=0; $i <$dataCount ; $i++) { 


//IS_1045
//IS_1204
    
    $block_master_qry=$d->select("block_master","society_id='$society_id' order by block_id desc");
    if(mysqli_num_rows($block_master_qry) > 0 ){
          $block_master_data=mysqli_fetch_array($block_master_qry);
          $block_sort= ($block_master_data['block_sort'] + 1 );
    } else {
      $block_sort=1;
    }
  


   
     $m->set_data('block_sort',$block_sort);
//IS_1045
//IS_1204
      $m->set_data('society_id',$society_id);
      $m->set_data('block_name',$block_name1[$i]);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_name'=> $m->get_data('block_name'),

        //IS_1045
         'block_sort'=> $m->get_data('block_sort'),
      );

      $q=$d->insert("block_master",$a);
    }
    if($q>0) {
      $_SESSION['msg']="$blockLngName Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$blockLngName added");
      header("location:../branch");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../branch");
    }
  }
  

  if(isset($_POST['blockNameSingle'])){

    
    //IS_1045

    //IS_1204
    
    $block_master_qry=$d->select("block_master","society_id='$society_id' order by block_id desc");
    if(mysqli_num_rows($block_master_qry) > 0 ){
          $block_master_data=mysqli_fetch_array($block_master_qry);
          $block_sort= ($block_master_data['block_sort'] + 1) ;
    } else {
      $block_sort=1;
    }

   
    $m->set_data('society_id',$society_id);
    $m->set_data('block_name',$blockNameSingle);
     $m->set_data('block_sort',$block_sort);
//IS_1045


    $a =array(
      'society_id'=> $m->get_data('society_id'),
      'block_name'=> $m->get_data('block_name'),
      //IS_1045
         'block_sort'=> $m->get_data('block_sort'),
    );

    $q=$d->insert("block_master",$a);
    if($q>0) {
      $_SESSION['msg']="$blockLngName Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$blockLngName added");
      header("location:../branches");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../branch");
    }
  }

// update building details
  if(isset($_POST['updateBuilding'])){

    $file = $_FILES['socieaty_logo']['tmp_name'];
    if(file_exists($file)) {
     $errors     = array();
     $maxsize    = 2097152;
     $acceptable = array(
                // 'application/pdf',
      'image/jpeg',
      'image/jpg',
      'image/png'
    );
     if(($_FILES['socieaty_logo']['size'] >= $maxsize) || ($_FILES["socieaty_logo"]["size"] == 0)) {
      $errors[] = "<strong  class=' text-danger'>Profile photo too large. Must be less than 2 MB.</strong><br>";
      
    }
    if(!in_array($_FILES['socieaty_logo']['type'], $acceptable) && (!empty($_FILES["socieaty_logo"]["type"]))) {
      $errors[] = "<strong class=' text-danger'>Invalid  photo. Only  JPG and PNG are allowed.</strong><br>";

    }
    if(count($errors) === 0) {
      $image_Arr = $_FILES['socieaty_logo'];   
      $temp = explode(".", $_FILES["socieaty_logo"]["name"]);
      $socieaty_logo = round(microtime(true)) . '.' . end($temp);
      move_uploaded_file($_FILES["socieaty_logo"]["tmp_name"], "../../img/society/".$socieaty_logo);
    } 
  } else{
    $socieaty_logo=$socieaty_logo_old;
  }

  

  $m->set_data('society_name',$society_name_update);
  $m->set_data('society_address',$society_address);
  $m->set_data('secretary_email',$secretary_email);
  $m->set_data('socieaty_logo',$socieaty_logo);
  $m->set_data('builder_name',$builder_name);
  $m->set_data('builder_address',$builder_address);
  $m->set_data('builder_mobile',$builder_mobile);
  
  $a =array(
    'society_name'=> $m->get_data('society_name'),
    'society_address'=> $m->get_data('society_address'),
    'secretary_email'=>$m->get_data('secretary_email'),
    'socieaty_logo'=>$m->get_data('socieaty_logo'),
    'builder_name'=>$m->get_data('builder_name'),
    'builder_address'=>$m->get_data('builder_address'),
    'builder_mobile'=>$m->get_data('builder_mobile'),
  );

  $q=$d->update("society_master",$a,"society_id='$society_id'");
  if($q>0) {
    
    $_SESSION['msg']="Details Updated Successfully";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$societyLngName Details Updated");
    header("location:../companyDetails");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../companyDetails");
  }
}

//IS_1045
if(isset($updateBlocksAjax) && $updateBlocksAjax=="yes") {

  $block_master_qry=$d->select("block_master","   block_sort=".$block_sort."  and   society_id  ='$society_id'  and  block_id != '$block_id' ");
  
  if(mysqli_num_rows($block_master_qry) >0 ){
   echo "error~Duplicate";exit;
 } else {
   $m->set_data('block_sort',$block_sort);
   $a5 =array(
     'block_sort'=> $m->get_data('block_sort'),
   );

   $q=$d->update("block_master",$a5 ,"block_id='$block_id' ");
   if($q>0) {
     // $_SESSION['msg']="Block Name Updated";
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$blockLngName Order Updated");
     echo  "success~Success"; exit;
   } else {
    echo  "error~Failure"; exit;
  }
}
}
  //IS_1045


if(isset($updateBlocks)) {

//IS_1045
   $block_master_dupli=$d->select("block_master","  block_sort=".$block_sort."     and block_id !=".$block_id." and   society_id  ='$society_id'   ");
 
   if(mysqli_num_rows($block_master_dupli) >0 ){
      $_SESSION['msg1']="Duplicate $blockLngName Order";
      header("location:../branches"); exit;
   }
//IS_1045

  $m->set_data('block_name',$block_name);
  $m->set_data('block_sort',$block_sort);
  
  $a5 =array(
    'block_name'=> $m->get_data('block_name'),
    'block_sort'=> $m->get_data('block_sort'),
  );

  $q=$d->update("block_master",$a5 ,"block_id='$block_id' AND society_id='$society_id'");
  if($q>0) {
    $_SESSION['msg']="$blockLngName Name Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$blockLngName Name Updated");
    header("location:../branches");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../branches");
  }
}

if(isset($deleteBlock)) {
 $q=$d->delete("block_master","block_id='$deleteBlock' AND society_id='$society_id'");
 $q=$d->delete("floors_master","block_id='$deleteBlock' AND society_id='$society_id'");
 $q=$d->delete("unit_master","block_id='$deleteBlock' AND society_id='$society_id'");
 $q=$d->delete("users_master","block_id='$deleteBlock' AND society_id='$society_id'");
 if($q>0) {
  $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$blockLngName Deleted");
  echo 1;
} else {
  echo 0;
}
}

  if(isset($updateFloorSort) && $updateFloorSort == "updateFloorSort") 
  {
    $block_master_qry = $d->select("floors_master","floor_sort = ".$floor_sort." AND floor_id != '$floor_id'");

   
      $m->set_data('floor_sort',$floor_sort);
      $a5 = array(
        'floor_sort'=> $m->get_data('floor_sort'),
      );

      $q = $d->update("floors_master",$a5 ,"floor_id='$floor_id'");
      if($q > 0) 
      {
        $floor_data = $d->selectRow("floor_name","floors_master","floor_id = '$floor_id'");
        $floor_arr = $floor_data->fetch_assoc();
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$floor_arr[floor_name] Order Updated");
        echo  "success~Success"; exit;
      } 
      else 
      {
        echo  "error~Failure"; exit;
      }
  }
  if(isset($updateBLockLatLong) && $updateBLockLatLong == "updateBLockLatLong") 
  {
      $blockLatLong = array(
        'block_geofence_latitude'=>$block_latitude,
        'block_geofence_longitude'=>$block_longitude
      );
      $q = $d->update("block_master",$blockLatLong ,"block_id='$update_block_id'");
      if($q > 0) 
      {

        $block_data = $d->selectRow("*","block_master","block_id = '$update_block_id'");
        $block_datas = $block_data->fetch_assoc();
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$block_datas[block_name] Lat LOng Update");
        header("location:../branches");

      } 
      else 
      {
        header("location:../branches");
      }
  }


}
else{
  header('location:../login');
}
?>
