<?php
include '../common/objectController.php';
extract($_POST);
 
if (isset($_POST) && !empty($_POST)) {
 
 
  //IS_1328
  if (isset($_POST['addDiscussion'])) {
    
      $maxsize    = 10097152;
       $file11=$_FILES["discussion_file"]["tmp_name"];
       if (file_exists($file11)) {

        if(($_FILES['discussion_file']['size'] >= $maxsize) || ($_FILES["discussion_file"]["size"] == 0)) {
            $_SESSION['msg1']="Document too large. Must be less than 10 MB";
            header("location:../discussionForum");
            exit();
        }   

        $extId = pathinfo($_FILES['discussion_file']['name'], PATHINFO_EXTENSION);
         $extAllow=array("pdf","doc","docx","csv","xls","xlsx","jpg","png","jpeg");
        if(in_array($extId,$extAllow)) {
           $temp = explode(".", $_FILES["discussion_file"]["name"]);
            $discussion_file = "File_".$user_id.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["discussion_file"]["tmp_name"], "../../img/request/" . $discussion_file);
        } else {
           $_SESSION['msg1']="Invalid Document only Photo,Doc,CSV,XLSX & PDF are allowed.";
            header("location:../discussionForum");
          exit();
        }
      } else {
         $discussion_file ="";
      }

      $uploadedFile = $_FILES["discussion_photo"]["tmp_name"];
      $ext = pathinfo($_FILES['discussion_photo']['name'], PATHINFO_EXTENSION);
      if (file_exists($uploadedFile)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand() . $user_id;
          $dirPath = "../../img/request/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth > 1000) {
              $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
              $newImageWidth = $imageWidth * $newWidthPercentage / 100;
              $newImageHeight = $imageHeight * $newWidthPercentage / 100;
          } else {
              $newImageWidth = $imageWidth;
              $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

              case IMAGETYPE_PNG:
                  $imageSrc = imagecreatefrompng($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagepng($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              case IMAGETYPE_JPEG:
                  $imageSrc = imagecreatefromjpeg($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagejpeg($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              case IMAGETYPE_GIF:
                  $imageSrc = imagecreatefromgif($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagegif($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              default:

                  break;
          }
          $discussion_photo = $newFileName . "_discussion." . $ext;

          $notiUrl = $base_url . 'img/request/' . $discussion_photo;
      } else {
          $discussion_photo = '';
          $notiUrl = "";
      }
 

    $m->set_data('society_id',$society_id);
    $m->set_data('discussion_forum_title',test_input($discussion_forum_title));
    $m->set_data('discussion_forum_description',$discussion_forum_description);
    $m->set_data('created_date',date("Y-m-d H:i:s"));
    $m->set_data('admin_id',$_COOKIE['bms_admin_id']);
    $m->set_data('discussion_forum_for',$discussion_forum_for);
    $m->set_data('discussion_block_id',$discussion_block_id);
    $m->set_data('discussion_photo',test_input($discussion_photo));
    $m->set_data('discussion_file',test_input($discussion_file));
    $a = array(
      'society_id'=>$m->get_data('society_id'),
      'discussion_forum_title'=>$m->get_data('discussion_forum_title'),
      'discussion_forum_description'=>$m->get_data('discussion_forum_description'),
      'created_date'=>$m->get_data('created_date'),
      'admin_id'=>$m->get_data('admin_id'),
      'discussion_forum_for'=>$m->get_data('discussion_forum_for'),
      'discussion_block_id'=>$m->get_data('discussion_block_id'),
      'discussion_photo'=>$m->get_data('discussion_photo'),
      'discussion_file'=>$m->get_data('discussion_file'),
    );

    $q1=$d->insert("discussion_forum_master",$a);
    $discussion_forum_id = $con->insert_id;
    if(isset($_POST['user_id'])){
      for($i=0; $i<sizeof($_POST['user_id']); $i++){
        print_r($_POST['user_id'][$i]);
        $m->set_data('discussion_forum_id',$discussion_forum_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('user_id',$_POST['user_id'][$i]);

        $a2 = array(
          'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
          'society_id'=>$m->get_data('society_id'),
          'user_id'=>$m->get_data('user_id'),
        );

        $q2=$d->insert("discussion_forum_users",$a2);
      }

    }
    if($q1 > 0) {
      $qry ="";
      if(isset($_POST['user_id'])){
        $userList = implode(',', $_POST['user_id']);
        $qry =" and user_id IN ($userList)  AND delete_status=0";
        $append_query="users_master.user_id IN ($userList) AND users_master.delete_status=0";
      }
      else{
        if ($discussion_forum_for==0) {
          $append_query="users_master.delete_status=0";
        } else  {
          $qry =" and floor_id =$discussion_forum_for  AND delete_status=0";
          $append_query="users_master.floor_id =$discussion_forum_for  AND users_master.delete_status=0";
        }
      }
    
      

      $title='New Discussion Forum Added';
      $description= "By Admin $created_by";
      $d->insertUserNotificationWithId($society_id,$title,$description,"discussion","Discussion_1xxxhdpi.png",$append_query,$discussion_forum_id);


      $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");
      $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
      $nResident->noti("DiscussionFragment",$notiUrl,$society_id,$fcmArray,"New Discussion Forum Added","By Admin $created_by",$discussion_forum_id);
      $nResident->noti_ios("DiscussionVC",$notiUrl,$society_id,$fcmArrayIos,"New Discussion Forum Added","By Admin $created_by",$discussion_forum_id);
      $_SESSION['msg']="Discussion Forum Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Discussion Forum Added");
      header("location:../manageDiscussions");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../discussionForum");
    }
  }
  

  if (isset($_POST['editDiscussion'])) {

    $maxsize    = 10097152;
       $file11=$_FILES["discussion_file"]["tmp_name"];
       if (file_exists($file11)) {

        if(($_FILES['discussion_file']['size'] >= $maxsize) || ($_FILES["discussion_file"]["size"] == 0)) {
            $_SESSION['msg1']="Document too large. Must be less than 10 MB";
            header("location:../manageDiscussions");
            exit();
        }

        $extId = pathinfo($_FILES['discussion_file']['name'], PATHINFO_EXTENSION);
         $extAllow=array("pdf","doc","docx","csv","xls","xlsx","jpg","png","jpeg");
        if(in_array($extId,$extAllow)) {
           $temp = explode(".", $_FILES["discussion_file"]["name"]);
            $discussion_file = "File_".$user_id.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["discussion_file"]["tmp_name"], "../../img/request/" . $discussion_file);
        } else {
           $_SESSION['msg1']="Invalid Document only Photo,Doc,CSV,XLSX & PDF are allowed.";
            header("location:../manageDiscussions");
          exit();
        }
      } else {
         $discussion_file =$discussion_file_old;
      }
    
    $uploadedFile = $_FILES["discussion_photo"]["tmp_name"];
      $ext = pathinfo($_FILES['discussion_photo']['name'], PATHINFO_EXTENSION);
      if (file_exists($uploadedFile)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand() . $user_id;
          $dirPath = "../../img/request/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth > 1000) {
              $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
              $newImageWidth = $imageWidth * $newWidthPercentage / 100;
              $newImageHeight = $imageHeight * $newWidthPercentage / 100;
          } else {
              $newImageWidth = $imageWidth;
              $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

              case IMAGETYPE_PNG:
                  $imageSrc = imagecreatefrompng($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagepng($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              case IMAGETYPE_JPEG:
                  $imageSrc = imagecreatefromjpeg($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagejpeg($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              case IMAGETYPE_GIF:
                  $imageSrc = imagecreatefromgif($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagegif($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                  break;

              default:

                  break;
          }
          $discussion_photo = $newFileName . "_discussion." . $ext;

          $notiUrl = $base_url . 'img/request/' . $discussion_photo;
      } else {
          $discussion_photo = $discussion_photo_old;
          $notiUrl = "";
      }

      

    $m->set_data('society_id',$society_id);
    $m->set_data('discussion_forum_title',test_input($discussion_forum_title));
    $m->set_data('discussion_forum_description',$discussion_forum_description);
    $m->set_data('discussion_forum_for',$discussion_forum_for);
    $m->set_data('discussion_photo',test_input($discussion_photo));
    $m->set_data('discussion_file',test_input($discussion_file));
    $a = array(
      'society_id'=>$m->get_data('society_id'),
      'discussion_forum_title'=>$m->get_data('discussion_forum_title'),
      'discussion_forum_description'=>$m->get_data('discussion_forum_description'),
      'discussion_forum_for'=>$m->get_data('discussion_forum_for'),
      'discussion_photo'=>$m->get_data('discussion_photo'),
      'discussion_file'=>$m->get_data('discussion_file'),
    );

    $q1=$d->update("discussion_forum_master",$a,"discussion_forum_id='$discussion_forum_id'");
  
    if(isset($_POST['user_id'])){
      $q3=$d->delete("discussion_forum_users","discussion_forum_id='$discussion_forum_id'");
      for($i=0; $i<sizeof($_POST['user_id']); $i++){
        $m->set_data('discussion_forum_id',$discussion_forum_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('user_id',$_POST['user_id'][$i]);

        $a2 = array(
          'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
          'society_id'=>$m->get_data('society_id'),
          'user_id'=>$m->get_data('user_id'),
        );
        $q2=$d->insert("discussion_forum_users",$a2);
      }

    }
    if($q1 > 0) {
      $qry ="";
      if(isset($_POST['user_id'])){
        $userList = implode(',', $_POST['user_id']);
        $qry =" and user_id IN ($userList)  AND delete_status=0";
        $append_query="users_master.user_id IN ($userList) AND users_master.delete_status=0";
      }
      else{
        if ($discussion_forum_for==0) {
          $append_query="users_master.delete_status=0";
        } else  {
          $qry =" and floor_id =$discussion_forum_for  AND delete_status=0";
          $append_query="users_master.floor_id =$discussion_forum_for  AND users_master.delete_status=0";
        }
      }
    
      

      $title='New Discussion Forum Updated';
      $description= "By Admin $created_by";
      $d->insertUserNotificationWithId($society_id,$title,$description,"discussion","Discussion_1xxxhdpi.png",$append_query,$discussion_forum_id);


      $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");
      $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
      $nResident->noti("DiscussionFragment",$notiUrl,$society_id,$fcmArray,"Discussion Forum Updated","By Admin $created_by",$discussion_forum_id);
      $nResident->noti_ios("DiscussionVC",$notiUrl,$society_id,$fcmArrayIos,"Discussion Forum Updated","By Admin $created_by",$discussion_forum_id);
      $_SESSION['msg']="Discussion Forum Updated";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Discussion Forum Added");
      header("location:../manageDiscussions");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../discussionForum");
    }
  }

  if (isset($_POST['addComment'])) {
  

    $m->set_data('society_id',$society_id);
    $m->set_data('discussion_forum_id',$discussion_forum_id);
    $m->set_data('comment_messaage',$comment_messaage);
    $m->set_data('created_date',date("Y-m-d H:i:s"));
    $m->set_data('admin_id',$_COOKIE['bms_admin_id']);
    $a = array(
      'society_id'=>$m->get_data('society_id'),
      'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
      'comment_messaage'=>$m->get_data('comment_messaage'),
      'created_date'=>$m->get_data('created_date'),
      'admin_id'=>$m->get_data('admin_id'),
    );

  

    $q1=$d->insert("discussion_forum_comment",$a);
  
    if($q1 > 0) {
      $qry ="";
      if ($discussion_forum_for==0) {
        $append_query="users_master.delete_status=0";
      } else  {
        $qry =" and floor_id =$discussion_forum_for  AND delete_status=0";
        $append_query="users_master.floor_id =$discussion_forum_for  AND users_master.delete_status=0";
      }
      
    
    if ($active_status==0) {
      


        $title="$comment_messaage";
        $description= "Comment On Discussion Forum By Admin $created_by";
        $d->insertUserNotificationWithId($society_id,$title,$description,"discussion","Discussion_1xxxhdpi.png",$append_query,$discussion_forum_id);


        $muteArray=array();
        $qc11=$d->select("discussion_forum_mute,users_master","discussion_forum_mute.user_id=users_master.user_id AND  discussion_forum_mute.discussion_forum_id='$discussion_forum_id'");
        while ($muteData=mysqli_fetch_array($qc11)) {
            array_push($muteArray, $muteData['user_mobile']);
        }
      

        $ids = join("','",$muteArray);   

        $fcmArray=$d->get_android_fcm("users_master","user_mobile  IN ('$ids') AND user_token!='' AND society_id='$society_id' AND device='android' $qry ");
        $fcmArrayIos=$d->get_android_fcm("users_master","user_mobile  IN ('$ids') AND user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
        $nResident->noti("DiscussionFragment","",$society_id,$fcmArray,"$comment_messaage","Comment On Discussion Forum By Admin $created_by",$discussion_forum_id);
        $nResident->noti_ios("DiscussionVC","",$society_id,$fcmArrayIos,"$comment_messaage","Comment On Discussion Forum By Admin $created_by",$discussion_forum_id);

      }
      $_SESSION['msg']="Comment Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Comment Added");
      header("location:../discussionHistory?id=$discussion_forum_id");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../discussionHistory?id=$discussion_forum_id");
    }
  }
 
  if (isset($comment_id_delete)) {
    

      $q1=$d->delete("discussion_forum_comment","comment_id='$comment_id_delete' ");

      if ($q1>0) {
          $_SESSION['msg']="Comment Deleted";
         header("location:../discussionHistory?id=$discussion_forum_id");
      }else{
        $_SESSION['msg1']="Soenthing Wrong.";
         header("location:../discussionHistory?id=$discussion_forum_id");
      }
  } 


}
?>