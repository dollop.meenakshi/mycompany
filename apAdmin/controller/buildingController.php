<?php 
include '../common/objectController.php';
extract($_POST);

$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../../img/$language_id.xml");
$societyLngName = $xml->string->society;
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  

  // update building details
   if(isset($_POST['updateBuildingSingle']) ){



      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['socieaty_logo']['tmp_name'];
      $ext = pathinfo($_FILES['socieaty_logo']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$society_id;
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1800) {
            $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1800 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;

              switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagepng($tmp,$dirPath. $newFileName. "_logo.". $ext);
                    break;           

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagejpeg($tmp,$dirPath. $newFileName. "_logo.". $ext);
                    break;
                
                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagegif($tmp,$dirPath. $newFileName. "_logo.". $ext);
                    break;

                default:
                   $_SESSION['msg1']="Invalid Loog";
                    header("Location: ../companyDetails");
                    exit;
                    break;
                }
            $socieaty_logo= $newFileName."_logo.".$ext;
            } else {
                $socieaty_logo= $newFileName."_logo.".$ext;
                $temp = explode(".", $_FILES["socieaty_logo"]["name"]);
                move_uploaded_file($_FILES["socieaty_logo"]["tmp_name"], $dirPath.$socieaty_logo);

            }

         } else {
          $_SESSION['msg1']="Invalid Loog";
          header("location:../companyDetails");
          exit();
         }
        } else {
          $socieaty_logo= $socieaty_logo_old;
        
        }

      $uploadedFile = $_FILES['socieaty_cover_photo']['tmp_name'];
      $ext = pathinfo($_FILES['socieaty_cover_photo']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>800) {
            $newWidthPercentage= 800*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_cover.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_cover.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_cover.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Loog";
                header("Location: ../companyDetails");
                exit;
                break;
            }
            $socieaty_cover_photo= $newFileName."_cover.".$ext;
         } else {
          $_SESSION['msg1']="Invalid Cover";
          header("location:../companyDetails");
          exit();
         }
        } else {
          $socieaty_cover_photo= $socieaty_cover_photo_old;

        }

      $pan_number = strtoupper($pan_number);
      $gst_no = strtoupper($gst_no);
      $m->set_data('society_name',test_input($society_name_update));
     /*  $m->set_data('tracking_app_version_android',$tracking_app_version_android);
      $m->set_data('download_url_tracking_app',$download_url_tracking_app); */
      $m->set_data('society_based',test_input($society_based));
      $m->set_data('society_address',test_input($society_address_edit));
      $m->set_data('company_website',test_input($company_website));
      $m->set_data('secretary_mobile',test_input($secretary_mobile));
      $m->set_data('secretary_email',test_input($secretary_email));
      $m->set_data('society_pincode',test_input($society_pincode));
      $m->set_data('socieaty_logo',$socieaty_logo);
      $m->set_data('socieaty_cover_photo',$socieaty_cover_photo);
      $m->set_data('builder_name',test_input($builder_name));
      $m->set_data('builder_address',test_input($builder_address));
      $m->set_data('builder_mobile',test_input($builder_mobile));
      $m->set_data('builder_mobile_country_code',test_input($builder_mobile_country_code));
      $m->set_data('gst_no',test_input($gst_no));
      $m->set_data('currency',test_input($currency));
      $m->set_data('pan_number',test_input($pan_number));
      $m->set_data('financial_year_start',$financial_year_start);
      $m->set_data('financial_year_end',$financial_year_end);
      $m->set_data('auto_reject_vistor_minutes',test_input($auto_reject_vistor_minutes));
      $m->set_data('bill_cancel_minutes',test_input($bill_cancel_minutes));
      $m->set_data('complaint_reopen_minutes',test_input($complaint_reopen_minutes));
      $m->set_data('default_time_zone',test_input($default_time_zone));
      $m->set_data('society_latitude',test_input($society_latitude));
      $m->set_data('society_longitude',test_input($society_longitude));
      
      $a =array(
        'society_name'=> $m->get_data('society_name'),
        'society_based'=> $m->get_data('society_based'),
       /*  'tracking_app_version_android'=> $m->get_data('tracking_app_version_android'),
        'download_url_tracking_app'=> $m->get_data('download_url_tracking_app'), */
        'society_address'=> $m->get_data('society_address'),
        'secretary_mobile'=> $m->get_data('secretary_mobile'),
        'secretary_email'=> $m->get_data('secretary_email'),
        'company_website'=> $m->get_data('company_website'),
        'society_pincode'=>$m->get_data('society_pincode'),
        'socieaty_logo'=>$m->get_data('socieaty_logo'),
        'socieaty_cover_photo'=>$m->get_data('socieaty_cover_photo'),
        'builder_name'=>$m->get_data('builder_name'),
        'builder_address'=>$m->get_data('builder_address'),
        'builder_mobile'=>$m->get_data('builder_mobile'),
        'builder_mobile_country_code'=>$m->get_data('builder_mobile_country_code'),
        'gst_no'=>$m->get_data('gst_no'),
        'currency'=>$m->get_data('currency'),
        'pan_number'=>$m->get_data('pan_number'),
        'financial_year_start'=>$m->get_data('financial_year_start'),
        'financial_year_end'=>$m->get_data('financial_year_end'),
        'auto_reject_vistor_minutes'=>$m->get_data('auto_reject_vistor_minutes'),
        'bill_cancel_minutes'=>$m->get_data('bill_cancel_minutes'),
        'complaint_reopen_minutes'=>$m->get_data('complaint_reopen_minutes'),
        'default_time_zone'=>$m->get_data('default_time_zone'),
        'society_latitude'=>$m->get_data('society_latitude'),
        'society_longitude'=>$m->get_data('society_longitude'),
      );

      
      $q=$d->update("society_master",$a,"society_id='$society_id'");
      if($q>0) {
        $_COOKIE['society_name']=$society_name_update;
        $_SESSION['msg']="$societyLngName Details Updated.";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$societyLngName Details Updated.");
        header("location:../companyDetails");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../companyDetails");
      }
  }
    

  


  // update building details
   if(isset($_POST['addPaymentGetwat'])){


      if ($payment_getway_master_id==3) {
        $is_upi=1;
      }else {
        $is_upi=0;
      }

      if ($payment_getway_master_id==2) {
        $merchant_id = $RozerPaykeyId;
        $merchant_key = $SecretKey;
        $transaction_charges = $transaction_charges_razer;
      } else if ($payment_getway_master_id==3) {
        $merchant_id = $UpiId;
        $merchant_key = $MerchantAccount;
      }else if ($payment_getway_master_id==4) {
        $merchant_id = $flutterPublicKey;
        $merchant_key = $flutterSecretKey;
        $salt_key = $flutterEncryptionKey;
        $transaction_charges = $transaction_charges_flutter;
      }
      $m->set_data('society_id',$society_id);
      $m->set_data('payment_getway_master_id',$payment_getway_master_id);
      $m->set_data('merchant_id',test_input($merchant_id));
      $m->set_data('merchant_key',test_input($merchant_key));
      $m->set_data('salt_key',test_input($salt_key));
      $m->set_data('name',test_input($name));
      $m->set_data('is_upi',test_input($is_upi));
      $m->set_data('transaction_charges',test_input($transaction_charges));

       
      //28MARCH
      $a =array(
        'society_id'=> $m->get_data('society_id'),
          'payment_getway_master_id'=> $m->get_data('payment_getway_master_id'),
          'merchant_id'=>$m->get_data('merchant_id'),
          'merchant_key'=>$m->get_data('merchant_key'),
          'salt_key'=>$m->get_data('salt_key'),
          'name'=>$m->get_data('name'),
          'is_upi'=>$m->get_data('is_upi'),
          'transaction_charges'=>$m->get_data('transaction_charges'),
          
      );

      
        $q=$d->insert("society_payment_getway",$a);
       
        if($q>0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Details Added.");
          $_SESSION['msg']="Payment Gateway Details Added.";
          header("location:../paymentGatewaySetting");
        } else {
          $_SESSION['msg1']="Something Wrong";
          header("location:../paymentGatewaySetting");
        }
  }


  if (isset($admin_logout)) {
        if ($device=='android') {
          $nAdmin->noti_new($society_id,"", $token, 'Logout','Please Login Again', "");
        } else {
          $nAdmin->noti_ios_new($society_id,"", $token, 'Logout','Please Login Again', "");
        }

        
        $m->set_data('token', "");
        $m->set_data('device', "");
        $m->set_data('device_mac', "");

        $a = array(
            'token' => $m->get_data('token'),
            'device' => $m->get_data('device'),
            'updated_by' => $m->get_data('device_mac'),
        );
       
        $q=$d->update("bms_admin_master",$a,"admin_id='$admin_id_logout'");


        if($q>0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$logoutAdminName Admin App Logout");
          $_SESSION['msg']="$logoutAdminName Admin App Logout";
          header("location:../companyAdmins");
        } else {
          $_SESSION['msg1']="Something Wrong";
          header("location:../companyAdmins");
        }

  }

  if (isset($admin_id_mail)) {

    
    $q=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.admin_active_status=0 AND bms_admin_master.admin_id='$admin_id_mail'","");

      $data= mysqli_fetch_array($q);
      extract($data);

      $secretary_name = $admin_name;
      $secretary_mobile = $admin_mobile;
      $secretary_email = $admin_email;
      $forgotLink = $base_url.'apAdmin/';
      $society_nameMsg = substr($society_name, 0, 20).'.';
      $smsObj->send_welcome_message_admin($society_id,$secretary_name,$role_name,$society_nameMsg,$secretary_mobile,$secretary_email,$admin_password,$forgotLink,$country_code);
      $d->add_sms_log($secretary_mobile,"Admin Welcome Message",$society_id,$country_code,4);
          
        $to = $secretary_email;
        $admin_mobile= $secretary_mobile;
        $subject = "Account Created - $society_name";
        $admin_name = $secretary_name;
        include '../mail/newAdminMail.php';
        include '../mail.php';
        
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$logoutAdminName Admin App Logout");
        $_SESSION['msg']="Welcome Mail/Message sent to $admin_name";
       
        header("location:../companyAdmins");
        exit();
  }
//28march
  if(isset($_POST['editPaymentGetwat'])){
      if ($payment_getway_master_id==2) {
        $merchant_id = $RozerPaykeyId;
        $merchant_key = $SecretKey;
        $transaction_charges = $transaction_charges_razer;
      } else if ($payment_getway_master_id==3) {
        $merchant_id = $UpiId;
        $merchant_key = $MerchantAccount;
      }else if ($payment_getway_master_id==4) {
        $merchant_id = $flutterPublicKey;
        $merchant_key = $flutterSecretKey;
        $salt_key = $flutterEncryptionKey;
        $transaction_charges = $transaction_charges_flutter;
      }
      
      $m->set_data('society_id',$society_id);
      $m->set_data('payment_getway_master_id',$payment_getway_master_id);
      $m->set_data('merchant_id',test_input($merchant_id));
      $m->set_data('merchant_key',test_input($merchant_key));
      $m->set_data('salt_key',test_input($salt_key));
      $m->set_data('name',test_input($name));
      $m->set_data('transaction_charges',test_input($transaction_charges));
      
       
      //28MARCH
      $a =array(
         
        
          'merchant_id'=>$m->get_data('merchant_id'),
          'merchant_key'=>$m->get_data('merchant_key'),
          'salt_key'=>$m->get_data('salt_key'),
          'name'=>$m->get_data('name'),
          'transaction_charges'=>$m->get_data('transaction_charges'),
          
      );
      
        $q=$d->update("society_payment_getway",$a,"society_id='$society_id' and society_payment_getway_id = '$society_payment_getway_id'");
      
        if($q>0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Details Updated.");
          $_SESSION['msg']="Payment Gateway Details Updated.";
          header("location:../paymentGatewaySetting");
        } else {
          $_SESSION['msg1']="Something Wrong";
          header("location:../paymentGatewaySetting");
        }
  }

  if(isset($deletepaymentgateway) && $deletepaymentgateway=="deletepaymentgateway"){

       $totalUserd=$d->count_data_direct("society_payment_getway_id","balancesheet_master","society_id='$society_id' AND society_payment_getway_id='$society_payment_getway_id'");
       if ($totalUserd>0) {
        $_SESSION['msg1']="This Payment Gateway Used In Balancesheet ";
        header("Location: ../paymentGatewaySetting");
        exit();
       }


       $q=$d->delete("society_payment_getway","society_id='$society_id' and society_payment_getway_id ='$society_payment_getway_id' ");
        if($q>0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Details Deleted.");
          $_SESSION['msg']="Payment Gateway Details Deleted.";
          header("location:../paymentGatewaySetting");
        } else {
          $_SESSION['msg1']="Something Wrong";
          header("location:../paymentGatewaySetting");
        }
  }
  //28march
   if(isset($_POST['removePaymentGetway'])){
      
        $q=$d->delete("society_payment_getway","society_id='$society_id'");
     
        if($q>0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Details Removed.");
          $_SESSION['msg']="Payment Gateway Details Removed.";
          header("location:../paymentGatewaySetting");
        } else {
          $_SESSION['msg1']="Something Wrong";
          header("location:../paymentGatewaySetting");
        }
  }

  if (isset($_POST['admin_name_add'])) {

        $admin_mobile= (int)$admin_mobile;
        if (strlen($admin_mobile)<8 ) {
            $_SESSION['msg1']="Invalid Mobile Number";
            header("Location: ../companyAdmin");
            exit;
        }

       $qcb1=$d->select("bms_admin_master","admin_mobile='$admin_mobile' AND society_id='$society_id'");
       $dataq=mysqli_fetch_array($qcb1);
       $old_admin_id= $dataq['admin_id'];
       if ($dataq>0 && $dataq['admin_active_status']==0) {
          $_SESSION['msg1']="Mobile number is Already Register";
          header("location:../companyAdmin");
          exit();
       }

      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['admin_profile']['tmp_name'];
      $ext = pathinfo($_FILES['admin_profile']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          
          if ($imageWidth>800) {
            $newWidthPercentage= 800*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../companyAdmin");
                exit;
                break;
            }
            // /IS_1205
            $admin_profile= $newFileName."_user.".$ext;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../companyAdmin");
          exit();
         }
      } else {
        $admin_profile="user.png";
      }

        $bytes = openssl_random_pseudo_bytes(4);
        $admin_password = bin2hex($bytes);

        if (isset($_POST['menu_id'])) {
            $menu_id= implode(",", $_POST['menu_id']);
        }
        if (isset($_POST['pagePrivilege'])) {
            $pagePrivilege= implode(",", $_POST['pagePrivilege']);
        }
        if (isset($_POST['admin_app_right_id'])) {
            $adminAppPrivilege= implode(",", $_POST['admin_app_right_id']);
        }
        if (isset($_POST['complaint_category_id'])) {
            $complaint_category_id = implode(",", $_POST['complaint_category_id']);
        }

        if (isset($_POST['access_branchs'])) {
            $access_branchs = implode(",", $_POST['access_branchs']);
        }
        if (isset($_POST['access_departments'])) {
            $access_departments = implode(",", $_POST['access_departments']);
        }
        
      


        $m->set_data('society_id',$society_id);
        $m->set_data('role_id',$role_id);
        $m->set_data('admin_name_add',test_input($admin_name_add));
        $m->set_data('admin_email',test_input($admin_email));
        $m->set_data('admin_mobile',test_input($admin_mobile));
        $m->set_data('country_code',test_input($country_code));
        $m->set_data('admin_password',test_input($admin_password));
        $m->set_data('admin_profile',test_input($admin_profile));
        $m->set_data('complaint_category_id',test_input($complaint_category_id));
        $m->set_data('user_type',test_input($user_type));
        $m->set_data('mobile_private',test_input($mobile_private));
        $m->set_data('report_download_access',test_input($report_download_access));
        $m->set_data('access_branchs',$access_branchs);
        $m->set_data('access_departments',$access_departments);

        $a3 =array(
          'society_id'=> $m->get_data('society_id'),
          'role_id'=> $m->get_data('role_id'),
          'admin_name'=>$m->get_data('admin_name_add'),
          'admin_email'=>$m->get_data('admin_email'),
          'admin_mobile'=>$m->get_data('admin_mobile'),
          'country_code'=>$m->get_data('country_code'),
          'admin_password'=>$admin_password,
          'admin_profile'=>$m->get_data('admin_profile'),
          'created_date'=>date("Y-m-d"),
          'complaint_category_id'=>$m->get_data('complaint_category_id'),
          'user_type'=>$m->get_data('user_type'),
          'admin_active_status'=>0,
          'mobile_private'=>$m->get_data('mobile_private'),
          'report_download_access'=>$m->get_data('report_download_access'),
          'access_branchs'=>$m->get_data('access_branchs'),
          'access_departments'=>$m->get_data('access_departments'),
        );

       

  
        if ($dataq['admin_active_status']==1) {
         $q= $d->update("bms_admin_master",$a3,"admin_id='$old_admin_id'");
         $d->delete("admin_block_master","admin_id='$old_admin_id'");
         $d->delete("bms_admin_notification_master","admin_id='$old_admin_id'");
         $_SESSION['msg']="Committee Member Activated";
        } else {
         $q=$d->insert("bms_admin_master",$a3);
         $admin_id= $con->insert_id;
         $aUpdate =array(
          'order_number'=> $admin_id,
         );
         $d->update("bms_admin_master",$aUpdate,"admin_id='$admin_id'");
         $_SESSION['msg']="New Committee Member Added";
        }
         if($q>0) {
        
          if (isset($_POST['notificationAccess'])) {
            for ($i4=0; $i4 <count($_POST['notificationAccess']) ; $i4++) { 
                 $m->set_data('admin_notification_id',$_POST['notificationAccess'][$i4]);
                 $m->set_data('society_id', $society_id);
                 $m->set_data('admin_id', $admin_id);

                  $a2222 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'admin_id'=>$m->get_data('admin_id'),
                    'admin_notification_id'=>$m->get_data('admin_notification_id'),
                  );
                  echo "string";

                 $d->insert("bms_admin_notification_master",$a2222);
              }
          }


         
           $society_name = $_COOKIE['society_name'];

            
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","New $admin_name_add ($admin_mobile) Admin Added");

             $societyName = $_COOKIE['society_name'];
             $forgotLink= $base_url."apAdmin/";
            
            if ($country_code=='') {
              $country_code= "+91";
            }
            $smsObj->send_welcome_message_admin($society_id,$admin_name_add,$role_name,$society_name,$admin_mobile,$admin_email,$admin_password,$forgotLink,$country_code);
            $d->add_sms_log($admin_mobile,"Admin Welcome Message",$society_id,$country_code,4);
            

            $admin_name = $admin_name_add;
            $to = $admin_email;
            $subject = "Account Created for $societyName - MyCo";
           
            include '../mail/newAdminMail.php';
            include '../mail.php';
            header("location:../companyAdmins");
          } else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../companyAdmin");
          }


  }


  if (isset($_POST['admin_name_edit'])) {

        $admin_mobile= (int)$admin_mobile;
        if (strlen($admin_mobile)<8 ) {
            $_SESSION['msg1']="Invalid Mobile Number";
            header("Location: ../companyAdmins");
            exit;
        }

        $qcb1=$d->select("bms_admin_master","admin_mobile='$admin_mobile' AND society_id='$society_id'  AND admin_id!='$admin_id_edit'");
       $dataq=mysqli_fetch_array($qcb1);
       if ($dataq>0) {
          $_SESSION['msg1']="Mobile number is Already Register";
          header("location:../companyAdmins");
          exit();
       }

       $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['admin_profile']['tmp_name'];
      $ext = pathinfo($_FILES['admin_profile']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>800) {
            $newWidthPercentage= 800*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../companyAdmins");
                exit;
                break;
            }
            // /IS_1205
            $admin_profile= $newFileName."_user.".$ext;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../companyAdmins");
          exit();
         }
        } else {
          $admin_profile=$admin_profile_old;
        }

        if (isset($_POST['menu_id'])) {
            $menu_id= implode(",", $_POST['menu_id']);
        }
        if (isset($_POST['pagePrivilege'])) {
            $pagePrivilege= implode(",", $_POST['pagePrivilege']);
        }
        if (isset($_POST['admin_app_right_id'])) {
            $adminAppPrivilege= implode(",", $_POST['admin_app_right_id']);
        }
        if (isset($_POST['complaint_category_id'])) {
            $complaint_category_id = implode(",", $_POST['complaint_category_id']);
        }
        if (isset($_POST['access_branchs'])) {
            $access_branchs = implode(",", $_POST['access_branchs']);
        }
        if (isset($_POST['access_departments'])) {
            $access_departments = implode(",", $_POST['access_departments']);
        }
        
      
        $m->set_data('admin_name_edit',test_input($admin_name_edit));
        $m->set_data('role_id',test_input($role_id));
        $m->set_data('admin_email',test_input($admin_email));
        $m->set_data('admin_mobile',test_input($admin_mobile));
        $m->set_data('country_code',test_input($country_code));
        $m->set_data('complaint_category_id',test_input($complaint_category_id));
        $m->set_data('user_type',test_input($user_type));
        $m->set_data('admin_profile',test_input($admin_profile));
        $m->set_data('mobile_private',test_input($mobile_private));
        $m->set_data('report_download_access',test_input($report_download_access));
        $m->set_data('access_branchs',test_input($access_branchs));
        $m->set_data('access_departments',test_input($access_departments));

        $a3 =array(
          'admin_name'=>$m->get_data('admin_name_edit'),
          'role_id'=>$m->get_data('role_id'),
          'admin_email'=>$m->get_data('admin_email'),
          'admin_mobile'=>$m->get_data('admin_mobile'),
          'country_code'=>$m->get_data('country_code'),
          'created_date'=>date("Y-m-d"),
          'complaint_category_id'=>$m->get_data('complaint_category_id'),
          'user_type'=>$m->get_data('user_type'),
          'admin_profile'=>$m->get_data('admin_profile'),
          'mobile_private'=>$m->get_data('mobile_private'),
          'report_download_access'=>$m->get_data('report_download_access'),
          'access_branchs'=>$m->get_data('access_branchs'),
          'access_departments'=>$m->get_data('access_departments'),
        );
        

         $q=$d->update("bms_admin_master",$a3,"society_id='$society_id' AND admin_id='$admin_id_edit'");
         if($q>0) {

           $d->delete("bms_admin_notification_master","society_id='$society_id' AND admin_id='$admin_id_edit'");
           

          if (isset($_POST['notificationAccess'])) {
             for ($i4=0; $i4 <count($_POST['notificationAccess']) ; $i4++) { 
                 $m->set_data('admin_notification_id',$_POST['notificationAccess'][$i4]);
                 $m->set_data('society_id', $society_id);
                 $m->set_data('admin_id', $admin_id_edit);

                  $a2222 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'admin_id'=>$m->get_data('admin_id'),
                    'admin_notification_id'=>$m->get_data('admin_notification_id'),
                  );

                 $d->insert("bms_admin_notification_master",$a2222);
              }
          }
        
          
            $_SESSION['msg']="Admin Data Updated";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Admin $admin_name_edit ($admin_mobile) Data Updated");


            header("location:../companyAdmins");
          } else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../companyAdmin");
          }


  }



  if (isset($_POST['admin_id_delete'])) {

      if ($device=='android' && $token!='') {
        $nAdmin->noti_new($society_id,"", $token, 'Logout','Logout', "");
      } else if($token!='') {
        $nAdmin->noti_ios_new($society_id,"", $token, 'Logout', 'Logout', "");
      }

    
       $a5 =array(
         'admin_active_status'=>1,
         'device'=>'',
         'token'=>'',
        );

      $q=$d->update("bms_admin_master",$a5,"admin_id='$admin_id_delete'");

     
        $d->delete("bms_admin_notification_master","admin_id='$admin_id_delete'");
     if ($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Admin $adminNa ($adminMobile) Deleted");
        $_SESSION['msg']="User Deleted";
        header("location:../companyAdmins");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../companyAdmins");
      }

    # code...
  }

  if (isset($_POST['admin_id_restore'])) {

       $a5 =array(
         'admin_active_status'=>0
        );

      $q=$d->update("bms_admin_master",$a5,"admin_id='$admin_id_restore'");

     if ($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Admin $restoreAdminName Restore");
        $_SESSION['msg']="User Restore Successfully";
        header("location:../companyAdmins");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../companyAdmins");
      }

  }
  
 if (isset($_POST['updatePlan'])) {
    
      $q=$d->select("package_master","package_id='$package_id'","");
       $row=mysqli_fetch_array($q);
       $menu_id=$row['menu_id_package'];
       $pagePrivilege=$row['page_id_package'];
       $no_month=$row['no_of_month'];
       $package_name= $row['package_name'];
       $package_amount= $row['package_amount'];

    if ($amountReceivedType==1) {
       $plan_expire_date=date('Y-m-d', strtotime(' +'.$no_month.' months'));

       $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
       $hash = hash("sha512", $retHashSeq);
       $invoice_no= "0".date('ymdi').$udf1;

       $a122= array (
        'society_id'=> $societyId,
        'package_id'=> $package_id,
        'package_name'=> $package_name,
        'package_name'=> $package_name,
        'user_mobile'=> $admin_mobile,
        'payment_mode'=> "by Admin",
        'transection_amount'=> $amountReceived,
        'transection_date'=> date('Y-m-d h:m A'),
        'payment_status'=> "success",
        'payment_firstname'=> $society_name,
        'payment_phone'=> $secretary_mobile,
        'payment_email'=> $secretary_email,
        'invoice_no'=> $invoice_no,
         );

        $qq=$d->selectRow("payment_txnid","transection_master","payment_txnid='$txnid'");
        $oT=mysqli_fetch_array($qq);
        if($oT>0) {
           $d->update("transection_master",$a122,"payment_txnid='$txnid'");
        } else {
          $d->insert("transection_master",$a122);
        }
        
        $notDes="Received amount of $amountReceived INR";
    } else {
       $plan_expire_date=date ("Y-m-d", strtotime ($myDate ."-1 days"));
      
    }

      $m->set_data('societyId',$societyId);
      $m->set_data('package_id',$package_id);
      $m->set_data('trial_days',0);
      $m->set_data('plan_expire_date',$plan_expire_date);
      $awww =array(
          'society_id'=>$m->get_data('societyId'),
          'package_id'=>$m->get_data('package_id'),
          'trial_days'=>$m->get_data('trial_days'),
          'plan_expire_date'=>$m->get_data('plan_expire_date'),
          'last_renew_date'=>date('Y-m-d'),
      );

      $q=$d->update("society_master",$awww,"society_id='$societyId'");
      if ($q>0) {
          $a5 =array(
          'menu_id'=> $menu_id,
          'pagePrivilege'=> $pagePrivilege,
        );
          $q=$d->update("role_master",$a5,"society_id='$societyId' AND admin_type=1");
          
         $qSecretaryToken=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$societyId' AND role_master.admin_type=1 AND bms_admin_master.admin_active_status=0"); 
          $data_Secretary=mysqli_fetch_array($qSecretaryToken);
          $sos_Secretary_token=$data_Secretary['token'];
          if ($data_Secretary['device']=='android') {
            $nAdmin->noti_new($society_id,"",$sos_Secretary_token,"Your $societyLngName Plan Upgraded",$notDes,'');
          } else {
            $nAdmin->noti_ios_new($society_id,"",$sos_Secretary_token,"Your $societyLngName Plan Upgraded",$notDes,'');
          }
          $notiAry = array(
              'society_id'=>$societyId,
              'notification_tittle'=>"Your $societyLngName Plan Upgraded",
              'notification_description'=>$notDes,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>'',
              'admin_click_action '=>'planPayments'
            );
            $d->insert("admin_notification",$notiAry);

        $_SESSION['msg']="Plan Updated";
        header("location:../buildings");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../buildings");
      }
 }
 if (isset($lost_found_master_id)) {
       $q1=$d->select("lost_found_master","lost_found_master_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/lostFound/" . $data['lost_found_image'];
       unlink ($profileUrl); 

      $q=$d->delete("lost_found_master","lost_found_master_id='$lost_found_master_id'");
      if($q==TRUE){
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","lost and Found entry Deleted");
        $_SESSION['msg']="Deleted Successfully";
        header("location:../lostFound");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../lostFound");
      } # code...
 }


if(isset($_POST["forgot_email_admin_committee"])) {
  extract(array_map("test_input" , $_POST));
  $forgot_email_admin_committee=mysqli_real_escape_string($con, $forgot_email_admin_committee);
  
  $q=$d->select("bms_admin_master","admin_email='$forgot_email_admin_committee'");
  $data = mysqli_fetch_array($q); 
  extract($data);
  if ($data > 0) {
    $admin_name = $data['admin_name'];
    $admin_mobile = $data['admin_mobile'];
    $admin_email = $data['admin_email'];
    $country_code = $data['country_code'];
    
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    $forgotTime=date("Y/m/d");//Token Date
    $forgotLink=$base_url."apAdmin/resetPassword.php?t=".$token."&f=".$data['admin_id'];
    $m->set_data('token',$token);
    $m->set_data('token_date',$forgotTime);
    $a1= array ('forgot_token'=> $m->get_data('token'),
      'token_date'=> $m->get_data('token_date')
    );

    $d->update('bms_admin_master',$a1,"admin_email='$forgot_email_admin_committee'"); 


    $to = $forgot_email_admin_committee;
    $subject = "Forgot Password - MyCo";
    include '../mail/forgotPasswordMail.php';
    include '../mail.php';

    
    $smsObj->send_sms_password_reset($society_id,$admin_name,$admin_mobile,$forgotLink,$country_code);
    $d->add_sms_log($admin_mobile,"Admin Password Reset Sms",$society_id,$country_code,2);

        // Redirqt to homepage
    $_SESSION['msg']= "Password Reset Link sent to $to email id provided & mobile number";
    header("location:../companyAdmins");
  } 
  else {
    $_SESSION['msg1']= "Wrong Email";
    header("location:../companyAdmins");
  }
}

if(isset($_POST["updateChangebleValue"])) {
  extract(array_map("test_input" , $_POST));
  // echo "<Br>";
  // print_r($_POST['value_name_society']);  

  $key_name_sting = implode("~", $_POST['key_name']);
  $value_name_society_string = implode("~", $_POST['value_name_society']);


  $language_id = $_COOKIE['language_id'];
              $chLang = curl_init();
  curl_setopt($chLang, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
  curl_setopt($chLang, CURLOPT_POST, 1);
  curl_setopt($chLang, CURLOPT_POSTFIELDS,
                        "setChangebleLanguage=setChangebleLanguage&language_id=$language_id&society_id=$society_id&key_name=$key_name_sting&value_name_society=$value_name_society_string");

  curl_setopt($chLang, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($chLang, CURLOPT_HTTPHEADER, array(
  'key: bmsapikey'
  ));

  $server_output_lng = curl_exec($chLang);

  curl_close ($chLang);
  $server_output_lng=json_decode($server_output_lng,true);
  print_r($server_output_lng);

   if ($server_output_lng['status']==200) {
      $q=$d->selectRow("country_id","society_master","society_id='$society_id'");
      $bData=mysqli_fetch_array($q);
      $country_id  = $bData['country_id'];
          
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
                "getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id&language_id=$language_id");

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
      ));

      $server_output = curl_exec($ch);

      curl_close ($ch);
      $server_output=json_decode($server_output,true);

      $arrayCount= count($server_output['language_key']);

      $myFile = "../../img/$language_id.xml";
      $fh = fopen($myFile, 'w') or die("can't open file");
      $rss_txt = "";
      $rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
      $rss_txt .= "<rss version='2.0'>";
          
              $rss_txt .= '<string>';
          for ($i1=0; $i1 < $arrayCount ; $i1++) { 
            $key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

            $keyName  = $server_output['language_key'][$i1]['key_name'];
            

              $rss_txt .= "<$keyName>$key_value</$keyName>";
          }
              $rss_txt .= '</string>';
      $rss_txt .= '</rss>';

      fwrite($fh, $rss_txt);
      fclose($fh);

      $_SESSION['msg']= $server_output_lng['message'];
      header("location:../companyDetails");
   } else {
      $_SESSION['msg1']= $server_output_lng['message'];
      header("location:../companyDetails");
   }

}

if(isset($_POST["addGoogleDriveSetting"])) {
  print_r($_POST);
  $m->set_data('google_client_id', $google_client_id);
  $m->set_data('google_client_secret_key', $google_client_secret_key);

  $a1 = array(
    'google_client_id' => $m->get_data('google_client_id'),
    'google_client_secret_key' => $m->get_data('google_client_secret_key'),
  );

  $q = $d->update("society_master", $a1, "society_id ='$society_id'");

  if ($q == true) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Google Drive Setting Successfully");
    $_SESSION['msg'] = "Google Drive Setting Successfully";
    header("Location: ../googleDriveSetting");
  } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../googleDriveSetting");
  }
}

}
else{
  header('location:../login');
 }
 ?>
