<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    if (isset($_POST['addCourse'])) {     
        $m->set_data('course_title', $course_title);
        $m->set_data('society_id', $society_id);
        $m->set_data('course_description', $course_description);
        $m->set_data('course_created_date', date("Y-m-d H:i:s"));
        $m->set_data('course_created_by', $_COOKIE['bms_admin_id']);

        //////image upload

        $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG","pdf","PDF");
        $uploadedFile = $_FILES['course_image']['tmp_name'];
        $ext = pathinfo($_FILES['course_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["course_image"]["size"];
        $maxsize = 8000000;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 8MB.";
                    header("location:../courses");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../../img/course/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "course." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "course." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "course." . $ext);
                        break;
                    case IMAGETYPE_PDF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "course." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../courses");
                        exit;
                        break;
                }
                $hr_document = $newFileName . "course." . $ext;
                $notiUrl = $base_url . 'img/course/' . $hr_document;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../courses");
                exit();
            }
        } else {
            if($course_id =="")
            {
                $_SESSION['msg1'] = "Here Is No File ";
                header("location:../courses");
                exit();
            }
            
        }
       
        if(isset($hr_document))
        {
            $m->set_data('course_image', $hr_document);
        }
        else
        {
            $m->set_data('course_image', $course_image_old);
        }      
        $a1 = array(   
            'society_id' => $m->get_data('society_id'),
            'course_title' => $m->get_data('course_title'),
            'course_description' => $m->get_data('course_description'),
            'course_image' => $m->get_data('course_image'),
            'course_created_date' => $m->get_data('course_created_date'),
            'course_created_by' => $m->get_data('course_created_by'),
        );
        if (isset($course_id) && $course_id > 0) {
            $q = $d->update("course_master", $a1, "course_id ='$course_id'");
            $_SESSION['msg'] = "Course Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Updated Successfully");
        } else {
            $q = $d->insert("course_master", $a1);
            $_SESSION['msg'] = "Course Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Added Successfully");

        }
        if ($q == true) {
            header("Location: ../courses");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../courses");
        }

    }

}
