<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    if (isset($_POST['addCanteenVendor'])) {   
        
        $m->set_data('society_id', $society_id);
        $m->set_data('vendor_category_id', $vendor_category_id);
        $m->set_data('vendor_name', $vendor_name);
        $m->set_data('vendor_email', $vendor_email);
        $m->set_data('vendor_mobile_country_code', $vendor_mobile_country_code);
        $m->set_data('vendor_mobile', $vendor_mobile);
        $m->set_data('vendor_address', $vendor_address);
        $m->set_data('vendor_latitude', $vendor_latitude);
        $m->set_data('vendor_longitude', $vendor_longitude);
        $m->set_data('unit_measurement_id', $unit_measurement_id);

        //////image upload

        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['vendor_logo']['tmp_name'];
        $ext = pathinfo($_FILES['vendor_logo']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["vendor_logo"]["size"];
        $maxsize = 12582912;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 12MB.";
                    header("location:../addCanteenVendor");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/vendor_logo/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "vendor_logo." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "vendor_logo." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../addCanteenVendor");
                        exit;
                        break;
                }
                $vendor_logo = $newFileName . "vendor_logo." . $ext;
                $notiUrl = $base_url . 'img/vendor_logo/' . $vendor_logo;
                $m->set_data('vendor_logo', $vendor_logo);
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../addCanteenVendor");
                exit();
            }
        }   
        if(isset($vendor_logo))
        {
            $m->set_data('vendor_logo', $vendor_logo);
            if($vendor_logo_old != ''){
                unlink("../../img/vendor_logo/".$vendor_logo_old);
            }
        }
        else
        {
            $m->set_data('vendor_logo', $vendor_logo_old);
        }
            
        $a1 = array(   
            'society_id' => $m->get_data('society_id'),
            'vendor_category_id' => $m->get_data('vendor_category_id'),
            'vendor_name' => $m->get_data('vendor_name'),
            'vendor_email' => $m->get_data('vendor_email'),
            'vendor_mobile_country_code' => $m->get_data('vendor_mobile_country_code'),
            'vendor_mobile' => $m->get_data('vendor_mobile'),
            'vendor_address' => $m->get_data('vendor_address'),
            'vendor_latitude' => $m->get_data('vendor_latitude'),
            'vendor_longitude' => $m->get_data('vendor_longitude'),
            'vendor_logo' => $m->get_data('vendor_logo'),
        );

        if (isset($vendor_id) && $vendor_id > 0) {
            $q = $d->update("vendor_master", $a1, "vendor_id ='$vendor_id'");
            $_SESSION['msg'] = "Vendor Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Vendor Updated Successfully");
        } else {
            $q = $d->insert("vendor_master", $a1);
            $_SESSION['msg'] = "Vendor Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Vendor Added Successfully");

        }
        if ($q == true) {
            header("Location: ../canteenVendor");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addCanteenVendor");
        }

    }

}
