<?php
error_reporting(0);
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if (isset($_POST['shiftChangeApprove']))
    {
        $user_shift = array(
            'shift_time_id' => $new_shift_id
        );
        $q = $d->update("users_master", $user_shift, "user_id ='$user_id'");
        if ($q == true)
        {
            $title = "Shift Change Request Approved";
            $description = "Shift Changed From $oldShift to $newShift";
            if ($device == 'android')
            {
                $nResident->noti("", "", $society_id, $user_token, $title, $description, "");
            }
            else
            {
                $nResident->noti_ios("", "", $society_id, $user_token, $title, $description, "");
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee $user_full_name Shift Changed From $oldShift to $newShift");
            $d->delete("shift_change_request", "changed_by_id='$user_id'");
            $today = date('Y-m-d');
            $d->update("attendance_master", $user_shift, "attendance_date_start = '$today' AND attendance_date_end='0000-00-00'");
            $_SESSION['msg'] = "Shift Updated Successfully";
            header("Location: ../shiftChangeRequest");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../shiftChangeRequest");
        }
    }

    if (isset($_POST['rejectShiftRequest']))
    {
        $q = $d->delete("shift_change_request", "changed_by_id ='$user_id' AND shift_change_request_id='$shift_change_request_id'");
        if ($q == true)
        {
            $title = "Shift Change Request Rejected";
            $description = "Rejected by $created_by";
            if ($device == 'android')
            {
                $nResident->noti("", "", $society_id, $user_token, $title, $description, "");
            }
            else
            {
                $nResident->noti_ios("", "", $society_id, $user_token, $title, $description, "");
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee $user_full_name Shift Changed Request Rejected For $oldShift to $newShift");
            $_SESSION['msg'] = "Shift Change Request Rejected Successfully";
            header("Location: ../shiftChangeRequest");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../shiftChangeRequest");
        }
    }

    if (isset($_POST['addShiftTiming']))
    {
        if (isset($shift_time_id) && $shift_time_id > 0)
        {
            $appendSelfQuery = " AND shift_time_id != '$shift_time_id'";
        }
        $dq = $d->selectRow("shift_time_id", "shift_timing_master", "shift_name='$shift_name' AND society_id='$society_id' AND shift_start_time='$shift_start_time' AND shift_end_time='$shift_end_time' $appendSelfQuery");
        if (mysqli_num_rows($dq) > 0)
        {
            $_SESSION['msg1'] = "Shift $shift_name Already Added";
            header("Location: ../addShiftTiming");exit;
        }
        if ($_POST['week_off_days'] != "")
        {
            $week_off_days = implode(",", $_POST['week_off_days']);
        }
        if ($_POST['alternate_week_off'] != '')
        {
            $alternate_week_off = implode(",", $_POST['alternate_week_off']);
        }
        if ($_POST['alternate_weekoff_days'] != '')
        {
            $alternate_weekoff_days = implode(",", $_POST['alternate_weekoff_days']);
        }
        if ($max_shift_hour == 0)
        {
            $max_shift_hour = "";
        }
        if ($max_punch_out_time == "" && $max_shift_hour != 0)
        {
            $max_hour = 1;
            $max_punch_out_time = "";
        }
        else
        {
            $max_hour = 0;
            $max_shift_hour = "";
        }

        $m->set_data('society_id', $society_id);
        $m->set_data('shift_name', $shift_name);
        $m->set_data('shift_start_time', $shift_start_time);
        $m->set_data('early_out_reason', $early_out_reason);
        $m->set_data('late_in_reason', $late_in_reason);
        $m->set_data('shift_end_time', $shift_end_time);
        $m->set_data('lunch_break_start_time', $lunch_break_start_time);
        $m->set_data('lunch_break_end_time', $lunch_break_end_time);
        $m->set_data('tea_break_start_time', $tea_break_start_time);
        $m->set_data('tea_break_end_time', $tea_break_end_time);
        $m->set_data('week_off_days', $week_off_days);
        $m->set_data('half_day_time_start', $half_day_time_start);
        $m->set_data('halfday_before_time', $halfday_before_time);
        $m->set_data('late_time_start', $late_time_start);
        $m->set_data('early_out_time', $early_out_time);
        $m->set_data('maximum_in_out', $maximum_in_out);
        $m->set_data('maximum_halfday_hours', $maximum_halfday_hours);
        $m->set_data('minimum_hours_for_full_day', $minimum_hours_for_full_day);
        $m->set_data('has_altenate_week_off', $has_altenate_week_off);
        $m->set_data('alternate_week_off', $alternate_week_off);
        $m->set_data('alternate_weekoff_days', $alternate_weekoff_days);
        $m->set_data('is_multiple_punch_in', $is_multiple_punch_in);
        $m->set_data('max_hour', $max_hour);
        $m->set_data('max_shift_hour', $max_shift_hour);
        $m->set_data('max_punch_out_time', $max_punch_out_time);
        $m->set_data('take_out_of_range_reason', $take_out_of_range_reason);
        $m->set_data('shift_time_created_by', $_COOKIE['bms_admin_id']);

        if ($shift_start_time > $shift_end_time)
        {
            $shift_type = "Night";
            $today = date("Y-m-d");
            $prev_day = date("Y-m-d", strtotime('-1 day'));
        }
        else
        {
            $today = date("Y-m-d");
            $prev_day = date("Y-m-d");
            $shift_type = "Day";
        }
        $per_day_hour = $d->getTotalHours($prev_day, $today, $shift_start_time, $shift_end_time);
        $a1 = array(
            'society_id' => $m->get_data('society_id') ,
            'shift_name' => $m->get_data('shift_name') ,
            'shift_start_time' => $m->get_data('shift_start_time') ,
            'early_out_reason' => $m->get_data('early_out_reason') ,
            'late_in_reason' => $m->get_data('late_in_reason') ,
            'shift_end_time' => $m->get_data('shift_end_time') ,
            'lunch_break_start_time' => $m->get_data('lunch_break_start_time') ,
            'lunch_break_end_time' => $m->get_data('lunch_break_end_time') ,
            'tea_break_start_time' => $m->get_data('tea_break_start_time') ,
            'tea_break_end_time' => $m->get_data('tea_break_end_time') ,
            'per_day_hour' => $per_day_hour,
            'week_off_days' => $m->get_data('week_off_days') ,
            'half_day_time_start' => $m->get_data('half_day_time_start') ,
            'halfday_before_time' => $m->get_data('halfday_before_time') ,
            'maximum_in_out' => $m->get_data('maximum_in_out') ,
            'maximum_halfday_hours' => $m->get_data('maximum_halfday_hours') ,
            'minimum_hours_for_full_day' => $m->get_data('minimum_hours_for_full_day') ,
            //'late_time_start' => "00:".$late_time_start.":00",
            ///'early_out_time' =>  "00:".$early_out_time.":00",
            'has_altenate_week_off' => $m->get_data('has_altenate_week_off') ,
            'alternate_week_off' => $m->get_data('alternate_week_off') ,
            'shift_time_created_by' => $m->get_data('shift_time_created_by') ,
            'is_multiple_punch_in' => $m->get_data('is_multiple_punch_in') ,
            'max_hour' => $m->get_data('max_hour') ,
            'max_shift_hour' => $m->get_data('max_shift_hour') ,
            'max_punch_out_time' => $m->get_data('max_punch_out_time') ,
            'take_out_of_range_reason' => $m->get_data('take_out_of_range_reason') ,
            'shift_created_date' => date("Y-m-d H:i:s")
        );

        // $early_out_time = date(" H:i", strtotime("-".$early_out_time." minutes", strtotime($shift_end_time)));
        // $late_time_start = strtotime($shift_start_time.' + '.$late_time_start.' minute');
        if ($has_altenate_week_off == 1)
        {
            $a1['alternate_weekoff_days'] = $m->get_data('alternate_weekoff_days');
        }
        if ($shift_start_time > $shift_end_time)
        {
            $a1['shift_type'] = "Night";
        }
        else
        {
            $a1['shift_type'] = "Day";
        }
        if ($late_time_start != "" && $late_time_start > 0)
        {
            if ($late_time_start < 60)
            {
                $late_time_starts = "00:" . $late_time_start . ":00";
                $a1['late_time_start'] = $late_time_starts;
            }
            else
            {
                $hours = floor($late_time_start / 60);
                $minutes = ($late_time_start % 60);
                $late_time_starts = $hours . ":" . $minutes . ":00";
                $a1['late_time_start'] = date('h:i:s', strtotime($late_time_starts));
            }
        }
        else
        {
            $a1['late_in_reason'] = 0;
            $a1['late_time_start'] = "";
        }
        if ($late_time_start != "" && $late_time_start > 0)
        {
            if ($early_out_time < 60)
            {
                $early_out_times = "00:" . $early_out_time . ":00";
                $a1['early_out_time'] = $early_out_times;
            }
            else
            {
                $hours2 = floor($early_out_time / 60);
                $minutes2 = ($early_out_time % 60);
                $early_out_times = $hours2 . ":" . $minutes2 . ":00";
                $a1['early_out_time'] = date('h:i:s', strtotime($early_out_times));
            }
        }
        else
        {
            $a1['early_out_reason'] = 0;
            $a1['early_out_time'] = "";
        }
        if (isset($shift_time_id) && $shift_time_id > 0)
        {
            $q = $d->update("shift_timing_master", $a1, "shift_time_id ='$shift_time_id'");
            $_SESSION['msg'] = "Shift Timing Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Shift Timing Updated Successfully");
        }
        else
        {
            $q = $d->insert("shift_timing_master", $a1);
            $_SESSION['msg'] = "Shift $shift_name Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Shift $shift_name Added ");
        }
        if ($q == true)
        {
            header("Location: ../shiftTiming");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addShiftTiming");
        }
    }

    if (isset($_POST['addShiftTimingNew']))
    {
        mysqli_autocommit($con,FALSE);
        if(in_array(null, $shift_start_time, true) || in_array('', $shift_start_time, true))
        {
            $_SESSION['msg1'] = "Shift start time should not be empty";
            header("Location: ../addShift");exit;
        }
        if(in_array(null, $shift_end_time, true) || in_array('', $shift_end_time, true))
        {
            $_SESSION['msg1'] = "Shift end time should not be empty";
            header("Location: ../addShift");exit;
        }
        if($shift_name == "" || empty($shift_name))
        {
            $_SESSION['msg1'] = "Please enter shift name";
            header("Location: ../addShift");exit;
        }
        if(isset($week_off_days) && $week_off_days != "")
        {
            $week_off_days_str = implode(",",$week_off_days);
        }
        else
        {
            $week_off_days_str = '';
        }

        if ($has_altenate_week_off == 1)
        {
            $alternate_weekoff_str = implode(",", $alternate_week_off);
            $alternate_weekoff_days_str = implode(",", $alternate_weekoff_days);
        }
        else
        {
            $alternate_weekoff_str = "";
            $alternate_weekoff_days_str = "";
        }

        $shift_timing_master = [
            'society_id' => $society_id,
            'shift_name' => $shift_name,
            'week_off_days' => $week_off_days_str,
            'maximum_in_out' => $maximum_in_out,
            'late_in_reason' => $late_in_reason,
            'early_out_reason' => $early_out_reason,
            'has_altenate_week_off' => $has_altenate_week_off,
            'alternate_week_off' => $alternate_weekoff_str,
            'alternate_weekoff_days' => $alternate_weekoff_days_str,
            'is_multiple_punch_in' => $is_multiple_punch_in,
            'take_out_of_range_reason' => $take_out_of_range_reason
        ];
        if(isset($shift_time_id) && $shift_time_id != "")
        {
            $q = $d->update("shift_timing_master",$shift_timing_master,"shift_time_id = '$shift_time_id'");
        }
        else
        {
            $q = $d->insert("shift_timing_master",$shift_timing_master);
            $shift_time_id_in = $con->insert_id;
        }
        if($q)
        {
            $ss_c = 1;
            foreach($shift_start_time AS $k_ss => $v_ss)
            {
                if($ss_c < 7)
                {
                    $shift_start_time_new[$ss_c] = $v_ss;
                }
                else
                {
                    $shift_start_time_new[0] = $v_ss;
                }
                $ss_c++;
            }
            $_POST['shift_start_time'] = $shift_start_time_new;
            $shift_start_time = $shift_start_time_new;
            foreach($shift_start_time AS $key => $value)
            {
                $all_day[$week_days[$key]] = [
                    'shift_start_time' => $value,
                    'shift_end_time' => $shift_end_time[$key],
                    'lunch_break_start_time' => $lunch_break_start_time[$key],
                    'lunch_break_end_time' => $lunch_break_end_time[$key],
                    'tea_break_start_time' => $tea_break_start_time[$key],
                    'tea_break_end_time' => $tea_break_end_time[$key],
                    'half_day_time_start' => $half_day_time_start[$key],
                    'halfday_before_time' => $halfday_before_time[$key],
                    'late_time_start' => $late_time_start[$key],
                    'early_out_time' => $early_out_time[$key],
                    'maximum_halfday_hours' => $maximum_halfday_hours[$key],
                    'minimum_hours_for_full_day' => $minimum_hours_for_full_day[$key],
                    'max_tea_break' => $max_tea_break[$key],
                    'max_lunch_break' => $max_lunch_break[$key],
                    'max_personal_break' => $max_personal_break[$key],
                    'max_punch_out_time' => $max_punch_out_time[$key],
                    'max_shift_hour' => $max_shift_hour[$key],
                ];
                if ($value > $shift_end_time[$key])
                {
                    $shift_type = "Night";
                    $today = date("Y-m-d");
                    $prev_day = date("Y-m-d", strtotime('-1 day'));
                }
                else
                {
                    $today = date("Y-m-d");
                    $prev_day = date("Y-m-d");
                    $shift_type = "Day";
                }
                $per_day_hour = $d->getTotalHours($prev_day, $today, $value, $shift_end_time[$key]);

                if ($max_shift_hour[$key] == 0)
                {
                    $max_shift_hour[$key] = "";
                }
                if ($max_punch_out_time[$key] == "" && $max_shift_hour[$key] != 0)
                {
                    $max_hour = 1;
                    $max_punch_out_time[$key] = "";
                }
                else
                {
                    $max_hour = 0;
                    $max_shift_hour[$key] = "";
                }

                if(isset($week_off_days) && in_array($key,$week_off_days))
                {
                    $is_week_off_day = 1;
                }
                else
                {
                    $is_week_off_day = 0;
                }

                if ($late_time_start[$key] != "" && $late_time_start[$key] > 0)
                {
                    if ($late_time_start[$key] < 60)
                    {
                        $late_time_starts = "00:" . $late_time_start[$key] . ":00";
                    }
                    else
                    {
                        $hours = floor($late_time_start[$key] / 60);
                        $minutes = ($late_time_start[$key] % 60);
                        $late_time_starts = $hours . ":" . $minutes . ":00";
                    }
                }
                else
                {
                    $late_time_starts = "";
                }
                if ($early_out_time[$key] != "" && $early_out_time[$key] > 0)
                {
                    if ($early_out_time[$key] < 60)
                    {
                        $early_out_times = "00:" . $early_out_time[$key] . ":00";
                    }
                    else
                    {
                        $hours2 = floor($early_out_time[$key] / 60);
                        $minutes2 = ($early_out_time[$key] % 60);
                        $early_out_times = $hours2 . ":" . $minutes2 . ":00";
                    }
                }
                else
                {
                    $early_out_times = "";
                }

                $shift_day_master = [
                    'shift_day' => $key,
                    'shift_day_name' => $week_days[$key],
                    'society_id' => $society_id,
                    'shift_start_time' => $value,
                    'shift_end_time' => $shift_end_time[$key],
                    'lunch_break_start_time' => $lunch_break_start_time[$key],
                    'lunch_break_end_time' => $lunch_break_end_time[$key],
                    'tea_break_start_time' => $tea_break_start_time[$key],
                    'tea_break_end_time' => $tea_break_end_time[$key],
                    'per_day_hour' => $per_day_hour,
                    'half_day_time_start' => $half_day_time_start[$key],
                    'halfday_before_time' => $halfday_before_time[$key],
                    'late_time_start' => $late_time_starts,
                    'early_out_time' => $early_out_times,
                    'shift_type' => $shift_type,
                    'maximum_halfday_hours' => $maximum_halfday_hours[$key],
                    'minimum_hours_for_full_day' => $minimum_hours_for_full_day[$key],
                    'max_hour' => $max_hour,
                    'max_punch_out_time' => $max_punch_out_time[$key],
                    'max_shift_hour' => $max_shift_hour[$key],
                    'max_tea_break' => $max_tea_break[$key],
                    'max_lunch_break' => $max_lunch_break[$key],
                    'max_personal_break' => $max_personal_break[$key],
                    'is_week_off_day' => $is_week_off_day
                ];
                if(isset($shift_time_id) && $shift_time_id != "")
                {
                    $shift_day_master['updated_by'] = $_COOKIE['bms_admin_id'];
                    $shift_day_master['updated_date'] = date("Y-m-d H:i:s");
                    $q1 = $d->update("shift_day_master",$shift_day_master,"shift_day_id = '$shift_day_id[$key]'");
                    $d->insert_log("", $society_id, $_COOKIE['bms_admin_id'], $created_by, "$shift_name Shift updated");
                }
                else
                {
                    $shift_day_master['created_by'] = $_COOKIE['bms_admin_id'];
                    $shift_day_master['created_date'] = date("Y-m-d H:i:s");
                    $shift_day_master['shift_time_id'] = $shift_time_id_in;
                    $q1 = $d->insert("shift_day_master",$shift_day_master);
                    $shift_day_id[] = $con->insert_id;
                    $d->insert_log("", $society_id, $_COOKIE['bms_admin_id'], $created_by, "$shift_name Shift added");
                }
            }
            $comm = [];
            foreach($all_day AS $k => $v)
            {
                foreach($all_day AS $k1 => $v1)
                {
                    if($k != $k1 && $all_day[$k] == $all_day[$k1] && $all_day[$k] === $all_day[$k1])
                    {
                        $comm[$k][] = $k1;
                    }
                    else
                    {
                        $comm[$k][] = "";
                    }
                }
            }
            foreach($comm AS $k12 => $v12)
            {
                for ($i=0; $i <= 6; $i++)
                {
                    if(empty($v12[$i]))
                    {
                        unset($comm[$k12][$i]);
                    }
                }
            }
            foreach($comm AS $k123 => $v123)
            {
                $comm[$k123] = array_values($v123);
            }

            if(isset($shift_time_id) && $shift_time_id != "")
            {
                $ccc = 1;
            }
            else
            {
                $ccc = 0;
            }
            foreach($comm AS $k2 => $v2)
            {
                if(isset($v2[0]) && !empty($v2[0]) && !empty($v2) && count($v2) > 0)
                {
                    $grp = implode(",",$v2);
                }
                else
                {
                    $grp = "";
                }
                $app = $k2.",".$grp;
                $app = rtrim($app,",");
                $app_arr = explode(",",$app);
                $dataToSort = [];
                foreach($app_arr AS $ank => $anv)
                {
                    $dataToSort[] = (object)['event_day' => $anv];
                }
                $dayOrder = [
                    'Monday'    => 1,
                    'Tuesday'   => 2,
                    'Wednesday' => 3,
                    'Thursday'  => 4,
                    'Friday'    => 5,
                    'Saturday'  => 6,
                    'Sunday'    => 7,
                ];
                usort($dataToSort,function ($a, $b) use ($dayOrder) {
                    return $dayOrder[$a->event_day] > $dayOrder[$b->event_day];
                });
                $final = [];
                foreach($dataToSort AS $kkk => $vvv)
                {
                    $vvv = (array) $vvv;
                    $final[] = $vvv['event_day'];
                }
                $final_str = implode(",",$final);
                $aaa = [
                    'shift_group_name' => $final_str
                ];
                if(isset($shift_time_id) && $shift_time_id != "")
                {
                    if($ccc == 7)
                    {
                        $ccc = 0;
                    }
                    $q11 = $d->update("shift_day_master",$aaa,"shift_day_id = '$shift_day_id[$ccc]'");
                }
                else
                {
                    $q11 = $d->update("shift_day_master",$aaa,"shift_day_id = '$shift_day_id[$ccc]'");
                }
                $ccc++;
            }
            if($q11)
            {
                if(isset($shift_time_id) && $shift_time_id != "")
                {
                    $_SESSION['msg'] = "Shift successfully updated";
                }
                else
                {
                    $_SESSION['msg'] = "Shift successfully added";
                }
                mysqli_commit($con);
                header("Location: ../manageShift");exit;
            }
            else
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Something went wrong";
                header("Location: ../addShift");exit;
            }
        }
        else
        {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something went wrong";
            header("Location: ../addShift");exit;
        }
    }

    if (isset($_POST['getShiftDetailsNew']))
    {
        $q = $d->selectRow("sdm.*","shift_day_master AS sdm","sdm.society_id = '$society_id' AND sdm.shift_time_id = '$shift_time_id'");
        $data = [];
        while($row = $q->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);exit;
    }
}
?>
