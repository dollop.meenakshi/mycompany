<?php 
 include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


// $admin_user_id=$_SESSION['admin_user_id'];  
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
	if(isset($updateProfile)){
      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['profile_image']['tmp_name'];
      $ext = pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>400) {
            $newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../profile");
                exit;
                break;
            }
            $profile_image= $newFileName."_user.".$ext;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../profile");
          exit();
         }
        } else {
          $profile_image= $profile_image_old;
        }        

        $q111=$d->select("bms_admin_master","admin_email='$admin_email' AND admin_id!='$_COOKIE[bms_admin_id]'");
        if (mysqli_num_rows($q111)>0) {
            $_SESSION['msg1']="Email Already Register";
          header("location:../profile");
          exit();
        }  

         $q111=$d->select("bms_admin_master","admin_mobile='$admin_mobile' AND admin_id!='$_COOKIE[bms_admin_id]'");
        if (mysqli_num_rows($q111)>0) {
            $_SESSION['msg1']="Mobile Number Already Register";
          header("location:../profile");
          exit();
        }  

          $m->set_data("admin_name",test_input($admin_name));
          $m->set_data("admin_email",test_input($admin_email));
          $m->set_data("admin_mobile",test_input($admin_mobile));
          $m->set_data("country_code",test_input($country_code));
          $m->set_data("profile_image",test_input($profile_image));
          $a = array(
                    'admin_name'=>$m->get_data('admin_name'),
                    'admin_email'=>$m->get_data('admin_email'),
                    'admin_mobile'=>$m->get_data('admin_mobile'),
                    'country_code'=>$m->get_data('country_code'),
                    'admin_profile'=>$m->get_data('profile_image'),
                  ); 

     $q_temp=$d->update("bms_admin_master",$a,"admin_id='$_COOKIE[bms_admin_id]'");
     if($q_temp>0){
       setcookie('admin_profile', $profile_image, time() + (60 ), "/$cookieUrl"); // 86400 = 1 day
       setcookie('admin_name', $admin_name, time() + (60 ), "/$cookieUrl"); // 86400 = 1 day
       setcookie('admin_email', $admin_email, time() + (60 ), "/$cookieUrl"); // 86400 = 1 day
      
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Profile successfully updated.");

       $_SESSION['msg']="Profile successfully updated.";
       header("location:../profile");
     }
     else{
       $_SESSION['msg1']="Something Wrong";
       header("location:../profile");
     }
                
  }

  // Change Password
  if(isset($_POST["passwordChange"])) {
      extract(array_map("test_input" , $_POST));
        if ($password== $password2) {
            $q = $d->select("bms_admin_master","admin_id='$_COOKIE[bms_admin_id]'");
            $data = mysqli_fetch_array($q);
            if ($data["admin_password"]==$old_password) {
              
            $m->set_data('password',$password);
                $a1= array (
                  'admin_password'=> $m->get_data('password'),
                  );
                $insert=$d->update('bms_admin_master',$a1,"admin_id='$_COOKIE[bms_admin_id]'"); 
                if ($insert == true) {
                    $_SESSION['msg']= "Password Changed Successfully..!";
                    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Password Changed Successfully");
                  
                    $to = $data['admin_email'];
                    $subject = "MyCo Admin Panel Password Changed Successfully";
                    include '../mail/passwordChangesuccess.php';
                   
                    include '../mail.php';

                    $d->insert_log("","0","$_COOKIE[bms_admin_id]","$created_by","Password Changed Successfully");

                  $_SESSION['msg']= "Password Changed Successfully";
                  header("location:../profile");
                }else{
                    $_SESSION['msg1']= "Somthig wrong..";
                    header("location:../profile");
                }
            }else{
                $_SESSION['msg1']= "Old Password is wrong!";
                header("location:../profile");
            }
      } else {
        //IS_592
         //$_SESSION['msg1']= "Comirm Password is wrong";
        $_SESSION['msg1']= "confirm Password is wrong";
         
         header("location:../profile");
      }  
    }


  if (isset($updateVerUser)) {
    
     $file = $_FILES['back_banner']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['back_banner']['size'] >= $maxsize) || ($_FILES["back_banner"]["size"] == 0)) {
                 $_SESSION['msg1']=" Photo too large. File must be less than 1 MB.";
                 header("location:../appVer");
                 exit();
            }
            if(!in_array($_FILES['back_banner']['type'], $acceptable) && (!empty($_FILES["back_banner"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../appVer");
                 exit();
            }

            $image_Arr = $_FILES['back_banner'];   
            $temp = explode(".", $_FILES["back_banner"]["name"]);
            $back_banner = rand().'.' . end($temp);
            move_uploaded_file($_FILES["back_banner"]["tmp_name"], "../../img/backBanner/".$back_banner);
       } else {
        $back_banner=$back_banner_old;
       }

        $m->set_data('version_code',$version_code);
        $m->set_data('back_banner',$back_banner);
        $m->set_data('modify_date',date('Y-m-d H:i'));

         $a1= array (
            
          'version_code'=> $m->get_data('version_code'),
          'version_name'=> $m->get_data('version_code'),
          'modify_date'=> $m->get_data('modify_date'),
          'back_banner'=> $m->get_data('back_banner'),
        );


    $q=$d->update("version_master",$a1,"version_app='$version_app' AND mobile_app='$mobile_app'");
    if($q==TRUE) {
      $_SESSION['msg']="Data Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","App Version Updated");
      header("Location: ../appVer");
    } else {
       $_SESSION['msg1']="Something Wrong";
      header("Location: ../appVer");
    }

  }

    if (isset($updateAdvertisement)) {
    
     $file = $_FILES['advertisement_url']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['advertisement_url']['size'] >= $maxsize) || ($_FILES["advertisement_url"]["size"] == 0)) {
                 $_SESSION['msg1']=" Photo too large. File must be less than 2 MB.";
                 header("location:../appVer");
                 exit();
            }
            if(!in_array($_FILES['advertisement_url']['type'], $acceptable) && (!empty($_FILES["advertisement_url"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../appVer");
                 exit();
            }

            $image_Arr = $_FILES['advertisement_url'];   
            $temp = explode(".", $_FILES["advertisement_url"]["name"]);
            $advertisement_url = rand().'.' . end($temp);
            move_uploaded_file($_FILES["advertisement_url"]["tmp_name"], "../../img/sliders/".$advertisement_url);
       } else {
        $advertisement_url=$advertisement_url_old;
       }

        $m->set_data('active_status',$active_status);
        $m->set_data('view_status',$view_status);
        $m->set_data('advertisement_url',$advertisement_url);

         $a1= array (
            
          'advertisement_url'=> $m->get_data('advertisement_url'),
          'active_status'=> $m->get_data('active_status'),
          'view_status'=> $m->get_data('view_status'),
        );


    $q=$d->update("advertisement_master",$a1,"");
    if($q==TRUE) {
      $_SESSION['msg']="Data Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Main Banner Updated");
      header("Location: ../appVer");
    } else {
       $_SESSION['msg1']="Something Wrong";
      header("Location: ../appVer");
    }

  }
//IS_577
  if($validatePassword){
    $q = $d->select("bms_admin_master","admin_id='$_COOKIE[bms_admin_id]'");
            $data = mysqli_fetch_array($q);
            if ($data["admin_password"]==$old_password) {
              echo 'true' ;exit; 
            } else {
              echo 'false' ;exit;
            }
  }
  //IS_577
}
?>