<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
   
    if (isset($_POST['addAdminPin'])) {    
        if($admin_pin == $confirm_admin_pin){
            $m->set_data('admin_pin', test_input($admin_pin));

            $a1 = array('admin_pin' => $m->get_data('admin_pin'));
            
            $q = $d->update("bms_admin_master", $a1, "admin_id ='$admin_id'");
            $_SESSION['msg'] = "Admin Pin Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Admin Pin Updated Successfully");

            if ($q == true) {
                header("Location: ../faceDetectionAppSetting");
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../faceDetectionAppSetting");
            }
        }else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../faceDetectionAppSetting");
        }
        

    }
    if (isset($_POST['editFaceAppDevice'])) {  
       
        if($face_app_device_id>0){
            if(isset($_POST['block_ids']) && count($_POST['block_ids'])>0){
                $m->set_data('block_ids', implode(',',$_POST['block_ids']));
            }else{
                $m->set_data('block_ids',0);
            }
            if(isset($_POST['floor_ids']) && count($_POST['floor_ids'])>0){
                $m->set_data('floor_ids', implode(',',$_POST['floor_ids']));
            }else{
                $m->set_data('floor_ids',0);
            }
           
            $m->set_data('device_location', test_input($device_location));
            $a1 = array(
                'block_ids' => $m->get_data('block_ids'),
                'floor_ids' => $m->get_data('floor_ids'),
                'device_location' => $m->get_data('device_location'),
                'updated_by' => $_COOKIE['bms_admin_id'],
                'updated_date' => date('Y-m-d H:i:s'),
            );
            
            $q = $d->update("face_app_device_master", $a1, "face_app_device_id  ='$face_app_device_id'");
            $_SESSION['msg'] = "Face App Device Update  Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Face App Device Updated Successfully");

            if ($q == true) {

                $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' AND face_app_device_id  ='$face_app_device_id'");
                $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' AND face_app_device_id  ='$face_app_device_id'");
                $title = "sync_data";
                $description = "Face Data Updated on ".date('d-M-y h:i A');
                $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
                $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");


               
                header("Location: ../faceDetectionAppSetting");
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../faceDetectionAppSetting");
            }
        }else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../faceDetectionAppSetting");
        }
        

    }
    if (isset($_POST['logoutFaceApp'])) {  
       
        if($face_app_device_id>0){
            $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' AND face_app_device_id  ='$face_app_device_id'");
            $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' AND face_app_device_id  ='$face_app_device_id'");
            $title = "logout";
            $description = "Face Data Updated on ".date('d-M-y h:i A');
            $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
            $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

            
            $a1 = array(
                'user_token' => '',
                'updated_by' => $_COOKIE['bms_admin_id'],
                'updated_date' => date('Y-m-d H:i:s'),
            );
            
            $q = $d->update("face_app_device_master", $a1, "face_app_device_id  ='$face_app_device_id'");
            $_SESSION['msg'] = "Face App Device Update  Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Face App Device Updated Successfully");

            if ($q == true) {


                header("Location: ../faceDetectionAppSetting");
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../faceDetectionAppSetting");
            }
        }else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../faceDetectionAppSetting");
        }
        

    }

    if (isset($_POST['reloadDevice'])) {  
       
        if($face_app_device_id>0){

            $a1 = array(
                'updated_by' => $_COOKIE['bms_admin_id'],
                'updated_date' => date('Y-m-d H:i:s'),
                'last_syc_date' => date('Y-m-d H:i:s'),
            );
            
            $q = $d->update("face_app_device_master", $a1, "face_app_device_id  ='$face_app_device_id'");
                
                $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' AND face_app_device_id  ='$face_app_device_id'");
                $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' AND face_app_device_id  ='$face_app_device_id'");
                $title = "sync_data";
                $description = "Face Data Updated on ".date('d-M-y h:i A');
                $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
                $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");


            if ($q == true) {

               
                $_SESSION['msg'] = "Face App Device Refresh Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Face App Device Refresh Successfully");


                header("Location: ../faceDetectionAppSetting");
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../faceDetectionAppSetting");
            }
        }else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../faceDetectionAppSetting");
        }
        

    }

}
