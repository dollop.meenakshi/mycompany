<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
  if(isset($_POST['addCanteenProductCategory'])){
    $qv = $d->selectRow('vendor_category_id',"vendor_master","vendor_id='$vendor_id'");
    $vendordata = mysqli_fetch_assoc($qv);  
    //print_r($vendordata);die;

     $m->set_data('society_id',$society_id);
     $m->set_data('vendor_id',$vendor_id);
     $m->set_data('vendor_category_id',$vendordata['vendor_category_id']);   
     $m->set_data('vendor_product_category_name',$vendor_product_category_name); 
      
    
    //////image upload

        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['vendor_product_category_image']['tmp_name'];
        $ext = pathinfo($_FILES['vendor_product_category_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["vendor_product_category_image"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../canteenCategory");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $vendor_id;
                $dirPath = "../../img/vendor_category/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "vendor_category." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "vendor_category." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../canteenCategory");
                        exit;
                        break;
                }
                $image = $newFileName . "vendor_category." . $ext;
                $notiUrl = $base_url . 'img/vendor_category/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../canteenCategory");
                exit();
            }
        } else {
            if($vendor_product_category_id  =="")
            {
                $_SESSION['msg1'] = "Here Is No File ";
                header("location:../canteenCategory");
                exit();
            }
            
        } 
        if(isset($image) && $image !="" )
        {
            $m->set_data('vendor_product_category_image', $image);

        }
        else
        {
            $m->set_data('vendor_product_category_image', $vendor_product_category_image_old);

        } 
        
            
     $a1 = array(
      'society_id'=>$m->get_data('society_id'),
      'vendor_id'=>$m->get_data('vendor_id'),
      'vendor_category_id'=>$m->get_data('vendor_category_id'), 
      'vendor_product_category_name'=>$m->get_data('vendor_product_category_name'),  
      'vendor_product_category_image'=>$m->get_data('vendor_product_category_image'),  
    );  



     if(isset($vendor_product_category_id) && $vendor_product_category_id>0 )
     {
     $q=$d->update("vendor_product_category_master",$a1,"vendor_product_category_id ='$vendor_product_category_id'");
      $_SESSION['msg']="Canteen Product Category Updated Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Canteen Product Category Updated Successfully");
     }
     else{
      $q=$d->insert("vendor_product_category_master",$a1);
      $_SESSION['msg']="Canteen Product Category Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Canteen Product Category Added Successfully");

     }
     
     if($q==TRUE) {     
      header("Location: ../canteenCategory");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../canteenCategory");
   }

 }



 if(isset($_POST['addStationaryProductCategory'])){

    $qv = $d->selectRow('vendor_category_id',"vendor_master","vendor_id='$vendor_id'");
    $vendordata = mysqli_fetch_assoc($qv);  
    //print_r($vendordata);die;

     $m->set_data('society_id',$society_id);
     $m->set_data('vendor_id',$vendor_id);
     $m->set_data('vendor_category_id',$vendordata['vendor_category_id']);   
     $m->set_data('vendor_product_category_name',$vendor_product_category_name); 
      
    
    //////image upload

        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['vendor_product_category_image']['tmp_name'];
        $ext = pathinfo($_FILES['vendor_product_category_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["vendor_product_category_image"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../stationaryCategory");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $vendor_id;
                $dirPath = "../../img/vendor_category/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "vendor_category." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "vendor_category." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../stationaryCategory");
                        exit;
                        break;
                }
                $image = $newFileName . "vendor_category." . $ext;
                $notiUrl = $base_url . 'img/vendor_category/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../stationaryCategory");
                exit();
            }
        } else {
            if($vendor_product_category_id  =="")
            {
                $_SESSION['msg1'] = "Here Is No File ";
                header("location:../stationaryCategory");
                exit();
            }
            
        } 
        if(isset($image) && $image !="" )
        {
            $m->set_data('vendor_product_category_image', $image);

        }
        else
        {
            $m->set_data('vendor_product_category_image', $vendor_product_category_image_old);

        } 
        
            
     $a1 = array(
      'society_id'=>$m->get_data('society_id'),
      'vendor_id'=>$m->get_data('vendor_id'),
      'vendor_category_id'=>$m->get_data('vendor_category_id'), 
      'vendor_product_category_name'=>$m->get_data('vendor_product_category_name'),  
      'vendor_product_category_image'=>$m->get_data('vendor_product_category_image'),  
    );  



     if(isset($vendor_product_category_id) && $vendor_product_category_id>0 )
     {
     $q=$d->update("vendor_product_category_master",$a1,"vendor_product_category_id ='$vendor_product_category_id'");
      $_SESSION['msg']="Stationery Product Category Updated Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Stationery Product Category Updated Successfully");
     }
     else{
      $q=$d->insert("vendor_product_category_master",$a1);
      $_SESSION['msg']="Stationery Product Category Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Stationery Product Category Added Successfully");

     }
     
     if($q==TRUE) {     
      header("Location: ../stationaryCategory");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../stationaryCategory");
   }

 }

}
?>