<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addLeaveType'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('leave_type_name', $leave_type_name);
        $m->set_data('leave_apply_on_date', $leave_apply_on_date);
        $m->set_data('leave_type_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('leave_type_created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'leave_type_name' => $m->get_data('leave_type_name'),
            'leave_apply_on_date' => $m->get_data('leave_apply_on_date'),
            'leave_type_created_by' => $m->get_data('leave_type_created_by'),
            'leave_type_created_date' => $m->get_data('leave_type_created_date'),

        );
        if (isset($leave_type_id) && $leave_type_id > 0) {
            $q = $d->update("leave_type_master", $a1, "leave_type_id ='$leave_type_id'");
            $_SESSION['msg'] = "Leave Type Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Type Updated Successfully");
        } else {
            $q = $d->insert("leave_type_master", $a1);
            $_SESSION['msg'] = "Leave Type Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Type Added Successfully");

        }
        if ($q == true) {
            header("Location: ../leaveType");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveType");
        }

    }

}
