<?php 

include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )
{


if(isset($handover_custodian)) {

    $qe=$d->selectRow("assets_category_id","assets_item_detail_master","assets_id='$assets_id'");
    $catData=mysqli_fetch_array($qe);
    $assets_category_id = $catData['assets_category_id'];

    $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
    $uploadedFile = $_FILES['handover_image']['tmp_name'];
    $ext = pathinfo($_FILES['handover_image']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["handover_image"]["size"];
    $maxsize = 20971520;
    if (file_exists($uploadedFile)) {
        if (in_array($ext, $extension)) {
            if ($filesize > $maxsize) {
                $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                header("location:../itemDetails");
                exit();
            }
            $sourceProperties = getimagesize($uploadedFile);
            $newFileName = rand();
            $dirPath = "../../img/society/";
            $imageType = $sourceProperties[2];
            $imageHeight = $sourceProperties[1];
            $imageWidth = $sourceProperties[0];
            if ($imageWidth > 1200) {
                $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                $newImageHeight = $imageHeight * $newWidthPercentage / 100;
            } else {
                $newImageWidth = $imageWidth;
                $newImageHeight = $imageHeight;
            }

            switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_handover." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_handover." . $ext);
                    break;

                default:
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../itemDetails");
                    exit;
                    break;
            }
            $handover_image = $newFileName . "_handover." . $ext;
            $notiUrl = $base_url . 'img/society/' . $handover_image;
        } else {
            $_SESSION['msg1'] = "Invalid Document ";
            header("location:../itemDetails");
            exit();
        }
    }

    $m->set_data('assets_id',$assets_id);
    $m->set_data('floor_id',$floor_id);
    $m->set_data('user_id',$user_id);
    $m->set_data('society_id',$society_id);
    $m->set_data('assets_category_id',$assets_category_id);
    $m->set_data('start_date',$handover_date);
    $m->set_data('handover_image',$handover_image);
    $m->set_data('condition_type',$condition_type);
    $m->set_data('remark',$remark);
    $m->set_data('iteam_created_date',date("Y-m-d H:i:s"));
    $m->set_data('created_by',$_COOKIE['bms_admin_id']);
        
    $a2 = array(
      'assets_id'=>$m->get_data('assets_id'),
      'floor_id'=>$m->get_data('floor_id'),
      'user_id'=>$m->get_data('user_id'),
      'society_id'=>$m->get_data('society_id'),
      'remark'=>$m->get_data('remark'),
      'condition_type'=>$m->get_data('condition_type'),
      'start_date'=>$m->get_data('start_date'),  
      'handover_image'=>$m->get_data('handover_image'),  
      'assets_category_id'=>$m->get_data('assets_category_id'),
      'iteam_created_date'=>$m->get_data('iteam_created_date'),
      'created_by'=>$m->get_data('created_by')
    );
      
        
    $q=$d->insert("assets_detail_inventory_master",$a2);

    if($q==TRUE)
    {
      ///// Send Notification to handover user 
      $handoverq = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
      $dataHandover = mysqli_fetch_assoc($handoverq);
      $titleHandover = "Assets Management";
      $descriptionHandover = "New asset has handover by $created_by";
      $menuClickHandover = "assets_management";
      $imageHandover = "assets_management.png";
      $activityHandover = "0";
      $user_token_handover = $dataHandover['user_token'];
      $deviceHandover = $dataHandover['device'];
      if ($deviceHandover=='android') {
          $nResident->noti($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
      } else {
          $nResident->noti_ios($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
      }
      $d->insertUserNotification($society_id,$titleHandover,$descriptionHandover,"assets_management","assets_management.png","user_id='$user_id'");
      
      $_SESSION['msg']="New asset has handover Successfully";
      $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","New asset has handover Successfully");
      header("Location: ../itemDetails");
    }
    else{
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../itemDetails");
    }
}

if(isset($takeover_custodian)) {

    $q=$d ->selectRow('user_id',"assets_detail_inventory_master","assets_id='$assets_id' AND inventory_id='$inventory_id' AND end_date='0000-00-00'");

    $row=mysqli_fetch_assoc($q);
    $takeover_user_id=$row['user_id'];

    $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
    $uploadedFile = $_FILES['takeover_image']['tmp_name'];
    $ext = pathinfo($_FILES['takeover_image']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["takeover_image"]["size"];
    $maxsize = 20971520;
    if (file_exists($uploadedFile)) {
        if (in_array($ext, $extension)) {
            if ($filesize > $maxsize) {
                $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                header("location:../itemDetails");
                exit();
            }
            $sourceProperties = getimagesize($uploadedFile);
            $newFileName = rand();
            $dirPath = "../../img/society/";
            $imageType = $sourceProperties[2];
            $imageHeight = $sourceProperties[1];
            $imageWidth = $sourceProperties[0];
            if ($imageWidth > 1200) {
                $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                $newImageHeight = $imageHeight * $newWidthPercentage / 100;
            } else {
                $newImageWidth = $imageWidth;
                $newImageHeight = $imageHeight;
            }

            switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_takeover." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_takeover." . $ext);
                    break;

                default:
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../itemDetails");
                    exit;
                    break;
            }
            $takeover_image = $newFileName . "_takeover." . $ext;
            $notiUrl = $base_url . 'img/society/' . $takeover_image;
        } else {
            $_SESSION['msg1'] = "Invalid Document ";
            header("location:../itemDetails");
            exit();
        }
    }

    $m->set_data('end_date',$takeover_date);
    $m->set_data('takeover_image',$takeover_image);
    $m->set_data('takeover_remark',$takeover_remark);
    $a2 = array('end_date'=>$m->get_data('end_date'),
                'takeover_image'=>$m->get_data('takeover_image'),
                'takeover_remark'=>$m->get_data('takeover_remark')
                );
    if($inventory_id>0){
      $q2=$d->update("assets_detail_inventory_master",$a2,"inventory_id='$inventory_id'");
    }

    if($handover_assets == 1){
        $qe=$d->selectRow("assets_category_id","assets_item_detail_master","assets_id='$assets_id'");
        $catData=mysqli_fetch_array($qe);
        $assets_category_id = $catData['assets_category_id'];

        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['handover_image']['tmp_name'];
        $ext = pathinfo($_FILES['handover_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["handover_image"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../itemDetails");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/society/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_handover." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_handover." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("location:../itemDetails");
                        exit;
                        break;
                }
                $handover_image = $newFileName . "_handover." . $ext;
                $notiUrl = $base_url . 'img/society/' . $handover_image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../itemDetails");
                exit();
            }
        }

        $m->set_data('assets_id',$assets_id);
        $m->set_data('floor_id',$floor_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('assets_category_id',$assets_category_id);
        $m->set_data('start_date',$handover_date);
        $m->set_data('handover_image',$handover_image);
        $m->set_data('condition_type',$condition_type);
        $m->set_data('remark',$remark);
        $m->set_data('iteam_created_date',date("Y-m-d H:i:s"));
        $m->set_data('created_by',$_COOKIE['bms_admin_id']);
            
        $a2 = array(
        'assets_id'=>$m->get_data('assets_id'),
        'floor_id'=>$m->get_data('floor_id'),
        'user_id'=>$m->get_data('user_id'),
        'society_id'=>$m->get_data('society_id'),
        'remark'=>$m->get_data('remark'),
        'condition_type'=>$m->get_data('condition_type'),
        'start_date'=>$m->get_data('start_date'),  
        'handover_image'=>$m->get_data('handover_image'),  
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'iteam_created_date'=>$m->get_data('iteam_created_date'),
        'created_by'=>$m->get_data('created_by')
        );
        
            
        $q=$d->insert("assets_detail_inventory_master",$a2);
    }

    if($q==TRUE)
    {
      ///// Send Notification to Takeover user
      $takeoverq = $d->selectRow('user_token,device',"users_master","user_id='$takeover_user_id'");
      $dataTakeover = mysqli_fetch_assoc($takeoverq);
      $titleTakeover = "Assets Management";
      $descriptionTakeover = "Assigned asset has taken over by $created_by";
      $menuClickTakeover = "assets_management";
      $imageTakeover = "assets_management.png";
      $activityTakeover = "1";
      $user_token_takeover = $dataTakeover['user_token'];
      $deviceTakeover = $dataTakeover['device'];
      if ($deviceTakeover=='android') {
          $nResident->noti($menuClickTakeover,$imageTakeover,$society_id,$user_token_takeover,$titleTakeover,$descriptionTakeover,$activityTakeover);
      } else {
          $nResident->noti_ios($menuClickTakeover,$imageTakeover,$society_id,$user_token_takeover,$titleTakeover,$descriptionTakeover,$activityTakeover);
      }
      $d->insertUserNotification($society_id,$titleTakeover,$descriptionTakeover,"assets_management","assets_management.png","user_id='$takeover_user_id'");
      
      if($handover_assets == 1){
        $handoverq = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
        $dataHandover = mysqli_fetch_assoc($handoverq);
        $titleHandover = "Assets Management";
        $descriptionHandover = "New asset has handover by $created_by";
        $menuClickHandover = "assets_management";
        $imageHandover = "assets_management.png";
        $activityHandover = "0";
        $user_token_handover = $dataHandover['user_token'];
        $deviceHandover = $dataHandover['device'];
        if ($deviceHandover=='android') {
            $nResident->noti($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
        } else {
            $nResident->noti_ios($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
        }
        $d->insertUserNotification($society_id,$titleHandover,$descriptionHandover,"assets_management","assets_management.png","user_id='$user_id'");
      }
      $_SESSION['msg']="Assigned asset has taken over Successfully";
      $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assigned asset has taken over Successfully");
      header("Location: ../itemDetails");
    }
    else{
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../itemDetails");
    }
}
 

}else {
  header('location:../login');
}
?>
