<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
  if(isset($_POST['addStationaryProductVariant'])){
    /* echo "<pre>";
    print_r($_FILES);die; */
    $qp = $d->selectRow('vendor_id,vendor_category_id',"vendor_product_master","vendor_product_id='$vendor_product_id'");
    $productData = mysqli_fetch_assoc($qp);  
    //print_r($vendordata);die;

     $m->set_data('vendor_id',$productData['vendor_id']);
     $m->set_data('vendor_category_id',$productData['vendor_category_id']);   
     $m->set_data('vendor_product_id',$vendor_product_id); 
     $m->set_data('vendor_product_variant_name',$vendor_product_variant_name); 
     $m->set_data('vendor_product_variant_description',$vendor_product_variant_description); 
     $m->set_data('vendor_product_variant_price',$vendor_product_variant_price); 
      
     $a1 = array(
      'vendor_id'=>$m->get_data('vendor_id'),
      'vendor_category_id'=>$m->get_data('vendor_category_id'), 
      'vendor_product_id'=>$m->get_data('vendor_product_id'),  
      'vendor_product_variant_name'=>$m->get_data('vendor_product_variant_name'),  
      'vendor_product_variant_description'=>$m->get_data('vendor_product_variant_description'),  
      'vendor_product_variant_price'=>$m->get_data('vendor_product_variant_price'),  
    );  

     if(isset($vendor_product_variant_id) && $vendor_product_variant_id>0 )
     {
     $q=$d->update("vendor_product_variant_master",$a1,"vendor_product_variant_id ='$vendor_product_variant_id'");
      $_SESSION['msg']="Stationery Product Variant Updated Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Stationery Product Variant Updated Successfully");
     }
     else{
      $q=$d->insert("vendor_product_variant_master",$a1);
      $vendor_product_variant_id = $con->insert_id;
      for($i=0; $i<sizeof($_FILES['vendor_product_image_name']['name']); $i++){
            $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $uploadedFile = $_FILES['vendor_product_image_name']['tmp_name'][$i];
            $ext = pathinfo($_FILES['vendor_product_image_name']['name'][$i], PATHINFO_EXTENSION);
            $filesize = $_FILES["vendor_product_image_name"]["size"][$i];
            $maxsize = 20971520;
            if (file_exists($uploadedFile)) {
                if (in_array($ext, $extension)) {
                    if ($filesize > $maxsize) {
                        $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                        header("location:../addStationaryProductVariant");
                        exit();
                    }
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand();
                    $dirPath = "../../img/vendor_product/";
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    if ($imageWidth > 1200) {
                        $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                        $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                            break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                            break;

                        default:
                            $_SESSION['msg1'] = "Invalid Document ";
                            header("Location:../addStationaryProductVariant");
                            exit;
                            break;
                    }
                    $image = $newFileName . "vendor_product." . $ext;
                    $notiUrl = $base_url . 'img/vendor_product/' . $image;
                    $m->set_data('vendor_id', $productData['vendor_id']);
                    $m->set_data('vendor_product_id', $vendor_product_id);
                    $m->set_data('vendor_product_variant_id', $vendor_product_variant_id);
                    $m->set_data('vendor_product_image_name', $image);

                    $a2 = array(
                        'vendor_id'=>$m->get_data('vendor_id'),
                        'vendor_product_id'=>$m->get_data('vendor_product_id'),  
                        'vendor_product_variant_id'=>$m->get_data('vendor_product_variant_id'),  
                        'vendor_product_image_name'=>$m->get_data('vendor_product_image_name'),  
                      );  
                    $q1=$d->insert("vendor_product_image_master",$a2);
                } else {
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../addStationaryProductVariant");
                    exit();
                }
            } else {
                if($vendor_product_category_id  =="")
                {
                    $_SESSION['msg1'] = "Here Is No File ";
                    header("location:../addStationaryProductVariant");
                    exit();
                }
                
            }
      }
      $_SESSION['msg']="Stationery Product Variant Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Stationery Product Variant Added Successfully");

     }
     
     if($q==TRUE) {     
      header("Location: ../stationaryProductVariant");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../addStationaryProductVariant");
   }

 }


 if(isset($_POST['addCanteenProductVariant'])){
    /* echo "<pre>";
    print_r($_FILES);die; */
    $qp = $d->selectRow('vendor_id,vendor_category_id',"vendor_product_master","vendor_product_id='$vendor_product_id'");
    $productData = mysqli_fetch_assoc($qp);  
    //print_r($vendordata);die;

     $m->set_data('vendor_id',$productData['vendor_id']);
     $m->set_data('vendor_category_id',$productData['vendor_category_id']);   
     $m->set_data('vendor_product_id',$vendor_product_id); 
     $m->set_data('vendor_product_variant_name',$vendor_product_variant_name); 
     $m->set_data('vendor_product_variant_description',$vendor_product_variant_description); 
     $m->set_data('vendor_product_variant_price',$vendor_product_variant_price); 
      
     $a1 = array(
      'vendor_id'=>$m->get_data('vendor_id'),
      'vendor_category_id'=>$m->get_data('vendor_category_id'), 
      'vendor_product_id'=>$m->get_data('vendor_product_id'),  
      'vendor_product_variant_name'=>$m->get_data('vendor_product_variant_name'),  
      'vendor_product_variant_description'=>$m->get_data('vendor_product_variant_description'),  
      'vendor_product_variant_price'=>$m->get_data('vendor_product_variant_price'),  
    );  

     if(isset($vendor_product_variant_id) && $vendor_product_variant_id>0 )
     {
     $q=$d->update("vendor_product_variant_master",$a1,"vendor_product_variant_id ='$vendor_product_variant_id'");
      $_SESSION['msg']="Canteen Product Variant Updated Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Canteen Product Variant Updated Successfully");
     }
     else{
      $q=$d->insert("vendor_product_variant_master",$a1);
      $vendor_product_variant_id = $con->insert_id;
      for($i=0; $i<sizeof($_FILES['vendor_product_image_name']['name']); $i++){
            $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $uploadedFile = $_FILES['vendor_product_image_name']['tmp_name'][$i];
            $ext = pathinfo($_FILES['vendor_product_image_name']['name'][$i], PATHINFO_EXTENSION);
            $filesize = $_FILES["vendor_product_image_name"]["size"][$i];
            $maxsize = 20971520;
            if (file_exists($uploadedFile)) {
                if (in_array($ext, $extension)) {
                    if ($filesize > $maxsize) {
                        $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                        header("location:../addCanteenProductVariant");
                        exit();
                    }
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand();
                    $dirPath = "../../img/vendor_product/";
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    if ($imageWidth > 1200) {
                        $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                        $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                            break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                            break;

                        default:
                            $_SESSION['msg1'] = "Invalid Document ";
                            header("Location:../addCanteenProductVariant");
                            exit;
                            break;
                    }
                    $image = $newFileName . "vendor_product." . $ext;
                    $notiUrl = $base_url . 'img/vendor_product/' . $image;
                    $m->set_data('vendor_id', $productData['vendor_id']);
                    $m->set_data('vendor_product_id', $vendor_product_id);
                    $m->set_data('vendor_product_variant_id', $vendor_product_variant_id);
                    $m->set_data('vendor_product_image_name', $image);

                    $a2 = array(
                        'vendor_id'=>$m->get_data('vendor_id'),
                        'vendor_product_id'=>$m->get_data('vendor_product_id'),  
                        'vendor_product_variant_id'=>$m->get_data('vendor_product_variant_id'),  
                        'vendor_product_image_name'=>$m->get_data('vendor_product_image_name'),  
                      );  
                    $q1=$d->insert("vendor_product_image_master",$a2);
                } else {
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../addCanteenProductVariant");
                    exit();
                }
            } else {
                if($vendor_product_category_id  =="")
                {
                    $_SESSION['msg1'] = "Here Is No File ";
                    header("location:../addCanteenProductVariant");
                    exit();
                }
                
            }
      }
      $_SESSION['msg']="Canteen Product Variant Added Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Canteen Product Variant Added Successfully");

     }
     
     if($q==TRUE) {     
      header("Location: ../canteenProductVariant");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../addCanteenProductVariant");
   }

 }

 if(isset($_POST['addProductVariantImage'])){
    /* echo "<pre>";
    print_r($_FILES);die; */
    $qp = $d->selectRow('vendor_id,vendor_product_id',"vendor_product_variant_master","vendor_product_variant_id='$vendor_product_variant_id'");
    $productData = mysqli_fetch_assoc($qp);  
    //print_r($vendordata);die;
    $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
    $uploadedFile = $_FILES['vendor_product_image_name']['tmp_name'];
    $ext = pathinfo($_FILES['vendor_product_image_name']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["vendor_product_image_name"]["size"];
    $maxsize = 20971520;
    if (file_exists($uploadedFile)) {
        if (in_array($ext, $extension)) {
            if ($filesize > $maxsize) {
                $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                header("location:../productVariantDetail?id=".$vendor_product_variant_id);
                exit();
            }
            $sourceProperties = getimagesize($uploadedFile);
            $newFileName = rand();
            $dirPath = "../../img/vendor_product/";
            $imageType = $sourceProperties[2];
            $imageHeight = $sourceProperties[1];
            $imageWidth = $sourceProperties[0];
            if ($imageWidth > 1200) {
                $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                $newImageHeight = $imageHeight * $newWidthPercentage / 100;
            } else {
                $newImageWidth = $imageWidth;
                $newImageHeight = $imageHeight;
            }

            switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "vendor_product." . $ext);
                    break;

                default:
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("Location:../productVariantDetail?id=".$vendor_product_variant_id);
                    exit;
                    break;
            }
            $image = $newFileName . "vendor_product." . $ext;
            $notiUrl = $base_url . 'img/vendor_product/' . $image;
        } else {
            $_SESSION['msg1'] = "Invalid Document ";
            header("location:../productVariantDetail?id=".$vendor_product_variant_id);
            exit();
        }
    } else {
        $_SESSION['msg1'] = "Here Is No File ";
        header("location:../productVariantDetail?id=".$vendor_product_variant_id);
        exit();
    }
    if(isset($vendor_product_image_id) && $vendor_product_image_id>0 ){
        $m->set_data('vendor_product_image_name', $image);
        $a2 = array('vendor_product_image_name'=>$m->get_data('vendor_product_image_name'));  
        $q=$d->update("vendor_product_image_master",$a2,"vendor_product_image_id ='$vendor_product_image_id'");
        
        unlink("../../img/vendor_product/".$vendor_product_image_name_old);
        
        $_SESSION['msg']="Product Variant Image Updated Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Variant Image Updated Successfully");
    }else{
        $m->set_data('vendor_id', $productData['vendor_id']);
        $m->set_data('vendor_product_id', $productData['vendor_product_id']);
        $m->set_data('vendor_product_variant_id', $vendor_product_variant_id);
        $m->set_data('vendor_product_image_name', $image);

        $a2 = array(
            'vendor_id'=>$m->get_data('vendor_id'),
            'vendor_product_id'=>$m->get_data('vendor_product_id'),  
            'vendor_product_variant_id'=>$m->get_data('vendor_product_variant_id'),  
            'vendor_product_image_name'=>$m->get_data('vendor_product_image_name'),  
          );  
        $q=$d->insert("vendor_product_image_master",$a2); 
        $_SESSION['msg']="Product Variant Image Added Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Variant Image Added Successfully");
    }

    if($q==TRUE) {     
      header("Location: ../productVariantDetail?id=".$vendor_product_variant_id);
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../productVariantDetail?id=".$vendor_product_variant_id);
   }

 }


}
?>