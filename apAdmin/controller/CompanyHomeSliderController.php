<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
  if(isset($_POST['addCompanyHomeSlider'])){
   /* echo "<pre>";
    print_r($_POST);
    print_r($_FILES);die;
 */
     $m->set_data('society_id',$society_id); 
     $m->set_data('company_home_slider_title',$company_home_slider_title); 
     $m->set_data('company_home_slider_description',$company_home_slider_description); 
     $m->set_data('company_home_slider_url',$company_home_slider_url); 
     $m->set_data('company_home_slider_mobile',$company_home_slider_mobile); 
     $m->set_data('company_home_slider_added_by',$_COOKIE['bms_admin_id']); 
     $m->set_data('company_home_slider_created_at', date("Y-m-d H:i:s"));
      
     $a1 = array(
      'society_id'=>$m->get_data('society_id'),
      'company_home_slider_title'=>$m->get_data('company_home_slider_title'), 
      'company_home_slider_description'=>$m->get_data('company_home_slider_description'),  
      'company_home_slider_url'=>$m->get_data('company_home_slider_url'),  
      'company_home_slider_mobile'=>$m->get_data('company_home_slider_mobile'),  
      'company_home_slider_added_by'=>$m->get_data('company_home_slider_added_by'),  
      'company_home_slider_created_at'=>$m->get_data('company_home_slider_created_at'),  
    );  


    /////////// for top image///////////////////////
     if($_FILES['company_home_slider_image']['tmp_name'] != '')
     {
        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['company_home_slider_image']['tmp_name'];
        $ext = pathinfo($_FILES['company_home_slider_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["company_home_slider_image"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../companyHomeSlider");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/front_slider/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "home_slider." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "home_slider." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../companyHomeSlider");
                        exit;
                        break;
                }
                $image = $newFileName . "home_slider." . $ext;
                $notiUrl = $base_url . 'img/front_slider/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../companyHomeSlider");
                exit();
            }
        }
        $m->set_data('company_home_slider_image', $image);
        $a1['company_home_slider_image'] = $m->get_data('company_home_slider_image');
     }
     
     if(isset($company_home_slider_id) && $company_home_slider_id>0 ){
        $q=$d->update("company_home_slider_master",$a1,"company_home_slider_id ='$company_home_slider_id'");
        $_SESSION['msg']="Company Home Slider Updated Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Home Slider Updated Successfully");   
     }else{
        $q=$d->insert("company_home_slider_master",$a1);
        $_SESSION['msg']="Company Home Slider Added Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Home Slider Added Successfully");   
     }

     if($q==TRUE) {  
      header("Location: ../companyHomeSlider");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../companyHomeSlider");
   }

 }

 if(isset($_POST['delete_company_home_slider'])){
    $q=$d->delete("company_home_slider_master","company_home_slider_id='$company_home_slider_id'");
    if($q>0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Home Slider Deleted");
        $_SESSION['msg']="Company Home Slider Deleted.";
        header("location:../companyHomeSlider");
    } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../companyHomeSlider");
    }
 }


}
?>