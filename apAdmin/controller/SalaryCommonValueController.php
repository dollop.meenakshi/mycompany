<?php 
include '../common/objectController.php';
extract($_POST);
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{  

  if(isset($_POST['deleteSalaryGroup'])){

        $q = $d->delete("salary_group_master","salary_group_id='$delete_salary_group_id'");
         $d->delete("salary_common_value_master","salary_group_id='$delete_salary_group_id'");

         if(($q==TRUE || $qu==TRUE) ) { 
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary $delete_group_name  Group Deleted");
            header("Location: ../salaryCommonValueMaster");
            exit();
        } else {
            $_SESSION['msg1']="Something Wrong";
            header("Location: ../salaryCommonValueMaster");
            exit();
        }

  }
 
  if(isset($_POST['addSalaryCommonValue'])){
    // echo "<pre>";
    // print_r($_POST);die;
   
    $appendNotEqQuery  ='';
    mysqli_autocommit($con,FALSE);
     if (isset($salary_group_id) && $salary_group_id>0) {
         $appendNotEqQuery = " AND salary_group_id!='$salary_group_id'";
     }
     
     $qs=$d->select("salary_group_master","salary_group_name='$salary_group_name' $appendNotEqQuery ");

     if (mysqli_num_rows($qs)>0) {
        $_SESSION['msg1']="This salary group already aded";
        header("Location: ../salaryCommonValueMaster");
        exit();
     }
     if($allow_extra_day_payout_per_day=="0")
     {
        $extra_day_payout_type_per_day ="0";
        $fixed_amount_for_per_day ="0";
        
     }else{
        if($extra_day_payout_type_per_day=="0"){
            $fixed_amount_for_per_day='0';
        }
     }
     if($allow_extra_day_payout_per_hour=="0")
     {
        $extra_day_payout_type_per_hour ="0";
        $fixed_amount_for_per_hour ="0";
        $extra_hour_calculation_minutes ="0";
        
     }else{
        if($extra_day_payout_type_per_hour=="0"){
            $fixed_amount_for_per_hour='0';
           
        }
     }
     if($allow_extra_day_payout_fixed=="0")
     {
        $extra_day_payout_type_fixed ="0";
        $fixed_amount_for_fixed ="0";
        
     }else{
        if($extra_day_payout_type_fixed=="0"){
            $fixed_amount_for_fixed='0';
        }
     }
    
     
     
     $m->set_data('extra_day_payout_type_per_day',$extra_day_payout_type_per_day);
     $m->set_data('extra_hour_calculation',$extra_hour_calculation);
     $m->set_data('extra_hour_calculation_minutes',$extra_hour_calculation_minutes);
     $m->set_data('allow_extra_day_payout_per_day',$allow_extra_day_payout_per_day);
     $m->set_data('fixed_amount_for_per_day',$fixed_amount_for_per_day);
     $m->set_data('allow_extra_day_payout_per_hour',$allow_extra_day_payout_per_hour);
     $m->set_data('extra_day_payout_type_per_hour',$extra_day_payout_type_per_hour);
     $m->set_data('fixed_amount_for_per_hour',$fixed_amount_for_per_hour);
     $m->set_data('allow_extra_day_payout_fixed',$allow_extra_day_payout_fixed);
     $m->set_data('extra_day_payout_type_fixed',$extra_day_payout_type_fixed);
     $m->set_data('fixed_amount_for_fixed',$fixed_amount_for_fixed);
    $m->set_data('hourly_salary_extra_hours_payout', $hourly_salary_extra_hours_payout);
    $m->set_data('working_day_calculation', $working_day_calculation);
     $m->set_data('society_id',$society_id);
     $m->set_data('salary_group_name',$salary_group_name);
     $m->set_data('salary_earning_deduction_id',$salary_earning_deduction_id);
     $m->set_data('amount_type',$amount_type);
     $m->set_data('amount_value',$amount_value);
     $m->set_data('salary_common_created_by', $_COOKIE['bms_admin_id']);
     $m->set_data('salary_group_created_by', $_COOKIE['bms_admin_id']);
     $m->set_data('salary_common_created_date',date("Y-m-d H:i:s"));
     $m->set_data('salary_group_created_date',date("Y-m-d H:i:s"));
     
    
     
    if (isset($salary_group_id) && $salary_group_id>0) {

        $mainGroup = array(
          'salary_group_name'=>$m->get_data('salary_group_name'), 
          'salary_group_created_by'=>$m->get_data('salary_group_created_by'),
          'working_day_calculation'=>$m->get_data('working_day_calculation'),
        );
        if($hourly_salary_extra_hours_payout !=""){
            $mainGroup['hourly_salary_extra_hours_payout']=$hourly_salary_extra_hours_payout;
        };
        $mainGroup['extra_day_payout_type_per_day']=$extra_day_payout_type_per_day;
                $mainGroup['allow_extra_day_payout_per_day']=$allow_extra_day_payout_per_day;
                $mainGroup['fixed_amount_for_per_day']=$fixed_amount_for_per_day;
                $mainGroup['allow_extra_day_payout_per_hour']=$allow_extra_day_payout_per_hour;
                $mainGroup['extra_day_payout_type_per_hour']=$extra_day_payout_type_per_hour;
                $mainGroup['fixed_amount_for_per_hour']=$fixed_amount_for_per_hour;
                $mainGroup['allow_extra_day_payout_fixed']=$allow_extra_day_payout_fixed;
                $mainGroup['extra_day_payout_type_fixed']=$extra_day_payout_type_fixed;
                $mainGroup['fixed_amount_for_fixed']=$fixed_amount_for_fixed;
                $mainGroup['extra_hour_calculation_minutes']=$extra_hour_calculation_minutes;
                $mainGroup['extra_hour_calculation']=$extra_hour_calculation;
                
        $q=$d->update("salary_group_master",$mainGroup,"salary_group_id='$salary_group_id'");
        $_SESSION['msg']="Salary group update successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary $salary_group_name group updated");
    }else {
         $mainGroup = array(
          'society_id'=>$m->get_data('society_id'),
          'salary_group_name'=>$m->get_data('salary_group_name'), 
          'salary_group_created_by'=>$m->get_data('salary_group_created_by'),
          'working_day_calculation'=>$m->get_data('working_day_calculation'),
          'salary_group_created_date'=>$m->get_data('salary_group_created_date'),
        );
        if($hourly_salary_extra_hours_payout !=""){
            $mainGroup['hourly_salary_extra_hours_payout']=$hourly_salary_extra_hours_payout;
        };
                $mainGroup['extra_day_payout_type_per_day']=$extra_day_payout_type_per_day;
                $mainGroup['allow_extra_day_payout_per_day']=$allow_extra_day_payout_per_day;
                $mainGroup['fixed_amount_for_per_day']=$fixed_amount_for_per_day;
                $mainGroup['allow_extra_day_payout_per_hour']=$allow_extra_day_payout_per_hour;
                $mainGroup['extra_day_payout_type_per_hour']=$extra_day_payout_type_per_hour;
                $mainGroup['fixed_amount_for_per_hour']=$fixed_amount_for_per_hour;
                $mainGroup['allow_extra_day_payout_fixed']=$allow_extra_day_payout_fixed;
                $mainGroup['extra_day_payout_type_fixed']=$extra_day_payout_type_fixed;
                $mainGroup['fixed_amount_for_fixed']=$fixed_amount_for_fixed;
                $mainGroup['extra_hour_calculation_minutes']=$extra_hour_calculation_minutes;
                $mainGroup['extra_hour_calculation']=$extra_hour_calculation;
        $q=$d->insert("salary_group_master",$mainGroup);
        $salary_group_id = $con->insert_id;  
        $_SESSION['msg']="Salary group successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary $salary_group_name added");

    }

     

    
    
    $checkData= $d->delete("salary_common_value_master", "salary_group_id='$salary_group_id' ");
            
        
        for($j=0;$j<COUNT($_POST['salary_earning_deduction_id']); $j++)
        {
            $percent_min_max = "";
            if (in_array($_POST['salary_earning_deduction_id'][$j],$_POST['salary_common_value_checkbox']))
            {   
                $salary_earning_deduction_id = $_POST['salary_earning_deduction_id'][$j];
                
                $m->set_data('salary_earning_deduction_id',$salary_earning_deduction_id);
                $m->set_data('salary_group_id',$salary_group_id);

                if($_POST['amount_type'][$j]==2)
                {
                    $MxMinArary = array();
                    $max =  $_POST['max'][$_POST['salary_earning_deduction_id'][$j]];
                    $min =  $_POST['min'][$_POST['salary_earning_deduction_id'][$j]];
                    $value =  $_POST['value'][$_POST['salary_earning_deduction_id'][$j]];

                    if (isset($_POST['max'][$_POST['salary_earning_deduction_id'][$j]])) {
                        // code...
                        $MaxMInValueCount = count($_POST['max'][$_POST['salary_earning_deduction_id'][$j]]);
                        for($m1=0; $m1<$MaxMInValueCount; $m1++){
                            if ($value[$m1]>0) {
                                $MaxminJasoN = array('max'=>$max[$m1],'min'=>$min[$m1],'value'=>$value[$m1] );
                                array_push($MxMinArary,json_encode($MaxminJasoN));
                            }
                        }
                    }

                }
                
                if(isset($_POST['multiEarning']) && !empty($_POST['multiEarning']))
                {
                    
                    // $toatlMultiEarn = COUNT($_POST['multiEarning'][$_POST['salary_earning_deduction_id'][$j]]);
                    $salary_common_value_earn_deduction = $_POST['multiEarning'][$_POST['salary_earning_deduction_id'][$j]];
                    
                }
                if($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1){
                    if(isset($_POST['percent_min']) && !empty($_POST['percent_min']))
                    { 
                        $percent_min = $_POST['percent_min'][$_POST['salary_earning_deduction_id'][$j]];
                        $percent_max = $_POST['percent_max'][$_POST['salary_earning_deduction_id'][$j]];
                        if($percent_max !="" && $percent_min !=""){
                            $percent_min_max = array('percent_min'=>$percent_min,'percent_max'=>$percent_max);
                           
                          //  print_r($percent_min_max);
                            $percent_min_max = json_encode($percent_min_max);
                           

                        }else{
                            $percent_min_max ="" ;
                        }
                    }else{
                        $percent_min_max ="" ;
                    }
                }else{
                    $percent_min_max ="" ;
                }
                if($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1){
                    if(isset($_POST['amount_value_employeer']) && !empty($_POST['amount_value_employeer']))
                    { 
                        $amount_value_employeer = $_POST['amount_value_employeer'][$_POST['salary_earning_deduction_id'][$j]];
                        
                    }else{
                        $amount_value_employeer ="" ;
                    }
                }else{
                    $amount_value_employeer ="" ;
                }
             
                if($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1){
                    if(isset($_POST['show_employer_contribution']) && !empty($_POST['show_employer_contribution']))
                    { 
                        $show_employer_contribution = $_POST['show_employer_contribution'][$_POST['salary_earning_deduction_id'][$j]];
                        if($amount_value_employeer !="" && (float)$amount_value_employeer>0 && $show_employer_contribution>0){
                            
                            $show_employer_contribution = $_POST['show_employer_contribution'][$_POST['salary_earning_deduction_id'][$j]];
                        }else{
                           
                            $show_employer_contribution ="0";
                        }
                        
                    }else{
                       
                        $show_employer_contribution ="0";
                    }
                }else{
                   
                    $show_employer_contribution ="0";
                }
                
                if($_POST['amount_type'][$j]==2)
                {
                    $a1['slab_json']=implode('~',$MxMinArary);
                }
                else
                {
                    $a1['slab_json']="";

                }                            
                if(($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==2) && isset($salary_common_value_earn_deduction) )
                {
                    $a1['salary_common_value_earn_deduction']=implode(',',$salary_common_value_earn_deduction);
                }
                else
                {
                    $a1['salary_common_value_earn_deduction']="";

                } 
                if(($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1) && isset($percent_min_max) )
                {
                    $a1['percent_min_max']=$percent_min_max;
                }
                else
                {
                    $a1['percent_min_max']="";

                } 
                if(($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1) && isset($amount_value_employeer) )
                {
                    $a1['amount_value_employeer']=$amount_value_employeer;
                }
                else
                {

                    $a1['amount_value_employeer']="";

                } 
                if(($_POST['amount_type'][$j]==0 || $_POST['amount_type'][$j]==1) && isset($show_employer_contribution) )
                {
                    if($a1['amount_value_employeer'] !="" && $a1['amount_value_employeer']>0){

                        $a1['show_employer_contribution']=$show_employer_contribution;
                    }else
                    {
                        $a1['show_employer_contribution']="0";
    
                    }
                }
                else
                {
                    $a1['show_employer_contribution']="0";

                } 
                
                $a1['society_id']=$society_id;
                $a1['salary_group_id']=$salary_group_id;
                $a1['salary_earning_deduction_id']=$_POST['salary_earning_deduction_id'][$j];
                $a1['amount_type']=$_POST['amount_type'][$j];
                $a1['amount_value']=$_POST['amount_value'][$_POST['salary_earning_deduction_id'][$j]];
                $a1['salary_common_value_remark']=test_input($_POST['salary_common_value_remark'][$j]);
                $a1['percent_deduction_max_amount']=$a1['amount_type']==0?$_POST['percent_deduction_max_amount'][$_POST['salary_earning_deduction_id'][$j]]:0;
                
                
                $q=$d->insert("salary_common_value_master",$a1);
                $salary_common_value_id = $con->insert_id;
                  
            }
        }
              
        
        if(($q==TRUE || $qu==TRUE) ) { 
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary $salary_group_name  Group Added");
            mysqli_commit($con);
            header("Location: ../salaryCommonValueMaster?gId=$salary_group_id");
            exit();
        } else {
            mysqli_rollback($con);
            $_SESSION['msg1']="Something Wrong";
            header("Location: ../salaryCommonValueMaster?gId=$salary_group_id");
            exit();
        }
    }



}
?>