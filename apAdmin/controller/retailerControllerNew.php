<?php
error_reporting(0);
session_start();
$url=$_SESSION['url'] ;
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include_once '../lib/sms_api.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
include_once '../fcm_file/resident_fcm_topic.php';
$d = new dao();
$m = new model();
$smsObj = new sms_api();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nResidentTopic = new firebase_resident_topic();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../../img/$language_id.xml");
$societyLngName=  $xml->string->society;
$base_url=$m->base_url();
$urlArya = explode("/", $base_url);
$cookieUrl = $urlArya[3];

$sq=$d->selectRow("society_name,currency,society_address,city_name,society_pincode,secretary_email,socieaty_logo","society_master","society_id='$society_id'");
$societyData=mysqli_fetch_array($sq);
$society_name=$societyData['society_name'];
$secretary_email= $societyData['secretary_email'];
$socieaty_logo= $societyData['socieaty_logo'];
$society_address_invoice= $societyData['society_address'].', '.$societyData['city_name'].' ('.$societyData['society_pincode'].')';
$currency = $societyData['currency'];
$received_by = "Auto";

if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['getRetailerList']))
    {
        $append = "";
        if(isset($country_id) && !empty($country_id))
        {
            $append .= " AND rm.country_id = '$country_id'";
        }
        if(isset($state_id) && !empty($state_id))
        {
            $append .= " AND rm.state_id = '$state_id'";
        }
        if(isset($city_id) && !empty($city_id))
        {
            $append .= " AND rm.city_id = '$city_id'";
        }
        if(isset($area_id) && !empty($area_id))
        {
            $append .= " AND rm.area_id = '$area_id'";
        }
        $q = $d->selectRow("rm.*,rom.retailer_order_id,amn.area_name,c.city_name","retailer_master AS rm LEFT JOIN retailer_order_master AS rom ON rom.retailer_id = rm.retailer_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN cities AS c ON c.city_id = rm.city_id","rm.society_id = '$society_id'".$append,"GROUP BY rm.retailer_id ORDER BY retailer_id DESC");
        $iNo = 1;
        $rtData = [];
        $data = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['retailer_contact_person_number'] = $row['retailer_contact_person_country_code'] . " " . $row['retailer_contact_person_number'];
            $data['action'] = [
                'retailer_id' => $row['retailer_id'],
                'country_id' => $row['country_id'],
                'state_id' => $row['state_id'],
                'city_id' => $row['city_id'],
                'area_id' => $row['area_id'],
                'retailer_photo' => $row['retailer_photo'],
                'retailer_order_id' => $row['retailer_order_id']
            ];
            $data['status'] = [
                'retailer_id' => $row['retailer_id'],
                'retailer_status' => $row['retailer_status']
            ];
            if(file_exists("../../img/users/recident_profile/".$row['retailer_photo']))
            {
                $data['photo_new'] = [
                    'retailer_photo' => $row['retailer_photo'],
                    'retailer_name' => $row['retailer_name'],
                    'photo_exists' => 1
                ];
            }
            else
            {
                $data['photo_new'] = [
                    'retailer_photo' => "",
                    'retailer_name' => $row['retailer_name'],
                    'photo_exists' => 0
                ];
            }
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getOrderList']))
    {
        if(isset($_POST['date_from']) && !empty($_POST['date_from']) && $_POST['date_from'] != "0000-00-00 00:00:00")
        {
            $date_from = $_POST['date_from'];
        }
        else
        {
            $date_from = date("Y-m-d");
        }

        if(isset($_POST['date_to']) && !empty($_POST['date_to']) && $_POST['date_to'] != "0000-00-00 00:00:00")
        {
            $date_to = $_POST['date_to'];
        }
        else
        {
            $date_to = date("Y-m-d");
        }
        $append = "";
        $append .= "rom.order_date BETWEEN '$date_from 00:00:01' AND '$date_to 23:59:59'";
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $append .= " AND rom.order_by_user_id = '$user_id'";
        }
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            $append .= " AND rom.retailer_id = '$retailer_id'";
        }
        if(isset($distributor_id) && !empty($distributor_id) && $distributor_id != 0)
        {
            $append .= " AND rom.distributor_id = '$distributor_id'";
        }
        if(isset($order_status) && $order_status != "")
        {
            $append .= " AND rom.order_status = '$order_status'";
        }
        if(isset($area_id) && !empty($area_id) && $area_id != "")
        {
            $append .= " AND rm.area_id = '$area_id'";
        }
        if(isset($product_id) && !empty($product_id) && $product_id != "")
        {
            $append .= " AND ropm.product_id = '$product_id'";
        }
        $q = $d->selectRow("rdvtm.out_of_range,rdvtm.out_of_range_reason,rom.*,rm.retailer_name,dm.distributor_name,um.user_full_name,amn.area_name","retailer_order_master AS rom JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id JOIN distributor_master AS dm ON dm.distributor_id = rom.distributor_id JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_order_product_master AS ropm ON ropm.retailer_order_id = rom.retailer_order_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN retailer_daily_visit_timeline_master AS rdvtm ON rdvtm.retailer_daily_visit_timeline_id = rom.retailer_visit_id",$append,"GROUP BY rom.retailer_order_id ORDER BY rom.order_date DESC");
        $iNo = 1;
        $rtData = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['out_of_range_rea'] = [
                'out_of_range' => $row['out_of_range'],
                'out_of_range_reason' => $row['out_of_range_reason']
            ];
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getNoOrderList']))
    {
        if(isset($_POST['date_from']) && !empty($_POST['date_from']) && $_POST['date_from'] != "0000-00-00 00:00:00")
        {
            $date_from = $_POST['date_from'];
        }
        else
        {
            $date_from = date("Y-m-d");
        }

        if(isset($_POST['date_to']) && !empty($_POST['date_to']) && $_POST['date_to'] != "0000-00-00 00:00:00")
        {
            $date_to = $_POST['date_to'];
        }
        else
        {
            $date_to = date("Y-m-d");
        }
        $append = "";
        $append .= "rnom.no_order_created_date BETWEEN '$date_from 00:00:01' AND '$date_to 23:59:59'";
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $append .= " AND rnom.visit_by_user_id = '$user_id'";
        }
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            $append .= " AND rnom.retailer_id = '$retailer_id'";
        }
        $q = $d->selectRow("rnom.*,um.user_full_name,rm.retailer_name,amn.area_name,c.city_name,bm.block_name,fm.floor_name","retailer_no_order_master AS rnom JOIN users_master AS um ON um.user_id = rnom.visit_by_user_id JOIN retailer_master AS rm ON rm.retailer_id = rnom.retailer_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN cities AS c ON c.city_id = rm.city_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id",$append,"ORDER BY rnom.retailer_no_order_id DESC");
        $iNo = 1;
        $rtData = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['user_name'] = [
                'user_full_name' => $row['user_full_name'],
                'block_name' => $row['block_name'],
                'floor_name' => $row['floor_name']
            ];
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getRetailerNoOrderMap']))
    {
        $q = $d->selectRow("rnom.no_order_latitude,rnom.no_order_longitude,rm.retailer_latitude,rm.retailer_longitude,rm.retailer_name,rm.retailer_address,rm.retailer_contact_person,rm.retailer_contact_person_country_code,rm.retailer_contact_person_number,um.user_full_name,um.user_mobile,um.country_code","retailer_no_order_master AS rnom JOIN retailer_master AS rm ON rm.retailer_id = rnom.retailer_id JOIN users_master AS um ON um.user_id = rnom.visit_by_user_id","rnom.retailer_no_order_id = '$retailer_no_order_id'");
        $map = $q->fetch_assoc();
        echo json_encode($map);exit;
    }
}
?>