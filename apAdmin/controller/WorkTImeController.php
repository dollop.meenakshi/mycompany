<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    									
    
    if (isset($_POST['addWorkTime'])) {
        $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('shift_time_id', $shift_time_id);
        $m->set_data('month_year', $month_year);
        $m->set_data('month_days', $month_days);      
        $m->set_data('working_days_created_by', $_COOKIE['bms_admin_id']);       
        $workdays = array();
        $workdaysMain = array();
        $type = CAL_GREGORIAN;
        $month = date('n'); // Month ID, 1 through to 12.
        $year = date('Y'); // Year in 4 digit 2009 format. 
        for ($i = 1; $i <= $month_days; $i++) {

            $date = $year.'/'.$month.'/'.$i; //format date
            $get_name = date('l', strtotime($date)); //get week day
            $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
            
            //if not a weekend add day to array
            if($day_name != 'Sun' && $day_name != 'Sat'){
                array_push($workdays,$day_name);               

            }

        }     
        $weekOff = $month_days - COUNT($workdays) ;
        $totalHoliday = $d->selectRow("COUNT(*) AS holiday", "holiday_master", "DATE_FORMAT(holiday_start_date,'%m-%Y')='$month_year' AND society_id ='$society_id'");
        $maData = mysqli_fetch_array($totalHoliday);       
        $totalWorkingDays = COUNT($workdays)- $maData['holiday'];
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'floor_id' => $m->get_data('floor_id'),
            'shift_time_id' => $m->get_data('shift_time_id'),
            'month_year' => $m->get_data('month_year'),
            'month_days' => $m->get_data('month_days'),
            'working_days' =>$totalWorkingDays ,
            'total_weekoff_days' => $weekOff,
            'total_month_holidays' => $maData['holiday'],
            'working_days_created_by' => $m->get_data('working_days_created_by'),

        );
        if (isset($working_day_id) && $working_day_id > 0) {
            $q = $d->update("working_days_master", $a1, "working_day_id ='$working_day_id'");
            $_SESSION['msg'] = "Working Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Working Updated Successfully");
        } else {
            $q = $d->insert("working_days_master", $a1);
            $_SESSION['msg'] = "Working Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Working Added Successfully");

        }
        if ($q == true) {
            header("Location: ../workTime");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../workTime");
        }

    }

}
