<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));
$today = date("Y-m-d");
$merchant_id = $_POST['merchant_id'];
$merchant_key = $_POST['merchant_key'];
$order_id = $_POST['order_id'];
$payment_mode = $_POST['payment_mode'];
$transection_id = $_POST['transection_id'];
$no_of_month = $_POST['no_of_month'];
$package_id = $_POST['package_id'];
$response = array();
$payment_mode = strtolower($payment_mode);

if ($merchant_id != "" && $order_id!="" && $merchant_key!="" && strlen($order_id)>10 && ($payment_mode=="paytm" || $payment_mode=="upi" || $payment_mode=="upi_intent")) 
{
	

	/* for Staging */
	$url = $base_url."residentApiNew/paytm_checksum/checkOrderStatus.php";
	// $url = "http://localhost/fincasys/residentApiNew/paytm_checksum/checkOrderStatus.php";
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "MID=$merchant_id&ORDER_ID=$order_id&M_KEY=$merchant_key");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	$result = curl_exec($ch);
	
	echo $result;
	
	
}
else if ($merchant_id != "" && $order_id!="" && $merchant_key!="" && strlen($order_id)>10 && $payment_mode=="razorpay" ) 
{

	/* for Staging */
	//$url = "../residentApiNew/paytm_checksum/checkOrderStatus.php";
	$url = "https://api.razorpay.com/v1/orders/$order_id/payments";
        
    $headers = array('Content-Type: application/json',
                    'Authorization: Basic '. base64_encode("$merchant_id:$merchant_key")
                            );                    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    $curl_response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    if($code=='200'){
		echo $result = $curl_response;
	}else if($code=='400'){
		echo "<span>Unauthorized</span>";
	}else if($code=='403'){
		echo "<span>Forbidden</span>";
	}else{
		echo "<span>Razorpay Not Responding $code</span>";
	}
    
   
}
else if ($transection_id!="" && $merchant_key!="" && $payment_mode=="payumoney"  && $token!="" && $payment_txnid!='') 
{
	//$url = "https://www.payumoney.com/payment/payment/chkMerchantTxnStatus";
	$url = "https://www.payumoney.com/payment/op/getPaymentResponse";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
	            "merchantKey=$merchant_key&merchantTransactionIds=$payment_txnid");
	            // "merchantKey=Lx7RZXOj&merchantTransactionIds=d0bb6e0bc1ad201e0720dd894340a923");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	   "authorization: $token"
	   // "authorization: TFwQwwmeHuQzxl6dF1E0lpINK69Rr/HRNbqtGlTF2JE="
	));

	$server_output = curl_exec($ch);
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);  

	curl_close ($ch);
	if($code=='200'){
		echo $server_output;
	}else if($code=='401'){
		echo "<span>Unauthorized</span>";
	}else if($code=='403'){
		echo "<span>Forbidden</span>";
	}else{
		echo "<span>Payumoney Not Responding</span>";
	}
	//$server_output=json_decode($server_output,true);
}

if($updateStatus=="yes" && $status!="" && $order_id!="" && $transection_id!="" && $package_id!="" && $no_of_month!=""){
	
	$transactionData = array();
	if($status=="success"){
		$transactionData['order_status'] = "0"; //Success
		$transactionData['payment_status'] = "success"; //Failed
		$res =  $d->update("transection_master", $transactionData, "transection_id='$transection_id'");
		
		$q=$d->select("society_master","society_id='$society_id'");
		$bData=mysqli_fetch_array($q);
		$package_id = $bData['package_id'];
		$plan_expire_date = $bData['plan_expire_date'];

		if ($today>$plan_expire_date) {
           $start_date =$today;
        } else {
           $start_date =$plan_expire_date;
        }
        
        $plan_expire_date = date('Y-m-d', strtotime("+".$no_of_month." months", strtotime($start_date)));
        $last_renew_date = $today;

        $m->set_data('package_id',$package_id);
        $m->set_data('plan_expire_date',$plan_expire_date);
        $m->set_data('last_renew_date',$last_renew_date);

        $a =array(
          'package_id'=>$m->get_data('package_id'),
          'plan_expire_date'=>$m->get_data('plan_expire_date'),
          'last_renew_date'=>$m->get_data('last_renew_date'),
        );
       	
       	$q=$d->update("society_master",$a,"society_id='$society_id'");


		if($res){
            $response["message"] = "Transaction Successfull";
            $response["status"] = "200";
            
        }else{
            $response["message"] = "Something Wrong";
            $response["status"] = "201";
            
        }
		echo json_encode($response);
	}else if($status=="failed"){
		$transactionData['order_status'] = "2"; //Failed
		$transactionData['payment_status'] = "failed"; //Failed
		$res =  $d->update("transection_master", $transactionData, "transection_id='$transection_id'");
		if($res){
            $response["message"] = "Transaction failed";
            $response["status"] = "200";
            
        }else{
            $response["message"] = "Something Wrong";
            $response["status"] = "201";
            
        }
        echo json_encode($response);
	}else if($status=="pending"){
		$transactionData['order_status'] = "1"; //Pending
		$transactionData['payment_status'] = "pending"; //Pending
		$res =  $d->update("transection_master", $transactionData, "transection_id='$transection_id'");
		if($res){
            $response["message"] = "Transaction pending";
            $response["status"] = "200";
            
        }else{
            $response["message"] = "Something Wrong";
            $response["status"] = "201";
           
        }
        echo json_encode($response);

	}
}

