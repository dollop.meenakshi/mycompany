<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addPurchase'])) {

        $requirements = explode(',',$_POST['requirements']);

        $m->set_data('society_id', $society_id);
        $m->set_data('purchase_date', date("Y-m-d H:i:s"));
        $m->set_data('site_id', $site_id);
        $m->set_data('vendor_id', $vendor_id);
        $m->set_data('purchase_descripion', $purchase_descripion);
        $m->set_data('po_number', $po_number);
        $m->set_data('created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'purchase_date' => $m->get_data('purchase_date'),
            'site_id' => $m->get_data('site_id'),
            'vendor_id' => $m->get_data('vendor_id'),
            'purchase_descripion' => $m->get_data('purchase_descripion'),
            'po_number' => $m->get_data('po_number'),
            'created_by' => $m->get_data('created_by'),
            'created_date' => $m->get_data('created_date'),
        );
      
        if (isset($purchase_id) && $purchase_id > 0) {
            $q = $d->update("purchase_master", $a1, "purchase_id ='$purchase_id'");
            for ($i=0; $i < count($requirements); $i++) { 
                if($requirements[$i] != ''){
                    $product_id = $i;
                    $quantity = $requirements[$i];
                    $m->set_data('society_id', $society_id);
                    $m->set_data('purchase_id', $purchase_id);
                    $m->set_data('product_id', $product_id);
                    $m->set_data('quantity', $quantity);

                    $a2 = array(
                        'society_id' => $m->get_data('society_id'),
                        'purchase_id' => $m->get_data('purchase_id'),
                        'product_id' => $m->get_data('product_id'),
                        'quantity' => $m->get_data('quantity'),
                    );
                   
                    $q1 = $d->insert("purchase_product_master", $a2);
                  }
            }
            $_SESSION['msg'] = "Purchase Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Purchase Updated Successfully");
        } else {
            $q = $d->insert("purchase_master", $a1);
            $purchase_id = $con->insert_id;
          
            for ($i=0; $i < count($requirements); $i++) { 
                if($requirements[$i] != ''){
                    $product_id = $i;
                    $quantity = $requirements[$i];
                    $m->set_data('society_id', $society_id);
                    $m->set_data('purchase_id', $purchase_id);
                    $m->set_data('product_id', $product_id);
                    $m->set_data('quantity', $quantity);

                    $a2 = array(
                        'society_id' => $m->get_data('society_id'),
                        'purchase_id' => $m->get_data('purchase_id'),
                        'product_id' => $m->get_data('product_id'),
                        'quantity' => $m->get_data('quantity'),
                    );
                   
                    $q1 = $d->insert("purchase_product_master", $a2);
                  }
            }

            $_SESSION['msg'] = "Purchase Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Purchase Added Successfully");

        }
        if ($q == true) {
            header("Location: ../viewPurchase");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addPurchase");
        }

    }

}
