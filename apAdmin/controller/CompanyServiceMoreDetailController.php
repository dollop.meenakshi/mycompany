<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addCompanyServiceMoreDetail'])) {

        /* echo "<pre>";
        print_r($_POST);die; */
        $m->set_data('society_id', $society_id);
        $m->set_data('company_service_details_id', $company_service_details_id);
        $m->set_data('company_service_more_details_name', $company_service_more_details_name);
        $m->set_data('company_service_more_details_title', $company_service_more_details_title);
        $m->set_data('company_service_more_details_description', $company_service_more_details_description);
        $m->set_data('company_service_more_details_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('company_service_more_details_created_at',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'company_service_details_id' => $m->get_data('company_service_details_id'),
            'company_service_more_details_name' => $m->get_data('company_service_more_details_name'),
            'company_service_more_details_title' => $m->get_data('company_service_more_details_title'),
            'company_service_more_details_description' => $m->get_data('company_service_more_details_description'),
            'company_service_more_details_added_by' => $m->get_data('company_service_more_details_added_by'),
            'company_service_more_details_created_at' => $m->get_data('company_service_more_details_created_at'),

        );

        if($_FILES['company_service_more_details_image']['tmp_name'] != '')
        {
            $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $uploadedFile = $_FILES['company_service_more_details_image']['tmp_name'];
            $ext = pathinfo($_FILES['company_service_more_details_image']['name'], PATHINFO_EXTENSION);
            $filesize = $_FILES["company_service_more_details_image"]["size"];
            $maxsize = 20971520;
            if (file_exists($uploadedFile)) {
                if (in_array($ext, $extension)) {
                    if ($filesize > $maxsize) {
                        $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                        header("location:../companyServiceMoreDetail");
                        exit();
                    }
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand();
                    $dirPath = "../../img/company_services/";
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    if ($imageWidth > 1200) {
                        $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                        $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "company_services." . $ext);
                            break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "company_services." . $ext);
                            break;

                        default:
                            $_SESSION['msg1'] = "Invalid Document ";
                            header("Location:../companyServiceMoreDetail");
                            exit;
                            break;
                    }
                    $image = $newFileName . "company_services." . $ext;
                    $notiUrl = $base_url . 'img/company_services/' . $image;
                } else {
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../companyServiceMoreDetail");
                    exit();
                }
            }
            $m->set_data('company_service_more_details_image', $image);
            $a1['company_service_more_details_image'] = $m->get_data('company_service_more_details_image');
        }



        if (isset($company_service_more_details_id) && $company_service_more_details_id > 0) {
            $q = $d->update("company_service_more_details_master", $a1, "company_service_more_details_id ='$company_service_more_details_id'");
            $qd=$d->delete("company_service_more_details_list_master","company_service_more_details_id='$company_service_more_details_id'");
            for($i=0; $i<sizeof($_POST['company_service_more_detail_list_name']); $i++){
                $m->set_data('society_id', $society_id);
                $m->set_data('company_service_more_details_id', $company_service_more_details_id);
                $m->set_data('company_service_more_detail_list_name', $_POST['company_service_more_detail_list_name'][$i]);
                $m->set_data('company_service_more_detail_list_added_by', $_COOKIE['bms_admin_id']);
                $m->set_data('company_service_more_detail_list_created_at',date("Y-m-d H:i:s"));

                $a2 = array(
                    'society_id' => $m->get_data('society_id'),
                    'company_service_more_details_id' => $m->get_data('company_service_more_details_id'),
                    'company_service_more_detail_list_name' => $m->get_data('company_service_more_detail_list_name'),
                    'company_service_more_detail_list_added_by' => $m->get_data('company_service_more_detail_list_added_by'),
                    'company_service_more_detail_list_created_at' => $m->get_data('company_service_more_detail_list_created_at'),
                );

                $q1 = $d->insert("company_service_more_details_list_master", $a2);
            }
            $_SESSION['msg'] = "Company Service More Detail Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service More Detail Updated Successfully");
        } else {
            $q = $d->insert("company_service_more_details_master", $a1);
            $company_service_more_details_id = $con->insert_id;
            for($i=0; $i<sizeof($_POST['company_service_more_detail_list_name']); $i++){
                $m->set_data('society_id', $society_id);
                $m->set_data('company_service_more_details_id', $company_service_more_details_id);
                $m->set_data('company_service_more_detail_list_name', $_POST['company_service_more_detail_list_name'][$i]);
                $m->set_data('company_service_more_detail_list_added_by', $_COOKIE['bms_admin_id']);
                $m->set_data('company_service_more_detail_list_created_at',date("Y-m-d H:i:s"));

                $a2 = array(
                    'society_id' => $m->get_data('society_id'),
                    'company_service_more_details_id' => $m->get_data('company_service_more_details_id'),
                    'company_service_more_detail_list_name' => $m->get_data('company_service_more_detail_list_name'),
                    'company_service_more_detail_list_added_by' => $m->get_data('company_service_more_detail_list_added_by'),
                    'company_service_more_detail_list_created_at' => $m->get_data('company_service_more_detail_list_created_at'),
                );

                $q1 = $d->insert("company_service_more_details_list_master", $a2);
            }
            $_SESSION['msg'] = "Company Service More Detail Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service More Detail Added Successfully");

        }
        if ($q == true) {
            header("Location: ../companyServiceMoreDetail");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyServiceMoreDetail");
        }

    }

}
