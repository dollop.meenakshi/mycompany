<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// print_r($_POST);
/*
  Develop By : Asif Hingora
*/


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{  

  if($_POST['status']=="accessibilityOff") {
    $take_accessibility_permission=0;
    $m->set_data('take_accessibility_permission',$take_accessibility_permission);
    $a1= array ('take_accessibility_permission'=> $m->get_data('take_accessibility_permission'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Accessibility permission Off ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="accessibilityOn") {
    $take_accessibility_permission=1;
    $m->set_data('take_accessibility_permission',$take_accessibility_permission);
    $a1= array ('take_accessibility_permission'=> $m->get_data('take_accessibility_permission'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Accessibility permission On ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }
  if($_POST['status']=="AllowInDevelperMode") {
    $attendance_with_developer_mode_on=0;
    $m->set_data('attendance_with_developer_mode_on',$attendance_with_developer_mode_on);
    $a1= array ('attendance_with_developer_mode_on'=> $m->get_data('attendance_with_developer_mode_on'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Developer Mode Restrictions Off ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="NotAllowInDevelperMode") {
    $attendance_with_developer_mode_on=1;
    $m->set_data('attendance_with_developer_mode_on',$attendance_with_developer_mode_on);
    $a1= array ('attendance_with_developer_mode_on'=> $m->get_data('attendance_with_developer_mode_on'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Developer Mode Restrictions On ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="setVPNOn") {
    $VPNCheck=1;
    $m->set_data('VPNCheck',$VPNCheck);
    $a1= array ('VPNCheck'=> $m->get_data('VPNCheck'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","VPN Allow ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="setVPNOff") {
    $VPNCheck=0;
    $m->set_data('VPNCheck',$VPNCheck);
    $a1= array ('VPNCheck'=> $m->get_data('VPNCheck'));

    $q=$d->update('users_master',$a1,"user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","VPN Off ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }


  if($_POST['status']=="salaryGroupDeactive") {
    $active_status=1;
    $m->set_data('active_status',$active_status);
    $a1= array ('active_status'=> $m->get_data('active_status'));

    $q=$d->update('salary_group_master',$a1,"salary_group_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary group Deactiveted ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="salaryGroupActive") {
    $active_status=0;
    $m->set_data('active_status',$active_status);
    $a1= array ('active_status'=> $m->get_data('active_status'));

    $q=$d->update('salary_group_master',$a1,"salary_group_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary group activated ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }


  if($_POST['status']=="ShowPhotoGallary") {
    $hide_gallery_view=0;
    $m->set_data('hide_gallery_view',$hide_gallery_view);
    $a1= array ('hide_gallery_view'=> $m->get_data('hide_gallery_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Photo Gallary in mobile app dashboard show");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="hidePhotoGallary") {
    $hide_gallery_view=1;
    $m->set_data('hide_gallery_view',$hide_gallery_view);
    $a1= array ('hide_gallery_view'=> $m->get_data('hide_gallery_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Photo Gallary in mobile app dashboard hide");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="showMyDepartment") {
    $hide_department_employee_view=0;
    $m->set_data('hide_department_employee_view',$hide_department_employee_view);
    $a1= array ('hide_department_employee_view'=> $m->get_data('hide_department_employee_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","My Department in mobile app dashboard show");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="HidesMyDepartment") {
    $hide_department_employee_view=1;
    $m->set_data('hide_department_employee_view',$hide_department_employee_view);
    $a1= array ('hide_department_employee_view'=> $m->get_data('hide_department_employee_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","My Department in mobile app dashboard hide");
      echo 1;
    } else {
      echo 0;
    }
  }


  if($_POST['status']=="ShowUpcomingBirthday") {
    $hide_birthday_view=0;
    $m->set_data('hide_birthday_view',$hide_birthday_view);
    $a1= array ('hide_birthday_view'=> $m->get_data('hide_birthday_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Upcoming Birthday in mobile app dashboard show");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="HideUpcomingBirthday") {
    $hide_birthday_view=1;
    $m->set_data('hide_birthday_view',$hide_birthday_view);
    $a1= array ('hide_birthday_view'=> $m->get_data('hide_birthday_view'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Upcoming Birthday in mobile app dashboard hide");
      echo 1;
    } else {
      echo 0;
    }
  }


  if($_POST['status']=="ShowWorkingHoursinApp") {
    $hide_working_hours=0;
    $m->set_data('hide_working_hours',$hide_working_hours);
    $a1= array ('hide_working_hours'=> $m->get_data('hide_working_hours'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Working hours show in app setting changed (Show)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="hideWorkingHoursinApp") {
    $hide_working_hours=1;
    $m->set_data('hide_working_hours',$hide_working_hours);
    $a1= array ('hide_working_hours'=> $m->get_data('hide_working_hours'));

    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Working hours show in app setting changed (Hide)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="AllowFrontDeskPortalLoginNo") {
    $isActive=0;
    $m->set_data('allow_front_desk_portal_login',$isActive);
    $a1= array ('allow_front_desk_portal_login'=> $m->get_data('allow_front_desk_portal_login'));

    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Front Desk Portal Login Removed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="AllowFrontDeskPortalLoginYes") {
    $isActive=1;
    $m->set_data('allow_front_desk_portal_login',$isActive);
    $a1= array ('allow_front_desk_portal_login'=> $m->get_data('allow_front_desk_portal_login'));
      
    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Front Desk Portal Login Given ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="userAccountActive") {
    $status=0;
    $m->set_data('status',$status);
    $a1= array ('active_status'=> $m->get_data('status')
  );
  
    $q=$d->update('users_entry_login',$a1,"user_entry_id ='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","User Account Active ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }
  if($_POST['status']=="userAccountDeactive") {
    $status=1;
    $m->set_data('status',$status);
    $a1= array ('active_status'=> $m->get_data('status')
  );
  
    $q=$d->update('users_entry_login',$a1,"user_entry_id ='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","User Account Deactivated ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="UserAttendanceMandatoryYes") {
    $isActive=0;
    $m->set_data('is_attendance_mandatory',$isActive);
    $a1= array ('is_attendance_mandatory'=> $m->get_data('is_attendance_mandatory'));

    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attendance Mandatory Setting Cahnged ($id) (Mandatory)");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="UserAttendanceMandatoryNo") {
    $isActive=1;
    $m->set_data('is_attendance_mandatory',$isActive);
    $a1= array ('is_attendance_mandatory'=> $m->get_data('is_attendance_mandatory'));
      
    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attendance Mandatory Setting Cahnged ($id) (Not Mandatory)");
      echo 1;
    } else {
      echo 0;
    }
  }


  if ($_POST['status'] == "hrSubCategoryStatusDeactive") {
    $a1 = array('hr_document_sub_category_status' => 1);
    //print_r($id);die;
    $q = $d->update('hr_document_sub_category_master', $a1, "hr_document_sub_category_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Sub Category Deactivated $id");
        echo 1;
    } else {
        echo 0;
    }
  }
  //////////////////////hr doc  category active

  if ($_POST['status'] == "hrSubCategoryStatusActive") {
      $a1 = array('hr_document_sub_category_status' => 0);
      $q = $d->update('hr_document_sub_category_master', $a1, "hr_document_sub_category_id='$id'");
    
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Sub Category Activated $id");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "changeAccessMode") {
    $a1 = array('access_mode' => $value);
    $q = $d->update('app_access_master', $a1, "access_by_id='$user_id' AND access_type='$access_type'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","App Access Changed");
        echo 1;
    } else {
        echo 0;
    }
  }

  if ($_POST['status'] == "taskPriorityStatusDeactive") {
    $a1 = array('task_priority_status' => 1);
    $q = $d->update('task_priority_master', $a1, "task_priority_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Pririoty Deactiveted ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "taskPriorityStatusActive") {
      $a1 = array('task_priority_status' => 0);
      $q = $d->update('task_priority_master', $a1, "task_priority_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Pririoty Activated ($id)");
        echo 1;
          echo 1;
      } else {
          echo 0;
      }
  }


  if($_POST['status']=="takeAttendanceSelfieAccess") {
    $m->set_data('take_attendance_selfie',$value);
    $a1= array ('take_attendance_selfie'=> $m->get_data('take_attendance_selfie')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attendance Selfi Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="bindMacAddressOn") {
    $isActive=1;
    $m->set_data('bind_mac_address',$isActive);
    $a1= array ('bind_mac_address'=> $m->get_data('bind_mac_address')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Device Mac Bind Setting Changed (On)");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="bindMacAddressOff") {
    $isActive=0;
    $m->set_data('bind_mac_address',$isActive);
    $a1= array ('bind_mac_address'=> $m->get_data('bind_mac_address')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Device Mac Bind Setting Changed (Off)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="allowMultiWFH") {
    $idCount = count($_POST['ids']);
    for ($i=0; $i <$idCount ; $i++) { 
      $a = array('allow_wfh' => $value);
      $user_id = $_POST['ids'][$i];
      $q=$d->update("users_master",$a,"user_id ='$user_id'");
    }
      if($q>0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work from home setting changed ($user_id)");
        echo 1;
      } else {
        echo 0;
      }
  }

  if ($_POST['status'] == "wfhOn") {
    $a1 = array('allow_wfh' => 1);
    $q = $d->update('users_master', $a1, "user_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work from home setting on ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "wfhOff") {
      $a1 = array('allow_wfh' => 0);
      $q = $d->update('users_master', $a1, "user_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work from home setting off ($id)");

          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "taskStepStatusDeactive") {
    $a1 = array('task_step_status' => 1);
    $q = $d->update('task_step_master', $a1, "task_step_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task step Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "taskStepStatusActive") {
      $a1 = array('task_step_status' => 0);
      $q = $d->update('task_step_master', $a1, "task_step_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task step Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "taskStatusDeactive") {
    $a1 = array('task_status' => 1);
    $q = $d->update('task_master', $a1, "task_id='$id'");
    if ($q > 0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Status Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "taskStatusActive") {
      $a1 = array('task_status' => 0);
      $q = $d->update('task_master', $a1, "task_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Status Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "chatGroupStatusDeactive") {
    $a1 = array('active_status' => 1);
    $q = $d->update('chat_group_master', $a1, "group_id='$id'");
    if ($q > 0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Deactiveted ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "chatGroupStatusActive") {
      $a1 = array('active_status' => 0);
      $q = $d->update('chat_group_master', $a1, "group_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "chatGroupMemberStatusDeactive") {
    $a1 = array('active_status' => 1);
    $q = $d->update('chat_group_member_master', $a1, "chat_group_member_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Member Deactivated ($id)");
          echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "chatGroupMemberStatusActive") {
      $a1 = array('active_status' => 0);
      $q = $d->update('chat_group_member_master', $a1, "chat_group_member_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Member Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "roleStatusDeactive") {
    $a1 = array('role_status' => 1);
    $q = $d->update('role_master', $a1, "role_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Role Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "ChangeSalarySlipStatus") {
    $a1 = array('share_with_user' => 1);
    $q = $d->update('salary_slip_master', $a1, "salary_slip_id=$id");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Slip Status Changed ($id)");

        echo 1;
        $q3 = $d->selectRow('*',"salary_slip_master","salary_slip_id='$id'");
        $data3 = mysqli_fetch_assoc($q3);
        $salary_month_name = date('F', strtotime($data3[salary_start_date]));
        $salary_year = date('Y', strtotime($data3[salary_start_date]));

        $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data3[user_id]'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Salary Slip Published";
            $description = "For Month of $salary_month_name, $salary_year";
            $menuClick = "Salary_Slip";
            $image = "";
            $activity = "Salary_Slip";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($data2['user_token']!="") {
              if ($device=='android') {
                  $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
              } else {
                  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
              }

               $notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$data3['user_id'],
                    'notification_title'=>$title,
                    'notification_desc'=>$description,    
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'Salary_Slip',
                    'notification_logo'=>'payslip.png',
                    'notification_type'=>'0',
                );
                
                $d->insert("user_notification",$notiAry);
            }

    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "ChangeSalarySlipDeactiveStatus") {
    $a1 = array('share_with_user' => 0);
    $q = $d->update('salary_slip_master', $a1, "salary_slip_id=$id");
    if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Slip Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "roleStatusActive") {
      $a1 = array('role_status' => 0);
      $q = $d->update('role_master', $a1, "role_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Role Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "salaryCommonValueStatusDeactive") {
    $a1 = array('salary_common_active_status' => 1);
    $q = $d->update('salary_common_value_master', $a1, "salary_common_value_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary common value updated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "salaryCommonValueStatusActive") {
      $a1 = array('salary_common_active_status' => 0);
      $q = $d->update('salary_common_value_master', $a1, "salary_common_value_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary common value Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "EarningDeductionStatusActive") {
    $a1 = array('salary_earning_deduction_status' => 0);
    $q = $d->update('salary_earning_deduction_type_master', $a1, "salary_earning_deduction_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Earning/Deduction Type Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "EarningDeductionStatusDeactive") {
      $a1 = array('salary_earning_deduction_status' => 1);
      $q = $d->update('salary_earning_deduction_type_master', $a1, "salary_earning_deduction_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Earning/Deduction Type Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "currentOpeningStatusDeactive") {
    $a1 = array('company_current_opening_status' => 1);
    $q = $d->update('company_current_opening_master', $a1, "company_current_opening_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Current Opening Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "currentOpeningStatusActive") {
      $a1 = array('company_current_opening_status' => 0);
      $q = $d->update('company_current_opening_master', $a1, "company_current_opening_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Current Opening Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "esclationStatusDeactive") {
    $a1 = array('escalation_active_status' => 1);
    $q = $d->update('escalation_master', $a1, "escalation_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Esclation Status Deactiveted ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "esclationStatusActive") {
      $a1 = array('escalation_active_status' => 0);
      $q = $d->update('escalation_master', $a1, "escalation_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Esclation Status Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "levelDeactive") {
    $a1 = array('level_status' => 1);
    $q = $d->update('employee_level_master', $a1, "level_id='$id'");
    if ($q > 0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "levelActive") {
      $a1 = array('level_status' => 0);
      $q = $d->update('employee_level_master', $a1, "level_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "zoneDeactive") {
    $a1 = array('zone_status' => 1);
    $q = $d->update('zone_master', $a1, "zone_id='$id'");
    if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Zone Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "zoneActive") {
      $a1 = array('zone_status' => 0);
      $q = $d->update('zone_master', $a1, "zone_id='$id'");
      if ($q > 0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Zone Activated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }
  /* birthdayNotification */
  if ($_POST['status'] == "birthdayNotification")
  {
      $q2 = $d->selectRow('user_token,device,user_full_name',"users_master","user_id='$user_id'");
      $data2 = mysqli_fetch_assoc($q2);
      $menuClick = "custom_notification";
      if($celebrationType == "birthday")
      {
        $image = "happy-birthday.png";
        $imageUrl = $base_url."img/app_icon/happy-birthday.png";
      }
      elseif($celebrationType == "wedding")
      {
        $image = "happy_wedding_anniversary.png";
        $imageUrl = $base_url."img/app_icon/happy_wedding_anniversary.png";
      }
      else
      {
        $image = "happy_work_anniversary.png";
        $imageUrl = $base_url."img/app_icon/happy_work_anniversary.png";
      }
        $activity = array(
              'title' => $title,
              'description' => $description,
              'img_url' => $imageUrl,
              'notification_time' => date("h:i A, dS M Y"),
        );

      $user_token = $data2['user_token'];
      $device = $data2['device'];
      if ($device=='android')
      {
        $nResident->noti($menuClick,$imageUrl,$society_id,$user_token,$title,$description,$activity);
      }
      else
      {
        // $activity = [];
        $nResident->noti_ios($menuClick,$imageUrl,$society_id,$user_token,$title,$description,$activity);
      }
      $q = $d->insertUserNotification($society_id,$title,$description,"custom_notification",$image,"user_id = '$user_id'");
      if(isset($url) && $url == "celebrationsList")
      {
        $_SESSION['msg'] = "Wish Sent Successfully";
        header("Location: ../$url?bId=$block_id&dId=$department_id&celebrationTypeFilter=&csrf=$csrf");exit;
      }
      else
      {
        $_SESSION['msg'] = "Wish Sent Successfully";
        header("Location: ../welcome");exit;
      }
  }
  /* birthdayNotification */
  /* Dollop 1  Dec 2021  */

  
  if ($_POST['status'] == "UpdatexeroxStatus") {
    $a1 = array('xerox_doc_status' =>$xerox_doc_master );
        $q = $d->update('xerox_doc_master', $a1, "xerox_paper_size_id='$id'");
        if ($q > 0) {
            echo 1;
            if($xerox_doc_master=="0"){
              $status = "Pending";
            }
            else if($xerox_doc_master=="1")
            {
              $status = "Approved";

            }
            else if($xerox_doc_master=="2")
            {
              $status = "Inprogress";

            }
            else if($xerox_doc_master=="3")
            {
              $status = "Ready";

            }
            else if($xerox_doc_master=="4")
            {
              $status = "Delivered";
            }
            else
            {
              $status = "Cancel";

            }
            /* notification */

            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your Xerox Document Order ".$status;
            $description = "By Admin $created_by";
            $menuClick = "";
            $image = "";
            $activity = "";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }
            /* notification */
        } else {
            echo 0;
        }
  }

  if ($_POST['status'] == "vendorProductCategoryActive") {
    $a1 = array('vendor_product_category_status' => 0);
        $q = $d->update('vendor_product_category_master', $a1, "vendor_product_category_id='$id'");
        if ($q > 0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Product Category Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
}
if ($_POST['status'] == "vendorProductCategoryDeactive") {
    $a1 = array('vendor_product_category_status' => 1);
        $q = $d->update('vendor_product_category_master', $a1, "vendor_product_category_id='$id'");
        if ($q > 0) {
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Product Category Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
}
  /* Dollop 1  Dec 2021  */


    /* Dollop Infotech date 15-Nov-2021 -   */

        if ($_POST['status'] == "paperSizeActive") {
          $a1 = array('xerox_paper_size_status' => 0);
              $q = $d->update('xerox_paper_size', $a1, "xerox_paper_size_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }
        if ($_POST['status'] == "paperSizeDeactive") {
          $a1 = array('xerox_paper_size_status' => 1);
              $q = $d->update('xerox_paper_size', $a1, "xerox_paper_size_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }

        if ($_POST['status'] == "xeroxTypeActive") {
          $a1 = array('xerox_type_status' => 0);
              $q = $d->update('xerox_type_category', $a1, "xerox_type_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }
        if ($_POST['status'] == "xeroxTypeDeactive") {
          $a1 = array('xerox_type_status' => 1);
              $q = $d->update('xerox_type_category', $a1, "xerox_type_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }

        if ($_POST['status'] == "xeroxDocActive") {
          $a1 = array('xerox_doc_status' => 0);
              $q = $d->update('xerox_doc_master', $a1, "xerox_doc_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }
        if ($_POST['status'] == "xeroxDocDeactive") {
          $a1 = array('xerox_doc_status' => 1);
              $q = $d->update('xerox_doc_master', $a1, "xerox_doc_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
        }

        if ($_POST['status'] == "UserDeleteActive") {
            $q3 = $d->selectRow('*',"users_master", "user_id='$id'");
            $data3 = mysqli_fetch_assoc($q3); 
            $a1 = array('delete_status' => 0);
            $a2 = array('unit_delete_status' => 0);
            $qUser = $d->update('users_master', $a1, "user_id='$id'");
            $qUnit = $d->update('unit_master', $a2, "unit_id='$data3[unit_id]'");
                if ($qUser > 0) {
                  $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' ");
                  $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' ");
                  $title = "sync_data";
                  $description = "Face Data Updated on ".date('d-M-y h:i A');
                  $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
                  $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

                    echo 1;

                  $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Re Activated ($id)");

                } else {
                    echo 0;
                }
        }
        

        if ($_POST['status'] == "canteenVendorStatusDeactive") {
                
            $a1 = array('vendor_active_status' => 1);
            $q = $d->update('vendor_master', $a1, "vendor_id='$id'");
            if ($q > 0) {

                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "canteenVendorStatusActive") {
            
            $a1 = array('vendor_active_status' => 0);
            $q = $d->update('vendor_master', $a1, "vendor_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "stationaryVendorStatusDeactive") {
                
            $a1 = array('vendor_active_status' => 1);
            $q = $d->update('vendor_master', $a1, "vendor_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "stationaryVendorStatusActive") {
            
            $a1 = array('vendor_active_status' => 0);
            $q = $d->update('vendor_master', $a1, "vendor_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "canteenProductDeactive") {
                
            $a1 = array('vendor_product_active_status' => 1);
            $q = $d->update('vendor_product_master', $a1, "vendor_product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "canteenProductActive") {
            
            $a1 = array('vendor_product_active_status' => 0);
            $q = $d->update('vendor_product_master', $a1, "vendor_product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "stationaryProductDeactive") {
                
            $a1 = array('vendor_product_active_status' => 1);
            $q = $d->update('vendor_product_master', $a1, "vendor_product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "stationaryProductActive") {
            
            $a1 = array('vendor_product_active_status' => 0);
            $q = $d->update('vendor_product_master', $a1, "vendor_product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "popularProductNo") {
                
            $a1 = array('popular_product' => 0);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "popularProductYes") {
            
            $a1 = array('popular_product' => 1);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "stationaryProductVariantDeactive") {
           
            $a1 = array('vendor_product_variant_active_status' => 1);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "stationaryProductVariantActive") {
            
            $a1 = array('vendor_product_variant_active_status' => 0);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "canteenProductVariantDeactive") {
                
            $a1 = array('vendor_product_variant_active_status' => 1);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "canteenProductVariantActive") {
            
            $a1 = array('vendor_product_variant_active_status' => 0);
            $q = $d->update('vendor_product_variant_master', $a1, "vendor_product_variant_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
       

        


        if ($_POST['status'] == "userWorkReportPermissionActive") {
            
            $a1 = array('user_work_report_on' => 0);
            $q = $d->update('users_master', $a1, "user_id='$id'");
            if ($q > 0) {
                echo 1;
                /* notification */
                
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$id'");
                $data2 = mysqli_fetch_assoc($q2);
                $title = "Your employee work report permission is active";
                $description = "By Admin $created_by";
                $menuClick = "";
                $image = "";
                $activity = "";
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device=='android') {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                } else {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }

                $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work Report Seting Changed ($id) OFF");
                /* notification */
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "userWorkReportPermissionDeactive") {
            
            $a1 = array('user_work_report_on' => 1);
            $q = $d->update('users_master', $a1, "user_id='$id'");
            if ($q > 0) {
                echo 1;
                /* notification */
                
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$id'");
                $data2 = mysqli_fetch_assoc($q2);
                $title = "Your employee work report permission is Deactive";
                $description = "By Admin $created_by";
                $menuClick = "";
                $image = "";
                $activity = "";
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device=='android') {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                } else {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Work Report Seting Changed ($id) ON");
                /* notification */
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "productVariantImageDeactive") {
            
            $a1 = array('vendor_product_image_active_status' => 1);
            $q = $d->update('vendor_product_image_master', $a1, "vendor_product_image_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "productVariantImageActive") {
            
            $a1 = array('vendor_product_image_active_status' => 0);
            $q = $d->update('vendor_product_image_master', $a1, "vendor_product_image_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "adminAttendanceTakingOff") {
            $a1 = array('is_attendance_taking_on' => 0, 'admin_pin' => '');
                $q = $d->update('bms_admin_master', $a1, "admin_id='$id'");
                if ($q > 0) {
                  // logout out device 
                    $q2 = $d->select("face_app_device_master","society_id= '$society_id' AND admin_id='$id'");
                    while ($data2 = mysqli_fetch_array($q2)) {
                        
                        $face_app_device_id = $data2['face_app_device_id'];
                        $fcmArray = $data2['user_token'];
                        $title = "logout";
                        $description = "Admin Account Removed";
                        $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");

                       
                        
                        $q = $d->delete("face_app_device_master", "face_app_device_id  ='$face_app_device_id'");

                    }
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Face app admin removed ($id)");
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "adminAttendanceTakingOn") {
                $adminIdLen = strlen($id);
                $remningLen = 4 - $adminIdLen; 
                $admin_pin = rand(pow(10, $remningLen - 1), pow(10, $remningLen) - 1);
                $admin_pin = $admin_pin.$id;

                $a1 = array('is_attendance_taking_on' => 1, 'admin_pin' => $admin_pin);
                $q = $d->update('bms_admin_master', $a1, "admin_id='$id'");
                if ($q > 0) {
                  $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Face app admin access given ($id)");
                    echo 1;
                } else {
                    echo 0;
                }
        }


    
        if ($_POST['status'] == "expenseOff") {
            
            $a1 = array('expense_on_off' => 1);
            $q = $d->update('floors_master', $a1, "floor_id='$id'");
            if ($q > 0) {
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "department Expenses setting changed ($id)");
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "expenseOn") {
            
            $a1 = array('expense_on_off' => 0);
            $q = $d->update('floors_master', $a1, "floor_id='$id'");
            if ($q > 0) {
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "department Expenses setting changed ($id)");
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "approvedEmployeeExpenses") {
          $a1 = array('expense_status' => 1,'expense_approved_by_id'=>$_COOKIE['bms_admin_id'],'expense_approved_by_type'=>1);
          $q = $d->update('user_expenses', $a1, "user_expense_id='$id' ");
          if ($q > 0) {
              echo 1;
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Expenses Approved");
              $_SESSION['msg'] = "Employee Expenses Approved";
              $q1 = $d->selectRow('user_id,expense_title,date,amount',"user_expenses","user_expense_id='$id'");
              $data1 = mysqli_fetch_assoc($q1);    
              $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data1[user_id]'");
              $data2 = mysqli_fetch_assoc($q2);
              $title = "Your expense request is Approved";
              $description ="By Admin, ".$created_by."\nExpense : ".$data1['expense_title']."\nAmount : ".$data1['amount']."\nFor Date : ".date('d F Y',strtotime($data1['date']));
              $menuClick = "expense_page";
              $image = "";
              $activity = "0";
              $user_token = $data2['user_token'];
              $device = $data2['device'];
              if ($device=='android') {
                  $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
              } else {
                  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
              }
              $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png","$activity","users_master.user_id = '$data1[user_id]'");
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expenses Approved($id)");
          } else {
              echo 0;
              $_SESSION['msg1'] = "Something Wrong";
          }
        }

        if ($_POST['status'] == "declinedEmployeeExpenses") {
            $a1 = array('expense_status' => 2,
                        'expense_reject_by_id'=>$_COOKIE['bms_admin_id'],
                        'expense_reject_by_type'=>1,
                        'expense_reject_reason' => $expense_reject_reason);
            $q = $d->update('user_expenses', $a1, "user_expense_id='$user_expense_id'");
            if ($q > 0) {
                echo 1;
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Expenses Declined");
                $_SESSION['msg'] = "Employee Expenses Declined";
                $q1 = $d->selectRow('user_id,expense_title,date,amount',"user_expenses","user_expense_id='$user_expense_id'");
                $data1 = mysqli_fetch_assoc($q1);    
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data1[user_id]'");
                $data2 = mysqli_fetch_assoc($q2);
          
                $title = "Your expense request is Declined";
                $description ="By Admin, ".$created_by."\nExpense : ".$data1['expense_title']."\nAmount : ".$data1['amount']."\nFor Date : ".date('d F Y',strtotime($data1['date']));
                $menuClick = "expense_page";
                $image = "";
                $activity[] = "0";
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device=='android') {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                } else {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                $activity = "0";
                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png","$activity","users_master.user_id = '$data1[user_id]'");

                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expenses Declined ($id)");

                header("Location: ../employeeExpenses?bId=$bId&dId=$dId&ueMonth=$ueMonth&ueYear=$ueYear");
            } else {
              $_SESSION['msg1'] = "Something Wrong";
              header("Location: ../employeeExpenses?bId=$bId&dId=$dId&ueMonth=$ueMonth&ueYear=$ueYear");
            }
        }

         ///site active status
        if ($_POST['status'] == "siteStatusDeactive") {
            $a1 = array('site_active_status' => 1);
                $q = $d->update('site_master', $a1, "site_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }
        if ($_POST['status'] == "siteStatusActive") {
            $a1 = array('site_active_status' => 0);
                $q = $d->update('site_master', $a1, "site_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }
        /// Unit Measurement Status
        
        if ($_POST['status'] == "unitMeasurementStatusActive") {
            $a1 = array('unit_measurement_status' => 0);
                $q = $d->update('unit_measurement_master', $a1, "unit_measurement_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }
        if ($_POST['status'] == "unitMeasurementStatusDeactive") {
            $a1 = array('unit_measurement_status' => 1);
                $q = $d->update('unit_measurement_master', $a1, "unit_measurement_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        ///category status
        if ($_POST['status'] == "productCategoryStatusActive") {
            $a1 = array('product_category_status' => 0);
                $q = $d->update('product_category_master', $a1, "product_category_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "productCategoryStatusDeactive") {
            $a1 = array('product_category_status' => 1);
                $q = $d->update('product_category_master', $a1, "product_category_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }
        if ($_POST['status'] == "productVarientStatusActive") {
            $a1 = array('variant_active_status' => 0);
                $q = $d->update('product_variant_master', $a1, "product_variant_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "productVarientStatusDeactive") {
            $a1 = array('variant_active_status' => 1);
                $q = $d->update('product_variant_master', $a1, "product_variant_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "productVendoCategoryStatusActive") {
          $a1 = array('active_status' => 0);
              $q = $d->update('product_category_vendor_master', $a1, "product_category_vendor_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
      }

      if ($_POST['status'] == "productVendoCategoryStatusDeactive") {
          $a1 = array('active_status' => 1);
              $q = $d->update('product_category_vendor_master', $a1, "product_category_vendor_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
      }
        if ($_POST['status'] == "productPriceStatusActive") {
          $a1 = array('product_active_status' => 0);
              $q = $d->update('product_price_master', $a1, "product_price_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
      }

      if ($_POST['status'] == "productPriceStatusDeactive") {
          $a1 = array('product_active_status' => 1);
              $q = $d->update('product_price_master', $a1, "product_price_id='$id'");
              if ($q > 0) {
                  echo 1;
              } else {
                  echo 0;
              }
      }
        ///sub category status update
        if ($_POST['status'] == "subCategoryStatusDeactive") {
            $a1 = array('product_sub_category_status' => 1);
                $q = $d->update('product_sub_category_master', $a1, "product_sub_category_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "subCategoryStatusActive") {
            $a1 = array('product_sub_category_status' => 0);
                $q = $d->update('product_sub_category_master', $a1, "product_sub_category_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }
        if ($_POST['status'] == "productVendoSubCatStatusDeactive") {
            $a1 = array('active_status' => 1);
                $q = $d->update('product_sub_category_vendor_master', $a1, "product_sub_category_vendor_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "productVendoSubCatStatusActive") {
            $a1 = array('active_status' => 0);
                $q = $d->update('product_sub_category_vendor_master', $a1, "product_sub_category_vendor_id='$id'");
                if ($q > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
        }

        if ($_POST['status'] == "productStatusDeactive") {
            $a1 = array('product_status' => 1);
            $q = $d->update('product_master', $a1, "product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "productStatusActive") {
            $a1 = array('product_status' => 0);
            $q = $d->update('product_master', $a1, "product_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }

        if ($_POST['status'] == "orderStatus")
        {
            $a1 = array('order_status' => $value);
            $q = $d->update('order_master', $a1, "order_id='$id'");
            if ($q > 0) {
                echo 1;
                $q1 = $d->selectRow('user_id',"order_master","order_id='$id'");
                $data1 = mysqli_fetch_assoc($q1);    
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data1[user_id]'");
                $data2 = mysqli_fetch_assoc($q2);

                if($value == 0){
                    $title = "Your order is Placed";
                }
                else if($value == 1){
                    $title = "Your order is Preparing";
                }
                else if($value == 2){
                    $title = "Your order is Despatched";
                }
                else if($value == 3){
                    $title = "Your order is Delivered";
                }
                else{
                    $title = "Your order is Cancelled";
                }
                
                $description = "By Admin $created_by";
                $menuClick = "";
                $image = "";
                $activity = "";
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device=='android') {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                } else {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }

                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Order Status Changed  ($id)"); 
            } else {
                echo 0;
            }
        }


        if ($_POST['status'] == "vendorcategoryMasterStatusDeactive") {
            $a1 = array('vendor_category_satus' => 1);
            $q = $d->update('vendor_category_master', $a1, "vendor_category_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
        if ($_POST['status'] == "vendorcategoryMasterStatusActive") {
            $a1 = array('vendor_category_satus' => 0);
            $q = $d->update('vendor_category_master', $a1, "vendor_category_id='$id'");
            if ($q > 0) {
                echo 1;
            } else {
                echo 0;
            }
        }
    /* Dollop Infotech date 22-oct-2021 -  */


    if ($_POST['status'] == "attendaceStatus") {
        
        $attendance_id = $id;
        
        $sq=$d->selectRow("*","attendance_master","attendance_id ='$attendance_id'");
        $attData=mysqli_fetch_array($sq);
        extract($attData);
        $shiftTimeId = $shift_time_id;
        $attendance_user_id = $user_id;
        $qsm = $d->selectRow("user_token,device,shift_time_id,unit_id,user_full_name","users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
        $data_notification = mysqli_fetch_array($qsm);
        $user_token = $data_notification['user_token'];
        $device = $data_notification['device'];
        $unit_id = $data_notification['unit_id'];
        // $shiftTimeId = $data_notification['shift_time_id'];
        $tabPosition = "0"; 

        $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
        $shiftData = mysqli_fetch_array($shiftQry);
        $perdayHours = $shiftData['per_day_hour'];
        $early_out_time = $shiftData['early_out_time'];
        $half_day_time_start = $shiftData['half_day_time_start'];
        $shift_end_time = $shiftData['shift_end_time'];
        $halfday_before_time = $shiftData['halfday_before_time'];
        $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
        $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
        $maximum_in_out = $shiftData['maximum_in_out'];

        if($value == 1 && $attendance_date_end!="0000-00-00"){
          $parts = explode(':', $perdayHours);
          $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
          $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;
          $totalHours = getTotalHours($attendance_date_start,$attendance_date_end,$punch_in_time,$punch_out_time);
          $totalHoursName = getTotalHoursWithNames($attendance_date_start,$attendance_date_end,$punch_in_time,$punch_out_time);
          $times[] = $totalHours;
          $time = explode(':', $totalHours.":00");
          $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

          $avgWorkingDays = $total_minutes/$perDayHourMinute;
          $avg_working_days = round($avgWorkingDays * 2) / 2;

        } else {
          $avg_working_days  = 0;
        }

        $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

        $hasLeave = false;

        if (mysqli_num_rows($qryLeave) > 0) {

            $hasLeave = true;

            $leaveData = mysqli_fetch_array($qryLeave);

            $leave_id = $leaveData['leave_id'];
            $leave_type_id = $leaveData['leave_type_id'];
            $paid_unpaid = $leaveData['paid_unpaid'];
            $leave_reason = $leaveData['leave_reason'];
            $leave_day_type = $leaveData['leave_day_type'];
            $half_day_session = $leaveData['half_day_session'];
        }

        $attendance_date = $attendance_date_end;
        $attendance_time = $punch_out_time;

        $datePunchOUT = $attendance_date ." ".$attendance_time;

        if ($early_out_time!="") {
          // code...
          $partEO = explode(':', $early_out_time);
          $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
          $defaultOutTime = $attendance_date." ".$shift_end_time;
          $strMin = "-".$min." minutes";
          $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));
        }

        // $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

        if ($datePunchOUT >= $defaultOutTime) {
            $early_out = "0";
        }else{
            $early_out = "1";              
        }

        if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
            $early_out = "0";
        }

        if ($attendance_id > 0) {
          echo 1;
            if($value == 1){
              $a1 = array(
                'early_out' => $early_out,
                'attendance_status' => 1,
                'attendance_status_change_by'=>$_COOKIE['bms_admin_id'],
                'attendance_status_change_by_type'=>1,
                'total_working_hours'=>$totalHours,
                'avg_working_days'=>$avg_working_days,
              );
        
              $q = $d->update('attendance_master', $a1, "attendance_id='$attendance_id'");

              $menuClick = "attendance";
              $title = "Your Attendance is Approved";
              $description = "For Date: ".date('d F Y',strtotime($attendance_date_start))."\nBy Admin, ".$created_by;

              if ($attendance_date_end!="0000-00-00") {
               

                $sDTime = $attendance_date_start." ".$punch_in_time;
                $eDTime = $attendance_date_end." ".$punch_out_time;

                $pTime = date('Y-m-d h:i A',strtotime($sDTime));
                $eTime = date('Y-m-d h:i A',strtotime($eDTime));

                $date_a = new DateTime($pTime);
                $date_b = new DateTime($eTime);

                $interval = $date_a->diff($date_b);
               
                $days = $interval->format('%d')*24;
                $hours = $interval->format('%h');
                $hours = $hours+$days;
                $minutes = $interval->format('%i');
                $sec = $interval->format('%s');

                $hr = sprintf('%02d:%02d', $hours, $minutes);

               
                $time = explode(':', $hr.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

              
               
                if ($hours > 0 && $minutes) {
                    $totalHours = sprintf('%02d hr %02d min', $hours, $minutes);
                }else if ($hours > 0 && $minutes <= 0) {
                    $totalHours=  sprintf('%02d hr', $hours);
                }else if ($hours <= 0 && $minutes > 0) {
                    $totalHours = sprintf('%02d min', $minutes);
                }else{
                    $totalHours = "No Data";
                }

                $leaveValue = false;

                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $attendance_time) {
                    $leaveValue = true;
                }



                $maximum_halfday_hours = explode(':', $maximum_halfday_hours);
                $maximum_halfday_hours_min =  ($maximum_halfday_hours[0]*60) + ($maximum_halfday_hours[1]) + ($maximum_halfday_hours[2]/60);

                $minimum_hours_for_full_day_min = explode(':', $minimum_hours_for_full_day);
                $minimum_hours_for_full_day_min =  ($minimum_hours_for_full_day_min[0]*60) + ($minimum_hours_for_full_day_min[1]) + ($minimum_hours_for_full_day_min[2]/60);


                if ($total_minutes >= $minimum_hours_for_full_day_min) {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }

                $alreadyLeaveQryCheck = false; 

                if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {

                    $leave_total_days = 1;

                    // check half day and full day

                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $half_day_session = "0";   
                        $workingDaysTotal = "0";             
                    }else{
                        $leaveType = "1";
                        $leaveTypeName = "Half Day";
                        $workingDaysTotal = "0.5";
                    }

                    if ($hasLeave == true) {

                        // Change already applied leave

                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);
                        $m->set_data('auto_leave_reason',$titleMessage);

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'half_day_session'=>$m->get_data('half_day_session'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        );

                        $leaveQry = $d->update("leave_master",$a,"user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
                        

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                        
                    }else{

                        // Auto Leave

                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$attendance_user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$attendance_date_start);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $alreadyLeaveQryCheck = true;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$workingDaysTotal);
                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    
                }else if($noLeave == true && $hasLeave == true){
                    // Remove leave if present and full time

                    $leaveQry = $d->delete("leave_master","user_id = '$attendance_user_id' AND leave_id = '$leave_id'");

                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                }

                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave

                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND late_in = '1'");

                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND early_out = '1'");

                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                if ($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($late_in == "1" || $early_out == "1")) {

                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$attendance_user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$attendance_date_start);
                    $m->set_data('leave_end_date',$attendance_date);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'), 
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date'),
                    );

                    $leaveQry = $d->insert("leave_master",$a);

                    $title = "Auto Leave Applied - Half Day";

                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");

                    $m->set_data('auto_leave',"1");

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }

              }
            }
            else {

              $a1 = array(
                'attendance_status' => 2,
                'attendance_declined_reason'=>$attendance_declined_reason,
                'attendance_status_change_by'=>$_COOKIE['bms_admin_id'],
                'attendance_status_change_by_type'=>1,
                'avg_working_days'=>0,
              );
        
              $q = $d->update('attendance_master', $a1, "attendance_id='$attendance_id'");
              $d->delete("leave_master","leave_start_date='$attendance_date_start' AND user_id='$user_id' ");

              $title = "Your Attendance is Rejected";
              $menuClick = "attendance";
              $description = "For Date: ".date('d F Y',strtotime($attendance_date_start))."\nBy Admin, ".$created_by;
          }
            
            $image = "";
            $activity = "";
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }
            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"attendance","attendance_tracker.png","$activity","users_master.user_id = '$data1[user_id]'");
        } else {
         echo 0;
        }

        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance status changed  ($attendance_id)"); 
    }

     /////////////chapter status update////////////////////

    if ($_POST['status'] == "courseChapterStatusDeactive") {
        $a1 = array('course_chapter_is_active' => 1);
        $q = $d->update('course_chapter_master', $a1, "course_chapter_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Chapter Deactivated ($id)"); 
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "courseChapterStatusActive") {
        $a1 = array('course_chapter_is_active' => 0);
        $q = $d->update('course_chapter_master', $a1, "course_chapter_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Chapter Activated ($id)"); 
            echo 1;
        } else {
            echo 0;
        }
    }
     /////////////lesson type status update////////////////////

    if ($_POST['status'] == "courseLessonTypeStatusDeactive") {
        $a1 = array('is_active' => 1);
        $q = $d->update('lesson_type_master', $a1, "lesson_type_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Deactivated ($id)"); 
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "courseLessonTypetatusActive") {
        $a1 = array('is_active' => 0);
        $q = $d->update('lesson_type_master', $a1, "lesson_type_id='$id'");
        if ($q > 0) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Activated ($id)"); 

            echo 1;
        } else {
            echo 0;
        }
    }

    /////////////Attendance Type status update////////////////////

    if ($_POST['status'] == "attendanceTypeStatusDeactive") {
        $a1 = array('attendance_type_active_status' => 1);
        $q = $d->update('attendance_type_master', $a1, "attendance_type_id='$id'");
        if ($q > 0) {
          
            echo 1;
            $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0'");
          $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1'");
          $title = "sync_data";
          $description = "Face Data Updated on ".date('d-M-y h:i A');
          $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
          $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Status Changed ($id)"); 
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "attendanceTypeStatusActive") {
        $a1 = array('attendance_type_active_status' => 0);
        $q = $d->update('attendance_type_master', $a1, "attendance_type_id='$id'");
        if ($q > 0) {
            echo 1;
            $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0'");
            $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1'");
            $title = "sync_data";
            $description = "Face Data Updated on ".date('d-M-y h:i A');
            $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
            $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Status Changed ($id)"); 
        } else {
            echo 0;
        }
    }




    /////////////////////////salary


    if ($_POST['status'] == "salaryStatusActive") {
        $a1 = array('is_active' =>0);
        $q = $d->update('salary', $a1, "salary_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Defualt Salary Status Changed ($id)"); 
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "salaryStatusDeactive") {
        $a1 = array('is_active' => 1);
        $q = $d->update('salary', $a1, "salary_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Defualt Salary Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }

    /////////////leave Default Count status update////////////////////

    if ($_POST['status'] == "lessonStatusDeactive") {
        $a1 = array('lesson_is_active' => 1);
        $q = $d->update('course_lessons_master', $a1, "lessons_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Lesson Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "lessonStatusActive") {
        $a1 = array('lesson_is_active' => 0);
        $q = $d->update('course_lessons_master', $a1, "lessons_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Lesson Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }


    /////////////leave Default Count status update////////////////////

    if ($_POST['status'] == "leaveDefaultCountDeactive") {
        $a1 = array('active_status' => 1);
        $q = $d->update('leave_default_count_master', $a1, "leave_default_count_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Default Leave Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "leaveDefaultCountActive") {
        $a1 = array('active_status' => 0);
        $q = $d->update('leave_default_count_master', $a1, "leave_default_count_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Default Leave Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }


    /////////////leave assign status update////////////////////

    if ($_POST['status'] == "leaveAssignStatusDeactive") {
        $a1 = array('leave_assign_active_status' => 1);
        $q = $d->update('leave_assign_master', $a1, "user_leave_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Assing Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "leaveAssignStatusActive") {
        $a1 = array('leave_assign_active_status' => 0);
        $q = $d->update('leave_assign_master', $a1, "user_leave_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Assing Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }


    /////////////leave type status update////////////////////

    if ($_POST['status'] == "leaveTypeStatusDeactive") {
        $a1 = array('leave_type_active_status' => 1);
        $q = $d->update('leave_type_master', $a1, "leave_type_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Type Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "leaveTypeStatusActive") {
        $a1 = array('leave_type_active_status' => 0);
        $q = $d->update('leave_type_master', $a1, "leave_type_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Type Status Changed ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }

    /////////////idea Master status update////////////////////
    if ($_POST['status'] == "ideaMasterStatus") {
      if($value == 0){
          $idea_approved_by = "";
          $idea_declined_by = "";
      }
      elseif($value == 1){
          $idea_approved_by = $_COOKIE['bms_admin_id'];
          $idea_declined_by = "";
      }
      else{
          $idea_approved_by = "";
          $idea_declined_by = $_COOKIE['bms_admin_id'];
      }
      $a1 = array('idea_status' => $value, 
      'idea_approved_by' => $idea_approved_by, 
      'idea_declined_by' => $idea_declined_by,
      'idea_status_change_by_type' => 1);
      $q = $d->update('idea_master', $a1, "idea_id='$id'");
      if ($q > 0) {
          echo 1;
          $q1 = $d->selectRow('user_id',"idea_master","idea_id='$id'");
          $data1 = mysqli_fetch_assoc($q1);    
          $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data1[user_id]'");
          $data2 = mysqli_fetch_assoc($q2);
          if($value == 1){
            $title = "Idea Box";
            $description = "New idea approved by $created_by";
            $menuClick = "idea_page";
            $image = "";
            $activity = $id;
            $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' ");
            $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' ");
            $nResident->noti($menuClick,$image,$society_id,$fcmArray,$title,$description,$menuClick);
            $nResident->noti_ios($menuClick,$image,$society_id,$fcmArrayIos,$title,$description,$menuClick);
            $d->insertUserNotification($society_id,$title,$description,"idea_page","idea_box.png","");

            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "New Idea Approved ($id)");
          }
          else if($value == 2){
            $title = "Idea Box";
            $description = "New idea Declined by $created_by";
            $menuClick = "idea_page";
            $image = "";
            $activity = $id;
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }
            $d->insertUserNotification($society_id,$title,$description,"idea_page","idea_box.png","user_id='$data1[user_id]'");

            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "New Idea Declined ($id)");
          }
      } else {
          echo 0;
      }
    }

    ///////////////////////////////leave approved declined

    if ($_POST['status'] == "leaveStatus") {
       
        if($value == 0){
            $leave_approved_by = "";
            $leave_declined_by = "";
        }
        elseif($value == 1){
            $leave_approved_by = $_COOKIE['bms_admin_id']; 
            $leave_declined_by = "";           
        }
        else{
            $leave_declined_by = $_COOKIE['bms_admin_id'];
            $leave_approved_by = "";
        }
        $a1 = array('leave_status' => $value,'leave_admin_reason'=>$leave_admin_reason,'leave_approved_by'=>$leave_approved_by,'leave_declined_by'=>$leave_declined_by);
        $q = $d->update('leave_master', $a1, "leave_id='$id'");
        if ($q > 0) {
            echo 1;
            $q1 = $d->selectRow('user_id',"leave_master","leave_id='$id'");
            $data1 = mysqli_fetch_assoc($q1);    
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data1[user_id]'");
            $data2 = mysqli_fetch_assoc($q2);

            if($value == 1){
                $title = "Your leave is Approved";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Status Changed ($id) Approved");
            }
            else if($value == 2){
                $title = "Your leave is Declined";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Status Changed ($id) Declined");
            }
            else{
                $title = "Your leave is Pending";
                 $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Status Changed ($id) Pending");
            }
            
            $description = "By Admin $created_by";
            $menuClick = "leave_tracker";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }
        } else {
            echo 0;
        }
    }


    /////////////idea Category status update////////////////////

    if ($_POST['status'] == "ideaCategoryDeactive") {
        $a1 = array('idea_category_active_status' => 1);
        $q = $d->update('idea_category_master', $a1, "idea_category_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Idea Category Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    if ($_POST['status'] == "ideaCategoryActive") {
        $a1 = array('idea_category_active_status' => 0);
        $q = $d->update('idea_category_master', $a1, "idea_category_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Idea Category Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
/////////////////////////////////////////course status

if ($_POST['status'] == "courseStatusDeactive") {
    $a1 = array('course_is_active' => 1);
    $q = $d->update('course_master', $a1, "course_id='$id'");
    if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Category Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
}

if ($_POST['status'] == "courseStatusActive") {
    $a1 = array('course_is_active' => 0);
    $q = $d->update('course_master', $a1, "course_id='$id'");
    if ($q > 0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Category Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
}


    //////////////////////hr doc deactive

    if ($_POST['status'] == "hrdocDeactive") {
        $a1 = array('hr_document_status' => 1);
        //print_r($id);die;
        $q = $d->update('hr_document_master', $a1, "hr_document_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Document Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    //////////////////////hr doc aactive

    if ($_POST['status'] == "hrdocActive") {
        $a1 = array('hr_document_status' => 0);
        $q = $d->update('hr_document_master', $a1, "hr_document_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Document Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    //////////////////////holiday

    if ($_POST['status'] == "holidayStatusDeactive") {
        $a1 = array('holiday_status' => 1);
        //print_r($id);die;
        $q = $d->update('holiday_master', $a1, "holiday_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    //////////////////////holiday aactive

    if ($_POST['status'] == "holidayStatusActive") {
        $a1 = array('holiday_status' => 0);
        $q = $d->update('holiday_master', $a1, "holiday_id='$id'");
        if ($q > 0) {
           $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    //////////////////////hr doc category deactive

    if ($_POST['status'] == "hrCategoryStatusDeactive") {
        $a1 = array('hr_document_category_status' => 1);
        //print_r($id);die;
        $q = $d->update('hr_document_category_master', $a1, "hr_document_category_id='$id'");
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Document Category Deactivated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }
    //////////////////////hr doc  category active

    if ($_POST['status'] == "hrCategoryStatusActive") {
        $a1 = array('hr_document_category_status' => 0);
        $q = $d->update('hr_document_category_master', $a1, "hr_document_category_id='$id'");
      
        if ($q > 0) {
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Document Category Activated ($id)");
            echo 1;
        } else {
            echo 0;
        }
    }


    /////////////////////////attendance

  if($_POST['status']=="SpDeactive") {
    $a1= array ('service_provider_status'=> 1);
    $data=$d->selectArray('local_service_provider_users',"service_provider_users_id='$id'");
    $name = $data['service_provider_name'];
    $q=$d->update('local_service_provider_users',$a1,"service_provider_users_id='$id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Service Provider Deactivated ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="SpActive") {
    $a1= array ('service_provider_status'=> 0);
    $data=$d->selectArray('local_service_provider_users',"service_provider_users_id='$id'");
    $name = $data['service_provider_name'];
    $q=$d->update('local_service_provider_users',$a1,"service_provider_users_id='$id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Service Provider Activated ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }
  
 

  if($_POST['status']=="emptyActive") {
    

    $m->set_data('empty_allow_visitor',1);

    $a = array('empty_allow_visitor' => $m->get_data('empty_allow_visitor'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Allow Visitor in Empty Unit Activated");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="emptyDeactive") {
    

    $m->set_data('empty_allow_visitor',0);

    $a = array('empty_allow_visitor' => $m->get_data('empty_allow_visitor'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Allow Visitor in Empty Unit Deactivated");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="walletDeactive") {
    

    $m->set_data('virtual_wallet', 1);

    $a = array('virtual_wallet' => $m->get_data('virtual_wallet'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","App Wallet Deactivated");
      echo 1;
    } else {
      echo 0;
    }
  }


   if($_POST['status']=="walletActive") {
    

    $m->set_data('virtual_wallet', 0);

    $a = array('virtual_wallet' => $m->get_data('virtual_wallet'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","App Wallet Activated");
      echo 1;
    } else {
      echo 0;
    }
  }

  

 if($_POST['status']=="parkingtabActive") {
    

    $m->set_data('hide_member_parking', 0);

    $a = array('hide_member_parking' => $m->get_data('hide_member_parking'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Parking Activated ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="parkingtabDeactive") {
    

    $m->set_data('hide_member_parking', 1);

    $a = array('hide_member_parking' => $m->get_data('hide_member_parking'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Parking Deactivated ($id)");

      echo 1;
    } else {
      echo 0;
    }
  }


 


 if($_POST['status']=="otpVerifyActive") {
    

    $m->set_data('visitor_otp_verify', 0);

    $a = array('visitor_otp_verify' => $m->get_data('visitor_otp_verify'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor OTP Setting Changed ($id)");

      echo 1;
    } else {
      echo 0;
    }
  }

if($_POST['status']=="otpVerifyDeactive") {
    

    $m->set_data('visitor_otp_verify', 1);

    $a = array('visitor_otp_verify' => $m->get_data('visitor_otp_verify'));


    $q = $d->update("society_master", $a, "society_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor OTP Setting Changed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }


if($_POST['status']=="comercialDeactive") {
    

    $m->set_data('active_status', 1);

    $a = array('active_status' => $m->get_data('active_status'));


    $q = $d->update("users_entry_login", $a, "user_entry_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Commercial User Status Changed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }



 if($_POST['status']=="comercialActive") {
    

    $m->set_data('active_status', 0);

    $a = array('active_status' => $m->get_data('active_status'));


    $q = $d->update("users_entry_login", $a, "user_entry_id ='$id'");

    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Commercial User Status Changed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }


  



  
  //IS_3238
  if($_POST['status']=="UserStatusActive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('active_status'=> $m->get_data('active_status')
  );

    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee ($id) App Access Activated");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="UserStatusDeactive") {
    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('active_status'=> $m->get_data('active_status')
  );
      
    $q=$d->update('users_master',$a1,"  user_id='$id'");
    if($q>0) {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee ($id) App Access Deactivated");
      echo 1;
    } else {
      echo 0;
    }
  }
  //IS_3238

  




   if($_POST['status']=="sosDeactive") {
    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('sos_alert'=> $m->get_data('active_status')
  );
    $q=$d->update('bms_admin_master',$a1,"  society_id='$society_id' AND admin_id='$_COOKIE[bms_admin_id]'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "SOS Type Status Changed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="sosActive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('sos_alert'=> $m->get_data('active_status')
  );
    $q=$d->update('bms_admin_master',$a1,"  society_id='$society_id' AND admin_id='$_COOKIE[bms_admin_id]'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "SOS Type Status Changed ($id)");
      echo 1;
    } else {
      echo 0;
    }
  }




//14july2020
  if($_POST['status']=="timelineActive") {
    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('timeline_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Timeline View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="timelineDeactive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('timeline_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Timeline View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }


  if($_POST['status']=="chatActive") {
    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('chat_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Chat View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="chatDeactive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('chat_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Chat View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="activityActive") {
    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('myactivity_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "My Activity View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="activityDeactive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('myactivity_aceess_for_society'=> $m->get_data('active_status')
  );
    $q=$d->update('society_master',$a1,"  society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "My Activity View Setting Changed ");
      echo 1;
    } else {
      echo 0;
    }
  }
//14july2020

   if($_POST['status']=="discussionDeactive") {

    

    $isActive=1;
    $m->set_data('active_status',$isActive);
    $a1= array ('active_status'=> $m->get_data('active_status')
  );
    $q=$d->update('discussion_forum_master',$a1,"  discussion_forum_id='$id'");
    if($q>0) {
      echo 1;

      $dq=$d->select("discussion_forum_master","discussion_forum_id='$id'");
      $data=mysqli_fetch_array($dq);
      extract($data);
                    // /IS_874
      $append_query ="";
       if ($discussion_forum_for==0) {
        $append_query=" AND delete_status=0";
      } else  {
        $append_query=" and floor_id =$discussion_forum_for  AND delete_status=0";
      }
   


      $title= "Discussion Forum Deactivate";
      $description= "By Admin $created_by";

      $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $append_query ");
      $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $append_query ");

      $nResident->noti("DiscussionFragment","",$society_id,$fcmArray,$title,$description,$id);
      $nResident->noti_ios("DiscussionVC","",$society_id,$fcmArrayIos,$title,$description,$id);
       $_SESSION['msg']="Status Changed";

       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Discussion Status Deactivated ");
    } else {
      echo 0;
    }
  }

   if($_POST['status']=="discussionActive") {
    $isActive=0;
    $m->set_data('active_status',$isActive);
    $a1= array ('active_status'=> $m->get_data('active_status')
  );
    $q=$d->update('discussion_forum_master',$a1,"  discussion_forum_id='$id'");
    if($q>0) {
      echo 1;

       $dq=$d->select("discussion_forum_master","discussion_forum_id='$id'");
      $data=mysqli_fetch_array($dq);
      extract($data);
                    // /IS_874
      $append_query ="";
         if ($discussion_forum_for==0) {
        $append_query="  AND delete_status=0";
      } else  {
        $append_query=" AND floor_id =$discussion_forum_for  AND delete_status=0";
      }
   

      $title= "Discussion Forum Activated";
      $description= "By Admin $created_by";

      $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $append_query ");
      $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $append_query ");

      $nResident->noti("DiscussionFragment","",$society_id,$fcmArray,$title,$description,$id);
      $nResident->noti_ios("DiscussionVC","",$society_id,$fcmArrayIos,$title,$description,$id);
      $_SESSION['msg']="Status Changed";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Discussion Status Activated ");
    } else {
      echo 0;
    }
  }

//IS_298
   



  //IS_1291
  if($_POST['status']=="eventDeactive") {
    $isActive=0;
    $m->set_data('isActive',$isActive);
    $a1= array ('event_status'=> $m->get_data('isActive')
  );
    $q=$d->update('event_master',$a1,"event_id='$id' AND society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Event Status Deactivated ");
      echo 1;
    } else {
      echo 0;
    }
  }

  if($_POST['status']=="eventActive") {
    $isActive=1;
    $m->set_data('isActive',$isActive);
    $a1= array ('event_status'=> $m->get_data('isActive')
  );

    $q=$d->update('event_master',$a1,"event_id='$id' AND society_id='$society_id'");
    if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Event Status Activated ");

      echo 1;
    } else {
      echo 0;
    }
  }



//24march2020 IS_1581
if($_POST['status']=="TermConditionActive") {
  $isActive=0;
  $m->set_data('isActive',$isActive);
  $a1= array ('status'=> $m->get_data('isActive')
);
  $q=$d->update('invoice_term_conditions_master',$a1,"invoice_term_condition_id='$id' AND society_id='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Invoice Terms Updated");

    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="TermConditionDeactive") {
  $isActive=1;
  $m->set_data('isActive',$isActive);
  $a1= array ('status'=> $m->get_data('isActive')
);

  $q=$d->update('invoice_term_conditions_master',$a1,"invoice_term_condition_id='$id' AND society_id='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Invoice Terms Updated");
    echo 1;
  } else {
    echo 0;
  }
}
//24march2020 IS_1581


if($_POST['status']=="phoneUnlock") {
  $isActive=1;
  $m->set_data('isActive',$isActive);
  $a1= array ('device_lock'=> $m->get_data('isActive')
);
  $q=$d->update('employee_master',$a1,"emp_id='$id' AND society_id='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Gatekeeper Phone Unloked");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="phoneLock") {
  $isActive=0 ;
  $m->set_data('isActive',$isActive);
  $a1= array ('device_lock'=> $m->get_data('isActive')
);

  $q=$d->update('employee_master',$a1,"emp_id='$id' AND society_id='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Gatekeeper Phone Locked");

    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="eventClose") {

  $eventSelect = $d->selectArray("event_days_master","events_day_id='$id' AND society_id='$society_id'");
  $eventName = $eventSelect['event_day_name'];
  $isActive=1;
  $m->set_data('isActive',$isActive);
  $a1= array ('active_status'=> $m->get_data('isActive')
);
  $q=$d->update('event_days_master',$a1,"events_day_id='$id' AND society_id='$society_id'");
  if($q>0) {

   echo 1;

   $title = "Booking Closed For Event ".$eventSelect['event_day_name'];
   $description = "By Admin ".$_COOKIE['admin_name'];
   $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");

        // echo "<pre>";print_r($fcmArray);exit;
   $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
         //IS_769  ".$_COOKIE['admin_name']
   $nResident->noti("EventDetailsFragment","",$society_id,$fcmArray,$title,$description,'events');
   $nResident->noti_ios("EventsVC","",$society_id,$fcmArrayIos,$title,$description,'events');

   $d->insertUserNotification($society_id,$title,$description,"events","Events_1xxxhdpi.png","");

   $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Event Booking Closed ($id)");



 } else {
  echo 0;
}
}

if($_POST['status']=="eventOpen") {
  $eventSelect = $d->selectArray("event_days_master","events_day_id='$id' AND society_id='$society_id'");
  $eventName = $eventSelect['event_day_name'];

  $isActive=0;
  $m->set_data('isActive',$isActive);
  $a1= array ('active_status'=> $m->get_data('isActive')
);

  $q=$d->update('event_days_master',$a1,"events_day_id='$id' AND society_id='$society_id'");
  if($q>0) {
    echo 1;

     $title = "Event Booking Open For Event ".$eventSelect['event_day_name'];
    $description = "By Admin ".$_COOKIE['admin_name'];

   $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");

        // echo "<pre>";print_r($fcmArray);exit;
   $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
         //IS_769  ".$_COOKIE['admin_name']
   $nResident->noti("EventDetailsFragment","",$society_id,$fcmArray,$title,$description,'events');
   $nResident->noti_ios("EventsVC","",$society_id,$fcmArrayIos,$title,$description,'events');

    $d->insertUserNotification($society_id,$title,$description,"events","Events_1xxxhdpi.png","");

    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Event Booking Open ($id)");

  } else {
    echo 0;
  }
}



if($_POST['status']=="pollActive") {


    //IS_855
 $voting_master_qry=$d->select("voting_master","voting_id='$id'");
 $i = 0;
 while($voting_master_data=mysqli_fetch_array($voting_master_qry)) {
  extract($voting_master_data);
                  // /IS_874
  $pulling_start_date = $voting_master_data['voting_start_date'];
  $society_id =$voting_master_data['society_id'];

  

    $qry ="";
    if ($poll_for==0) {
        $append_query ="";
      } else {
        $qry =" and floor_id=$poll_for ";
        $append_query ="users_master.floor_id=$poll_for ";
      }


    $title= "Poll Open";
    $description= "Poll start date is : $pulling_start_date";
    $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);


    
    $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");


    $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");

    $nResident->noti("PollFragment","",$society_id,$fcmArray,"Poll Open","Poll start date is : $pulling_start_date",'vottingactivity');
    $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"Poll Open","Poll start date is : $pulling_start_date",'vottingactivity');
  }
  //IS_855


  $isActive=0;
  $m->set_data('isActive',$isActive);
  $a1= array ('voting_status'=> $m->get_data('isActive')
  );

  $q=$d->update('voting_master',$a1,"voting_id='$id' ");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Poll Open ($id)");

    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="pollClose") {

//IS_855
 $voting_master_qry=$d->select("voting_master","voting_id='$id'");
 $i = 0;
 while($voting_master_data=mysqli_fetch_array($voting_master_qry)) {
  extract($voting_master_data);
                  //IS_874
  $pulling_start_date = $voting_master_data['voting_start_date'];
  $society_id =$voting_master_data['society_id'];


    $qry ="";
    if ($poll_for==0) {
        $append_query ="";
      } else {
        $qry =" and floor_id=$poll_for ";
        $append_query ="users_master.floor_id=$poll_for ";
      }


    $title= "Poll Closed";
    $description= "By Admin $created_by";
    $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);

    

    
    $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");
    $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");

    $nResident->noti("PollFragment","",$society_id,$fcmArray,"Poll Closed","By Admin $created_by",'vottingactivity');
    $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"Poll Closed","By Admin $created_by",'vottingactivity');
  }
  //IS_855


  $isActive=1;
  $m->set_data('isActive',$isActive);
  $a1= array ('voting_status'=> $m->get_data('isActive')
  );

  $q=$d->update('voting_master',$a1,"voting_id='$id' ");
  if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Poll Closed ($id)");

    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="appMenuDeactive") {
   $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/app_menu_controller.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "changeMenuStatus=changeMenuStatus&menu_status=1&app_menu_society_id=$id&society_id=203");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);


  if($server_output['status']=='200') {
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="appMenuActive") {
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/app_menu_controller.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "changeMenuStatus=changeMenuStatus&menu_status=0&app_menu_society_id=$id&society_id=203");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);


  if($server_output['status']=='200') {
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="GroupChatDeactive") {
  $group_chat_status=1;
  $m->set_data('group_chat_status',$group_chat_status);
  $a1= array ('group_chat_status'=> $m->get_data('group_chat_status')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Group Chat View Setting Changed ($id)");

    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="GroupChatActive") {
  $group_chat_status=0;
  $m->set_data('group_chat_status',$group_chat_status);
  $a1= array ('group_chat_status'=> $m->get_data('group_chat_status')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Group Chat View Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="VisitorApprovalDeactive") {
  $visitor_on_off=1;
  $m->set_data('visitor_on_off',$visitor_on_off);
  $a1= array ('visitor_on_off'=> $m->get_data('visitor_on_off')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor Approval Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="VisitorApprovalActive") {
  $visitor_on_off=0;
  $m->set_data('visitor_on_off',$visitor_on_off);
  $a1= array ('visitor_on_off'=> $m->get_data('visitor_on_off')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor Approval Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="residentinoutActive") {
  $resident_in_out=0;
  $m->set_data('resident_in_out',$resident_in_out);
  $a1= array ('resident_in_out'=> $m->get_data('resident_in_out')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee In/Out Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="residentinoutDeactive") {
  $resident_in_out=1;
  $m->set_data('resident_in_out',$resident_in_out);
  $a1= array ('resident_in_out'=> $m->get_data('resident_in_out')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee In/Out Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}


if($_POST['status']=="visitorMobileActive") {
  $visitor_mobile_number_show_gatekeeper=0;
  $m->set_data('visitor_mobile_number_show_gatekeeper',$visitor_mobile_number_show_gatekeeper);
  $a1= array ('visitor_mobile_number_show_gatekeeper'=> $m->get_data('visitor_mobile_number_show_gatekeeper')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor Mobile Number VIew Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="visitorMobiletDeactive") {
  $visitor_mobile_number_show_gatekeeper=1;
  $m->set_data('visitor_mobile_number_show_gatekeeper',$visitor_mobile_number_show_gatekeeper);
  $a1= array ('visitor_mobile_number_show_gatekeeper'=> $m->get_data('visitor_mobile_number_show_gatekeeper')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Visitor Mobile Number VIew Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}



//IS_321
if($_POST['status']=="CreateGroupActive") {
  $create_group=0;
  $m->set_data('create_group',$create_group);
  $a1= array ('create_group'=> $m->get_data('create_group')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Create Group From App Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="CreateGroupDeactive") {
  $create_group=1;
  $m->set_data('create_group',$create_group);
  $a1= array ('create_group'=> $m->get_data('create_group')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
     $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Create Group From App Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}



if($_POST['status']=="screensortActive") {
  $screen_sort_capture_in_timeline=0;
  $m->set_data('screen_sort_capture_in_timeline',$screen_sort_capture_in_timeline);
  $a1= array ('screen_sort_capture_in_timeline'=> $m->get_data('screen_sort_capture_in_timeline')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
     $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Screen Sort On Timeline Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="screensortDeactive") {
  $screen_sort_capture_in_timeline=1;
  $m->set_data('screen_sort_capture_in_timeline',$screen_sort_capture_in_timeline);
  $a1= array ('screen_sort_capture_in_timeline'=> $m->get_data('screen_sort_capture_in_timeline')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Screen Sort On Timeline Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}


if($_POST['status']=="registrationRequestDeactive") {
  $registration_request_from_app=1;
  $m->set_data('registration_request_from_app',$registration_request_from_app);
  $a1= array ('registration_request_from_app'=> $m->get_data('registration_request_from_app')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Registration From App Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="registrationRequestActive") {
  $registration_request_from_app=0;
  $m->set_data('registration_request_from_app',$registration_request_from_app);
  $a1= array ('registration_request_from_app'=> $m->get_data('registration_request_from_app')
);

  $q=$d->update('society_master',$a1,"society_id ='$society_id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Registration From App Setting Changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}



//IS_1546
if($_POST['status']=="GSTDeactive") {
  $status=1;
  $m->set_data('status',$status);
  $a1= array ('status'=> $m->get_data('status')
);

  $q=$d->update('gst_master',$a1,"slab_id ='$id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "GST slab status changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="GSTActive") {
  $status=0;
  $m->set_data('status',$status);
  $a1= array ('status'=> $m->get_data('status')
);

  $q=$d->update('gst_master',$a1,"slab_id ='$id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "GST slab status changed ($id)");
    echo 1;
  } else {
    echo 0;
  }
}
//IS_1546



  



//28march
if($_POST['status']=="PaymentGatDeactive") {

  $status=1;
  $m->set_data('status',$status);
  $a1= array ('status'=> $m->get_data('status')
);

  $q=$d->update('society_payment_getway',$a1,"society_payment_getway_id ='$id'");
 
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Payment Gateway Deactivated ($id)");
    echo 1;
  } else {
    echo 0;
  }
}

if($_POST['status']=="PaymentGatActive") {
  $status=0;
  $m->set_data('status',$status);
  $a1= array ('status'=> $m->get_data('status')
);

  $q=$d->update('society_payment_getway',$a1,"society_payment_getway_id ='$id'");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Payment Gateway Activated ($id)");
    echo 1;
  } else {
    echo 0;
  }
}
//28march
if($_POST['status']=="SurveyActive") {
 
   $survey_master_qry=$d->select("survey_master","survey_id='$id'");
   $i = 0;
   while($survey_master_data=mysqli_fetch_array($survey_master_qry)) {
    extract($survey_master_data);
    
    $survey_date =  date("d-m-Y", strtotime($survey_master_data['survey_date']));
    $society_id =$survey_master_data['society_id'];
    $qry ="";
    $append_query ="";
    if ($survey_for==0) {
      $for="All Employees";
    } else {
      $for="Primary Member";
      $qry =" and floor_id=$survey_for ";
      $append_query =" users_master.floor_id=$survey_for";
    }
   
    $title= "Survey Open ";
    $description= "Survey start date is : $survey_date";
    $d->insertUserNotification($society_id,$title,$description,"viewSurvey","Survey_1xxxhdpi.png",$append_query);


    $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");
    $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
    $nResident->noti("SurveyFragment","",$society_id,$fcmArray,"Survey Open","Survey start date is : $survey_date",'SurveyFragment');
    $nResident->noti_ios("SurveyVC","",$society_id,$fcmArrayIos,"Survey Open","Survey start date is : $survey_date",'SurveyVC');
  }

  $isActive=0;
  $m->set_data('isActive',$isActive);
  $a1= array ('survey_status'=> $m->get_data('isActive')
  );

  $q=$d->update('survey_master',$a1,"survey_id='$id' ");
  if($q>0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Survey Open ($id)");
    echo 1;
  } else {
    echo 0;
  }
}
if($_POST['status']=="SurveyClose") {
 
   $survey_master_qry=$d->select("survey_master","survey_id='$id'");
   $i = 0;
   while($survey_master_data=mysqli_fetch_array($survey_master_qry)) {
    extract($survey_master_data);
    
    
    $survey_date =  date("d-m-Y", strtotime($survey_master_data['survey_date']));
    $society_id =$survey_master_data['society_id'];
    $qry ="";
    $append_query ="";
    if ($survey_for==0) {
      $for="All Employees";
    } else {
      $qry =" and floor_id=$survey_for ";
      $append_query =" users_master.floor_id=$survey_for";
    }
   
    $title= "Survey Closed ";
    $description= "By Admin $created_by";
    $d->insertUserNotification($society_id,$title,$description,"viewSurvey","Survey_1xxxhdpi.png",$append_query);


    $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' $qry ");
    $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
    $nResident->noti("SurveyFragment","",$society_id,$fcmArray,"Survey Closed","By Admin $created_by",'SurveyFragment');
    $nResident->noti_ios("SurveyVC","",$society_id,$fcmArrayIos,"Survey Closed","By Admin $created_by",'SurveyVC');
  }

  $isActive=1;
  $m->set_data('isActive',$isActive);
  $a1= array ('survey_status'=> $m->get_data('isActive')
  );
  $q=$d->update('survey_master',$a1,"survey_id='$id' ");
  if($q>0) {
     $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Survey Close ($id)");
    echo 1;
  } else {
    echo 0;
  }
}
if ($_POST['action'] == "bulkSalaryStatusChanegs") {
  
  if ($status != '' && $salary_id !="") {
      $a = array();
      if($status==0){
          $a['checked_by'] = $bms_id;
          $a['salary_slip_status'] =1;
      }else if($status==1){
          $a['authorised_by'] = $bms_id;
          $a['salary_slip_status'] =2;
          if($shareWtUser==1){
            $a['share_with_user'] =1;
          }

      }
      else
      {
          $a['share_with_user'] =1;
      }
      if(!empty($a)){
          $q = $d->update("salary_slip_master", $a, "salary_slip_id ='$salary_id'");
          if ($q) {
              if($status==2 || $shareWtUser==1){
                  $q3 = $d->selectRow('*',"salary_slip_master","salary_slip_id='$salary_id'");
                  $data3 = mysqli_fetch_assoc($q3);
                  $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data3[user_id]'");
                  $data2 = mysqli_fetch_assoc($q2);
                  $title = "Salary Slip Share With User ";
                  $description = "By Admin $created_by";
                  $menuClick = "";
                  $image = "";
                  $activity = "Salary_Slip";
                  $user_token = $data2['user_token'];
                  $device = $data2['device'];
                  if ($device=='android') {
                    
                    $v =   $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                  } else {
                      $v =  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                  }
                      
              }

              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Slip Status ($id)");
              $response["message"] = "Success";
              $response["status"] = "200";
              echo json_encode($response);
          } else {
              $response["message"] = "Failed";
              $response["status"] = "201";
              echo json_encode($response);
          }
      }else {
          $response["message"] = "All Required";
          $response["status"] = "201";
          echo json_encode($response);
      }

  } else {
      $response["message"] = "All Required";
      $response["status"] = "201";
      echo json_encode($response);
  }
}
  
  /* PMS Module 07/07/2022 */

if ($_POST['status'] == "dimensionalStatusActive") {
  $a1 = array('dimensional_status' => 0);
  $q = $d->update('dimensional_master', $a1, "dimensional_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PMS Dimensional Activated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}
if ($_POST['status'] == "dimensionalStatusDeactive") {
    $a1 = array('dimensional_status' => 1);
    $q = $d->update('dimensional_master', $a1, "dimensional_id='$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PMS Dimensional Deactivated ($id)");

        echo 1;
    } else {
        echo 0;
    }
}

if ($_POST['status'] == "attributeStatusActive") {
  $a1 = array('attribute_status' => 0);
  $q = $d->update('attribute_master', $a1, "attribute_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PMS Attribute Activated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}
if ($_POST['status'] == "attributeStatusDeactive") {
    $a1 = array('attribute_status' => 1);
    $q = $d->update('attribute_master', $a1, "attribute_id='$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PMS Attribute Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
}

/* DAR Module 11/07/2022 */

if ($_POST['status'] == "templateStatusActive") {
  $a1 = array('template_status' => 0);
  $q = $d->update('template_master', $a1, "template_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work Report Template Deactivated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}
if ($_POST['status'] == "templateStatusDeactive") {
    $a1 = array('template_status' => 1);
    $q = $d->update('template_master', $a1, "template_id='$id'");
    if ($q > 0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work Report Template Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
}

if ($_POST['status'] == "templateQuestionStatusActive") {
  $a1 = array('template_question_status' => 0);
  $q = $d->update('template_question_master', $a1, "template_question_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work Report Template Question Activated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}
if ($_POST['status'] == "templateQuestionStatusDeactive") {
    $a1 = array('template_question_status' => 1);
    $q = $d->update('template_question_master', $a1, "template_question_id='$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work Report Template Question Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
}

/*2022 July 29*/
/////////////leave type status update////////////////////

if ($_POST['status'] == "idProofStatusDeactive") {
  $a1 = array('id_proof_status' => 1);
  $q = $d->update('id_proof_master', $a1, "id_proof_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Id Proof Status Deactivated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}
if ($_POST['status'] == "idProofStatusActive") {
  $a1 = array('id_proof_status' => 0);
  $q = $d->update('id_proof_master', $a1, "id_proof_id='$id'");
  if ($q > 0) {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Id Proof Status Activated ($id)");
      echo 1;
  } else {
      echo 0;
  }
}

if ($_POST['status'] == "holidayGroupStatusActive")
{
  $a1 = array('holiday_group_active_status' => 0);
  $q = $d->update('holiday_group_master', $a1, "holiday_group_id = '$id'");
  if ($q > 0)
  {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Group Status Activated ($id)");
      echo 1;
  }
  else
  {
      echo 0;
  }
}
if ($_POST['status'] == "holidayGroupStatusDeactive")
{
  $a1 = array('holiday_group_active_status' => 1);
  $q = $d->update('holiday_group_master', $a1, "holiday_group_id = '$id'");
  if ($q > 0)
  {
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Group Status Deactivated ($id)");
      echo 1;
  }
  else
  {
      echo 0;
  }
}
  
  if ($_POST['status'] == "areaActive") {
    $a1 = array('area_flag' => 1);
    $q = $d->update('area_master_new', $a1, "area_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Area Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "areaDeactive") {
      $a1 = array('area_flag' => 2);
      $q = $d->update('area_master_new', $a1, "area_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Area Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "routeActive") {
    $a1 = array('route_active_status' => 0);
    $q = $d->update('route_master', $a1, "route_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Route Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "routeDeactive") {
      $a1 = array('route_active_status' => 1);
      $q = $d->update('route_master', $a1, "route_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Route Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "categoryActive") {
    $a1 = array('product_category_status' => 0);
    $q = $d->update('product_category_master', $a1, "product_category_id = '$id'");
    if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "categoryDeactive") {
      $a1 = array('product_category_status' => 1);
      $q = $d->update('product_category_master', $a1, "product_category_id = '$id'");
      if ($q > 0) {
         $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "subCategoryActive") {
    $a1 = array('product_sub_category_status' => 0);
    $q = $d->update('product_sub_category_master', $a1, "product_sub_category_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Sub Category Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "subCategoryDeactive") {
      $a1 = array('product_sub_category_status' => 1);
      $q = $d->update('product_sub_category_master', $a1, "product_sub_category_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Sub Category Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "retailerActive") {
    $a1 = array('retailer_status' => 1);
    $q = $d->update('retailer_master', $a1, "retailer_id = '$id'");
    if ($q) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "retailerDeactive") {
      $a1 = array('retailer_status' => 2);
      $q = $d->update('retailer_master', $a1, "retailer_id = '$id'");
      if ($q) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer Deactivated ($id)");

          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "distributorActive") {
    $a1 = array('distributor_status' => 1);
    $q = $d->update('distributor_master', $a1, "distributor_id = '$id'");
    if ($q) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Distributor Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "distributorDeactive") {
      $a1 = array('distributor_status' => 2);
      $q = $d->update('distributor_master', $a1, "distributor_id = '$id'");
      if ($q) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Distributor Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }


  if ($_POST['status'] == "variantActive") {
    $a1 = array('variant_active_status' => 0);
    $q = $d->update('retailer_product_variant_master', $a1, "product_variant_id = '$id'");
    if ($q > 0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "variantDeactive") {
      $a1 = array('variant_active_status' => 1);
      $q = $d->update('retailer_product_variant_master', $a1, "product_variant_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "letterActive") {
    $a1 = array('active_status' => 0);
    $q = $d->update('letter_master', $a1, "letter_id = '$id'");
    if ($q > 0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Letter Template Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "letterDeactive") {
      $a1 = array('active_status' => 1);
      $q = $d->update('letter_master', $a1, "letter_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Letter Template Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  /* 30 Nov 2022 SHUBHAM*/
  if ($_POST['status'] == "profileMenuStatusActive") {
    $a1 = array('profile_menu_status' => 0);
    $q = $d->update('employee_profile_menu_access', $a1, "profile_menu_id = '$id'");
    if ($q > 0) {
       $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Profile Menu Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "profileMenuStatusDeactive") {
      $a1 = array('profile_menu_status' => 1);
      $q = $d->update('employee_profile_menu_access', $a1, "profile_menu_id = '$id'");
      if ($q > 0) {
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Profile Menu Activated Deactivated ($id)");
          echo 1;
      } else {
          echo 0;
      }
  }

  if ($_POST['status'] == "leaveGroupDeactive") {
    $a1 = array('leave_group_active_status' => 1);
    $q = $d->update('leave_group_master', $a1, "leave_group_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Group Activated Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "leaveGroupActive") {
    $a1 = array('leave_group_active_status' => 0);
    $q = $d->update('leave_group_master', $a1, "leave_group_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Group Activated Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }

  if ($_POST['status'] == "sisterCompanyDeactive") {
    $a1 = array('status' => 2);
    $q = $d->update('sister_company_master', $a1, "sister_company_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id",$_COOKIE['bms_admin_id'],"$created_by","Sister Company Deactivated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }
  if ($_POST['status'] == "sisterCompanyActive") {
    $a1 = array('status' => 1);
    $q = $d->update('sister_company_master', $a1,"sister_company_id = '$id'");
    if ($q > 0) {
      $d->insert_log("", "$society_id",$_COOKIE['bms_admin_id'],"$created_by","Sister Company Activated ($id)");
        echo 1;
    } else {
        echo 0;
    }
  }

}
else{
  header('location:../logout');
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}
function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "00:00";
    }
}
?>
