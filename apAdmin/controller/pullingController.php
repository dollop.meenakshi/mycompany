<?php
include '../common/objectController.php';
extract($_POST);
 

$election_for_string= $xml->string->election_for; 
$electionArray = explode("~", $election_for_string);

if (isset($_POST) && !empty($_POST)) {
 // print_r($_POST);
 
  //IS_1328
  if (isset($_POST['addPullingQue'])) {
  //IS_1310
   //echo "<pre>";print_r($_FILES);die;
    $values = array_count_values($_POST['option_name']);

    for ($i=0; $i < count($values) ; $i++) {
      $detail = $values[$_POST['option_name'][$i]];
      
      if($detail >1){
       $_SESSION['msg1']="Please provide unique option name for polling options to avoid confusion";
       header('Location: ' . $_SERVER['HTTP_REFERER']);
       exit;
     }
   }

        //IS_1310
   $option_name = implode("~", $_POST['option_name']);
   $option_name1 = explode('~', (string)$option_name);
   if ($_POST['option_name'] == '' || count($option_name1)==0) {
     $_SESSION['msg1']="Please provide poll option";
       header('Location: ' . $_SERVER['HTTP_REFERER']);
       exit;

   }
   
   $file = $_FILES['voting_attachment']['tmp_name'];
   if(file_exists($file)) {
     $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG","csv","ods","mp4","MP4");
     $extId = pathinfo($_FILES['voting_attachment']['name'], PATHINFO_EXTENSION);

     $errors     = array();
     $maxsize    = 12097152;
       
       if(($_FILES['voting_attachment']['size'] >= $maxsize) || ($_FILES["voting_attachment"]["size"] == 0)) {
           $_SESSION['msg1']="Attachment too large. Must be less than 12 MB.";
           header("location:../addPolling");
           exit();
       }
       if(!in_array($extId, $extensionResume) && (!empty($_FILES["voting_attachment"]["type"]))) {
            $_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
           header("location:../addPolling");
           exit();
       }
      if(count($errors) === 0) {
       $image_Arr = $_FILES['voting_attachment'];   
       $temp = explode(".", $_FILES["voting_attachment"]["name"]);
       $voting_attachment = 'voting_'.round(microtime(true)) . '.' . end($temp);
       move_uploaded_file($_FILES["voting_attachment"]["tmp_name"], "../../img/voting_doc/".$voting_attachment);
       $voting_attachment_url = $base_url.'img/voting_doc/'.$voting_attachment;
     } 
   } else{
     $voting_attachment="";
   }
  
  $m->set_data('society_id',$society_id);
  $m->set_data('voting_question',test_input($pulling_question));
  $m->set_data('voting_description',$pulling_description);
  $m->set_data('voting_start_date',$polling_start_time);
  $m->set_data('polling_end_time',$polling_end_time);
  $m->set_data('poll_for',$poll_for);
  $m->set_data('voting_attachment',test_input($voting_attachment));
  $a = array(
    'society_id'=>$m->get_data('society_id'),
    'voting_question'=>$m->get_data('voting_question'),
    'voting_description'=>$m->get_data('voting_description'),
    'voting_start_date'=>$m->get_data('voting_start_date'),
    'voting_end_date'=>$m->get_data('polling_end_time'),
    'poll_for'=>$m->get_data('poll_for'),
    'voting_attachment'=>$m->get_data('voting_attachment'),
  );
  $q1=$d->insert("voting_master",$a);
  $voting_id = $con->insert_id;


  for ($i=0; $i <$no_of_option ; $i++) {
    
    $m->set_data('voting_id',$voting_id);
    $m->set_data('society_id',$society_id);
    $m->set_data('option_name',$option_name1[$i]);
    $a = array(
      'voting_id'=>$m->get_data('voting_id'),
      'society_id'=>$m->get_data('society_id'),
      'option_name'=>$m->get_data('option_name'),
    );
    $q=$d->insert("voting_option_master",$a);
  }

  if($q > 0 && $q1 > 0) {
              //IS_562
    $qry ="";
    if ($poll_for==0) {
      $append_query ="";
    } else {
      $qry =" and floor_id=$poll_for ";
      $append_query ="users_master.floor_id=$poll_for ";
    }
              //IS_1110
      $pulling_start_date =  $polling_start_time;
    

      $title= "New Poll Added";
      $description= "Poll start date is : $pulling_start_date";
      $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);

              //IS_562

    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND  user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android' $qry ");
    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND  user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");
    $nResident->noti("PollFragment","",$society_id,$fcmArray,"New Poll Added","Poll start date is : $pulling_start_date",'vottingactivity');
    $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"New Poll Added","Poll start date is : $pulling_start_date",'vottingactivity');
    $_SESSION['msg']="Poll Added";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Poll Added");
    header("location:../viewPolling");
  } else {
    $_SESSION['msg1']="Something Wrong";
    header("location:../addPolling");
  }
}
if (isset($_POST['editPullingQue'])) {

 $options = array();
 for ($g=0; $g < count($_POST['option_name']) ; $g++) { 
   $options[] = strtolower($_POST['option_name'][$g]);
 }
 $values = array_count_values($options);

 for ($i=0; $i < count($values) ; $i++) {
  $detail = $values[$options[$i]];

  if($detail >1){
   $_SESSION['msg1']="Please provide unique option name for polling options to avoid confusion";
   header('Location: ../viewPolling');
   exit;
 }
}

$file = $_FILES['voting_attachment']['tmp_name'];
   if(file_exists($file)) {
     $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG","csv","ods","mp4","MP4");
     $extId = pathinfo($_FILES['voting_attachment']['name'], PATHINFO_EXTENSION);

     $errors     = array();
     $maxsize    = 12097152;
       
       if(($_FILES['voting_attachment']['size'] >= $maxsize) || ($_FILES["voting_attachment"]["size"] == 0)) {
           $_SESSION['msg1']="Attachment too large. Must be less than 12 MB.";
           header("location:../addPolling");
           exit();
       }
       if(!in_array($extId, $extensionResume) && (!empty($_FILES["voting_attachment"]["type"]))) {
            $_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
           header("location:../addPolling");
           exit();
       }
      if(count($errors) === 0) {
       $image_Arr = $_FILES['voting_attachment'];   
       $temp = explode(".", $_FILES["voting_attachment"]["name"]);
       $voting_attachment = 'voting_'.round(microtime(true)) . '.' . end($temp);
       move_uploaded_file($_FILES["voting_attachment"]["tmp_name"], "../../img/voting_doc/".$voting_attachment);
       $voting_attachment_url = $base_url.'img/voting_doc/'.$voting_attachment;
     } 
   } else{
     $voting_attachment=$voting_attachment_old;
   }

$m->set_data('society_id',$society_id);
$m->set_data('voting_question',test_input($pulling_question));
$m->set_data('voting_description',$pulling_description);
$m->set_data('voting_start_date',$polling_start_time);
$m->set_data('polling_end_time',$polling_end_time);
$m->set_data('poll_for',$poll_for);
$m->set_data('voting_id',$voting_id);
$m->set_data('voting_attachment',test_input($voting_attachment));
$a = array(

  'voting_question'=>$m->get_data('voting_question'),
  'voting_description'=>$m->get_data('voting_description'),
  'voting_start_date'=>$m->get_data('voting_start_date'),
  'voting_end_date'=>$m->get_data('polling_end_time'),
  'poll_for'=>$m->get_data('poll_for'),
  'voting_attachment'=>$m->get_data('voting_attachment'),
);
     //echo "<pre>";print_r($a);exit;

$q=$d->update("voting_master",$a,"voting_id='$voting_id' AND society_id='$society_id'");
$q2 = $d->delete("voting_option_master","voting_id='$voting_id'");
if($q2){

                   /*  $option_name = implode("~", $_POST['option_name']);
                   $option_name1 = explode('~', (string)$option_name);*/
                   for ($i=0; $i < count($_POST['option_name']) ; $i++) {


                    $m->set_data('society_id',$society_id);
                    $m->set_data('option_name',$_POST['option_name'][$i]);
                    $a = array(
                      'voting_id'=>$m->get_data('voting_id'),
                      'society_id'=>$m->get_data('society_id'),
                      'option_name'=>$m->get_data('option_name'),
                    );

                    $q=$d->insert("voting_option_master",$a);

                  }
                }

                if($q > 0  ) {
                //IS_562
                   $qry ="";
                  if ($poll_for==0) {
                    $append_query ="";
                  } else {
                    $qry =" and floor_id=$poll_for ";
                    $append_query ="users_master.floor_id=$poll_for ";
                  }
                //IS_1110
                  $pulling_start_date =  $polling_start_time;
                

                  $title= "Updated: Poll";
                  $description= "Poll start date is : $pulling_start_date";
                  $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);


                  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android' $qry ");
                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");
                  $nResident->noti("PollFragment","",$society_id,$fcmArray,"Updated: Poll","Poll start date is : $pulling_start_date",'vottingactivity');
                  $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"Updated: Poll","Poll start date is : $pulling_start_date",'vottingactivity');
                  $_SESSION['msg']="Poll Updated";
                  $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Poll Updated");
                  header("location:../viewPolling");
                } else {
                  $_SESSION['msg1']="Something Wrong";
                  header("location:../addPolling");
                }
              }
              if(isset($_POST['votingDone']))
        // exit();
              {
                extract(array_map("test_input" , $_POST));
                if($voting_status==1)
                {
                  $end_date=date("Y/m/d");
                  $m->set_data('voting_status',$voting_status);
                  $m->set_data('voting_end_date',$end_date);
                  $a = array ('voting_status'=> $m->get_data('voting_status'),
                    'voting_end_date'=> $m->get_data('voting_end_date')
                  );
                  $id= $voting_id;
                  $s_id= $society_id;

                  $q=$d->update("voting_master",$a,"voting_id='$id' AND society_id='$s_id'");
                  if($q>0) {

                    
                     $title= "Poll Closed";
                  $description= "By Admin ".$_COOKIE['admin_name'];
                  $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);


                    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'");
                    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios'");
                    $nResident->noti("PollFragment","",$society_id,$fcmArray,"Poll Closed","By Admin  ".$_COOKIE['admin_name'],'vottingactivity');
                    $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"Poll Closed","By Admin  ".$_COOKIE['admin_name'],'vottingactivity');
                    $_SESSION['msg']="Poll is Close";
                    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Poll Closed");
                    header('location:../viewPolling');
                  }
                  else {
                    $_SESSION['msg1']="Something Wrong";
                    header('location:../viewPolling');
                  }
                }
                else if($voting_status==0)
                { 
                  $end_date='';
                  $m->set_data('voting_status',$voting_status);
                  $a2 = array ('voting_status'=> $m->get_data('voting_status'),
                );    
                  $id= $voting_id;
                  $s_id= $society_id;

                  $q2=$d->update("voting_master",$a2,"voting_id='$id' AND society_id='$s_id'");
                  if($q2 > 0) {


                  $title= "Poll is Open";
                  $description= "By Admin ".$_COOKIE['admin_name'];
                  $d->insertUserNotification($society_id,$title,$description,"vottingactivity","Pollsxxxhdpi.png",$append_query);


                    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'");
                    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios'");
                    $nResident->noti("PollFragment","",$society_id,$fcmArray,"Poll Is Open","Vote Now",'vottingactivity');
                    $nResident->noti_ios("PollingVc","",$society_id,$fcmArrayIos,"Poll Is Open","Vote Now",'vottingactivity');
                    $_SESSION['msg']="Poll is Open";
                    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Poll Open");
                    header('location:../viewPolling');
                  }
                  else {
                    $_SESSION['msg1']="Something Wrong";
                    header('location:../viewPolling');
                  }
                }
              }
              if (isset($editElection)) {

                

              $m->set_data('election_name',$election_name);
              $m->set_data('election_description',$election_description);
              $m->set_data('election_date',$election_date);
              $m->set_data('election_for',$election_for);
              $m->set_data('created_by',$_COOKIE['bms_admin_id']);

               
              $a = array(

                'election_name'=>$m->get_data('election_name'),
                'election_description'=>$m->get_data('election_description'),
                'election_date'=>$m->get_data('election_date'),
                'election_for'=>$m->get_data('election_for'),
                'created_by'=>$m->get_data('created_by'),
                  
              );
              $update_election=$d->update("election_master",$a,"election_id='$election_id'");

             
               //IS_108
              $m->set_data('is_nota',$is_nota);
              $m->set_data('election_id',$election_id);
              $m->set_data('user_id',"0");
              $m->set_data('society_id',$society_id);
              $m->set_data('unit_id',"0");
              $m->set_data('election_user_status',"1");
             
              $election_users_array = array(
                'election_id'=>$m->get_data('election_id'),
                'is_nota'=>$m->get_data('is_nota'),
                'user_id'=>$m->get_data('user_id'),
                'society_id'=>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'election_user_status'=>$m->get_data('election_user_status'),
                 
              );
              $election_users_result=$d->update("election_users",$election_users_array,"election_id='$election_id' AND society_id='$society_id' AND user_id='0'");
               //IS_108


              
              if($update_election > 0) {
               // /IS_1049
                //IS_1050
                $qry = "";

                 $qry ="";
                if ($election_for==0) {
                  $append_query ="";
                } else {
                  $qry =" and floor_id=$election_for ";
                  $append_query ="users_master.floor_id=$election_for ";
                }
               
                
                  $title= "Updated: Election For $election_name";
                  $description= "Nominate Now";
                  $d->insertUserNotification($society_id,$title,$description,"election","ElectionNew.png",$append_query);


                $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'  $qry ");
                $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");

                 


                $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"Updated: Election For $election_name","Nominate Now",'election');
                $nResident->noti_ios("ElectionVC" ,"",$society_id,$fcmArrayIos,"Updated: Election For $election_name","Nominate Now",'election');
                $_SESSION['msg']="Election Updated";
                $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$election_name Election Updated");
                header("location:../manageElections");
              } else {
                $_SESSION['msg1']="Something Wrong";
                header("location:../election");
              }
            } 
            if (isset($addElection)) {

             
            $m->set_data('society_id',$society_id);
            $m->set_data('election_name',$election_name);
            $m->set_data('election_description',$election_description);
            $m->set_data('election_date',$election_date);
            $m->set_data('election_for',$election_for);
            $m->set_data('created_by',$_COOKIE['bms_admin_id']);
            
            $a = array(
              'society_id'=>$m->get_data('society_id'),
              'election_name'=>$m->get_data('election_name'),
              'election_description'=>$m->get_data('election_description'),
              'election_date'=>$m->get_data('election_date'),
              'election_for'=>$m->get_data('election_for'),
              'created_by'=>$m->get_data('created_by'),
               
            );
            $q1=$d->insert("election_master",$a);
             $election_id = $con->insert_id;

              $m->set_data('is_nota',$is_nota);
              $m->set_data('user_id',"0");
              $m->set_data('society_id',$society_id);
              $m->set_data('unit_id',"0");
              $m->set_data('election_user_status',"1");
              $election_users_array = array(
                'election_id'=>$election_id,
                'is_nota'=>$m->get_data('is_nota'),
                'user_id'=>$m->get_data('user_id'),
                'society_id'=>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'election_user_status'=>$m->get_data('election_user_status'),
                 
              );
              $election_users_result=$d->insert("election_users",$election_users_array);
               //IS_108
              $last_election_id = $con->insert_id;
              $m->set_data('election_id',$last_election_id);

            if($q1 > 0) {
               // /IS_1049
                //IS_1050
                
                $qry ="";
                if ($election_for==0) {
                  $append_query ="";
                } else {
                  $qry =" and floor_id=$election_for ";
                  $append_query ="users_master.floor_id=$election_for ";
                }
                
                  $title= "New Election For $election_name";
                  $description= "Nominate Now";
                  $d->insertUserNotification($society_id,$title,$description,"election","ElectionNew.png",$append_query);

                 //IS_1050
                 //IS_1050
              $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'  $qry ");
              $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");

                  //IS_1050

              $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"New Election For $election_name","Nominate Now",'election');
              $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"New Election For $election_name","Nominate Now",'election');
              $_SESSION['msg']="Election Added";
              $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$election_name Election Added");
              header("location:../manageElections");
            } else {
              $_SESSION['msg1']="Something Wrong";
              header("location:../election");
            }
          }
          if(isset($_POST['electionStart']))
          // exit();
          {
            extract(array_map("test_input" , $_POST));
            $TotalParti=$d->count_data_direct("election_user_id","election_users","election_id='$election_id' AND society_id='$society_id'");
            if ($TotalParti>1) {
              $TotalPending=$d->count_data_direct("election_user_id","election_users","election_id='$election_id' AND society_id='$society_id' AND election_user_status=0");
              if ($TotalPending>0) {
                $_SESSION['msg1']="Appove Or Reject Participate User Than Start Election";
                header('location:../electionParticipateUsers?election_id='.$election_id);
              } else {
                $m->set_data('election_status',$election_status);
                $a2 = array ('election_status'=> $m->get_data('election_status'),
              );    


                $q2=$d->update("election_master",$a2,"election_id='$election_id' AND society_id='$society_id'");
                if($q2 > 0) {

               $qry ="";
                if ($election_for==0) {
                  $append_query ="";
                } else {
                  $qry =" and floor_id=$poll_for ";
                  $append_query ="users_master.floor_id=$poll_for ";
                }
                
                  $title= "Election $election_name Start";
                  $description= "Vote Now";
                  $d->insertUserNotification($society_id,$title,$description,"election","ElectionNew.png",$append_query);

                  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'  $qry ");
                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");

                 
                  $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"Election $election_name Start","Vote Now",'election');
                  $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"Election $election_name Start","Vote Now",'election');
                  $_SESSION['msg']="Election is Start";
                  $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Election $election_name Start");
                  header('location:../manageElections');
                }
                else {
                  $_SESSION['msg1']="Something Wrong";
                  header('location:../manageElections');
                }
              }
            } else {
              $_SESSION['msg1']="Can not Start Election Need more than 1 Participate";
              header('location:../manageElections');
            }
          } 
          if(isset($_POST['electionEnd']))
          // exit();
          {
            extract(array_map("test_input" , $_POST));
            $m->set_data('election_status',$election_status);
            $a2 = array ('election_status'=> $m->get_data('election_status'),
          );    


            $q2=$d->update("election_master",$a2,"election_id='$election_id' AND society_id='$society_id'");
            if($q2 > 0) {
                 
               $qry ="";
                if ($election_for==0) {
                  $append_query ="";
                } else {
                  $qry =" and floor_id=$poll_for ";
                  $append_query ="users_master.floor_id=$poll_for ";
                }
                
                  $title= "Election $election_name Ended";
                  $description= "Closed By Admin ".$_COOKIE['admin_name'];
                  $d->insertUserNotification($society_id,$title,$description,"election","ElectionNew.png",$append_query);

                  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'  $qry ");
                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");


              
                    // /IS_1048
              $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"Election $election_name Ended","Closed By Admin ".$_COOKIE['admin_name'],'election');
              $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"Election $election_name Ended","Closed By Admin ".$_COOKIE['admin_name'],'election');
              $_SESSION['msg']="Election Ended";
              $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Election $election_name Closed");
              header('location:../manageElections');
            }
            else {
              $_SESSION['msg1']="Something Wrong";
              header('location:../manageElections');
            }
          }
          if(isset($_POST['resultPublish']))
          // exit();
          {
    extract(array_map("test_input" , $_POST));

        $m->set_data('election_status',2);

        $a2 = array ('election_status'=> $m->get_data('election_status'),
        );    
       
        
        $q2=$d->update("election_master",$a2,"election_id='$election_id' AND society_id='$society_id'");

        if($q2 > 0) {
               
               $qry ="";
                if ($election_for==0) {
                  $append_query ="";
                } else {
                  $qry =" and floor_id=$poll_for ";
                  $append_query ="users_master.floor_id=$poll_for ";
                }
                
                  $title= "Election Result Published";
                  $description= "for $election_name By Admin ".$_COOKIE['admin_name'];
                  $d->insertUserNotification($society_id,$title,$description,"election","ElectionNew.png",$append_query);


                  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android'  $qry ");
                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios' $qry ");
           
             

            $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"Election Result Published","for $election_name By Admin  ".$_COOKIE['admin_name'],'election');
            $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"Election Result Published","for $election_name  By Admin  ".$_COOKIE['admin_name'],'election');

            $_SESSION['msg']="Election Result Published";
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Election Result Published");

            header('location:../manageElections');
        }
        else {
            $_SESSION['msg1']="Something Wrong";
            header('location:../manageElections');
        }
}
          if(isset($_POST['nomApporve']))
          // exit();
          {
            
    extract(array_map("test_input" , $_POST));

        $m->set_data('election_user_status',1);

        $a2 = array ('election_user_status'=> $m->get_data('election_user_status'),
        );    
     
       $q2=$d->update("election_users",$a2,"election_id='$election_id' AND society_id='$society_id' AND election_user_id='$election_user_id'");

        if($q2 > 0   ) {
            $_SESSION['msg']="User Approved";

            //IS_555
            /*$qUserToken=$d->select("users_master","society_id='$society_id' AND unit_id='$unit_id'");
            $data_notification=mysqli_fetch_array($qUserToken);
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            if ($device=='android') {
              $nResident->noti("",$society_id,$sos_user_token,"Nomination Approved","By Admin",'Election');
            }  else if($device=='ios') {
              $nResident->noti_ios("",$society_id,$sos_user_token,"Nomination Approved","By Admin",'Election');
            }
            */
  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android' AND user_id='$user_id' ");
          $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'AND user_id='$user_id'  ");

        $device=$data_notification['device'];
            
              $nResident->noti("ElectionFragment","",$society_id,$fcmArray,"Nomination Approved","By Admin ".$_COOKIE['admin_name'],'Election');
             
              $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"Nomination Approved","By Admin ".$_COOKIE['admin_name'],'Election');
            
//IS_555
            $notiAry = array(
            'society_id'=>$society_id,
            'user_id'=>$user_id,
            'notification_title'=>'Nomination Approved',
            'notification_desc'=>"By Admin  ".$_COOKIE['admin_name'],    
            'notification_date'=>date('Y-m-d H:i'),
            'notification_action'=>'Election',
            'notification_logo'=>'ElectionNew.png',
            );
            $d->insert("user_notification",$notiAry);
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Election Nomination Approved ");

            header('location:../electionParticipateUsers?election_id='.$election_id);
        }
        else {
            $_SESSION['msg1']="Something Wrong";
            header('location:../electionParticipateUsers?election_id='.$election_id);
        }

          }
              if(isset($_POST['nomReject']))
          // exit();
              {
                extract(array_map("test_input" , $_POST));

                $m->set_data('election_user_status',2);
                $m->set_data('reject_msg',$reject_msg);
                $a2 = array ('election_user_status'=> $m->get_data('election_user_status'),
                  'reject_msg'=> $m->get_data('reject_msg'),
                );    


                $q2=$d->update("election_users",$a2,"election_id='$election_id' AND society_id='$society_id' AND election_user_id='$election_user_id'");
                if($q2 > 0) {
                  $_SESSION['msg']="User Rejected";
                  //IS_555
                 /* $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                  $data_notification=mysqli_fetch_array($qUserToken);
                  $sos_user_token=$data_notification['user_token'];
                  $device=$data_notification['device'];
                  if ($device=='android') {
                    $nResident->noti("",$society_id,$sos_user_token,"Nomination Rejected by Admin","for $reject_msg",'Election');
                  }  else if($device=='ios') {
                    $nResident->noti_ios("",$society_id,$sos_user_token,"Nomination Rejected by Admin","for $reject_msg",'Election');
                  }*/
                  
                  $fcmArray =$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='android' AND user_id='$user_id' ");

                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND user_status=1 AND society_id='$society_id' AND device='ios'AND user_id='$user_id'  ");
                  $nResident->noti("ElectionFragment",  "",$society_id,$fcmArray,"Nomination Rejected by Admin ".$_COOKIE['admin_name'],"for $reject_msg",'Election');
                  $nResident->noti_ios("ElectionVC","",$society_id,$fcmArrayIos,"Nomination Rejected by Admin ".$_COOKIE['admin_name'],"for $reject_msg",'Election');
                  
                  
      //IS_555


                  $notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$user_id,
                    'notification_title'=>'Nomination Rejected By Admin  '.$_COOKIE['admin_name'],
                    'notification_desc'=>"for $reject_msg",    
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'Election',
                    'notification_logo'=>'ElectionNew.png',
                  );
                  $d->insert("user_notification",$notiAry);
                  $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Election Nomination Rejected ");
                  header('location:../electionParticipateUsers?election_id='.$election_id);
                }
                else {
                  $_SESSION['msg1']="Something Wrong";
                  header('location:../electionParticipateUsers?election_id='.$election_id);
                }
              }
            }
            ?>