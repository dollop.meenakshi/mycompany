<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['assignDimensional'])) {

        if($pms_type == 0){
            $day = $week_day;
        }else{
            $day = $month_day;
        }
        
        $m->set_data('society_id', $society_id);
        $m->set_data('dimensional_id', $dimensional_id);
        $m->set_data('block_ids', implode(",",$_POST['block_ids']));
        $m->set_data('floor_ids', implode(",",$_POST['floor_ids']));
        $m->set_data('level_ids', implode(",",$_POST['level_ids']));
        $m->set_data('users_ids', implode(",",$_POST['users_ids']));
        $m->set_data('pms_type', $pms_type);
        $m->set_data('day', $day);
        $m->set_data('start_month', $start_month);
        $m->set_data('created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'dimensional_id' => $m->get_data('dimensional_id'),
            'block_ids' => $m->get_data('block_ids'),
            'floor_ids' => $m->get_data('floor_ids'),
            'level_ids' => $m->get_data('level_ids'),
            'users_ids' => $m->get_data('users_ids'),
            'pms_type' => $m->get_data('pms_type'),
            'day' => $m->get_data('day'),
            'start_month' => $m->get_data('start_month'),
            'created_by' => $m->get_data('created_by'),
            'created_date' => $m->get_data('created_date'),
        );
        
        
        

       $pdaq = $d->selectRow('performance_dimensional_assign.performance_dimensional_assign_id',"performance_dimensional_assign","performance_dimensional_assign.dimensional_id='$dimensional_id'"); 

        if (mysqli_num_rows($pdaq)>0) {
            $data = mysqli_fetch_array($pdaq);
            $q = $d->update("performance_dimensional_assign", $a1, "performance_dimensional_assign_id ='$data[performance_dimensional_assign_id]'");
            $_SESSION['msg'] = "Assign Dimensional Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Assign Dimensional Updated Successfully");
        } else {
            $q = $d->insert("performance_dimensional_assign", $a1);
            $_SESSION['msg'] = "Assign Dimensional Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Assign Dimensional Successfully");

        }

        $usersIds = $_POST['users_ids'];
        
        if($_POST['users_ids'][0] == 0){

            $levelIds = implode("','",$_POST['level_ids']);
            $blockIds = implode("','",$_POST['block_ids']);
            $floorIds = implode("','",$_POST['floor_ids']);
            if($blockIds != 0){
                $blockAppendQuery = " AND users_master.block_id IN ('$blockIds')";
            }
            if($floorIds != 0){
                $floorAppendQuery = " AND users_master.floor_id IN ('$floorIds')";
            }
            $uq = $d->selectRow("user_id","users_master","users_master.level_id IN ('$levelIds') $blockAppendQuery $floorAppendQuery"); 

            $usersIds = array();

            while($userData = mysqli_fetch_array($uq)){
                $result = $userData['user_id'];
                array_push($usersIds, $result);
            }

        }
        
        $d->delete("pms_schedule_master","dimensional_id='$dimensional_id' AND schedule_status=0");

        $currentDate = date('Y-m-d');
        if($pms_type == 0){
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $day = $day;
            $to_date = date('Y-m-d');
            $schedule_date = $weekly_date = date('Y-m-d', strtotime($days[$day], strtotime($to_date)));
        }
        elseif($pms_type == 1){
            $month = date('F Y');
            $month_days = explode(',',$day);
            $month_day = $day.' '.$month; 
            if(date('Y-m-d', strtotime($month_day)) < $currentDate){
                $month_day = date('Y-m-d', strtotime($month_day. ' + 1 months'));
            } else {
                 $month_day = date('Y-m-d', strtotime($month_day));
            } 
            $schedule_date = $month_day;
        }
        elseif($pms_type == 2){
            $date = date('Y').'-'.$start_month.'-'.$day;
            if($currentDate <= date('Y-m-d', strtotime($date))){
                $monthNum  = $start_month;
                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                $monthName = $dateObj->format('F').' '.date('Y');
                $quarterly_date = $day.' '.$monthName;
            }else{
                  $newDate = date('m', strtotime($date. ' + 3 months'));
                  $monthNum  = $newDate;
                  $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                  $monthName = $dateObj->format('F').' '.date('Y');
                  $quarterly_date = $day.' '.$monthName;
            }

            if(date('Y-m-d', strtotime($quarterly_date)) < $currentDate){
                $quarterly_date = date('Y-m-d', strtotime($quarterly_date. ' + 3 months'));
                while(date('Y-m-d', strtotime($quarterly_date)) < $currentDate){
                  $quarterly_date = date('Y-m-d', strtotime($quarterly_date. ' + 3 months'));
                }
            }
            $schedule_date = $quarterly_date;
        }
        elseif($pms_type == 3){
            $date = date('Y').'-'.$start_month.'-'.$day;
            if($currentDate <= date('Y-m-d', strtotime($date))){
                $monthNum  = $start_month;
                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                $monthName = $dateObj->format('F').' '.date('Y');
                $half_year_date = $day.' '.$monthName;
            }else{
                  $newDate = date('m', strtotime($date. ' + 6 months'));
                  $monthNum  = $newDate;
                  $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                  $monthName = $dateObj->format('F').' '.date('Y');
                  $half_year_date = $day.' '.$monthName;
            }

            if(date('Y-m-d', strtotime($half_year_date)) < $currentDate){
                $half_year_date = date('Y-m-d', strtotime($half_year_date. ' + 6 months'));
                while(date('Y-m-d', strtotime($half_year_date)) < $currentDate){
                  $half_year_date = date('Y-m-d', strtotime($half_year_date. ' + 6 months'));
                }
            }
            $schedule_date = $half_year_date;
        }
        elseif($pms_type == 4){
            $date = date('Y').'-'.$start_month.'-'.$day;
            $monthNum  = $start_month;
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F').' '.date('Y');
            $year_date = $day.' '.$monthName;

            if(date('Y-m-d', strtotime($year_date)) < $currentDate){
                $year_date = date('Y-m-d', strtotime($year_date. ' + 1 year'));
            }
            $schedule_date = $year_date;
        }


        for($i=0; $i<count($usersIds); $i++){
             $schedule_date = date('Y-m-d', strtotime($schedule_date));
             $user_id = $usersIds[$i];
             $uq = $d->selectRow("level_id","users_master","user_id='$user_id'"); 
             $levelData=mysqli_fetch_array($uq);
             $level_id = $levelData['level_id'];

            $m->set_data('society_id', $society_id);
            $m->set_data('user_id', $usersIds[$i]);
            $m->set_data('schedule_date', $schedule_date);
            $m->set_data('user_level_id', $level_id);
            $m->set_data('dimensional_id', $dimensional_id);
            $m->set_data('pms_type_on_schedule', $pms_type);
            $m->set_data('schedule_created_date', date("Y-m-d H:i:s"));

            $a2 = array(
                'society_id' => $m->get_data('society_id'),
                'user_id' => $m->get_data('user_id'),
                'schedule_date' => $m->get_data('schedule_date'),
                'user_level_id' => $m->get_data('user_level_id'),
                'dimensional_id' => $m->get_data('dimensional_id'),
                'pms_type_on_schedule' => $m->get_data('pms_type_on_schedule'),
                'schedule_created_date' => $m->get_data('schedule_created_date'),
            );

            
            $q1 = $d->insert("pms_schedule_master", $a2);
        }


        if ($q == true) {
            header("Location: ../manageDimensionalAssign");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../manageDimensionalAssign");
        }

    }

}
