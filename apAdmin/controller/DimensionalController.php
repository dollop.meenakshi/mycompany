<?php
include '../common/objectController.php';
extract($_POST);

if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605


    if (isset($_POST['addDimensionals'])) {

        if (isset($_POST['deletedIds']) && !empty($_POST['deletedIds'])) {

            for ($i = 0; $i < (count($_POST['deletedIds'])); $i++) {
                $dimensional_id = $_POST['deletedIds'][$i];
                $q11 = $d->delete("dimensional_master", "dimensional_id='$dimensional_id'");
            }
        }
        if (!empty($_POST['dimensional_name']) && !empty($_POST['dimensional_weightage'])) {

            $m->set_data('society_id', $society_id);
            $m->set_data('dimensional_added_by', $_COOKIE['bms_admin_id']);

            $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");

            $usq = $d->select("dimensional_master", "society_id='$society_id'");
            if (mysqli_num_rows($usq) > 0) {
                for ($i = 0; $i < (count($_POST['dimensional_name'])); $i++) {
                    if ($_POST['dimensional_name'][$i] != "" && $_POST['dimensional_weightage'][$i] != "") {
                    $a = array(
                        'society_id' => $m->get_data('society_id'),
                        'dimensional_name' => $_POST['dimensional_name'][$i],
                        'dimensional_weightage' => $_POST['dimensional_weightage'][$i],
                        'dimensional_added_by' => $m->get_data('dimensional_added_by'),
                    );
                    // $dimensional_image='';
                        $file = $_FILES['dimensional_image']['tmp_name'][$i];
                        if (file_exists($file)) {
                            $extensionResume = array('jpeg','JPEG',"jpg","JPG","png","PNG");
                            $extId = pathinfo($_FILES['dimensional_image']['name'][$i], PATHINFO_EXTENSION);

                            $errors = array();
                            $maxsize = 26214400;

                            if (($_FILES['dimensional_image']['size'][$i] >= $maxsize) || ($_FILES["dimensional_image"]["size"][$i] == 0)) {
                                $_SESSION['msg1'] = "Image is too large. Must be less than 25 MB.";
                                header("location:../dimensionals");
                                exit();
                            }
                            if (!in_array($extId, $extensionResume) && (!empty($_FILES["dimensional_image"]["type"][$i]))) {
                                $_SESSION['msg1'] = "Invalid File format, Only jpg, jpeg and png is allowed.";
                                header("location:../dimensionals");
                                exit();
                            }
                            if (count($errors) === 0) {
                                $image_Arr = $_FILES['dimensional_image'];
                                $temp = explode(".", $_FILES["dimensional_image"]["name"][$i]);
                                $dimensional_image = 'dimensional_image_' . round(microtime(true)).$i. '.' . end($temp);
                                move_uploaded_file($_FILES["dimensional_image"]["tmp_name"][$i], "../../img/dimensionals/".$dimensional_image);
                                $a['dimensional_image']=$dimensional_image;
                            }
                        }

                        if ($_POST['dimensional_id'][$i] != "") {
                            $dimensional_id = $_POST['dimensional_id'][$i];
                            
                            $q = $d->update("dimensional_master", $a, "dimensional_id='$dimensional_id'");
                        } else {
                            
                            $q = $d->insert("dimensional_master", $a);
                        }
                    }
                }

                if ($q > 0) {
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$dimensional_added_by", "Dimensional Updated Successfully");
                    $_SESSION['msg'] = "Dimensional Updated Successfully " . $errorMessageAppend;
                } else {
                    $_SESSION['msg1'] = "Something Wrong";
                }
                header("location:../dimensionals");
            } else {

                for ($i = 0; $i < (count($_POST['dimensional_name'])); $i++) {
                    if ($_POST['dimensional_name'][$i] != "" && $_POST['dimensional_weightage'][$i] != "") {
                        $a = array(
                            'society_id' => $m->get_data('society_id'),
                            'dimensional_name' => $_POST['dimensional_name'][$i],
                            'dimensional_weightage' => $_POST['dimensional_weightage'][$i],
                            'dimensional_added_by' => $m->get_data('dimensional_added_by'),
                            'dimensional_created_date' => date('Y-m-d H:i:s'),

                        );
                        $file = $_FILES['dimensional_image']['tmp_name'][$i];
                        if (file_exists($file)) {
                            $extensionResume = array('jpeg','JPEG',"jpg","JPG","png","PNG");
                            $extId = pathinfo($_FILES['dimensional_image']['name'][$i], PATHINFO_EXTENSION);

                            $errors = array();
                            $maxsize = 26214400;

                            if (($_FILES['dimensional_image']['size'][$i] >= $maxsize) || ($_FILES["dimensional_image"]["size"][$i] == 0)) {
                                $_SESSION['msg1'] = "Image is too large. Must be less than 25 MB.";
                                header("location:../dimensionals");
                                exit();
                            }
                            if (!in_array($extId, $extensionResume) && (!empty($_FILES["dimensional_image"]["type"][$i]))) {
                                $_SESSION['msg1'] = "Invalid File format, Only jpg, jpeg and png is allowed.";
                                header("location:../dimensionals");
                                exit();
                            }
                            if (count($errors) === 0) {
                                $image_Arr = $_FILES['dimensional_image'];
                                $temp = explode(".", $_FILES["dimensional_image"]["name"][$i]);
                                $dimensional_image = 'dimensional_image_' . round(microtime(true)).$i. '.' . end($temp);
                                move_uploaded_file($_FILES["dimensional_image"]["tmp_name"][$i], "../../img/dimensionals/".$dimensional_image);
                                $a['dimensional_image']=$dimensional_image;
                            }
                        }

                        
                        $q = $d->insert("dimensional_master", $a);
                    }
                }

                if ($q > 0) {
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$dimensional_added_by", "Dimensional Added Successfully");
                    $_SESSION['msg'] = "Dimensional Added Successfully";
                } else {
                    $_SESSION['msg1'] = "Something Wrong";
                }
                header("location:../dimensionals");
            }
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../dimensionals");
        }
    }
}
