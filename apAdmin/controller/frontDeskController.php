<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));

if (isset($_POST)) {
  if (isset($_POST['addUser'])) {

    $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
    $Files = $_FILES['user_pic'];
    $temp_name = $Files['tmp_name'];
    $ext = pathinfo($Files['name'], PATHINFO_EXTENSION);
    if (isset($temp_name) && $temp_name != "") {
      $dirPath = "../../commercial/img/profile/";
      $newFileName = rand();
      $name = $newFileName . "." . $ext;
      $folder = $dirPath . $name;
      if (move_uploaded_file($temp_name, $folder)) {
        $image_path = $name;
      } else {
        $image_path = " ";
      }
    }
  } else {
    $image_path = $old_profile;
  }

  $m->set_data('user_name', $user_name);
  $m->set_data('society_id', $society_id);
  $m->set_data('unit_id', $_SESSION['unit_id']);
  $m->set_data('user_mobile', $user_mobile);
  $m->set_data('user_password', $user_password);
  $m->set_data('branch_id', $branch_id);
  $m->set_data('active_status', 0);
  $m->set_data('login_profile', $image_path);
  $m->set_data('login_date', date('Y-m-d'));

  $a = array(
    'society_id' => $m->get_data('society_id'),
    'unit_id' => $m->get_data("unit_id"),
    'login_name' => $m->get_data('user_name'),
    'login_mobile' => $m->get_data('user_mobile'),
    'login_password' => $m->get_data('user_password'),
    'login_profile' => $m->get_data('login_profile'),
    'block_id' => $m->get_data('branch_id'),
    'active_status' => $m->get_data('active_status'),
    'login_created_date' => $m->get_data('login_date'),
  );
  if (isset($_POST['user_id']) && $_POST['user_id'] != "") {
    $check = $d->select("users_entry_login", "login_mobile='" . $user_mobile . "' and active_status=0 AND user_entry_id NOT IN ('".$_POST['user_id']."')");
    if (mysqli_num_rows($check) > 0) {
      $_SESSION['msg1'] = $user_mobile . " Already Exist Please Enter Another Number";
      header("location:../frontDesk");
      exit;
    };
    $q = $d->update("users_entry_login", $a, "user_entry_id='" . $_POST['user_id'] . "'");
    $msg = "$user_name Account Edit Successfully";
  } else {
    $check = $d->select("users_entry_login", "login_mobile='" . $user_mobile . "' and active_status=0");
    if (mysqli_num_rows($check) > 0) {
      $_SESSION['msg1'] = $user_mobile . " Already Exist Please Enter Another Number";
      header("location:../frontDesk");
      exit;
    };
    $q = $d->insert("users_entry_login", $a);
    $msg = "$user_name Account Create Successfully";
  }

  if ($q > 0) {
    $_SESSION['msg'] = $msg;
    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$msg");
    header("location:../listAllUser");
  } else {
    $_SESSION['msg1'] = "Something Wrong";
    header("location:../frontDesk");
  }
}
