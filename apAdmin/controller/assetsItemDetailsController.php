
<?php 
include '../common/objectController.php';
extract($_POST);


if(isset($_POST) && !empty($_POST) )
{

  if(isset($submit_assets)) {

    /* echo "<pre>";
    print_r($_POST);die;
 */
    /* $assets_nameTemp=mysqli_real_escape_string($con, $assets_name);
    $get_cat = $d->selectRow("assets_category_id","assets_item_detail_master","assets_name = '$assets_nameTemp' AND assets_category_id='$assets_category_id'");
      if(mysqli_num_rows($get_cat) > 0){
        $_SESSION['msg1']="Assets Category  And Assets Name Already Exists";
        header("Location: ../itemDetails");
        exit;
      } */

      if($stock_measurement != 'Other'){
        $stock_measurement_other_name = '';
      }

      $file = $_FILES['assets_file']['tmp_name'];
    if(file_exists($file)) {

      $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['assets_file']['tmp_name'];
      $ext = pathinfo($_FILES['assets_file']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = $city_name.round(microtime(true));
          $dirPath = "../../img/society/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>1200) {
            $newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }

         switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName.$ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../profile");
                exit;
                break;
            }
            $assets_file= $newFileName.$ext;
         } 
        }else {
            $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
            header("location:../itemDetails?");
            exit();
         }
        




          /* $errors     = array();
          $maxsize    = 1097152;
          $acceptable = array(
            'image/jpeg',
            'image/jpg',
            'image/png'
          );
          if(($_FILES['assets_file']['size'] >= $maxsize) || ($_FILES["assets_file"]["size"] == 0)) {
            $_SESSION['msg1']="photo too large. Must be less than 1 MB.";
            header("location:../itemDetails");
            exit();
          }
          if(!in_array($_FILES['m']['type'], $acceptable) && (!empty($_FILES["assets_file"]["type"]))) {
            $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
            header("location:../itemDetails");
            exit();
          }
          if(count($errors) === 0) {
            $image_Arr = $_FILES['assets_file'];   
            $temp = explode(".", $_FILES["assets_file"]["name"]);
            $assets_file = $city_name.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["assets_file"]["tmp_name"], "../../img/society/".$assets_file);
          }  */

    }
  /*-----------------------------------------------------------------------*/  


  $file = $_FILES['assets_invoice']['tmp_name'];
    if(file_exists($file)) {
    // checking if main category value was changed
      $errors     = array();
      $maxsize    = 5097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['assets_invoice']['size'] >= $maxsize) || ($_FILES["assets_invoice"]["size"] == 0)) {
        $_SESSION['msg1']=" Pdf / Photo too large. File must be less than 5 MB.";
        header("location:../itemDetails?");
        exit();
      }
      if(!in_array($_FILES['assets_invoice']['type'], $acceptable) && (!empty($_FILES["assets_invoice"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../itemDetails?");
        exit();
      }

      $assets_invoice = $_FILES['assets_invoice'];   
      $temp = explode(".", $_FILES["assets_invoice"]["name"]);
      $assets_invoice = rand().'.' . end($temp);
      move_uploaded_file($_FILES["assets_invoice"]["tmp_name"], "../../img/society/".$assets_invoice);
    } 



      
      $m->set_data('society_id',$society_id);
      $m->set_data('assets_name',ucfirst($assets_name));
      $m->set_data('assets_location',ucfirst($assets_location));
      $m->set_data('assets_item_code',ucfirst($assets_item_code));
      $m->set_data('assets_description',ucfirst($assets_description));
      $m->set_data('assets_brand_name',ucfirst($assets_brand_name));
      $m->set_data('assets_category_id',$assets_category_id);
      $m->set_data('item_purchase_date',$item_purchase_date);
      $m->set_data('assets_file',$assets_file);
      $m->set_data('sr_no',$sr_no);
      $m->set_data('item_price',$item_price);
      $m->set_data('assets_invoice',$assets_invoice);
      $m->set_data('iteam_created_date',date("Y-m-d H:i:s"));
      $m->set_data('created_by',$_COOKIE['bms_admin_id']);
      
     
      $a = array(
        'society_id'=>$m->get_data('society_id'),
        'assets_brand_name'=>$m->get_data('assets_brand_name'),
        'assets_location'=>$m->get_data('assets_location'),
        'assets_item_code'=>$m->get_data('assets_item_code'),
        'assets_description'=>$m->get_data('assets_description'),
        'assets_name'=>$m->get_data('assets_name'),
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'item_purchase_date'=>$m->get_data('item_purchase_date'),   
        'assets_file'=>$m->get_data('assets_file'),
        'sr_no'=>$m->get_data('sr_no'),
        'item_price'=>$m->get_data('item_price'),
        'assets_invoice'=>$m->get_data('assets_invoice'),
        'iteam_created_date'=>$m->get_data('iteam_created_date'),
        'created_by'=>$m->get_data('created_by'),
        
      );
      
      $q=$d->insert("assets_item_detail_master",$a);
    
     $assets_id = $con->insert_id;

      $m->set_data('assets_id',$assets_id);
      $m->set_data('assets_category_id',$assets_category_id);
      $m->set_data('society_id',$society_id);
      $m->set_data('floor_id',$floor_id);
      $m->set_data('user_id',$user_id);
      $m->set_data('start_date',$start_date);
      $m->set_data('iteam_created_date',date("Y-m-d H:i:s"));
      $m->set_data('created_by',$_COOKIE['bms_admin_id']);
      

      $a = array(
        'assets_id'=>$m->get_data('assets_id'),
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'society_id'=>$m->get_data('society_id'),
        'floor_id'=>$m->get_data('floor_id'),
        'user_id'=>$m->get_data('user_id'),
        'start_date'=>$m->get_data('start_date'),
        'iteam_created_date'=>$m->get_data('iteam_created_date'),
        'created_by'=>$m->get_data('created_by')
      );


      if($user_id>0){
        
      $q=$d->insert("assets_detail_inventory_master",$a);
      } 

      if($q==TRUE){
        if($user_id>0){
            $handoverq = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
            $dataHandover = mysqli_fetch_assoc($handoverq);
            $titleHandover = "Assets Management";
            $descriptionHandover = "New asset has handover by $created_by";
            $menuClickHandover = "assets_management";
            $imageHandover = "assets_management.png";
            $activityHandover = "0";
            $user_token_handover = $dataHandover['user_token'];
            $deviceHandover = $dataHandover['device'];
            if ($deviceHandover=='android') {
                $nResident->noti($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
            } else {
                $nResident->noti_ios($menuClickHandover,$imageHandover,$society_id,$user_token_handover,$titleHandover,$descriptionHandover,$activityHandover);
            }
        }
      $d->insertUserNotification($society_id,$titleHandover,$descriptionHandover,"assets_management","assets_management.png","user_id='$user_id'");
        $_SESSION['msg']="Assets Item Added successfully";
        $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Item $assets_name Added");
        header("Location: ../itemDetails");
      }else{
        $_SESSION['msg1']="Assets Item Added Unsuccessfull";
        header("Location: ../itemDetails");
      }
  }
/*--------------------------------------------------------------*/
        if(isset($delete_assets)) 
        {
          $m->set_data('move_to_scrap_reason',$_COOKIE['move_to_scrap_reason']);
          $a = array('move_to_scrap' => 1,
                      'move_to_scrap_reason' => $move_to_scrap_reason);

          $q=$d->update("assets_item_detail_master",$a,"assets_id='$assets_id'");
          if($q==TRUE)
          {
            $_SESSION['msg']="Assets Item Moved In Scrap";
            $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Item Moved In Scrap");
            header("Location:../scrapAssetsItem");
          }
          else{
            $_SESSION['msg1']="Somthing Wrong";
            header("Location:../scrapAssetsItem");
          }
        }

/*--------------------------------------------------------------*/

        if(isset($sold_out_assets)) 
        {
          $m->set_data('move_to_scrap_reason',$move_to_scrap_reason);
          $m->set_data('sold_out_price',$sold_out_price);
          $a = array('move_to_scrap' => 1,
                      'is_assets_item_sold_out' => 1,
                      'sold_out_price' => $sold_out_price,
                      'move_to_scrap_reason' => $move_to_scrap_reason);

          $q=$d->update("assets_item_detail_master",$a,"assets_id='$assets_id'");
          if($q==TRUE)
          {
            $_SESSION['msg']="Sold Out Successfully";
            $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Item Sold Out Successfully");
            header("Location:../scrapAssetsItem");
          }
          else{
            $_SESSION['msg1']="Somthing Wrong";
            header("Location:../scrapAssetsItem");
          }
        }
/*---------------------------------------------------------------*/
        if (isset($update_assets)) 
         {
           
          /* $assets_nameTemp=mysqli_real_escape_string($con, $assets_name);
         $get_cat = $d->selectRow("assets_category_id","assets_item_detail_master","assets_category_id='$assets_category_id' AND assets_name = '$assets_nameTemp' AND assets_id!='$assets_id'");
        if(mysqli_num_rows($get_cat) > 0)
        {
          $_SESSION['msg1']="Assets Category  And Assets Name Are Same";
          header("Location: ../itemDetails");
          exit;
        }
         */
      if($stock_measurement != 'Other'){
        $stock_measurement_other_name = '';
      }


      $file = $_FILES['assets_file']['tmp_name'];
    if(file_exists($file)) {
      $errors     = array();
      $maxsize    = 1097152;
      $acceptable = array(
        'image/jpeg',
        'image/jpg',
        'image/png'
      );
      if(($_FILES['assets_file']['size'] >= $maxsize) || ($_FILES["assets_file"]["size"] == 0)) {
        $_SESSION['msg1']="photo too large. Must be less than 1 MB.";
        header("location:../itemDetails");
        exit();
      }
      if(!in_array($_FILES['assets_file']['type'], $acceptable) && (!empty($_FILES["assets_file"]["type"]))) {
        $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
        header("location:../itemDetails");
        exit();
      }
      if(count($errors) === 0) {
        $image_Arr = $_FILES['assets_file'];   
        $temp = explode(".", $_FILES["assets_file"]["name"]);
        $assets_file = $city_name.round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["assets_file"]["tmp_name"], "../../img/society/".$assets_file);
      } 
    } else{
                      $assets_file=$image_old;
                    }

/*-------------------------------------------------------------------------------------------*/
$file = $_FILES['assets_invoice']['tmp_name'];
    if(file_exists($file)) {
    // checking if main category value was changed
      $errors     = array();
      $maxsize    = 5097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['assets_invoice']['size'] >= $maxsize) || ($_FILES["assets_invoice"]["size"] == 0)) {
        $_SESSION['msg1']=" Pdf / Photo too large. File must be less than 5 MB.";
        header("location:../itemDetails?");
        exit();
      }
      if(!in_array($_FILES['assets_invoice']['type'], $acceptable) && (!empty($_FILES["assets_invoice"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../itemDetails?");
        exit();
      }

      $assets_invoice = $_FILES['assets_invoice'];   
      $temp = explode(".", $_FILES["assets_invoice"]["name"]);
      $assets_invoice = rand().'.' . end($temp);
      move_uploaded_file($_FILES["assets_invoice"]["tmp_name"], "../../img/society/".$assets_invoice);
    } 


 else{
                      $assets_invoice=$old_pdf;
                    }

/*-------------------------------------------------------------------------------------------*/
       
      $m->set_data('society_id',$society_id);
      $m->set_data('assets_name',$assets_name);
      $m->set_data('assets_location',$assets_location);
      $m->set_data('assets_item_code',$assets_item_code);
      $m->set_data('assets_description',$assets_description);
      $m->set_data('assets_brand_name',$assets_brand_name);
      $m->set_data('assets_category_id',$assets_category_id);
      $m->set_data('item_purchase_date',$item_purchase_date);
      $m->set_data('assets_file',$assets_file);
      $m->set_data('sr_no',$sr_no);
      $m->set_data('item_price',$item_price);
      $m->set_data('assets_invoice',$assets_invoice);
      $m->set_data('iteam_created_date',date("Y-m-d H:i:s"));
      $m->set_data('created_by',$_COOKIE['bms_admin_id']);
      
     
      $a = array(
        'society_id'=>$m->get_data('society_id'),
        'assets_brand_name'=>$m->get_data('assets_brand_name'),
        'assets_name'=>$m->get_data('assets_name'),
        'assets_location'=>$m->get_data('assets_location'),
        'assets_item_code'=>$m->get_data('assets_item_code'),
        'assets_description'=>$m->get_data('assets_description'),
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'item_purchase_date'=>$m->get_data('item_purchase_date'),   
        'assets_file'=>$m->get_data('assets_file'),
        'sr_no'=>$m->get_data('sr_no'),
        'item_price'=>$m->get_data('item_price'),
        'assets_invoice'=>$m->get_data('assets_invoice'),
        'created_by'=>$m->get_data('created_by'),
        
      );

        $q=$d->update("assets_item_detail_master",$a,"assets_id='$assets_id'");
        if($q==TRUE){
          $_SESSION['msg']="Assets Item Update successfully";
          $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Item $assets_name Update");
          header("Location:../itemDetails");
        }else{
          $_SESSION['msg1']="Assets Item Update Unsuccessfull";
          header("Location:../itemDetails");
        }
  }

  if(isset($addAssetsMaintenance) && $addAssetsMaintenance == "addAssetsMaintenance"){

      if($maintenance_type == 1){
        $days = implode(',',$_POST['week_days']);
      }

      if($maintenance_type == 2){
        $days = implode(',',$_POST['month_days']);
      }

      if($maintenance_type == 3){
        $days = $_POST['month_date'];
      }

      if($maintenance_type == 3 || $maintenance_type == 4 || $maintenance_type == 5 || $maintenance_type == 6){
        $days = $_POST['month_date'];
      }

      $m->set_data('society_id',$society_id);
      $m->set_data('assets_category_id',$assets_category_id);
      $m->set_data('assets_id',$assets_id);
      $m->set_data('maintenance_type',$maintenance_type);
      $m->set_data('custom_date',$custom_date);
      $m->set_data('days',$days);
      $m->set_data('start_month',$start_month);
      $m->set_data('vendor_name',$vendor_name);
      $m->set_data('vendor_mobile_no',$vendor_mobile_no);
      $m->set_data('maintenance_added_by',$_COOKIE['bms_admin_id']);
      $m->set_data('maintenance_created_at',date("Y-m-d H:i:s"));

      $a = array(
        'society_id'=>$m->get_data('society_id'),
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'assets_id'=>$m->get_data('assets_id'),
        'maintenance_type'=>$m->get_data('maintenance_type'),
        'custom_date'=>$m->get_data('custom_date'),
        'days'=>$m->get_data('days'),
        'start_month'=>$m->get_data('start_month'),
        'vendor_name'=>$m->get_data('vendor_name'),
        'vendor_mobile_no'=>$m->get_data('vendor_mobile_no'),
      );

      $amq = $d->selectRow("assets_maintenance_id","assets_maintenance_master","assets_category_id='$assets_category_id' AND assets_id='$assets_id' AND maintenance_type='$maintenance_type' AND society_id='$society_id'");
      $row = mysqli_fetch_array($amq);
      if($row){
        $q = $d->update("assets_maintenance_master", $a, "assets_maintenance_id ='$row[assets_maintenance_id]'");
      }else{
        $a['maintenance_added_by'] = $m->get_data('maintenance_added_by');
        $a['maintenance_created_at'] = $m->get_data('maintenance_created_at');
        $q = $d->insert("assets_maintenance_master", $a);
        $assets_maintenance_id = $con->insert_id;

        $a1 = array(
          'society_id'=>$society_id,
          'assets_id'=>$assets_id,
          'assets_maintenance_id'=>$assets_maintenance_id,
          'last_sync_date'=>date("Y-m-d H:i:s"),
        );

        $q1 = $d->insert("assets_maintenance_last_sync", $a1);
      }

      if ($q == true) {
          $_SESSION['msg'] = "Maintenance Added Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Maintenance Added Successfully");
          header("Location: ../assetsMaintenance");
      } else {
          $_SESSION['msg1'] = "Something Wrong";
          header("Location: ../addAssetsMaintenance");
      }
  }

  if(isset($editAssetsMaintenance) && $editAssetsMaintenance == "editAssetsMaintenance"){
    
      if($maintenance_type == 1){
        $days = implode(',',$_POST['week_days']);
      }

      if($maintenance_type == 2){
        $days = implode(',',$_POST['month_days']);
      }

      if($maintenance_type == 3){
        $days = $_POST['month_date'];
      }

      if($maintenance_type == 3 || $maintenance_type == 4 || $maintenance_type == 5 || $maintenance_type == 6){
        $days = $_POST['month_date'];
      }
      
      $m->set_data('maintenance_type',$maintenance_type);
      $m->set_data('custom_date',$custom_date);
      $m->set_data('days',$days);
      $m->set_data('start_month',$start_month);
      $m->set_data('vendor_name',$vendor_name);
      $m->set_data('vendor_mobile_no',$vendor_mobile_no);

      $a = array(
        'maintenance_type'=>$m->get_data('maintenance_type'),
        'custom_date'=>$m->get_data('custom_date'),
        'days'=>$m->get_data('days'),
        'start_month'=>$m->get_data('start_month'),
        'vendor_name'=>$m->get_data('vendor_name'),
        'vendor_mobile_no'=>$m->get_data('vendor_mobile_no'),
      );

      $q = $d->update("assets_maintenance_master", $a, "assets_maintenance_id ='$assets_maintenance_id'");

      if ($q == true) {
          $_SESSION['msg'] = "Maintenance Added Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Maintenance Added Successfully");
          header("Location: ../assetsMaintenance");
      } else {
          $_SESSION['msg1'] = "Something Wrong";
          header("Location: ../addAssetsMaintenance");
      }
  }

  if(isset($completeAssetsMaintenance) && $completeAssetsMaintenance == "completeAssetsMaintenance"){
     
      $m->set_data('society_id',$society_id);
      $m->set_data('assets_category_id',$assets_category_id);
      $m->set_data('assets_id',$assets_id);
      $m->set_data('assets_maintenance_id',$assets_maintenance_id);
      $m->set_data('maintenance_amount',$maintenance_amount);
      $m->set_data('remark',$remark);
      $m->set_data('is_maintenance_complete',1);
      $m->set_data('maintenance_schedule_date',date('Y-m-d', strtotime($date)));
      $m->set_data('maintenance_complete_at',date("Y-m-d H:i:s"));

      $a = array(
        'society_id'=>$m->get_data('society_id'),
        'assets_category_id'=>$m->get_data('assets_category_id'),
        'assets_id'=>$m->get_data('assets_id'),
        'assets_maintenance_id'=>$m->get_data('assets_maintenance_id'),
        'maintenance_amount'=>$m->get_data('maintenance_amount'),
        'remark'=>$m->get_data('remark'),
        'is_maintenance_complete'=>$m->get_data('is_maintenance_complete'),
        'maintenance_schedule_date'=>$m->get_data('maintenance_schedule_date'),
        'maintenance_complete_at'=>$m->get_data('maintenance_complete_at'),
      );

      $q = $d->insert("assets_maintenance_complete", $a);

      if ($q == true) {
        $_SESSION['msg'] = "Maintenance Completed";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Maintenance Completed");
        header("Location: ../completedAssetsMaintenance");
      } else {
          $_SESSION['msg1'] = "Something Wrong";
          header("Location: ../completedAssetsMaintenance");
      }
  }

  if(isset($markAsMaintenanceCompleted) && $markAsMaintenanceCompleted == "markAsMaintenanceCompleted"){
     
    $m->set_data('maintenance_amount',$maintenance_amount);
    $m->set_data('remark',$remark);
    $m->set_data('is_maintenance_complete',1);
    $m->set_data('maintenance_complete_at',date("Y-m-d H:i:s"));

    $a = array(
      'maintenance_amount'=>$m->get_data('maintenance_amount'),
      'remark'=>$m->get_data('remark'),
      'is_maintenance_complete'=>$m->get_data('is_maintenance_complete'),
      'maintenance_complete_at'=>$m->get_data('maintenance_complete_at'),
    );

    $q = $d->update("assets_maintenance_complete", $a, "assets_maintenance_complete_id ='$assets_maintenance_complete_id'");

    if ($q == true) {
      $_SESSION['msg'] = "Maintenance Completed";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Maintenance Completed");
      header("Location: ../completedAssetsMaintenance");
    } else {
        $_SESSION['msg1'] = "Something Wrong";
        header("Location: ../completedAssetsMaintenance");
    }
  }
}
else
{
  header('location:../login');
}
?>