<?php 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
$base_url=$m->base_url();

header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
 //print_r($_POST);exit;
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
    $response = array();

  // add main menu
  if(isset($_POST['getDemoLoginNumber']) && $societyId!=0 && $societyId!=''){
      $response["users"] = array();
      $q=$d->select("users_master","delete_status=0 AND is_demo=1","");
      while ($userData=mysqli_fetch_array($q)) {
          $users=array();
          $users["user_mobile"]=$userData["user_mobile"];
          $users["user_full_name"]=$userData["user_full_name"];
          array_push($response["users"], $users); 
      } 

      $response["message"]="success.";
      $response["status"]="200";
      echo json_encode($response);

  }

   if(isset($_POST['getMobileDetails']) && $societyId!=0 && $societyId!=''){
      $response["users"] = array();
      $q=$d->select("users_master","delete_status=0 AND user_mobile ='$mobileNumber'  AND is_demo=1","");
      $userData=mysqli_fetch_array($q);
      $cTime = date("Y-m-d H:i:s");
      $expire = strtotime($userData[demo_expire_time]);
      $today = strtotime("$cTime");

      if ($userData['demo_expire_time']!='' && $today <= $expire) {
        $message = "OTP Already Set For ".$userData['user_full_name']." Expire on ".date("d M Y h:i A", strtotime($userData['demo_expire_time']));
      } else {
        $message = "";
      }
      $response["server_message"]=$message;
      $response["message"]="success.";
      $response["status"]="200";
      echo json_encode($response);

  }
  
  if(isset($_POST['setOTP']) && $societyId!=0 && $societyId!=''){

        if ($societyId==50 && $mobileNumber=='3213213210') {
          $response["message"]="This Is IOS Testing Number Try Another Number.";
          $response["status"]="200";
          echo json_encode($response);
          exit();
        }

        $q=$d->select("users_master","delete_status=0 AND user_mobile ='$mobileNumber'","");
        $userData=mysqli_fetch_array($q);
        if ($userData['user_token']!="") {
          $user_token = $userData['user_token'];
          $device = $userData['device'];
          if ($device=='ios') {
            $nResident->noti_ios("","",$societyId,$user_token,'Logout','Logout',"");
          } else {
            $nResident->noti("","",$societyId,$user_token,'Logout','Logout',"");
          }
        }
        $country_code  = $userData['country_code'];

        $sq = $d->select("society_master","society_id='$societyId'");
        $socData=mysqli_fetch_array($sq);
        $society_name = $socData['society_name'];
        $city_name = $socData['city_name'];
        $country_id = $socData['country_id'];
        $state_id = $socData['state_id'];
        $cTime = date("Y-m-d H:i:s");
        $nextHours= $valid_till *60;
        $timestamp = strtotime($cTime) + 60*$nextHours;
        $valid_till_date = date('Y-m-d H:i:s', $timestamp);

        $nameAry = explode(" ", $clientName);
        if ($nameAry[0]=="") {
          $user_first_name = "Demo";
        } else {
          $user_first_name = $nameAry[0];
        }
        if ($nameAry[1]=="") {
          $user_last_name = "Demo";
        }else {
          $user_last_name = $nameAry[1];
        }

        $m->set_data('user_full_name', $clientName);
        $m->set_data('user_first_name', $user_first_name);
        $m->set_data('user_last_name', $user_last_name);
        $m->set_data('otp', $new_otp);
        $m->set_data('demo_expire_time', $valid_till_date);
        $a = array(
          'user_full_name' => $m->get_data('user_full_name'),
          'user_first_name' => $m->get_data('user_first_name'),
          'user_last_name' => $m->get_data('user_last_name'),
          'otp' => $m->get_data('otp'),
          'opt_attempt' => '',
          'otp_attempt_time' => '',
          'demo_expire_time' => $m->get_data('demo_expire_time'),
      );

      $q = $d->update("users_master", $a, "user_mobile='$mobileNumber'  AND is_demo=1");
      if ($q >0) {


        $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS,
                      "getCountriesSingle=getCountriesSingle&country_id=$country_id&language_id=1");

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'key: bmsapikey'
          ));

          $server_output = curl_exec($ch);

          curl_close ($ch);
          $server_output=json_decode($server_output,true);

       $country_name= $server_output['name'];


     $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "getStateSingle=getStateSingle&state_id=$state_id&language_id=1");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'key: bmsapikey'
        ));

        $server_output = curl_exec($ch);

        curl_close ($ch);
        $server_output=json_decode($server_output,true);

     $state_name= $server_output['name'];

        $response["message"]="Demo Account Created <br>Country : $country_name<br>State : $state_name<br>City : $city_name<br>Society : $society_name<br>Country Code : $country_code Mobile Number: $mobileNumber<br>OTP: $new_otp ";
        $response["status"]="200";
        echo json_encode($response);
      } else {
        $response["message"]="Something Went Wrong";
        $response["status"]="201";
        echo json_encode($response);
      }
      

  }

}
 ?>
