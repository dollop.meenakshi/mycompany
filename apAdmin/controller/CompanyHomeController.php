<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addCompanyHomeMaster'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('company_home_title', $company_home_title);
        $m->set_data('company_home_description', $company_home_description);
        $m->set_data('company_home_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('company_home_created_at',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'company_home_title' => $m->get_data('company_home_title'),
            'company_home_description' => $m->get_data('company_home_description'),
            'company_home_added_by' => $m->get_data('company_home_added_by'),
            'company_home_created_at' => $m->get_data('company_home_created_at'),

        );
        if (isset($company_home_id) && $company_home_id > 0) {
            $q = $d->update("company_home_master", $a1, "company_home_id ='$company_home_id'");
            $_SESSION['msg'] = "Company Home Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Home Updated Successfully");
        } else {
            $q = $d->insert("company_home_master", $a1);
            $_SESSION['msg'] = "Company Home Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Home Added Successfully");

        }
        if ($q == true) {
            header("Location: ../companyHomeMaster");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyHomeMaster");
        }

    }

}
