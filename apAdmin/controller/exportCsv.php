<?php
session_start();
include_once 'lib/dao.php';
include 'lib/model.php';
$created_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

//Enter the headings of the excel columns
if(!isset($_COOKIE['bms_admin_id']))
{
  $_SESSION['msg1']= "Login First.";
  header("location:index.php?Login First");
}

$d = new dao();
$m = new model();
extract($_GET);
if ($report=="Bill") {

$contents="No,Bill Name,Block Unit,User,Amount,No of Unit,Unit Price,Status,Bill Paid Date,Generated Date\n";
 $qb=$d->select("bill_master","bill_master_id='$_GET[bill_master_id]' ");
 $billData=mysqli_fetch_array($qb);
$bill_name= $billData['bill_name'];
$auto_created_date= $billData['auto_created_date'];
switch ($_GET['type']) {
    case '100':
       $q=$d->select("receive_bill_master,bill_master,unit_master,block_master,users_master","users_master.unit_id=receive_bill_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_bill_master.unit_id AND receive_bill_master.bill_master_id=bill_master.bill_master_id AND receive_bill_master.society_id='$society_id' AND receive_bill_master.bill_master_id='$_GET[bill_master_id]' ");
      break;
    case '3':
       $q=$d->select("receive_bill_master,bill_master,unit_master,block_master,users_master","users_master.unit_id=receive_bill_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_bill_master.unit_id AND receive_bill_master.bill_master_id=bill_master.bill_master_id AND receive_bill_master.society_id='$society_id' AND receive_bill_master.bill_master_id='$_GET[bill_master_id]' AND receive_bill_master.receive_bill_status=3");
      break;
    case '2':
       $q=$d->select("receive_bill_master,bill_master,unit_master,block_master,users_master","users_master.unit_id=receive_bill_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_bill_master.unit_id AND receive_bill_master.bill_master_id=bill_master.bill_master_id AND receive_bill_master.society_id='$society_id' AND receive_bill_master.bill_master_id='$_GET[bill_master_id]' AND receive_bill_master.receive_bill_status=2");
    break;
    case '0':
         $q=$d->select("receive_bill_master,bill_master,unit_master,block_master,users_master","users_master.unit_id=receive_bill_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_bill_master.unit_id AND receive_bill_master.bill_master_id=bill_master.bill_master_id AND receive_bill_master.society_id='$society_id' AND receive_bill_master.bill_master_id='$_GET[bill_master_id]' AND receive_bill_master.receive_bill_status=0");
    break;
    default:
       $q=$d->select("receive_bill_master,bill_master,unit_master,block_master,users_master","users_master.unit_id=receive_bill_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_bill_master.unit_id AND receive_bill_master.bill_master_id=bill_master.bill_master_id AND receive_bill_master.society_id='$society_id' AND receive_bill_master.bill_master_id='$_GET[bill_master_id]'");
      break;
    }

$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{
    if ($row['receive_bill_status']==1) {
        $status= "Bill Genereted";
       } elseif ($row['receive_bill_status']==2) {
        $status = "Unpaid";
       } elseif ($row['receive_bill_status']==3) {
        $status = "Paid";
       } elseif ($row['receive_bill_status']==0) {
        $status = "Bill Not Generate";
       }
$contents.=$i++.",";
$contents.=$bill_name."-".$auto_created_date.",";
$contents.=$row['block_name']."-".$row['unit_name'].",";
$contents.=$row['user_full_name'].",";
$contents.=$row['bill_amount'].",";
$contents.=$row['no_of_unit'].",";
$contents.=$row['unit_price'].",";
$contents.=$status.",";
$contents.=$row['bill_payment_date'].",";
$contents.=$row['bill_genrate_date']."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Bill_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}

// Bill Report
if ($report=="Maintenance") {

$contents="No,Name,Block Unit,User,Amount,Status,Paid Date,Generated Date\n";
$qb=$d->select("maintenance_master","maintenance_id='$_GET[maintenance_id]' ");
$maintenanceData=mysqli_fetch_array($qb);
$maintenance_name= $maintenanceData['maintenance_name'];
$auto_created_date= $maintenanceData['created_date'];

switch ($_GET['type']) {
    case '100':
       $q=$d->select("receive_maintenance_master,maintenance_master,unit_master,block_master,users_master","users_master.unit_id=receive_maintenance_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_maintenance_master.unit_id AND receive_maintenance_master.maintenance_id =maintenance_master.maintenance_id  AND receive_maintenance_master.society_id='$society_id' AND receive_maintenance_master.maintenance_id ='$_GET[maintenance_id]' ");
      break;
    case '0':
       $q=$d->select("receive_maintenance_master,maintenance_master,unit_master,block_master,users_master","users_master.unit_id=receive_maintenance_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_maintenance_master.unit_id AND receive_maintenance_master.maintenance_id =maintenance_master.maintenance_id  AND receive_maintenance_master.society_id='$society_id' AND receive_maintenance_master.maintenance_id ='$_GET[maintenance_id]' AND receive_maintenance_master.receive_maintenance_status=0");
      break;
    case '1':
       $q=$d->select("receive_maintenance_master,maintenance_master,unit_master,block_master,users_master","users_master.unit_id=receive_maintenance_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_maintenance_master.unit_id AND receive_maintenance_master.maintenance_id =maintenance_master.maintenance_id  AND receive_maintenance_master.society_id='$society_id' AND receive_maintenance_master.maintenance_id ='$_GET[maintenance_id]' AND receive_maintenance_master.receive_maintenance_status=1");
    break;
    default:
       $q=$d->select("receive_maintenance_master,maintenance_master,unit_master,block_master,users_master","users_master.unit_id=receive_maintenance_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=receive_maintenance_master.unit_id AND receive_maintenance_master.maintenance_id =maintenance_master.maintenance_id  AND receive_maintenance_master.society_id='$society_id' AND receive_maintenance_master.maintenance_id ='$_GET[maintenance_id]'");
      break;
  }

$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{
    if ($row['receive_maintenance_status']==1) {
        $status= "Paid";
       } else if ($row['receive_maintenance_status']==0) {
        $status = "Unpaid";
       } 
$contents.=$i++.",";
$contents.=$maintenance_name."-".$auto_created_date.",";
$contents.=$row['block_name']."-".$row['unit_name'].",";
$contents.=$row['user_full_name'].",";
$contents.=$row['maintence_amount'].",";
$contents.=$status.",";
$contents.=$row['receive_maintenance_date'].",";
$contents.=$row['auto_created_date']."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Maintenance_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}


// facility Report
if ($report=="Facility") {

$contents="No,Facility,Block Unit,User,Amount,Status,Booking Date,Payment Date\n";
$qb=$d->select("facilities_master","facility_id='$_GET[facility_id]' ");
$facilityData=mysqli_fetch_array($qb);
$facility_name= $facilityData['facility_name'];

if ($_GET['facility_id']=="All") {
     $q=$d->select("facilitybooking_master,facilities_master,unit_master,block_master,users_master","users_master.unit_id=facilitybooking_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=facilitybooking_master.unit_id AND facilitybooking_master.facility_id =facilities_master.facility_id  AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.book_status=1 AND facilitybooking_master.booked_date BETWEEN '$from' AND '$toDate'");
  } else {
     $q=$d->select("facilitybooking_master,facilities_master,unit_master,block_master,users_master","users_master.unit_id=facilitybooking_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=facilitybooking_master.unit_id AND facilitybooking_master.facility_id =facilities_master.facility_id  AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.facility_id ='$_GET[facility_id]' AND facilitybooking_master.book_status=1 AND facilitybooking_master.booked_date BETWEEN '$from' AND '$toDate'");
}


$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{
    if ($row['payment_status']==0) {
        $status= "Paid";
       } else if ($row['payment_status']==1) {
        $status = "Unpaid";
       } 
$contents.=$i++.",";
$contents.=$facility_name.",";
$contents.=$row['block_name']."-".$row['unit_name'].",";
$contents.=$row['user_full_name'].",";
$contents.=$row['receive_amount'].",";
$contents.=$status.",";
$contents.=$row['booked_date'].",";
$contents.=$row['payment_received_date']."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Facility_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}



// Visitor Report
if ($report=="Visitor") {

$contents="No,Visitor,Mobile,Block Unit,User,In Time,Out Time,Status\n";

if ($_GET['block_id']=="All") {
     $q=$d->select("visitors_master,unit_master,block_master,users_master","users_master.unit_id=visitors_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=visitors_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visit_date BETWEEN '$from' AND '$toDate'");
  } else {
     $q=$d->select("visitors_master,unit_master,block_master,users_master","users_master.unit_id=visitors_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=visitors_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.block_id ='$_GET[block_id]' AND visitors_master.visit_date BETWEEN '$from' AND '$toDate'");
}


$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{
    if ($row['visitor_status']==0) {
      $status= "Pending";
     } elseif ($row['visitor_status']==1) {
      $status= "Approved ";
     } elseif ($row['visitor_status']==2) {
      $status= "Entered ";
     } elseif ($row['visitor_status']==3) {
      $status= "Exit ";
     } elseif ($row['visitor_status']==4) {
      $status= "Rejected ";
     } elseif ($row['visitor_status']==6) {
      $status= "Hold";
     } 
$contents.=$i++.",";
$contents.=$row['visitor_name'].",";
$contents.=$row['visitor_mobile'].",";
$contents.=$row['user_full_name'].",";
$contents.=$row['visit_date']."-".$row['visit_time'].",";
$contents.=$row['exit_date']."-".$row['exit_time'].",";
$contents.=$row['block_name']."-".$row['unit_name'].",";
$contents.=$status."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Visitor_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}


// Employee Report
if ($report=="EmployeeAtt") {

$contents="No,Employee,Type,Mobile,In Time,Out Time,Working, Status\n";

if ($_GET['emp_id']=="All") {
     $q=$d->select("employee_master,staff_visit_master","employee_master.emp_id =staff_visit_master.emp_id  AND employee_master.society_id='$society_id' AND staff_visit_master.filter_data BETWEEN '$from' AND '$toDate'");
  } else {

     $q=$d->select("employee_master,staff_visit_master","employee_master.emp_id =staff_visit_master.emp_id  AND employee_master.society_id='$society_id' AND staff_visit_master.filter_data BETWEEN '$from' AND '$toDate' AND staff_visit_master.emp_id='$emp_id'");
}


$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{

    $EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
    $empTypeData=mysqli_fetch_array($EW);

    if ($row['visit_status']==1) {
      $status= "Exit";
     } elseif ($row['visit_status']==0) {
      $status= "In ";
     } 
     $intTime=$row['visit_entry_date_time'];
      $outTime=$row['visit_exit_date_time'];
      $date_a = new DateTime($intTime);
      $date_b = new DateTime($outTime);
      $interval = date_diff($date_a,$date_b);
      $wHours= $interval->format('%h:%i:%s');

$contents.=$i++.",";
$contents.=$row['emp_name'].",";
$contents.=$empTypeData['emp_type_name'].",";
$contents.=$row['emp_mobile'].",";
$contents.=$row['visit_entry_date_time'].",";
$contents.=$row['visit_exit_date_time'].",";
$contents.=$wHours.",";
$contents.=$status."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Employee_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}



// Expencse Report
if ($report=="Expense") {

$contents="No,Expense,Date,Amount,Balancesheet\n";

 if ($_GET['balancesheet_id']=="All") {
     $q=$d->select("balancesheet_master,expenses_balance_sheet_master","balancesheet_master.balancesheet_id =expenses_balance_sheet_master.balancesheet_id  AND expenses_balance_sheet_master.society_id='$society_id' AND  expenses_balance_sheet_master.expenses_add_date BETWEEN '$from' AND '$toDate'");
  } else {
     $q=$d->select("balancesheet_master,expenses_balance_sheet_master","balancesheet_master.balancesheet_id =expenses_balance_sheet_master.balancesheet_id  AND balancesheet_master.society_id='$society_id' AND balancesheet_master.balancesheet_id ='$_GET[balancesheet_id]' AND  expenses_balance_sheet_master.expenses_add_date BETWEEN '$from' AND '$toDate'");
}


$i=1;
//While loop to fetch the records
while($row = mysqli_fetch_array($q))
{


  $contents.=$i++.",";
  $contents.=$row['expenses_title'].",";
  $contents.=$row['expenses_add_date'].",";
  $contents.=$row['expenses_amount'].",";
  $contents.=$row['balancesheet_name']."\n";
}

$contents = strip_tags($contents); 

header("Content-Disposition: attachment; filename=Expenses_Report_".date('Y-m-d-h-i').".csv");
print $contents;

}
?>