<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
  if(isset($_POST['addCompanyAboutUs'])){
   /*  echo "<pre>";
    print_r($_FILES);die; */

     $m->set_data('society_id',$society_id); 
     $m->set_data('company_about_us_top_description',$company_about_us_top_description); 
     $m->set_data('company_about_us_bottom_title',$company_about_us_bottom_title); 
     $m->set_data('company_about_us_bottom_description',$company_about_us_bottom_description); 
     $m->set_data('company_about_us_added_by',$_COOKIE['bms_admin_id']); 
     $m->set_data('company_about_us_created_at', date("Y-m-d H:i:s"));
      
     $a1 = array(
      'society_id'=>$m->get_data('society_id'),
      'company_about_us_top_description'=>$m->get_data('company_about_us_top_description'), 
      'company_about_us_bottom_title'=>$m->get_data('company_about_us_bottom_title'),  
      'company_about_us_bottom_description'=>$m->get_data('company_about_us_bottom_description'),  
      'company_about_us_added_by'=>$m->get_data('company_about_us_added_by'),  
      'company_about_us_created_at'=>$m->get_data('company_about_us_created_at'),  
    );  


    /////////// for top image///////////////////////
     if($_FILES['company_about_us_top_image']['tmp_name'] != '')
     {
        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['company_about_us_top_image']['tmp_name'];
        $ext = pathinfo($_FILES['company_about_us_top_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["company_about_us_top_image"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../companyAboutUs");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/company_about_us/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "about_us_top_image." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "about_us_top_image." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../companyAboutUs");
                        exit;
                        break;
                }
                $image = $newFileName . "about_us_top_image." . $ext;
                $notiUrl = $base_url . 'img/company_about_us/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../companyAboutUs");
                exit();
            }
        }
        $m->set_data('company_about_us_top_image', $image);
        $a1['company_about_us_top_image'] = $m->get_data('company_about_us_top_image');
     }

     /////////// for top image 1///////////////////////
     if($_FILES['company_about_us_image_one']['tmp_name'] != '')
     {
        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['company_about_us_image_one']['tmp_name'];
        $ext = pathinfo($_FILES['company_about_us_image_one']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["company_about_us_image_one"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../companyAboutUs");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/company_about_us/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "about_us_image_one." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "about_us_image_one." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../companyAboutUs");
                        exit;
                        break;
                }
                $image = $newFileName . "about_us_image_one." . $ext;
                $notiUrl = $base_url . 'img/company_about_us/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../companyAboutUs");
                exit();
            }
        }
        $m->set_data('company_about_us_image_one', $image);
        $a1['company_about_us_image_one'] = $m->get_data('company_about_us_image_one');
     }

     /////////// for top image 2///////////////////////
     if($_FILES['company_about_us_image_two']['tmp_name'] != '')
     {
        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['company_about_us_image_two']['tmp_name'];
        $ext = pathinfo($_FILES['company_about_us_image_two']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["company_about_us_image_two"]["size"];
        $maxsize = 20971520;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                    header("location:../companyAboutUs");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/company_about_us/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "about_us_image_two." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "about_us_image_two." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../companyAboutUs");
                        exit;
                        break;
                }
                $image = $newFileName . "about_us_image_two." . $ext;
                $notiUrl = $base_url . 'img/company_about_us/' . $image;
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../companyAboutUs");
                exit();
            }
        }
        $m->set_data('company_about_us_image_two', $image);
        $a1['company_about_us_image_two'] = $m->get_data('company_about_us_image_two');
     }
     
     if(isset($company_about_us_id) && $company_about_us_id>0 ){
        $q=$d->update("company_about_us_master",$a1,"company_about_us_id ='$company_about_us_id'");
        $_SESSION['msg']="Company About Us Updated Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company About Us Updated Successfully");   
     }else{
        $q=$d->insert("company_about_us_master",$a1);
        $_SESSION['msg']="Company About Us Added Successfully";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company About Us Added Successfully");   
     }

     if($q==TRUE) {  
      header("Location: ../companyAboutUs");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../companyAboutUs");
   }

 }



}
?>