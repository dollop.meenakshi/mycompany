<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract($_POST);

$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../../img/$language_id.xml");
$unitLngName = $xml->string->unit;
// add new Notice Board
$base_url=$m->base_url();
/*  ECHO mysqli_real_escape_string($conn,$_POST['notice_description']);
ECHO "<PRE>";print_r($_POST);EXIT;*/

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  // add main menu

  if (isset($_POST['updateUnitName'])) {

      $counts = array_count_values($unit_name);
      foreach ($counts as $name => $count) {
        if ($count>1) {
          $_SESSION['msg1']="Please enter unique $unitLngName name";
          header("location:../units?bId=$bId");
          exit();
        }
      }
      $size = sizeof($unit_id);
      for ($i = 0; $i<$size;$i++){
        if (trim($unit_name[$i])=='') {
          $_SESSION['msg1']="Please enter $unitLngName name";
          header("location:../units?bId=$bId");
          exit();
        }

        $m -> set_data('unit_name',$unit_name[$i]); 
        $a = array(
                'unit_name'=>$m->get_data('unit_name'));
            $unitNameUpdate = $d->update("unit_master",$a,"unit_id='$unit_id[$i]' AND floor_id='$floor_id'"); 
      }
      if ($unitNameUpdate > 0) {
        $_SESSION['msg']="$unitLngName Name Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$unitLngName Updated");
        header("location:../units?bId=$bId");
      }  else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../units?bId=$bId");
      }

    }

  if (isset($_POST['unit_name_single'])) {
      $m->set_data('society_id',$society_id);
      $m->set_data('block_id',$block_id);
      $m->set_data('floor_id',$floor_id);
      $m->set_data('unit_name',$unit_name_single);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_id'=> $m->get_data('block_id'),
        'floor_id'=> $m->get_data('floor_id'),
        'unit_name'=> $m->get_data('unit_name'),
      );

      $q=$d->insert("unit_master",$a);

      if($q>0) {
        $_SESSION['msg']="$unitLngName Added";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$unitLngName Added");
        header("location:../units?bId=$block_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../units?bId=$block_id");
      }

  }

  if(isset($deleteUnit)) {

     $q=$d->delete("unit_master","unit_id='$deleteUnit' AND society_id='$society_id'");
     $q=$d->delete("users_master","unit_id='$deleteUnit' AND society_id='$society_id'");

     if($q>0) {
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$unitLngName Deleted");
      echo 1;
     } else {
      echo 0;
     }
  }

}
else{
  header('location:../login');
 }
 ?>
