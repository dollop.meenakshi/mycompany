<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    //IS_605
    if (isset($_POST['addEmployeeLoan'])) {

        $m->set_data('floor_id', $floor_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('loan_id', $loan_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('loan_date', $loan_date);
        $m->set_data('user_id', $user_id);
        $m->set_data('loan_amount', $loan_amount);
        $m->set_data('loan_emi', $loan_emi);
        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $emi_amount = $loan_amount/$loan_emi;
        $a1 = array('floor_id'=> $m->get_data('floor_id'),
                    'block_id'=> $m->get_data('block_id'),
                    'society_id'=> $m->get_data('society_id'),
                    'user_id'=> $m->get_data('user_id'),
                    'loan_amount'=> $m->get_data('loan_amount'),
                    'loan_emi_amount'=> $emi_amount,
                    'loan_emi'=> $m->get_data('loan_emi'),
                    'created_at'=> $m->get_data('created_at'),
                    'loan_date'=> $m->get_data('loan_date'),
                    'loan_created_by'=> $_COOKIE['bms_admin_id'],
        );
       

        $uSql = $d->selectRow('users_master.*','users_master',"users_master.user_id = '$user_id'");
        $userData =mysqli_fetch_assoc($uSql);
       
          if($loan_id>0){
              $deltloan_emi_master = $d->delete('loan_emi_master',"loan_id =$loan_id");
             
              $q = $d->update("employee_loan_master",$a1,"user_id = '$user_id'");
              for($i=0; $i < $loan_emi; $i++) {
                $emi_date = date('Y-m-d', strtotime("+$i months", strtotime($loan_date)));
                $a2 = array(
                    'loan_id' => $loan_id,
                    'society_id' => $society_id,
                    'user_id' => $user_id,
                    'emi_amount' => $emi_amount,
                    'emi_created_at' => $emi_date,
                );
                $q2=$d->insert("loan_emi_master",$a2);
            }
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", $userData['user_full_name']."'s Loan Updated successfully");
            $_SESSION['msg'] = "Loan Updated successfully";

          } else{
            $q=$d->insert("employee_loan_master",$a1);
            $loan_id = $con->insert_id;
            for($i=0; $i < $loan_emi; $i++) {
                $emi_date = date('Y-m-d', strtotime("+$i months", strtotime($loan_date)));
                $a2 = array(
                    'loan_id' => $loan_id,
                    'society_id' => $society_id,
                    'user_id' => $user_id,
                    'emi_amount' => $emi_amount,
                    'emi_created_at' => $emi_date,
                );
                $q2=$d->insert("loan_emi_master",$a2);
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", $userData['user_full_name']."'s Loan Added successfully");
            $_SESSION['msg'] = "Loan Added successfully";
          }   
        if ($q == true) {
            header("Location: ../employeeLoan");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../employeeLoan");
        }

    }
    if (isset($_POST['payEmi'])) {
        $m->set_data('emi_paid_remark', $emi_paid_remark);
        $m->set_data('user_id', $user_id);
        $m->set_data('loan_id', $loan_id);
        $m->set_data('emi_paid_mode', $emi_paid_mode);
        $m->set_data('received_date', $received_date);
        $m->set_data('emi_paid_date', date("Y-m-d H:i:s"));
        $emi_amount = $loan_amount/$loan_emi;
        $a1 = array( 'emi_paid_remark'=> $m->get_data('emi_paid_remark'),
                    'emi_paid_mode'=> $m->get_data('emi_paid_mode'),
                    'received_date'=> $m->get_data('received_date'),
                    'emi_paid_date'=> $m->get_data('emi_paid_date'),
                    'emi_paid_by'=> $_COOKIE['bms_admin_id'],
                    'is_emi_paid'=> 1,
                    );
        $uSql = $d->selectRow('users_master.*','users_master',"users_master.user_id = '$user_id'");
        $userData =mysqli_fetch_assoc($uSql);
         $q15 = $d->update("loan_emi_master",$a1,"loan_emi_id = '$loan_emi_id'");
         $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", $userData['user_full_name']."'s Loan Emi paid successfully");
        if ($q15 == true) {
            header("Location: ../loanEmiDetail?lId=$loan_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../loanEmiDetail?lId=$loan_id");
        }

    }
}
