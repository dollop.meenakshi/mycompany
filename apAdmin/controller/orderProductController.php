<?php
include '../common/objectController.php';
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addProduct']))
    {
        $m->set_data('society_id', $society_id);
        $m->set_data('product_code', $product_code);
        $m->set_data('product_name', $product_name);
        $m->set_data('product_category_id', $product_category_id);
        $m->set_data('product_sub_category_id', $product_sub_category_id);
        $m->set_data('product_description', $product_description);
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'product_code' => $m->get_data('product_code'),
            'product_name' => $m->get_data('product_name'),
            'product_category_id' => $m->get_data('product_category_id'),
            'product_sub_category_id' => $m->get_data('product_sub_category_id'),
            'product_description' => $m->get_data('product_description'),
            'product_created_date' => date("Y-m-d H:i:s")
        );
        if(isset($product_id) && $product_id > 0)
        {
            $q = $d->update("retailer_product_master",$a1,"product_id = '$product_id'");
            $_SESSION['msg'] = "Product Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Product Successfully Updated.";
                header("Location: ../manageProduct?PCId=$product_category_id&PSCId=$product_sub_category_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../manageProduct?PCId=$product_category_id&PSCId=$product_sub_category_id");exit;
            }
        }
        else
        {
            $q = $d->insert("retailer_product_master", $a1);
            $_SESSION['msg'] = "Product Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Product Successfully Added.";
                header("Location: ../manageProduct?PCId=$product_category_id&PSCId=$product_sub_category_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addProduct");
            }
        }
    }
    elseif(isset($_POST['getSubCatList']))
    {
        $q = $d->selectRow("product_sub_category_id,sub_category_name","product_sub_category_master","product_category_id = '$product_category_id'");
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['deleteProduct']))
    {
        $q = $d->delete("retailer_product_master","product_id = '$product_id'");
        if($q)
        {
            $q1 = $d->selectRow("variant_photo","retailer_product_variant_master","product_id = '$product_id'");
            while($data = $q1->fetch_assoc())
            {
                unlink('../../img/vendor_product/'.$data['variant_photo']);
            }
            $d->delete("retailer_product_variant_master","product_id = '$product_id'");
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product deleted");
            $_SESSION['msg'] = "Product Successfully Delete.";
            header("Location: ../manageProduct?PCId=$product_category_id&PSCId=$product_sub_category_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageProduct?PCId=$product_category_id&PSCId=$product_sub_category_id");exit;
        }
    }
    elseif(isset($_POST['addProductVariant']))
    {
        $file = $_FILES['variant_photo']['tmp_name'];
        if(file_exists($file))
        {
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png',
                'image/webp'
            );
            if(($_FILES['variant_photo']['size'] >= $maxsize) || ($_FILES["variant_photo"]["size"] == 0))
            {
                $_SESSION['msg1']=" Photo too large. File must be less than 1 MB.";
                header("location:../addProductVariant");exit;
            }
            if(!in_array($_FILES['variant_photo']['type'], $acceptable) && (!empty($_FILES["variant_photo"]["type"])))
            {
                $_SESSION['msg1']="Invalid Photo type. Only JPG and PNG types are accepted.";
                header("location:../addProductVariant");exit;
            }
            if(isset($product_id) && $product_id > 0 && $old_photo != "")
            {
                unlink("../../img/vendor_product/".$old_photo);
            }
            $product_name_image = str_replace(' ', '_', $product_name);
            $temp = explode(".", $_FILES["variant_photo"]["name"]);
            $variant_photo = "Product_variant_".$product_name_image."_".rand().'.' . end($temp);
            move_uploaded_file($_FILES["variant_photo"]["tmp_name"], "../../img/vendor_product/".$variant_photo);
        }
        else
        {
            if(isset($old_photo))
            {
                $variant_photo = $old_photo;
            }
            else
            {
                $variant_photo = "";
            }
        }

        if($variant_bulk_type == 1)
        {
            $m->set_data('variant_per_box_piece', $variant_per_box_piece);
        }
        else
        {
            $m->set_data('variant_per_box_piece', 0);
        }

        $m->set_data('product_id', $product_id);
        $m->set_data('product_variant_name', $product_variant_name);
        $m->set_data('variant_bulk_type', $variant_bulk_type);
        $m->set_data('retailer_selling_price', $retailer_selling_price);
        $m->set_data('maximum_retail_price', $maximum_retail_price);
        $m->set_data('menufacturing_cost', $menufacturing_cost);
        $m->set_data('sku', $sku);
        $m->set_data('weight_in_kg', $weight_in_kg);
        $m->set_data('variant_description', $variant_description);
        $m->set_data('variant_photo', $variant_photo);
        $m->set_data('unit_measurement_id', $unit_measurement_id);
        $a1 = array(
            'product_id' => $m->get_data('product_id'),
            'product_variant_name' => $m->get_data('product_variant_name'),
            'variant_bulk_type' => $m->get_data('variant_bulk_type'),
            'variant_per_box_piece' => $m->get_data('variant_per_box_piece'),
            'retailer_selling_price' => $m->get_data('retailer_selling_price'),
            'maximum_retail_price' => $m->get_data('maximum_retail_price'),
            'menufacturing_cost' => $m->get_data('menufacturing_cost'),
            'sku' => $m->get_data('sku'),
            'weight_in_kg' => $m->get_data('weight_in_kg'),
            'variant_description' => $m->get_data('variant_description'),
            'variant_photo' => $m->get_data('variant_photo'),
            'unit_measurement_id' => $m->get_data('unit_measurement_id'),
            'created_date' => date("Y-m-d H:i:s"),
            'variant_created_by' => $_COOKIE['bms_admin_id'],
        );
        $get_cat = $d->selectRow("product_category_id","retailer_product_master","product_id = '$product_id'");
        $fetch = $get_cat->fetch_assoc();
        $product_category_id = $fetch['product_category_id'];
        if(isset($product_variant_id) && $product_variant_id > 0)
        {
            $q = $d->update("retailer_product_variant_master",$a1,"product_variant_id = '$product_variant_id'");
            $_SESSION['msg'] = "Product Variant Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Variant Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Product Variant Successfully Updated.";
                header("Location: ../manageProductVariant?PCId=$product_category_id&PId=$product_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../manageProductVariant?PCId=$product_category_id&PId=$product_id");exit;
            }
        }
        else
        {
            $q = $d->insert("retailer_product_variant_master", $a1);
            $_SESSION['msg'] = "Product Variant Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Variant Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Product Variant Successfully Added.";
                header("Location: ../manageProductVariant?PCId=$product_category_id&PId=$product_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addProductVariant");
            }
        }
    }
    elseif(isset($_POST['deleteProductVariant']))
    {
        if($variant_photo != "")
        {
            unlink("../../img/vendor_product/".$variant_photo);
        }
        $q = $d->delete("retailer_product_variant_master","product_variant_id = '$product_variant_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant deleted");
            $_SESSION['msg'] = "Product Variant Successfully Delete.";
            header("Location: ../manageProductVariant?PCId=$product_category_id&PId=$product_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageProductVariant?PCId=$product_category_id&PId=$product_id");exit;
        }
    }
    elseif(isset($_POST['getProductList']))
    {
        $q = $d->selectRow("product_id,product_name","retailer_product_master","product_category_id = '$product_category_id'");
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['changeOrderStatus']))
    {
        $m->set_data('order_status', $order_status);
        $a1 = array(
            'order_status' => $m->get_data('order_status')
        );
        $q = $d->update("retailer_order_master",$a1,"retailer_order_id = '$retailer_order_id'");
        if($q)
        {
            $_SESSION['msg'] = "Order Status Successfully Updated.";
            echo 1;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            echo 0;
        }
    }
    elseif(isset($_POST['getOrderList']))
    {
        if(isset($_POST['date_from']) && !empty($_POST['date_from']) && $_POST['date_from'] != "0000-00-00 00:00:00")
        {
            $date_from = $_POST['date_from'];
        }
        else
        {
            $date_from = date("Y-m-d");
        }

        if(isset($_POST['date_to']) && !empty($_POST['date_to']) && $_POST['date_to'] != "0000-00-00 00:00:00")
        {
            $date_to = $_POST['date_to'];
        }
        else
        {
            $date_to = date("Y-m-d");
        }
        $append = "";
        $append .= "rom.order_date BETWEEN '$date_from 00:00:01' AND '$date_to 23:59:59'";
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $append .= " AND rom.order_by_user_id = '$user_id'";
        }
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            $append .= " AND rom.retailer_id = '$retailer_id'";
        }
        if(isset($distributor_id) && !empty($distributor_id) && $distributor_id != 0)
        {
            $append .= " AND rom.distributor_id = '$distributor_id'";
        }
        if(isset($order_status) && $order_status != "")
        {
            $append .= " AND rom.order_status = '$order_status'";
        }
        if(isset($area_id) && !empty($area_id) && $area_id != "")
        {
            $append .= " AND rm.area_id = '$area_id'";
        }
        if(isset($product_id) && !empty($product_id) && $product_id != "")
        {
            $append .= " AND ropm.product_id = '$product_id'";
        }
        $q = $d->selectRow("rom.*,rm.retailer_name,dm.distributor_name,um.user_full_name,amn.area_name","retailer_order_master AS rom JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id JOIN distributor_master AS dm ON dm.distributor_id = rom.distributor_id JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_order_product_master AS ropm ON ropm.retailer_order_id = rom.retailer_order_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id",$append,"GROUP BY rom.retailer_order_id ORDER BY rom.order_date DESC");
        $iNo = 1;
        $rtData = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getProductSalesListReport']))
    {
        if(isset($_POST['date_from']) && !empty($_POST['date_from']) && $_POST['date_from'] != "0000-00-00 00:00:00")
        {
            $date_from = $_POST['date_from'];
        }
        else
        {
            $date_from = date("Y-m-d");
        }

        if(isset($_POST['date_to']) && !empty($_POST['date_to']) && $_POST['date_to'] != "0000-00-00 00:00:00")
        {
            $date_to = $_POST['date_to'];
        }
        else
        {
            $date_to = date("Y-m-d");
        }
        $append = "";
        $append .= "rom.order_date BETWEEN '$date_from 00:00:01' AND '$date_to 23:59:59'";
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $append .= " AND rom.order_by_user_id = '$user_id'";
        }
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            $append .= " AND rom.retailer_id = '$retailer_id'";
        }
        if(isset($distributor_id) && !empty($distributor_id) && $distributor_id != 0)
        {
            $append .= " AND rom.distributor_id = '$distributor_id'";
        }
        if(isset($order_status) && $order_status != "")
        {
            $append .= " AND rom.order_status = '$order_status'";
        }
        if(isset($area_id) && !empty($area_id) && $area_id != "")
        {
            $append .= " AND rm.area_id = '$area_id'";
        }
        if(isset($product_id) && !empty($product_id) && $product_id != "")
        {
            $append .= " AND ropm.product_id = '$product_id'";
        }
        $q = $d->selectRow("rom.order_placed_via,rom.order_latitude,rom.order_longitude,rom.order_remark,rom.order_status,rom.order_route_name,rom.retailer_order_id,rom.order_date,rom.created_date,rm.retailer_name,rm.retailer_contact_person_country_code,rm.retailer_contact_person_number,rm.retailer_address,dm.distributor_name,um.user_full_name,amn.area_name,pcm.category_name,rpm.product_name,rpvm.product_variant_name,rpvm.sku,ropm.total_weight_in_kg,rpvm.variant_packing_type,ropm.order_packing_type,ropm.product_quantity,ropm.no_of_box,ropm.order_per_box_piece,ropm.product_price,ropm.product_total_price","retailer_order_product_master AS ropm JOIN retailer_order_master AS rom ON rom.retailer_order_id = ropm.retailer_order_id JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id JOIN retailer_product_variant_master AS rpvm ON rpvm.product_id = rpm.product_id JOIN distributor_master AS dm ON dm.distributor_id = rom.distributor_id JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id",$append,"GROUP BY rom.retailer_order_id ORDER BY rom.order_date DESC");
        $iNo = 1;
        $rtData = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['product_variant'] = $row['product_name'] . " " . $row['product_variant_name'];
            $data['retailer_num'] = $row['retailer_contact_person_country_code'] . " " . $row['retailer_contact_person_number'];
            $data['o_date'] = date("Y-m-d",strtotime($row['order_date']));
            $data['o_time'] = date("H:i:s",strtotime($row['order_date']));
            $data['s_date'] = date("Y-m-d",strtotime($row['created_date']));
            $data['s_time'] = date("H:i:s",strtotime($row['created_date']));
            $data['variant_packing_type'] = $row['variant_packing_type'];
            $data['case'] = $row['no_of_box'] . " ( " . $row['order_per_box_piece'] . " )";
            if($row['order_packing_type'] == 0)
            {
                $data['unit'] = $row['product_quantity'];
            }
            else
            {
                $mul = $row['no_of_box'] * $row['order_per_box_piece'];
                $data['unit'] = $row['product_quantity'] - $mul;
            }
            $data['total_weight_in_kg'] = ($row['total_weight_in_kg'] == 0.00) ? "" : $row['total_weight_in_kg'];
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getFilterData']))
    {
        $appendQuery = "";
        $appendQuery2 = "";
        $response['total_orders'] = "";
        $response['total_sales'] = "";
        $response['total_quantity'] = "";
        $response['total_visits'] = "";
        $response['total_visit_duration'] = "";
        $response['productivity'] = "";
        if(isset($filter_date) && !empty($filter_date) && $filter_date != '')
        {
            $appendQuery = " AND DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$filter_date'";
            $appendQuery2 = " AND DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') = '$filter_date'";
        }
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $totalAmount = $d->sum_data("order_total_amount","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","um.level_id = '$user_id' AND rom.order_status = '1' AND rom.society_id = '$society_id' $appendQuery");
            $totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master AS rdvtm JOIN users_master AS um ON um.user_id = rdvtm.retailer_visit_by_id","um.level_id = '$user_id' $appendQuery2");
            $totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","um.level_id = '$user_id' AND rom.order_status = 1 AND rom.society_id = '$society_id' $appendQuery");
            $totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master AS rdvtm JOIN users_master AS um ON um.user_id = rdvtm.retailer_visit_by_id","um.level_id = '$user_id' AND rdvtm.retailer_visit_start_datetime IS NOT NULL AND rdvtm.retailer_visit_end_datetime IS NOT NULL AND rdvtm.order_taken != 0 $appendQuery2");
            $totalOrders = $d->count_data_direct("retailer_order_id","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","um.level_id = '$user_id'".$appendQuery);

            $today_date = date("Y-m-d");
            $get_product = $d->selectRow("SUM(rom.order_total_amount) AS total_amount,um.user_full_name,SUM(rom.total_order_product_qty) AS total_qty,COUNT(rom.order_by_user_id) AS total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","DATE(rom.order_date) = '$today_date' AND um.level_id != 0 AND um.level_id IS NOT NULL AND um.level_id != '' AND um.level_id = '$user_id'","GROUP BY rom.order_by_user_id ORDER BY total_amount DESC");
            $order_value_today = [];
            $user_name_today = [];
            $response['ostd'] = [];
            $response['order_value_today_str'] = '';
            $response['user_name_today_str'] = '';
            while($row = $get_product->fetch_assoc())
            {
                $response['ostd'][] = $row;
                $order_value_today[] = $row['total_amount'];
                $user_name_today[] = $row['user_full_name'];
            }
            $response['order_value_today_str'] = implode(",",$order_value_today);
            $response['user_name_today_str'] = "'" .  implode("','",$user_name_today) . "'";
        }
        else
        {
            $totalAmount = $d->sum_data("order_total_amount","retailer_order_master AS rom","rom.order_status = '1' AND rom.society_id = '$society_id' $appendQuery");
            $totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master AS rdvtm","1=1" . $appendQuery2);
            $totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master AS rom","rom.order_status = 1 AND rom.society_id = '$society_id' $appendQuery");
            $totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master AS rdvtm","rdvtm.retailer_visit_start_datetime IS NOT NULL AND rdvtm.retailer_visit_end_datetime IS NOT NULL AND rdvtm.order_taken != 0 $appendQuery2");
            $totalOrders = $d->count_data_direct("retailer_order_id","retailer_order_master AS rom","1=1".$appendQuery);
        }
        $response['total_orders'] = $totalOrders.'';
        if ($totalOrders > 0 && $totalVisits > 0)
        {
            $totalProductivity = ($totalOrders * 100) / $totalVisits;
        }
        else
        {
            $totalProductivity = 0;
        }

        $totalAmountData = mysqli_fetch_array($totalAmount);
        if ($totalAmountData['SUM(order_total_amount)'] != '')
        {
            $response['total_sales'] = $totalAmountData['SUM(order_total_amount)'].'';
        }
        else
        {
            $response['total_sales'] = "0";
        }

        $totalQuantityData=mysqli_fetch_array($totalQuantity);
        if ($totalQuantityData['SUM(total_order_product_qty)'] != '')
        {
            $response['total_quantity'] = $totalQuantityData['SUM(total_order_product_qty)'].'';
        }
        else
        {
            $response['total_quantity'] = "0";
        }

        $totalDurationData = mysqli_fetch_array($totalDuration);
        $totalSecounds = $totalDurationData['SUM(retailer_visit_duration)'].'';
        if($totalSecounds == "" || empty($totalSecounds))
        {
            $totalVisitDuration = gmdate("H:i:s", 0);
        }
        else
        {
            $totalVisitDuration = gmdate("H:i:s", $totalSecounds);
        }
        $response['total_visits'] = $totalVisits.'';
        $response['total_visit_duration'] = $totalVisitDuration.'';
        $response['productivity'] = number_format($totalProductivity,2,'.','').'';
        echo json_encode($response);exit;
    }
    elseif(isset($_POST['getNoOrderList']))
    {
        if(isset($_POST['date_from']) && !empty($_POST['date_from']) && $_POST['date_from'] != "0000-00-00 00:00:00")
        {
            $date_from = $_POST['date_from'];
        }
        else
        {
            $date_from = date("Y-m-d");
        }

        if(isset($_POST['date_to']) && !empty($_POST['date_to']) && $_POST['date_to'] != "0000-00-00 00:00:00")
        {
            $date_to = $_POST['date_to'];
        }
        else
        {
            $date_to = date("Y-m-d");
        }
        $append = "";
        $append .= "rnom.no_order_created_date BETWEEN '$date_from 00:00:01' AND '$date_to 23:59:59'";
        if(isset($user_id) && !empty($user_id) && $user_id != 0)
        {
            $append .= " AND rnom.visit_by_user_id = '$user_id'";
        }
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            $append .= " AND rnom.retailer_id = '$retailer_id'";
        }
        $q = $d->selectRow("rnom.*,um.user_full_name,rm.retailer_name,amn.area_name,c.city_name,bm.block_name,fm.floor_name","retailer_no_order_master AS rnom JOIN users_master AS um ON um.user_id = rnom.visit_by_user_id JOIN retailer_master AS rm ON rm.retailer_id = rnom.retailer_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN cities AS c ON c.city_id = rm.city_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id",$append,"ORDER BY rnom.retailer_no_order_id DESC");
        $iNo = 1;
        $rtData = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['user_name'] = [
                'user_full_name' => $row['user_full_name'],
                'block_name' => $row['block_name'],
                'floor_name' => $row['floor_name']
            ];
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
}
?>