<?php
include '../common/objectController.php';

$lsnContent = $_POST['lesson_text'];
extract($_POST);

if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    //IS_605
    if (isset($_POST['addCourseLesson'])) {
        $m->set_data('society_id', $society_id);
        $m->set_data('course_chapter_id', $course_chapter_id);
      
        $m->set_data('lesson_video_link', test_input($lesson_video_link));
        $m->set_data('lesson_text', $lsnContent);
        $m->set_data('lesson_is_file_or_link', $lesson_is_file_or_link);
        $m->set_data('lesson_title', $lesson_title);
        $m->set_data('lesson_type_id', $lesson_type_id);
        $m->set_data('lesson_created_date', date("Y-m-d H:i:s"));
        $m->set_data('lesson_created_by', $_COOKIE['bms_admin_id']);

        
        $file = $_FILES['lesson_video']['tmp_name'];

        if (file_exists($file)) {
            if($lesson_type_id==2){
                $extensionResume = array( "pdf", "PDF" );  
            }else if($lesson_type_id==3){
                $extensionResume = array("txt", "TXT", "doc", "DOC", "docx", "DOCX");
            }else if($lesson_type_id==4){

                $extensionResume = array("mp4", "MP4");
            }
            else if($lesson_type_id==6){

                $extensionResume = array( "mp3", "MP3");
            }
            $extensionResumeSprt = implode(',',$extensionResume);
           
            $extId = pathinfo($_FILES['lesson_video']['name'], PATHINFO_EXTENSION);
            $errors     = array();
            $maxsize    = 26214400;
            if (($_FILES['lesson_video']['size'] >= $maxsize) || ($_FILES["lesson_video"]["size"] == 0)) {
                $_SESSION['msg1'] = "Lesson File too large. Must be less than 32 MB.";
                header("location:../courseChapter");
                exit();
            }
            if (!in_array($extId, $extensionResume) && (!empty($_FILES["lesson_video"]["type"]))) {
               
                $_SESSION['msg1'] = "Invalid File format, Only ".$extensionResumeSprt." is allowed.";
                header("location:../courseChapter");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['lesson_video'];
                $temp = explode(".", $_FILES["lesson_video"]["name"]);
                $lesson_video = $course_chapter_id . '_id_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["lesson_video"]["tmp_name"], "../../img/course/" . $lesson_video);
            }
        } else {
            $lesson_video = "";
        }
        
        /////////////////////////////////////////////////////
        if ($total_video_duration>0) {
            $total_video_duration = $total_video_duration*60;
        }

        $ChapterSql = $d->selectRow('course_chapter_master.*', 'course_chapter_master', "course_chapter_id=$course_chapter_id");
        $chapterData = mysqli_fetch_assoc($ChapterSql);
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'lesson_title' => $m->get_data('lesson_title'),
            'lesson_video_link' => $m->get_data('lesson_video_link'),
            'lesson_video' => $m->get_data('lesson_video'),
            'lesson_text' => $m->get_data('lesson_text'),
            'lesson_video' => $lesson_video,
            'course_id' => $chapterData['course_id'],
            'course_chapter_id' => $m->get_data('course_chapter_id'),
            'lesson_created_by' => $m->get_data('lesson_created_by'),
            'lesson_type_id' => $m->get_data('lesson_type_id'),
            'lesson_created_date' => $m->get_data('lesson_created_date'),
            'total_video_duration' => $total_video_duration,
        );

        if ($lesson_type_id == 2 || $lesson_type_id == 3 || $lesson_type_id == 4 || $lesson_type_id == 6) {
            if ($lesson_video != "") {
                $a1['lesson_video'] = $lesson_video;
            } else {
                $a1['lesson_video'] = $lesson_video_old;
            }
            $a1['lesson_text']="";
            
            $a1['lesson_video_link']="";
            $a1['lesson_is_file_or_link']=1;

        } else if ($lesson_type_id == 7) {
            //////text
            $a1['lesson_text']=$m->get_data('lesson_text');
            $a1['lesson_video'] = "";
            $a1['lesson_video_link']="";
        } else {
            /////Link
            $a1['lesson_text']="";
            $a1['lesson_video'] = "";
            $a1['lesson_video_link']=$m->get_data('lesson_video_link');
        }
        if (isset($lessons_id) && $lessons_id > 0) {

            $q = $d->update("course_lessons_master", $a1, "lessons_id ='$lessons_id'");
            $_SESSION['msg'] = "Course Lesson Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Lesson Updated Successfully");
        } else {

            $q = $d->insert("course_lessons_master", $a1);
            $_SESSION['msg'] = "Course Lesson Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Lesson Added Successfully");
        }
        if ($q == true) {
            header("Location: ../courseChapter?cId=$chapterData[course_id]");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../courseChapter?cId=$chapterData[course_id]");
        }
    }
}
