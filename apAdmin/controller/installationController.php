<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{


  if (isset($_POST['addMember'])) {

       $qcb=$d->select("installation_team_master","mobile_number='$mobile_number'");
       $dataqcb=mysqli_fetch_array($qcb);
       $qcb1=$d->select("society_master","");
       $dataqcb1=mysqli_fetch_array($qcb1);
      if($full_name=='') {
          $_SESSION['msg1']="Full Name Field Is Required";
          header("location:../installationTeam");
          exit();
       }else if(strlen($full_name)<'2' || strlen($full_name)>'50') {
          $_SESSION['msg1']="Full Name Length Must Be Between 2 To 50 Characters Long";
          header("location:../installationTeam");
          exit();
       }else if(!is_numeric($mobile_number)){
          $_SESSION['msg1']="Mobile Number Must Contain Only Numeric Value";
          header("location:../installationTeam");
          exit();
       }else if(strlen(trim($mobile_number))!='10'){
          $_SESSION['msg1']="Mobile Number Length Must Be Equal To 10";
          header("location:../installationTeam");
          exit();
       }else if($dataqcb>0) {
          $_SESSION['msg1']="This Mobile Number Is Already Registered";
          header("location:../installationTeam");
          exit();
       }else if(strlen($password)<'8' || strlen($password)>'30'){
          $_SESSION['msg1']="Password Length Must Be Between 8 To 30 Digit Long";
          header("location:../installationTeam");
          exit();
       }else{}

        $m->set_data('society_id',$society_id);
        $m->set_data('full_name',$full_name);
        $m->set_data('mobile_number',$mobile_number);
        $m->set_data('password',$password);
        $a3 =array(
          'society_id'=> $m->get_data('society_id'),
          'full_name'=>$m->get_data('full_name'),
          'mobile_number'=>$m->get_data('mobile_number'),
          'password'=>$m->get_data('password'),
          'created_date'=>date("Y-m-d"),
        );
        

         $q=$d->insert("installation_team_master",$a3);
         if($q>0) {
          
            
            $_SESSION['msg']="New Team Member Added";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","New Installation Team Member Added");
            header("location:../installationTeam");
          } else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../installationTeam");
          }
  }


  
  if (isset($_POST['editMember'])) {

       $qcb=$d->select("installation_team_master","mobile_number='$member_mobile_number' AND installation_team_id!='$member_id'");
       $dataqcb=mysqli_fetch_array($qcb);
       $qcb1=$d->select("society_master","");
       $dataqcb1=mysqli_fetch_array($qcb1);
       if($member_full_name=='') {
          $_SESSION['msg1']="Full Name Field Is Required";
          header("location:../installationTeam");
          exit();
       }else if(strlen($member_full_name)<'2' || strlen($member_full_name)>'50') {
          $_SESSION['msg1']="Full Name Length Must Be Between 2 To 50 Characters Long";
          header("location:../installationTeam");
          exit();
       }else if(!is_numeric($member_mobile_number)){
          $_SESSION['msg1']="Mobile Number Must Contain Only Numeric Value";
          header("location:../installationTeam");
          exit();
       }else if(strlen(trim($member_mobile_number))!='10'){
          $_SESSION['msg1']="Mobile Number Length Must Be Equal To 10";
          header("location:../installationTeam");
          exit();
       }else if($dataqcb>0) {
          $_SESSION['msg1']="This Mobile Number Is Already Registered";
          header("location:../installationTeam");
          exit();
       }else{}


        $m->set_data('full_name',$member_full_name);
        $m->set_data('mobile_number',$member_mobile_number);
        $m->set_data('installation_team_id',$member_id);
        $m->set_data('society_id',$member_society_id);

        $a3 =array(
          'full_name'=>$m->get_data('full_name'),
          'mobile_number'=>$m->get_data('mobile_number'),
          'society_id'=>$m->get_data('society_id'),
        );
        

         $q=$d->update("installation_team_master",$a3,"installation_team_id='$member_id'");
         if($q>0) {

          
            $_SESSION['msg']=" Member Data Update";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Installation Team Data Update");

            
            header("location:../installationTeam");
          } else {
            $_SESSION['msg1']="Something Wrong";
            header("location:../installationTeam");
          }
  }


// forgot Password 
if(isset($_POST["forgot_mobile_installation_team"])) {
  extract(array_map("test_input" , $_POST));

  $q=$d->select("installation_team_master","mobile_number='$forgot_mobile_installation_team'");
  $data = mysqli_fetch_array($q); 
  extract($data);
  if ($data > 0) {
    $admin_name = $data['full_name'];
    $admin_mobile = $data['mobile_number'];
    $bytes = openssl_random_pseudo_bytes(4);
    $password = bin2hex($bytes);

    $m->set_data('password',$password);
    $a1= array (
      'password'=> $m->get_data('password'),
    );
    $insert=$d->update('installation_team_master',$a1,"mobile_number='$forgot_mobile_installation_team'"); 
   
  
   
        // Redirqt to homepage
    $_SESSION['msg']= "New Password Send on Mobile Number";
    header("location:../installationTeam");
  } 
  else {
    $_SESSION['msg1']= "Wrong Mobile";
    header("location:../installationTeam");
  }
}




  if (isset($_POST['installation_team_id_delete'])) {

     $q=$d->delete("installation_team_master","installation_team_id='$installation_team_id_delete'");
     if ($q>0) {
        $_SESSION['msg']="Member Deleted";
        header("location:../installationTeam");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../installationTeam");
      }

    # code...
  }
 

}
else{
  header('location:../login');
 }
 ?>
