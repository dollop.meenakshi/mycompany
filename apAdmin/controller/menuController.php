<?php
include '../common/objectController.php';

if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    // add main menu
    if (isset($_POST['menu_name']))
    {
        $m->set_data('menu_name', test_input($menu_name));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('menu_icon', test_input($menu_icon));
        $m->set_data('sub_menu', test_input($sub_menu));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('created_by', $created_by);
        $a = array(
            'menu_name' => $m->get_data('menu_name') ,
            'menu_link' => $m->get_data('menu_link') ,
            'menu_icon' => $m->get_data('menu_icon') ,
            'sub_menu' => $m->get_data('sub_menu') ,
            'status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'created_by' => $m->get_data('created_by') ,
        );
        $q = $d->insert("master_menu", $a);
        if ($q > 0)
        {
            // create new file for new menu url
            if (!file_exists("../" . $menu_link . ".php") && $sub_menu == 0)
            {
                $myfile = fopen("../" . $menu_link . ".php", "w");
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Menu Added");
            $_SESSION['msg'] = "New menu successfully  added.";
            header("location:../mainMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../menu");
        }
    }
    // Edit main menu
    if (isset($_POST['menu_nameEdit']))
    {
        $m->set_data('menu_nameEdit', test_input($menu_nameEdit));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('menu_icon', test_input($menu_icon));
        $m->set_data('sub_menu', test_input($sub_menu));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('updated_by', $updated_by);
        $a = array(
            'menu_name' => $m->get_data('menu_nameEdit') ,
            'menu_link' => $m->get_data('menu_link') ,
            'menu_icon' => $m->get_data('menu_icon') ,
            'sub_menu' => $m->get_data('sub_menu') ,
            'status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'updated_by' => $m->get_data('updated_by') ,
        );
        // print_r($a);
        $q = $d->update("master_menu", $a, "menu_id='$menu_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Updated Menu");
            $_SESSION['msg'] = " Menu Successfully  Updated.";
            header("location:../mainMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../menu");
        }
    }
    // add sub menu
    if (isset($_POST['SubmenuAdd']))
    {
        $m->set_data('parent_menu_id', test_input($parent_menu_id));
        $m->set_data('menu_name', test_input($sub_menu_name));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('created_by', $created_by);
        $a = array(
            'parent_menu_id' => $m->get_data('parent_menu_id') ,
            'menu_name' => $m->get_data('menu_name') ,
            'menu_link' => $m->get_data('menu_link') ,
            'status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'created_by' => $m->get_data('created_by') ,
        );
        $q = $d->insert("master_menu", $a);
        if ($q > 0)
        {
            // create new file
            if (!file_exists("../" . $menu_link . ".php"))
            {
                $myfile = fopen("../" . $menu_link . ".php", "w");
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Menu Added");
            $_SESSION['msg'] = "New sub menu successfully  added.";
            header("location:../subMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../addSubMenu");
        }
    }
    // Edit sub menu
    if (isset($_POST['SubmenuEdit']))
    {
        $m->set_data('parent_menu_id', test_input($parent_menu_id));
        $m->set_data('menu_name', test_input($sub_menu_name));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('updated_by', $updated_by);
        $a = array(
            'parent_menu_id' => $m->get_data('parent_menu_id') ,
            'menu_name' => $m->get_data('menu_name') ,
            'menu_link' => $m->get_data('menu_link') ,
            'status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'updated_by' => $m->get_data('updated_by') ,
        );
        $q = $d->update("master_menu", $a, "menu_id='$SubmenuEdit'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Menu Updated.");
            $_SESSION['msg'] = "Sub Menu Updated.";
            header("location:../subMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../addSubMenu");
        }
    }
    // add new icon
    if (isset($_POST['icon_name']))
    {
        $m->set_data('icon_name', test_input($icon_name));
        $m->set_data('icon_class', test_input($icon_class));
        $m->set_data('status', $status);
        $m->set_data('created_by', $created_by);
        $a = array(
            'icon_name' => $m->get_data('icon_name') ,
            'icon_class' => $m->get_data('icon_class') ,
            'status' => $m->get_data('status') ,
            'created_by' => $m->get_data('created_by') ,
        );
        $q = $d->insert("icons", $a);
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Icon Added.");
            $_SESSION['msg'] = "New Icon successfully  added.";
            header("location:../icons");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../icons");
        }
    }
    // delete Icon
    if (isset($_POST['deleteIcon']))
    {
        $q = $d->delete("icons", "icon_id='$icon_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Icon Deleted.");
            $_SESSION['msg'] = "Icon Deleted  successfully.";
            header("location:../icons");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../icons");
        }
    }
    // delete Main Menu
    if (isset($_POST['menu_id_delete']))
    {
        $q = $d->delete("master_menu", "menu_id='$menu_id_delete'");
        $q = $d->delete("master_menu", "parent_menu_id='$menu_id_delete'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Menu Deleted.");
            $_SESSION['msg'] = "Menu Deleted  successfully.";
            header("location:../mainMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../mainMenu");
        }
    }
    // Add user Role
    if (isset($_POST['role_name']))
    {
        $qD = $d->select("role_master", "role_name='$role_name'");

        if (mysqli_num_rows($qD))
        {
            $_SESSION['msg1'] = "This role already added !";
            header("location:../role");
            exit();
        }

        if (isset($_POST['menu_id']))
        {
            $menu_id = implode(",", $_POST['menu_id']);
        }
        if (isset($_POST['pagePrivilege']))
        {
            $pagePrivilege = implode(",", $_POST['pagePrivilege']);
        }
        if (isset($_POST['notificationAccess']))
        {
            $notificationAccess = implode(",", $_POST['notificationAccess']);
        }
        if (isset($_POST['sub_menu_id']))
        {
            $sub_menu_id = implode(",", $_POST['sub_menu_id']);
        }

        $m->set_data('society_id', $society_id);
        $m->set_data('role_name', test_input($role_name));
        $m->set_data('role_description', test_input($role_description));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('menu_id', $menu_id);
        $m->set_data('pagePrivilege', $pagePrivilege);
        $m->set_data('notificationAccess', $notificationAccess);
        $m->set_data('created_by', $created_by);
        $a = array(
            'society_id' => $m->get_data('society_id') ,
            'role_name' => $m->get_data('role_name') ,
            'role_description' => $m->get_data('role_description') ,
            'role_status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'menu_id' => $m->get_data('menu_id') ,
            'pagePrivilege' => $m->get_data('pagePrivilege') ,
            'notificationAccess' => $m->get_data('notificationAccess') ,
            'created_by' => $m->get_data('created_by') ,
        );
        $q = $d->insert("role_master", $a);
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Role Type Added.");
            $_SESSION['msg'] = "New Role Successfully  Added.";
            header("location:../roleType");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../roleType");
        }
    }
    // Edit user Role
    if (isset($_POST['role_nameEdit']))
    {
        $qD = $d->select("role_master", "role_name='$role_nameEdit' AND role_id!='$role_id'");

        if (mysqli_num_rows($qD))
        {
            $_SESSION['msg1'] = "This role already added !";
            header("location:../roleType");
            exit();
        }

        if (isset($_POST['menu_id']))
        {
            $menu_id = implode(",", $_POST['menu_id']);
        }
        if (isset($_POST['pagePrivilege']))
        {
            $pagePrivilege = implode(",", $_POST['pagePrivilege']);
        }
        if (isset($_POST['notificationAccess']))
        {
            $notificationAccess = implode(",", $_POST['notificationAccess']);
        }

        $m->set_data('society_id', $societyId);
        $m->set_data('role_nameEdit', test_input($role_nameEdit));
        $m->set_data('role_description', test_input($role_description));
        $m->set_data('status', $status);
        $m->set_data('order_no', test_input($order_no));
        $m->set_data('menu_id', test_input($menu_id));
        $m->set_data('pagePrivilege', $pagePrivilege);
        $m->set_data('notificationAccess', $notificationAccess);
        // $m->set_data('parent_menu_id',$parent_menu_id);
        $m->set_data('updated_by', $updated_by);
        $a = array(
            'society_id' => $m->get_data('society_id') ,
            'role_name' => $m->get_data('role_nameEdit') ,
            'role_description' => $m->get_data('role_description') ,
            'role_status' => $m->get_data('status') ,
            'order_no' => $m->get_data('order_no') ,
            'menu_id' => $m->get_data('menu_id') ,
            'pagePrivilege' => $m->get_data('pagePrivilege') ,
            'notificationAccess' => $m->get_data('notificationAccess') ,
            // 'parent_menu_id'=>$m->get_data('parent_menu_id'),
            'updated_by' => $m->get_data('updated_by') ,
        );
        $q = $d->update("role_master", $a, "role_id='$role_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Role Type Updated");
            $_SESSION['msg'] = "Role Updated Successfully .";
            header("location:../roleType");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../roleType");
        }
    }
    // Delete Role Type
    if (isset($_POST['role_id_delete']))
    {
        $q = $d->delete("role_master", "role_id='$role_id_delete'");

        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Role Type Deleted.");
            $_SESSION['msg'] = "Role Type Deleted successfully.";
            header("location:../roleType");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../roleType");
        }
    }
    //Page Privilege
    if (isset($_POST['pagePri']))
    {
        $pagePrivilege = implode(",", $_POST['pagePrivilege']);
        $m->set_data('pagePrivilege', $pagePrivilege);
        $a = array(
            'pagePrivilege' => $m->get_data('pagePrivilege') ,
        );
        $q = $d->update("role_master", $a, "role_id='$role_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PAge Previvileges Updated");
            $_SESSION['msg'] = "Privileges Updated Successfully";
            header("location:../roleType");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../roleType");
        }
    }
    //Pages
    if (isset($_POST['addPage']))
    {
        $m->set_data('parent_menu_id', test_input($parent_menu_id));
        $m->set_data('menu_name', test_input($sub_menu_name));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('status', $status);
        $m->set_data('page_status', "1");
        $m->set_data('created_by', $created_by);
        $a = array(
            'parent_menu_id' => $m->get_data('parent_menu_id') ,
            'menu_name' => $m->get_data('menu_name') ,
            'menu_link' => $m->get_data('menu_link') ,
            'page_status' => $m->get_data('page_status') ,
            'status' => $m->get_data('status') ,
            'created_by' => $m->get_data('created_by') ,
        );
        $q = $d->insert("master_menu", $a);
        if ($q > 0)
        {
            // create new file
            if (!file_exists("../" . $menu_link . ".php"))
            {
                $myfile = fopen("../" . $menu_link . ".php", "w");
            }
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "PageAdded");
            $_SESSION['msg'] = "New Page successfully  added.";
            header("location:../pages");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pages");
        }
    }
    // Edit Pages
    if (isset($_POST['pagesEdit']))
    {
        $m->set_data('parent_menu_id', test_input($parent_menu_id));
        $m->set_data('sub_menu_name', test_input($sub_menu_name));
        $m->set_data('menu_link', test_input($menu_link));
        $m->set_data('status', $status);
        $m->set_data('page_status', "1");
        $m->set_data('updated_by', $updated_by);
        $a = array(
            'parent_menu_id' => $m->get_data('parent_menu_id') ,
            'menu_name' => $m->get_data('sub_menu_name') ,
            'menu_link' => $m->get_data('menu_link') ,
            'status' => $m->get_data('status') ,
            'page_status' => $m->get_data('page_status') ,
            'updated_by' => $m->get_data('updated_by') ,
        );
        $q = $d->update("master_menu", $a, "menu_id='$menu_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Page Updated.");
            $_SESSION['msg'] = "Page Updated.";
            header("location:../pages");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pages");
        }
    }
    if (isset($_POST['page_id_delete']))
    {
        $q = $d->delete("master_menu", "menu_id='$page_id_delete'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Menu Deleted.");
            $_SESSION['msg'] = "Page Deleted  successfully.";
            header("location:../pages");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pages");
        }
    }
    //Delete Sub Menu
    if (isset($_POST['sub_menu_id_delete']))
    {
        $q = $d->delete("master_menu", "menu_id='$sub_menu_id_delete'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Menu Deleted");
            $_SESSION['msg'] = "Menu Deleted  successfully.";
            header("location:../subMenu");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../subMenu");
        }
    }

    if(isset($_POST['admin_menu_sorting']))
    {
        foreach ($menu_id as $key => $value)
        {
            $a = [
                'order_no' => $key
            ];
            $q = $d->update("master_menu",$a,"menu_id = '$value'");
        }
        if($q)
        {
            $d->insert_log("","$society_id",$_COOKIE['bms_admin_id'],"$created_by","Main menu order changed threw admin menu ordering");
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }
    }

    if(isset($_POST['admin_sub_menu_sorting']))
    {
        foreach ($sub_menu_id as $key => $value)
        {
            $a = [
                'order_no' => $key
            ];
            $q = $d->update("master_menu",$a,"menu_id = '$value'");
        }
        if($q)
        {
            $d->insert_log("","$society_id",$_COOKIE['bms_admin_id'],"$created_by","Sub menu order changed threw admin menu ordering");
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }
    }

    if(isset($_POST['regenerateMenuJson']))
    {
        unlink("../../img/".$_COOKIE['menuUpdatedDate_9']);
        $bms_admin_id = $_COOKIE['bms_admin_id'];
        unset($_COOKIE['menuUpdatedDate_'.$bms_admin_id]);
        $_SESSION['msg'] = "Menu Re-loaded Successfully";
        $_SESSION['reload_page'] = 1;
        header('location:../adminMenuOrdering');exit;
    }
}
else
{
    header('location:../login');
}
?>