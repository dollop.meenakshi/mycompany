<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_COOKIE['bms_admin_id']))
{
	header("location:../welcome");
}
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
	
	if(isset($_POST["adminMobile"]) && $_POST['SendOPT'] =="yes")
	{
		extract(array_map("test_input" , $_POST));
		$adminMobile=mysqli_real_escape_string($con, $adminMobile);
		$country_code=mysqli_real_escape_string($con, $country_code);
		$q=$d->select("bms_admin_master","admin_mobile='$adminMobile' AND country_code='$country_code'");
	 	$bms_admin_master_data = mysqli_fetch_array($q);
		if ( mysqli_num_rows($q) == 1 && $bms_admin_master_data['admin_active_status']==0)
		{
		 	extract($bms_admin_master_data);
		 	$digits = 6;
         	$otp_web= rand(pow(10, $digits-1), pow(10, $digits)-1);
         	$m->set_data('otp_web', $otp_web);
            $a = array(
                'otp_web' => $m->get_data('otp_web'),
            );
	        $sq=$d->selectRow("society_name,city_name","society_master","");
	        $societyData=mysqli_fetch_array($sq);
	        $society_name=$societyData['society_name'];
	        $city_name=$societyData['city_name'];
	        $society_id=$societyData['society_id'];
	        $result = $d->update("bms_admin_master",$a,"admin_mobile='$adminMobile' ");
	        if($result)
	        {
	        	$society_name = html_entity_decode($society_name);
	        	$smsObj->send_otp_admin($otp_web,$adminMobile,$society_name,$country_code);
            	$d->add_sms_log($adminMobile,"Admin OTP SMS",$society_id,$country_code,1);
				echo "1";
	        }
	        else
	        {
	        	echo "2";
	        }
        }
        else if ( mysqli_num_rows($q) == 1 && $bms_admin_master_data['admin_active_status']==1)
        {
        	echo "5";
        }
        else
        {
        	$forgot_email=mysqli_real_escape_string($con, $adminMobile);
			$q=$d->select("bms_admin_master,society_master","bms_admin_master.society_id = society_master.society_id AND admin_email='$forgot_email' AND admin_active_status=0");
			if ( mysqli_num_rows($q) == 1)
			{
				$data = mysqli_fetch_array($q);
				extract($data);
				$digits = 6;
		        $otp_web= rand(pow(10, $digits-1), pow(10, $digits)-1);
		        $m->set_data('otp_web', $otp_web);
	            $a = array(
	                'otp_web' => $m->get_data('otp_web'),
	            );
		        $result = $d->update("bms_admin_master",$a,"admin_email='$adminMobile'");
		        if($result)
		        {
		        	$to = $admin_email;
					$subject = "MyCo Admin Panel Login OTP";
					$society_name = html_entity_decode($society_name);
					$msg= " $otp_web";
					$app = "Admin Panel";
					include '../mail/adminPanelLoginOTP.php';
					include '../mail.php';
					echo "1";
		        }
		        else
		        {
		        	echo "2";
		        }
			}
			else
			{
				echo "0";
			}
        }

	}
	//IS_3534

	if(isset($_POST["mobile"])) {
		
		extract(array_map("test_input" , $_POST));
		$mobile=mysqli_real_escape_string($con, $mobile);
		$inputPass=mysqli_real_escape_string($con, $inputPass);
		$otp_web=(int)$otp_web;

		if (strlen($otp_web)==6) {
			$apeendOTPQueryMainAdmin = " AND otp_web='$otp_web'";
			$apeendOTPQuery = "  AND bms_admin_master.otp_web='$otp_web'";
		} else {
			$apeendOTPQueryMainAdmin = " AND admin_password='$inputPass'";
			$apeendOTPQuery = "  AND bms_admin_master.admin_password='$inputPass'";
		}

		if($mobile=='9737564998') {
			$q=$d->select("bms_admin_master","admin_mobile='$mobile' AND role_id='1' AND country_code='$country_code' $apeendOTPQueryMainAdmin");
		} else {
			$q=$d->select("bms_admin_master,society_master,role_master","(bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id AND bms_admin_master.country_code='$country_code' AND role_master.society_id=society_master.society_id $apeendOTPQuery) AND (bms_admin_master.admin_mobile='$mobile' OR bms_admin_master.admin_email='$mobile')");
		}

		$data = mysqli_fetch_array($q); 
		if ($data > 0 && mysqli_num_rows($q) == 1 && $data['admin_active_status']==0) {
			
			$country_id  = $data['country_id'];
		    $society_id  = $data['society_id'];
		    
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			          "getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id");

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'key: bmsapikey'
			));

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$server_output=json_decode($server_output,true);

			$arrayCount= count($server_output['language_key']);
			$language_id = $server_output['language_id'];

			$myFile = "../../img/$language_id.xml";

			$fh = fopen($myFile, 'w') or die("can't open file");
			$rss_txt = "";
			$rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
			$rss_txt .= "<rss version='2.0'>";
			    
			        $rss_txt .= '<string>';
			    for ($i1=0; $i1 < $arrayCount ; $i1++) { 
			    	$key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

			    	$keyName  = $server_output['language_key'][$i1]['key_name'];
			    	

			        $rss_txt .= "<$keyName>$key_value</$keyName>";
			    }
			        $rss_txt .= '</string>';
			$rss_txt .= '</rss>';

			fwrite($fh, $rss_txt);
			fclose($fh);

			
				
			setcookie('admin_name', $data['admin_name'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('city_name', $data['city_name'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_name', $data['society_name'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('bms_admin_id', $data['admin_id'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_id', $data['society_id'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('role_name', $data['role_name'], time() + (86400 * 15), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_type', $data['society_type'], time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('language_id', $language_id, time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('country_id', $country_id, time() + (86400 * 5 ), "/$cookieUrl"); // 86400 = 1 day
			
			setcookie("lngUpdated", "lngUpdated", time() + (86400 * 5), "/$cookieUrl"); // 86400 = 5 day
			
			$_SESSION['msg']= "Welcome $_COOKIE[admin_name]";
			
			
			

			// Session Data insert
			$admin_id=$_COOKIE['bms_admin_id'];
			$ip_address=$_SERVER['REMOTE_ADDR']; # Save The IP Address
			$browser=$_SERVER['HTTP_USER_AGENT']; # Save The User Agent
			$loginTime=date("d M,Y h:i:sa");//Login Time
			$m->set_data('admin_id',$admin_id);
			$m->set_data('name',$_COOKIE['admin_name']);
			$m->set_data('role_name','Admin');
			$m->set_data('ip_address',$ip_address);
			$m->set_data('browser',$browser);
			$m->set_data('loginTime',$loginTime);
			$a1= array ('admin_id'=> $m->get_data('admin_id'),
				'name'=> $m->get_data('name'),
				'role_name'=> $m->get_data('role_name'),
				'ip_address'=> $m->get_data('ip_address'),
				'browser'=> $m->get_data('browser'),
				'loginTime'=> $m->get_data('loginTime'),
			);
			$insert=$d->insert('session_log',$a1); 
        // Redirqt to homepage
        // echo "Success";
		$_SESSION['msg']= "Welcome ".$data['admin_name'];
		header("location:../welcome");
	}else if ($data > 0 && mysqli_num_rows($q) == 1 && $data['admin_active_status']==1) {
		$_SESSION['msg1']= "Your account is deactivated !";
		header("location:../");
	} 
	else {
		$_SESSION['msg1']= "Wrong Credentials Details";
		header("location:../");
	}
}


// forgot Password 
if(isset($_POST["forgot_email"])) {
	extract(array_map("test_input" , $_POST));
	$forgot_email=mysqli_real_escape_string($con, $forgot_email);

	$q=$d->select("bms_admin_master","admin_email='$forgot_email' AND admin_active_status=0 OR admin_mobile='$forgot_email'  AND admin_active_status=0 AND country_code='$country_code'");
	$data = mysqli_fetch_array($q); 
	extract($data);
	if ($data > 0) {
		$default_time_zone=$d->getTimezone($data['society_id']);
		date_default_timezone_set($default_time_zone);
		$admin_name = $data['admin_name'];
		$admin_mobile = $data['admin_mobile'];
		$admin_email = $data['admin_email'];
		$country_code = $data['country_code'];
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		$forgotTime=date("Y/m/d");//Token Date
		$forgotLink=$base_url."apAdmin/resetPassword.php?t=".$token."&f=".$data['admin_id'];
		$m->set_data('token',$token);
		$m->set_data('token_date',$forgotTime);
		$a1= array ('forgot_token'=> $m->get_data('token'),
			'token_date'=> $m->get_data('token_date')
		);

		$d->update('bms_admin_master',$a1,"admin_mobile='$admin_mobile'"); 


		$to = $admin_email;
		$subject = "Forgot Password - MyCo";

		include '../mail/forgotPasswordMail.php';
		include '../mail.php';

		
		$smsObj->send_sms_password_reset($society_id,$admin_name,$admin_mobile,$forgotLink,$country_code);
	    $d->add_sms_log($admin_mobile,"Admin Password Reset Sms",$society_id,$country_code,2);

        // Redirqt to homepage
		$_SESSION['msg']= "Password reset link sent";
		header("location:../forgot.php");
	} 
	else {
		$_SESSION['msg1']= "Wrong Email or Mobile Number";
		header("location:../forgot.php");
	}
}




// forgot Password 
if(isset($_POST["forgot_email_admin"])) {
	extract(array_map("test_input" , $_POST));
	$forgot_email_admin=mysqli_real_escape_string($con, $forgot_email_admin);

	$q=$d->select("bms_admin_master","admin_email='$forgot_email_admin'  AND admin_active_status=0");
	$data = mysqli_fetch_array($q); 
	extract($data);
	if ($data > 0) {
		$default_time_zone=$d->getTimezone($data['society_id']);
		date_default_timezone_set($default_time_zone);
		$admin_name = $data['admin_name'];
		$admin_mobile = $data['admin_mobile'];
		$admin_email = $data['admin_email'];
		$country_code = $data['country_code'];

		$token = bin2hex(openssl_random_pseudo_bytes(16));
		$forgotTime=date("Y/m/d");//Token Date
		$forgotLink=$base_url."apAdmin/resetPassword.php?t=".$token."&f=".$data['admin_id'];
		$m->set_data('token',$token);
		$m->set_data('token_date',$forgotTime);
		$a1= array ('forgot_token'=> $m->get_data('token'),
			'token_date'=> $m->get_data('token_date')
		);

		$d->update('bms_admin_master',$a1,"admin_email='$forgot_email_admin'"); 


		$to = $forgot_email_admin;
		$subject = "Forgot Password - MyCo";

		include '../mail/forgotPasswordAdminMail.php';
		include '../mail.php';

		$smsObj->send_sms_password_reset($society_id,$admin_name,$admin_mobile,$forgotLink,$country_code);
	    $d->add_sms_log($admin_mobile,"Admin Password Reset Sms",$society_id,$country_code,2);

        // Redirqt to homepage
		$_SESSION['msg']= "Password reset link sent";
		header("location:../buildings");
	} 
	else {
		$_SESSION['msg1']= "Wrong Email";
		header("location:../buildings");
	}
}





//Reset Password
if(isset($_POST["password2"])) {

	extract(array_map("test_input" , $_POST));
	if ($password2==$passwordNew) {
		

		$m->set_data('token',"");
		$m->set_data('token_date',"");
		$m->set_data('passwordNew',$passwordNew);
		$forgot= array (
			'admin_password'=> $m->get_data('passwordNew'),
			'forgot_token'=> $m->get_data('token'),
			'token_date'=> $m->get_data('token_date'),
		);
		
		$qForgot = $d->update("bms_admin_master",$forgot,"admin_id= '$_SESSION[forgot_admin_id]' AND admin_active_status=0");
		if ($qForgot>0) {
		//IS_590
			session_start();
			session_destroy();
		//IS_590
			$_SESSION['msg']= "New Password set successfully.";
			header("location:../index.php");
		} 
		else {
			$_SESSION['msg1']= "Something Wrong...";
			header("location:../index.php");
		}
	} else {
		$_SESSION['msg1']= "Confirm Password Not Match";
		header("location:../index.php");
	}
}



} else  {
	header("location:../forgot.php");
}
?>