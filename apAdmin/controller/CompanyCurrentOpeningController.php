<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addCompanyCurrentOpening'])) {

        $company_current_opening_schedule = json_encode($_POST['company_current_opening_schedule']);

        $m->set_data('society_id', $society_id);
        $m->set_data('company_current_opening_title', $company_current_opening_title);
        $m->set_data('company_current_opening_position', $company_current_opening_position);
        $m->set_data('company_current_opening_timing', $company_current_opening_timing);
        $m->set_data('company_current_opening_schedule', $company_current_opening_schedule);
        $m->set_data('company_current_opening_address', $company_current_opening_address);
        $m->set_data('company_current_opening_pay_scale', $company_current_opening_pay_scale);
        $m->set_data('company_current_opening_salary_period', $company_current_opening_salary_period);
        $m->set_data('company_current_opening_experience', $company_current_opening_experience);
        $m->set_data('company_current_opening_description', $company_current_opening_description);
        $m->set_data('company_current_opening_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('company_current_opening_created_at',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'company_current_opening_title' => $m->get_data('company_current_opening_title'),
            'company_current_opening_position' => $m->get_data('company_current_opening_position'),
            'company_current_opening_timing' => $m->get_data('company_current_opening_timing'),
            'company_current_opening_schedule' => $m->get_data('company_current_opening_schedule'),
            'company_current_opening_address' => $m->get_data('company_current_opening_address'),
            'company_current_opening_pay_scale' => $m->get_data('company_current_opening_pay_scale'),
            'company_current_opening_salary_period' => $m->get_data('company_current_opening_salary_period'),
            'company_current_opening_experience' => $m->get_data('company_current_opening_experience'),
            'company_current_opening_description' => $m->get_data('company_current_opening_description'),
            'company_current_opening_added_by' => $m->get_data('company_current_opening_added_by'),
            'company_current_opening_created_at' => $m->get_data('company_current_opening_created_at'),

        );

       

        if (isset($company_current_opening_id) && $company_current_opening_id > 0) {
            $q = $d->update("company_current_opening_master", $a1, "company_current_opening_id ='$company_current_opening_id'");
            $_SESSION['msg'] = "Company Current Opening Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Current Opening Updated Successfully");
        } else {
            $q = $d->insert("company_current_opening_master", $a1);
            $_SESSION['msg'] = "Company Current Opening Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Current Opening Added Successfully");

        }
        if ($q == true) {
            header("Location: ../companyCurrentOpening");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyCurrentOpening");
        }

    }

    if(isset($_POST['approvedReferStatus'])){
        
        $q3 = $d->selectRow('*',"company_opening_refer_master","company_opening_refer_id = '$company_opening_refer_id'");
        $data3 = mysqli_fetch_assoc($q3);

        $userId = $data3['company_opening_refer_by'];

        $m->set_data('company_opening_refer_status', 1);
        $m->set_data('company_opening_refer_status_change_type', 1);
        $m->set_data('company_opening_refer_status_change_by_id', $_COOKIE['bms_admin_id']);
        $m->set_data('company_opening_refer_modified_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'company_opening_refer_status' => $m->get_data('company_opening_refer_status'),
            'company_opening_refer_status_change_type' => $m->get_data('company_opening_refer_status_change_type'),
            'company_opening_refer_status_change_by_id' => $m->get_data('company_opening_refer_status_change_by_id'),
            'company_opening_refer_modified_date' => $m->get_data('company_opening_refer_modified_date'),
        );

        if(isset($company_opening_refer_id) && $company_opening_refer_id>0){
            $q = $d->update('company_opening_refer_master', $a1, "company_opening_refer_id='$company_opening_refer_id'");
        }
        if ($q == true) {
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$userId'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your refer request is Approved";
            $description ="By Admin, ".$created_by;
            $menuClick = "";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png","$activity","users_master.user_id = '$uId'");

            $_SESSION['msg'] = "Refer Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Refer Status Change Successfully");
            header("Location: ../companyOpeningRefer");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyOpeningRefer");
        }
    }

    if(isset($_POST['rejectReferStatus'])){
        $q3 = $d->selectRow('*',"company_opening_refer_master","company_opening_refer_id = '$company_opening_refer_id'");
        $data3 = mysqli_fetch_assoc($q3);

        $userId = $data3['company_opening_refer_by'];

        $m->set_data('company_opening_refer_status', 2);
        $m->set_data('company_opening_refer_status_change_type', 1);
        $m->set_data('company_opening_refer_admin_note', $company_opening_refer_admin_note);
        $m->set_data('company_opening_refer_status_change_by_id', $_COOKIE['bms_admin_id']);
        $m->set_data('company_opening_refer_modified_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'company_opening_refer_status' => $m->get_data('company_opening_refer_status'),
            'company_opening_refer_status_change_type' => $m->get_data('company_opening_refer_status_change_type'),
            'company_opening_refer_admin_note' => $m->get_data('company_opening_refer_admin_note'),
            'company_opening_refer_status_change_by_id' => $m->get_data('company_opening_refer_status_change_by_id'),
            'company_opening_refer_modified_date' => $m->get_data('company_opening_refer_modified_date'),
        );

        if(isset($company_opening_refer_id) && $company_opening_refer_id>0){
            $q = $d->update('company_opening_refer_master', $a1, "company_opening_refer_id='$company_opening_refer_id'");
        }
        if ($q == true) {
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$userId'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your refer request is Reject";
            $description ="By Admin, ".$created_by;
            $menuClick = "";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png","$activity","users_master.user_id = '$uId'");

            $_SESSION['msg'] = "Refer Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Refer Status Change Successfully");
            header("Location: ../companyOpeningRefer");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyOpeningRefer");
        }
    }

}
