<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    
    //////single leave assign///////
    if (isset($_POST['addcourseChapter'])) {
        
        $m->set_data('society_id', $society_id);
        $m->set_data('course_id', $course_id);
        $m->set_data('chapter_title', $chapter_title);
        $m->set_data('chapter_description', $chapter_description);
        $m->set_data('course_chapter_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('course_chapter_created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'course_id' => $m->get_data('course_id'),
            'chapter_title' => $m->get_data('chapter_title'),
            'chapter_description' => $m->get_data('chapter_description'),
            'course_chapter_created_by' => $m->get_data('course_chapter_created_by'),
            'course_chapter_created_date' => $m->get_data('course_chapter_created_date'),

        );
        if (isset($course_chapter_id) && $course_chapter_id > 0) {
            $q = $d->update("course_chapter_master", $a1, "course_chapter_id ='$course_chapter_id'");
            $_SESSION['msg'] = "Course Chapter Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Chapter Updated Successfully");
        } else {
            $q = $d->insert("course_chapter_master", $a1);
            $course_chapter_id = $con->insert_id;
            $_SESSION['msg'] = "Course Chapter Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Course Chapter Added Successfully");

        }
        if ($q == true) {
            
            header("Location: ../courseChapter?cId=$course_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../courseChapter");
        }

    }


    

    //////bulk leave assign///////
    if (isset($_POST['addLeaveAssignBulk'])) {
        $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        for ($i=0; $i < count($_POST['leave_type_id']); $i++) { 
            
            if($_POST['user_total_leave'][$i] != ''){
                $m->set_data('society_id', $society_id);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('user_id', $user_id);
                $m->set_data('unit_id', $maData['unit_id']);
                $m->set_data('user_total_leave', $_POST['user_total_leave'][$i]);
                $m->set_data('assign_leave_year', $assign_leave_year);
                $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);
                $m->set_data('user_leave_assign_date',date("Y-m-d H:i:s"));

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_total_leave' => $m->get_data('user_total_leave'),
                    'assign_leave_year' => $m->get_data('assign_leave_year'),
                    'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
                    'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),
        
                );

                $q = $d->insert("leave_assign_master", $a1);
                $_SESSION['msg'] = "Leave Assign Added Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Assign Added Successfully");
            }
        }

        if ($q == true) {
            header("Location: ../leaveTracker");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveTracker");
        }

    }

}
