<?php
include '../common/objectController.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    extract($_POST);
    //IS_605
    if(isset($_POST['addsalary']))
    {
       // print_r($_POST);die;
        $gross_salary_new = (float)$gross_salary+(float)$flatAmount;
      
        mysqli_autocommit($con, FALSE);
        $m->set_data('user_id', $user_id);
       
        $m->set_data('salary_group_id', $salary_group_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('salary_type', $salary_type);
        $m->set_data('floor_id', $floor_id_old);
        $m->set_data('block_id', $block_id_old);
        $m->set_data('salary_id', $salary_id);
        $m->set_data('total_fixed_amount', $flatAmount);
        $m->set_data('total_ctc', $total_ctc);
        $m->set_data('salary_start_date', $salary_start_date);
        $m->set_data('salary_increment_remark', $salary_increment_remark);
        $m->set_data('salary_increment_date', $salary_increment_date);
        $m->set_data('net_salary', $net_salary);
        $m->set_data('gross_salary', $gross_salary_new);

        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);


        ///salary_amount					
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'total_fixed_amount' => $m->get_data('total_fixed_amount'),
            'salary_group_id' => $m->get_data('salary_group_id'),
            'user_id' => $m->get_data('user_id'),
            'net_salary' => $m->get_data('net_salary'),
            'gross_salary' => $m->get_data('gross_salary'),
            'total_ctc' => $m->get_data('total_ctc'),
            'salary_type' => $m->get_data('salary_type'),
            'floor_id' => $m->get_data('floor_id'),
            'block_id' => $m->get_data('block_id'),
            'salary_start_date' => $m->get_data('salary_start_date'),
            'salary_increment_remark' => $m->get_data('salary_increment_remark'),
            'salary_increment_date' => $m->get_data('salary_increment_date'),
            'created_at' => $m->get_data('created_at'),
        );
        
       
        $flatAmnt = 0;
        foreach ($_POST['amount_type'] as $s => $vall) {
            if($vall==1){
                $flatAmnt = $flatAmnt+$_POST['salary_earning_deduction_id'][$s];
            }
        }
        
        if (isset($salary_id) && $salary_id > 0) {
            $q = $d->update("salary", $a1, "salary_id ='$salary_id'");
            $_SESSION['msg'] = "Salary Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Updated Successfully");
        } else {
            $q = $d->insert("salary", $a1);
            $salary_id = $con->insert_id;
            $_SESSION['msg'] = "Salary Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Added Successfully");
        }
        // echo "<pre>";
        // print_r($_POST);die;
        if ($salary_id) {
            
            if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                $q2 = $d->delete("salary_master", "salary_id = '$salary_id'");
                foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {
                    $amntPrcnt = 0;
                        // if(($_POST['earn_deduct_type'][$key]==0)){
                        //     if($_POST['salary_common_value_earn_deduction'][$key] !=""){
                        //         $salary_common_value_earn_deductionAR = explode(',',$_POST['salary_common_value_earn_deduction'][$key]);
                        //         $tempAmount = 0;
                        //         for ($m=0; $m < count($salary_common_value_earn_deductionAR); $m++) { 
                        //             $earnValue = $_POST['salary_earning_deduction_id'][$salary_common_value_earn_deductionAR[$m]];
                        //             $tempAmount = $tempAmount+$earnValue;
                        //         }
                        //         //$amntPrcnt =  (((int)$val / (int)$tempAmount) * 100);
                        //         $amntPrcnt =  (((int)$val / (int)($gross_salary-$flatAmnt)) * 100);
                        //     }else{
                        //         $amntPrcnt =  (((int)$val / (int)($gross_salary-$flatAmnt)) * 100);
                        //     }
                        // }else{
                         $amntPrcnt = $_POST['amount_value'][$key];

                      //  }
                    
                      if($_POST['amount_value_employeer'][$key]>0){
                        if($_POST['amount_type'][$key]=="0"){
                            if($_POST['salary_common_value_earn_deduction'][$key] !=""){
                                $salary_common_value_earn_deductionAR = explode(',',$_POST['salary_common_value_earn_deduction'][$key]);
                                $tempAmount = 0;
                                for ($m=0; $m < count($salary_common_value_earn_deductionAR); $m++) { 
                                    $earnValue = $_POST['salary_earning_deduction_id'][$salary_common_value_earn_deductionAR[$m]];
                                    $tempAmount = $tempAmount+$earnValue;
                                }
                                $tempEmployeer =  (float)($tempAmount) * (float)($_POST['amount_value_employeer'][$key] / 100);;
                            }else{

                                $tempEmployeer = (float)($gross_salary) * (float)($_POST['amount_value_employeer'][$key] / 100);
                            }
                        }else if($_POST['amount_type'][$key]=="1"){
                            $tempEmployeer = $_POST['amount_value_employeer'][$key];
                        }
                        $contribution_precent = $_POST['amount_value_employeer'][$key];
                      }else{
                        $contribution_precent =0;
                        $tempEmployeer =0;
                      }
                    

                    
                   
                    $salaryEarnValueData = array(
                        'salary_id' => $salary_id,
                        'salary_earning_deduction_id' => $key,
                        'contribution_precent' => $contribution_precent,
                        'contribution_amount' => $tempEmployeer,
                        // 'is_changable'=>$_POST['is_changable'][$key],
                        'salary_earning_deduction_value' => $val,
                        'amount_type' => $_POST['amount_type'][$key],
                        'amount_percentage' => $amntPrcnt,
                        'created_date' => date('Y-m-d h:i:s'),
                    );
                 ///  print_r($val);
                 ///  print_r($salaryEarnValueData);
                    if($val !=""){
                    $q2 = $d->insert("salary_master", $salaryEarnValueData);
                    }
                }
            }
        }
       /// die;
        if ($q && $q2) {
            mysqli_commit($con);
            header("Location: ../salary?gId=$salary_group_id&dId=$floor_id_old&bId=$block_id_old&uId=$user_id");
        } else {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../salary");
        }
    }

    /* increment salary    */
    if(isset($_POST['NextIncrementsalary']))
    {
        $preSalaryEndDate = date('Y-m-d', strtotime($salary_start_date . ' -1 day'));
        $gross_salary_new = (float)$gross_salary+(float)$flatAmount;
        $m->set_data('user_id', $user_id);
        $m->set_data('salary_group_id', $salary_group_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('salary_type', $salary_type);
        $m->set_data('floor_id', $floor_id_old);
        $m->set_data('gross_salary', $gross_salary_new);
        $m->set_data('total_fixed_amount', $flatAmount);
        $m->set_data('total_ctc', $total_ctc);
        $m->set_data('block_id', $block_id_old);
        $m->set_data('salary_id', $salary_id);
        $m->set_data('salary_start_date', $salary_start_date);
        $m->set_data('salary_increment_remark', $salary_increment_remark);
        $m->set_data('salary_increment_date', $salary_increment_date);
        $m->set_data('net_salary', $net_salary);

        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);

        ///salary_amount					
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'salary_group_id' => $m->get_data('salary_group_id'),
            'user_id' => $m->get_data('user_id'),
            'gross_salary' => $m->get_data('gross_salary'),
            'total_ctc' => $m->get_data('total_ctc'),
            'net_salary' => $m->get_data('net_salary'),
            'salary_type' => $m->get_data('salary_type'),
            'floor_id' => $m->get_data('floor_id'),
            'block_id' => $m->get_data('block_id'),
            'salary_start_date' => $m->get_data('salary_start_date'),
            'salary_increment_remark' => $m->get_data('salary_increment_remark'),
            'salary_increment_date' => $m->get_data('salary_increment_date'),
            'created_at' => $m->get_data('created_at'),
        );


        $a2 = array('salary_end_date' => $preSalaryEndDate,'is_preivous_salary' => 1);
        $q = $d->update("salary", $a2, "salary_id ='$salary_id'");
        // $_SESSION['msg'] = "Salary Updated Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Updated Successfully");

        $q = $d->insert("salary", $a1);
        $salary_id = $con->insert_id;
       
        $_SESSION['msg'] = "Salary Added Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Added Successfully");

        if ($q == true) {
            if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                $q2 = $d->delete("salary_master", "salary_id = '$salary_id'");
                foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {
                    
                    $amntPrcnt = 0;
                    // if(($_POST['earn_deduct_type'][$key]==0)){
                    //     if($_POST['salary_common_value_earn_deduction'][$key] !=""){
                    //         $salary_common_value_earn_deductionAR = explode(',',$_POST['salary_common_value_earn_deduction'][$key]);
                    //         $tempAmount = 0;
                    //         for ($m=0; $m < count($salary_common_value_earn_deductionAR); $m++) { 
                    //             $earnValue = $_POST['salary_earning_deduction_id'][$salary_common_value_earn_deductionAR[$m]];
                    //             $tempAmount = $tempAmount+$earnValue;
                    //         }
                    //         //$amntPrcnt =  (((int)$val / (int)$tempAmount) * 100);
                    //         $amntPrcnt =  (((int)$val / (int)($gross_salary-$flatAmnt)) * 100);
                    //     }else{
                    //         $amntPrcnt =  (((int)$val / (int)($gross_salary-$flatAmnt)) * 100);
                    //     }
                    // }else{
                     $amntPrcnt = $_POST['amount_value'][$key];

                  //  }
                
                  if($_POST['amount_value_employeer'][$key]>0){
                    if($_POST['amount_type'][$key]=="0"){
                        if($_POST['salary_common_value_earn_deduction'][$key] !=""){
                            $salary_common_value_earn_deductionAR = explode(',',$_POST['salary_common_value_earn_deduction'][$key]);
                            $tempAmount = 0;
                            for ($m=0; $m < count($salary_common_value_earn_deductionAR); $m++) { 
                                $earnValue = $_POST['salary_earning_deduction_id'][$salary_common_value_earn_deductionAR[$m]];
                                $tempAmount = $tempAmount+$earnValue;
                            }
                            $tempEmployeer =  (float)($tempAmount) * (float)($_POST['amount_value_employeer'][$key] / 100);;
                        }else{

                            $tempEmployeer = (float)($gross_salary) * (float)($_POST['amount_value_employeer'][$key] / 100);
                        }
                    }else if($_POST['amount_type'][$key]=="1"){
                        $tempEmployeer = $_POST['amount_value_employeer'][$key];
                    }
                    $contribution_precent = $_POST['amount_value_employeer'][$key];
                  }else{
                    $contribution_precent =0;
                    $tempEmployeer =0;
                  }
                
                    $salaryEarnValueData = array(
                        'salary_id' => $salary_id,
                        'salary_earning_deduction_id' => $key,
                        /// 'is_changable'=>$_POST['is_changable'][$key],
                        'contribution_precent' => $contribution_precent,
                        'contribution_amount' => $tempEmployeer,
                        'salary_earning_deduction_value' => $val,
                        'amount_type' => $_POST['amount_type'][$key],
                        'amount_percentage' => $amntPrcnt,
                        'created_date' => date('Y-m-d h:i:s'),
                    );
                  
                    if($val !=""){
                    $q2 = $d->insert("salary_master", $salaryEarnValueData);
                    }
                }
            }
            header("Location: ../salary?dId=$floor_id_old&bId=$block_id_old");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../salary?dId=$floor_id_old&bId=$block_id_old");
        }
    }

    if(isset($_POST['updateSalarySetting']))
    {

        ///$m->set_data('salary_setting_sign_stamp', $salary_setting_sign_stamp);
        // $m->set_data('hourly_salary_extra_hours_payout', $hourly_salary_extra_hours_payout);
       // $m->set_data('working_day_calculation', $working_day_calculation);
        $m->set_data('society_id', $society_id);
        $m->set_data('created_by', $_COOKIE['bms_admin_id']);
        $a1 = array(
           'created_by'=>$m->get_data('created_by'),
            'society_id'=>$m->get_data('society_id'),
            //'working_day_calculation'=>$m->get_data('working_day_calculation'),
        );
       
        // if($hourly_salary_extra_hours_payout !=""){
        //     $a1['hourly_salary_extra_hours_payout']=$hourly_salary_extra_hours_payout;
        // }
      // print_r((float)$a1['hourly_salary_extra_hours_payout']);die;
        $file = $_FILES['salary_setting_sign_stamp']['tmp_name'];
        if (file_exists($file)) {
            $extensionResume = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
            $extId = pathinfo($_FILES['salary_setting_sign_stamp']['name'], PATHINFO_EXTENSION);

            $errors = array();
            $maxsize = 26214400;

            if (($_FILES['salary_setting_sign_stamp']['size'] >= $maxsize) || ($_FILES["salary_setting_sign_stamp"]["size"] == 0)) {
                $_SESSION['msg1'] = "File too large. Must be less than 25 MB.";
                header("location:../salarySetting");
                exit();
            }
            if (!in_array($extId, $extensionResume) && (!empty($_FILES["salary_setting_sign_stamp"]["type"]))) {
                $_SESSION['msg1'] = "Invalid File format, Only Pdf and Doc is allowed.";
                header("location:../salarySetting");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['salary_setting_sign_stamp'];
                $temp = explode(".", $_FILES["salary_setting_sign_stamp"]["name"]);
                $salary_setting_sign_stamp = 'stamp_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["salary_setting_sign_stamp"]["tmp_name"], "../../img/stamp/" . $salary_setting_sign_stamp);
            }
        } else {
            $salary_setting_sign_stamp = "";
        }
        if (isset($salary_setting_sign_stamp) && $salary_setting_sign_stamp != "") {
            $m->set_data('salary_setting_sign_stamp', $salary_setting_sign_stamp);
        } else {
            $m->set_data('salary_setting_sign_stamp', $salary_setting_sign_stamp_old);
        }
        
       $a1['salary_setting_sign_stamp']=$m->get_data('salary_setting_sign_stamp');
       /* $a1['salary_setting_sign']=$m->get_data('salary_setting_sign'); */
      
        if (isset($salary_setting_id) && $salary_setting_id > 0) {
           
            $q = $d->update("salary_setting", $a1, "salary_setting_id ='$salary_setting_id'");
            $_SESSION['msg'] = "Salary Setting Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Setting Updated");
        } else {
            $q = $d->insert("salary_setting", $a1);
            $salary_id = $con->insert_id;
            $_SESSION['msg'] = "Salary Setting Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Setting Updated");
        }
        if ($q) {
            mysqli_commit($con);
            header("Location: ../salarySetting");
        } else {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../salarySetting");
        }
    }

    if(isset($_POST['changeSalaryIssueStatus']) && $_POST['changeSalaryIssueStatus'] == "changeSalaryIssueStatus")
    {
        if($reject_reason == "" || empty($reject_reason))
        {
            $_SESSION['msg1'] = "Reject reason can't be empty!";
            header("Location: ../salaryRaisedIssues");exit;
        }
        $m->set_data('reject_reason', $reject_reason);
        $a1 = array(
            'status' => 2,
            'reject_reason' => $m->get_data('reject_reason'),
            'issue_resolved_by' => $_COOKIE['bms_admin_id'],
            'modified_date' => date("Y-m-d H:i:s")
        );
        $q = $d->update("salary_raised_issue_master", $a1,"salary_issue_id = '$salary_issue_id'");
        if($q)
        {
            $_SESSION['msg'] = "Salary Issue rejected successfully";
            header("Location: ../salaryRaisedIssues");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../salaryRaisedIssues");exit;
        }
    }

    if(isset($_POST['approveSalaryIssue']) && $_POST['approveSalaryIssue'] == "approveSalaryIssue")
    {
        $a1 = array(
            'status' => 1,
            'issue_resolved_by' => $_COOKIE['bms_admin_id'],
            'modified_date' => date("Y-m-d H:i:s")
        );
        $q = $d->update("salary_raised_issue_master", $a1,"salary_issue_id = '$salary_issue_id'");
        if($q)
        {
            $_SESSION['msg'] = "Salary Issue solved successfully";exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";exit;
        }
    }
}
?>