<?php 
include '../common/objectController.php';
extract($_POST);

if(isset($_POST) && !empty($_POST) )

{



   
 
  if(isset($addNotice)) {
    
    $file = $_FILES['notice_attachment']['tmp_name'];
    if(file_exists($file)) {
      $extensionResume=array('jpeg', 'jpg', 'png', 'doc','docx', 'pdf', 'csv', 'xls' , 'xlsx','ods','xlsb');
      $extId = pathinfo($_FILES['notice_attachment']['name'], PATHINFO_EXTENSION);
      $extId = strtolower($extId);

      $errors     = array();
      $maxsize    = 32097152;
        
        if(($_FILES['notice_attachment']['size'] >= $maxsize) || ($_FILES["notice_attachment"]["size"] == 0)) {
            $_SESSION['msg1']="Attachment too large. Must be less than 32 MB.";
            header("location:../circularAdd");
            exit();
        }
        if(!in_array($extId, $extensionResume) && (!empty($_FILES["notice_attachment"]["type"]))) {
             $_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
            header("location:../circularAdd");
            exit();
        }
       if(count($errors) === 0) {
        $image_Arr = $_FILES['notice_attachment'];   
        $temp = explode(".", $_FILES["notice_attachment"]["name"]);
        $notice_attachment = 'Notice_'.round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["notice_attachment"]["tmp_name"], "../../img/noticeBoard/".$notice_attachment);
        $notice_attachment_url = $base_url.'img/noticeBoard/'.$notice_attachment;
      } 
    } else{
      $notice_attachment="";
    }

    $m->set_data('society_id',$society_id);
    $m->set_data('notice_description',$notice_description);
    $m->set_data('notice_title',test_input($notice_title));
    $m->set_data('notice_attachment',test_input($notice_attachment));
    $m->set_data('active_status',$active_status);
		$m->set_data('notice_time',date('d M Y h:i A'));

    if (count($floor_id)>0) {
      $m->set_data('noticeboard_type',1);
         $ids = join("','",$_POST['floor_id']);   
        if ($ids!=0 || $ids!= '') {
          $appendNew= " AND users_master.floor_id IN ('$ids')";
        } 
    } else {
      $m->set_data('noticeboard_type',0);
    }
    ////// for zone
    if($_POST['zone_id'][0] != 0){
      $m->set_data('noticeboard_zone_type',1); 
         $ids = join("','",$_POST['zone_id']);
          if ($ids!=0 || $ids!= '') {
            $appendZoneNew= " AND users_master.zone_id IN ('$ids')";
          } 
    } else {
      $m->set_data('noticeboard_zone_type',0);
    }
    //////for level
    if($_POST['level_id'][0] != 0){
      $m->set_data('noticeboard_level_type',1);
         $ids = join("','",$_POST['level_id']);   
        if ($ids!=0 || $ids!= '') {
          $appendLevelNew= " AND users_master.level_id IN ('$ids')";
        } 
    } else {
      $m->set_data('noticeboard_level_type',0);
    }
    //IS_1292
    if(in_array("0", $block_id)){
      $block_idnew = 0;
    } else {
       $block_ids = implode(",", $block_id);
      $block_idnew = $block_ids;
    }
   


    //IS_983
    $m->set_data('block_id',$block_idnew);
    //IS_983


    //IS_1008
    $created_at = date('Y-m-d H:i:s');
     $m->set_data('created_at',$created_at);
    //IS_1008
         $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'notice_description'=> $m->get_data('notice_description'),
            'notice_title'=> $m->get_data('notice_title'),
            'notice_attachment'=> $m->get_data('notice_attachment'),
	          'notice_time'=> $m->get_data('notice_time'),
            'block_id'=> $m->get_data('block_id') ,
             'created_at'=> $m->get_data('created_at'),
             'updated_at'=> $m->get_data('created_at'),
             'noticeboard_type'=> $m->get_data('noticeboard_type'),
             'noticeboard_zone_type'=> $m->get_data('noticeboard_zone_type'),
             'noticeboard_level_type'=> $m->get_data('noticeboard_level_type'),
             'active_status'=> $m->get_data('active_status'),
	      );

       /*  echo "<pre>";
        print_r($a1);die; */
    
              //IS_1001
          $bl_qry ="";
          if(in_array("0", $block_id)){
              $block_id_arr = 0;
              $append_query="";
            } else {
               $block_ids = implode(",", $block_id);
              $block_id_arr = $block_ids;
              $bl_qry =" and block_id IN ('$block_id_arr') ";

             $append_query="block_master.block_id IN ('$block_id_arr') ";
            }   

        $q=$d->insert("notice_board_master",$a1);
        $notice_board_id = $con->insert_id;

        if($active_status=='0'){
          $title= "Circular";
          $description= "Added By Admin $created_by";
          $d->insertUserNotification($society_id,$title,$description,"announcement","Circularxxxhdpi.png",$append_query);


         $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $bl_qry $appendNew $appendZoneNew $appendLevelNew");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $bl_qry  $appendNew $appendZoneNew $appendLevelNew");
         $nResident->noti("AnnouncementFragment","",$society_id,$fcmArray,"Circular","Added By Admin $created_by",$notice_board_id);
         $nResident->noti_ios("NoticeVC","",$society_id,$fcmArrayIos,"Circular","Added By Admin $created_by",$notice_board_id);
       }
        
        
    if($q==TRUE) {
      for ($i1=0; $i1 <count($floor_id); $i1++) { 
        
         $aUnit= array (
            'society_id'=> $m->get_data('society_id'),
            'notice_board_id'=> $notice_board_id,
            'floor_id'=> $floor_id[$i1],
        );
         $d->insert("notice_board_unit_master",$aUnit);
      }

      if($_POST['zone_id'][0] != 0){
        for ($i2=0; $i2 <count($zone_id); $i2++) { 
        
          $aZone= array (
             'society_id'=> $m->get_data('society_id'),
             'notice_board_id'=> $notice_board_id,
             'zone_id'=> $zone_id[$i2],
         );
          $d->insert("notice_board_zone_master",$aZone);
       }
      }

      if($_POST['level_id'][0] != 0){
        for ($i3=0; $i3 <count($level_id); $i3++) { 
        
          $aLevel= array (
             'society_id'=> $m->get_data('society_id'),
             'notice_board_id'=> $notice_board_id,
             'level_id'=> $level_id[$i3],
         );
          $d->insert("notice_board_level_master",$aLevel);
       }
      }

      $_SESSION['msg']="Circular Added";
      //IS_1210
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Circular Added");
      header("Location: ../circulars");
    } else {
      header("Location: ../circulars");
    }
  }


  if(isset($updateNotice)) {
    echo "<pre>";
        print_r($_POST);
    $file = $_FILES['notice_attachment']['tmp_name'];
    if(file_exists($file)) {
      $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG","csv","ods");
      $extId = pathinfo($_FILES['notice_attachment']['name'], PATHINFO_EXTENSION);

      $errors     = array();
      $maxsize    = 10097152;
        
        if(($_FILES['notice_attachment']['size'] >= $maxsize) || ($_FILES["notice_attachment"]["size"] == 0)) {
            $_SESSION['msg1']="Attachment too large. Must be less than 10 MB.";
            header("location:../circulars");
            exit();
        }
        if(!in_array($extId, $extensionResume) && (!empty($_FILES["notice_attachment"]["type"]))) {
             $_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
            header("location:../circulars");
            exit();
        }
       if(count($errors) === 0) {
        $image_Arr = $_FILES['notice_attachment'];   
        $temp = explode(".", $_FILES["notice_attachment"]["name"]);
        $notice_attachment = 'Notice_'.round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["notice_attachment"]["tmp_name"], "../../img/noticeBoard/".$notice_attachment);
      } 
    } else{
      $notice_attachment=$notice_attachment_old;
    }
   

    $m->set_data('society_id',$society_id);
    $m->set_data('notice_description',$notice_description);
    $m->set_data('notice_title',test_input($notice_title));
    $m->set_data('notice_attachment',test_input($notice_attachment));
    $m->set_data('active_status',$active_status);
    $m->set_data('notice_time',date('d M Y h:i A'));

    if (count($floor_id)>0) {
      $m->set_data('noticeboard_type',1);
         $ids = join("','",$_POST['floor_id']);   
        if ($ids!=0 || $ids!= '') {
          $appendNew= " AND users_master.floor_id IN ('$ids')";
        } 
    } else {
      $m->set_data('noticeboard_type',0);
    }
    ////// for zone
    if($_POST['zone_id'][0] != 0){
      $m->set_data('noticeboard_zone_type',1);
         $ids = join("','",$_POST['zone_id']);
          if ($ids!=0 || $ids!= '') {
            $appendZoneNew= " AND users_master.zone_id IN ('$ids')";
          } 
    } else {
      $m->set_data('noticeboard_zone_type',0);
    }
    //////for level
    if($_POST['level_id'][0] != 0){
      $m->set_data('noticeboard_level_type',1);
         $ids = join("','",$_POST['level_id']);   
        if ($ids!=0 || $ids!= '') {
          $appendLevelNew= " AND users_master.level_id IN ('$ids')";
        } 
    } else {
      $m->set_data('noticeboard_level_type',0);
    }

    $d->delete("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
    $d->delete("notice_board_zone_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
    $d->delete("notice_board_level_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");

    //IS_983
    //IS_1292


    $bl_qry ="";
    if(in_array("0", $block_id)){
            $block_id_arr = 0;
          } else {
             $block_ids = implode(",", $block_id);
            $block_id_arr = $block_ids;
            $bl_qry =" and block_id IN ('$block_id_arr') ";
          }  
    $m->set_data('block_id',$block_id_arr);
    //IS_983

  
     //IS_1008
    $updated_at = date('Y-m-d H:i:s');
     $m->set_data('updated_at',$updated_at);
    //IS_1008
 
         $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'notice_description'=> $m->get_data('notice_description'),
            'notice_title'=> $m->get_data('notice_title'),
            'notice_attachment'=> $m->get_data('notice_attachment'),
            'notice_time'=> $m->get_data('notice_time'),
            'block_id'=> $m->get_data('block_id'),
            'updated_at'=> $m->get_data('updated_at'),
            'noticeboard_type'=> $m->get_data('noticeboard_type'),
            'noticeboard_zone_type'=> $m->get_data('noticeboard_zone_type'),
            'noticeboard_level_type'=> $m->get_data('noticeboard_level_type'),
            'active_status'=> $m->get_data('active_status'),
        );

             

              //IS_1001
          $bl_qry ="";
         if(in_array("0", $block_id)){
            $block_id_arr = 0;
             $append_query="";
          } else {
             $block_ids = implode(",", $block_id);
            $block_id_arr = $block_ids;
            $bl_qry =" and block_id IN ('$block_id_arr') ";
             $append_query="block_master.block_id IN ('$block_id_arr') ";
          }  

        if($active_status=='0'){
            $title= "Circular";
            $description= "Updated By Admin $created_by";
            $d->insertUserNotification($society_id,$title,$description,"announcement","Circularxxxhdpi.png",$append_query);


           $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'  $bl_qry $appendNew $appendZoneNew $appendLevelNew");
           $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $bl_qry $appendNew $appendZoneNew $appendLevelNew");
           $nResident->noti("AnnouncementFragment","",$society_id,$fcmArray,"Circular","Updated By Admin $created_by",$notice_board_id);
           $nResident->noti_ios("NoticeVC","",$society_id,$fcmArrayIos,"Circular","Updated By Admin $created_by",$notice_board_id);
           // print_r($fcmArrayIosay);
         }
        $q=$d->update("notice_board_master",$a1,"society_id='$society_id' AND notice_board_id='$notice_board_id'");
    if($q==TRUE) {

      for ($i1=0; $i1 <count($floor_id); $i1++) { 
        
        $aUnit= array (
           'society_id'=> $m->get_data('society_id'),
           'notice_board_id'=> $notice_board_id,
           'floor_id'=> $floor_id[$i1],
       );
        $d->insert("notice_board_unit_master",$aUnit);
     }

     if($_POST['zone_id'][0] != 0){
       for ($i2=0; $i2 <count($zone_id); $i2++) { 
       
         $aZone= array (
            'society_id'=> $m->get_data('society_id'),
            'notice_board_id'=> $notice_board_id,
            'zone_id'=> $zone_id[$i2],
        );
         $d->insert("notice_board_zone_master",$aZone);
      }
     }

     if($_POST['level_id'][0] != 0){
       for ($i3=0; $i3 <count($level_id); $i3++) { 
       
         $aLevel= array (
            'society_id'=> $m->get_data('society_id'),
            'notice_board_id'=> $notice_board_id,
            'level_id'=> $level_id[$i3],
        );
         $d->insert("notice_board_level_master",$aLevel);
      }
     }


      $_SESSION['msg']="Circular Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Circular Updated");
      header("Location: ../circulars");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../circulars");
    }
  }

   if(isset($notice_board_id_delete)) {
   
   //IS_1001
    $q=$d->delete("notice_board_master","society_id='$society_id' AND notice_board_id='$notice_board_id_delete' and block_id='$block_id'  ");
    $d->delete("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id_delete'");
    $d->delete("notice_board_zone_master","society_id='$society_id' AND notice_board_id='$notice_board_id_delete'");
    $d->delete("notice_board_level_master","society_id='$society_id' AND notice_board_id='$notice_board_id_delete'");
    if($q==TRUE) {
      $_SESSION['msg']="Circular Deleted";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Circular Deleted");
      header("Location: ../circulars");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../circulars");
    }
  }


  if(isset($notice_board_id_unread)) {

    $q=$d->select("notice_board_master","society_id='$society_id' AND notice_board_id='$notice_board_id_unread'");
    $row=mysqli_fetch_array($q);

     $q3=$d->select("unit_master,block_master,users_master,floors_master,notice_board_read_status","notice_board_read_status.user_id=users_master.user_id AND notice_board_read_status.notice_board_id='$notice_board_id_unread' AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 $blockAppendQuery","ORDER BY block_master.block_sort ASC");
     $userIdArray = array();
     $userIdOnly = array();
     while ($araData=mysqli_fetch_array($q3)) {
       array_push($userIdArray, $araData);
       array_push($userIdOnly, $araData['user_id']);
     }

     $newqry="";
           $aleradyRead= "";
       if($row['block_id']!=0){

          $block_ids = explode(",", $row['block_id']);
          $ids = join("','",$block_ids); 
          $block_id_arr = $block_ids;
          $qry =" and users_master.block_id IN ('$ids') ";
      } 
      if ($row['noticeboard_type']==1) {
          $oldUnitAray = array();
          $q11=$d->select("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id_unread'");
          while($rowUnit=mysqli_fetch_array($q11)) {
            array_push($oldUnitAray, $rowUnit['unit_id']);
          }
          $ids1 = join("','",$oldUnitAray); 
          $newqry =" and users_master.unit_id IN ('$ids1') ";
      }
      if (count($userIdOnly>0)) {
         $ids2 = join("','",$userIdOnly); 
          $ALreadyReadQuery =" and users_master.user_id NOT IN ('$ids2') ";
      }

      
          $title= "Circular for $row[notice_title]";
          $description= "Added By Admin $created_by";

          // $d->insertUserNotification($society_id,$title,$description,"announcement","Circularxxxhdpi.png",$append_query);


         $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'  $qry $newqry $ALreadyReadQuery");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $qry $newqry $ALreadyReadQuery");
         $nResident->noti("AnnouncementFragment","",$society_id,$fcmArray,"Circular","Added By Admin $created_by",'announcement');
         $nResident->noti_ios("NoticeVC","",$society_id,$fcmArrayIos,"Circular","Added By Admin $created_by",'announcement');

         $_SESSION['msg']="Circular Notification Send Successfully ";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Circular Notification Send Successfully");
        header("Location: ../circulars");
        exit();

  }

  
}
 ?>