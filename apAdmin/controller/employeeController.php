<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../../img/$language_id.xml");
$employeeLngName = $xml->string->employee;

//IS_1284
  if ( (isset($employee_type_name_add) && !preg_match('/^[a-z\d\-_\s]+$/i',$employee_type_name_add))  || (isset($employee_type_name_edit) && !preg_match('/^[a-z\d\-_\s]+$/i',$employee_type_name_edit) ) ){
    $_SESSION['msg1']="Special Characters are Not Allowed, please provide Alphanumeric string";
     header("location:../viewEmployeeType");
     exit;
} 
//IS_1284

 if (isset($empTypeDelete)) {

       $q=$d->delete("emp_type_master","society_id='$society_id' AND emp_type_id='$emp_type_id'");
    
      if($q>0) {
         $_SESSION['msg']="$employeeLngName Type Deleted";
        header("location:../viewEmployeeType");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../viewEmployeeType");
      }
  }



 if (isset($rating_id_delete)) {

       $q=$d->delete("employee_rating_master","society_id='$society_id' AND rating_id='$rating_id_delete'");
    
      if($q>0) {
         $_SESSION['msg']="$employeeLngName Rating Deleted";
        header("location:../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
      }
  }



if (isset($setMpinNew) && $mpin!="") {


      $q11 = $d->select("employee_master","emp_id!='$emp_id' AND mpin='$mpin' AND mpin!=0");
        if (mysqli_num_rows($q11)>0) {
             $_SESSION['msg1']="Please Enter Another MPIN ! Try Again";
            header("location:../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
            exit();
        }
        

      $m->set_data('mpin',$mpin);
    
      $a1 = array(
          'mpin'=>$m->get_data('mpin')
      );

      $q=$d->update("employee_master",$a1,"emp_id='$emp_id'");

      if($q>0) {
         $_SESSION['msg']="Mpin Set Successfully";
        header("location:../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
      }

}

if (isset($updateAttendace)) {


        $start_time = date('H:i:s',strtotime($start_time));
        $end_time = date('H:i:s',strtotime($end_time));
      
      if ($ey_attendace==0) {
        $start_time="00:00:00";
        $end_time="00:00:00";
        $duration="0";
      }else {
        $qr_attendace = 0;
      }


        $m->set_data('qr_attendace',$qr_attendace);
        $m->set_data('ey_attendace',$ey_attendace);
        $m->set_data('start_time',$start_time);
        $m->set_data('end_time',$end_time);
        $m->set_data('duration',$duration);
      
        
         $a1= array (
          'ey_attendace'=> $m->get_data('ey_attendace'),
          'qr_attendace'=> $m->get_data('qr_attendace'),
          'start_time'=> $m->get_data('start_time'),
          'end_time'=> $m->get_data('end_time'),
          'duration'=> $m->get_data('duration'),
            
        );

     $q=$d->update("employee_master",$a1,"emp_id='$emp_id'");


      if($q>0) {

        $nGaurd->noti_new("service",$emp_token,"$employeeLngName Eye Attendance Data Updated","By Admin $created_by","");

        $_SESSION['msg']="$employeeLngName Eye Data Updated  ";
        header("location:../getEyeStatus?manageEmployeeEye=eye&emp_id=$emp_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../getEyeStatus?manageEmployeeEye=eye&emp_id=$emp_id");
      }

}

if (isset($updateAttendaceQr)) {


        $start_time = date('H:i:s',strtotime($start_time));
        $end_time = date('H:i:s',strtotime($end_time));
      

      if($qr_attendace==0) {
        $start_time="00:00:00";
        $end_time="00:00:00";
        $duration="0";
      } else {
        $ey_attendace = 0;
      }

      if ($qr_attendace_self==0) {
        $start_time="00:00:00";
        $end_time="00:00:00";
        $duration="0";
      }

        $m->set_data('ey_attendace',$ey_attendace);
        $m->set_data('qr_attendace',$qr_attendace);
        $m->set_data('qr_attendace_self',$qr_attendace_self);
        $m->set_data('start_time',$start_time);
        $m->set_data('end_time',$end_time);
        $m->set_data('duration',$duration);
      
        
         $a1= array (
          'ey_attendace'=> $m->get_data('ey_attendace'),
          'qr_attendace'=> $m->get_data('qr_attendace'),
          'qr_attendace_self'=> $m->get_data('qr_attendace_self'),
          'start_time'=> $m->get_data('start_time'),
          'end_time'=> $m->get_data('end_time'),
          'duration'=> $m->get_data('duration'),
            
        );

     $q=$d->update("employee_master",$a1,"emp_id='$emp_id'");


      if($q>0) {
        $nGaurd->noti_new("service",$emp_token,"$employeeLngName QR Attendance Data Updated","By Admin $created_by","");

         $_SESSION['msg']="$employeeLngName QR Data Updated  ";
        header("location:../getEyeStatus?manageEmployeeEye=qr&emp_id=$emp_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../getEyeStatus?manageEmployeeEye=qr&emp_id=$emp_id");
      }

}

// delete user
if(isset($deleteemployee)) {
  $a22 = array(
      'emp_status'=> 0,
      'private_resource'=> 0,
      'entry_status'=>0,
  );
  $q = $d->update("employee_master",$a22,"emp_id='$emp_id'");

  if($q==TRUE) 
  {
    $m->set_data('emp_id', $emp_id);
    $m->set_data('society_id', $society_id);
    $temDate = date("Y-m-d H:i:s");
    $m->set_data('visit_exit_date_time', $temDate);
    $m->set_data('visit_status', '1');

    $a = array(
        'visit_exit_date_time' => $m->get_data('visit_exit_date_time'),
        'visit_status' => $m->get_data('visit_status'),
        'exit_by' => "",
    );

    $qselect=$d->select("staff_visit_master","emp_id = '$emp_id'","ORDER BY staff_visit_id DESC");

    $data = mysqli_fetch_array($qselect);

    $qdelete = $d->update("staff_visit_master",$a,"emp_id = '$emp_id' AND staff_visit_id = '$data[staff_visit_id]' OR emp_id = '$emp_id' AND visit_status=0");

    $qd=$d->select("employee_unit_master","emp_id='$emp_id'");

    while($employee_unit_masterr_detail=mysqli_fetch_array($qd)) 
    {
      $title =$deleteEmpName." - $employeeLngName Deleted";
      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$employee_unit_masterr_detail['user_id'],
        'notification_title'=>$title,
        'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'employees',
        'notification_logo'=>'buildingResourcesNew.png',
      );
      $d->insert("user_notification",$notiAry);
 
      $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$employee_unit_masterr_detail[user_id]' ");
      $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$employee_unit_masterr_detail[user_id]'  ");
      $nResident->noti("StaffFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'StaffFragment');
      $nResident->noti_ios("ResourcesVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'ResourcesVC');
    }
    // $d->delete("emp_salary_master","emp_id='$emp_id'");
    $d->delete("employee_schedule","emp_id='$emp_id'");
    // $d->delete("staff_visit_master","emp_id='$emp_id'");
    // $d->delete("lost_found_master","gatekeeper_id='$emp_id'");
    $d->delete("chat_master","msg_for='$emp_id' AND sent_to=1");
    $d->delete("chat_master","msg_by='$emp_id' AND send_by=1");
    $d->delete("employee_unit_master","emp_id='$emp_id'");
    $_SESSION['msg']="$employeeLngName Deleted";
    
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName $deleteEmpName Deleted");

    header("Location: ../employees");
  } 
  else 
  {
    header("Location: ../employees");
  }
}



// add new employee
if(isset($addemployee)) {


        $emp_mobile= (int)$emp_mobile;
        if (strlen($emp_mobile)<8 ) {
            $_SESSION['msg1']="Invalid Mobile Number";
            header("Location: ../employee");
            exit;
        }

      if ($emp_type_id!=1) {
       
        if ($emp_type==1) {
            $emp_type_id=$emp_type_id_user;
        }
       $qcb1=$d->select("employee_master","emp_mobile='$emp_mobile' AND emp_type_id='$emp_type_id' ");
       $dataq=mysqli_fetch_array($qcb1);
       if ($dataq>0) {
          $_SESSION['msg1']="Mobile number is Already Register in Same Category !";
          header("location:../employee");
          exit();
       }
      } else {
         $qcb1=$d->select("employee_master","emp_mobile='$emp_mobile' AND emp_type_id!='0' ");
         $dataq=mysqli_fetch_array($qcb1);
         if ($dataq>0) {
            $_SESSION['msg1']="Mobile number is Already Register in Other Category";
            header("location:../employee");
            exit();
         }
      }


      $extension=array("jpeg","jpg","png","gif","JPG","jpeg","JPEG","PNG");
      $uploadedFile = $_FILES['emp_profile']['tmp_name'];
      $ext = pathinfo($_FILES['emp_profile']['name'], PATHINFO_EXTENSION);
      
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {
            $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/emp/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          
          // less 30 % size 
          if ($imageWidth>720) {
            $newWidthPercentage= 720*100 / $imageWidth;  //for maximum 800 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }

          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../employee");
                exit;
                break;
            }
            $emp_profile= $newFileName."_emp.".$ext;

          } else{
            $_SESSION['msg1']="Invalid Profile Photo";
            header("location:../employee");
            exit();
          }
        } else {
          $emp_profile="user.png";
        }
        
        $file = $_FILES['emp_id_proof']['tmp_name'];
        $file11 = $_FILES['emp_id_proof1']['tmp_name'];
        if(file_exists($file)) {
          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['emp_id_proof']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['emp_id_proof']['size'] >= $maxsize) || ($_FILES["emp_id_proof"]["size"] == 0)) {
                $_SESSION['msg1']="Id Proof too large. Must be less than 4 MB.";
                header("location:../employee");
                exit();
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["emp_id_proof"]["type"]))) {
                 $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
                header("location:../employee");
                exit();
            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_id_proof'];   
            $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
            $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../../img/emp_id/".$emp_id_proof);
          } 
        } else{
          $emp_id_proof="";
        }


        if(file_exists($file11)) {
          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['emp_id_proof1']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['emp_id_proof1']['size'] >= $maxsize) || ($_FILES["emp_id_proof1"]["size"] == 0)) {
                $_SESSION['msg1']="Id Proof too large. Must be less than 4 MB.";
                header("location:../employee");
                exit();
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["emp_id_proof1"]["type"]))) {
                 $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
                header("location:../employee");
                exit();
            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_id_proof1'];   
            $temp = explode(".", $_FILES["emp_id_proof1"]["name"]);
            $emp_id_proof1 = $emp_mobile.'_id_back'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_id_proof1"]["tmp_name"], "../../img/emp_id/".$emp_id_proof1);
          } 
        } else{
          $emp_id_proof1="";
        }
        
        if ($emp_type==1) {
            $emp_type_id=$emp_type_id_user;
        } else {
          $block_id = implode(",", $_POST['block_id']);
        } 
        
        if ($emp_type_id==1) {
          $emp_type_id=0;
          function genrateMpin() {
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            return $otp;
          }
           $mpinGenerate = 'false';
            $newmpin=  genrateMpin();
           $q11 = $d->select("employee_master","mpin='$newmpin' AND mpin!=0");
           if (mysqli_num_rows($q11)>0) {
            $mpinGenerate = 'false';
             $newmpin=  genrateMpin();
          } else {
            $mpinGenerate = 'true';
            
          }

          if ($mpinGenerate=='true') {
            $mpin = $newmpin;
          } else {
            $mpin = "";
          }

        }


       
        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('emp_type_id',$emp_type_id);
        $m->set_data('emp_name',$emp_name);
        $m->set_data('emp_profile',$emp_profile);
        $m->set_data('emp_mobile',$emp_mobile);
        $m->set_data('country_code',$country_code);
        $m->set_data('emp_email',$emp_email);
        $m->set_data('emp_address',$emp_address);
        $m->set_data('emp_id_proof',$emp_id_proof);
        $m->set_data('emp_id_proof1',$emp_id_proof1);
        $m->set_data('emp_date_of_joing',$emp_date_of_joing);
        $m->set_data('emp_sallary',$emp_sallary);
        $m->set_data('emp_type',$emp_type);
        $m->set_data('block_id',$block_id);
        $m->set_data('mpin',$mpin);


        // /IS_604
         if(isset($gate_number) && $gate_number!=""){
           $m->set_data('gate_number',$gate_number);
         }else {
           $m->set_data('gate_number','');
         }


         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'emp_type_id'=> $m->get_data('emp_type_id'),
            'emp_name'=> $m->get_data('emp_name'),
            'emp_profile'=> $m->get_data('emp_profile'),
            'emp_mobile'=> $m->get_data('emp_mobile'),
            'country_code'=> $m->get_data('country_code'),
            'emp_email'=> $m->get_data('emp_email'),
            'emp_address'=> $m->get_data('emp_address'),
            'emp_id_proof'=> $m->get_data('emp_id_proof'),
            'emp_id_proof1'=> $m->get_data('emp_id_proof1'),
            'emp_date_of_joing'=> $m->get_data('emp_date_of_joing'),
            'emp_sallary'=> $m->get_data('emp_sallary'),
            'emp_type'=> $m->get_data('emp_type'),
            'gate_number'=> $m->get_data('gate_number'),
            'blocks_id'=> $m->get_data('block_id'),
            'mpin'=> $m->get_data('mpin')
        );
      $q=$d->insert("employee_master",$a1);
      $emp_id = $con->insert_id;
        
        for ($i2=0; $i2 <count($_POST['block_id']) ; $i2++) { 
           $m->set_data('block_id',$_POST['block_id'][$i2]);
           $m->set_data('society_id', $society_id);
           $m->set_data('emp_id', $emp_id);

            $a111 = array(
              'block_id'=>$m->get_data('block_id'),
              'society_id'=>$m->get_data('society_id') ,
              'emp_id'=>$m->get_data('emp_id') 
            );

           $d->insert("employee_block_master",$a111);
        }
  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Added";
      if ($emp_type_id==0) {
       $societyName = $_COOKIE['society_name'];
       if ($country_code=='') {
          $country_code= "+91";
        }
     
       $smsObj->send_welcome_message_gatekeeper($society_id,$emp_name,$emp_mobile,$country_code);
       $d->add_sms_log($emp_mobile,"Security Guard Welcome Message",$society_id,$country_code,2);

      }

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$emp_name $employeeLngName Added");
    header("Location: ../employees");
  } else {
    header("Location: ../employees");
  }
}


// Update  user
if(isset($updateemployee)) {

          # code...
  $emp_mobile= (int)$emp_mobile;
  if (strlen($emp_mobile) < 8) {
    $_SESSION['msg1']="Invalid Mobile Number";
    header("Location: ../employee");
    exit;
  }

         if ($emp_type_id!=1) {

          $qcb1=$d->select("employee_master","emp_id!='$emp_id' AND emp_mobile='$emp_mobile' AND emp_type_id='$emp_type_id'");
           $dataq=mysqli_fetch_array($qcb1);
           if ($dataq>0) {
              $_SESSION['msg1']="Mobile number is Already Register in same category";
              header("location:../employees");
              exit();
           }
         }
         // else {
         //     $qcb1=$d->select("employee_master","emp_id!='$emp_id' AND emp_mobile='$emp_mobile' AND emp_type_id!='0' ");
         //     $dataq=mysqli_fetch_array($qcb1);
         //     if ($dataq>0) {
         //        $_SESSION['msg1']="Mobile number is Already Register in Other Category";
         //        header("location:../employee");
         //        exit();
         //     }
         //  }

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG","jpeg");
      $uploadedFile = $_FILES['emp_profile']['tmp_name'];
      $ext = pathinfo($_FILES['emp_profile']['name'], PATHINFO_EXTENSION);

      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {
            $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand().$user_id;
          $dirPath = "../../img/emp/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          
          // less 30 % size 
           if ($imageWidth>720) {
            $newWidthPercentage= 720*100 / $imageWidth;  //for maximum 800 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_emp.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../employees");
                exit;
                break;
            }
            $emp_profile= $newFileName."_emp.".$ext;
            
          } else{
            $_SESSION['msg1']="Invalid Profile Photo";
            header("location:../employees");
            exit();
          }
        } else {
          $emp_profile=$emp_profile_old;
        }


        $uploadedFile = $_FILES['emp_id_proof']['tmp_name'];
        $ext = pathinfo($_FILES['emp_id_proof']['name'], PATHINFO_EXTENSION);

     
        
        $file = $_FILES['emp_id_proof']['tmp_name'];
        $file11 = $_FILES['emp_id_proof1']['tmp_name'];
        if(file_exists($file)) {

          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['emp_id_proof']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['emp_id_proof']['size'] >= $maxsize) || ($_FILES["emp_id_proof"]["size"] == 0)) {
                $_SESSION['msg1']="Id Proof too large. Must be less than 4 MB.";
                header("location:../employee");
                exit();
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["emp_id_proof"]["type"]))) {
                 $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
                header("location:../employee");
                exit();
            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_id_proof'];   
            $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
            $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../../img/emp_id/".$emp_id_proof);
          } 
        } else{
          $emp_id_proof=$emp_id_proof_old;
        }


        if(file_exists($file11)) {
          $extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
          $extId = pathinfo($_FILES['emp_id_proof1']['name'], PATHINFO_EXTENSION);

           $errors     = array();
            $maxsize    = 4097152;
            
            if(($_FILES['emp_id_proof1']['size'] >= $maxsize) || ($_FILES["emp_id_proof1"]["size"] == 0)) {
                $_SESSION['msg1']="Id Proof too large. Must be less than 4 MB.";
                header("location:../employee");
                exit();
            }
            if(!in_array($extId, $extensionResume) && (!empty($_FILES["emp_id_proof1"]["type"]))) {
                 $_SESSION['msg1']="Invalid File format, Only  JPG,PDF, PNG,Doc are allowed.";
                header("location:../employee");
                exit();
            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_id_proof1'];   
            $temp = explode(".", $_FILES["emp_id_proof1"]["name"]);
            $emp_id_proof1 = $emp_mobile.'_id_back_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_id_proof1"]["tmp_name"], "../../img/emp_id/".$emp_id_proof1);
          } 
        } else{
          $emp_id_proof1=$emp_id_proof_old1;
        }


        if ($emp_type_id==1) {
          $emp_type_id=0;
        }

        if ($emp_type==1) {
          $unit_id= implode(",", $_POST['unit_id']);
        } else {
          $block_id = implode(",", $_POST['block_id']);
        }

        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('emp_type_id',$emp_type_id);
        $m->set_data('emp_type',$emp_type);
        $m->set_data('emp_name',$emp_name);
        $m->set_data('emp_profile',$emp_profile);
        $m->set_data('emp_mobile',$emp_mobile);
        $m->set_data('country_code',$country_code);
        $m->set_data('emp_email',$emp_email);
        $m->set_data('emp_address',$emp_address);
        $m->set_data('emp_id_proof',$emp_id_proof);
        $m->set_data('emp_id_proof1',$emp_id_proof1);
        $m->set_data('emp_date_of_joing',$emp_date_of_joing);
        $m->set_data('emp_sallary',$emp_sallary);
        // $m->set_data('unit_id',$unit_id);
        $m->set_data('block_id',$block_id);
         // /IS_604
        if(isset($gate_number) && $gate_number!=""){
           $m->set_data('gate_number',$gate_number);
         }else {
           $m->set_data('gate_number','');
         }
         $a1= array (
            
          'society_id'=> $m->get_data('society_id'),
            'emp_type_id'=> $m->get_data('emp_type_id'),
            'emp_type'=> $m->get_data('emp_type'),
            'emp_name'=> $m->get_data('emp_name'),
            'emp_profile'=> $m->get_data('emp_profile'),
            'emp_mobile'=> $m->get_data('emp_mobile'),
            'country_code'=> $m->get_data('country_code'),
            'emp_email'=> $m->get_data('emp_email'),
            'emp_address'=> $m->get_data('emp_address'),
            'emp_id_proof'=> $m->get_data('emp_id_proof'),
            'emp_id_proof1'=> $m->get_data('emp_id_proof1'),
            'emp_date_of_joing'=> $m->get_data('emp_date_of_joing'),
            'emp_sallary'=> $m->get_data('emp_sallary'),
            'gate_number'=> $m->get_data('gate_number'),
            'blocks_id'=> $m->get_data('block_id')
        );
        


    $q=$d->update("employee_master",$a1,"emp_id='$emp_id'");
  if($q==TRUE) {
        $d->delete("employee_block_master","emp_id='$emp_id'");

        for ($i2=0; $i2 <count($_POST['block_id']) ; $i2++) { 
           $m->set_data('block_id',$_POST['block_id'][$i2]);
           $m->set_data('society_id', $society_id);
           $m->set_data('emp_id', $emp_id);

            $a111 = array(
              'block_id'=>$m->get_data('block_id'),
              'society_id'=>$m->get_data('society_id') ,
              'emp_id'=>$m->get_data('emp_id') 
            );
            
           $d->insert("employee_block_master",$a111);
        }


    $_SESSION['msg']="$employeeLngName Details Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$emp_name $employeeLngName Details Updated");
    header("Location: ../employee?manageEmployeeUnit=yes&emp_id=$emp_id");
  } else {
    header("Location: ../employees");
  }
}



// add new employee
if(isset($employee_type_name_add)) {
        //IS_1284
       /* $file = $_FILES['emp_type_icon']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if(($_FILES['emp_type_icon']['size'] >= $maxsize) || ($_FILES["emp_type_icon"]["size"] == 0)) {
                $_SESSION['msg1']="Profile photo too large. Must be less than 2 MB.";
                header("location:../employeeType");
                
            }
            if(!in_array($_FILES['emp_type_icon']['type'], $acceptable) && (!empty($_FILES["emp_type_icon"]["type"]))) {
                 $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employeeType");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_type_icon'];   
            $temp = explode(".", $_FILES["emp_type_icon"]["name"]);
            $emp_type_icon = $employee_type_name_add.'_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_type_icon"]["tmp_name"], "../../img/emp_icon/".$emp_type_icon);
          } 
        } else{
          $emp_type_icon="icon.png";
        }*/

        //img start
         $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['emp_type_icon']['tmp_name'];
      $ext = pathinfo($_FILES['emp_type_icon']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName =$employee_type_name_add.'_'.round(microtime(true));
          $dirPath = "../../img/emp_icon/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>400) {
            $newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. ".". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. ".". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. ".". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../employeeType");
                exit;
                break;
            }
            $emp_type_icon= $newFileName.".".$ext;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../employeeType");
          exit();
         }
        } else {
          $emp_type_icon= $emp_type_icon_old;
        }  
        //img end IS_1284
        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('employee_type_name_add',$employee_type_name_add);
        $m->set_data('emp_type_icon',$emp_type_icon);

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'emp_type_name'=> $m->get_data('employee_type_name_add'),
            'emp_type_icon'=> $m->get_data('emp_type_icon'),
        );


  $q=$d->insert("emp_type_master",$a1);
  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Type Added";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Type Added");
    header("Location: ../viewEmployeeType");
  } else {
    header("Location: ../employeeType");
  }
}


// add new employee
if(isset($employee_type_name_edit)) {
        // /IS_1284
       /* $file = $_FILES['emp_type_icon']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if(($_FILES['emp_type_icon']['size'] >= $maxsize) || ($_FILES["emp_type_icon"]["size"] == 0)) {
                $_SESSION['msg1']="Profile photo too large. Must be less than 2 MB.";
                header("location:../employeeType");
                
            }
            if(!in_array($_FILES['emp_type_icon']['type'], $acceptable) && (!empty($_FILES["emp_type_icon"]["type"]))) {
                 $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employeeType");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['emp_type_icon'];   
            $temp = explode(".", $_FILES["emp_type_icon"]["name"]);
            $emp_type_icon = $employee_type_name_edit.'_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["emp_type_icon"]["tmp_name"], "../../img/emp_icon/".$emp_type_icon);
          } 
        } else{
          $emp_type_icon=$emp_type_icon_old;
        }*/
        //img start IS_1284
         $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
      $uploadedFile = $_FILES['emp_type_icon']['tmp_name'];
      $ext = pathinfo($_FILES['emp_type_icon']['name'], PATHINFO_EXTENSION);
      if(file_exists($uploadedFile)) {
        if(in_array($ext,$extension)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName =$employee_type_name_edit.'_'.round(microtime(true));
          $dirPath = "../../img/emp_icon/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth>400) {
            $newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. ".". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. ".". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. ".". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../employeeType");
                exit;
                break;
            }
            $emp_type_icon= $newFileName.".".$ext;
         } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../employeeType");
          exit();
         }
        } else {
          $emp_type_icon= $emp_type_icon_old;
        }  
        //img end
        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('employee_type_name_edit',$employee_type_name_edit);
        $m->set_data('emp_type_icon',$emp_type_icon);

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'emp_type_name'=> $m->get_data('employee_type_name_edit'),
            'emp_type_icon'=> $m->get_data('emp_type_icon'),
        );


    $q=$d->update("emp_type_master",$a1,"emp_type_id='$emp_type_id_edit'");
  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Type Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Type Updated");
    header("Location: ../viewEmployeeType");
  } else {
    header("Location: ../employeeType");
  }
}

// add common type
if (isset($addBulk)) {
  for ($i=0; $i <count($_POST['emp_type_id']); $i++) { 
      $emp_type_id = $_POST['emp_type_id'][$i];
      $qqq=$d->select("emp_type_common","emp_type_id='$emp_type_id'");
      $empData=mysqli_fetch_array($qqq);  


     $m->set_data('society_id',$_COOKIE['society_id']);
     $m->set_data('employee_type_name_add',$empData['emp_type_name']);
     $m->set_data('emp_type_icon',$empData['emp_type_icon']);

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'emp_type_name'=> $m->get_data('employee_type_name_add'),
            'emp_type_icon'=> $m->get_data('emp_type_icon'),
        );

      $q11=$d->select("emp_type_master","society_id='$society_id' AND emp_type_name='$empData[emp_type_name]'");
      if (mysqli_num_rows($q11)==0) {
         $q=$d->insert("emp_type_master",$a1);
      } 
  }

  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Type Added";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Type Added");
    header("Location: ../viewEmployeeType");
  } else {
    $_SESSION['msg1']="Please choose at least 1 option";
    header("Location: ../employeeType");
  }
}


// delete user
if(isset($deleteemployeeType)) {
  $q=$d->delete("emp_type_master","emp_id='$deleteemployeeType'");
  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Type Deleted";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Type Deleted");
    header("Location: ../employeeType");
  } else {
    header("Location: ../employeeType");
  }
}


// add salary employee
if(isset($salary_amount)) {


  /*$count=$d->sum_data("received_amount","receive_maintenance_master","receive_maintenance_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id' OR receive_maintenance_status='2' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row=mysqli_fetch_array($count))
   {
        $asif=$row['SUM(received_amount)'];
      $totalMain=number_format($asif,2,'.','');
           
    }*/

    /*$count1=$d->sum_data("received_amount","receive_bill_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row1=mysqli_fetch_array($count1))
   {
        $asif1=$row1['SUM(received_amount)'];
      $totalBill=number_format($asif1,2,'.','');
           
    }*/

    $count2=$d->sum_data("receive_amount","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row2=mysqli_fetch_array($count2))
   {
        $asif2=$row2['SUM(receive_amount)'];
      $totalFac=number_format($asif2,2,'.','');
           
    }
    $countev=$d->sum_data("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
        while($rowev=mysqli_fetch_array($countev))
       {
            $asif=$rowev['SUM(recived_amount)'];
          $totalEventBooking=number_format($asif,2,'.','');
               
        }

    $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($row=mysqli_fetch_array($count7))
     {
          $asif=$row['SUM(penalty_amount)'];
         $totalPenalaty=number_format($asif,2,'.','');
             
      }

     $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row2=mysqli_fetch_array($icnome))
   {
        $asif5=$row2['SUM(income_amount)'];
      $totalIncome=number_format($asif5,2,'.','');
           
    }
     $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

    $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($row5=mysqli_fetch_array($count4))
     {
          $asif5=$row5['SUM(expenses_amount)'];
        $totalExp=number_format($asif5,2,'.','');
             
      }

      $amount= $totalIncome-$totalExp;

      if ($amount<$salary_amount) {
         $_SESSION['msg1']="Not enough Balance in this balance sheet ";
        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {
        $month_year = $month.'-'.$year;
        $m->set_data('society_id',$society_id);
        $m->set_data('emp_id',$emp_id);
        $m->set_data('month_year',$month_year);
        $m->set_data('month_working_days',$month_working_days);
        $m->set_data('working_days',$working_days);
        $m->set_data('leave_days',$leave_days);
        $m->set_data('salary_amount',$salary_amount);
        $m->set_data('created_date',date("Y-m-d H:i:s"));

       

      $q11=$d->select("emp_salary_master","society_id='$society_id' AND emp_id='$emp_id' AND month_year='$month_year'","");
      $row=mysqli_fetch_array($q11);
      if ($row>0) {
        $_SESSION['msg1']="$month_year Already Created";
        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {

            $expenses_created_date = $month_year;

            $m->set_data('expenses_title',$emp_name." Salary for ".$month_year);
            $m->set_data('expense_category_id',$expense_category_id);
            $m->set_data('expenses_amount',$salary_amount);
            $m->set_data('expenses_add_date',date("Y-m-d H:i:s"));
            $m->set_data('balancesheet_id',$balancesheet_id);
            $m->set_data('expenses_created_date',$expenses_created_date);

            //IS_1686
            $m->set_data('amount_without_gst',$salary_amount);
            $m->set_data('gst',$gst);
            $m->set_data('tax_slab',$tax_slab);
            $m->set_data('is_taxble',$is_taxble);
            $m->set_data('taxble_type',$taxble_type);
            $gst_amount = (($salary_amount * $tax_slab));

            if($gst_amount > 0 ){
              $gst_amount  = $gst_amount /100; 
            } else {
              $gst_amount  = 0 ;
            }
            if($gst=="1"){ 
              $gst_amount =  $salary_amount *$tax_slab/100;  //only for Exclude
              $salary_amount = $salary_amount + $gst_amount;
            } else {
              $gst_amount =  $salary_amount - ($salary_amount * (100/(100+$tax_slab)));  //only for Include
              $salary_amount = $salary_amount;
              $m->set_data('amount_without_gst',($salary_amount -$gst_amount ));
            }

            $m->set_data('expenses_amount',$salary_amount);

            $m->set_data('payment_type',$payment_type);
            $m->set_data('bank_name',$bank_name);
            $m->set_data('payment_ref_no',$payment_ref_no);


            $a2 = array(
                'society_id'=>$society_id,
                'expense_category_id'=>$m->get_data('expense_category_id'),
                'balancesheet_id'=>$m->get_data('balancesheet_id'),
                'expenses_title'=>$m->get_data('expenses_title'),
                'expenses_amount'=>$m->get_data('expenses_amount'),
                'expenses_add_date'=>date("Y-m-d H:i:s"),
                'expenses_created_date'=>$m->get_data('expenses_created_date'),

                //IS_1686
                'gst'=>$m->get_data('gst'),
                'tax_slab'=>$m->get_data('tax_slab'), 
                'amount_without_gst'=>$m->get_data('amount_without_gst'), 
                'is_taxble'=>$m->get_data('is_taxble'), 
                'taxble_type'=>$m->get_data('taxble_type'), 
                'created_date'=>date("Y-m-d H:i:s"), 
               //IS_1686

          
            );
            // print_r($a1);
            $q = $d->insert("expenses_balance_sheet_master",$a2);
            $expenses_balance_sheet_id = $con->insert_id;
            
              $a1= array (
                  'society_id'=> $m->get_data('society_id'),
                  'emp_id'=> $m->get_data('emp_id'),
                  'month_year'=> $m->get_data('month_year'),
                  'month_working_days'=> $m->get_data('month_working_days'),
                  'working_days'=> $m->get_data('working_days'),
                  'leave_days'=> $m->get_data('leave_days'),
                  'salary_amount'=> $m->get_data('salary_amount'),
                  'created_date'=> $m->get_data('created_date'),
                  'expenses_balance_sheet_id'=> $expenses_balance_sheet_id,
                  'created_by'=>$_COOKIE['bms_admin_id'],
                  'payment_type'=> $m->get_data('payment_type'),
                  'bank_name'=> $m->get_data('bank_name'),
                  'payment_ref_no'=> $m->get_data('payment_ref_no'),
               
              );
           
            $q=$d->insert("emp_salary_master",$a1);
      }
      if($q==TRUE) {
        $sq=$d->selectRow("society_name","society_master","society_id='$society_id'");
        $societyData=mysqli_fetch_array($sq);
        $society_name=$societyData['society_name'];

          
         
        $_SESSION['msg']="$employeeLngName Salary Created";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Salary Created");

        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {
        header("Location: ../viewSalary?emp_id=$emp_id");
      }
    }
}



// add salary employee
if(isset($salary_amount_edit)) {

  $salary_amount = $salary_amount_edit;

  
    $count2=$d->sum_data("receive_amount","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row2=mysqli_fetch_array($count2))
   {
        $asif2=$row2['SUM(receive_amount)'];
      $totalFac=number_format($asif2,2,'.','');
           
    }
    $countev=$d->sum_data("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
        while($rowev=mysqli_fetch_array($countev))
       {
            $asif=$rowev['SUM(recived_amount)'];
          $totalEventBooking=number_format($asif,2,'.','');
               
        }

    $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($row=mysqli_fetch_array($count7))
     {
          $asif=$row['SUM(penalty_amount)'];
         $totalPenalaty=number_format($asif,2,'.','');
             
      }

     $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row2=mysqli_fetch_array($icnome))
   {
        $asif5=$row2['SUM(income_amount)'];
      $totalIncome=number_format($asif5,2,'.','');
           
    }
     $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

    $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($row5=mysqli_fetch_array($count4))
     {
          $asif5=$row5['SUM(expenses_amount)'];
        $totalExp=number_format($asif5,2,'.','');
             
      }

      $amount= $totalIncome-$totalExp;

      if($balancesheet_id_old==$balancesheet_id && $salary_amount_old<$salary_amount) {
        $salary_amountCheck = $salary_amount-$salary_amount_old;
      } else {

        $salary_amountCheck = $balancesheet_id_old-$salary_amount;
      }

      if ($amount<$salary_amountCheck) {
         $_SESSION['msg1']="Not enough Balance in this balance sheet ";
        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {
        $month_year = $month.'-'.$year;
        $m->set_data('society_id',$society_id);
        $m->set_data('emp_id',$emp_id);
        $m->set_data('month_year',$month_year);
        $m->set_data('month_working_days',$month_working_days);
        $m->set_data('working_days',$working_days);
        $m->set_data('leave_days',$leave_days);
        $m->set_data('salary_amount',$salary_amount_edit);
        $m->set_data('created_date',date("Y-m-d H:i:s"));

       

      $q11=$d->select("emp_salary_master","society_id='$society_id' AND emp_id='$emp_id' AND month_year='$month_year' AND emp_salary_master!='$emp_salary_master_edit'","");
      $row=mysqli_fetch_array($q11);
      if ($row>0) {
        $_SESSION['msg1']="$month_year Salary Already Created";
        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {

            $expenses_created_date = $month_year;

            $m->set_data('expenses_title',$emp_name." Salary for ".$month_year);
            $m->set_data('expense_category_id',$expense_category_id);
            $m->set_data('expenses_amount',$salary_amount);
            $m->set_data('expenses_add_date',date("Y-m-d H:i:s"));
            $m->set_data('balancesheet_id',$balancesheet_id);
            $m->set_data('expenses_created_date',$expenses_created_date);

            //IS_1686
            $m->set_data('amount_without_gst',$salary_amount);
            $m->set_data('gst',$gst);
            $m->set_data('tax_slab',$tax_slab);
            $m->set_data('is_taxble',$is_taxble);
            $m->set_data('taxble_type',$taxble_type);
            $gst_amount = (($salary_amount * $tax_slab));

            if($gst_amount > 0 ){
              $gst_amount  = $gst_amount /100; 
            } else {
              $gst_amount  = 0 ;
            }
            if($gst=="1"){ 
              $gst_amount =  $salary_amount *$tax_slab/100;  //only for Exclude
              $salary_amount = $salary_amount + $gst_amount;
            } else {
              $gst_amount =  $salary_amount - ($salary_amount * (100/(100+$tax_slab)));  //only for Include
              $salary_amount = $salary_amount;
              $m->set_data('amount_without_gst',($salary_amount -$gst_amount ));
            }

            $m->set_data('expenses_amount',$salary_amount);

             $m->set_data('payment_type',$payment_type);
            $m->set_data('bank_name',$bank_name);
            $m->set_data('payment_ref_no',$payment_ref_no);

 
              $a1= array (
                  'society_id'=> $m->get_data('society_id'),
                  'emp_id'=> $m->get_data('emp_id'),
                  'month_year'=> $m->get_data('month_year'),
                  'month_working_days'=> $m->get_data('month_working_days'),
                  'working_days'=> $m->get_data('working_days'),
                  'leave_days'=> $m->get_data('leave_days'),
                  'salary_amount'=> $m->get_data('salary_amount'),
                  'created_date'=> $m->get_data('created_date'),
                  'expenses_balance_sheet_id'=> $expenses_balance_sheet_id,
                  'created_by'=>$_COOKIE['bms_admin_id'],
                  'payment_type'=> $m->get_data('payment_type'),
                  'bank_name'=> $m->get_data('bank_name'),
                  'payment_ref_no'=> $m->get_data('payment_ref_no'),
               
              );

            $a2 = array(
                'society_id'=>$society_id,
                'expense_category_id'=>$m->get_data('expense_category_id'),
                'balancesheet_id'=>$m->get_data('balancesheet_id'),
                'expenses_title'=>$m->get_data('expenses_title'),
                'expenses_amount'=>$m->get_data('expenses_amount'),
                'expenses_add_date'=>date("Y-m-d"),
                'expenses_created_date'=>$m->get_data('expenses_created_date'),
                //IS_1686
                'gst'=>$m->get_data('gst'),
                'tax_slab'=>$m->get_data('tax_slab'), 
                'amount_without_gst'=>$m->get_data('amount_without_gst'), 
                'is_taxble'=>$m->get_data('is_taxble'), 
                'taxble_type'=>$m->get_data('taxble_type'), 
                'created_date'=>date("Y-m-d H:i:s"), 
               //IS_1686

          
            );
            // print_r($a1);
            $q = $d->update("expenses_balance_sheet_master",$a2,"expenses_balance_sheet_id='$expenses_balance_sheet_id'");


           
            $q=$d->update("emp_salary_master",$a1,"emp_salary_master='$emp_salary_master_edit'");
      }
      if($q==TRUE) {
        $sq=$d->selectRow("society_name","society_master","society_id='$society_id'");
        $societyData=mysqli_fetch_array($sq);
        $society_name=$societyData['society_name'];

         

        $_SESSION['msg']="$employeeLngName Salary Data Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName $emp_name  Salary $month_year ($salary_amount) Data Updated");

        header("Location: ../viewSalary?emp_id=$emp_id");
      } else {
        header("Location: ../viewSalary?emp_id=$emp_id");
      }
    }
}


if (isset($_POST['calSalary'])) {
    $from = $year.'-'.$month.'-1';
    $from= date('Y-m-d', strtotime($from));
    $to=  date("Y-m-t", strtotime($from));
     $qq4=$d->select("staff_visit_master","emp_id='$emp_id' AND filter_data BETWEEN '$from' AND '$to'","GROUP BY filter_data");
     $empWorkingDays= mysqli_num_rows($qq4);

    $q1=$d->select("employee_master","society_id='$society_id' AND emp_id='$emp_id'","");
    $row=mysqli_fetch_array($q1);
    $emp_sallary=$row['emp_sallary'];
    $perDayCharge= round($emp_sallary/$motnhWrokingDays,2);
    $finalSalry = $empWorkingDays * $perDayCharge;
    $leaveDays= $motnhWrokingDays-$empWorkingDays;

    echo  $empWorkingDays.'~'.$leaveDays.'~'.$finalSalry.'~'.$perDayCharge;
  # code...
}

if (isset($_POST['emp_salary_master_delete'])) {

    $q=$d->delete("emp_salary_master","emp_salary_master='$emp_salary_master_delete' AND emp_id='$emp_id'");

  if($q==TRUE) {
  $d->delete("expenses_balance_sheet_master","expenses_balance_sheet_id='$expenses_balance_sheet_id_delete'");
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName-$emp_name Salary $month_year ($salary_amount_deleted) Deleted");
    $_SESSION['msg']="Salary Deleted";
    header("Location: ../viewSalary?emp_id=$emp_id");
  } else {
    header("Location: ../viewSalary?emp_id=$bemp_id");
  }

}
if (isset($emp_logout)) {

    $m->set_data('emp_token','');
      
    $a = array(
      'emp_token'=>$m->get_data('emp_token'),
      'entry_status'=>2,
    );

    $qdelete = $d->update("employee_master",$a,"emp_id='$emp_id'");

    $nGaurd->noti_new("",$emp_token,"Logout","Logout","Logout");

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$empName Security Guard Logout From Back End");

    $_SESSION['msg']="Security Guard logout successfully";
    header("location:../employees");
    exit();

  }


  //IS_1498
  if(isset($EmployeeUserManageBtn)) {
 

  if( (count($_POST['user_data']) + count($_POST['user_data'])) <= 0  ){
    $_SESSION['msg1']="Please Select Atleast One Unit";
          header("location:../employees");
          exit;
  }
     $daily_visitor_unit_master_Added = $d->select("employee_unit_master","emp_id ='$emp_id' ");
       


     for ($i=0; $i < count($_POST['user_data']) ; $i++) {

      $user_data = $_POST['user_data'][$i];
      $user_details = explode("~", $user_data); 
      $unit_id= $user_details[0];
      $user_id= $user_details[1];
      $user_type= $user_details[2];

      // find all family member
      $qf=$d->selectRow("user_id","users_master","user_status=1 AND unit_id='$unit_id' AND user_type='$user_type' AND delete_status=0");
      while ($userData=mysqli_fetch_array($qf)) {
          $user_id= $userData['user_id'];

         $m->set_data('unit_id',$unit_id);
         $m->set_data('user_id',$user_id);
         $m->set_data('society_id', $_COOKIE['society_id']);
         $m->set_data('emp_id', $emp_id);

          $a = array(
            'unit_id'=>$m->get_data('unit_id'),
            'user_id'=>$m->get_data('user_id') ,
            'society_id'=>$m->get_data('society_id') ,
            'emp_id'=>$m->get_data('emp_id') 
          );


          $dc= $d->selectRow("user_id","employee_unit_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id ='$emp_id'");
          if (mysqli_num_rows($dc)) {
            
          } else {
            $q=$d->insert("employee_unit_master",$a);


             $title ="$emp_name - $employeeLngName Added in your $employeeLngName";
           
              $notiAry = array(
                'society_id'=>$society_id,
                'user_id'=>$user_id,
                'notification_title'=>$title,
                'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
                'notification_date'=>date('Y-m-d H:i'),
                'notification_action'=>'employees',
                'notification_logo'=>'Resourcesxxxhdpi.png',
                );
                $d->insert("user_notification",$notiAry);
       
              $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$user_id' ");
              $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$user_id'  ");
                $nResident->noti("StaffFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'StaffFragment');
              $nResident->noti_ios("ResourcesVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'ResourcesVC');
          }
        }
      } 

      //schedule code strat
 
        $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                   for ($i = 0; $i < count($_POST['time_slot_id']); $i++) {

                    $details = explode("~", $_POST['time_slot_id'][$i]);

                    $dayName=trim($details[1]);
                    $time_slot_id_data= $details[0];
                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_id',$emp_id);
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('day',$dayName);
                    $m->set_data('time_slot_id',$time_slot_id_data);
                    $a = array(
                        'society_id' =>$m->get_data('society_id'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'schedule_day'=>$m->get_data('day'),
                        'time_slot_id'=>$m->get_data('time_slot_id'),
                        );
                   
                         $q = $d->insert("employee_schedule",$a);
                     
                }

            
    //schedule code end


  if($q){ 
    
      $_SESSION['msg']="$employeeLngName Unit Added";
      $msg= "$employeeLngName Unit Added";
    
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$msg);
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      } 
    
}

 if(isset($EmployeeUserManageBtnSingle)) {
 

  if( (count($_POST['user_data']) + count($_POST['user_data'])) <= 0  ){
    $_SESSION['msg1']="Please Select Atleast One Unit";
          header("location:../employees");
          exit;
  }
     $daily_visitor_unit_master_Added = $d->select("employee_unit_master","emp_id ='$emp_id' ");
       


     for ($i=0; $i < count($_POST['user_data']) ; $i++) {

      $user_data = $_POST['user_data'][$i];
      $user_details = explode("~", $user_data); 
      $unit_id= $user_details[0];
      $user_id= $user_details[1];
      $user_type= $user_details[2];

      // find all family member
      $qf=$d->selectRow("user_id","users_master","user_status=1 AND unit_id='$unit_id' AND user_type='$user_type' AND delete_status=0 AND user_id='$user_id'");
      while ($userData=mysqli_fetch_array($qf)) {
          $user_id= $userData['user_id'];

         $m->set_data('unit_id',$unit_id);
         $m->set_data('user_id',$user_id);
         $m->set_data('society_id', $_COOKIE['society_id']);
         $m->set_data('emp_id', $emp_id);

          $a = array(
            'unit_id'=>$m->get_data('unit_id'),
            'user_id'=>$m->get_data('user_id') ,
            'society_id'=>$m->get_data('society_id') ,
            'emp_id'=>$m->get_data('emp_id') 
          );


          $dc= $d->selectRow("user_id","employee_unit_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id ='$emp_id' AND user_id='$user_id'");
          if (mysqli_num_rows($dc)) {
            
          } else {
            $q=$d->insert("employee_unit_master",$a);


             $title ="$emp_name - $employeeLngName Added in your resources";
           
              $notiAry = array(
                'society_id'=>$society_id,
                'user_id'=>$user_id,
                'notification_title'=>$title,
                'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
                'notification_date'=>date('Y-m-d H:i'),
                'notification_action'=>'employees',
                'notification_logo'=>'Resourcesxxxhdpi.png',
                );
                $d->insert("user_notification",$notiAry);
       
              $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$user_id' ");
              $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$user_id'  ");
                $nResident->noti("StaffFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'StaffFragment');
              $nResident->noti_ios("ResourcesVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'ResourcesVC');
          }
        }
      } 

      //schedule code strat
 

            
    //schedule code end


  if($q){ 
    
      $_SESSION['msg']="$employeeLngName Unit Added";
      $msg= "$employeeLngName Unit Added";
    
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$msg);
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      } 
    
}

if(isset($editTimeSlotBtn)){
//echo "<pre>";print_r($_POST);exit;
   $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                   for ($i = 0; $i < count($_POST['time_slot_id']); $i++) {
                   $details = explode("~", $_POST['time_slot_id'][$i]);

                    $dayName=trim($details[1]);
                    $time_slot_id_data= $details[0];
                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_id',$emp_id);
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('day',$dayName);
                    $m->set_data('time_slot_id',$time_slot_id_data);
                    $a = array(
                        'society_id' =>$m->get_data('society_id'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'schedule_day'=>$m->get_data('day'),
                        'time_slot_id'=>$m->get_data('time_slot_id'),
                        );
                      $q = $d->insert("employee_schedule",$a);
                     
                }


                // if(count($_POST['time_slot_id']) <= 0  || !isset($_POST['time_slot_id'])){
                //  $q = $d->delete("employee_unit_master","user_id='$user_id' ");
                // }
 
    if($q){ 
    
      $_SESSION['msg']="Time Slot Updated";
      $msg= "Time Slot Updated";
    
         $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$msg);
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      } else {
        $_SESSION['msg']="Time Slot Updated";
        header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
      }         
    //schedule code end
}
if(isset($EmpUserDelete)) {

   $q=$d->select("employee_unit_master","unit_id='$unit_id' AND emp_id='$emp_id'");
    

  $employee_master_qry=$d->select("employee_master","emp_id='$emp_id' ");
  $employee_master_data = mysqli_fetch_array($employee_master_qry);

 
  if($q==TRUE) {

    while($employee_unit_masterr_detail=mysqli_fetch_array($q)) {
        
        $d->delete("employee_schedule"," emp_id ='$employee_unit_masterr_detail[emp_id]' and unit_id='$employee_unit_masterr_detail[unit_id]' ");

        $title =$employee_master_data['emp_name']." - $employeeLngName Deleted";
        $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$employee_unit_masterr_detail['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'employees',
              'notification_logo'=>'Resourcesxxxhdpi.png',
              );
              $d->insert("user_notification",$notiAry);
 
         $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$employee_unit_masterr_detail[user_id]' ");
         $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$employee_unit_masterr_detail[user_id]'  ");
          $nResident->noti("StaffFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'StaffFragment');
        $nResident->noti_ios("ResourcesVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'ResourcesVC');
    }
     
    $q=$d->delete("employee_unit_master","unit_id='$unit_id'  AND emp_id='$emp_id'");

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName Unit $unit_name_remove Removed from $emp_name_unit_remove ");

    $_SESSION['msg']= $xml->string->unit." Removed Successfully";
    header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
  } else { 
    $_SESSION['msg']="Something Wrong";
     header("Location: ../employee?emp_id=".$emp_id."&manageEmployeeUnit=yes");
  }
}
  //IS_1498


if(isset($deleteSingleUser)) {

 
  $q=$d->delete("employee_unit_master","unit_id='$unit_id' AND emp_id='$emp_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}

if(isset($muteResourceNotification)) {
    $a = array(
      'active_status'=>1,
      );
  $q=$d->update("employee_unit_master",$a,"unit_id='$unit_id' AND emp_id='$emp_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}

if(isset($unmuteResourceNotification)) {
    $a = array(
      'active_status'=>0,
      );
  $q=$d->update("employee_unit_master",$a,"unit_id='$unit_id' AND emp_id='$emp_id' AND user_id='$user_id'");
  if($q==TRUE) {
    echo "0";
  } else { 
    echo "1";
  }
}


if(isset($getEmpNotification)) {

    $admin_id = $_COOKIE['bms_admin_id'];

    $a = array(
      'society_id'=>$society_id,
      'emp_id'=>$emp_id,
      'admin_id'=>$admin_id,
      );

    $qc=$d->select("employee_in_out_notification","admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=0");
    if (mysqli_num_rows($qc)>0) {
      $q=$d->update("employee_in_out_notification",$a,"admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=0");
    } else {
      $q=$d->insert("employee_in_out_notification",$a);
    }

  if($q==TRUE) {
    echo "<a href='javascript:void(0)' onclick='notGetEmpNotification($emp_id);' > <i class='fa fa-bell' aria-hidden='true'></i> </a>";
  } else { 
    echo "1";
  }
}


if(isset($NotgetEmpNotification)) {
  $admin_id = $_COOKIE['bms_admin_id'];
  $q=$d->delete("employee_in_out_notification","admin_id='$admin_id' AND emp_id='$emp_id'  AND empType=0");
    
  if($q==TRUE) {
    echo "<a href='javascript:void(0)' onclick='getEmpNotification($emp_id);' > <i class='fa fa-bell-slash' aria-hidden='true'></i> </a>";
  } else { 
    echo "1";
  }
}

if(isset($changeSortBy)) {
   setcookie('sortby', $sortby, time() + (86400 * 365 ), "/$cookieUrl"); // 86400 = 1 day
   header("location:../employees?emp_type=$emp_type&emp_type_id=$emp_type_id");
}

if(isset($changeSortByDaily)) {
   setcookie('sortby', $sortby, time() + (86400 * 365 ), "/$cookieUrl"); // 86400 = 1 day
   header("location:../dailyVisitors");
}

// Restore soft user
if(isset($restoreEmployee)) {

  $a22 =array(
      'emp_status'=> 1,
  );
  $q=$d->update("employee_master",$a22,"emp_id='$emp_id'");
  if($q==TRUE) {
    $_SESSION['msg']="$employeeLngName Restore";

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$employeeLngName $deleteEmpName Restore");

    header("Location: ../employees");
  } else {
    header("Location: ../employees");
  }
}
 ?>