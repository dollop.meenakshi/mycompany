<?php 
include '../common/objectController.php';
extract($_POST);

$floorLngName = $xml->string->floor;
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{

  // add main menu
  if(isset($_POST['unit_type'])){

  // $dataCount = count($_POST['block_name']);
  
   $floor_name = implode("~", $_POST['floor_name']);
   $floor_name1= explode('~', (string)$floor_name);
   $tt =1;
   for ($i=0; $i <$no_of_floor ; $i++) { 
    
      $m->set_data('floor_id',$floor_id);
      $m->set_data('society_id',$society_id);
      $m->set_data('block_id',$block_id);
      $m->set_data('no_of_unit',$no_of_unit);
      $m->set_data('unit_type',$unit_type);
      $m->set_data('floor_name',test_input($floor_name1[$i]));
      $m->set_data('floor_sort',$i+1);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_id'=> $m->get_data('block_id'),
        'floor_name'=> $m->get_data('floor_name'),
        'no_of_unit'=> $m->get_data('no_of_unit'),
        'unit_type'=> $m->get_data('unit_type'),
        'floor_sort'=> $m->get_data('floor_sort'),
      );
      $q=$d->insert("floors_master",$a);
      $floor_id = $con->insert_id;
      if($unit_type=='101') {
        for ($j=1; $j <= $no_of_unit ; $j++) { 
          $k= $i+1;
          if ($j<10) {
            $unit_name= $k."0".$j;
          } else  {
            $unit_name= $k."".$j;
          }
        
          $m->set_data('society_id',$society_id);
          $m->set_data('block_id',$block_id);
          $m->set_data('floor_id',$floor_id);
          $m->set_data('unit_name',test_input($unit_name));
          
          $a5 =array(
            'society_id'=> $m->get_data('society_id'),
            'block_id'=> $m->get_data('block_id'),
            'floor_id'=> $m->get_data('floor_id'),
            'unit_name'=> $m->get_data('unit_name'),
          );

          $d->insert("unit_master",$a5);
        }
      } 
      else if($unit_type=='1') {
        for ($j=1; $j <= $no_of_unit ; $j++) { 
          
          $unit_name= $tt++;
          $m->set_data('society_id',$society_id);
          $m->set_data('block_id',$block_id);
          $m->set_data('floor_id',$floor_id);
          $m->set_data('unit_name',test_input($unit_name));
          
          $a5 =array(
            'society_id'=> $m->get_data('society_id'),
            'block_id'=> $m->get_data('block_id'),
            'floor_id'=> $m->get_data('floor_id'),
            'unit_name'=> $m->get_data('unit_name'),
          );
          $d->insert("unit_master",$a5);
        }
      }else {
        for ($j=1; $j <= $no_of_unit ; $j++) { 
          
          $k= $i+1;
          $unit_name= "Unit ".$j;
          $m->set_data('society_id',$society_id);
          $m->set_data('block_id',$block_id);
          $m->set_data('floor_id',$floor_id);
          $m->set_data('unit_name',test_input($unit_name));
          
          $a5 =array(
            'society_id'=> $m->get_data('society_id'),
            'block_id'=> $m->get_data('block_id'),
            'floor_id'=> $m->get_data('floor_id'),
            'unit_name'=> $m->get_data('unit_name'),
          );
          $d->insert("unit_master",$a5);
        }
      }
     
    }
      if($q>0) {
        $_SESSION['msg']="$floorLngName Added Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$floorLngName Added");
        header("location:../departments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../departments");
      }
  }
  
  if(isset($_POST['addUnitSingle'])){

     $q1 = $d->select("floors_master","society_id='$society_id' AND block_id = '$block_id'","ORDER BY floor_sort DESC");
    $data1 = mysqli_fetch_array($q1);
     $nextShortNumber = $data1['floor_sort']+1;
      

  
  
      
      $m->set_data('floor_id',$floor_id);
      $m->set_data('society_id',$society_id);
      $m->set_data('block_id',$block_id);
      $m->set_data('no_of_unit',$no_of_unit);
      $m->set_data('unit_type',$unit_type_single);
      $m->set_data('floor_name',test_input($floor_name));
      $m->set_data('floor_sort',$nextShortNumber);
      
      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'block_id'=> $m->get_data('block_id'),
        'floor_name'=> $m->get_data('floor_name'),
        'no_of_unit'=> $m->get_data('no_of_unit'),
        'unit_type'=> $m->get_data('unit_type'),
        'floor_sort'=> $m->get_data('floor_sort'),
      );
      $q=$d->insert("floors_master",$a);
      $floor_id = $con->insert_id;

      
      if($q>0) {
        $_SESSION['msg']="$floorLngName Added Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$floorLngName Added");

        header("location:../departments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../departments");
      }
  }


  if(isset($deleteFloor)) {
     $q=$d->delete("floors_master","floor_id='$deleteFloor' AND society_id='$society_id'");
     $q=$d->delete("unit_master","floor_id='$deleteFloor' AND society_id='$society_id'");
     $q=$d->delete("users_master","floor_id='$deleteFloor' AND society_id='$society_id'");
     if($q>0) {
      echo 1;
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$floorLngName Deleted");

     } else {
      echo 0;
     }
  }
  if(isset($updateFloor)) {
          $m->set_data('floor_name',test_input($floor_name));
          
          $a5 =array(
            'floor_name'=> $m->get_data('floor_name'),
          );

     $q=$d->update("floors_master",$a5 ,"floor_id='$floor_id' AND society_id='$society_id'");
      if($q>0) {
        $_SESSION['msg']="$floorLngName Name Updated";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$floorLngName Name Updated");
        
        header("location:../departments");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../departments");
      }
  }

}
else{
  header('location:../login');
 }
 ?>
