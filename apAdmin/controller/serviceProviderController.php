<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) )
{
  if(isset($local_service_provider_id_delete)) {

    $q=$d->delete("local_service_provider_master","local_service_provider_id='$local_service_provider_id_delete' ");
    $q=$d->delete("local_service_provider_users","local_service_master_id='$local_service_provider_id_delete' ");

    if($q>0) {
      echo 1;
    } else {
      echo 0;
    }
  }

  if(isset($local_service_provider_sub_id_delete)) {

    $q=$d->delete("local_service_provider_sub_master","local_service_provider_sub_id='$local_service_provider_sub_id_delete' ");
    $q=$d->delete("local_service_provider_users","local_service_provider_sub_id='$local_service_provider_sub_id_delete' ");

    if($q>0) {
      echo 1;
    } else {
      echo 0;
    }
  }

  if(isset($deletSpCategory)) {

    $q=$d->delete("local_service_provider_users_category","users_category_id='$users_category_id'");

    if($q>0) {
     
      $_SESSION['msg']="Service Provider Category Deleted";
      header("Location: ../addVendor?editSp=editSp&service_provider_users_id=$service_provider_users_id");
    } else {
        $_SESSION['msg1']="Something Wrong";
      header("Location: ../addVendor?editSp=editSp&service_provider_users_id=$service_provider_users_id");

    }
  }

    if(isset($addMoreCategory)) {

        $m->set_data('service_provider_users_id',$service_provider_users_id);
        $m->set_data('service_provider_phone',$service_provider_phone);
        $m->set_data('category_id',$category_id);
        $m->set_data('sub_category_id',$sub_category_id);
        
        $a223 =array(
          'service_provider_users_id'=> $m->get_data('service_provider_users_id'),
          'category_id'=>$m->get_data('category_id'),
          'sub_category_id'=>$m->get_data('sub_category_id'),
        );

        $qqqq=$d->select("local_service_provider_users_category","service_provider_users_id='$service_provider_users_id' AND category_id='$category_id' AND sub_category_id='$sub_category_id'");
        if (mysqli_num_rows($qqqq)>0) {
           $_SESSION['msg1']="Already Added in this category";
          header("Location: ../addVendor?editSp=editSp&service_provider_users_id=$service_provider_users_id");
          exit();

        } else {
          $q= $d->insert("local_service_provider_users_category",$a223);
        }

    if($q>0) {
     
      $_SESSION['msg']="Service Provider Category Added";
      header("Location: ../addVendor?editSp=editSp&service_provider_users_id=$service_provider_users_id");
    } else {
        $_SESSION['msg1']="Something Wrong";
      header("Location: ../addVendor?editSp=editSp&service_provider_users_id=$service_provider_users_id");

    }
  }


  if (isset($addCategory)) {

    $file = $_FILES['service_provider_category_image']['tmp_name'];
    if(file_exists($file)) {
    // checking if main category value was changed
      $errors     = array();
      $maxsize    = 1097152;
      $acceptable = array(
        // 'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_category_image']['size'] >= $maxsize) || ($_FILES["service_provider_category_image"]["size"] == 0)) {
        $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
        header("location:../vendorCategory");
        exit();
      }
      if(!in_array($_FILES['service_provider_category_image']['type'], $acceptable) && (!empty($_FILES["service_provider_category_image"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../vendorCategory");
        exit();
      }
      $ddd=date("ymdhis");
      $image_Arr = $_FILES['service_provider_category_image'];   
      $temp = explode(".", $_FILES["service_provider_category_image"]["name"]);
      $service_provider_category_image = 'spCate'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_category_image"]["tmp_name"], "../../img/local_service_provider/local_service_cat/".$service_provider_category_image);
      $m->set_data('feed_type',1);
    }

    $m->set_data('service_provider_category_name',$service_provider_category_name);
    $m->set_data('service_provider_category_image',$service_provider_category_image);

    $a1 = array(
      'service_provider_category_name'=>$m->get_data('service_provider_category_name'),
      'service_provider_category_image'=>$m->get_data('service_provider_category_image'),
    );

    $q=$d->insert("local_service_provider_master",$a1);

    if($q==TRUE) {
      $_SESSION['msg']="News Category Added";
      header("Location: ../vendorCategory");
    } else {
      header("Location: ../vendorCategory");
    }
  }

  if (isset($addSubCategory)) {

    $file = $_FILES['service_provider_sub_category_image']['tmp_name'];
    if(file_exists($file)) {
      $errors     = array();
      $maxsize    = 1097152;
      $acceptable = array(
        // 'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_sub_category_image']['size'] >= $maxsize) || ($_FILES["service_provider_sub_category_image"]["size"] == 0)) {
        $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
        header("location:../vendorSubcategory");
        exit();
      }
      if(!in_array($_FILES['service_provider_sub_category_image']['type'], $acceptable) && (!empty($_FILES["service_provider_sub_category_image"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../vendorSubcategory");
        exit();
      }
      $ddd=date("ymdhis");
      $image_Arr = $_FILES['service_provider_sub_category_image'];   
      $temp = explode(".", $_FILES["service_provider_sub_category_image"]["name"]);
      $service_provider_sub_category_image = 'spCate'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_sub_category_image"]["tmp_name"], "../../img/local_service_provider/local_service_cat/".$service_provider_sub_category_image);
      $m->set_data('feed_type',1);
    }

    $m->set_data('local_service_provider_id',$local_service_provider_id);
    $m->set_data('service_provider_sub_category_name',$service_provider_sub_category_name);
    $m->set_data('service_provider_sub_category_image',$service_provider_sub_category_image);

    $a1 = array(
      'local_service_provider_id'=>$m->get_data('local_service_provider_id'),
      'service_provider_sub_category_name'=>$m->get_data('service_provider_sub_category_name'),
      'service_provider_sub_category_image'=>$m->get_data('service_provider_sub_category_image'),
    );

    $q=$d->insert("local_service_provider_sub_master",$a1);

    if($q==TRUE) {
      $_SESSION['msg']="News Category Added";
      header("Location: ../vendorSubcategory");
    } else {
      header("Location: ../vendorSubcategory");
    }
  }

  if (isset($editCategory)) {

    $file = $_FILES['service_provider_category_image']['tmp_name'];
    if(file_exists($file)) {
      $errors     = array();
      $maxsize    = 1097152;
      $acceptable = array(
        // 'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_category_image']['size'] >= $maxsize) || ($_FILES["service_provider_category_image"]["size"] == 0)) {
        $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
        header("location:../vendorCategory");
        exit();
      }
      if(!in_array($_FILES['service_provider_category_image']['type'], $acceptable) && (!empty($_FILES["service_provider_category_image"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../vendorCategory");
        exit();
      }
      $ddd=date("ymdhis");
      $image_Arr = $_FILES['service_provider_category_image'];   
      $temp = explode(".", $_FILES["service_provider_category_image"]["name"]);
      $service_provider_category_image = 'spCate'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_category_image"]["tmp_name"], "../../img/local_service_provider/local_service_cat/".$service_provider_category_image);
      $m->set_data('feed_type',1);
    } else {
      $service_provider_category_image=$service_provider_category_image_old;
    }

    $m->set_data('service_provider_category_name',$service_provider_category_name);
    $m->set_data('service_provider_category_image',$service_provider_category_image);

    $a1 = array(
      'service_provider_category_name'=>$m->get_data('service_provider_category_name'),
      'service_provider_category_image'=>$m->get_data('service_provider_category_image'),
    );

    $q=$d->update("local_service_provider_master",$a1,"local_service_provider_id='$local_service_provider_id'");

    if($q==TRUE) {
      $_SESSION['msg']=" Category Updated";
      header("Location: ../vendorCategory");
    } else {
      header("Location: ../vendorCategory");
    }
  }

  if (isset($editSubCategory)) {

    $file = $_FILES['service_provider_sub_category_image']['tmp_name'];
    if(file_exists($file)) {
      $errors     = array();
      $maxsize    = 1097152;
      $acceptable = array(
        // 'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_sub_category_image']['size'] >= $maxsize) || ($_FILES["service_provider_sub_category_image"]["size"] == 0)) {
        $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
        header("location:../vendorCategory");
        exit();
      }
      if(!in_array($_FILES['service_provider_sub_category_image']['type'], $acceptable) && (!empty($_FILES["service_provider_sub_category_image"]["type"]))) {
        $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
        header("location:../vendorCategory");
        exit();
      }
      $ddd=date("ymdhis");
      $image_Arr = $_FILES['service_provider_sub_category_image'];   
      $temp = explode(".", $_FILES["service_provider_sub_category_image"]["name"]);
      $service_provider_sub_category_image = 'spCate'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_sub_category_image"]["tmp_name"], "../../img/local_service_provider/local_service_cat/".$service_provider_sub_category_image);
      $m->set_data('feed_type',1);
    } else {
      $service_provider_sub_category_image=$service_provider_sub_category_image_old;
    }

    $m->set_data('service_provider_sub_category_name',$service_provider_sub_category_name);
    $m->set_data('service_provider_sub_category_image',$service_provider_sub_category_image);

    $a1 = array(
      'service_provider_sub_category_name'=>$m->get_data('service_provider_sub_category_name'),
      'service_provider_sub_category_image'=>$m->get_data('service_provider_sub_category_image'),
    );

    $q=$d->update("local_service_provider_sub_master",$a1,"local_service_provider_sub_id='$local_service_provider_sub_id'");

    if($q==TRUE) {
      $_SESSION['msg']="Sub Category Updated";
      header("Location: ../vendorSubcategory");
    } else {
      header("Location: ../vendorSubcategory");
    }
  }

  if (isset($service_provider_name)) {

    $service_provider_phone= (int)$service_provider_phone;
    if (strlen($service_provider_phone)<10 ) {
      $_SESSION['msg1']="Invalid Mobile Number";
      header("Location: ../addVendor");
      exit;
    }

    $qci=$d->selectRow("service_provider_phone","local_service_provider_users","service_provider_phone='$service_provider_phone'");
    if (mysqli_num_rows($qci)>0) {
      $_SESSION['msg1']="Mobile Number Already Register";
      header("Location: ../addVendor");
      exit;
    }

    $file = $_FILES['id_proof']['tmp_name'];
    if(file_exists($file)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['id_proof']['size'] >= $maxsize) || ($_FILES["id_proof"]["size"] == 0)) {
        $_SESSION['msg1']="Id Proof too large. File must be less than 4 MB.";
        header("location:../addVendor");
        exit();
      }
      if(!in_array($_FILES['id_proof']['type'], $acceptable) && (!empty($_FILES["id_proof"]["type"]))) {
        $_SESSION['msg1']="Invalid Id Proof type. Only  JPG, PNG & PDF types are accepted.";
        header("location:../addVendor");
        exit();
      }
      $ddd=date("ymdhi");
      $image_Arr = $_FILES['id_proof'];   
      $temp = explode(".", $_FILES["id_proof"]["name"]);
      $id_proof = 'idProof'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["id_proof"]["tmp_name"], "../../img/local_service_provider/local_service_id_proof/".$id_proof);
      $m->set_data('kyc_status',1);
      $is_kyc = 1;
    } else {
      $m->set_data('kyc_status',0);
      $is_kyc = 0;
    }

    $file1 = $_FILES['location_proof']['tmp_name'];
    if(file_exists($file1)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['location_proof']['size'] >= $maxsize) || ($_FILES["location_proof"]["size"] == 0)) {
        $_SESSION['msg1']="Address Proof too large. File must be less than 4 MB.";
        header("location:../addVendor");
        exit();
      }
      if(!in_array($_FILES['location_proof']['type'], $acceptable) && (!empty($_FILES["location_proof"]["type"]))) {
        $_SESSION['msg1']="Invalid Address Proof type. Only  JPG, PNG & PDF types are accepted.";
        header("location:../addVendor");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['location_proof'];   
      $temp = explode(".", $_FILES["location_proof"]["name"]);
      $location_proof = 'addProof'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["location_proof"]["tmp_name"], "../../img/local_service_provider/local_service_location_proof/".$location_proof);
    }
    $file5 = $_FILES['brochure_profile']['tmp_name'];
    if(file_exists($file5)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['brochure_profile']['size'] >= $maxsize) || ($_FILES["brochure_profile"]["size"] == 0)) {
        $_SESSION['msg1']="Brochure too large. File must be less than 4 MB.";
        header("location:../addVendor");
        exit();
      }
      if(!in_array($_FILES['brochure_profile']['type'], $acceptable) && (!empty($_FILES["brochure_profile"]["type"]))) {
        $_SESSION['msg1']="Invalid brochure type. Only  JPG & PNG  types are accepted.";
        header("location:../addVendor");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['brochure_profile'];   
      $temp = explode(".", $_FILES["brochure_profile"]["name"]);
      $brochure_profile = 'serProvider'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["brochure_profile"]["tmp_name"], "../../img/local_service_provider/local_service_location_proof/".$brochure_profile);
    } 

    $service_provider_user_image_url = "";
    $file3 = $_FILES['service_provider_user_image']['tmp_name'];
    if(file_exists($file3)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_user_image']['size'] >= $maxsize) || ($_FILES["service_provider_user_image"]["size"] == 0)) {
        $_SESSION['msg1']="Logo too large. File must be less than 4 MB.";
        header("location:../addVendor");
        exit();
      }
      if(!in_array($_FILES['service_provider_user_image']['type'], $acceptable) && (!empty($_FILES["service_provider_user_image"]["type"]))) {
        $_SESSION['msg1']="Invalid logo type. Only  JPG & PNG  types are accepted.";
        header("location:../addVendor");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['service_provider_user_image'];   
      $temp = explode(".", $_FILES["service_provider_user_image"]["name"]);
      $service_provider_user_image = 'serProvider'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_user_image"]["tmp_name"], "../../img/local_service_provider/local_service_users/".$service_provider_user_image);

      $service_provider_user_image_url = $base_url.'img/local_service_provider/local_service_users/'.$service_provider_user_image;
    }

   
    $local_service_master_id = explode("~", $_POST['local_service_master_id']);


    


     $m->set_data('society_id',$society_id);
    $m->set_data('local_service_master_id',$local_service_master_id[0]);
    $m->set_data('local_service_provider_sub_id',$local_service_provider_sub_id);
    $m->set_data('category_name',$local_service_master_id[1]);
    $m->set_data('service_provider_name',$service_provider_name);
    $m->set_data('contact_person_name',$contact_person_name);
    $m->set_data('service_provider_user_image',$service_provider_user_image);
    $m->set_data('service_provider_address',$service_provider_address);
    $m->set_data('service_provider_latitude',$service_provider_latitude);
    $m->set_data('service_provider_logitude',$service_provider_logitude);
    $m->set_data('service_provider_phone',$service_provider_phone);
    $m->set_data('service_provider_email',$service_provider_email);
    $m->set_data('sp_webiste',$sp_webiste);
    $m->set_data('work_description',$work_description);
    $m->set_data('service_provider_zipcode',$service_provider_zipcode);
    $m->set_data('start_date',$start_date);
    $m->set_data('end_date',$end_date);
    $m->set_data('start_time',$start_time);
    $m->set_data('end_time',$end_time);
    $m->set_data('id_proof',$id_proof);
    $m->set_data('gst_no',strtoupper($gst_no));
    $m->set_data('brochure_profile',$brochure_profile);
    $m->set_data('location_proof',$location_proof);
    $m->set_data('country_name',$country_name);
    $m->set_data('state_name',$state_name);
    $m->set_data('city_name',$city_name);
    $m->set_data('country_id',$country_id);
    $m->set_data('state_id',$state_id);
    $m->set_data('city_id',$city_id);
    $m->set_data('service_provider_password','12345');
     


    $a1 = array(
       'society_id'=>$m->get_data('society_id'),
      'local_service_master_id'=>$m->get_data('local_service_master_id'),
      'local_service_provider_sub_id'=>$m->get_data('local_service_provider_sub_id'),
      'category_name'=>$m->get_data('category_name'),
      'service_provider_name'=>$m->get_data('service_provider_name'),
      'contact_person_name'=>$m->get_data('contact_person_name'),
      'service_provider_user_image'=>$m->get_data('service_provider_user_image'),
      'service_provider_address'=>$m->get_data('service_provider_address'),
      'service_provider_latitude'=>$m->get_data('service_provider_latitude'),
      'service_provider_logitude'=>$m->get_data('service_provider_logitude'),
      'service_provider_phone'=>$m->get_data('service_provider_phone'),
      'service_provider_email'=>$m->get_data('service_provider_email'),
      'sp_webiste'=>$m->get_data('sp_webiste'),
      'work_description'=>$m->get_data('work_description'),
      'service_provider_zipcode'=>$m->get_data('service_provider_zipcode'),
      'service_provider_start_date'=>$m->get_data('start_date'),
      'service_provider_end_date'=>$m->get_data('end_date'),
      'open_time'=>$m->get_data('start_time'),
      'close_time'=>$m->get_data('end_time'),
      'id_proof'=>$m->get_data('id_proof'),
      'gst_no'=>$m->get_data('gst_no'),
      'brochure_profile'=>$m->get_data('brochure_profile'),
      'location_proof'=>$m->get_data('location_proof'),
      'country_name'=>$m->get_data('country_name'),
      'state_name'=>$m->get_data('state_name'),
      'city_name'=>$m->get_data('city_name'),
      'country_id'=>$m->get_data('country_id'),
      'state_id'=>$m->get_data('state_id'),
      'city_id'=>$m->get_data('city_id'),
      'kyc_status'=>$m->get_data('kyc_status'),
      'service_provider_password'=>$m->get_data('service_provider_password'),
    );

    $q=$d->insert("local_service_provider_users",$a1);
    $service_provider_users_id = $con->insert_id;

    $m->set_data('service_provider_users_id',$service_provider_users_id);



    if($q==TRUE) {
        $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Vendor Added Successfully");

        $a223 =array(
           'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
          'category_id'=>$m->get_data('local_service_master_id'),
          'sub_category_id'=>$m->get_data('local_service_provider_sub_id'),
        );

        $d->insert("local_service_provider_users_category",$a223);

        $title= "New Vendor $service_provider_name Added";
        $description= "Added in ".$local_service_master_id[1]." category";
        $d->insertUserNotificationVendor($society_id,$title,$description,"vendor","Seervice-providerxxxhdpi.png","",$service_provider_users_id);

       

       $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $bl_qry $appendNew");
       $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $bl_qry  $appendNew");
       $nResident->noti("ServiceProviderFragment","",$society_id,$fcmArray,$title,$description,$service_provider_users_id);
       $nResident->noti_ios("ServiceProviderVC","",$society_id,$fcmArrayIos,$title,$description,$service_provider_users_id);

      $_SESSION['msg']="Vendor Added";
      header("Location: ../vendors");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../vendors");
    }
  }

  if (isset($service_provider_name_edit)) {

    $service_provider_phone= (int)$service_provider_phone;
    if (strlen($service_provider_phone)<10 ) {
      $_SESSION['msg1']="Invalid Mobile Number";
      header("Location: ../vendors");
      exit;
    }
     $qci=$d->selectRow("service_provider_phone","local_service_provider_users","service_provider_phone='$service_provider_phone' AND service_provider_users_id!='$service_provider_users_id'");
    if (mysqli_num_rows($qci)>0) {
      $_SESSION['msg1']="Mobile Number Already Register";
      header("Location: ../vendors");
      exit;
    }

    $file = $_FILES['id_proof']['tmp_name'];
    if(file_exists($file)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['id_proof']['size'] >= $maxsize) || ($_FILES["id_proof"]["size"] == 0)) {
        $_SESSION['msg1']="Id Proof too large. File must be less than 4 MB.";
        header("location:../vendors");
        exit();
      }
      if(!in_array($_FILES['id_proof']['type'], $acceptable) && (!empty($_FILES["id_proof"]["type"]))) {
        $_SESSION['msg1']="Invalid Id Proof type. Only  JPG, PNG & PDF types are accepted.";
        header("location:../vendors");
        exit();
      }
      $ddd=date("ymdhi");
      $image_Arr = $_FILES['id_proof'];   
      $temp = explode(".", $_FILES["id_proof"]["name"]);
      $id_proof = 'idProof'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["id_proof"]["tmp_name"], "../../img/local_service_provider/local_service_id_proof/".$id_proof);
      $m->set_data('kyc_status',1);
    } else {
      if ($id_proof_old=='') {
        $m->set_data('kyc_status',0);
        $id_proof=$id_proof_old;
      } else {
        $m->set_data('kyc_status',1);
        $id_proof=$id_proof_old;
      }
    }

    $file1 = $_FILES['location_proof']['tmp_name'];
    if(file_exists($file1)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['location_proof']['size'] >= $maxsize) || ($_FILES["location_proof"]["size"] == 0)) {
        $_SESSION['msg1']="Address Proof too large. File must be less than 4 MB.";
        header("location:../vendors");
        exit();
      }
      if(!in_array($_FILES['location_proof']['type'], $acceptable) && (!empty($_FILES["location_proof"]["type"]))) {
        $_SESSION['msg1']="Invalid Address Proof type. Only  JPG, PNG & PDF types are accepted.";
        header("location:../vendors");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['location_proof'];   
      $temp = explode(".", $_FILES["location_proof"]["name"]);
      $location_proof = 'addProof'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["location_proof"]["tmp_name"], "../../img/local_service_provider/local_service_location_proof/".$location_proof);
    } else {
      $location_proof=$location_proof_old;
    }


    $file3 = $_FILES['service_provider_user_image']['tmp_name'];
    if(file_exists($file3)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['service_provider_user_image']['size'] >= $maxsize) || ($_FILES["service_provider_user_image"]["size"] == 0)) {
        $_SESSION['msg1']="Logo too large. File must be less than 4 MB.";
        header("location:../vendors");
        exit();
      }
      if(!in_array($_FILES['service_provider_user_image']['type'], $acceptable) && (!empty($_FILES["service_provider_user_image"]["type"]))) {
        $_SESSION['msg1']="Invalid Logo type. Only  JPG & PNG  types are accepted.";
        header("location:../vendors");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['service_provider_user_image'];   
      $temp = explode(".", $_FILES["service_provider_user_image"]["name"]);
      $service_provider_user_image = 'serProvider'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["service_provider_user_image"]["tmp_name"], "../../img/local_service_provider/local_service_users/".$service_provider_user_image);
    } else {
      $service_provider_user_image=$service_provider_user_image_old;

    }

   

    $file5 = $_FILES['brochure_profile']['tmp_name'];
    if(file_exists($file5)) {
      // checking if main category value was changed
      $errors     = array();
      $maxsize    = 4097152;
      $acceptable = array(
        'application/pdf',
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
      );
      if(($_FILES['brochure_profile']['size'] >= $maxsize) || ($_FILES["brochure_profile"]["size"] == 0)) {
        $_SESSION['msg1']="Brochure too large. File must be less than 4 MB.";
        header("location:../vendors");
        exit();
      }
      if(!in_array($_FILES['brochure_profile']['type'], $acceptable) && (!empty($_FILES["brochure_profile"]["type"]))) {
        $_SESSION['msg1']="Invalid brochure type. Only  JPG & PNG  types are accepted.";
        header("location:../vendors");
        exit();
      }

      $ddd=date("ymdhi");
      $image_Arr = $_FILES['brochure_profile'];   
      $temp = explode(".", $_FILES["brochure_profile"]["name"]);
      $brochure_profile = 'serProvider'.$ddd.'.' . end($temp);
      move_uploaded_file($_FILES["brochure_profile"]["tmp_name"], "../../img/local_service_provider/local_service_location_proof/".$brochure_profile);
    } else {
      $brochure_profile=$brochure_profile_old;


    }


    $local_service_master_id = explode("~", $_POST['local_service_master_id']);

     

    // $m->set_data('society_id',$society_id);
    $m->set_data('category_name',$local_service_master_id[1]);
    $m->set_data('service_provider_name',$service_provider_name_edit);
    $m->set_data('contact_person_name',$contact_person_name);
    $m->set_data('service_provider_user_image',$service_provider_user_image);
    $m->set_data('service_provider_address',$service_provider_address);
    $m->set_data('service_provider_latitude',$service_provider_latitude);
    $m->set_data('service_provider_logitude',$service_provider_logitude);
    $m->set_data('service_provider_phone',$service_provider_phone);
    $m->set_data('service_provider_email',$service_provider_email);
    $m->set_data('sp_webiste',$sp_webiste);
    $m->set_data('work_description',$work_description);
    $m->set_data('service_provider_zipcode',$service_provider_zipcode);
    $m->set_data('start_date',$start_date);
    $m->set_data('end_date',$end_date);
    $m->set_data('start_time',$start_time);
    $m->set_data('end_time',$end_time);
    $m->set_data('id_proof',$id_proof);
    $m->set_data('gst_no',strtoupper($gst_no));
    $m->set_data('brochure_profile',$brochure_profile);
    $m->set_data('location_proof',$location_proof);
    $m->set_data('country_name',$country_name);
    $m->set_data('state_name',$state_name);
    $m->set_data('city_name',$city_name);
    $m->set_data('country_id',$country_id);
    $m->set_data('state_id',$state_id);
    $m->set_data('city_id',$city_id);


    $a1 = array(
      'category_name'=>$m->get_data('category_name'),
      'service_provider_name'=>$m->get_data('service_provider_name'),
      'contact_person_name'=>$m->get_data('contact_person_name'),
      'service_provider_user_image'=>$m->get_data('service_provider_user_image'),
      'service_provider_address'=>$m->get_data('service_provider_address'),
      'service_provider_latitude'=>$m->get_data('service_provider_latitude'),
      'service_provider_logitude'=>$m->get_data('service_provider_logitude'),
      'service_provider_phone'=>$m->get_data('service_provider_phone'),
      'service_provider_email'=>$m->get_data('service_provider_email'),
      'sp_webiste'=>$m->get_data('sp_webiste'),
      'work_description'=>$m->get_data('work_description'),
      'service_provider_zipcode'=>$m->get_data('service_provider_zipcode'),
      'service_provider_start_date'=>$m->get_data('start_date'),
      'service_provider_end_date'=>$m->get_data('end_date'),
      'open_time'=>$m->get_data('start_time'),
      'close_time'=>$m->get_data('end_time'),
      'id_proof'=>$m->get_data('id_proof'),
      'gst_no'=>$m->get_data('gst_no'),
      'brochure_profile'=>$m->get_data('brochure_profile'),
      'location_proof'=>$m->get_data('location_proof'),
       'country_name'=>$m->get_data('country_name'),
      'state_name'=>$m->get_data('state_name'),
      'city_name'=>$m->get_data('city_name'),
      'country_id'=>$m->get_data('country_id'),
      'state_id'=>$m->get_data('state_id'),
      'city_id'=>$m->get_data('city_id'),
      'kyc_status'=>$m->get_data('kyc_status'),
    );



    $q=$d->update("local_service_provider_users",$a1,"service_provider_users_id='$service_provider_users_id' ");


    if($q==TRUE) {

      $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Vendor Update Successfully");

       //  $title= "New Vendor $service_provider_name Added";
       //  $description= "Added in ".$local_service_master_id[1]." category";
       //  $d->insertUserNotificationVendor($society_id,$title,$description,"vendor","Seervice-providerxxxhdpi.png","",$service_provider_users_id);

       

       // $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $bl_qry $appendNew");
       // $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $bl_qry  $appendNew");
       // $nResident->noti("ServiceProviderFragment","",$society_id,$fcmArray,$title,$description,$service_provider_users_id);
       // $nResident->noti_ios("ServiceProviderVC","",$society_id,$fcmArrayIos,$title,$description,$service_provider_users_id);

       
      $_SESSION['msg']="Vendor Data Updated";
      header("Location: ../vendors");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../vendors");
    }
  }

  if(isset($society_id_delete_from_sp)){
    $spData=$d->selectArray("local_service_provider_users","service_provider_users_id='$id'");
    $q=$d->delete("common_service_providers","service_provider_users_id='$id' AND society_id='$society_id_delete_from_sp'");

    if($q==TRUE) {
      $_SESSION['msg']="Society Removed Successfully..";
      header("Location: ../CommonServiceProviderSetting?id=$id");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../CommonServiceProviderSetting?id=$id");
    }
  }

  if(isset($CommonServiceProviderBtn)){

    if( (count($_POST['society_id']) + count($_POST['selected_society'])) <= 0  ){
      $_SESSION['msg1']="Please Select Atleast One Society";
      header("location:../vendors");
      exit;
    }
    $common_service_providers_Added = $d->select("common_service_providers","  service_provider_users_id ='$service_provider_users_id' ");
    if(mysqli_num_rows($common_service_providers_Added)>0) {
    }


    for ($i=0; $i < count($_POST['society_id']) ; $i++) {
      $society_id = $_POST['society_id'][$i];
        $m->set_data('service_provider_users_id',$service_provider_users_id);
        $m->set_data('society_id',$society_id);

      $qcd=$d->select("local_service_provider_users_category","service_provider_users_id='$service_provider_users_id'");
      while ($catData=mysqli_fetch_array($qcd)) {
        $users_category_id = $catData['users_category_id'];
        $m->set_data('users_category_id',$users_category_id);


        $a = array(
          'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
          'society_id'=>$m->get_data('society_id'), 
          'users_category_id'=>$m->get_data('users_category_id'), 
        );

        $id = $_POST['society_id'][$i];
        $spData=$d->selectArray("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
        $q=$d->insert("common_service_providers",$a);


      }

    }
    // for ($i=0; $i < count($_POST['selected_society']) ; $i++) {
    //   $m->set_data('service_provider_users_id',$service_provider_users_id);
    //   $m->set_data('society_id',$_POST['selected_society'][$i]);

    //   $a = array(
    //     'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
    //     'society_id'=>$m->get_data('society_id') 
    //   );
    //   $q=$d->insert("common_service_providers",$a);
    // }


    if($q){ 
      if(mysqli_num_rows($common_service_providers_Added)>0) {
        $_SESSION['msg']="Service Provider Associations Updated";
        $msg= "Service Provider Associations Updated";
      }else {
        $_SESSION['msg']="Service Provider Associations Added";
        $msg= "Service Provider Associations Added";
      }
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } 
  }

  if(isset($CommonServiceProviderCountryBtn)){

    if( (count($_POST['country_id'])) <= 0  ){
      $_SESSION['msg1']="Please Select Atleast One State";
      header("location:../vendors");
      exit;
    }
    $common_service_providers_Added = $d->select("common_service_providers","  service_provider_users_id ='$service_provider_users_id' ");
    if(mysqli_num_rows($common_service_providers_Added)>0) {
    }
    $num = 0;
    for ($i=0; $i <count($_POST['country_id']) ; $i++) { 
      $country = $_POST['country_id'][$i];
      $q = $d->select('society_master',"country_id='$country'");
      while($countrydata = mysqli_fetch_array($q)){
        $society = $countrydata['society_id'];
        $m->set_data('service_provider_users_id',$service_provider_users_id);
        $m->set_data('society_id',$society);
        $qcd=$d->select("local_service_provider_users_category","service_provider_users_id='$service_provider_users_id'");
        while ($catData=mysqli_fetch_array($qcd)) {
          $users_category_id = $catData['users_category_id'];
          $m->set_data('users_category_id',$users_category_id);
          $a = array(
            'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
            'society_id'=>$m->get_data('society_id'), 
            'users_category_id'=>$m->get_data('users_category_id'), 
          );
          $c = $d->select('common_service_providers',"service_provider_users_id ='$service_provider_users_id' AND society_id='$society' AND users_category_id='$users_category_id'");
            if(mysqli_num_rows($c)==0){
              if($d->insert("common_service_providers",$a)){
                $num++;
                $spData=$d->selectArray("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
                $msg= "Service Provider ". $spData['service_provider_name'] ." added to the society";
              }
            }              
          }
      }
    }

    if($num>0){
      $_SESSION['msg']="Service Provider Associations Added";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } else {
      $_SESSION['msg1']="No socities in this country or already inserted";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } 
  }

  if(isset($CommonServiceProviderStateBtn)){

    if( (count($_POST['state_id'])) <= 0  ){
      $_SESSION['msg1']="Please Select Atleast One State";
      header("location:../vendors");
      exit;
    }
    $common_service_providers_Added = $d->select("common_service_providers","  service_provider_users_id ='$service_provider_users_id' ");
    if(mysqli_num_rows($common_service_providers_Added)>0) {
      // $q2 = $d->delete("common_service_providers","service_provider_users_id='$service_provider_users_id'");
    }
    $num = 0;
    for ($i=0; $i <count($_POST['state_id']) ; $i++) { 
      $state = $_POST['state_id'][$i];
      $q = $d->select('society_master',"state_id='$state'");
      while($statedata = mysqli_fetch_array($q)){
        $society = $statedata['society_id'];
        $m->set_data('service_provider_users_id',$service_provider_users_id);
        $m->set_data('society_id',$society);
         $qcd=$d->select("local_service_provider_users_category","service_provider_users_id='$service_provider_users_id'");
        while ($catData=mysqli_fetch_array($qcd)) {
          $users_category_id = $catData['users_category_id'];
          $m->set_data('users_category_id',$users_category_id);

          $a = array(
            'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
            'society_id'=>$m->get_data('society_id'), 
            'users_category_id'=>$m->get_data('users_category_id'), 
          );
            $c = $d->select('common_service_providers',"service_provider_users_id ='$service_provider_users_id' AND society_id='$society' AND users_category_id='$users_category_id'");
            if(mysqli_num_rows($c)==0){
              if($d->insert("common_service_providers",$a)){
                $num++;
                $spData=$d->selectArray("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
                $msg= "Service Provider ". $spData['service_provider_name'] ." added to the society";
              }
            } 
          }             
        }
    }

    if($num>0){
      $_SESSION['msg']="Service Provider Associations Added";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } else {
      $_SESSION['msg1']="No socities in this state or already inserted";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } 
  }

  if(isset($CommonServiceProviderCityBtn)){

    if( (count($_POST['city_id'])) <= 0  ){
      $_SESSION['msg1']="Please Select Atleast One City";
      header("location:../vendors");
      exit;
    }
    $common_service_providers_Added = $d->select("common_service_providers","  service_provider_users_id ='$service_provider_users_id' ");
    if(mysqli_num_rows($common_service_providers_Added)>0) {
    // $q2 = $d->delete("common_service_providers","service_provider_users_id='$service_provider_users_id'");
    }


    $num = 0;
    for ($i=0; $i <count($_POST['city_id']) ; $i++) { 
      $city = $_POST['city_id'][$i];
      $q = $d->select('society_master',"city_id='$city'");
      while($citydata = mysqli_fetch_array($q)){
        $society = $citydata['society_id'];
        $m->set_data('service_provider_users_id',$service_provider_users_id);
        $m->set_data('society_id',$society);

         $qcd=$d->select("local_service_provider_users_category","service_provider_users_id='$service_provider_users_id'");
        while ($catData=mysqli_fetch_array($qcd)) {
          $users_category_id = $catData['users_category_id'];
          $m->set_data('users_category_id',$users_category_id);
          $a = array(
            'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
            'society_id'=>$m->get_data('society_id'), 
            'users_category_id'=>$m->get_data('users_category_id'), 
          );
          $c = $d->select('common_service_providers',"service_provider_users_id ='$service_provider_users_id' AND society_id='$society' AND users_category_id='$users_category_id'");
          if(mysqli_num_rows($c)==0){
            if($d->insert("common_service_providers",$a)){
              $num++;
              $spData=$d->selectArray("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
              $msg= "Service Provider ". $spData['service_provider_name'] ." added to the society";
            }
          }  
        }            
      }
    }

    if($num>0){
      $_SESSION['msg']="Service Provider Associations Added";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } else {
      $_SESSION['msg1']="No socities in this city or already inserted";
      header("location:../CommonServiceProviderSetting?id=$service_provider_users_id");
    } 
  }

  if (isset($delete_rating)) {
    // $a = array(
    //   'status'=>1 
    // );
    // $ratingArray = $d->selectArray("local_service_providers_ratings","rating_id='$delete_rating'");
    $query = $d->delete("local_service_providers_ratings","rating_id='$delete_rating'");

    if ($query>0) {
      $society_id = $ratingArray['society_id'];
      $msg = "Ratings by ".$ratingArray['user_name']." deleted";
      $_SESSION['msg']='Rating deleted Successfully';
      header("Location:../serviceProviderDetails?sp_id=$sp_id");
    } else{
      $_SESSION['msg1']='Something Wrong';
      header("Location:../serviceProviderDetails?sp_id=$sp_id");
    }
  }

  if (isset($delete_complain)) {
    $a = array(
      'status'=>1 
    );
    $ComplainArray = $d->selectArray("local_service_provider_complains","complain_id='$delete_complain'");
    $query = $d->update("local_service_provider_complains",$a,"complain_id='$delete_complain'");
    if ($query>0) {
      $society_id = $ComplainArray['society_id'];
      $msg = "Complain by ".$ComplainArray['user_name']." deleted";
      $_SESSION['msg']='Complain deleted Successfully';
      header("Location:../serviceProviderDetails?sp_id=$sp_id");
    } else{
      $_SESSION['msg1']='Something Wrong';
      header("Location:../serviceProviderDetails?sp_id=$sp_id");
    }
  }

  if(isset($publishNotiNew)) { 
      
      $title = html_entity_decode($title);
      $description = html_entity_decode($description);

      $post_log_master = $d->select("sp_post_log_master"," service_provider_users_id = '$service_provider_users_id'  AND status=200 ");
        $success_array = array();
        while ($post_log_master_data = mysqli_fetch_array($post_log_master)) {
            array_push($success_array,$post_log_master_data['society_id']);
        }

       $ids = join("','",$success_array);
       $society_master_qry=$d->select("society_master"," society_id  ='$society_id_post' AND society_id NOT IN ('$ids') ");

       while($society_master_data=mysqli_fetch_array($society_master_qry)) {
         
         $image = $banner_url;

         $target_url = $society_master_data['sub_domain']. "apAdmin/controller/sendNotificationCurlController.php";



         $post = array(
          'sendto' => 'Users',
          'send_notofication' => 'send_notofication_module',
          'society_id' => $society_master_data['society_id'],
          'image' => $image,
          'title' => $title,
          'description'=>$description,
          'notiUrl'=>$image,
          'menu_click'=>'ServiceProviderFragment',

        );


         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL,$target_url);
         curl_setopt($ch, CURLOPT_POST,1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $result=curl_exec ($ch);

            //echo "<pre>";print_r($result);exit;
         curl_close ($ch);
         $json = json_decode($result, true);

         $m->set_data('society_id',$society_master_data['society_id']);
         $m->set_data('service_provider_users_id',$service_provider_users_id);


         $result2 = $json["message"];
         $m->set_data('result',$result2);
         $status = $json["status"];
         $m->set_data('status',$status);
         $m->set_data('created_date',date('Y-m-d H:i:s'));

         if($result2 !=""){
          $a1 = array(
            'society_id'=>$m->get_data('society_id'),
            'service_provider_users_id'=>$m->get_data('service_provider_users_id'),
            'result'=>$m->get_data('result'), 
            'status'=>$m->get_data('status'), 
            'created_at'=>$m->get_data('created_date') 
          );

          $q=$d->insert("sp_post_log_master",$a1);
        }else {
           $a1 = array(
            'society_id'=>$m->get_data('society_id'),
            'kbg_game_id'=>$m->get_data('kbg_game_id'),
            'result'=>'No Response', 
            'status'=>'202', 
            'created_at'=>$m->get_data('created_date') 
          );

          $q=$d->insert("sp_post_log_master",$a1);
        }

        // $_SESSION['msg']="Post Successfully";

        if($result2==""){
          echo " - No Response:201";exit;
        } else {
          echo $json["message"].':'.$json["status"];exit;
        }
      }


  }

  if(isset($changeTimelineStatus)) {

        $m->set_data('service_provider_timeline_status',$service_provider_timeline_status);
        $m->set_data('service_provider_timeline_status_desc',$rejected_remark);
        $m->set_data('rejected_by',$_SESSION[bms_admin_id]);
        
        $a223 =array(
          'service_provider_timeline_status'=> $m->get_data('service_provider_timeline_status'),
          'service_provider_timeline_status_desc'=> $m->get_data('service_provider_timeline_status_desc'),
          'rejected_by'=> $m->get_data('rejected_by'),
        );

        
        $q= $d->update("local_service_providers_timeline",$a223,"service_provider_timeline_id='$service_provider_timeline_id' AND local_service_provider_id='$local_service_provider_id'");

    if($q>0) {
       $title = "Hi your Timeline Request";
       if ($service_provider_timeline_status==1) {
        $description = "Approve by admin";
       } else if($service_provider_timeline_status==2) {
        $description = "Rejected by admin";
       } 
       $nResident->noti("view_timeline","",'0',$fcm_token,$title,$description,"$service_provider_timeline_id");

       $aNoti =array(
          'service_provider_users_id'=> $local_service_provider_id,
          'notification_title'=> $title,
          'notification_description'=> $description,
          'notification_date'=> date("Y-m-d H:i:s"),
          'menu_click'=>'view_timeline'
        );

        $q1 =$d->insert("local_service_provider_notifications",$aNoti);


      $_SESSION['msg']="Service Provider Status Changed";
      header("Location: ../spTimelineRequests");
    } else {
        $_SESSION['msg1']="Something Wrong";
      header("Location: ../spTimelineRequests");

    }
  }


}
else{
  header('location:../login');
}
?>
