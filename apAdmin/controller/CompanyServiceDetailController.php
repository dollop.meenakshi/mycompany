<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addCompanyServiceDetail'])) {

        /* echo "<pre>";
        print_r($_POST);die; */
        $m->set_data('society_id', $society_id);
        $m->set_data('company_service_id', $company_service_id);
        $m->set_data('company_service_details_title', $company_service_details_title);
        $m->set_data('company_service_details_description', $company_service_details_description);
        $m->set_data('company_service_details_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('company_service_details_created_at',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'company_service_id' => $m->get_data('company_service_id'),
            'company_service_details_title' => $m->get_data('company_service_details_title'),
            'company_service_details_description' => $m->get_data('company_service_details_description'),
            'company_service_details_added_by' => $m->get_data('company_service_details_added_by'),
            'company_service_details_created_at' => $m->get_data('company_service_details_created_at'),

        );

        if (isset($company_service_details_id) && $company_service_details_id > 0) {
            $q = $d->update("company_service_details_master", $a1, "company_service_details_id ='$company_service_details_id'");
            $qd=$d->delete("company_service_detail_list_master","company_service_details_id='$company_service_details_id'");
            for($i=0; $i<sizeof($_POST['company_service_detail_list_name']); $i++){
                $m->set_data('society_id', $society_id);
                $m->set_data('company_service_details_id', $company_service_details_id);
                $m->set_data('company_service_detail_list_name', $_POST['company_service_detail_list_name'][$i]);
                $m->set_data('company_service_detail_list_added_by', $_COOKIE['bms_admin_id']);
                $m->set_data('company_service_detail_list_created_at',date("Y-m-d H:i:s"));

                $a2 = array(
                    'society_id' => $m->get_data('society_id'),
                    'company_service_details_id' => $m->get_data('company_service_details_id'),
                    'company_service_detail_list_name' => $m->get_data('company_service_detail_list_name'),
                    'company_service_detail_list_added_by' => $m->get_data('company_service_detail_list_added_by'),
                    'company_service_detail_list_created_at' => $m->get_data('company_service_detail_list_created_at'),
                );

                $q1 = $d->insert("company_service_detail_list_master", $a2);
            }
            $_SESSION['msg'] = "Company Service Detail Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service Detail Updated Successfully");
        } else {
            $q = $d->insert("company_service_details_master", $a1);
            $company_service_details_id = $con->insert_id;
            for($i=0; $i<sizeof($_POST['company_service_detail_list_name']); $i++){
                $m->set_data('society_id', $society_id);
                $m->set_data('company_service_details_id', $company_service_details_id);
                $m->set_data('company_service_detail_list_name', $_POST['company_service_detail_list_name'][$i]);
                $m->set_data('company_service_detail_list_added_by', $_COOKIE['bms_admin_id']);
                $m->set_data('company_service_detail_list_created_at',date("Y-m-d H:i:s"));

                $a2 = array(
                    'society_id' => $m->get_data('society_id'),
                    'company_service_details_id' => $m->get_data('company_service_details_id'),
                    'company_service_detail_list_name' => $m->get_data('company_service_detail_list_name'),
                    'company_service_detail_list_added_by' => $m->get_data('company_service_detail_list_added_by'),
                    'company_service_detail_list_created_at' => $m->get_data('company_service_detail_list_created_at'),
                );

                $q1 = $d->insert("company_service_detail_list_master", $a2);
            }
            $_SESSION['msg'] = "Company Service Detail Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service Detail Added Successfully");

        }
        if ($q == true) {
            header("Location: ../companyServiceDetail");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyServiceDetail");
        }

    }

}
