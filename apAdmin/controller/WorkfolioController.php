<?php
include_once '../lib/dao.php';
include '../lib/model.php';
$d = new dao();
$m = new model();
//$society_id=$_COOKIE['society_id'];
//$society_id=1;
//$default_time_zone=$d->getTimezone($society_id);
//date_default_timezone_set($default_time_zone);

date_default_timezone_set('UTC');

//$workfolio_api_token = array('Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdhbmlzYXRpb25JZCI6IjAzZDUwMjAwLTg4Y2MtMTFlYy05ZjVhLTc3Njc5OWVhMGI3MiIsImRhdGUiOiIyMDIyLTAyLTA4VDEyOjUxOjMwLjE0NFoiLCJpYXQiOjE2NDQzMjQ2OTB9.hqitjYRFHwZQ0mW3JxBjdyLvPVXnHI64TRias9gcf2E');
 
////14-01-2022//
//$workfolio_api_token = array('Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdhbmlzYXRpb25JZCI6IjAzZDUwMjAwLTg4Y2MtMTFlYy05ZjVhLTc3Njc5OWVhMGI3MiIsImRhdGUiOiIyMDIyLTAyLTE0VDExOjQyOjA5Ljg4N1oiLCJpYXQiOjE2NDQ4Mzg5Mjl9.IaTpNvS9oDkQmEiIpek-qLr0l4EpxdLebKwPLDiqEn0');

////15-01-2022//
$workfolio_api_token = array('Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdhbmlzYXRpb25JZCI6IjAzZDUwMjAwLTg4Y2MtMTFlYy05ZjVhLTc3Njc5OWVhMGI3MiIsImRhdGUiOiIyMDIyLTAyLTE1VDA1OjU2OjMzLjIyNVoiLCJpYXQiOjE2NDQ5MDQ1OTN9.sOrbOzcJWHMA6tTtZAA-cjXlacDKPQbD7kYluM0msCQ');

if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    if (isset($_POST['action']) && $_POST['action'] == 'team') {

		$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.workfolio.io/team',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
			CURLOPT_HTTPHEADER =>$workfolio_api_token,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $result = (array) json_decode($response, true);
		
		if(is_array($result)) {
			foreach ($result as $team) {
				$a = array(
                    'teamId' => $team['teamId'],
                    'teamName' => $team['teamName'],
                );
				
				$teamq = $d->selectRow('workfolio_team_id',"workfolio_team","teamId='$team[teamId]'");
             	$teamData = mysqli_fetch_assoc($teamq);
				if($teamData){
					$q = $d->update('workfolio_team', $a, "workfolio_team_id='$teamData[workfolio_team_id]'");
				}else{
					$q = $d->insert("workfolio_team", $a);
				}

			}
        }
        curl_close($curl);
    }

	if (isset($_POST['action']) && $_POST['action'] == 'employees') {

		$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.workfolio.io/employees',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
			CURLOPT_HTTPHEADER =>$workfolio_api_token,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $result = (array) json_decode($response, true);

		if(is_array($result)) {
			foreach ($result as $employee) {
				$a = array(
                    'email' => $employee['email'],
                    'displayName' => $employee['displayName'],
                    'profilePictureUrl' => $employee['profilePictureUrl'],
                );
				foreach ($employee['team'] as $team) {
					$a['teamId'] = $team['teamId'];

					$employeeq = $d->selectRow('workfolio_employee_id',"workfolio_employees","teamId='$team[teamId]' AND email='$employee[email]'");
					$employeeData = mysqli_fetch_assoc($employeeq);
					if($employeeData){
						$q = $d->update('workfolio_employees', $a, "workfolio_employee_id='$employeeData[workfolio_employee_id]'");
					}else{
						$q = $d->insert("workfolio_employees", $a);
					}
				}
			}
        }
        curl_close($curl);
    }

	if (isset($_POST['action']) && $_POST['action'] == 'timesheets') {

		$eq=$d->select("workfolio_employees");
		while ($data=mysqli_fetch_array($eq)) {
			$teamId = $data['teamId'];
			$userEmail = $data['email'];
			//$teamId = "ac66b056-90c3-49c7-9c15-5ce303796b4a";
			//$userEmail = "shubhamdubey.dollop@gmail.com";
			//$userEmail = "dollop.meenakshi@gmail.com";
			//$userEmail = "geetika.dollop@gmail.com";
			//$userEmail = "arunp.dollop@gmail.com";
			//$userEmail = "uzair.dollop@gmail.com";
			//$userEmail = "dev@chplgroup.org";
			date_default_timezone_set('UTC');
			$sDate = "2022-02-23";
			$startDate = 1000 * strtotime($sDate);
			$eDate = "2022-02-23";
			$endDate = 1000 * strtotime($eDate);

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.workfolio.io/timesheets?teamId=$teamId&userEmail=$userEmail&startDate=$startDate&endDate=$endDate",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_HTTPHEADER =>$workfolio_api_token,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
			));

			$response = curl_exec($curl);
			$result = (array) json_decode($response, true);
			//print_r($result);die;
			if(is_array($result)) {
				foreach ($result as $timesheet) {
					if(isset($timesheet['days'])){
						foreach ($timesheet['days'] as $row) {
								date_default_timezone_set('Asia/Calcutta');
								$date = $row['date']==''?'':date("Y-m-d", $row['date']/1000);
								$in = $row['in']==''?'':date("Y-m-d H:i:s", $row['in']/1000);
								$out = $row['out']==''?'':date("Y-m-d H:i:s", $row['out']/1000);
								$a = array(
									'teamId' => $teamId,
									'email' => $userEmail,
									'date' => $date,
									'in_time' => $in,
									'out_time' => $out,
									'breakSec' => $row['breakSec'],
									'workedSec' => $row['workedSec'],
									'idleSec' => $row['idleSec'],
									'dayType' => $row['dayType'],
									'productiveSec' => $row['productiveSec'],
									'unproductiveSec' => $row['unproductiveSec'],
									'neutralSec' => $row['neutralSec'],
									'activeSec' => $row['activeSec'],
									'untrackedSec' => $row['untrackedSec'],
								);
								//print_r($a);
								$timesheetq = $d->selectRow('workfolio_timesheet_id',"workfolio_timesheet","teamId='$teamId' AND email='$userEmail' AND date='$date'");
								$timesheetData = mysqli_fetch_assoc($timesheetq);
								if($timesheetData){
									$q = $d->update('workfolio_timesheet', $a, "workfolio_timesheet_id='$timesheetData[workfolio_timesheet_id]'");
								}else{
									$q = $d->insert("workfolio_timesheet", $a);
								}
							
						}
					}
				}
			}
			curl_close($curl);
		}
    }

	if (isset($_POST['action']) && $_POST['action'] == 'appsAndWebsitesHistory') {

		$eq=$d->select("workfolio_employees","");
		while ($data=mysqli_fetch_array($eq)) {
			$teamId = $data['teamId'];
			$userEmail = $data['email'];
			//$userEmail = "shubhamdubey.dollop@gmail.com";
			//$userEmail = "dollop.meenakshi@gmail.com";
			//$userEmail = "geetika.dollop@gmail.com";
			//$userEmail = "arunp.dollop@gmail.com";
			//$userEmail = "uzair.dollop@gmail.com";
			date_default_timezone_set('UTC');
			$sDate = "2022-02-23";
			$startDate = 1000 * strtotime($sDate);
			$eDate = "2022-02-23";
			$endDate = 1000 * strtotime($eDate);
			$limit = 50;
			$offset = 0;

			do{
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://api.workfolio.io/appsAndWebsitesHistory?teamId=$teamId&userEmail=$userEmail&startDate=$startDate&endDate=$endDate&limit=$limit&offset=$offset",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_HTTPHEADER =>$workfolio_api_token,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
				));

				$response = curl_exec($curl);
				$result = (array) json_decode($response, true);
				//print_r($result);
				if(isset($result['appUsageHistory'])) {
					foreach ($result['appUsageHistory'] as $history) {
						date_default_timezone_set('Asia/Calcutta');
						$date = $history['date']==''?'':date("Y-m-d H:i:s", $history['date']/1000);
						$a = array(
							'teamId' => $teamId,
							'email' => $userEmail,
							'title' => $history['title'],
							'icon' => $history['icon'],
							'productivity_status' => $history['productivityStatus'],
							'window_title' => $history['windowTitle'],
							'date' => $date,
							'totalSec' => $history['totalSec'],
						);
						
						$historyq = $d->selectRow('workfolio_appsAndWebsitesHistory_id',"workfolio_appsandwebsiteshistory","teamId='$teamId' AND email='$userEmail' AND date='$date'");
						$historyData = mysqli_fetch_assoc($historyq);
						if($historyData){
							$q = $d->update('workfolio_appsandwebsiteshistory', $a, "workfolio_appsAndWebsitesHistory_id='$historyData[workfolio_appsAndWebsitesHistory_id]'");
						}else{
							$q = $d->insert("workfolio_appsandwebsiteshistory", $a);
						}
					}
				}
				
				if(isset($result['appUsageHistory'])){
					$offset = $result['nextOffset'];
				}else{
					$offset = 0;
				}
			}while($offset > 0);

			curl_close($curl);
		}
    }

	if (isset($_POST['action']) && $_POST['action'] == 'screenshot') {

		//$eq=$d->select("workfolio_employees","");
		//while ($data=mysqli_fetch_array($eq)) {
			//$teamId = $data['teamId'];
			//$userEmail = $data['email'];
			$teamId = "ac66b056-90c3-49c7-9c15-5ce303796b4a";
			//$teamId = "f6eb5dca-2257-400b-8c5a-774aa7837688";
			$userEmail = "shubhamdubey.dollop@gmail.com";
			//$userEmail = "dollop.meenakshi@gmail.com";
			//$userEmail = "geetika.dollop@gmail.com";
			//$userEmail = "arunp.dollop@gmail.com";
			//$userEmail = "uzair.dollop@gmail.com";
			//$userEmail = "nakrani.jatin@gmail.com";
			//$userEmail = "rathodashu87@gmail.com";
			date_default_timezone_set('UTC');
			$sDate = "2022-02-23";
			$startDate = 1000 * strtotime($sDate);
			$eDate = "2022-02-23";
			$endDate = 1000 * strtotime($eDate);
			$limit = 50;
			$offset = 0;

			//print_r($startDate);
			//print_r("--");
			//print_r($endDate);die;
			do{
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://api.workfolio.io/screenshot?teamId=$teamId&userEmail=$userEmail&startDate=$startDate&endDate=$endDate&limit=$limit&offset=$offset&sortBy=ASC",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_HTTPHEADER =>$workfolio_api_token,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
				));

				$response = curl_exec($curl);
				$result = (array) json_decode($response, true);
				if(isset($result['screenshots'])) {
					foreach ($result['screenshots'] as $screenshot) {
						date_default_timezone_set('Asia/Calcutta');
						$date = $screenshot['date']==''?'':date("Y-m-d H:i:s", $screenshot['date']/1000);
						$a = array(
							'teamId' => $teamId,
							'email' => $userEmail,
							'image_url' => $screenshot['imageUrl'],
							'thumbnail_url' => $screenshot['thumbnailUrl'],
							'app_title' => $screenshot['appTitle'],
							'app_icon' => $screenshot['appIcon'],
							'date' => $date,
						);
						$screenshotq = $d->selectRow('workfolio_screenshot_id',"workfolio_screenshot","teamId='$teamId' AND email='$userEmail' AND date='$date'");
						$screenshotData = mysqli_fetch_assoc($screenshotq);
						if($screenshotData){
							$q = $d->update('workfolio_screenshot', $a, "workfolio_screenshot_id='$screenshotData[workfolio_screenshot_id]'");
						}else{
							$q = $d->insert("workfolio_screenshot", $a);
						}
					}
				}
				
				if(isset($result['screenshots'])){
					$offset = $result['nextOffset'];
				}else{
					$offset = 0;
				}
			}while($offset > 0);
			/* if($q){
				echo "Data Inserted!";
			}else{
				echo "Data Not Inserted!";
			} */
			curl_close($curl);
		//}
    }
}
