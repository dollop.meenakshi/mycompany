<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    //////single leave assign///////
    if (isset($_POST['addLeaveAssign'])) {

        $bId = $block_id;
        $dId = $floor_id;
        $uId = $user_id;
        $ltId = $leave_type_id;
        $laYear = $assign_leave_year;

        $user = $d->selectRow("unit_id,user_full_name,user_designation", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        $user_full_name = $maData['user_full_name'] . ' (' . $maData['user_designation'] . ')';

        $m->set_data('society_id', $society_id);
        $m->set_data('leave_type_id', $leave_type_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('unit_id', $maData['unit_id']);
        $m->set_data('user_total_leave', test_input($user_total_leave));
        $m->set_data('applicable_leaves_in_month', $applicable_leaves_in_month);
        $m->set_data('assign_leave_year', $assign_leave_year);
        $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);
        $m->set_data('user_leave_assign_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'leave_type_id' => $m->get_data('leave_type_id'),
            'user_id' => $m->get_data('user_id'),
            'unit_id' => $m->get_data('unit_id'),
            'user_total_leave' => $m->get_data('user_total_leave'),
            'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
            'assign_leave_year' => $m->get_data('assign_leave_year'),
            'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
            'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),

        );

        $q = $d->insert("leave_assign_master", $a1);
        $_SESSION['msg'] = "Leave Assign Added Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Leave Assign");
        if ($q == true) {
            header("Location: ../leaveAssign?bId=$bId&dId=$dId&uId=$uId&ltId=&year=$laYear");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveAssign");
        }
    }

    //////bulk leave assign///////
    /////////////Dollop Infotech(October 21 2021) by shubham ////////////// 
    if (isset($_POST['addLeaveAssignBulk'])) {
        /* echo "<pre>";
        print_r($_POST);die; */
        $user = $d->selectRow("unit_id, block_id, floor_id,user_full_name,user_designation", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        $bId = $maData['block_id'];
        $dId = $maData['floor_id'];
        $user_full_name = $maData['user_full_name'] . ' (' . $maData['user_designation'] . ')';

        $laYear = $assign_leave_year;

        for ($i = 0; $i < count($_POST['leave_type_id']); $i++) {
            //if($_POST['user_total_leave'][$i] != '' && $_POST['user_total_leave'][$i] > 0){
            if (isset($_POST['carry_forward_leaves']) && $_POST['carry_forward_leaves'][$i] != '') {
                $user_total_leave = $_POST['user_total_leave'][$i] + $_POST['carry_forward_leaves'][$i];
            } else {
                $user_total_leave = $_POST['user_total_leave'][$i];
            }
            $m->set_data('society_id', $society_id);
            $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
            $m->set_data('leave_group_id', $leave_group_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('unit_id', $maData['unit_id']);
            $m->set_data('floor_id', $maData['floor_id']);
            $m->set_data('user_total_leave', test_input($user_total_leave));
            $m->set_data('applicable_leaves_in_month', $_POST['applicable_leaves_in_month'][$i]);
            $m->set_data('assign_leave_year', $assign_leave_year);
            $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);
            $m->set_data('user_leave_assign_date', date("Y-m-d H:i:s"));

            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'leave_type_id' => $m->get_data('leave_type_id'),
                'leave_group_id' => $m->get_data('leave_group_id'),
                'user_id' => $m->get_data('user_id'),
                'unit_id' => $m->get_data('unit_id'),
                'floor_id' => $m->get_data('floor_id'),
                'user_total_leave' => $m->get_data('user_total_leave'),
                'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
                'assign_leave_year' => $m->get_data('assign_leave_year'),
                'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
                'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),

            );
            $q = $d->insert("leave_assign_master", $a1);
            if (isset($_POST['carry_forward_leaves']) && $_POST['carry_forward_leaves'][$i] != '') {
                $m->set_data('society_id', $society_id);
                $m->set_data('user_id', $user_id);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('no_of_payout_leaves', $_POST['carry_forward_leaves'][$i]);
                $m->set_data('leave_payout_remark', "Carry Forward Leaves from $carry_forward_from_year to $assign_leave_year");
                $m->set_data('is_leave_carry_forward', 1);
                $m->set_data('leave_payout_date', date("Y-m-d H:i:s"));
                $m->set_data('leave_payout_year', $carry_forward_from_year);

                $a2 = array(
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'no_of_payout_leaves' => $m->get_data('no_of_payout_leaves'),
                    'leave_payout_remark' => $m->get_data('leave_payout_remark'),
                    'is_leave_carry_forward' => $m->get_data('is_leave_carry_forward'),
                    'leave_payout_date' => $m->get_data('leave_payout_date'),
                    'leave_payout_year' => $m->get_data('leave_payout_year'),
                );

                $q = $d->insert("leave_payout_master", $a2);
            }
            $_SESSION['msg'] = "Leave Assign Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Leave Assign");
            //}
        }
        if ($q == true) {
            header("Location: ../leaveTracker?bId=$bId&dId=$dId&year=$laYear");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveTracker");
        }
    }

    if (isset($_POST['updateLeaveAssign'])) {

        $user = $d->selectRow("user_id,leave_type_id,assign_leave_year", "leave_assign_master", "user_leave_id='$user_leave_id'");
        $maData = mysqli_fetch_array($user);

        $user01 = $d->selectRow("unit_id, block_id, floor_id,user_full_name,user_designation", "users_master", "user_id='$maData[user_id]'");
        $maData01 = mysqli_fetch_array($user01);
        $user_full_name = $maData01['user_full_name'] . ' (' . $maData01['user_designation'] . ')';
        $bId = $maData01['block_id'];
        $dId = $maData01['floor_id'];
        $uId = $maData['user_id'];
        $ltId = $maData['leave_type_id'];
        $laYear = $maData['assign_leave_year'];

        $m->set_data('user_total_leave', test_input($user_total_leave));
        $m->set_data('applicable_leaves_in_month', test_input($applicable_leaves_in_month));

        $a1 = array(
            'user_total_leave' => $m->get_data('user_total_leave'),
            'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
        );

        $q = $d->update("leave_assign_master", $a1, "user_leave_id ='$user_leave_id'");
        $_SESSION['msg'] = "Employee Assign Leave Updated Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Leave Assign");
        if ($q == true) {
            header("Location: ../leaveAssign?bId=$bId&dId=$dId&uId=$uId&ltId=&year=$laYear");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveAssign");
        }
    }

    if (isset($_POST['action']) && $_POST['action'] == "assignUserBulkLeave") {
        $userIdArr = explode(',', $_POST['userIds']);
        $idCount = count($userIdArr);
        for ($i = 0; $i < $idCount; $i++) {
            $user_id = $userIdArr[$i];
            $uq = $d->selectRow('block_id,floor_id,unit_id,user_full_name,user_designation', "users_master", "user_id='$user_id'");
            $uData = mysqli_fetch_array($uq);
            $user_full_name = $uData['user_full_name'] . ' (' . $uData['user_designation'] . ')';
            $ldcq = $d->select("leave_default_count_master", "leave_group_id='$leave_group_id'");
            while ($ldcqData = mysqli_fetch_array($ldcq)) {
                $m->set_data('leave_type_id', $ldcqData['leave_type_id']);
                $m->set_data('society_id', $society_id);
                $m->set_data('leave_group_id', $leave_group_id);
                $m->set_data('user_id', $user_id);
                $m->set_data('unit_id', $uData['unit_id']);
                $m->set_data('floor_id', $uData['floor_id']);
                $m->set_data('user_total_leave', $ldcqData['number_of_leaves']);
                $m->set_data('applicable_leaves_in_month', $ldcqData['leaves_use_in_month']);
                $m->set_data('assign_leave_year', $year);
                $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);
                $m->set_data('user_leave_assign_date', date("Y-m-d H:i:s"));
                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'leave_group_id' => $m->get_data('leave_group_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'user_total_leave' => $m->get_data('user_total_leave'),
                    'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
                    'assign_leave_year' => $m->get_data('assign_leave_year'),
                    'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
                    'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),

                );
                $q = $d->insert("leave_assign_master", $a1);
                $_SESSION['msg'] = "Leave Assign Added Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Leave Assign ");
            }
        }
        if ($q > 0) {
            header("Location: ../leaveTracker?bId=" . $uData['block_id'] . "&dId=" . $uData['floor_id'] . "&year=$year");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveTracker");
        }
    }

    if (isset($_POST['updateLeaveAssignBulk'])) {
        $user = $d->selectRow("unit_id, block_id, floor_id,user_full_name,user_designation", "users_master", "user_id='$user_id'");
        $maData = mysqli_fetch_array($user);
        $bId = $maData['block_id'];
        $dId = $maData['floor_id'];
        $user_full_name = $maData['user_full_name'] . ' (' . $maData['user_designation'] . ')';
        $laYear = $assign_leave_year;
        if (!isset($_POST['del_leave_group_id'])) {
            $q1 = $d->delete("leave_assign_master", "user_id='$user_id' AND assign_leave_year= '$assign_leave_year'");
        }
        for ($i = 0; $i < count($_POST['leave_type_id']); $i++) {
            $user_leave_id = $_POST['user_leave_id'][$i];
            if (isset($_POST['carry_forward_leaves']) && $_POST['carry_forward_leaves'][$i] != '') {
                $user_total_leave = $_POST['user_total_leave'][$i] + $_POST['carry_forward_leaves'][$i];
            } else {
                $user_total_leave = $_POST['user_total_leave'][$i];
            }
            if ($user_leave_id != '' && $user_leave_id > 0) {
                $m->set_data('user_total_leave', $user_total_leave);
                $m->set_data('applicable_leaves_in_month', $_POST['applicable_leaves_in_month'][$i]);

                $a1 = array(
                    'user_total_leave' => $m->get_data('user_total_leave'),
                    'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
                );
                $q = $d->update("leave_assign_master", $a1, "user_leave_id ='$user_leave_id'");
                $_SESSION['msg'] = "Leave Assign Upadate Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Assign Upadate Successfully");

            } else {

                $m->set_data('society_id', $society_id);
                $m->set_data('leave_type_id', $_POST['leave_type_id'][$i]);
                $m->set_data('leave_group_id', $_POST['leave_group_id']);
                $m->set_data('floor_id', $dId);
                $m->set_data('user_id', $user_id);
                $m->set_data('unit_id', $maData['unit_id']);
                $m->set_data('assign_leave_year', $assign_leave_year);
                $m->set_data('user_total_leave', $user_total_leave);
                $m->set_data('applicable_leaves_in_month', $_POST['applicable_leaves_in_month'][$i]);
                $m->set_data('user_leave_assign_date', date("Y-m-d H:i:s"));
                $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'leave_group_id' => $m->get_data('leave_group_id'),
                    'assign_leave_year' => $m->get_data('assign_leave_year'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'user_total_leave' => $m->get_data('user_total_leave'),
                    'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
                    'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),
                    'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
                );
                $q = $d->insert("leave_assign_master", $a1);
            }
        }

        if ($q == true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Bulk Leave Assign Updated");
            header("Location: ../leaveTracker?bId=$bId&dId=$dId&year=$laYear");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveTracker");
        }
    }
    if (isset($_POST['payoutLeave'])) {

        $m->set_data('payment_mode', $society_id);
        $m->set_data('payment_reference_number', $user_id);
        $m->set_data('payment_remark', test_input($payment_remark));
        $m->set_data('payment_date', date("Y-m-d"));
        $a1 = array(
            'leave_payout_status' => 1,
            'payment_mode' => $m->get_data('payment_mode'),
            'payment_reference_number' => $m->get_data('payment_reference_number'),
            'payment_remark' => $m->get_data('payment_remark'),
            'payment_date' => $m->get_data('payment_date'),
        );
        $q = $d->update('leave_payout_master', $a1, "leave_payout_id=$leave_payout_id");
        if ($q == true) {
            header("Location: ../leaveAssign?bId=30&dId=151&uId=1&ltId=&year=2022");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveAssign?bId=30&dId=151&uId=1&ltId=&year=2022");
        }
    }
}
