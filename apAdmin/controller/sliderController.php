<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// add new Notice Board
if(isset($_POST) && !empty($_POST) ){
  if(isset($_POST['deletesliderid'])) {
    $q=$d->delete("app_slider_master","app_slider_id='$deletesliderid'");
    if($q==TRUE) {
        $_SESSION['msg']="Slider Deleted";
        $d->insert_log_specific("0","$_SESSION[bms_admin_id]","$created_by","Slider - $deletesliderid Deleted",2);
        echo 1;
    } else {
        echo 0;
    }
  }

  if(isset($addSliderImage)) {

    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['slider_image']['tmp_name'];
    $ext = pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
    if(in_array($ext,$extension)) {

      $sourceProperties = getimagesize($uploadedFile);
      $newFileName = rand().$user_id;
      $dirPath = "../../img/sliders/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];
      
      // less 30 % size 
      if ($imageWidth>1800) {
          $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1200 widht
          $newImageWidth = $imageWidth * $newWidthPercentage /100;
          $newImageHeight = $imageHeight * $newWidthPercentage /100;
      } else {
          $newImageWidth = $imageWidth;
          $newImageHeight = $imageHeight;
      }

      switch ($imageType) {

          case IMAGETYPE_PNG:
              $imageSrc = imagecreatefrompng($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagepng($tmp,$dirPath. $newFileName. "_banner.". $ext);
              break;           

          case IMAGETYPE_JPEG:
              $imageSrc = imagecreatefromjpeg($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagejpeg($tmp,$dirPath. $newFileName. "_banner.". $ext);
              break;
          
          case IMAGETYPE_GIF:
              $imageSrc = imagecreatefromgif($uploadedFile); 
              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
              imagegif($tmp,$dirPath. $newFileName. "_banner.". $ext);
              break;

          default:
             $_SESSION['msg1']="Invalid Image";
              header("Location: ../sliderImages");
              exit;
              break;
          }
          $slider_image= $newFileName."_banner.".$ext;
      //Make sure we have a file path
    } else {
      $_SESSION['msg1']="Invalid Image....";
      header("Location: ../sliderImages");
      exit;
    }
    $m->set_data('society_id', $society_id);
    $m->set_data('slider_image_name', $slider_image);
    $m->set_data('youtube_url', $youtube_url);
    $m->set_data('page_url', $page_url);
    $m->set_data('page_mobile', $page_mobile);
    $m->set_data('about_offer', $about_offer);
    $m->set_data('order_by', $order_by);
    $m->set_data('order_value', $order_value);
                
    $a = array(
      'society_id' => 0,
      'slider_image_name' => $m->get_data('slider_image_name'),
      'youtube_url' => $m->get_data('youtube_url'),
      'page_url' => $m->get_data('page_url'),
      'page_mobile' => $m->get_data('page_mobile'),
      'about_offer' => $m->get_data('about_offer'),
      'order_by' => $m->get_data('order_by'),
      'order_value' => $m->get_data('order_value'),
    );
    $q = $d->insert("app_slider_master", $a);
    if($q==TRUE) {

        $title= "New Banner Added";
        $description= "$about_offer";
        // $d->insertUserNotification($society_id,$title,$description,"dashboard","home.png","");

       $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $bl_qry $appendNew");
       $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $bl_qry  $appendNew");
       $nResident->noti("","",$society_id,$fcmArray,$title,$description,"dashboard");
       $nResident->noti_ios("","",$society_id,$fcmArrayIos,$title,$description,"dashboard");

        $_SESSION['msg']="New Slider Added...";
        header("Location: ../sliderImages");
    } else {
      $_SESSION['msg1']="Something Went Wrong.";
      header("Location: ../sliderImages");
    }
  }

  if(isset($editSliderImage)) {
    if(!empty($_FILES['slider_image']['name'])){
     $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
     $uploadedFile = $_FILES['slider_image']['tmp_name'];
     $ext = pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
      if(in_array($ext,$extension)) {

        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/sliders/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        
        // less 30 % size 
        if ($imageWidth>1800) {
            $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1200 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
        }

        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_banner.". $ext);
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_banner.". $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_banner.". $ext);
                break;

            default:
               $_SESSION['msg1']="Invalid Image";
                header("Location: ../sliderImages");
                exit;
                break;
            }
            $slider_image= $newFileName."_banner.".$ext;
        //Make sure we have a file path
        } else {
          $_SESSION['msg1']="Invalid Image....";
          header("Location: ../sliderImages");
        }
      }

            // 
                $m->set_data('society_id', $society_id);
                if(!empty($_FILES['slider_image']['name'])){
                  $m->set_data('slider_image_name', $slider_image);
                }
                $m->set_data('youtube_url', $youtube_url);
                $m->set_data('page_url', $page_url);
                $m->set_data('page_mobile', $page_mobile);
                $m->set_data('about_offer', $about_offer);
                $m->set_data('order_by', $order_by);
                $m->set_data('order_value', $order_value);
                if(!empty($_FILES['slider_image']['name'])){
                  $a['slider_image_name'] = $m->get_data('slider_image_name');
                }
                $a['youtube_url'] = $m->get_data('youtube_url');
                $a['page_url'] = $m->get_data('page_url');
                $a['page_mobile'] = $m->get_data('page_mobile');
                $a['about_offer'] = $m->get_data('about_offer');
                $a['order_by'] = $m->get_data('order_by');
                $a['order_value'] = $m->get_data('order_value');
                
                $q = $d->update("app_slider_master", $a,"app_slider_id='$slider_id'");

                

    if($q==TRUE) {
        $_SESSION['msg']="Slider Updated...";
        header("Location: ../sliderImages");
    } else {
      $_SESSION['msg1']="Something Went Wrong.";
      header("Location: ../editSliderImage?id=$slider_id");
    }
  }

  

  

  

  
  if(isset($addSliderImageDefualt)) {
    $file = $_FILES['slider_image']['tmp_name'];
    if(file_exists($file)) {
      $errors     = array();
      $maxsize    = 2097152;
      $acceptable = array(
          // 'application/pdf',
          'image/jpeg',
          'image/jpg',
          'image/png'
      );
      if(($_FILES['slider_image']['size'] >= $maxsize) || ($_FILES["slider_image"]["size"] == 0)) {
           $_SESSION['msg1']="Profile photo too large. Must be less than 2 MB.";
          
      }
      if(!in_array($_FILES['slider_image']['type'], $acceptable) && (!empty($_FILES["slider_image"]["type"]))) {
           $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";

      }
     if(count($errors) === 0) {
        $image_Arr = $_FILES['slider_image'];   
        $temp = explode(".", $_FILES["slider_image"]["name"]);
        $slider_image = round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["slider_image"]["tmp_name"], "../../img/sliders/".$slider_image);
      } 
    } else{
      $slider_image="default.png";
    }
    $m->set_data('society_id', 0);
    $m->set_data('slider_image_name', $slider_image);
                
    $a = array(
      'society_id' => $m->get_data('society_id'),
      'slider_image_name' => $m->get_data('slider_image_name'),
    );
    $q = $d->insert("app_slider_master", $a);
    if($q==TRUE) {
        $_SESSION['msg']="New Default Slider Added...";
        header("Location: ../appSliderImages?society_id=$society_id");
    } else {
      $_SESSION['msg1']="Something Went Wrong.";
      header("Location: ../appSliderImages?society_id=$society_id");
    }
  }

  if(isset($deleteSliderImage)) {

      $q=$d->delete("app_slider_master","app_slider_id='$app_slider_id_delete'");
      if($q==TRUE) {
           $_SESSION['msg']="Slider Deleted";
          header("Location: ../sliderImages?society_id=$society_id");
      } else {
          header("Location: ../sliderImages?society_id=$society_id");
      }
  }

  if(isset($publishNotiNew)) { 
      
      $title = html_entity_decode($title);
      $description = html_entity_decode($description);

      $post_log_master = $d->select("slider_post_log_master"," app_slider_id = '$app_slider_id'  AND status=200 ");
        $success_array = array();
        while ($post_log_master_data = mysqli_fetch_array($post_log_master)) {
            array_push($success_array,$post_log_master_data['society_id']);
        }

       $ids = join("','",$success_array);
       $society_master_qry=$d->select("society_master"," society_id  ='$society_id_post' AND society_id NOT IN ('$ids') ");

       while($society_master_data=mysqli_fetch_array($society_master_qry)) {
         
         $image = $base_url."img/sliders/".$banner_url;

         $target_url = $society_master_data['sub_domain']. "apAdmin/controller/sendNotificationCurlController.php";
          

         $post = array(
          'sendto' => 'Users',
          'send_notofication' => 'send_notofication_module',
          'society_id' => $society_master_data['society_id'],
          'image' => $image,
          'title' => $title,
          'description'=>$description,
          'notiUrl'=>$image,
          'menu_click'=>'',

        );


         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL,$target_url);
         curl_setopt($ch, CURLOPT_POST,1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $result=curl_exec ($ch);

            //echo "<pre>";print_r($result);exit;
         curl_close ($ch);
         $json = json_decode($result, true);

         $m->set_data('society_id',$society_master_data['society_id']);
         $m->set_data('app_slider_id',$app_slider_id);


         $result2 = $json["message"];
         $m->set_data('result',$result2);
         $status = $json["status"];
         $m->set_data('status',$status);
         $m->set_data('created_date',date('Y-m-d H:i:s'));

         if($result2 !=""){
          $a1 = array(
            'society_id'=>$m->get_data('society_id'),
            'app_slider_id'=>$m->get_data('app_slider_id'),
            'result'=>$m->get_data('result'), 
            'status'=>$m->get_data('status'), 
            'created_at'=>$m->get_data('created_date') 
          );

          $q=$d->insert("slider_post_log_master",$a1);
        }else {
           $a1 = array(
            'society_id'=>$m->get_data('society_id'),
            'app_slider_id'=>$m->get_data('app_slider_id  '),
            'result'=>'No Response', 
            'status'=>'202', 
            'created_at'=>$m->get_data('created_date') 
          );

          $q=$d->insert("slider_post_log_master",$a1);
        }

        // $_SESSION['msg']="Post Successfully";

        if($result2==""){
          echo " - No Response:201";exit;
        } else {
          echo $json["message"].':'.$json["status"];exit;
        }
      }


  }
} ?>