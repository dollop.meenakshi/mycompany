<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    //IS_605
    if (isset($_POST['addTimeline'])) {
        $m->set_data('society_id', $society_id);
        $m->set_data('user_id', 0);
        $m->set_data('feed_msg', $feed_msg);
        $file = $_FILES['feed_img']['tmp_name'];
        $timeLineAr = array(
            'society_id' => $society_id,
            'user_id' => $user_id,
            'feed_msg' => $feed_msg,
        );
        if (file_exists($file)) {
            $extensionResume = array("mp4", "MP4", "jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");

            $extId = pathinfo($_FILES['feed_img']['name'], PATHINFO_EXTENSION);
            $errors     = array();
            $maxsize    = 26214400;
            if (($_FILES['feed_img']['size'] >= $maxsize) || ($_FILES["feed_img"]["size"] == 0)) {
                $_SESSION['msg1'] = "File too large. Must be less than 25 MB.";
                header("location:../timeline");
                exit();
            }
            if (!in_array($extId, $extensionResume) && (!empty($_FILES["feed_img"]["type"]))) {

                $_SESSION['msg1'] = "Invalid File format, Only  mp4, MP4,jpeg,jpg,png,gif,JPG,JPEG,PNG " . $extensionResumeSprt . " is allowed.";
                header("location:../timeline");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['feed_img'];
                $temp = explode(".", $_FILES["feed_img"]["name"]);
                $feed_file = 'newsfeed_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["feed_img"]["tmp_name"], "../../img/users/recident_feed/" . $feed_file);
                $extensionVideoResume = array("mp4", "MP4");
                if (in_array($extId, $extensionVideoResume)) {
                    $timeLineAr['feed_video'] = $feed_file;
                    $timeLineAr['feed_img'] = "";
                  $timeLineAr['feed_img_width'] ="";
                  $timeLineAr['feed_img_height'] ="";
                } else {
                    $timeLineAr['feed_img'] = $feed_file;
                    list($width, $height, $type, $attr) = getimagesize($_FILES["feed_img"]['tmp_name']);
                    $timeLineAr['feed_img_width'] =$width;
                    $timeLineAr['feed_img_height'] =$height;
                }
            }
            $timeLineAr['feed_type'] = 1;
        } else {
            $lesson_video = "";
            $timeLineAr['feed_type'] = 0;
        }

        if (isset($society_id) && $society_id > 0) {
            $uSql = $d->selectRow('bms_admin_master.*', "bms_admin_master", "admin_id=$_COOKIE[bms_admin_id]");
            $user = mysqli_fetch_assoc($uSql);
            if ($user) {
                $timeLineAr['block_name'] = 'Admin';
                $timeLineAr['user_type'] = '1';
                $timeLineAr['user_name'] = $user['admin_name'];
            }
            $q = $d->insert("news_feed", $timeLineAr);
        }
        if ($q == true) {
            header("Location: ../timeline");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../timeline");
        }
    }
}
