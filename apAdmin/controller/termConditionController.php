<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract($_POST);
// add new Notice Board
$base_url=$m->base_url();
/*  ECHO mysqli_real_escape_string($conn,$_POST['notice_description']);
ECHO "<PRE>";print_r($_POST);EXIT;*/

if(isset($_POST) && !empty($_POST) ) {
  if(isset($addTermBtn)) {
    $m->set_data('society_id',$society_id);
    $m->set_data('condition_desc',$condition_desc); 
    $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'condition_desc'=> $m->get_data('condition_desc') 
	  );

    //echo "<pre>";print_r($a1);exit;
   $result = $d->insert("invoice_term_conditions_master",$a1); 
    if($result==TRUE) {
      $_SESSION['msg']="Terms & Conditions Added";
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Terms & Conditions Added");
      header("Location: ../invoiceTerms");
    } else {
      header("Location: ../invoiceTerms");
    }
  }


  if(isset($editTermBtn)) {
   
   
     $m->set_data('society_id',$society_id);
    $m->set_data('condition_desc',$condition_desc); 
    $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'condition_desc'=> $m->get_data('condition_desc') 
    );
    $q=$d->update("invoice_term_conditions_master",$a1,"society_id='$society_id' AND invoice_term_condition_id='$invoice_term_condition_id'");
    if($q==TRUE) {
      $_SESSION['msg']="Terms & Conditions Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Terms & Conditions Updated");
      header("Location: ../invoiceTerms");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../invoiceTerms");
    }
  }

    
}
 ?>