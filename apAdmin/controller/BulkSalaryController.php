<?php
include '../common/objectController.php';

if(isset($_REQUEST["ExportSalaryFormat"]))
{
    $q = $d->selectRow("earning_deduction_name,earning_deduction_type","salary_earning_deduction_type_master","salary_earning_deduction_status = 0 AND earn_deduct_is_delete = 0","LIMIT 100");
    $earning_arr = [];
    $earning_str = "";
    $deduction_arr = [];
    $deduction_str = "";
    while($data = $q->fetch_assoc())
    {
        if($data['earning_deduction_type'] == 0)
        {
            $earning_arr[] = $data['earning_deduction_name'];
        }
        elseif($data['earning_deduction_type'] == 1)
        {
            $deduction_arr[] = $data['earning_deduction_name'];
        }
    }
    $earning_str = implode(",",$earning_arr);
    $deduction_str = implode(",",$deduction_arr);
    if($earning_str == "")
    {
        $contents = "Employee Name,Mobile,Branch,Department,Salary Month,Total Deduction,Total Earning,Net Salary,Salary Mode,Month Working Days,Paid Leave,Present Days,$deduction_str\n";
    }
    elseif($deduction_str == "")
    {
        $contents = "Employee Name,Mobile,Branch,Department,Salary Month,Total Deduction,Total Earning,Net Salary,Salary Mode,Month Working Days,Paid Leave,Present Days,$earning_str\n";
    }
    elseif($earning_str == "" && $deduction_str == "")
    {
        $contents = "Employee Name,Mobile,Branch,Department,Salary Month,Total Deduction,Total Earning,Net Salary,Salary Mode,Month Working Days,Paid Leave,Present Days\n";
    }
    else
    {
        $contents = "Employee Name,Mobile,Branch,Department,Salary Month,Total Deduction,Total Earning,Net Salary,Salary Mode,Month Working Days,Paid Leave,Present Days,$earning_str,$deduction_str\n";
    }
    $contents = strip_tags($contents);
    header("Content-Disposition: attachment; filename=salaryImport" . date('Y-m-d-h-i') . ".csv");
    print $contents;exit;
}

if(isset($_POST))
{
    extract($_POST);
    if(isset($_POST['bulkSalary']) && $_POST['bulkSalary'] !="")
    {
        $salary_bulk_test = array('salary_amount'=>100,'user_id'=>$user_id);
        $data = $d->insert('salary_bulk_test',$salary_bulk_test);
    }
    elseif(isset($_POST['importBulkSalary']))
    {
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        if ($ext == 'csv')
        {
            mysqli_autocommit($con,FALSE);
            $flag = true;
            $rows = 0;
            $cells = 0;
            $fp = fopen($_FILES["file"]["tmp_name"], "r");
            $op = 0;
            $osm = 0;
            $other_arr = [];
            while ($content = fgetcsv($fp))
            {
               
                if ($op == 1)
                {
                    if ($content[$op] == "" || empty($content[$op]))
                    {
                        $osm = 1;
                    }
                }
                if($op == 0)
                {
                    for($fc = 12;$fc < 112;$fc++)
                    {
                        if(isset($content[$fc]) && !empty($content[$fc]) && $content[$fc] != "")
                        {
                            $other_arr[] = $content[$fc];
                        }
                    }
                }
                $rows++;
                $cells += count($content);
                $op++;
            }
            
            fclose($fp);
            if ($osm == 1)
            {
                $_SESSION['msg1'] = "Please Enter Data In First Row";
                header("location:../bulkSalary");
                exit;
            }

            if ($rows == 1)
            {
                $_SESSION['msg1'] = "Please Enter Data Atleast In One Row";
                header("location:../bulkSalary");
                exit;
            }
            if ($_FILES["file"]["size"] > 0)
            {
                $flag = true;
                $file = fopen($_FILES["file"]["tmp_name"], "r");
                $no_c = 2;
                while (($getData = fgetcsv($file, 10000, ",")) !== false)
                {
                    $getData = array_map('trim', $getData);
                    if ($flag)
                    {
                        $flag = false;
                        continue;
                    }
                    $employee_name = $getData[0];
                    $mobile = $getData[1];
                    $block_name = $getData[2];
                    $floor_name = $getData[3];
                    $salary_month = $getData[4];
                    $total_deduction = $getData[5];
                    $total_earning = $getData[6];
                    $net_salary = $getData[7];
                    $salary_mode = $getData[8];
                    $month_working_days = $getData[9];
                    $paid_leave = $getData[10];
                    $present_days = $getData[11];

                    if ($employee_name == "" || empty($employee_name) || $employee_name == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Employee Name In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($mobile == "" || empty($mobile) || $mobile == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Mobile Number In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if(!preg_match('/^[0-9]{10}+$/', $mobile))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Mobile Number In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if (isset($mobile) && !empty($mobile) && !ctype_digit($mobile))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Mobile Number In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    $exists = $d->selectRow("user_id","users_master","user_mobile = '$mobile'");
                    if(mysqli_num_rows($exists) == 0)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Mobile Number Does Not Exists In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($block_name == "" || empty($block_name) || $block_name == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Branch Name In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($floor_name == "" || empty($floor_name) || $floor_name == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Department Name In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($salary_month == "" || empty($salary_month) || $salary_month == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Month & Year In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    $new = validateDate($salary_month);
                    if($new != 1)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Month & Year & Valid Format In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if(isset($total_deduction) && !empty($total_deduction) && !is_numeric($total_deduction))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Deduction Amount In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($total_earning == "" || empty($total_earning) || $total_earning == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Total Earning Amount In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if(isset($total_earning) && !empty($total_earning) && !is_numeric($total_earning))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Total Earning Amount In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($net_salary == "" || empty($net_salary) || $net_salary == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Net Salary Amount In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if(isset($net_salary) && !empty($net_salary) && !is_numeric($net_salary))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Net Earning Amount In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($salary_mode == "" || $salary_mode == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Salary Mode Value In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if (!ctype_digit($salary_mode))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Salary Mode Value In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($month_working_days == "" || $month_working_days == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Month Working Days In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if (!ctype_digit($month_working_days))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Month Working Days Value In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($paid_leave == "" || $paid_leave == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Paid Leave In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if (!is_numeric($paid_leave))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Paid Leave Value In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if ($present_days == "" || $present_days == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Present Days In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    if (!is_numeric($present_days))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Present Days Value In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }
                    $dtt = date("n-Y",strtotime($salary_month));
                    $check_q = $d->selectRow("salary_month_name","salary_slip_master AS ssm JOIN users_master AS um ON um.user_id = ssm.user_id","um.user_mobile = '$mobile' AND ssm.salary_month_name = '$dtt'");
                    if(mysqli_num_rows($check_q) > 0)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Salary Already Added In No " . $no_c . " .";
                        header("location:../bulkSalary");exit;
                    }

                    $get_u_id = $d->selectRow("user_id,user_full_name,user_designation,block_id,floor_id","users_master","user_mobile = '$mobile'");
                    $fetch = $get_u_id->fetch_assoc();
                    $user_id = $fetch['user_id'];
                    $user_full_name = $fetch['user_full_name'];
                    $user_designation = $fetch['user_designation'];
                    $block_id = $fetch['block_id'];
                    $floor_id = $fetch['floor_id'];

                    $dtt = date("Y-m-d",strtotime($salary_month));
                    $dtt1 = date("n-Y",strtotime($salary_month));
                    $first_day_this_month = date("Y-m-01", strtotime($dtt));
                    $last_day_this_month  = date("Y-m-t", strtotime($dtt));

                    $per_day_sal = $net_salary / $month_working_days;

                    $salary_slip_master = array(
                        'society_id' => $society_id,
                        'salary_id' => 0,
                        'salary_type_current' => 0,
                        'user_id' => $user_id,
                        'floor_id' => $floor_id,
                        'block_id' => $block_id,
                        'present_branch_name' => $block_name,
                        'present_department_name' => $floor_name,
                        'present_designation_name' => $user_designation,
                        'salary_start_date' => $first_day_this_month,
                        'salary_end_date' => $last_day_this_month,
                        'salary_month_name' => $dtt1,
                        'total_deduction_salary' => $total_deduction,
                        'total_earning_salary' => $total_earning,
                        'total_net_salary' => $net_salary,
                        'month_net_salary' => $net_salary,
                        'salary_mode' => $salary_mode,
                        'total_month_days' => $month_working_days,
                        'paid_leave_days' => $paid_leave,
                        'total_working_days' => $present_days,
                        'per_day_salary' => $per_day_sal,
                        'is_bulk_generated' => 1,
                        'prepared_by' => $_COOKIE['bms_admin_id'],
                        'salary_slip_status' => 2,
                        'created_date' => date("Y-m-d H:i:s")
                    );

                    $q = $d->insert("salary_slip_master", $salary_slip_master);
                    $salary_slip_id = $con->insert_id;
                    $d->insert_log($_COOKIE['bms_admin_id'],$society_id,$user_id,"$created_by", "'$user_full_name' Salary Added (bulk import)");
                    $cnt_other = 12;
                    $json = [];
                    foreach($other_arr AS $key => $value)
                    {
                        $earning_deduction_amount = $getData[$cnt_other];
                        $check_e_d = $d->selectRow("salary_earning_deduction_id,earning_deduction_type","salary_earning_deduction_type_master","earning_deduction_name = '$value' AND society_id = '$society_id' AND salary_earning_deduction_status = 0 AND earn_deduct_is_delete = 0");
                        if(mysqli_num_rows($check_e_d) > 0)
                        {
                            $get_e_d = $check_e_d->fetch_assoc();
                            $json[$get_e_d['salary_earning_deduction_id']] = number_format((float)$earning_deduction_amount, 2, '.', '');
                            $salary_slip_sub_master = array(
                                'salary_slip_id' => $salary_slip_id,
                                'society_id' => $society_id,
                                'user_id' => $user_id,
                                'salary_earning_deduction_id' => $get_e_d['salary_earning_deduction_id'],
                                'earning_deduction_name_current' => $value,
                                'earning_deduction_type_current' => $get_e_d['earning_deduction_type'],
                                'earning_deduction_amount' => $earning_deduction_amount,
                                'created_date' => date("Y-m-d H:i:s")
                            );
                            $q12 = $d->insert("salary_slip_sub_master", $salary_slip_sub_master);
                        }
                        else
                        {
                            mysqli_rollback($con);
                            $_SESSION['msg1'] = "Earning/Deduction Type Does Not Exists In No " . $no_c . " .";
                            header("location:../bulkSalary");exit;
                        }
                        $json_en = json_encode($json);
                        $salary_json = array(
                            'salary_json' => $json_en
                        );
                        $d->update("salary_slip_master", $salary_json, "salary_slip_id = '$salary_slip_id'");
                        $cnt_other++;
                    }
                    $no_c++;
                }
                if ($q12)
                {
                    mysqli_commit($con);
                    $_SESSION['msg'] = "CSV Uploaded Successfully";
                    header("location:../bulkSalary");exit;
                }
                else
                {
                    mysqli_rollback($con);
                    $_SESSION['msg1'] = "Something went wrong!";
                    header("location:../bulkSalary");exit;
                }
            }
            else
            {
                $_SESSION['msg1'] = "Please Upload Valid CSV File";
                header("location:../bulkSalary");
            }
        }
        else
        {
            $_SESSION['msg1'] = "Please Upload CSV File";
            header("location:../bulkSalary");exit;
        }
    }
    elseif(isset($_POST['exportGroupData']))
    {
        $q = $d->selectRow("sgm.salary_group_id,sgm.salary_group_name,sedtm.earning_deduction_name,scvm.amount_value_employeer,scvm.salary_common_value_earn_deduction,scvm.amount_type,scvm.amount_value","salary_group_master AS sgm JOIN salary_common_value_master AS scvm ON scvm.salary_group_id = sgm.salary_group_id JOIN salary_earning_deduction_type_master AS sedtm ON scvm.salary_earning_deduction_id = sedtm.salary_earning_deduction_id","scvm.salary_group_id = '$salary_group_id'");
        $all = [];
        $str = "";
       
        while($data = $q->fetch_assoc())
        {
            $sg = $data['salary_group_name'];
            $sgi = $data['salary_group_id'];
            if($data['salary_common_value_earn_deduction'] !=""){
                $subEarnValue = $d->selectRow('GROUP_CONCAT( DISTINCT earning_deduction_name  SEPARATOR " " ) AS earning_deduction_name','salary_earning_deduction_type_master',"salary_earning_deduction_id IN ($data[salary_common_value_earn_deduction])");
                
                $subEarnValueData =  mysqli_fetch_assoc($subEarnValue);
                $earn_show_name = "(".$subEarnValueData['earning_deduction_name'].")";
            }else{
                $earn_show_name = '';
            }
            if($data['amount_type']==0){
                $amount = "(".$data['amount_value']." %".")";
            }else if($data['amount_type']==1){
                $amount = "(".$data['amount_value']." Flat".")";
            }else{
                $amount = "(Slab)";
            }
            $all[] = $data['earning_deduction_name'].'~'.$amount.$earn_show_name;
            if($data['amount_value_employeer']>0){
                $all[] = $data['earning_deduction_name'].'~'."Employer Contribution~".$amount.$earn_show_name;
            }

            ///print_r($data);
        }
      
        $str = implode(",",$all);
     //   $contents = "Employee Mobile,Employee Name,Salary Type,Salary Start Date,Next Increament Date,Gross Salary,$str\n";
        $contents = "Employee Mobile,Employee Name,Salary Type,Salary Start Date,Next Increament Date,Gross Salary";
        // print_r($contents);  
        // die;
        $contents = strip_tags($contents);
        header("Content-Disposition: attachment; filename=".$sg."salaryGroupValues" . date('Y-m-d-h-i') . ".csv");
        print $contents;exit;
    }
    elseif(isset($_POST['importCTCBulk']))
    {
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        if ($ext == 'csv')
        {
            mysqli_autocommit($con,FALSE);
            $flag = true;
            $rows = 0;
            $cells = 0;
            $fp = fopen($_FILES["file"]["tmp_name"], "r");
            $op = 0;
            $osm = 0;
            $other_arr = [];
            while ($content = fgetcsv($fp))
            {
                
                if ($op == 1)
                {
                   
                    if ($content[$op] == "" || empty($content[$op]))
                    {
                        $osm = 1;
                    }
                }
                if($op == 0)
                {
                    
                    for($fc = 12;$fc < 112;$fc++)
                    {
                        if(isset($content[$fc]) && !empty($content[$fc]) && $content[$fc] != "")
                        {
                            $other_arr[] = $content[$fc];
                        }
                    }
                }
                $rows++;
                $cells += count($content);
                $op++;
            }
           
            
            fclose($fp);
            if ($osm == 1)
            {
                $_SESSION['msg1'] = "Please Enter Data In First Row";
                header("location:../salary");
                exit;
            }
            if ($rows == 1)
            {
                $_SESSION['msg1'] = "Please Enter Data Atleast In One Row";
                header("location:../salary");
                exit;
            }
            
            $chk_order = $d->selectRow("sedtm.earning_deduction_name,sedtm.salary_earning_deduction_id,sedtm.earning_deduction_type,scvm.amount_value_employeer,scvm.amount_type AS common_value_amount_type","salary_common_value_master AS scvm JOIN salary_earning_deduction_type_master AS sedtm ON sedtm.salary_earning_deduction_id = scvm.salary_earning_deduction_id","scvm.salary_group_id = '$salary_group_id'");
            $extra_order = [];
            $extra_order_type = [];
            $extra_order_dup = [];
            $extra_contri = [];
            $extra_amount_type = [];
            while($orderData = $chk_order->fetch_assoc())
            {
               
                $extra_order_dup[] = $orderData['earning_deduction_name'];
                if($orderData['amount_value_employeer']>0){
                    $extra_order_dup[] = $orderData['earning_deduction_name']. ' Contribution';
                    $extra_contri[$orderData['salary_earning_deduction_id']] = $orderData['earning_deduction_name']. ' Contribution';
                }
                $extra_order[$orderData['salary_earning_deduction_id']] = $orderData['earning_deduction_name'];
               $extra_amount_type[$orderData['salary_earning_deduction_id']] = $orderData['common_value_amount_type'];
                $extra_order_type[$orderData['salary_earning_deduction_id']] = $orderData['earning_deduction_type'];
            }
            $file = fopen($_FILES["file"]["tmp_name"], "r");
            $f_cnt = 1;
            $file_extra_order = [];
            while (($getData = fgetcsv($file, 10000, ",")) !== false)
            {
                if($f_cnt == 1)
                {
                    foreach($getData AS $k => $v)
                    {
                       
                       
                        if($k > 5)
                        {
                            $j = explode('~',$v);
                            
                            if(COUNT($j)>=3){
                                $file_extra_order[] = $j[0]." Contribution";
                            }else{
                                $file_extra_order[] = $j[0];
                            }
                           
                            $chk = $d->selectRow("sedtm.salary_earning_deduction_id","salary_common_value_master AS scvm JOIN salary_earning_deduction_type_master AS sedtm ON sedtm.salary_earning_deduction_id = scvm.salary_earning_deduction_id","sedtm.earning_deduction_name = '$j[0]' AND sedtm.society_id = '$society_id'");
                            if(mysqli_num_rows($chk) == 0)
                            {
                                mysqli_rollback($con);
                                $_SESSION['msg1'] = "Columns Does Not Match!";
                                header("location:../salary");exit;
                            }
                        }
                    }
                }
                $f_cnt++;
            }
           
            $uni = count($file_extra_order) !== count(array_unique($file_extra_order));
            if($uni)
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Duplicate Columns!";
                header("location:../salary");exit;
            }
            
            $arraysAreEqual = ($extra_order_dup === $file_extra_order);
            if($arraysAreEqual != 1)
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Column Order Does Not Match!";
                header("location:../salary");exit;
            }
            if ($_FILES["file"]["size"] > 0)
            {
                $flag = true;
                $file = fopen($_FILES["file"]["tmp_name"], "r");
                $no_c = 2;
                while (($getData = fgetcsv($file, 10000, ",")) !== false)
                {
                    $getData = array_map('trim', $getData);
                    if ($flag)
                    {
                        $flag = false;
                        continue;
                    }
                    $employee_number = $getData[0];
                    $employee_name = $getData[1];
                    $salary_type = $getData[2];
                    $salary_start_date = $getData[3];
                    $next_increament_date = $getData[4];
                    $gross_salary = $getData[5];
                    
                    if ($employee_number == "" || empty($employee_number) || $employee_number == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Mobile Number In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if(!preg_match('/^[0-9]{10}+$/', $employee_number))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Mobile Number In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if (isset($employee_number) && !empty($employee_number) && !ctype_digit($employee_number))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Mobile Number In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    $exists = $d->selectRow("user_id","users_master","user_mobile = '$employee_number' AND society_id = '$society_id' AND user_status = 1 AND delete_status = 0");
                    if(mysqli_num_rows($exists) == 0)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Mobile Number Does Not Exists Or Inactive Or Ex Employee In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if ($employee_name == "" || empty($employee_name) || $employee_name == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Employee Name In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if ($salary_type == "" || $salary_type == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Salary Type In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if ($salary_type > 2 || !ctype_digit($salary_type))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Salary Type In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if ($salary_start_date == "" || empty($salary_start_date) || $salary_start_date == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Salary Start Date In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    $new = validateFullDate($salary_start_date);
                    if($new != 1)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Salary Start Date Format In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if(isset($next_increament_date) && !empty($$next_increament_date))
                    {
                        $new1 = validateFullDate($next_increament_date);
                    }
                    if(isset($next_increament_date) && !empty($$next_increament_date) && $new1 != 1)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Next Increament Date Format In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if ($gross_salary == "" || empty($gross_salary) || $gross_salary == null)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Gross Salary In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    if(isset($gross_salary) && !empty($gross_salary) && !is_numeric($gross_salary))
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Please Enter Valid Deduction Amount In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                    $check_q = $d->selectRow("s.salary_start_date","salary AS s JOIN users_master AS um ON um.user_id = s.user_id","um.user_mobile = '$employee_number' AND um.society_id = '$society_id'");
                    if(mysqli_num_rows($check_q) > 0)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Salary Already Added In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }

                    $get_u_id = $d->selectRow("user_id,user_full_name,user_designation,block_id,floor_id","users_master","user_mobile = '$employee_number' AND society_id = '$society_id'");
                    $fetch = $get_u_id->fetch_assoc();
                    $user_id = $fetch['user_id'];
                    $user_full_name = $fetch['user_full_name'];
                    $user_designation = $fetch['user_designation'];
                    $block_id = $fetch['block_id'];
                    $floor_id = $fetch['floor_id'];

                    $ded_tot = 0;
                    $ear_tot = 0;
                    $contri_tot = 0;
                    $fixed_tot = 0;
                    $earn_With_value = array();
                    $contri_array = array();
                //    // echo "<pre>";
                //     foreach($extra_order_type AS $key => $value)
                //     {
                //         if($value == 1)
                //         {
                //             $ded_name = $extra_order[$key];
                //             $gg = array_search($ded_name,$file_extra_order,true);
                //             $kk = 6 + $gg;
                //             if ($getData[$kk] == "" || $getData[$kk] == null)
                //             {
                //                 mysqli_rollback($con);
                //                 $_SESSION['msg1'] = "Please Enter ".$ded_name." In No " . $no_c . " .";
                //                 header("location:../salary");exit;
                //             }
                //             if(isset($getData[$kk]) && !is_numeric($getData[$kk]))
                //             {
                //                 mysqli_rollback($con);
                //                 $_SESSION['msg1'] = "Please Enter Valid ".$ded_name." In No " . $no_c . " .";
                //                 header("location:../salary");exit;
                //             }
                //             $ded_tot += $getData[$kk];
                //             //if($extra_contri)
                //         }
                //         elseif($value == 0)
                //         {
                //            // print_r($extra_amount_type);
                           
                //             $ear_name = $extra_order[$key];
                //             $gg1 = array_search($ear_name,$file_extra_order,true);
                //             $uu = 6 + $gg1;
                //             if ($getData[$uu] == "" || $getData[$uu] == null)
                //             {
                //                 mysqli_rollback($con);
                //                 $_SESSION['msg1'] = "Please Enter ".$ear_name." In No " . $no_c . " .";
                //                 header("location:../salary");exit;
                //             }
                //             if(isset($getData[$uu]) && !is_numeric($getData[$uu]))
                //             {
                //                 mysqli_rollback($con);
                //                 $_SESSION['msg1'] = "Please Enter Valid ".$ear_name." In No " . $no_c . " .";
                //                 header("location:../salary");exit;
                //             }
                //             $ear_tot += $getData[$uu];
                //             $earn_With_value[$key] = $getData[$uu];
                            
                //             if($extra_amount_type[$key]=='1'){
                //                 $fixed_tot +=$getData[$uu];
                //             }
                //         }

                //     }

                //     if(count($extra_contri)>0){
                //         foreach ($extra_contri as $kNew => $vNew) {
                //             # code...
                //             $ggNew = array_search($vNew,$file_extra_order,true);
                //             $uuNew = 6 + $ggNew;
                //            $contri_tot +=$getData[$uuNew];
                //             $contri_array[$kNew] = $getData[$uuNew];
                //         }
                //     }
                   
                //     $gross_salary = $gross_salary+$fixed_tot;
                    $finaData = salaryCalculationByGrossSalary($d,$gross_salary,$salary_group_id);
                    
                    $gross_salary = $finaData['gross_salary'];
                    $net_salary = $finaData['net_salary'];
                    $ctc = $finaData['total_ctc'];
                    $total_fixed_amount = $finaData['total_fixed_amount'];
                    if($ear_tot > $gross_salary)
                    {
                        mysqli_rollback($con);
                        $_SESSION['msg1'] = "Earning Total Should Be Less Than Gross Salary In No " . $no_c . " .";
                        header("location:../salary");exit;
                    }
                   // $net_salary = $gross_salary - $ded_tot;
                   // $ctc = $ear_tot+$contri_tot;
                   
                    $salary = array(
                        'salary_group_id' => $salary_group_id,
                        'user_id' => $user_id,
                        'floor_id' => $floor_id,
                        'block_id' => $block_id,
                        'society_id' => $society_id,
                        'salary_type' => $salary_type,
                        'total_fixed_amount' => $total_fixed_amount,
                        'net_salary' => $net_salary,
                        'total_ctc' => $ctc,
                        'gross_salary' => $gross_salary,
                        'salary_start_date' => $salary_start_date,
                        'salary_increment_date' => $next_increament_date,
                        'created_at' => date("Y-m-d H:i:s")
                    );
                    // print_r($salary);
                    // print_r($gross_salary.'sdfsdfsdf');
                    // print_r($ear_tot.'sdfsdfsdf');
                    // die;
                    $q = $d->insert("salary", $salary);
                    $salary_id = $con->insert_id;
                  ///  print_r($finaData);die;
                    if($q)
                    {
                        $d->insert_log($_COOKIE['bms_admin_id'],$society_id,$user_id,"$created_by", "'$user_full_name' Salary CTC Added (bulk import)");
                        $json = [];
                        $rrr = 1;
                       /*  foreach($extra_order AS $key1 => $value1)
                        {
                            $lk = 5 + $rrr;
                            if ($getData[$lk] == "" || $getData[$lk] == null)
                            {
                                mysqli_rollback($con);
                                $_SESSION['msg1'] = "Please Enter ".$value1." In No " . $no_c . " .";
                                header("location:../salary");exit;
                            }
                            if(isset($getData[$lk]) && !is_numeric($getData[$lk]))
                            {
                                mysqli_rollback($con);
                                $_SESSION['msg1'] = "Please Enter Valid ".$value1." In No " . $no_c . " .";
                                header("location:../salary");exit;
                            }
                            $cnt = COUNT($finaData['deduction_ar'])+COUNT($finaData['earning_ar']);
                           
                          //  array_push($finaData['deduction_ar'],$finaData['earning_ar']);
                          echo "<pre>";
                          
                           print_r($finaData); */
                           foreach ($finaData['earning_ar'] as $key => $value) {
                            $check_e_d = $d->selectRow("scvm.amount_type,scvm.amount_value,scvm.amount_value_employeer,scvm.salary_common_value_earn_deduction,scvm.salary_earning_deduction_id","salary_common_value_master AS scvm","scvm.salary_common_active_status = 0 AND scvm.society_id = '$society_id' AND scvm.salary_group_id = '$salary_group_id' AND scvm.salary_earning_deduction_id = '$key'");
                           
                            $newData =  mysqli_fetch_assoc($check_e_d);
                            if($newData){
                                    $salary_master = [
                                        'salary_earning_deduction_id' => $key,
                                        'salary_earning_deduction_value' => $value,
                                        'salary_id' => $salary_id,
                                        'amount_percentage' => $newData['amount_value'],
                                        'contribution_amount' =>0,
                                        'contribution_precent' => 0,
                                        'amount_type' => $newData['amount_type'],
                                        'created_date' => date("Y-m-d H:i:s"),
                                        'created_by' => $_COOKIE['bms_admin_id']
                                    ];
                                 $q12 = $d->insert("salary_master", $salary_master);
                                }
                            }
                           
                            foreach ($finaData['deduction_ar'] as $key2 => $value) {
                               
                               
                                $check_e_d = $d->selectRow("scvm.amount_type,scvm.amount_value,scvm.amount_value_employeer,scvm.salary_common_value_earn_deduction,scvm.salary_earning_deduction_id","salary_common_value_master AS scvm","scvm.salary_common_active_status = 0 AND scvm.society_id = '$society_id' AND scvm.salary_group_id = '$salary_group_id' AND scvm.salary_earning_deduction_id = '$key2'");
                           
                                $newData =  mysqli_fetch_assoc($check_e_d);
                                if($newData){
                                        $contri = $finaData['contri_ar'][$key2];
                                        if(isset($finaData['contri_ar'][$key2])){
                                            $contribution_amount = $finaData['contri_ar'][$key2];
                                        }else{
                                            $contribution_amount = 0;
                                        }
                                        $salary_master = [
                                            'salary_earning_deduction_id' => $key2,
                                            'salary_earning_deduction_value' => $value,
                                            'salary_id' => $salary_id,
                                            'amount_percentage' => $newData['amount_value'],
                                            'contribution_amount' => $contribution_amount,
                                            'contribution_precent' => $newData['amount_value_employeer'],
                                            'amount_type' => $newData['amount_type'],
                                            'created_date' => date("Y-m-d H:i:s"),
                                            'created_by' => $_COOKIE['bms_admin_id']
                                        ];
                                     $q12 = $d->insert("salary_master", $salary_master);
                                    }
                                }

                            /* $check_e_d = $d->selectRow("scvm.amount_type,scvm.amount_value,scvm.amount_value_employeer,scvm.salary_common_value_earn_deduction,scvm.salary_earning_deduction_id","salary_common_value_master AS scvm","scvm.salary_common_active_status = 0 AND scvm.society_id = '$society_id' AND scvm.salary_group_id = '$salary_group_id' AND scvm.salary_earning_deduction_id = '$key1'");
                            $new_per = "";
                            if(mysqli_num_rows($check_e_d) > 0)
                            {
                                $fet = $check_e_d->fetch_assoc();
                                if($fet['amount_type'] == 0)
                                {
                                    $cal_basic = $fet['amount_value'] * $gross_salary / 100;
                                    // if($cal_basic != $getData[$lk])
                                    // {
                                    //     $new_per = $getData[$lk] * 100 / $gross_salary;
                                    //     $new_per = number_format((float)$new_per, 2, '.', '');
                                    // }
                                    // else
                                    // {
                                    //     $new_per = $fet['amount_value'];
                                    // }
                                }
                                //if($fet['amount_type'] == 1){
                                    
                                    /// print_r($fet);
                                
                                    if($fet['amount_value_employeer']>0){
                                        if($fet['amount_type']=="0"){
                                            // if($fet['salary_common_value_earn_deduction'] !=""){
                                            //     $salary_common_value_earn_deductionAR = explode(',',$fet['salary_common_value_earn_deduction']);
                                            //     $tempAmount = 0;
                                            //     for ($m=0; $m < count($earn_With_value); $m++) { 
                                                    
                                            //     }
                                                
                                            // }else{
                                                
                                            // }
                                            $tempEmployeer = $contri_array[$fet['salary_earning_deduction_id']];
                                        }else if($fet['amount_type']=="1"){
                                            $tempEmployeer = $fet['amount_value_employeer'];
                                        }
                                        $contribution_precent = $fet['amount_value_employeer'];
                                      }else{
                                        $contribution_precent =0;
                                        $tempEmployeer =0;
                                      }
                                //}
                                $salary_master = [
                                    'salary_earning_deduction_id' => $key1,
                                    'salary_earning_deduction_value' => $getData[$lk],
                                    'salary_id' => $salary_id,
                                    'amount_percentage' => $fet['amount_value'],
                                    'contribution_amount' => $tempEmployeer,
                                    'contribution_precent' => $contribution_precent,
                                    'amount_type' => $fet['amount_type'],
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $_COOKIE['bms_admin_id']
                                ];
                                $q12 = $d->insert("salary_master", $salary_master);
                            }
                            else
                            {
                                mysqli_rollback($con);
                                $_SESSION['msg1'] = "Common Value Does Not Exists!";
                                header("location:../salary");exit;
                            } */
                        //     $rrr++;
                        // }
                    }
                    $no_c++;
                }
               
                if ($q12)
                {

                    mysqli_commit($con);
                    $_SESSION['msg'] = "CSV Uploaded Successfully";
                    header("location:../salary?gId=$salary_group_id");exit;
                }
                else
                {
                    mysqli_rollback($con);
                    $_SESSION['msg1'] = "Something went wrong!";
                    header("location:../salary");exit;
                }
            }
            else
            {
                $_SESSION['msg1'] = "Please Upload Valid CSV File";
                header("location:../bulkSalary");
            }
        }
        else
        {
            $_SESSION['msg1'] = "Please Upload CSV File";
            header("location:../salary");exit;
        }
    }
}

function validateDate($date, $format = 'M-y')
{
    $d1 = DateTime::createFromFormat($format, $date);
    return $d1 && $d1->format($format) === $date;
}

function validateFullDate($date, $format = 'Y-m-d')
{
    $d1 = DateTime::createFromFormat($format, $date);
    return $d1 && $d1->format($format) === $date;
}


 function salaryCalculationByGrossSalary($d,$gross_salary = 20000,$salary_group_id=1)
{
    $q1 = $d->select("salary_common_value_master LEFT JOIN salary_earning_deduction_type_master ON salary_earning_deduction_type_master.salary_earning_deduction_id = salary_common_value_master.salary_earning_deduction_id","salary_group_id='$salary_group_id' ");
    $deduction_arr = array();
    $remaining_earn_arr = array();
    $fixedAmount = 0;
    $earn_with_final_value = array();
    $total_deduction = 0;
    $total_ctc = 0;
    $total_earing = 0;
    $total_contri = 0;
    $net_salary = 0;
    while ($data = mysqli_fetch_assoc($q1)) {
       if($data['amount_type']==1){
            $fixedAmount +=$data['amount_value'];
        }
           if($data['earning_deduction_type']==0){
            array_push($remaining_earn_arr,$data);
           }else{
                array_push($deduction_arr,$data);
           }
    }
    /////earning calculation
    if(COUNT($remaining_earn_arr)>0){
        foreach ($remaining_earn_arr as $earnKey => $earn) {
            if($earn['salary_common_value_earn_deduction']==''){
                if($earn['amount_type']==1){
                    $amount_value = $earn['amount_value'];
                }else{
                    $amount_value = ($gross_salary*$earn['amount_value'])/100;
                }
                $earnObj[$earn['salary_earning_deduction_id']] =$amount_value;
                $total_earing +=$amount_value;
            }
        }
        foreach ($remaining_earn_arr as $earnKey2 => $earn2) {
            $amount_value = 0;
            $tempEarn = 0;
            if($earn2['salary_common_value_earn_deduction']!=''){
               $salary_common_value_earn_deduction_array = explode(',',$earn2['salary_common_value_earn_deduction']);
               for ($i=0; $i <count($salary_common_value_earn_deduction_array) ; $i++) { 
                  $tempEarn  +=$earnObj[$salary_common_value_earn_deduction_array[$i]];
               }
               $amount_value = ($tempEarn*$earn2['amount_value'])/100;
               $earnObj[$earn2['salary_earning_deduction_id']] =$amount_value;
               $total_earing +=$amount_value;
            }
        }
    }
    if(count($deduction_arr)>0){
        foreach ($deduction_arr as $decutKey => $dedcution) {
            //print_r($dedcution);
            $earnAmount = 0;
            $earnSlabAmount = 0;
            $deduct_amount  =0;
           
            if($dedcution['amount_type']=="1"){
                ///////flat deduction
                $deduct_amount = $dedcution['amount_value'];
                $deductObj[$dedcution['salary_earning_deduction_id']] =$deduct_amount;
                $contributionObj[$dedcution['salary_earning_deduction_id']] =$dedcution['amount_value_employeer'];
                $total_deduction +=$deduct_amount;
                $total_contri +=$dedcution['amount_value_employeer'];
            }else if($dedcution['amount_type']=="0"){
                //////precent
                if($dedcution['salary_common_value_earn_deduction']!=""){
                    $salary_common_value_deduction_array = explode(',',$dedcution['salary_common_value_earn_deduction']);
                    for ($j=0; $j <count($salary_common_value_deduction_array) ; $j++) { 
                        $earnAmount  +=$earnObj[$salary_common_value_deduction_array[$j]];
                     }
                    $deduct_amount = ($earnAmount*$dedcution['amount_value'])/100;
                    $contri_amount = ($earnAmount*$dedcution['amount_value_employeer'])/100;
                    
                     if($dedcution['percent_min_max'] !=""){
                        $percent_min_max = json_decode($dedcution['percent_min_max'] ,true);
                        if($percent_min_max['percent_max']>=$gross_salary && $gross_salary >=$percent_min_max['percent_min']){
                            if($dedcution['percent_deduction_max_amount']>0){
                                if($deduct_amount<=$dedcution['percent_deduction_max_amount']){
                                    $deduct_amount = $deduct_amount;
                                    
                                }else{
                                    $deduct_amount = $dedcution['percent_deduction_max_amount'];
                                }
                            }else{
                                $deduct_amount = $deduct_amount; 
                            }
                            if($dedcution['percent_deduction_max_amount']>0){
                                if($contri_amount<=$dedcution['percent_deduction_max_amount']){
                                   
                                    $contri_amount = $contri_amount;
                                }else{
                                    
                                    $contri_amount = $dedcution['percent_deduction_max_amount'];
                                }
                            }else{
                               
                                $contri_amount = $contri_amount; 
                            }
                        }else{
                            $deduct_amount = 0; 
                            $contri_amount = 0; 
                        }
                     }else{
                        if($dedcution['percent_deduction_max_amount']>0){
                            if($deduct_amount<=$dedcution['percent_deduction_max_amount']){
                                $deduct_amount = $deduct_amount;
                            }else{
                                $deduct_amount = $dedcution['percent_deduction_max_amount'];
                            }
                        }else{
                            $deduct_amount = $deduct_amount; 
                        }
                        if($dedcution['percent_deduction_max_amount']>0){
                            if($contri_amount<=$dedcution['percent_deduction_max_amount']){
                                $contri_amount = $contri_amount;
                            }else{
                                $contri_amount = $dedcution['percent_deduction_max_amount'];
                            }
                        }else{
                            $contri_amount = $contri_amount; 
                        }
                     }
                     $total_deduction +=$deduct_amount;
                    $deductObj[$dedcution['salary_earning_deduction_id']] =$deduct_amount;
                    $contributionObj[$dedcution['salary_earning_deduction_id']] =$contri_amount;
                    $total_contri +=$contri_amount;
                }


            }else{
                /////slab
                if($dedcution['salary_common_value_earn_deduction']!=""){
                    $salary_common_value_deduction_array = explode(',',$dedcution['salary_common_value_earn_deduction']);
                    for ($j=0; $j <count($salary_common_value_deduction_array) ; $j++) { 
                        $earnSlabAmount  +=$earnObj[$salary_common_value_deduction_array[$j]];
                    }
                    if($dedcution['slab_json'] !=""){
                        $slab_ar = explode('~',$dedcution['slab_json']);
                         foreach ($slab_ar as $slabKey => $slb) {
                            $slab = json_decode($slb,true);
                            if ((float)$earnSlabAmount >= (float)$slab['min'] && (float)$earnSlabAmount <= (float)$slab['max']) {
                               $deduct_amount =  (float)$slab['value'];
                            }
                        }
                    }
                    $total_deduction +=$deduct_amount;
                $deductObj[$dedcution['salary_earning_deduction_id']] =$deduct_amount;

                }
            }
        }
    }
    /////////////////////////////////Deduction calculation
   
    $total_ctc = $total_earing+$total_contri;
    $net_salary = $total_earing-$total_deduction;
    $finalData['earning_ar']=$earnObj;
    $finalData['deduction_ar']=$deductObj;
    $finalData['contri_ar']=$contributionObj;
    $finalData['net_salary']=$net_salary;
    $finalData['total_ctc']=$total_ctc;
    $finalData['total_contri']=$total_contri;
    $finalData['total_earing']=$total_earing;
    $finalData['gross_salary']=$total_earing;
    $finalData['total_fixed_amount']=$fixedAmount;
    
    return $finalData;
}
?>