<?php
include '../common/objectController.php';

if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addRetailer']))
    {
        $file = $_FILES['retailer_photo']['tmp_name'];
        if(file_exists($file))
        {
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png',
                'image/webp'
            );
            if(($_FILES['retailer_photo']['size'] >= $maxsize) || ($_FILES["retailer_photo"]["size"] == 0))
            {
                $_SESSION['msg1']=" Photo too large. File must be less than 1 MB.";
                header("location:../addRetailer");exit;
            }
            if(!in_array($_FILES['retailer_photo']['type'], $acceptable) && (!empty($_FILES["retailer_photo"]["type"])))
            {
                $_SESSION['msg1']="Invalid Photo type. Only JPG and PNG types are accepted.";
                header("location:../addRetailer");exit;
            }
            if(isset($retailer_id) && $retailer_id > 0 && $old_photo != "")
            {
                unlink("../../img/users/recident_profile/".$old_photo);
            }
            $retailer_name_image = str_replace(' ', '_', $retailer_name);
            $temp = explode(".", $_FILES["retailer_photo"]["name"]);
            $retailer_photo = "Retailer_".$retailer_name_image."_".rand().'.' . end($temp);
            move_uploaded_file($_FILES["retailer_photo"]["tmp_name"], "../../img/users/recident_profile/".$retailer_photo);
        }
        else
        {
            if(isset($old_photo))
            {
                $retailer_photo = $old_photo;
            }
            else
            {
                $retailer_photo = "";
            }
        }

        $ci_st_id = explode("-",$_POST['state_city_id']);
        $city_id = $ci_st_id[0];
        $state_id = $ci_st_id[1];
        $m->set_data('society_id', $society_id);
        $m->set_data('country_id', $country_id);
        $m->set_data('city_id', $city_id);
        $m->set_data('state_id', $state_id);
        $m->set_data('area_id', $area_id);
        $m->set_data('retailer_name', $retailer_name);
        $m->set_data('retailer_contact_person', $retailer_contact_person);
        $m->set_data('retailer_contact_person_number', $retailer_contact_person_number);
        $m->set_data('retailer_contact_person_country_code', $retailer_contact_person_country_code);
        $m->set_data('retailer_alt_contact_number', $retailer_alt_contact_number);
        $m->set_data('retailer_alt_country_code', $retailer_alt_country_code);
        $m->set_data('retailer_address', $retailer_address);
        $m->set_data('retailer_geofence_range', $retailer_geofence_range);
        $m->set_data('retailer_pincode', $retailer_pincode);
        $m->set_data('retailer_gst_no', $retailer_gst_no);
        $m->set_data('retailer_type', $retailer_type);
        $m->set_data('retailer_credit_limit', $retailer_credit_limit);
        $m->set_data('retailer_credit_days', $retailer_credit_days);
        $m->set_data('retailer_google_address', $location_name);
        $m->set_data('retailer_latitude', $retailer_latitude);
        $m->set_data('retailer_longitude', $retailer_longitude);
        $m->set_data('retailer_photo', $retailer_photo);
        if(isset($retailer_id) && $retailer_id > 0)
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'state_id' => $m->get_data('state_id'),
                'city_id' => $m->get_data('city_id'),
                'area_id' => $m->get_data('area_id'),
                'retailer_name' => $m->get_data('retailer_name'),
                'retailer_contact_person' => $m->get_data('retailer_contact_person'),
                'retailer_contact_person_number' => $m->get_data('retailer_contact_person_number'),
                'retailer_contact_person_country_code' => $m->get_data('retailer_contact_person_country_code'),
                'retailer_alt_contact_number' => $m->get_data('retailer_alt_contact_number'),
                'retailer_alt_country_code' => $m->get_data('retailer_alt_country_code'),
                'retailer_address' => $m->get_data('retailer_address'),
                'retailer_google_address' => $m->get_data('retailer_google_address'),
                'retailer_geofence_range' => $m->get_data('retailer_geofence_range'),
                'retailer_pincode' => $m->get_data('retailer_pincode'),
                'retailer_gst_no' => $m->get_data('retailer_gst_no'),
                'retailer_type' => $m->get_data('retailer_type'),
                'retailer_credit_limit' => $m->get_data('retailer_credit_limit'),
                'retailer_credit_days' => $m->get_data('retailer_credit_days'),
                'retailer_latitude' => $m->get_data('retailer_latitude'),
                'retailer_longitude' => $m->get_data('retailer_longitude'),
                'retailer_photo' => $m->get_data('retailer_photo'),
                'retailer_modified_date' => date("Y-m-d H:i:s"),
                'retailer_updated_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->update("retailer_master",$a1,"retailer_id = '$retailer_id'");
            $_SESSION['msg'] = "Retailer Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer Updated Successfully");
            if($q)
            {
                foreach($distributor_id AS $key => $value)
                {
                    $check = $d->selectRow("retailer_distributor_relation_id","retailer_distributor_relation_master","retailer_id = '$retailer_id' AND distributor_id = '$value' AND society_id = '$society_id'");
                    if(mysqli_num_rows($check) == 0)
                    {
                        $m->set_data('retailer_id', $retailer_id);
                        $m->set_data('distributor_id', $value);
                        $a12 = array(
                            'society_id' => $m->get_data('society_id'),
                            'retailer_id' => $m->get_data('retailer_id'),
                            'distributor_id' => $m->get_data('distributor_id'),
                            'created_by' => $_COOKIE['bms_admin_id'],
                            'created_date' => date("Y-m-d H:i:s")
                        );
                        $d->insert("retailer_distributor_relation_master", $a12);
                    }
                }
                foreach($old_distributor_id AS $key1 => $value1)
                {
                    if(!in_array($value1,$distributor_id))
                    {
                        $d->delete("retailer_distributor_relation_master","retailer_id   = '$retailer_id ' AND distributor_id = '$value1'");
                    }
                }

                if(isset($route_id))
                {
                    foreach($route_id AS $key2 => $value2)
                    {
                        $check = $d->selectRow("route_retailer_id","route_retailer_master","retailer_id = '$retailer_id' AND route_id = '$value' AND society_id = '$society_id'");
                        if(mysqli_num_rows($check) == 0)
                        {
                            $m->set_data('retailer_id', $retailer_id);
                            $m->set_data('route_id', $value2);
                            $a1234 = array(
                                'society_id' => $m->get_data('society_id'),
                                'route_id' => $m->get_data('route_id'),
                                'retailer_id' => $m->get_data('retailer_id'),
                                'route_retailer_created_date' => date("Y-m-d H:i:s"),
                                'route_retailer_created_by' => $_COOKIE['bms_admin_id'],
                                'route_retailer_order_by' => $key2,
                            );
                            $d->insert("route_retailer_master", $a1234);
                        }
                    }
                }
                else
                {
                    $route_id = [];
                }
                foreach($old_route_id AS $key3 => $value3)
                {
                    if(!in_array($value3,$route_id))
                    {
                        $d->delete("route_retailer_master","retailer_id   = '$retailer_id ' AND route_id = '$value3'");
                    }
                }
                $_SESSION['msg'] = "Retailer Successfully Updated.";
                header("Location: ../manageRetailer?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../manageRetailer?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
            }
        }
        else
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'state_id' => $m->get_data('state_id'),
                'city_id' => $m->get_data('city_id'),
                'area_id' => $m->get_data('area_id'),
                'retailer_name' => $m->get_data('retailer_name'),
                'retailer_contact_person' => $m->get_data('retailer_contact_person'),
                'retailer_contact_person_number' => $m->get_data('retailer_contact_person_number'),
                'retailer_contact_person_country_code' => $m->get_data('retailer_contact_person_country_code'),
                'retailer_alt_contact_number' => $m->get_data('retailer_alt_contact_number'),
                'retailer_alt_country_code' => $m->get_data('retailer_alt_country_code'),
                'retailer_address' => $m->get_data('retailer_address'),
                'retailer_google_address' => $m->get_data('retailer_google_address'),
                'retailer_geofence_range' => $m->get_data('retailer_geofence_range'),
                'retailer_pincode' => $m->get_data('retailer_pincode'),
                'retailer_gst_no' => $m->get_data('retailer_gst_no'),
                'retailer_type' => $m->get_data('retailer_type'),
                'retailer_credit_limit' => $m->get_data('retailer_credit_limit'),
                'retailer_credit_days' => $m->get_data('retailer_credit_days'),
                'retailer_latitude' => $m->get_data('retailer_latitude'),
                'retailer_longitude' => $m->get_data('retailer_longitude'),
                'retailer_photo' => $m->get_data('retailer_photo'),
                'retailer_created_date' => date("Y-m-d H:i:s"),
                'retailer_created_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->insert("retailer_master", $a1);
            $retailer_id = $con->insert_id;
            $_SESSION['msg'] = "Retailer Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer Added Successfully");
            if($q)
            {
                $a123 = array(
                    'retailer_code' => "R".$retailer_id
                );
                $d->update("retailer_master",$a123,"retailer_id = '$retailer_id'");
                foreach($distributor_id AS $key => $value)
                {
                    $m->set_data('retailer_id', $retailer_id);
                    $m->set_data('distributor_id', $value);
                    $a12 = array(
                        'society_id' => $m->get_data('society_id'),
                        'retailer_id' => $m->get_data('retailer_id'),
                        'distributor_id' => $m->get_data('distributor_id'),
                        'created_by' => $_COOKIE['bms_admin_id'],
                        'created_date' => date("Y-m-d H:i:s")
                    );
                    $d->insert("retailer_distributor_relation_master", $a12);
                }
                foreach($route_id AS $key1 => $value1)
                {
                    $m->set_data('retailer_id', $retailer_id);
                    $m->set_data('route_id', $value1);
                    $a123 = array(
                        'society_id' => $m->get_data('society_id'),
                        'route_id' => $m->get_data('route_id'),
                        'retailer_id' => $m->get_data('retailer_id'),
                        'route_retailer_created_date' => date("Y-m-d H:i:s"),
                        'route_retailer_created_by' => $_COOKIE['bms_admin_id'],
                        'route_retailer_order_by' => 0
                    );
                    $d->insert("route_retailer_master", $a123);
                }
                $_SESSION['msg'] = "Retailer Successfully Added.";
                header("Location: ../manageRetailer?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addRetailer");exit;
            }
        }
    }
    elseif(isset($_POST['getStateCityTag']))
    {
        $q = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['getAreaTag']))
    {
        $q = $d->selectRow("area_id,area_name","area_master_new","country_id = '$country_id' AND state_id = '$state_id' AND city_id = '$city_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['deleteRetailer']))
    {
        if($retailer_photo != "")
        {
            unlink("../../img/users/recident_profile/".$retailer_photo);
        }
        $q = $d->delete("retailer_master","retailer_id = '$retailer_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer deleted");
            $_SESSION['msg'] = "Retailer Successfully Delete.";
            header("Location: ../manageRetailer?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageRetailer?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
        }
    }
    elseif(isset($_POST['checkRetailerName']))
    {
        if(isset($retailer_id) && !empty($retailer_id) && $retailer_id != 0)
        {
            if(isset($retailer_name) && !empty($retailer_name) && isset($retailer_contact_person_country_code) && !empty($retailer_contact_person_country_code) && isset($retailer_contact_person_number) && !empty($retailer_contact_person_number))
            {
                $q = $d->selectRow("retailer_name","retailer_master","retailer_name = '$retailer_name' AND retailer_id != '$retailer_id' AND retailer_contact_person_country_code = '$retailer_contact_person_country_code' AND retailer_contact_person_number = '$retailer_contact_person_number'");
            }
        }
        else
        {
            if(isset($retailer_name) && !empty($retailer_name) && isset($retailer_contact_person_country_code) && !empty($retailer_contact_person_country_code) && isset($retailer_contact_person_number) && !empty($retailer_contact_person_number))
            {
                $q = $d->selectRow("retailer_name","retailer_master","retailer_name = '$retailer_name' AND retailer_contact_person_country_code = '$retailer_contact_person_country_code' AND retailer_contact_person_number = '$retailer_contact_person_number'");
            }
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['getDistributorListTag']))
    {
        $q = $d->selectRow("dm.distributor_id,dm.distributor_name,amn.area_name","distributor_master AS dm JOIN area_master_new AS amn ON amn.area_id = dm.distributor_area_id","dm.city_id = '$city_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['getAreaLatLon']))
    {
        $response = [];
        $q1 = $d->selectRow("latitude,longitude","area_master_new","area_id = '$area_id'");
        $fetch = $q1->fetch_assoc();
        if($fetch['latitude'] == "" || empty($fetch['latitude']) || $fetch['latitude'] == 0)
        {
            $response['area_latitude'] = 23.037786;
        }
        else
        {
            $response['area_latitude'] = $fetch['latitude'];
        }

        if($fetch['longitude'] == "" || empty($fetch['longitude']) || $fetch['longitude'] == 0)
        {
            $response['area_longitude'] = 72.512043;
        }
        else
        {
            $response['area_longitude'] = $fetch['longitude'];
        }
        echo json_encode($response);exit;
    }
    elseif(isset($_POST['getRetailerList']))
    {
        $append = "";
        if(isset($country_id) && !empty($country_id))
        {
            $append .= " AND rm.country_id = '$country_id'";
        }
        if(isset($state_id) && !empty($state_id))
        {
            $append .= " AND rm.state_id = '$state_id'";
        }
        if(isset($city_id) && !empty($city_id))
        {
            $append .= " AND rm.city_id = '$city_id'";
        }
        if(isset($area_id) && !empty($area_id))
        {
            $append .= " AND rm.area_id = '$area_id'";
        }
        $q = $d->selectRow("rm.*,rom.retailer_order_id,amn.area_name,c.city_name","retailer_master AS rm LEFT JOIN retailer_order_master AS rom ON rom.retailer_id = rm.retailer_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN cities AS c ON c.city_id = rm.city_id","rm.society_id = '$society_id'".$append,"GROUP BY rm.retailer_id ORDER BY retailer_id DESC");
        $iNo = 1;
        $rtData = [];
        $data = [];
        while($row = $q->fetch_assoc())
        {
            $data = $row;
            $data['no'] = $iNo;
            $data['retailer_contact_person_number'] = $row['retailer_contact_person_country_code'] . " " . $row['retailer_contact_person_number'];
            $data['action'] = [
                'retailer_id' => $row['retailer_id'],
                'country_id' => $row['country_id'],
                'state_id' => $row['state_id'],
                'city_id' => $row['city_id'],
                'area_id' => $row['area_id'],
                'retailer_photo' => $row['retailer_photo'],
                'retailer_order_id' => $row['retailer_order_id']
            ];
            $data['status'] = [
                'retailer_id' => $row['retailer_id'],
                'retailer_status' => $row['retailer_status']
            ];
            if(file_exists("../../img/users/recident_profile/".$row['retailer_photo']))
            {
                $data['photo_new'] = [
                    'retailer_photo' => $row['retailer_photo'],
                    'retailer_name' => $row['retailer_name'],
                    'photo_exists' => 1
                ];
            }
            else
            {
                $data['photo_new'] = [
                    'retailer_photo' => "",
                    'retailer_name' => $row['retailer_name'],
                    'photo_exists' => 0
                ];
            }
            $rtData[] = $data;
            $iNo++;
        }
        echo json_encode($rtData);exit;
    }
    elseif(isset($_POST['getRouteList']))
    {
        $append = "";
        if(isset($city_id) && $city_id != 0)
        {
            $append .= " AND rm.city_id = '$city_id'";
        }
        $q = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id","rm.route_active_status = 0".$append);
        $data = [];
        while ($qd = $q->fetch_assoc())
        {
            $data[] = $qd;
        }
        echo json_encode($data);exit;
    }
}
?>