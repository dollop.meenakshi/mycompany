<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// add new employee
if(isset($addDocumentType)) {

        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('document_type_name',$document_type_name);

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'document_type_name'=> $m->get_data('document_type_name'),
        );


    $q=$d->insert("document_type_master",$a1);
  if($q==TRUE) {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Type Added");
    $_SESSION['msg']="Document Type Added";
    header("Location: ../viewDocumentType");
  } else {
    header("Location: ../documentType");
  }
}

if (isset($document_board_id_read)) {
    
   $q=$d->select("document_master","society_id='$society_id' AND document_id='$document_board_id_read' AND uploaded_user_id='0'");
    $row=mysqli_fetch_array($q);

    if ($row['share_with']=='public') 
    {
      $q3=$d->select("unit_master,block_master,users_master,floors_master,document_read_status","document_read_status.user_id=users_master.user_id AND document_read_status.document_id='$document_board_id_read' AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 $blockAppendQuery","ORDER BY block_master.block_sort ASC");
    } 
    else 
    {
      $q3=$d->select("unit_master,block_master,users_master,floors_master,document_read_status,document_shared_master","document_shared_master.user_id=users_master.user_id AND document_read_status.user_id=users_master.user_id AND document_shared_master.document_id='$document_board_id_read' AND document_read_status.document_id='$document_board_id_read' AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 $blockAppendQuery","ORDER BY block_master.block_sort ASC");

    }

     $userIdArray = array();
     $userIdOnly = array();
     while ($araData=mysqli_fetch_array($q3)) {
       array_push($userIdArray, $araData);
       array_push($userIdOnly, $araData['user_id']);
     }

     $newqry="";
           $aleradyRead= "";
       
      if (count($userIdOnly>0)) {
         $ids2 = join("','",$userIdOnly); 
          $ALreadyReadQuery =" and users_master.user_id NOT IN ('$ids2') ";
      }

      
          $title= "Document $row[ducument_name] Shared";
          $description= "Shared By Admin $created_by";

         

         $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'  $qry $newqry $ALreadyReadQuery");
         $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $qry $newqry $ALreadyReadQuery");
         $nResident->noti("DocumentFragment","",$society_id,$fcmArray,$title,$description,'documents');
         $nResident->noti_ios("DocumentVC","",$society_id,$fcmArrayIos,$title,$description,'documents');

         $_SESSION['msg']="Document Reminder Send Successfully ";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Reminder Send Successfully");
        header("Location: ../documents");
        exit();

  


}
// add new employee
if(isset($document_id_edidt)) {

        
        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('document_type_name',$document_type_name);

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'document_type_name'=> $m->get_data('document_type_name'),
        );


    $q=$d->update("document_type_master",$a1,"document_type_id='$document_id_edidt'");
  if($q==TRUE) {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Type Updated");
    $_SESSION['msg']="Document Type Updated";
    header("Location: ../viewDocumentType");
  } else {
    header("Location: ../viewDocumentType");
  }
}

// delete user
if(isset($deleteDocumentType)) {
  $q=$d->delete("document_type_master","document_id='$deleteDocumentType'");
  if($q==TRUE) {
    $_SESSION['msg']="Document Type Deleted";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Type Deleted");
    header("Location: ../viewDocumentType");
  } else {
    header("Location: ../viewDocumentType");
  }
}



// add new employee
if(isset($ducument_name_add)) {

        $file = $_FILES['document_file']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 10097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            );
            if(($_FILES['document_file']['size'] >= $maxsize) || ($_FILES["document_file"]["size"] == 0)) {
                $_SESSION['msg1']="Document too large. Must be less than 10 MB.";
                header("location:../document");
                
            }
            if(!in_array($_FILES['document_file']['type'], $acceptable) && (!empty($_FILES["document_file"]["type"]))) {
                 $_SESSION['msg1']="Invalid  Document. Only  JPG,PNG,Doc,CSV,XLS & PDF are allowed.";            
                header("location:../document");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['document_file'];   
            $temp = explode(".", $_FILES["document_file"]["name"]);
            $document_file = 'document_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["document_file"]["tmp_name"], "../../img/documents/".$document_file);
          } 
        } else{
           $_SESSION['msg1']="Upload Document File";
                header("location:../document");
        }

        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('document_type_id',$document_type_id);
        $m->set_data('ducument_name',$ducument_name_add);
        $m->set_data('ducument_description',$ducument_description);
        $m->set_data('document_file',$document_file);
        $m->set_data('uploade_date', date("Y-m-d H:i"));

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'document_type_id'=> $m->get_data('document_type_id'),
            'ducument_name'=> $m->get_data('ducument_name'),
            'ducument_description'=> $m->get_data('ducument_description'),
            'document_file'=> $m->get_data('document_file'),
            'uploade_date'=> $m->get_data('uploade_date'),
        );


    $q=$d->insert("document_master",$a1);
  if($q==TRUE) {
    $_SESSION['msg']="Document Added";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Added");
    header("Location: ../documents");
  } else {
    header("Location: ../document");
  }
}



// add new employee
if(isset($ducument_name_edit)) {

        $file = $_FILES['document_file']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 10097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if(($_FILES['document_file']['size'] >= $maxsize) || ($_FILES["document_file"]["size"] == 0)) {
                $_SESSION['msg1']="Document too large. Must be less than 10 MB.";
                header("location:../document");
                
            }
            if(!in_array($_FILES['document_file']['type'], $acceptable) && (!empty($_FILES["document_file"]["type"]))) {
                 $_SESSION['msg1']="Invalid  Document. Only  JPG,PNG & PDF are allowed.";
                header("location:../document");

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['document_file'];   
            $temp = explode(".", $_FILES["document_file"]["name"]);
            $document_file = 'document_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["document_file"]["tmp_name"], "../../img/documents/".$document_file);
          } 
        } else{
          $document_file=$document_file_old;
        }

        
        $m->set_data('society_id',$_COOKIE['society_id']);
        $m->set_data('document_type_id',$document_type_id);
        $m->set_data('ducument_name',$ducument_name_edit);
        $m->set_data('ducument_description',$ducument_description);
        $m->set_data('document_file',$document_file);
        $m->set_data('uploade_date', date("Y-m-d H:i"));

         $a1= array (
            
            'society_id'=> $m->get_data('society_id'),
            'document_type_id'=> $m->get_data('document_type_id'),
            'ducument_name'=> $m->get_data('ducument_name'),
            'ducument_description'=> $m->get_data('ducument_description'),
            'document_file'=> $m->get_data('document_file'),
            'uploade_date'=> $m->get_data('uploade_date'),
        );


    $q=$d->update("document_master",$a1,"document_id='$document_id_update'");
  if($q==TRUE) {
    $_SESSION['msg']="Document Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Updated");

    header("Location: ../documents");
  } else {
    header("Location: ../document");
  }
}


// add new employee
if(isset($sahreUser)) {


  if (count($_POST['user_id'])>0) {
    
    for ($i1=0; $i1 <count($_POST['user_id']) ; $i1++) { 
    

      $user_id= $_POST['user_id'][$i1];

   $q3=$d->select("document_shared_master","document_id='$document_id_share' AND user_id='$user_id'","");
   $userData=mysqli_fetch_array($q3);

      if($userData>0) {
          $_SESSION['msg1']="Document Already Shared";
          header("Location: ../documents");
      } else {
        $m->set_data('society_id',$society_id);
        $m->set_data('document_id',$document_id_share);
        $m->set_data('user_id',$user_id);

         $a1= array (
            'society_id'=> $m->get_data('society_id'),
            'document_id'=> $m->get_data('document_id'),
            'user_id'=> $m->get_data('user_id'),
        );
         
 
        $q=$d->insert("document_shared_master",$a1);
        $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND user_id='$user_id'");
        $data_notification=mysqli_fetch_array($qUserToken);
        $sos_user_token=$data_notification['user_token'];
        $device=$data_notification['device'];
        if ($device=='android') {
           $nResident->noti("DocumentFragment","",$society_id,$sos_user_token,"Document Shared","By Admin ".$_COOKIE['admin_name'],'documents');
        }  else if($device=='ios') {
          $nResident->noti_ios("DocumentsVC","",$society_id,$sos_user_token,"Document Shared","By Admin ".$_COOKIE['admin_name'],'documents');
        }
        // $nResident->noti("","",$society_id,$sos_user_token,"1 Document Shared","by Admin",'documents');

        $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$data_notification['user_id'],
        'notification_title'=>'1 Document Shared',
        'notification_desc'=>"By Admin ".$_COOKIE['admin_name'],    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'documents',
        'notification_logo'=>'document_management.png',

        );
        $d->insert("user_notification",$notiAry);

        }
      }
       
    if($q==TRUE) {

      $_SESSION['msg']="Document Shared";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Shared");

      header("Location: ../documents");
    } else {
      header("Location: ../document");
    }

} else {
     $_SESSION['msg1']="Please Select User";
      header("Location: ../documents");
}

}


if(isset($shareAll)) {
    $q11=$d->select("document_master","document_id='$document_id_share'","");
    $data=mysqli_fetch_array($q11);
    $ducument_name=$data['ducument_name']; 

        $m->set_data('share_with','public');
         $a1= array (
            'share_with'=> $m->get_data('share_with'),
        );
         
    $q=$d->update("document_master",$a1,"document_id='$document_id_share'");
    if($q==TRUE) {

      $qSociety = $d->select("society_master","society_id ='$society_id'");
      $user_data_society = mysqli_fetch_array($qSociety);
      if($user_data_society['tenant_access_for_document']==1) {
        $append_query  = "user_type=0";
        $appendNew  = "AND user_type=0";
      }

      
      $d->delete("document_shared_master","document_id='$document_id_share'");
      $_SESSION['msg']="Document Shared to All";
      $title= "$ducument_name Document Shared";
      $description= "By Admin ".$_COOKIE['admin_name'];

       $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $appendNew");
       $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'  $appendNew");
       $nResident->noti("DocumentFragment","",$society_id,$fcmArray,$title,$description,'documents');
       $nResident->noti_ios("DocumentsVC","",$society_id,$fcmArrayIos,$title,$description,'documents');

      $d->insertUserNotification($society_id,$title,$description,"documents","document_management.png","$append_query");


      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$ducument_name Document Shared ");
      header("Location: ../documents");

    } else {
      header("Location: ../document");
    }


}

// add new employee
if(isset($removeShareId)) {
  // print_r($_POST);
        $m->set_data('share_with','');
         $a1= array (
            'share_with'=> $m->get_data('share_with'),
        );
         
    $q=$d->update("document_master",$a1,"document_id='$removeShareId'");
    if($q==TRUE) {
      $_SESSION['msg']="Document Unshared to All";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Unshared");
      header("Location: ../documents");
    } else {
      header("Location: ../document");
    }


}


// add new employee
if(isset($share_id_remove)) {
  // print_r($_POST);
      
    $q=$d->delete("document_shared_master","share_id='$share_id_remove'");
    if($q==TRUE) {
      $_SESSION['msg']="Document Un Shared ";
      header("Location: ../documents");
    } else {
      header("Location: ../documents");
    }


}

 ?>