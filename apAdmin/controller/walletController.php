<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));



if(isset($_POST) && !empty($_POST) && $_POST["csrf"] == $_SESSION["token"])//it can be $_GET doesn't matter
{
  

    // Add Income
    if($_POST['addCredit']) {
       $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND user_mobile='$user_mobile' AND user_status=1");
        $data_notification=mysqli_fetch_array($qUserToken);
        $unit_id=$data_notification['unit_id'];

        $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
        $row=mysqli_fetch_array($count6);
        $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

           
      
        $m->set_data('society_id',$society_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('unit_id',$unit_id);
        $m->set_data('remark',$remark);
        $m->set_data('credit_amount',$credit_amount);
        $m->set_data('avl_balance',$totalDebitAmount1+$credit_amount);


        $a1 = array(
          'society_id'=>$m->get_data('society_id'),
          'user_mobile'=>$m->get_data('user_mobile'),
          'unit_id'=>$m->get_data('unit_id'),
          'remark'=>$m->get_data('remark'),
          'credit_amount'=>$m->get_data('credit_amount'),
          'avl_balance'=>$m->get_data('avl_balance'),
          'created_date'=>date("Y-m-d H:i:s"), 
           'admin_id'=>$_COOKIE['bms_admin_id'],
        );

         // print_r($a1);
        $q = $d->insert("user_wallet_master",$a1);

        if($q==TRUE) {
          $title= "$currency $credit_amount Credited in your wallet";
          $description = "Added by $created_by (Admin)";
         
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            if ($device=='android') {
              $nResident->noti("WalletHomeActivity","",$society_id,$sos_user_token,$title,$description,'Wallet');
            } elseif ($device=='Ios') {
              $nResident->noti_ios("walletVC","",$society_id,$sos_user_token,$title,$description,'Wallet');
            }

            $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$data_notification['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>$description,    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'Wallet',
              'notification_logo'=>'wallet.png',
            );
            $d->insert("user_notification",$notiAry);

          $_SESSION['msg']="Credit Added";
          
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Credit Added in Wallet of $user_mobile ($currency $credit_amount)");

          header("Location: ../userWallet?id=$user_mobile");
        } else {
         $_SESSION['msg1']="Something Wrong";
         header("Location: ../userWallet?id=$user_mobile");
        }
    }


    // Add Income
    if($_POST['addDebit']) {
        $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND user_mobile='$user_mobile' AND user_status=1");
        $data_notification=mysqli_fetch_array($qUserToken);
        $unit_id=$data_notification['unit_id'];
        
        $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
        $row=mysqli_fetch_array($count6);
        $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

        if ($totalDebitAmount1<$debit_amount) {
          $_SESSION['msg1']="Not enough balance in wallet for debit";
         header("Location: ../userWallet?id=$user_mobile");
          exit();
        } 
        $m->set_data('society_id',$society_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('unit_id',$unit_id);
        $m->set_data('remark',$remark);
        $m->set_data('debit_amount',$debit_amount);
        $m->set_data('avl_balance',$totalDebitAmount1-$debit_amount);


        $a1 = array(
          'society_id'=>$m->get_data('society_id'),
          'user_mobile'=>$m->get_data('user_mobile'),
          'unit_id'=>$m->get_data('unit_id'),
          'remark'=>$m->get_data('remark'),
          'debit_amount'=>$m->get_data('debit_amount'),
          'avl_balance'=>$m->get_data('avl_balance'),
          'created_date'=>date("Y-m-d H:i:s"), 
          'admin_id'=>$_COOKIE['bms_admin_id'],
        );
            // print_r($a1);
        $q = $d->insert("user_wallet_master",$a1);

        if($q==TRUE) {
          $title= "$currency $debit_amount Debited in your wallet";
          $description = "Added by $created_by (Admin)";
         
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            if ($device=='android') {
              $nResident->noti("WalletHomeActivity","",$society_id,$sos_user_token,$title,$description,'Wallet');
            } elseif ($device=='Ios') {
              $nResident->noti_ios("walletVC","",$society_id,$sos_user_token,$title,$description,'Wallet');
            }

            $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$data_notification['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>$description,    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'Wallet',
              'notification_logo'=>'wallet.png',
            );
            $d->insert("user_notification",$notiAry);

          $_SESSION['msg']="Debit Added";
          
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Debit Added in Wallet of $user_mobile ($currency $debit_amount)");

          header("Location: ../userWallet?id=$user_mobile");
        } else {
         $_SESSION['msg1']="Something Wrong";
         header("Location: ../userWallet?id=$user_mobile");
        }
    }

      if($_POST['wallet_id_delete']) {
        $qUserToken=$d->select("users_master","delete_status=0 AND society_id='$society_id' AND user_mobile='$user_mobile' AND user_status=1");
        $data_notification=mysqli_fetch_array($qUserToken);
        $unit_id=$data_notification['unit_id'];

        if ($debit_amount>0) {
         $tmpAt= "$currency $debit_amount Debit";
         $tmpAmount = $debit_amount;
          $title= "$currency $debit_amount Debit Wallet Entry Deleted";
          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
          $row=mysqli_fetch_array($count6);
          $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];
          $totalDebitAmount1 = $totalDebitAmount1+$debit_amount;
          if ($totalDebitAmount1<=0) {
            $_SESSION['msg1']="Not enough balance in wallet for delete this entry";
            header("Location: ../userWallet?id=$user_mobile");
            exit();
          } 
        } else {
          echo "string";
           $tmpAmount = $credit_amount;
           $tmpAt= "$currency $credit_amount Credit";
          $title= "$currency $credit_amount Credit Wallet Entry Deleted";
          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
          $row=mysqli_fetch_array($count6);
          $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];
          $totalDebitAmount1 = $totalDebitAmount1-$credit_amount;
          if ($totalDebitAmount1<=0) {
            $_SESSION['msg1']="Not enough balance in wallet for delete this entry ";
            header("Location: ../userWallet?id=$user_mobile");
            exit();
          } 
        }

            // print_r($a1);
        $q = $d->delete("user_wallet_master","wallet_id='$wallet_id_delete'");

        if($q==TRUE) {

          $description = "Added by $created_by (Admin)";
         
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            if ($device=='android') {
              $nResident->noti("WalletHomeActivity","",$society_id,$sos_user_token,$title,$description,'Wallet');
            } elseif ($device=='Ios') {
              $nResident->noti_ios("walletVC","",$society_id,$sos_user_token,$title,$description,'Wallet');
            }

            $notiAry = array(
              'society_id'=>$society_id,
              'user_id'=>$data_notification['user_id'],
              'notification_title'=>$title,
              'notification_desc'=>$description,    
              'notification_date'=>date('Y-m-d H:i'),
              'notification_action'=>'Wallet',
              'notification_logo'=>'wallet.png',
            );
            $d->insert("user_notification",$notiAry);

          $_SESSION['msg']="Entry Deleted";
          
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$tmpAt Wallet Entry Deleted of $user_mobile ($currency $credit_amount)");

          header("Location: ../userWallet?id=$user_mobile");
        } else {
         $_SESSION['msg1']="Something Wrong";
         header("Location: ../userWallet?id=$user_mobile");
        }
    }

} else {

  $_SESSION['msg1']="Invalid Request";
  header('location:../welcome');

}
?>