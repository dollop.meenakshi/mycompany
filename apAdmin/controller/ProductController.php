<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
    if (isset($_POST['addProduct'])) {     
        $m->set_data('society_id', $society_id);
         $m->set_data('product_name', $product_name);
        /* $m->set_data('product_alias_name', $product_alias_name);
        $m->set_data('product_short_name', $product_short_name); */
        $m->set_data('product_category_id', $product_category_id);
        $m->set_data('product_sub_category_id', $product_sub_category_id);
        $m->set_data('product_brand', $product_brand);
       // $m->set_data('minimum_order_quantity', $minimum_order_quantity);
        $m->set_data('unit_measurement_id', $unit_measurement_id);
        $m->set_data('other', $other);
        $m->set_data('product_created_date', date("Y-m-d H:i:s"));

        //////image upload

        $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG","pdf","PDF");
        $uploadedFile = $_FILES['product_image']['tmp_name'];
        $ext = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["product_image"]["size"];
        $maxsize = 12582912;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 12MB.";
                    header("location:../addProducts");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $product_created_date;
                $dirPath = "../../img/product/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "product." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "product." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "product." . $ext);
                        break;
                    case IMAGETYPE_PDF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "product." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("Location:../addProducts");
                        exit;
                        break;
                }
                $product_image = $newFileName . "product." . $ext;
                $notiUrl = $base_url . 'img/product/' . $product_image;
                $m->set_data('product_image', $product_image);
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../addProducts");
                exit();
            }
        }   
        if(isset($product_image))
        {
            $m->set_data('product_image', $product_image);
            if($product_image_old != ''){
                unlink("../../img/product/".$product_image_old);
            }
        }
        else
        {
            $m->set_data('product_image', $product_image_old);
        }
            
        $a1 = array(   
            'society_id' => $m->get_data('society_id'),
            'product_name' => $m->get_data('product_name'),
            'product_category_id' => $m->get_data('product_category_id'),
            'product_sub_category_id' => $m->get_data('product_sub_category_id'),
            'product_brand' => $m->get_data('product_brand'),
            'unit_measurement_id' => $m->get_data('unit_measurement_id'),
            'product_image' => $m->get_data('product_image'),
            'other' => $m->get_data('other'),
            'product_created_date' => $m->get_data('product_created_date'),
        );

        if (isset($product_id) && $product_id > 0) {

            $q = $d->update("product_master", $a1, "product_id ='$product_id'");
            $_SESSION['msg'] = "Product Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Updated Successfully");
        } else {
            $q = $d->insert("product_master", $a1);
            $product_id = $con->insert_id;
            if(count($_POST['product_variant_name'])>0){
                
                for ($i=0; $i <count($_POST['product_variant_name']) ; $i++) { 
                    $prAr = array(
                        'product_id' => $product_id,
                        'product_variant_name' => $_POST['product_variant_name'][$i],
                        'created_date' => date('Y-m-d H:i:s'),
                    );
                    $checkProductVer = $d->selectRow('product_variant_master.*', 'product_variant_master', "product_variant_name='$_POST[product_variant_name][$i]' AND product_id= $product_id");
                    $getData = mysqli_fetch_assoc($checkProductVer);
                    if($getData){
                        $qPr = $d->update("product_variant_master", $prAr, "product_variant_id ='$product_variant_id'");
                    }else
                    {
                        $qPr = $d->insert("product_variant_master", $prAr);
                    }
                }
            }
            $_SESSION['msg'] = "Product Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Added Successfully");

        }
        if ($q == true) {
            header("Location: ../manageProducts?cId=".$product_category_id."&scId=".$product_sub_category_id."");

        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addProducts");
        }

    }
    if (isset($_POST['addProductPrice'])) {   
        $m->set_data('product_id', $product_id);
        $m->set_data('product_variant_id', $product_variant_id);
        $m->set_data('vendor_id', $vendor_id);
        $m->set_data('product_price', $product_price);
        $m->set_data('minimum_order_quantity', $minimum_order_quantity);
        $m->set_data('price_added_date', date("Y-m-d"));
        $a1 = array(   
            'product_id' => $m->get_data('product_id'),
            'product_variant_id' => $m->get_data('product_variant_id'),
            'vendor_id' => $m->get_data('vendor_id'),
            'product_price' => $m->get_data('product_price'),
            'minimum_order_quantity' => $m->get_data('minimum_order_quantity'),
           'price_added_date' => $m->get_data('price_added_date'),
        );
        $checkVendor = $d->selectRow('product_price_master.*', 'product_price_master', " product_id= $product_id AND vendor_id = $vendor_id AND product_variant_id=$product_variant_id");
       // print_r($_POST);die;
        if(mysqli_num_rows($checkVendor) > 0 ||(isset($product_price_id) && $product_price_id > 0)){
            if(mysqli_num_rows($checkVendor)){
                $checkVendorData = mysqli_fetch_assoc($checkVendor);
            $product_price_id = $checkVendorData['product_price_id'];
            }
            $q = $d->update("product_price_master", $a1, "product_price_id ='$product_price_id'");
            $_SESSION['msg'] = "Product Price Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Price Updated Successfully");
            
        }
         else {
            $q = $d->insert("product_price_master", $a1);
            $product_price_id =$con->insert_id;
            $_SESSION['msg'] = "Product Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Added Successfully");
            
        }
        if ($q == true) {

            $a1['product_price_id']=$product_price_id;
            $qz = $d->insert("product_price_history", $a1);

            if($product_sub_category_id !=""){
                $checkSubCatVendor = $d->selectRow('product_sub_category_vendor_master.*', 'product_sub_category_vendor_master', " product_sub_category_id= $product_sub_category_id AND vendor_id = $vendor_id");
                if(mysqli_num_rows($checkSubCatVendor)==0){
                    $SubCatVnda1 = array(
                        'society_id' => $society_id,
                        'product_sub_category_id' => $product_sub_category_id,
                        'vendor_id' => $vendor_id,
                        'created_date' => date("Y-m-d H:i:s"),
                      );
                    $q2 = $d->insert("product_sub_category_vendor_master", $SubCatVnda1);
                    $_SESSION['msg'] = "vendor Sub Category Added Successfully";
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor Sub Category Added Successfully");
                }
            }
            $checkCatVendor = $d->selectRow('product_category_vendor_master.*', 'product_category_vendor_master', " product_category_id= $product_category_id AND vendor_id = $vendor_id");
            if(mysqli_num_rows($checkCatVendor)==0){
                $CatVnda1 = array(
                    'society_id' => $society_id,
                    'product_category_id' => $product_category_id,
                    'vendor_id' => $vendor_id,
                    'created_date' => date("Y-m-d H:i:s"),
                    );
                $q3 = $d->insert("product_category_vendor_master", $CatVnda1);
                $_SESSION['msg'] = "vendor  Category Added Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor  Category Added Successfully");
            }
           
            header("Location: ../productPrices?cId=$product_category_id&scId=$product_sub_category_id");

        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addProductPrice");
        }

    }

}
