<?php
include '../common/objectController.php';
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addCategory']))
    {
        $m->set_data('category_name', $category_name);
        $m->set_data('category_description', $category_description);
        $a1 = array(
            'society_id' => $society_id,
            'category_name' => $m->get_data('category_name'),
            'category_description' => $m->get_data('category_description'),
            'product_category_created_date' => date("Y-m-d H:i:s")
        );
        if(isset($product_category_id) && $product_category_id > 0)
        {
            $q = $d->update("product_category_master",$a1,"product_category_id = '$product_category_id'");
            $_SESSION['msg'] = "Category Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Category Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Category Successfully Updated.";
                header("Location: ../manageCategory");
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addCategory");
            }
        }
        else
        {
            $q = $d->insert("product_category_master", $a1);
            $_SESSION['msg'] = "Category Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Category Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Category Successfully Added.";
                header("Location: ../manageCategory");
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addCategory");
            }
        }
    }
    elseif(isset($_POST['deleteCategory']))
    {
        $q = $d->delete("product_category_master","product_category_id = '$product_category_id'");
        if($q)
        {
            $d->delete("product_sub_category_master","product_category_id = '$product_category_id'");
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Category deleted");
            $_SESSION['msg'] = "Category Successfully Delete.";
            header("Location: ../manageCategory");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageCategory");
        }
    }
    elseif(isset($_POST['checkCategoryName']))
    {
        if(isset($product_category_id))
        {
            $q = $d->selectRow("category_name","product_category_master","category_name = '$category_name' AND product_category_id != '$product_category_id'");
        }
        else
        {
            $q = $d->selectRow("category_name","product_category_master","category_name = '$category_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['addSubCategory']))
    {
        $m->set_data('product_category_id', $product_category_id);
        $m->set_data('sub_category_name', $sub_category_name);
        $m->set_data('sub_category_description', $sub_category_description);
        $a1 = array(
            'society_id' => $society_id,
            'product_category_id' => $m->get_data('product_category_id'),
            'sub_category_name' => $m->get_data('sub_category_name'),
            'sub_category_description' => $m->get_data('sub_category_description'),
            'product_sub_category_created_date' => date("Y-m-d H:i:s")
        );
        if(isset($product_sub_category_id) && $product_sub_category_id > 0)
        {
            $q = $d->update("product_sub_category_master",$a1,"product_sub_category_id = '$product_sub_category_id'");
            $_SESSION['msg'] = "Category Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Sub Category Successfully Updated.";
                header("Location: ../manageSubCategory?PCId=$product_category_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addSubCategory");
            }
        }
        else
        {
            $q = $d->insert("product_sub_category_master", $a1);
            $_SESSION['msg'] = "Sub Category Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Sub Category Successfully Added.";
                header("Location: ../manageSubCategory?PCId=$product_category_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addSubCategory");
            }
        }
    }
    elseif(isset($_POST['deleteSubCategory']))
    {
        $q = $d->delete("product_sub_category_master","product_sub_category_id = '$product_sub_category_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category deleted");
            $_SESSION['msg'] = "Sub Category Successfully Delete.";
            header("Location: ../manageSubCategory?PCId=$product_category_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageSubCategory?PCId=$product_category_id");exit;
        }
    }
    elseif(isset($_POST['checkSubCategoryName']))
    {
        if(isset($product_sub_category_id))
        {
            $q = $d->selectRow("sub_category_name","product_sub_category_master","sub_category_name = '$sub_category_name' AND product_sub_category_id != '$product_sub_category_id'");
        }
        else
        {
            $q = $d->selectRow("sub_category_name","product_sub_category_master","sub_category_name = '$sub_category_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['getSubCatList']))
    {
        $q = $d->selectRow("product_sub_category_id,sub_category_name","product_sub_category_master","product_category_id = '$product_category_id'");
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
}
?>