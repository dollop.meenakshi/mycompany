<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
  //IS_605 

  if (isset($_POST['addproductCategory01'])) {
    $m->set_data('society_id', $society_id);
    $m->set_data('vendor_id', $vendor_id);
    $m->set_data('category_name', $category_name);
    $m->set_data('category_description', $category_description);
    $m->set_data('product_category_created_date', date("Y-m-d H:i:s"));

    $a1 = array(

      'society_id' => $m->get_data('society_id'),
      'category_name' => $m->get_data('category_name'),
      'category_description' => $m->get_data('category_description'),
      'product_category_created_date' => $m->get_data('product_category_created_date'),
    );

    if (isset($product_category_id) && $product_category_id > 0) {
      $CatSql = $d->selectRow('product_category_master.*', 'product_category_master', "category_name='$category_name'");
      $getCat = mysqli_fetch_assoc($CatSql);
      if ($getCat) {
        if ($product_category_id == $getCat['product_category_id']) {
          $q = $d->update("product_category_master", $a1, "product_category_id ='$product_category_id'");
          $_SESSION['msg'] = "Product Category Updated Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Updated Successfully");
        } else {
          if ($vendor_id > 0) {
            $VenCatSql = $d->selectRow('product_category_vendor_master.*', 'product_category_vendor_master', " vendor_id=$vendor_id AND product_category_id=$product_category_id");
            if ($VenCatSql) {
              $_SESSION['msg'] = "Product Category Already Exist";
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
            } else {
              $a2 = array(
                'vendor_id' => $vendor_id,
                'society_id' => $society_id,
                'product_category_id' => $product_category_id,
                'created_date' => date('Y-m-d H:i:s')
              );
              $q = $d->insert("product_category_vendor_master", $a2);
              $_SESSION['msg'] = "Product Category Already Exist";
              $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
            }
          } else {
            $_SESSION['msg'] = "Product Category Already Exist";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
          }
        }
      } else {
        $q = $d->update("product_category_master", $a1, "product_category_id ='$product_category_id'");
        $_SESSION['msg'] = "Product Category Updated Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Updated Successfully");
      }
    } else {
      $CatSql = $d->selectRow('product_category_master.*', 'product_category_master', "category_name='$category_name'");
      $getCat = mysqli_fetch_assoc($CatSql);

      if ($getCat) {
        if ($vendor_id > 0) {
          $product_category_id = $getCat['product_category_id'];
          $VenCatSql = $d->selectRow('product_category_vendor_master.*', 'product_category_vendor_master', "vendor_id=$vendor_id AND product_category_id=$product_category_id");
          $getVend = mysqli_fetch_assoc($VenCatSql);
          if ($getVend) {
            $q = true;
            $_SESSION['msg'] = "Product Category Already Exist";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
          } else {
            $a2 = array(
              'vendor_id' => $vendor_id,
              'society_id' => $society_id,
              'product_category_id' => $product_category_id,
              'created_date' => date('Y-m-d H:i:s')
            );
            $q = $d->insert("product_category_vendor_master", $a2);
            $_SESSION['msg'] = "Product Category Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Added Successfully");
          }
        } else {
          $q = true;
          $_SESSION['msg'] = "Product Category Already Exist";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
        }
      } else {

        $q = $d->insert("product_category_master", $a1);
        $product_category_id = $con->insert_id;
        if ($vendor_id > 0) {
          $a2 = array(
            'vendor_id' => $vendor_id,
            'society_id' => $society_id,
            'product_category_id' => $product_category_id,
            'created_date' => date('Y-m-d H:i:s')
          );
          $q = $d->insert("product_category_vendor_master", $a2);
          $_SESSION['msg'] = "Product Category Added Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Added Successfully");
        } else {
          $_SESSION['msg'] = "Product Category Added Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Successfully");
        }
      }
    }
    if ($q == TRUE) {
      header("Location: ../categoryMaster");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../categoryMaster");
    }
  }



  if (isset($_POST['addproductCategory'])) {
    $m->set_data('society_id', $society_id);
    $m->set_data('category_name', $category_name);
    $m->set_data('category_description', $category_description);
    $m->set_data('product_category_created_date', date("Y-m-d H:i:s"));

    $a1 = array(

      'society_id' => $m->get_data('society_id'),
      'category_name' => $m->get_data('category_name'),
      'category_description' => $m->get_data('category_description'),
      'product_category_created_date' => $m->get_data('product_category_created_date'),
    );

    if (isset($product_category_id) && $product_category_id > 0) {
      $CatSql = $d->selectRow('product_category_master.*', 'product_category_master', "category_name='$category_name'");
      $getCat = mysqli_fetch_assoc($CatSql);
      if ($getCat) {
        if ($product_category_id == $getCat['product_category_id']) {
          $q = $d->update("product_category_master", $a1, "product_category_id ='$product_category_id'");
          $_SESSION['msg'] = "Product Category Updated Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Updated Successfully");
        } else {
          $_SESSION['msg'] = "Product Category Already Exist";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
        }
      } else {
        $q = $d->update("product_category_master", $a1, "product_category_id ='$product_category_id'");
        $_SESSION['msg'] = "Product Category Updated Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Updated Successfully");
      }
    } else {
      $CatSql = $d->selectRow('product_category_master.*', 'product_category_master', "category_name='$category_name'");
      $getCat = mysqli_fetch_assoc($CatSql);
      if ($getCat) {
        $_SESSION['msg'] = "Product Category Already Exist";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product Category Already Exist");
      } else {
        $q = $d->insert("product_category_master", $a1);
        $product_category_id = $con->insert_id;
      }
    }
    if ($q == TRUE) {
      header("Location: ../categoryMaster");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../categoryMaster");
    }
  }
  if (isset($_POST['addproductCategoryVendor'])) {
   
    
    if(count($_POST['vendor_id'])>0){
      
      for($j=0;$j<count($_POST['vendor_id']); $j++){
      $vendor_id = $_POST['vendor_id'][$j];
      $m->set_data('society_id', $society_id);
      $m->set_data('vendor_id', $vendor_id);
      $m->set_data('product_category_id', $product_category_id);
      $a1 = array(
        'society_id' => $m->get_data('society_id'),
        'vendor_id' => $m->get_data('vendor_id'),
        'product_category_id' => $m->get_data('product_category_id'),
        'created_date' => date('Y-m-d'),
      );
      $VenCatSql = $d->selectRow('product_category_vendor_master.*', 'product_category_vendor_master', " vendor_id=$vendor_id AND product_category_id=$product_category_id");
      $getData = mysqli_fetch_assoc($VenCatSql);
      
      if ($getData) {
        $_SESSION['msg'] = " Vendor Product Category Already Exist";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " Vendor Product Category Already Exist");
      } else {
        $a2 = array(
          'vendor_id' => $vendor_id,
          'society_id' => $society_id,
          'product_category_id' => $product_category_id,
          'created_date' => date('Y-m-d H:i:s')
        );
        $q = $d->insert("product_category_vendor_master", $a2);
        $_SESSION['msg'] = "Vendor Product Category Added Success";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Vendor Product Category Added Success");
      }
    }
  }

    if ($q == TRUE) {
      header("Location: ../productVendorCategory?cId=$product_category_id");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../productVendorCategory?cId=$product_category_id");
    }
  }
}
