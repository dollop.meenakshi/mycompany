<?php
include '../common/objectController.php';

// error_reporting(E_ALL);
// ini_set('display_errors', '1');

if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if (isset($_POST['MarkAsLeave']))
    {
        $user_id = $leave_user_id;
        $user = $d->selectRow('*', "users_master", "user_id='$user_id'");
        $userData = mysqli_fetch_assoc($user);
        $year = date('Y');

        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('unit_id', $userData['unit_id']);
        $m->set_data('leave_type_id', $leave_type_id);
        $m->set_data('paid_unpaid', $paid_unpaid);
        $m->set_data('leave_start_date', $leave_date);
        $m->set_data('leave_end_date', $leave_date);
        $m->set_data('leave_day_type', $leave_day_type);
        $m->set_data('leave_created_date', date("Y-m-d H:i:s"));
        $m->set_data('leave_approved_by', $_COOKIE['bms_admin_id']);
        $m->set_data('leave_approved_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $society_id,
            'unit_id' => $userData['unit_id'],
            'leave_type_id' => $m->get_data('leave_type_id'),
            'paid_unpaid' => $m->get_data('paid_unpaid'),
            'leave_start_date' => $m->get_data('leave_start_date'),
            'leave_end_date' => $m->get_data('leave_end_date'),
            'leave_day_type' => $m->get_data('leave_day_type'),
            'leave_created_date' => $m->get_data('leave_created_date'),
            'leave_approved_by' => $m->get_data('leave_approved_by'),
            'leave_approved_date' => $m->get_data('leave_approved_date'),
            'leave_total_days' => 1,
            'leave_status' => 1,
            'leave_approved_by_type' => 1,
        );

        if ($leave_id > 0)
        {

            $q = $d->update("leave_master", $a1, "leave_id ='$leave_id'");
            $_SESSION['msg'] = "Leave Update Successfully";
        }
        else
        {
            $q = $d->insert("leave_master", $a1);
            $_SESSION['msg'] = "Leave Added Successfully";

        }

        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave  Added Successfully");
        if ($q == true)
        {
            $q2 = $d->update("attendance_master", array(
                'attendance_status' => 1,
                'auto_leave' => 0
            ), "attendance_id =$leave_attendance_id");

            header("Location: ../attendances?dId=" . $dpId . '&from=' . $fromDate . '&toDate=' . $toDateTo . '&bId=' . $branchId);
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../attendances?dId=" . $dpId . '&from=' . $fromDate . '&toDate=' . $toDateTo . '&bId=' . $branchId);
        }
    }

    if (isset($_POST['addLeave']))
    {
        $user = $d->selectRow("users_master.*, (SELECT leave_apply_on_date FROM `leave_type_master` WHERE leave_type_id = '$leave_type_id') AS leave_apply_on_date", "users_master", "user_id='$user_id'");
        $userData = mysqli_fetch_assoc($user);
        $year = date('Y');

        if ($userData['leave_apply_on_date'] == 1)
        {
            if (($userData['member_date_of_birth'] != null && $userData['member_date_of_birth'] != '0000-00-00') || ($userData['wedding_anniversary_date'] == null || $userData['wedding_anniversary_date'] == '0000-00-00'))
            {
                if ((date("m-d", strtotime($userData['wedding_anniversary_date'])) != date("m-d", strtotime($leave_start_date))) && (date("m-d", strtotime($userData['member_date_of_birth'])) != date("m-d", strtotime($leave_start_date))))
                {
                    $_SESSION['msg1'] = "Leave Not Added";
                    header("Location: ../addLeave");
                    exit();
                }
            }
        }

        $userLeaveq = $d->selectRow('user_total_leave,applicable_leaves_in_month', "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year=DATE_FORMAT('$leave_start_date', '%Y')");
        $userLeaveData = mysqli_fetch_assoc($userLeaveq);

        $approvedFullDayLeave = $d->selectRow("COUNT(*) AS approved_leave", "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_start_date', '%Y')");
        $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);

        $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_start_date', '%Y')");
        $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);

        $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave'] / 2;

        $remaining_leave = $userLeaveData['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave);

        $useHalfLeaveq = $d->selectRow("COUNT(*) AS use_leave", "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_day_type=1 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_start_date', '%Y-%m')");
        $useHalfLeaveData = mysqli_fetch_assoc($useHalfLeaveq);

        $useFullLeaveq = $d->selectRow("COUNT(*) AS use_leave", "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_day_type=0 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_start_date', '%Y-%m')");
        $useFullLeaveData = mysqli_fetch_assoc($useFullLeaveq);

        $use_leave = $useFullLeaveData['use_leave'] + ($useHalfLeaveData['use_leave'] / 2);

        if ($remaining_leave == 0.5 && $leave_day_type == 0)
        {
            $paid_unpaid = 1;
        }
        else if ($userLeaveData['applicable_leaves_in_month'] > 0 && $userLeaveData['applicable_leaves_in_month'] == $use_leave)
        {
            $paid_unpaid = 1;
        }
        else if ($remaining_leave <= 0)
        {
            $paid_unpaid = 1;
        }

        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('unit_id', $userData['unit_id']);
        $m->set_data('leave_type_id', $leave_type_id);
        $m->set_data('paid_unpaid', $paid_unpaid);
        $m->set_data('leave_start_date', $leave_start_date);
        $m->set_data('leave_end_date', $leave_start_date);
        $m->set_data('leave_day_type', $leave_day_type);
        $m->set_data('leave_created_date', date("Y-m-d H:i:s"));
        $m->set_data('leave_approved_by', $_COOKIE['bms_admin_id']);
        $m->set_data('leave_approved_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $society_id,
            'unit_id' => $userData['unit_id'],
            'leave_type_id' => $m->get_data('leave_type_id'),
            'paid_unpaid' => $m->get_data('paid_unpaid'),
            'leave_start_date' => $m->get_data('leave_start_date'),
            'leave_end_date' => $m->get_data('leave_end_date'),
            'leave_day_type' => $m->get_data('leave_day_type'),
            'leave_created_date' => $m->get_data('leave_created_date'),
            'leave_approved_by' => $m->get_data('leave_approved_by'),
            'leave_approved_date' => $m->get_data('leave_approved_date'),
            'leave_total_days' => 1,
            'leave_status' => 1,
            'leave_approved_by_type' => 1,
        );

        $q = $d->insert("leave_master", $a1);

        if ($q == true)
        {

            $title = "New leave Added";
            $description = "By Admin $created_by";
            $menuClick = "leave_tracker";
            $image = "";
            $activity = "0";
            $user_token = $userData['user_token'];
            $device = $userData['device'];
            if ($device == 'android')
            {
                $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            else
            {
                $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }

            $d->insertUserNotification($society_id, $title, $description, "leave_tracker", "leave_tracker.png", "user_id = '$user_id'");

            $_SESSION['msg'] = "Leave Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Added Successfully");
            header("Location: ../leaves");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addLeave");
        }
    }

    if (isset($_POST['leaveStatusApproved']))
    {
        $q3 = $d->selectRow('user_id,leave_start_date,leave_type_id,leave_start_date', "leave_master", "leave_id='$leave_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $uId = $data3['user_id'];
        $leave_type_id = $data3['leave_type_id'];
        $leave_start_date = $data3['leave_start_date'];

        if ($paid_unpaid == 0)
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$base_url/residentApiNew/commonController.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "action=getLeaveBalanceForAutoLeave&leave_type_id=$leave_type_id&leave_date=$leave_start_date&user_id=$uId&society_id=$society_id");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'key: ' . $keydb
            ));

            $server_output = curl_exec($ch);

            curl_close($ch);
            $server_output = json_decode($server_output, true);

            if ($server_output != '' && $server_output['leave']['available_paid_leave'] <= 0)
            {
                $_SESSION['msg1'] = "Paid Leave not availabl";
                header("Location: ../leaves");
                exit();
            }
        }

        $m->set_data('paid_unpaid', $paid_unpaid);
        $m->set_data('leave_status', $leave_status);
        $m->set_data('leave_approved_date', date("Y-m-d H:i:s"));
        $m->set_data('leave_approved_by', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'paid_unpaid' => $m->get_data('paid_unpaid'),
            'leave_status' => $m->get_data('leave_status'),
            'leave_approved_date' => $m->get_data('leave_approved_date'),
            'leave_approved_by' => $m->get_data('leave_approved_by'),
            'leave_approved_by_type' => 1,
        );

        if (isset($leave_id) && $leave_id > 0)
        {
            $q = $d->update('leave_master', $a1, "leave_id='$leave_id'");
        }
        if ($q == true)
        {
            $q2 = $d->selectRow('user_token,device', "users_master", "user_id='$uId'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your leave is Approved";
            //$description = "By Admin $created_by";
            $description = "For Date : " . date('d F Y', strtotime($data3['leave_start_date'])) . "\nBy Admin, " . $created_by;
            $menuClick = "leave_tracker";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device == 'android')
            {
                $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            else
            {
                $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }

            $d->insertUserNotificationWithJsonData($society_id, $title, $description, "leave_tracker", "leave_tracker.png", "$activity", "users_master.user_id = '$uId'");

            $_SESSION['msg'] = "Leave Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Status Change Successfully");
            header("Location: ../leaves");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaves");
        }
    }

    if (isset($_POST['leaveStatusReject']))
    {
        $q3 = $d->selectRow('user_id,leave_start_date', "leave_master", "leave_id='$leave_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $uId = $data3['user_id'];
        $leave_date_temp = $data3['leave_start_date'];
        $m->set_data('leave_status', $leave_status);
        $m->set_data('leave_admin_reason', $leave_admin_reason);
        $m->set_data('leave_declined_by', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'leave_status' => $m->get_data('leave_status'),
            'leave_admin_reason' => $m->get_data('leave_admin_reason'),
            'leave_declined_by' => $m->get_data('leave_declined_by'),
            'leave_declined_by_type' => 1,
        );
        if (isset($leave_id) && $leave_id > 0)
        {
            $q = $d->update('leave_master', $a1, "leave_id='$leave_id'");
        }
        if ($q == true)
        {

            $m->set_data('auto_leave', "0");
            $m->set_data('is_leave', "0");
            $m->set_data('extra_day_leave_type', "0");

            $a1 = array(
                'auto_leave' => $m->get_data('auto_leave'),
                'is_leave' => $m->get_data('is_leave'),
                'extra_day_leave_type' => $m->get_data('extra_day_leave_type'),
            );

            $d->update("attendance_master", $a1, "attendance_date_start = '$leave_date_temp' AND user_id = '$uId'");

            $q2 = $d->selectRow('user_token,device', "users_master", "user_id='$uId'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your leave is Declined";
            //$description = "By Admin $created_by";
            $description = "For Date : " . date('d F Y', strtotime($data3['leave_start_date'])) . "\nBy Admin, " . $created_by;
            $menuClick = "leave_tracker";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device == 'android')
            {
                $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            else
            {
                $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            $d->insertUserNotificationWithJsonData($society_id, $title, $description, "leave_tracker", "leave_tracker.png", "$activity", "users_master.user_id = '$uId'");

            $_SESSION['msg'] = "Leave Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Status Change Successfully");
            header("Location: ../leaves");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaves");
        }
    }

    if (isset($_POST['updateLeaveDayType']))
    {
        $m->set_data('leave_day_type', $leave_day_type);
        $m->set_data('paid_unpaid', $paid_unpaid);

        $a1 = array(
            'leave_day_type' => $m->get_data('leave_day_type'),
            'paid_unpaid' => $m->get_data('paid_unpaid'),
        );

        if (isset($leave_id) && $leave_id > 0)
        {
            $q = $d->update('leave_master', $a1, "leave_id='$leave_id'");
        }
        if ($q == true)
        {
            $_SESSION['msg'] = "Leave Day Type Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Leave Day Type Updated Successfully");
            header("Location: ../leaves");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaves");
        }
    }

    if (isset($_POST['action']) && $_POST['action'] == "deleteUserLeave")
    {
        $q3 = $d->selectRow('user_id,leave_start_date,leave_type_id', "leave_master", "leave_id='$leave_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $q4 = $d->selectRow('leave_type_name', "leave_type_master", "leave_type_id='$data3[leave_type_id]'");
        $data4 = mysqli_fetch_assoc($q4);
        $user_id = $data3['user_id'];

        $leave_date_temp = date("Y-m-d", strtotime($data3['leave_start_date']));
        $leave_type = $data4['leave_type_name'];

        $q = $d->delete("leave_master", "leave_id='$leave_id'");
        if ($q > 0)
        {

            $m->set_data('auto_leave', "0");
            $m->set_data('is_leave', "0");
            $m->set_data('extra_day_leave_type', "0");

            $a1 = array(
                'auto_leave' => $m->get_data('auto_leave'),
                'is_leave' => $m->get_data('is_leave'),
                'extra_day_leave_type' => $m->get_data('extra_day_leave_type'),
            );

            $d->update("attendance_master", $a1, "attendance_date_start = '$leave_date_temp' AND user_id = '$user_id'");

            $q2 = $d->selectRow('user_token,device', "users_master", "user_id='$data3[user_id]'");
            $data2 = mysqli_fetch_assoc($q2);

            $leave_date = date("d M Y", strtotime($data3['leave_start_date']));

            $title = "Your leave is Deleted";
            $description = "By Admin $created_by for leave date $leave_date and leave type is $leave_type";
            $menuClick = "leave_tracker";
            $image = "";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device == 'android')
            {
                $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            else
            {
                $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
            }
            $nq = $d->insertUserNotification($society_id, $title, $description, "leave_tracker", "leave_tracker.png", "user_id = '$data3[user_id]'");
            echo 1;
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "leave date $leave_date ($leave_type) Deleted , Employee id $user_id");
            $_SESSION['msg'] = "User Leave Deleted.";
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        }
        else
        {
            echo 0;
            $_SESSION['msg1'] = "Something Wrong";
            $response["message"] = "ID Required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    if (isset($_POST['updateAutoLeave']))
    {
        $lq = $d->selectRow("leave_type_name", "leave_type_master", "leave_type_id='$leave_type_id'");
        $leaveTypeData = mysqli_fetch_array($lq);
        $leave_type_name = $leaveTypeData['leave_type_name'];

        $m->set_data('leave_type_id', $leave_type_id);
        $m->set_data('paid_unpaid', $paid_unpaid);
        $m->set_data('leave_approved_by', $_COOKIE['bms_admin_id']);
        $m->set_data('leave_approved_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'leave_type_id' => $m->get_data('leave_type_id'),
            'paid_unpaid' => $m->get_data('paid_unpaid'),
            'leave_approved_by' => $m->get_data('leave_approved_by'),
            'leave_approved_by_type' => 1,
            'leave_approved_date' => $m->get_data('leave_approved_date'),
        );
        $q = $d->update('leave_master', $a1, "leave_id='$leave_id'");

        if ($q == true)
        {
            $_SESSION['msg'] = "Leave Type Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_full_name Auto Leave Converted to $leave_type_name");
            header("Location: ../autoLeaves");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../autoLeaves");
        }
    }

    if (isset($_POST['addPayOutLeave']))
    {
        for ($i = 0;$i < count($_POST['leave_type_id']);$i++)
        {
            $leave_type_id = $_POST['leave_type_id'][$i];
            $remaining_leave = $_POST['remaining_leave'][$i];
            $no_of_payout_leaves = $_POST['no_of_payout_leaves'][$i];
            $leave_payout_amount = $_POST['leave_payout_amount'][$i];
            $leave_payout_remark = $_POST['leave_payout_remark'][$i];
            if ($remaining_leave >= $no_of_payout_leaves)
            {
                if ($no_of_payout_leaves != '' && $no_of_payout_leaves > 0)
                {

                    $m->set_data('society_id', $society_id);
                    $m->set_data('user_id', $user_id);
                    $m->set_data('leave_type_id', $leave_type_id);
                    $m->set_data('no_of_payout_leaves', $no_of_payout_leaves);
                    $m->set_data('leave_payout_amount', $leave_payout_amount);
                    $m->set_data('leave_payout_remark', $leave_payout_remark);
                    $m->set_data('leave_payout_date', date("Y-m-d H:i:s"));
                    $m->set_data('leave_payout_year', $leave_payout_year);

                    $a1 = array(
                        'society_id' => $m->get_data('society_id'),
                        'user_id' => $m->get_data('user_id'),
                        'leave_type_id' => $m->get_data('leave_type_id'),
                        'no_of_payout_leaves' => $m->get_data('no_of_payout_leaves'),
                        'leave_payout_amount' => $m->get_data('leave_payout_amount'),
                        'leave_payout_remark' => $m->get_data('leave_payout_remark'),
                        'leave_payout_date' => $m->get_data('leave_payout_date'),
                        'leave_payout_year' => $m->get_data('leave_payout_year'),
                    );
                    $q = $d->insert("leave_payout_master", $a1);
                }
            }
        }
        if ($q == true)
        {
            $_SESSION['msg'] = "Levae Pay Out Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Levae Pay Out Added Successfully");
            header("Location: ../leaveAssign?bId=$block_id&dId=$floor_id&uId=$user_id&ltId=&year=$leave_payout_year");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leavePayOut?bId=$block_id&dId=$floor_id&uId=$user_id&year=$leave_payout_year");
        }
    }

    if (isset($_POST['addCarryForwardLeave']))
    {
        mysqli_autocommit($con, false);
        $uq = $d->selectRow('floor_id,unit_id', "users_master", "user_id='$user_id'");
        $uData = mysqli_fetch_array($uq);

        for ($i = 0;$i < count($_POST['carry_forward_leaves']);$i++)
        {
            $leave_type_id = $_POST['leave_type_id'][$i];
            $remaining_leave = $_POST['remaining_leave'][$i];
            $carry_forward_leaves = $_POST['carry_forward_leaves'][$i];
            $leave_payout_amount = $_POST['leave_payout_amount'][$i];
            $leave_payout_remark = $_POST['leave_payout_remark'][$i];

            if ($carry_forward_leaves != '' && $carry_forward_leaves > 0)
            {
                $userAssignLeaveq = $d->selectRow('*', "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year='$carry_forward_from_year'");
                $userAssignLeaveData = mysqli_fetch_assoc($userAssignLeaveq);
                if ($userAssignLeaveData)
                {
                    $user_total_leave = $userAssignLeaveData['user_total_leave'] + $carry_forward_leaves;
                    $a1 = array(
                        'user_total_leave' => $user_total_leave
                    );
                    $q = $d->update('leave_assign_master', $a1, "user_leave_id='$userAssignLeaveData[user_leave_id]'");
                }
                else
                {
                    $ldcq = $d->selectRow('*', "leave_default_count_master", "floor_id='$uData[floor_id]' AND leave_type_id='$leave_type_id' AND leave_year='$carry_forward_from_year'");
                    $ldcData = mysqli_fetch_assoc($ldcq);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('leave_type_id', $leave_type_id);
                    $m->set_data('user_id', $user_id);
                    $m->set_data('unit_id', $uData['unit_id']);
                    $m->set_data('user_total_leave', $ldcData['number_of_leaves'] + $carry_forward_leaves);
                    $m->set_data('applicable_leaves_in_month', $ldcData['leaves_use_in_month']);
                    $m->set_data('assign_leave_year', $carry_forward_from_year);
                    $m->set_data('user_leave_assign_by', $_COOKIE['bms_admin_id']);
                    $m->set_data('user_leave_assign_date', date("Y-m-d H:i:s"));

                    $a1 = array(
                        'society_id' => $m->get_data('society_id'),
                        'leave_type_id' => $m->get_data('leave_type_id'),
                        'user_id' => $m->get_data('user_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_total_leave' => $m->get_data('user_total_leave'),
                        'applicable_leaves_in_month' => $m->get_data('applicable_leaves_in_month'),
                        'assign_leave_year' => $m->get_data('assign_leave_year'),
                        'user_leave_assign_by' => $m->get_data('user_leave_assign_by'),
                        'user_leave_assign_date' => $m->get_data('user_leave_assign_date'),

                    );
                    $q = $d->insert("leave_assign_master", $a1);
                }
                $m->set_data('society_id', $society_id);
                $m->set_data('user_id', $user_id);
                $m->set_data('leave_type_id', $leave_type_id);
                $m->set_data('no_of_payout_leaves', $carry_forward_leaves);
                $m->set_data('leave_payout_remark', "Carry Forward Leaves from $carry_forward_from_year to $assign_leave_year");
                $m->set_data('leave_payout_date', date("Y-m-d H:i:s"));
                $m->set_data('leave_payout_year', $leave_payout_year);
                $m->set_data('is_leave_carry_forward', 1);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'leave_type_id' => $m->get_data('leave_type_id'),
                    'no_of_payout_leaves' => $m->get_data('no_of_payout_leaves'),
                    'leave_payout_remark' => $m->get_data('leave_payout_remark'),
                    'leave_payout_date' => $m->get_data('leave_payout_date'),
                    'leave_payout_year' => $m->get_data('leave_payout_year'),
                    'is_leave_carry_forward' => $m->get_data('is_leave_carry_forward'),
                );
                $q1 = $d->insert("leave_payout_master", $a1);
            }
        }

        if ($q && $q1)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Levae Carry Forward Successfully");
            mysqli_commit($con);
            $_SESSION['msg'] = "Levae Carry Forward Successfully";
            header("Location: ../leaveAssign?bId=$block_id&dId=$floor_id&uId=$user_id&ltId=&year=$leave_payout_year");
        }
        else
        {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../leaveCarryForward?bId=$block_id&dId=$floor_id&uId=$user_id&year=$leave_payout_year");
        }
    }

    if(isset($checkApprovedHalfDay))
    {
        $q = $d->selectRow("lm.leave_id","leave_master AS lm","DATE_FORMAT(lm.leave_start_date, '%Y-%m-%d') = DATE_FORMAT('$attendance_date', '%Y-%m-%d') AND lm.user_id = '$user_id' AND lm.leave_status = 1 AND lm.leave_day_type = 1");
        if(mysqli_num_rows($q))
        {
            $response = 1;
            echo json_encode($response);exit;
        }
        else
        {
            $response = 0;
            echo json_encode($response);exit;
        }
    }

    if(isset($addAttendance))
    {
       
        $check_pen_lea_half = $d->selectRow("lm.leave_id","leave_master AS lm","DATE_FORMAT(lm.leave_start_date, '%Y-%m-%d') = DATE_FORMAT('$attendance_date', '%Y-%m-%d') AND lm.user_id = '$user_id' AND lm.leave_status = 0 AND lm.leave_day_type = 1");
        if(mysqli_num_rows($check_pen_lea_half) > 0)
        {
            $_SESSION['msg1'] = "Please approve reject pending leave first!";
            header("Location: ../addAttendance?bId=$block_id&dId=$floor_id&uId=$user_id&month=$month&laYear=$laYear&getReport=Get&csrf=$csrf");exit;
        }

        $check_pen_lea = $d->selectRow("lm.leave_id","leave_master AS lm","DATE_FORMAT(lm.leave_start_date, '%Y-%m-%d') = DATE_FORMAT('$attendance_date', '%Y-%m-%d') AND lm.user_id = '$user_id' AND lm.leave_status = 0 AND lm.leave_day_type = 0");
        $leave_id_del_arr = [];
        if(mysqli_num_rows($check_pen_lea) > 0)
        {
            if($attendance_type == 1)
            {
                $_SESSION['msg1'] = "Please approve reject pending leave first!";
                header("Location: ../addAttendance?bId=$block_id&dId=$floor_id&uId=$user_id&month=$month&laYear=$laYear&getReport=Get&csrf=$csrf");exit;
            }
            while($l_data = $check_pen_lea->fetch_assoc())
            {
                $leave_id_del_arr[] = $l_data['leave_id'];
            }
        }

        if(isset($punch_out_date) && !empty($punch_out_date))
        {
            $attendance_date_end = $punch_out_date;
        }
        else
        {
            $attendance_date_end = $attendance_date;
        }

        if($shift_type == "Night")
        {
            $time1 = date("Y-m-d",strtotime($attendance_date)) . " " .$punch_in_time;
            $time2 = date("Y-m-d",strtotime($attendance_date_end)) . " " .$punch_out_time;
        }
        else
        {
            $time1 = $punch_in_time;
            $time2 = $punch_out_time;
        }
        $diff = abs(strtotime($time1) - strtotime($time2));
        $tmins = $diff/60;
        $hours = floor($tmins/60);
        $mins = $tmins%60;
        if($mins == 0)
        {
            $mins = "00";
        }
        $tot_hm = $hours . ":" . $mins;

        $dataArray = array(
            "punch_in_date" => $attendance_date,
            "punch_in_time" => $punch_in_time,
            "punch_out_date" => $attendance_date_end,
            "punch_out_time" => $punch_out_time,
            "punch_in_image" => "",
            "punch_out_image" => "",
            "location_name_in" => '',
            "location_name_out" => "",
            "working_hour" => $tot_hm,
            "working_hour_minute" => $tmins
        );
        $multPunchDataJson = json_encode($dataArray);
        $per_day_hour1 = explode(':', $per_day_hour);
        $per_day_hour_min = ($per_day_hour1[0]*60) + ($per_day_hour1[1]);
        // $per_day_hour_min = ($per_day_hour1[0]*60) + ($per_day_hour1[1]) + ($per_day_hour1[2]/60);
        $extra_min = $tmins - $per_day_hour_min;
        if($extra_min > 0)
        {
            $extra_min = $extra_min;
            if($extra_min % 60 == 0)
            {
                $aad = "00";
            }
            else
            {
                $aad = $extra_min % 60;
                if(strlen((string)$aad) == 1)
                {
                    $aad = str_pad($aad, 2, '0', STR_PAD_LEFT);
                }
            }
            $extra_hour_min = intdiv($extra_min, 60).':'. ($aad);
        }
        else
        {
            $extra_min = "00";
            $extra_hour_min = "0:00";
        }
        $secs = strtotime($late_time_start)-strtotime("00:00:00");
        $result = date("H:i:s",strtotime($shift_start_time)+$secs);

        $secs1 = strtotime($early_out_time)-strtotime("00:00:00");
        $result1 = date("H:i:s",strtotime($shift_end_time)-$secs1);

        $CURRENTTIME = new DateTime($punch_in_time);
        $OFFICETIME  = new DateTime($result);
        if ($CURRENTTIME > $OFFICETIME)
        {
           $late_in = 1;
        }
        else
        {
           $late_in = 0;
        }

        $CURRENTTIME1 = new DateTime($punch_out_time);
        $OFFICETIME1  = new DateTime($result1);
        if ($CURRENTTIME1 < $OFFICETIME1)
        {
           $early_out = 1;
        }
        else
        {
           $early_out = 0;
        }
        $avgWorkingDays = $tmins/$per_day_hour_min;
        $avg_working_days = round($avgWorkingDays * 2) / 2;
        $a1 = array(
            'society_id' => $society_id,
            'shift_time_id' => $shift_time_id,
            'total_shift_hours' => $per_day_hour,
            'user_id' => $user_id,
            'unit_id' => $unit_id,
            'attendance_date_start' => $attendance_date,
            'attendance_date_end' => $attendance_date_end,
            'punch_in_time' => $punch_in_time,
            'punch_out_time' => $punch_out_time,
            'multiple_punch_in_out_data' => "[".$multPunchDataJson."]",
            'last_punch_in_date' => $attendance_date,
            'last_punch_in_time' => $punch_in_time,
            'total_working_hours' => $tot_hm,
            'total_working_minutes' => $tmins,
            'extra_working_hours' => $extra_hour_min,
            'extra_working_hours_minutes' => $extra_min,
            'productive_working_hours' => $tot_hm,
            'productive_working_hours_minutes' => $tmins,
            'late_in' => $late_in,
            'early_out' => $early_out,
            'attendance_status' => 1,
            'punch_in_request_day_type' => $attendance_type,
            'attendance_status_change_by' => $_COOKIE['bms_admin_id'],
            'attendance_status_change_by_type' => 1,
            'attendance_created_time' => date("Y-m-d H:i:s"),
            'is_extra_day' => 1,
            'avg_working_days'=> $avg_working_days,
            'is_leave'=> 0,
            'extra_day_leave_type'=> 0,
            'punch_in_branch'=> 'Attendance added by admin',
            'punch_out_branch'=> 'Attendance added by admin'
        );
        $q = $d->insert("attendance_master", $a1);
        $attendance_id = $con->insert_id;
        if ($q)
        {
            $check_pen_lea_half2 = $d->selectRow("lm.leave_id","leave_master AS lm","DATE_FORMAT(lm.leave_start_date, '%Y-%m-%d') = DATE_FORMAT('$attendance_date', '%Y-%m-%d') AND lm.user_id = '$user_id' AND lm.leave_status = 1 AND lm.leave_day_type = 1");
            if(mysqli_num_rows($check_pen_lea_half2) == 0 && $attendance_type == 1)
            {
                $a12 = array(
                    'leave_type_id' => 0,
                    'paid_unpaid' => 1,
                    'society_id' => $society_id,
                    'user_id' => $user_id,
                    'unit_id' => $unit_id,
                    'auto_leave_reason' => "Attendance added from admin",
                    'leave_start_date' => $attendance_date,
                    'leave_end_date' => $attendance_date_end,
                    'leave_total_days' => 1,
                    'leave_day_type' => 1,
                    'half_day_session' => 0,
                    'leave_task_dependency' => 0,
                    'leave_status' => 1,
                    'leave_created_date' => date("Y-m-d H:i:s"),
                    'leave_approved_by_type' => 1,
                    'leave_approved_by' => $_COOKIE['bms_admin_id'],
                    'leave_approved_date' => date("Y-m-d H:i:s")
                );
                $q111 = $d->insert("leave_master", $a12);
                if($q111)
                {
                    $a22 =array(
                        'auto_leave'=> 1,
                        'is_leave'=> 2,
                        'extra_day_leave_type'=> 2
                    );
                    $d->update("attendance_master",$a22,"attendance_id = '$attendance_id'");
                }
            }
            if(count($leave_id_del_arr) > 0 && $attendance_type == 0)
            {
                foreach($leave_id_del_arr AS $ld => $ldi)
                {
                    $d->delete("leave_master","leave_id = '$ldi'");
                }
            }
            $d->insert_log("", "$society_id", $_COOKIE['bms_admin_id'], "$created_by", "Attendance Added Successfully");
            $_SESSION['msg'] = "Attendance Added Successfully.";
            header("Location: ../addAttendance?bId=$block_id&dId=$floor_id&uId=$user_id&month=$month&laYear=$laYear&getReport=Get&csrf=$csrf");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went wrong!";
            header("Location: ../addAttendance?bId=$block_id&dId=$floor_id&uId=$user_id&month=$month&laYear=$laYear&getReport=Get&csrf=$csrf");exit;
        }
    }
}
?>