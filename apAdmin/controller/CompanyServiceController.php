<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addCompanyService'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('company_service_name', $company_service_name);
        $m->set_data('company_service_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('company_service_created_at',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'company_service_name' => $m->get_data('company_service_name'),
            'company_service_added_by' => $m->get_data('company_service_added_by'),
            'company_service_created_at' => $m->get_data('company_service_created_at'),

        );

        if($_FILES['company_service_image']['tmp_name'] != '')
        {
            $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $uploadedFile = $_FILES['company_service_image']['tmp_name'];
            $ext = pathinfo($_FILES['company_service_image']['name'], PATHINFO_EXTENSION);
            $filesize = $_FILES["company_service_image"]["size"];
            $maxsize = 20971520;
            if (file_exists($uploadedFile)) {
                if (in_array($ext, $extension)) {
                    if ($filesize > $maxsize) {
                        $_SESSION['msg1'] = "Please Upload Photo Smaller Than 20MB.";
                        header("location:../companyServices");
                        exit();
                    }
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand();
                    $dirPath = "../../img/company_services/";
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    if ($imageWidth > 1200) {
                        $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                        $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "company_services." . $ext);
                            break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "company_services." . $ext);
                            break;

                        default:
                            $_SESSION['msg1'] = "Invalid Document ";
                            header("Location:../companyServices");
                            exit;
                            break;
                    }
                    $image = $newFileName . "company_services." . $ext;
                    $notiUrl = $base_url . 'img/company_services/' . $image;
                } else {
                    $_SESSION['msg1'] = "Invalid Document ";
                    header("location:../companyServices");
                    exit();
                }
            }
            $m->set_data('company_service_image', $image);
            $a1['company_service_image'] = $m->get_data('company_service_image');
        }



        if (isset($company_service_id) && $company_service_id > 0) {
            $q = $d->update("company_service_master", $a1, "company_service_id ='$company_service_id'");
            $_SESSION['msg'] = "Company Service Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service Updated Successfully");
        } else {
            $q = $d->insert("company_service_master", $a1);
            $_SESSION['msg'] = "Company Service Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Company Service Added Successfully");

        }
        if ($q == true) {
            header("Location: ../companyServices");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyServices");
        }

    }

}
