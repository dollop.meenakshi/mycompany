<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addBranchAccess'])) {

        $access_block_ids= implode(",", $_POST['access_block_ids']);
		$m->set_data('access_block_ids',$access_block_ids);
		$a =array(
			'access_block_ids'=> $m->get_data('access_block_ids'),
		);
        
		$q=$d->update("block_master",$a,"block_id='$block_id'");
        if ($q == true) {
			$d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Branch Access Successfully Added");
        	$_SESSION['msg']="Branch Access Successfully Added";
            header("Location: ../branchAccess");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../branchAccess");
        }

    }

	if (isset($_POST['addDepartmentAccess'])) {
            //array_push($_POST['access_floor_ids'],$floor_id);
		    $q = $d->selectRow('*', "floors_master", "floor_id=$floor_id");
            $data = mysqli_fetch_assoc($q);
            $_POST['block_id'][$floor_id]=$data['block_id'];
           
            $q = $d->delete("access_floor_master", "access_floor_id='$floor_id'");
            
		for ($i=0; $i <count($_POST['access_floor_ids']) ; $i++) { 
            
                $block_id = $_POST['block_id'][$_POST['access_floor_ids'][$i]];
                $a =array(
                    'floor_id'=> $_POST['access_floor_ids'][$i],
                    'access_floor_id'=> $floor_id,
                    'society_id'=> $society_id,
                    'block_id'=> $block_id,
                 );
                 $q2 = $d->insert("access_floor_master", $a);
            
        }
        
        if ($q2 == true) {
			$d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Department Access Successfully Added");
        	$_SESSION['msg']="Department Access Successfully Added";
            header("Location: ../departmentAccess");
        }elseif ($q == true) {
			$d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Department Access Successfully Updated");
        	$_SESSION['msg']="Department Access Successfully Updated";
            header("Location: ../departmentAccess");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../departmentAccess");
        }

    }

}
