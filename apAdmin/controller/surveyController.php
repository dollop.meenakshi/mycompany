<?php
include '../common/objectController.php';
extract($_POST);

$all = $xml->string->all . ' ' . $xml->string->floors;

if (isset($_POST) && !empty($_POST)) {
  if (isset($addSurvey)) {


    $m->set_data('society_id', test_input($society_id));
    $m->set_data('survey_title', $survey_title);
    $m->set_data('is_username_required', $is_username_required);
    $m->set_data('survey_desciption', $survey_desciption);
    $survey_date = date("Y-m-d", strtotime($survey_date));
    $m->set_data('survey_date', $survey_date);
    $m->set_data('survey_for', $survey_for);
    $a1 = array(
      'society_id' => $m->get_data('society_id'),
      'survey_title' => $m->get_data('survey_title'),
      'survey_desciption' => $m->get_data('survey_desciption'),
      'survey_date' => $m->get_data('survey_date'),
      'is_username_required' => $m->get_data('is_username_required'),
      'survey_for' => $m->get_data('survey_for'),
      'survey_status' => 2,
    );
    //echo "<pre>";print_r($a1);exit;
    $result = $d->insert("survey_master", $a1);
    $survey_id = $con->insert_id;

    if ($result == TRUE) {
      $qry = "";


      $qry = "";
      if ($survey_for == 0) {
        $append_query = "";
      } else if ($survey_for == 1) {
        $qry = " and floor_id=$survey_for ";
      } else if ($survey_for == 2) {
        $qry = " and floor_id=$survey_for ";
      }

      $survey_date =  date("d-m-Y", strtotime($survey_date));

      $_SESSION['msg'] = "Survey Added, Please Add Question now";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Survey Added Successfully, Please Add Question Now");
      header("Location: ../addSurvey?addQuestion=true&survey_id=$survey_id");
    } else {
      header("Location: ../viewSurvey");
    }
  }
  if (isset($editSurvey)) {
    $m->set_data('survey_title', test_input($survey_title));
    $m->set_data('is_username_required', $is_username_required);
    $m->set_data('survey_desciption', $survey_desciption);
    $m->set_data('survey_for', $survey_for);
    $survey_date = date("Y-m-d", strtotime($survey_date));
    $m->set_data('survey_date', $survey_date);
    $a1 = array(
      'survey_title' => $m->get_data('survey_title'),
      'survey_desciption' => $m->get_data('survey_desciption'),
      'is_username_required' => $m->get_data('is_username_required'),
      'survey_date' => $m->get_data('survey_date'),
      'survey_for' => $m->get_data('survey_for'),
    );
    $q = $d->update("survey_master", $a1, "society_id='$society_id' AND survey_id='$survey_id'");
    if ($q == TRUE) {

      $qry = "";
      if ($survey_for == 0) {
        $append_query = "";
      } else if ($survey_for == 1) {
        $qry = " and floor_id=$survey_for ";
      } else if ($survey_for == 2) {
        $qry = " and floor_id=$survey_for ";
      }

      $survey_date =  date("d-m-Y", strtotime($survey_date));

      $_SESSION['msg'] = "Survey Details Updated";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Survey Updated");
      header("Location: ../viewSurvey");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../viewSurvey");
    }
  }
  if (isset($addSurveyQue)) {
    //IS_1310
    $sq = $d->select("survey_result_master", "survey_id = '$survey_id' AND society_id = '$society_id' ");
    if (mysqli_num_rows($sq) > 0) {
      $_SESSION['msg1'] = "Survey Already Running";
      header("location:../surveyQuestions?survey_id=$survey_id");
      exit();
    }

    $values = array_count_values($_POST['option_name']);
    if (count($_POST['option_name']) < 1) {
      $_SESSION['msg1'] = "Please provide option ";
      header('Location: ' . $_SERVER['HTTP_REFERER']);
      exit;
    }
    for ($i = 0; $i < count($values); $i++) {
      $detail = $values[$_POST['option_name'][$i]];
      if ($detail > 1) {
        $_SESSION['msg1'] = "Please provide unique option name for polling options to avoid confusion";
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
      }
    }
    //IS_1310

    $extension = array("jpeg", "jpg", "png", "gif", "JPG", "jpeg", "JPEG", "PNG");
    $uploadedFile = $_FILES['question_image']['tmp_name'];
    $ext = pathinfo($_FILES['question_image']['name'], PATHINFO_EXTENSION);
    if (file_exists($uploadedFile)) {
      if (in_array($ext, $extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand() . $user_id;
        $dirPath = "../../img/survey/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];

        // less 30 % size 
        $newImageWidth = $imageWidth * 30 / 100;
        $newImageHeight = $imageHeight * 30 / 100;
        switch ($imageType) {
          case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagepng($tmp, $dirPath . $newFileName . "." . $ext);
            break;
          case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagejpeg($tmp, $dirPath . $newFileName . "." . $ext);
            break;

          case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagegif($tmp, $dirPath . $newFileName . "." . $ext);
            break;
          default:
            $_SESSION['msg1'] = "Invalid Image";
            header("Location: ../surveyQuestions?survey_id=$survey_id");
            exit;
            break;
        }
        $question_image = $newFileName . "." . $ext;
      } else {
        $_SESSION['msg1'] = "Invalid Profile Photo";
        header("location:../surveyQuestions?survey_id=$survey_id");
        exit();
      }
    } else {
      $question_image = "";
    }
    $m->set_data('society_id', $society_id);
    $m->set_data('survey_id', $survey_id);
    $m->set_data('survey_question', test_input($survey_question));
    $m->set_data('question_image', test_input($question_image));
    $a = array(
      'society_id' => $m->get_data('society_id'),
      'survey_id' => $m->get_data('survey_id'),
      'survey_question' => $m->get_data('survey_question'),
      'question_image' => $m->get_data('question_image'),
    );
    $q1 = $d->insert("survey_question_master", $a);
    $survey_question_id = $con->insert_id;

    $option_name = implode("~", $_POST['option_name']);
    $option_name1 = explode('~', (string)$option_name);
    for ($i = 0; $i < $no_of_option; $i++) {

      $m->set_data('survey_question_id', $survey_question_id);
      $m->set_data('survey_id', $survey_id);
      $m->set_data('society_id', $society_id);
      $m->set_data('survey_option_name', test_input($option_name1[$i]));
      $a = array(
        'survey_question_id' => $m->get_data('survey_question_id'),
        'survey_id' => $m->get_data('survey_id'),
        'society_id' => $m->get_data('society_id'),
        'survey_option_name' => $m->get_data('survey_option_name'),
      );
      $q = $d->insert("survey_option_master", $a);
    }

    if ($q > 0 && $q1 > 0) {
      $_SESSION['msg'] = "Question Added";
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Question Added");
      if ($btnType == "0") {
        header("Location: ../addSurvey?addQuestion=true&survey_id=$survey_id");
      } else {
        header("location:../surveyQuestions?survey_id=$survey_id");
      }
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("location:../surveyQuestions?survey_id=$survey_id");
    }
  }
  if (isset($editSurveyQue)) {
    //echo "<pre>";print_r($_POST);exit;
    $options = array();
    for ($g = 0; $g < count($_POST['option_name']); $g++) {
      $options[] = strtolower($_POST['option_name'][$g]);
    }
    $values = array_count_values($options);
    for ($i = 0; $i < count($values); $i++) {
      $detail = $values[$options[$i]];
      if ($detail > 1) {
        $_SESSION['msg1'] = "Please provide unique option name for polling options to avoid confusion";
        header('Location: ../surveyQuestions?survey_id=$survey_id');
        exit;
      }
    }
    $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG", "jpeg");
    $uploadedFile = $_FILES['question_image']['tmp_name'];
    $ext = pathinfo($_FILES['question_image']['name'], PATHINFO_EXTENSION);
    if (file_exists($uploadedFile)) {
      if (in_array($ext, $extension)) {
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand() . $user_id;
        $dirPath = "../../img/survey/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];

        // less 30 % size 
        $newImageWidth = $imageWidth * 30 / 100;
        $newImageHeight = $imageHeight * 30 / 100;
        switch ($imageType) {
          case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagepng($tmp, $dirPath . $newFileName . "." . $ext);
            break;
          case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagejpeg($tmp, $dirPath . $newFileName . "." . $ext);
            break;

          case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile);
            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
            imagegif($tmp, $dirPath . $newFileName . "." . $ext);
            break;
          default:
            $_SESSION['msg1'] = "Invalid Image";
            header("Location: ../surveyQuestions?survey_id=$survey_id");
            exit;
            break;
        }
        $question_image = $newFileName . "." . $ext;
      } else {
        $_SESSION['msg1'] = "Invalid Profile Photo";
        header("location:../surveyQuestions?survey_id=$survey_id");
        exit();
      }
    } else {
      $question_image = $question_image_old;
    }
    $m->set_data('survey_question', test_input($survey_question));
    $m->set_data('question_image', test_input($question_image));
    $m->set_data('survey_id', $survey_id);
    $a = array(
      'survey_question' => $m->get_data('survey_question'),
      'question_image' => $m->get_data('question_image')
    );
    $q = $d->update("survey_question_master", $a, "survey_question_id='$survey_question_id' AND survey_id='$survey_id' AND society_id='$society_id'");
    /*echo "survey_id='$survey_id' AND society_id='$society_id'";
        echo "<pre>";print_r($_FILES);
          echo "<pre>";print_r($_POST);
            echo "<pre>";print_r($a);exit;*/
    $q2 = $d->delete("survey_option_master", "survey_question_id='$survey_question_id'");
    if ($q2) {
      for ($i = 0; $i < count($_POST['option_name']); $i++) {
        $m->set_data('society_id', $society_id);
        $m->set_data('option_name', test_input($_POST['option_name'][$i]));
        $m->set_data('survey_question_id', $survey_question_id);
        $a = array(
          'survey_id' => $m->get_data('survey_id'),
          'society_id' => $m->get_data('society_id'),
          'survey_question_id' => $m->get_data('survey_question_id'),
          'survey_option_name' => $m->get_data('option_name'),
        );
        $q = $d->insert("survey_option_master", $a);
      }
    }
    if ($q > 0) {
      $_SESSION['msg'] = "Question Updated";
      header("location:../surveyQuestions?survey_id=$survey_id");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("location:../surveyQuestions?survey_id=$survey_id");
    }
  }
  if (isset($deleteSurveyQuestion)) {
    $q2 = $d->delete("survey_question_master", "survey_question_id='$survey_question_id'");
    if ($q2 > 0) {
      $_SESSION['msg'] = "Question Deleted";
      header("location:../surveyQuestions?survey_id=$survey_id");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("location:../surveyQuestions?survey_id=$survey_id");
    }
  }
  if (isset($deleteSurvey)) {

    $q2 = $d->delete("survey_master", "survey_id='$survey_id'");
    if ($q2 > 0) {
      $_SESSION['msg'] = "Survey Deleted";
      header("location:../viewSurvey");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("location:../viewSurvey");
    }
  }
}
