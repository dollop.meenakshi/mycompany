<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
  if(isset($_POST['EditPanelty'])){

 
    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['penalty_photo']['tmp_name'];
    $ext = pathinfo($_FILES['penalty_photo']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["penalty_photo"]["size"];
    $maxsize = 2000000;
    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {
        if($filesize > $maxsize){
          $_SESSION['msg1']="Please Upload Photo Smaller Than 2MB.";
          header("location:../penalties");
          exit();
        }
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand();
        $dirPath = "../../img/billReceipt/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagepng($tmp,$dirPath. $newFileName. "penalty.". $ext);
            break;           

            case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagejpeg($tmp,$dirPath. $newFileName. "penalty.". $ext);
            break;
            
            case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagegif($tmp,$dirPath. $newFileName. "penalty.". $ext);
            break;

            default:
            $_SESSION['msg1']="Invalid Image";
            header("Location: ../penalties");
            exit;
            break;
          }
          $penalty_photo= $newFileName."penalty.".$ext;
          $notiUrl = $base_url.'img/billReceipt/'.$penalty_photo;



        } else {
          $_SESSION['msg1']="Invalid Photo";
          header("location:../penalties");
          exit();
        }
      } else {
      $penalty_photo = $penalty_photo_old;
     }
        
       $uploadedFile2 = $_FILES['penalty_photo_2']['tmp_name'];
  $ext = pathinfo($_FILES['penalty_photo_2']['name'], PATHINFO_EXTENSION);
  $filesize = $_FILES["penalty_photo_2"]["size"];
  $maxsize = 8000000;
  if(file_exists($uploadedFile2)) {
    if(in_array($ext,$extension)) {
      if($filesize > $maxsize){
        $_SESSION['msg1']="Please Upload Photo Smaller Than 8MB.";
        header("location:../penalties");
        exit();
      }
      $sourceProperties = getimagesize($uploadedFile2);
      $newFileName = rand().$user_id;
      $dirPath = "../../img/billReceipt/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];
      if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile2); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagepng($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
            break;           

            case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile2); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagejpeg($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
            break;
            
            case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile2); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagegif($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
            break;

            default:
            $_SESSION['msg1']="Invalid Photo 2";
            header("Location: ../penalties");
            exit;
            break;
          }
          $penalty_photo_2= $newFileName."penalty_2.".$ext;
        } else {
          $_SESSION['msg1']="Invalid Photo 2";
          header("location:../penalties");
          exit();
        }
      } else {
       $penalty_photo_2= $penalty_photo_old_2;
     }

     $uploadedFile3 = $_FILES['penalty_photo_3']['tmp_name'];
  $ext = pathinfo($_FILES['penalty_photo_3']['name'], PATHINFO_EXTENSION);
  $filesize = $_FILES["penalty_photo_3"]["size"];
  $maxsize = 8000000;
  if(file_exists($uploadedFile3)) {
    if(in_array($ext,$extension)) {
      if($filesize > $maxsize){
        $_SESSION['msg1']="Please Upload Photo Smaller Than 8MB.";
        header("location:../penalties");
        exit();
      }
      $sourceProperties = getimagesize($uploadedFile3);
      $newFileName = rand().$user_id;
      $dirPath = "../../img/billReceipt/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];
      if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagepng($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;           

            case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagejpeg($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;
            
            case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagegif($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;

            default:
            $_SESSION['msg1']="Invalid Photo 3";
            header("Location: ../penalties");
            exit;
            break;
          }
          $penalty_photo_3= $newFileName."penalty_3.".$ext;
        } else {
          $_SESSION['msg1']="Invalid Photo 3";
          header("location:../penalties");
          exit();
        }
      } else {
       $penalty_photo_3=$penalty_photo_old_3;
     }

     
     $m->set_data('society_id',$society_id);
     $m->set_data('penalty_name',$penalty_name);
     $m->set_data('balancesheet_id',$balancesheet_id);
     $m->set_data('penalty_date',date("Y-m-d H:i:s",strtotime($penalty_date)));
    // $m->set_data('penalty_amount',$penalty_amount);
     $penalty_datetime = $penalty_date;
     $m->set_data('penalty_datetime', date("Y-m-d H:i:s",strtotime($penalty_date)));

      if ($is_taxble=='0') {
          $taxble_type = '';
          $tax_slab = '';
      }
     //IS_1470
      $m->set_data('amount_without_gst',$penalty_amount);
      $m->set_data('gst',$gst);
      $m->set_data('tax_slab',$tax_slab);
      $m->set_data('is_taxble',$is_taxble);
      $m->set_data('taxble_type',$taxble_type);
      $m->set_data('penalty_photo',$penalty_photo);
      $m->set_data('penalty_photo_2',$penalty_photo_2);
      $m->set_data('penalty_photo_3',$penalty_photo_3);
          if($gst=="1"){ 
            $gst_amount =  $penalty_amount *$tax_slab/100;  //only for Exclude
            $penalty_amount = $penalty_amount + $gst_amount;
          } else {
            $gst_amount =  $penalty_amount - ($penalty_amount * (100/(100+$tax_slab)));  //only for Include
            $penalty_amount = $penalty_amount;
            $m->set_data('amount_without_gst',($penalty_amount -$gst_amount ));
          }

          $m->set_data('penalty_amount',$penalty_amount);
     //IS_1470
          
     $a1 = array(


      'balancesheet_id'=>$m->get_data('balancesheet_id'),
      'penalty_name'=>$m->get_data('penalty_name'),
      'penalty_date'=>$m->get_data('penalty_date'),
      
      'penalty_datetime' => $m->get_data('penalty_datetime'),
      'penalty_photo'=>$m->get_data('penalty_photo'),
      'penalty_photo_2'=>$m->get_data('penalty_photo_2'),
      'penalty_photo_3'=>$m->get_data('penalty_photo_3'),

      'penalty_amount'=>$m->get_data('penalty_amount'),
      //IS_1470

      'gst'=>$m->get_data('gst'),
      'tax_slab'=>$m->get_data('tax_slab'), 
      'amount_without_gst'=>$m->get_data('amount_without_gst'), 
      'is_taxble'=>$m->get_data('is_taxble'), 
      'taxble_type'=>$m->get_data('taxble_type'), 
     //IS_1470
    );
     
    // echo "<pre>";print_r($a1);exit;
      $q=$d->update("penalty_master",$a1,"society_id='$society_id' and penalty_id='$penalty_id' ");

     if($q==TRUE) {

      $_SESSION['msg']="Penalty Updated";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Penalty Updated");
      header("Location: ../penalties");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../penalties");
   }

 }
//IS_605
  // Add Income
 if($_POST['penalty_name'] && isset($_POST['add']) ) {
  extract($_POST);
 /*  foreach ($user_id as $key => $value)
  {
    $user = explode("~", $value);
    $unit_id_check = $user[0];
    $user_id_check = $user[1];

    //IS_1019
    $penalty_master=$d->select("penalty_master","  ( upper(penalty_name) = '".$penalty_name."' OR lower(penalty_name) = '".$penalty_name."' )     and penalty_date='".$penalty_date."' and  balancesheet_id ='$balancesheet_id' and user_id ='$user_id_check' and  unit_id ='$unit_id_check' and  society_id='$society_id' ");

    if(mysqli_num_rows($penalty_master) >0 ){
      $_SESSION['msg1']="Duplicate Penalty";
      header("location:../penalties");
      exit();
    }
  } */
  //IS_1019
  //print_r($user_id);die;
  foreach ($user_id as $key => $value)
  {
   /// $user =  $value;
    
    $user_id = $value;

    $user = $d->selectRow("user_full_name,unit_id","users_master","user_id = '$user_id'");
    $user_details = $user->fetch_assoc();
    $user_name = str_replace(' ', '_', $user_details['user_full_name']);
    $unit_id =$user_details['unit_id'];
    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['penalty_photo']['tmp_name'];
    $ext = pathinfo($_FILES['penalty_photo']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["penalty_photo"]["size"];
    $maxsize = 8000000;
    if(file_exists($uploadedFile))
    {
      if(in_array($ext,$extension))
      {
        if($filesize > $maxsize)
        {
          $_SESSION['msg1']="Please Upload Photo Smaller Than 8MB.";
          header("location:../penalties");
          exit();
        }
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = $user_name.rand().$user_id;
        $dirPath = "../../img/billReceipt/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        if ($imageWidth>1200)
        {
          $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
          $newImageWidth = $imageWidth * $newWidthPercentage /100;
          $newImageHeight = $imageHeight * $newWidthPercentage /100;
        }
        else
        {
          $newImageWidth = $imageWidth;
          $newImageHeight = $imageHeight;
        }
        switch ($imageType)
        {
          case IMAGETYPE_PNG:
          $imageSrc = imagecreatefrompng($uploadedFile);
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagepng($tmp,$dirPath. $newFileName. "penalty.". $ext);
          break;

          case IMAGETYPE_JPEG:
          $imageSrc = imagecreatefromjpeg($uploadedFile);
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagejpeg($tmp,$dirPath. $newFileName. "penalty.". $ext);
          break;

          case IMAGETYPE_GIF:
          $imageSrc = imagecreatefromgif($uploadedFile);
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagegif($tmp,$dirPath. $newFileName. "penalty.". $ext);
          break;

          default:
          $_SESSION['msg1']="Invalid Photo 1";
          header("Location: ../penalties");
          exit;
          break;
        }
        $penalty_photo= $newFileName."penalty.".$ext;
        $notiUrl = $base_url.'img/billReceipt/'.$penalty_photo;
      }
      else
      {
        $_SESSION['msg1']="Invalid Photo 1";
        header("location:../penalties");
        exit();
      }
    }
    else
    {
      $_SESSION['msg1']="Invalid Photo 1";
      header("location:../penalties");
      exit();
    }

    $uploadedFile2 = $_FILES['penalty_photo_2']['tmp_name'];
    $ext = pathinfo($_FILES['penalty_photo_2']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["penalty_photo_2"]["size"];
    $maxsize = 8000000;
    if(file_exists($uploadedFile2))
    {
      if(in_array($ext,$extension)) 
      {
        if($filesize > $maxsize){
          $_SESSION['msg1']="Please Upload Photo Smaller Than 8MB.";
          header("location:../penalties");
          exit();
        }
        $sourceProperties = getimagesize($uploadedFile2);
        $newFileName = $user_name.rand().$user_id;
        $dirPath = "../../img/billReceipt/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        if ($imageWidth>1200) 
        {
          $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
          $newImageWidth = $imageWidth * $newWidthPercentage /100;
          $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } 
        else 
        {
          $newImageWidth = $imageWidth;
          $newImageHeight = $imageHeight;
        }
        switch ($imageType) 
        {
          case IMAGETYPE_PNG:
          $imageSrc = imagecreatefrompng($uploadedFile2); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagepng($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
          break;           

          case IMAGETYPE_JPEG:
          $imageSrc = imagecreatefromjpeg($uploadedFile2); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagejpeg($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
          break;
          
          case IMAGETYPE_GIF:
          $imageSrc = imagecreatefromgif($uploadedFile2); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagegif($tmp,$dirPath. $newFileName. "penalty_2.". $ext);
          break;

          default:
          $_SESSION['msg1']="Invalid Photo 2";
          header("Location: ../penalties");
          exit;
          break;
        }
        $penalty_photo_2= $newFileName."penalty_2.".$ext;
      } 
      else 
      {
        $_SESSION['msg1']="Invalid Photo 2";
        header("location:../penalties");
        exit();
      }
    }
    else
    {
      $penalty_photo_2= "";
    }

    $uploadedFile3 = $_FILES['penalty_photo_3']['tmp_name'];
    $ext = pathinfo($_FILES['penalty_photo_3']['name'], PATHINFO_EXTENSION);
    $filesize = $_FILES["penalty_photo_3"]["size"];
    $maxsize = 8000000;
    if(file_exists($uploadedFile3))
    {
    if(in_array($ext,$extension)) {
      if($filesize > $maxsize){
        $_SESSION['msg1']="Please Upload Photo Smaller Than 8MB.";
        header("location:../penalties");
        exit();
      }
      $sourceProperties = getimagesize($uploadedFile3);
      $newFileName = $user_name.rand().$user_id;
      $dirPath = "../../img/billReceipt/";
      $imageType = $sourceProperties[2];
      $imageHeight = $sourceProperties[1];
      $imageWidth = $sourceProperties[0];
      if ($imageWidth>1200) {
            $newWidthPercentage= 1200*100 / $imageWidth;  //for maximum 400 widht
            $newImageWidth = $imageWidth * $newWidthPercentage /100;
            $newImageHeight = $imageHeight * $newWidthPercentage /100;
          } else {
            $newImageWidth = $imageWidth;
            $newImageHeight = $imageHeight;
          }



          switch ($imageType) {

            case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagepng($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;           

            case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagejpeg($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;
            
            case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile3); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
            imagegif($tmp,$dirPath. $newFileName. "penalty_3.". $ext);
            break;

            default:
            $_SESSION['msg1']="Invalid Photo 3";
            header("Location: ../penalties");
            exit;
            break;
          }
          $penalty_photo_3= $newFileName."penalty_3.".$ext;
        } else {
          $_SESSION['msg1']="Invalid Photo 3";
          header("location:../penalties");
          exit();
        }
      }
    else
    {
     $penalty_photo_3= "";
    }

    $m->set_data('society_id',$society_id);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('user_id',$user_id);
    $m->set_data('unit_id',$unit_id);
    $m->set_data('penalty_name',$penalty_name);
    $m->set_data('penalty_date',$penalty_date);
    $m->set_data('penalty_photo',$penalty_photo);
    $m->set_data('penalty_photo_2',$penalty_photo_2);
    $m->set_data('penalty_photo_3',$penalty_photo_3);

    //IS_678
    $penalty_datetime = date("Y-m-d", strtotime($penalty_date))." ".date("H:i:s");
    $m->set_data('penalty_datetime',date("Y-m-d H:i:s", strtotime($penalty_date)));
    $m->set_data('created_at',date("Y-m-d H:i:s"));

    //IS_1470
    $m->set_data('amount_without_gst',$penalty_amount);
    $m->set_data('gst',$gst);
    $m->set_data('tax_slab',$tax_slab);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('taxble_type',$taxble_type);
    // $gst_amount = $penalty_amount - ($penalty_amount * (100/(100+(int)$tax_slab))); 

    if($gst=="1")
    {
      $gst_amount_new =  $penalty_amount *(int)$tax_slab/100;  //only for Exclude
      $penalty_amount_new = $penalty_amount + $gst_amount_new;
    }
    else
    {
      $gst_amount_new =  $penalty_amount - ($penalty_amount * (100/(100+(int)$tax_slab)));  //only for Include
      $penalty_amount_new = $penalty_amount;
      $m->set_data('amount_without_gst',($penalty_amount -$gst_amount_new ));
    }

    $penalty_amount_new = number_format($penalty_amount_new,2,'.','');
    $m->set_data('penalty_amount',$penalty_amount_new);
    //IS_1470

    $a1 = array(
      'society_id'=>$m->get_data('society_id'),
      'balancesheet_id'=>$m->get_data('balancesheet_id'),
      'user_id'=>$m->get_data('user_id'),
      'unit_id'=>$m->get_data('unit_id'),
      'penalty_name'=>$m->get_data('penalty_name'),
      'penalty_date'=>$m->get_data('penalty_date'),
      'penalty_photo'=>$m->get_data('penalty_photo'),
      'penalty_photo_2'=>$m->get_data('penalty_photo_2'),
      'penalty_photo_3'=>$m->get_data('penalty_photo_3'),
          //IS_678
      'penalty_datetime' => $m->get_data('penalty_datetime'),
      'penalty_amount'=>$m->get_data('penalty_amount'),
      'created_at'=>$m->get_data('created_at'),
       //IS_1470
      'gst'=>$m->get_data('gst'),
      'tax_slab'=>$m->get_data('tax_slab'),
      'amount_without_gst' => $m->get_data('amount_without_gst'),
      'is_taxble' => $m->get_data('is_taxble'),
      'taxble_type' => $m->get_data('taxble_type'),
     //IS_1470
    );
    $final_penalty_amount = $m->get_data('penalty_amount');
    $q = $d->insert("penalty_master",$a1);
    if($q==TRUE)
    {
      $qUserToken=$d->select("users_master","society_id='$society_id' AND unit_id='$unit_id' AND user_id='$user_id'");
      $data_notification=mysqli_fetch_array($qUserToken);
      $sos_user_token=$data_notification['user_token'];
      $device=$data_notification['device'];
      $user_email=$data_notification['user_email'];
      $user_full_name=$data_notification['user_full_name'];
      if ($device=='android') {
       $nResident->noti("PenaltyFragment",$notiUrl,$society_id,$sos_user_token,"Penalty Added by $created_by","$penalty_name, amount is :$currency $penalty_amount ",'penalty');
       }  else if($device=='ios') {
        $nResident->noti_ios("PenaltyVC",$notiUrl,$society_id,$sos_user_token,"Penalty Added by $created_by","$penalty_name, amount is :$currency $penalty_amount",'penalty');
      }
       $society_name = $_COOKIE['society_name'];
      if ($user_email!='') {
        $to = $user_email;
        $subject = "New Penalty Generated";
        include '../mail/penaltyMail.php';
        include '../mail.php';
      }

      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$data_notification['user_id'],
        'notification_title'=>"Penalty Added by $created_by",
        'notification_desc'=>"$penalty_name, amount is :$currency $penalty_amount",
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'penalty',
        'notification_logo'=>'penaltyNew.png',
      );
      $d->insert("user_notification",$notiAry);
      $_SESSION['msg']="Penalty Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Penalty Added for $user_name");
    }
  }
  if($q==TRUE)
  {
    $_SESSION['msg']="Penalty Added";
    header("Location: ../penalties");
  }
  else
  {
    $_SESSION['msg1']="Something Wrong";
    header("Location: ../penalties");
  }
}

if (isset($sendMail)) {
    $society_name = $_COOKIE['society_name'];
    $penalty_name = $penalty_name_email;

      $subject = "Pay Your Penalty";
      include '../mail/penaltyMail.php';
      include '../mail.php';
       $_SESSION['msg']="Email send successfully";
     header("Location: ../penalties");
}
if (isset($paidPanalty)) {
//IS_1019
$penalty_master=$d->select("penalty_master","   penalty_receive_date='".date("Y-m-d H:i:s", strtotime($received_date))."' and  penalty_payment_type ='$penalty_payment_type' and bank_name ='$bank_name' and  payment_ref_no ='$payment_ref_no' and  penalty_id='$penalty_id' ");

 if(mysqli_num_rows($penalty_master) >0 ){
    $_SESSION['msg1']="Penalty Alrady Paid";
    header("location:../penalties");
    exit();
 }

 $get_det = $d->selectRow("block_name,floor_name","users_master JOIN block_master ON block_master.block_id = users_master.block_id JOIN floors_master ON floors_master.floor_id = users_master.floor_id","users_master.user_id = '$user_id'");
 $blo_flo = $get_det->fetch_assoc();
 $unit_name = $blo_flo['floor_name'] . " (" . $blo_flo['block_name'] . ")";

//IS_1019
      if ($penalty_amount>$penalty_amount_org) {
        $credit_amount = $penalty_amount -$penalty_amount_org;
        $qqq=$d->selectRow("user_mobile","users_master","user_id='$user_id' ");
          $userData= mysqli_fetch_array($qqq);
          $user_mobile = $userData['user_mobile'];
          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
          $row=mysqli_fetch_array($count6);
          $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

          $remark= "Overpaid Payment of Panalty ($currency $penalty_amount)";

          $m->set_data('society_id',$society_id);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('unit_id',$unit_id);
          $m->set_data('remark',$remark);
          $m->set_data('credit_amount',$credit_amount);
          $m->set_data('avl_balance',$totalDebitAmount1+$credit_amount);


          $aWallet = array(
            'society_id'=>$m->get_data('society_id'),
            'user_mobile'=>$m->get_data('user_mobile'),
            'unit_id'=>$m->get_data('unit_id'),
            'remark'=>$m->get_data('remark'),
            'credit_amount'=>$m->get_data('credit_amount'),
            'avl_balance'=>$m->get_data('avl_balance'),
            'created_date'=>date("Y-m-d H:i:s"), 
            'admin_id'=>$_COOKIE['bms_admin_id'],
          );
          
         $wallet_amount_type = 0;
         $wallet_amount = $credit_amount;
         $mailRemark = "& $credit_amount Credited in wallet";
         

      }

      $d1=date("Y-m-d H:i:s");
       $penalty_receive_date= $received_date;

      $d2=date("Y-m-d");
      $expDateAry = explode("-", $d1);
      $exYear = $expDateAry[0];
      $exMonth = $expDateAry[1];

      $dateObj   = DateTime::createFromFormat('!m', $exMonth);
      $monthName = $dateObj->format('F'); 
      $expenses_created_date = $monthName.'-'.$exYear;

      $penalty_amount = $penalty_amount_org;

      $m->set_data('payment_type',$payment_type);

      $m->set_data('expenses_title',"Penalty paid by $user_name");
      $m->set_data('income_amount',$penalty_amount);
      $m->set_data('expenses_add_date',$received_date);
      $m->set_data('balancesheet_id',$balancesheet_id);
      $m->set_data('expenses_created_date',$expenses_created_date);
      $m->set_data('received_by',$created_by);
      $m->set_data('paid_by',$user_id);
      $m->set_data('wallet_amount',$wallet_amount);
      $m->set_data('wallet_amount_type',$wallet_amount_type);


//IS_1686
    $penalty_master_qry=$d->select("penalty_master","penalty_id='$penalty_id' ");
     $penalty_master_data=mysqli_fetch_array($penalty_master_qry);
     $m->set_data('amount_without_gst',$penalty_master_data['amount_without_gst']);
     $m->set_data('gst',$penalty_master_data['gst']);
     $m->set_data('tax_slab',$penalty_master_data['tax_slab']);
     $m->set_data('is_taxble',$penalty_master_data['is_taxble']);
     $m->set_data('taxble_type',$penalty_master_data['taxble_type']);
 //IS_1686

       //IS_846
      $m->set_data('bank_name',$bank_name);
      $m->set_data('payment_ref_no',$payment_ref_no);
       //IS_846
      $a5 = array(
        'paid_status'=>1,
        'penalty_receive_date' =>$penalty_receive_date,
        'penalty_payment_type' =>$payment_type,
        'bank_name' => $bank_name,
        'payment_ref_no' => $payment_ref_no,
        'paid_by' => $user_id,
        'wallet_amount' => $wallet_amount,
        'wallet_amount_type' => $wallet_amount_type,
        'collected_by' => $_COOKIE['bms_admin_id'],
      );
     $q= $d->update("penalty_master",$a5,"penalty_id='$penalty_id'");


      if ($q==true) {
        if ($wallet_amount>0) {
          $wq=$d->insert("user_wallet_master",$aWallet);
        }

       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Penalty Collected");

       $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
       $data_notification=mysqli_fetch_array($qUserToken);
       $user_email=$data_notification['user_email'];
       $unit_id=$data_notification['unit_id'];
       $sos_user_token=$data_notification['user_token'];
       $user_first_name=$data_notification['user_full_name'];
       $device=$data_notification['device'];
       if ($device=='android') {
         $nResident->noti("PenaltyFragment","",$society_id,$sos_user_token,"Your Penalty Paid","Penalty Amount is :$currency $penalty_amount",'penalty');
       }  else if($device=='ios') {
        $nResident->noti_ios("PenaltyVC","",$society_id,$sos_user_token,"Your Penalty Paid","Penalty Amount is :$currency $penalty_amount",'penalty');
      }

      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$user_id,
        'notification_title'=>'Your Penalty Paid',
        'notification_desc'=>"Penalty Amount is :$currency $penalty_amount",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'penalty',
        'notification_logo'=>'penaltyNew.png',
      );
      $d->insert("user_notification",$notiAry);

       if ($user_email!='') {
        $to=$user_email;
        $paid_by= $user_first_name;
        $received_by= $created_by;
        $received_amount=$penalty_amount;
        $invoice_number= "INVPN".$penalty_id;
        $description= "Penalty Payment";
        $subject='Penalty Payment Acknowledgement '.$invoice_number;
        $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=P&societyid=$society_id&id=$penalty_id";
        $receive_date = $penalty_receive_date;
        $category = "Penalty";
        $txt_status= "Success";
        $txt_id = "NA";
        switch ($payment_type) {
          case '1':
            $payment_mode = 'Cheque';
            break;
          case '2':
            $payment_mode = 'Online Payment';
            break;
          default:
            $payment_mode = 'Cash';
            break;
        }


        include '../mail/paymentReceipt.php';
        include '../mail.php';
        // echo $message;
      }
      $d->delete("payment_paid_request","penalty_id='$penalty_id' AND penalty_id!=0");
      $_SESSION['msg']="Penalty Collected";
      header("Location: ../penalties");
    } else {
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../penalties");
    }
  }

  if (isset($rejectPenaltyRequest)) {

      $q= $d->delete("payment_paid_request","payment_request_id='$payment_request_id' AND unit_id='$unit_id'");


      if ($device=='android') {
        $nResident->noti("PenaltyFragment","",$society_id,$user_token,"Penalty Paid Request Rejected","Rejected by $created_by",'penalty');
      }  else if($device=='ios') {
        $nResident->noti_ios("PenaltyVC","",$society_id,$user_token,"Penalty Paid Request Rejected","Rejected by $created_by",'penalty');
      }


      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$user_id,
        'notification_title'=>"Penalty Paid Request Rejected",
        'notification_desc'=>"Rejected by $created_by",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'penalty',
        'notification_logo'=>'penaltyNew.png',
      );
      $d->insert("user_notification",$notiAry);
    if ($q==TRUE) {
      $_SESSION['msg']='Penalty Paid Request Rejected';
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Penalty Paid Request Rejected");
      header("Location: ../penalties");
    } else {
      $_SESSION['msg1']='Error';
      header("Location: ../penalties");
    }


  }

}
?>