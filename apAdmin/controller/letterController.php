<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if (isset($_POST['addLetterTemplate']))
    {
        $len = strlen($_POST['letter_text']);
        if($len < 100)
        {
            $_SESSION['msg1'] = "Enter minimum 100 characters in letter text";
            header("Location: ../addLetterTemplate?letter_id=$letter_id");exit;
        }
        $m->set_data('letter_text', $letter_text);
        $a1 = array(
            'society_id' => 0,
            'letter_text' => $m->get_data('letter_text')
        );
        if (isset($letter_id) && $letter_id > 0)
        {
            $q = $d->update("letter_master", $a1, "letter_id = '$letter_id'");
            $_SESSION['msg'] = "Letter Template Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Letter Updated Successfully");
        }
        else
        {
            $q = $d->insert("letter_master", $a1);
            $_SESSION['msg'] = "Letter Template Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Letter Added Successfully");
        }
        if($q)
        {
            header("Location: ../manageLetterTemplate");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../addLetterTemplate?letter_id=$letter_id");
        }
    }
    elseif(isset($_POST['getLetterDesc']))
    {
        $q = $d->selectRow("lm.letter_id,lm.letter_varibles,lm.letter_name,lm.letter_text","letter_master AS lm","lm.letter_id = '$letter_id'");
        $data = $q->fetch_assoc();
        echo json_encode($data);exit;
    }
    elseif(isset($_POST['generateDocuments']))
    {
        $q = $d->selectRow("lm.letter_text,lm.letter_varibles,lm.letter_name","letter_master AS lm","lm.letter_id = '$letter_id'");
        $data = $q->fetch_assoc();
        extract($data);
        $letter_varibles = (array)json_decode($letter_varibles);
        $in_letter_var = [];
        foreach($letter_varibles AS $key => $value)
        {
            if(preg_match("/{$key}/i", $letter_text))
            {
                $in_letter_var[] = $key;
            }
        }
        $get_h_f = $d->select("letter_setting_master");
        $lhf = $get_h_f->fetch_assoc();
        if(isset($preview))
        {
            $offer_date = date("F j, Y");
            $company_name = $_COOKIE['society_name'];

            $letter_text = str_replace('offer_date', $offer_date, $letter_text);
            $letter_text = str_replace('joining_date', $offer_date, $letter_text);
            $letter_text = str_replace('company_name', $company_name, $letter_text);
            $letter_text = str_replace('employee_full_name', $created_by, $letter_text);
            $letter_text = str_replace('employee_first_name', "John", $letter_text);
            $letter_text = str_replace('employee_address', "21, Shiv Shpg Cntr, J.p.rd, 7 Bunglows, Andheri (west), Mumbai - 400053", $letter_text);
            $letter_text = str_replace('employee_designation', "Ios Developer", $letter_text);
            $letter_text = str_replace('employee_mobile', "+91 96380 17000", $letter_text);
            $letter_text = str_replace('ctc', "650000.00", $letter_text);
            $letter_text = str_replace('&nbsp;',' ', $letter_text);
            $company_website = str_replace("https://","",$company_website);
            $company_website = str_replace("http://","",$company_website);
            if($company_website != "" && $secretary_email != "")
            {
                $email_web = '<br>Web: ' . $company_website . "  -  Email: " . $secretary_email;
            }
            else if($company_website == "" && $secretary_email != "")
            {
                $email_web =  "<br>Email: " . $secretary_email;
            }
            else if($company_website != "" && $secretary_email == "")
            {
                $email_web = '<br>Web: ' . $company_website;
            }
            else
            {
                $email_web = '';
            }

            if($lhf['footer_image'] != "" && !empty($lhf['footer_image']) && file_exists('../../img/'.$lhf['footer_image']))
            {
                $fi = $lhf['footer_image'];
                $foot = '<img style="height:130px;width:100%;" src="../../img/'.$fi.'"/>';
                $margin_bottom = 8;
                $footer = 1;
            }
            else
            {
                $foot = $society_address_invoice;
                $foot .= $email_web;
                $margin_bottom = 4;
                $footer = 2;
            }

            if($lhf['header_image'] != "" && !empty($lhf['header_image']) && file_exists('../../img/'.$lhf['header_image']))
            {
                $margin_top = 30;
            }
            else
            {
                $margin_top = 4;
            }

            if(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo))
            {
                $margin_top = 43;
            }

            require '../mpdf_new/autoload.php';
            $pdf = new \Mpdf\Mpdf(['format' => 'A4', 'orientation' => 'P', 'margin_left' => 0, 'margin_right' => 0, 'margin_top' => $margin_top, 'margin_bottom' => $margin_bottom, 'margin_header' => 0, 'margin_footer' => 0,'tempDir' =>  '../mpdf_new/mpdf/mpdf/tmp']);
            $pdf->SetTitle($letter_name." Preview");
            $pdf->showImageErrors = true;
            $pdf->curlAllowUnsafeSslRequests = true;
            if($lhf['header_image'] != "" && !empty($lhf['header_image']) && file_exists('../../img/'.$lhf['header_image']))
            {
                $hi = $lhf['header_image'];
                $pdf->SetHeader('<img style="height:90px;width:100%;" src="../../img/'.$hi.'"/><br><br>');
            }
            elseif(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo))
            {
                $pdf->SetHeader('<img style="width:100px;margin-right:40px;margin-top:20px;" src="../../img/society/'.$socieaty_logo.'"/><br><br>');
            }
            if(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo) && $lhf['add_watermark'] == 1)
            {
                $pdf->SetWatermarkImage('../../img/society/'.$socieaty_logo,0.2,array(80,60));
                $pdf->showWatermarkImage = true;
            }
            $pdf->setAutoBottomMargin = 'stretch';
            if($footer == 1)
            {
                $pdf->SetFooter($foot);
            }
            else
            {
                $pdf->SetFooter("<div style='margin:10px;'>".$foot."</div>","",true);
            }
            $pdf->writeHTML("<div style='margin-left:15px;margin-right:15px'>".$letter_text."</div>", 2);
            $filename = "Preview ".$letter_name."_".time().".pdf";
            $pdf->Output($filename, 'D');
        }
        else
        {
            if($society_address_invoice == "")
            {
                $response['file_name'] = "";
                $response['status'] = 10;
                echo json_encode($response);exit;
            }
            if($secretary_email == "")
            {
                $response['file_name'] = "";
                $response['status'] = 11;
                echo json_encode($response);exit;
            }
            if($company_website == "")
            {
                $response['file_name'] = "";
                $response['status'] = 12;
                echo json_encode($response);exit;
            }
            $check = $d->selectRow("hdm.hr_document_id,hdm.hr_document_category_id,hdm.hr_document_sub_category_id,hdm.hr_document_file","hr_document_master AS hdm","hdm.hr_document_category_id = '$hr_document_category_id' AND hdm.hr_document_sub_category_id = '$hr_document_sub_category_id' AND hdm.user_id = '$user_id' AND hdm.letter_id = '$letter_id'");
            if(mysqli_num_rows($check) > 0)
            {
                $old_data = $check->fetch_assoc();
                $old_hr_document_file = $old_data['hr_document_file'];
                $hr_document_id_old = $old_data['hr_document_id'];
                if(file_exists("../../img/hrdoc/".$old_hr_document_file))
                {
                    $response['file_name'] = $old_hr_document_file;
                    $response['status'] = 2;
                    echo json_encode($response);exit;
                }
                else
                {
                    $d->delete("hr_document_master","hr_document_id = '$hr_document_id_old'");
                }
            }

            $sal_det = $d->selectRow("s.gross_salary","salary AS s","s.user_id = '$user_id' AND s.is_preivous_salary = 0 AND s.is_active = 0 AND s.is_delete = 0","ORDER BY s.salary_id DESC");
            if(mysqli_num_rows($sal_det) > 0)
            {
                $s_det = $sal_det->fetch_assoc();
                $ctc = $s_det['gross_salary'];
                if(($ctc == "0" || $ctc == "" || empty($ctc)) && in_array('ctc',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 9;
                    echo json_encode($response);exit;
                }
            }
            else
            {
                if(in_array('ctc',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 9;
                    echo json_encode($response);exit;
                }
            }

            $u_det = $d->selectRow("ued.joining_date,um.user_full_name,um.user_first_name,um.user_designation,um.permanent_address,um.user_mobile,um.country_code,um.floor_id,um.block_id,um.unit_id","users_master AS um LEFT JOIN user_employment_details AS ued ON ued.user_id = um.user_id","um.user_id = '$user_id'");
            if(mysqli_num_rows($u_det) > 0)
            {
                $fud = $u_det->fetch_assoc();
                $joining_date = $fud['joining_date'];
                if(($joining_date == "0000-00-00" || $joining_date == "") && in_array('joining_date',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 3;
                    echo json_encode($response);exit;
                }
                $employee_full_name = $fud['user_full_name'];
                if((empty($employee_full_name) || $employee_full_name == "") && in_array('employee_full_name',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 4;
                    echo json_encode($response);exit;
                }
                $employee_first_name = $fud['user_first_name'];
                if((empty($employee_first_name) || $employee_first_name == "") && in_array('employee_first_name',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 5;
                    echo json_encode($response);exit;
                }
                $employee_address = $fud['permanent_address'];
                if((empty($employee_address) || $employee_address == "") && in_array('employee_address',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 6;
                    echo json_encode($response);exit;
                }
                $employee_designation = $fud['user_designation'];
                if((empty($employee_designation) || $employee_designation == "") && in_array('employee_designation',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 7;
                    echo json_encode($response);exit;
                }
                $employee_mobile = $fud['country_code'].' '.$fud['user_mobile'];
                if((empty($employee_mobile) || $employee_mobile == "") && in_array('employee_mobile',$in_letter_var))
                {
                    $response['file_name'] = "";
                    $response['status'] = 8;
                    echo json_encode($response);exit;
                }
                $floor_id = $fud['floor_id'];
                $block_id = $fud['block_id'];
                $unit_id = $fud['unit_id'];
            }
            $offer_date = date("F j, Y");
            $joining_date =date("F j, Y",strtotime($joining_date));
            $company_name = $_COOKIE['society_name'];

            $letter_text = str_replace('offer_date', $offer_date, $letter_text);
            $letter_text = str_replace('joining_date', $joining_date, $letter_text);
            $letter_text = str_replace('company_name', $company_name, $letter_text);
            $letter_text = str_replace('employee_full_name', $employee_full_name, $letter_text);
            $letter_text = str_replace('employee_first_name', $employee_first_name, $letter_text);
            $letter_text = str_replace('employee_address', $employee_address, $letter_text);
            $letter_text = str_replace('employee_designation', $employee_designation, $letter_text);
            $letter_text = str_replace('employee_mobile', $employee_mobile, $letter_text);
            $letter_text = str_replace('ctc', $ctc, $letter_text);
            $letter_text = str_replace('&nbsp;',' ', $letter_text);
            $employee_full_name_new = ucwords($employee_full_name);
            $company_website = str_replace("https://","",$company_website);
            $company_website = str_replace("http://","",$company_website);
            if($company_website != "" && $secretary_email != "")
            {
                $email_web = '<br>Web: ' . $company_website . "  -  Email: " . $secretary_email;
            }
            else if($company_website == "" && $secretary_email != "")
            {
                $email_web =  "<br>Email: " . $secretary_email;
            }
            else if($company_website != "" && $secretary_email == "")
            {
                $email_web = '<br>Web: ' . $company_website;
            }
            else
            {
                $email_web = '';
            }

            if($lhf['footer_image'] != "" && !empty($lhf['footer_image']) && file_exists('../../img/'.$lhf['footer_image']))
            {
                $fi = $lhf['footer_image'];
                $foot = '<img src="../../img/'.$fi.'"/>';
                $margin_bottom = 8;
                $footer = 1;
            }
            else
            {
                $foot = $society_address_invoice;
                $foot .= $email_web;
                $margin_bottom = 4;
                $footer = 2;
            }

            if($lhf['header_image'] != "" && !empty($lhf['header_image']) && file_exists('../../img/'.$lhf['header_image']))
            {
                $margin_top = 43;
            }
            else
            {
                $margin_top = 4;
            }

            if(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo))
            {
                $margin_top = 43;
            }

            require '../mpdf_new/autoload.php';
            $pdf = new \Mpdf\Mpdf(['format' => 'A4', 'orientation' => 'P', 'margin_left' => 0, 'margin_right' => 0, 'margin_top' => $margin_top, 'margin_bottom' => $margin_bottom, 'margin_header' => 0, 'margin_footer' => 0,'tempDir' =>  '../mpdf_new/mpdf/mpdf/tmp']);
            $pdf->SetTitle($letter_name." Preview");
            $pdf->showImageErrors = true;
            $pdf->curlAllowUnsafeSslRequests = true;
            if($lhf['header_image'] != "" && !empty($lhf['header_image']) && file_exists('../../img/'.$lhf['header_image']))
            {
                $hi = $lhf['header_image'];
                $pdf->SetHeader('<img src="../../img/'.$hi.'"/><br><br>');
            }
            elseif(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo))
            {
                $pdf->SetHeader('<img style="width:100px;margin-right:40px;margin-top:20px;" src="../../img/society/'.$socieaty_logo.'"/><br><br>');
            }

            if(isset($socieaty_logo) && !empty($socieaty_logo) && $socieaty_logo != "" && file_exists("../../img/society/".$socieaty_logo) && $lhf['add_watermark'] == 1)
            {
                $pdf->SetWatermarkImage('../../img/society/'.$socieaty_logo,0.2,array(80,60));
                $pdf->showWatermarkImage = true;
            }
            $pdf->setAutoBottomMargin = 'stretch';
            if($footer == 1)
            {
                $pdf->SetFooter($foot);
            }
            else
            {
                $pdf->SetFooter("<div style='margin:10px;'>".$foot."</div>","",true);
            }
            $pdf->writeHTML("<div style='margin-left:15px;margin-right:15px'>".$letter_text."</div>", 2);
            $filename = "../../img/hrdoc/".$employee_full_name_new."_offer_letter_".$user_id."_".time().".pdf";
            $ofn = $employee_full_name_new."_offer_letter_".$user_id."_".time().".pdf";
            $file = $pdf->Output($filename, 'F');
            if($file)
            {
                $a = array(
                    'hr_document_category_id' => $hr_document_category_id,
                    'hr_document_sub_category_id' => $hr_document_sub_category_id,
                    'society_id' => $society_id,
                    'floor_id' => $floor_id,
                    'block_id' => $block_id,
                    'user_id' => $user_id,
                    'unit_id' => $unit_id,
                    'document_type' => 0,
                    'hr_document_file' => $ofn,
                    'hr_document_name' => $letter_name,
                    'hr_document_uploaded_date' => date("Y-m-d H:i:s"),
                    'hr_document_uploaded_by' => $_COOKIE['bms_admin_id'],
                    'hr_document_status' => 0,
                    'hr_doc_deleted' => 0,
                    'letter_id' => $letter_id
                );
                $q = $d->insert("hr_document_master",$a);
                if($q)
                {
                    $response['file_name'] = $ofn;
                    $response['status'] = 1;
                    echo json_encode($response);exit;
                }
                else
                {
                    $response['file_name'] = "";
                    $response['status'] = 0;
                    echo json_encode($response);exit;
                }
            }
            else
            {
                $response['file_name'] = "";
                $response['status'] = 0;
                echo json_encode($response);exit;
            }
        }
    }
    elseif(isset($_POST['changeLetterSettingStatus']))
    {
        $a = [
            'add_watermark' => ($status == "Active" ? 1 : 2)
        ];
        $q = $d->update("letter_setting_master",$a,"");
        if($q)
        {
            echo 1;
        }
        else
        {
            echo 2;
        }
    }
    elseif(isset($_POST['saveLetterImages']))
    {
        $file = $_FILES['header_image']['tmp_name'];
        if(file_exists($file))
        {
            $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['header_image']['size'] >= $maxsize) || ($_FILES["header_image"]["size"] == 0))
            {
                $_SESSION['msg1'] = "Header photo too large. Must be less than 2 MB.";
                header("location:../manageLetterTemplate");exit;
            }
            if (!in_array($_FILES['header_image']['type'], $acceptable) && (!empty($_FILES["header_image"]["type"])))
            {
                $_SESSION['msg1'] = "Invalid header photo. Only JPG and PNG are allowed.";
                header("location:../manageLetterTemplate");exit;
            }
            if (count($errors) === 0)
            {
                $temp = explode(".", $_FILES["header_image"]["name"]);
                $header_image = 'header_image' . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["header_image"]["tmp_name"], "../../img/" . $header_image);
                if($old_header_image != "")
                {
                    unlink('../../img/'.$old_header_image);
                }
            }
        }
        else
        {
            $header_image = $old_header_image;
        }

        $file = $_FILES['footer_image']['tmp_name'];
        if(file_exists($file))
        {
            $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['footer_image']['size'] >= $maxsize) || ($_FILES["footer_image"]["size"] == 0))
            {
                $_SESSION['msg1'] = "Header photo too large. Must be less than 2 MB.";
                header("location:../manageLetterTemplate");exit;
            }
            if (!in_array($_FILES['footer_image']['type'], $acceptable) && (!empty($_FILES["footer_image"]["type"])))
            {
                $_SESSION['msg1'] = "Invalid header photo. Only JPG and PNG are allowed.";
                header("location:../manageLetterTemplate");exit;
            }
            if (count($errors) === 0)
            {
                $temp = explode(".", $_FILES["footer_image"]["name"]);
                $footer_image = 'footer_image' . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["footer_image"]["tmp_name"], "../../img/" . $footer_image);
                if($old_footer_image != "")
                {
                    unlink('../../img/'.$old_footer_image);
                }
            }
        }
        else
        {
            $footer_image = $old_footer_image;
        }

        $a = [
            'header_image' => $header_image,
            'footer_image' => $footer_image
        ];
        if(isset($update))
        {
            $q = $d->update("letter_setting_master",$a,"");
        }
        else
        {
            $q = $d->insert("letter_setting_master",$a);
        }

        if($q)
        {
            $_SESSION['msg'] = "Images Uploaded Successfully";
            header("Location: ../manageLetterTemplate");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../manageLetterTemplate");
        }
    }
    elseif(isset($_POST['deleteHeaderImage']))
    {
        $a = [
            'header_image' => ''
        ];
        $q = $d->update("letter_setting_master",$a,"");
        if($q)
        {
            unlink('../../img/'.$header_image);
            echo 1;
        }
        else
        {
            echo 2;
        }
    }
    elseif(isset($_POST['deleteFooterImage']))
    {
        $a = [
            'footer_image' => ''
        ];
        $q = $d->update("letter_setting_master",$a,"");
        if($q)
        {
            unlink('../../img/'.$footer_image);
            echo 1;
        }
        else
        {
            echo 2;
        }
    }
}
?>