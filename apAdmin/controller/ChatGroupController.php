<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    if (isset($_POST['removeEmployeeFromGroup'])) {


        $q = $d->update("chat_group_member_master", array("chat_group_member_delete" =>'1'), "chat_group_member_id ='$chat_group_member_id'");

        if ($q == true) {
             $topic_name = $society_id.'group'.$group_id;
             if ($topic_name!='' && isset($registrationIds)) {
                $server_key=$d->get_server_key();
                $project_id=$d->get_project_id();
                include '../../residentApiNew/unSubscribeTopic.php';
                // $nResidentTopic->noti("","",$society_id,$topic_name,$title,$description,$dataJson);
            }
            
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Chat Group Member $employee_name Delete From $group_name Group");

            $_SESSION['msg'] = "Chat Group Member Deleted Successfully";
            header("Location: ../chatGroupMember?gId=$group_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../chatGroupMember?gId=$group_id");
        }

    }
    //IS_605
    if (isset($_POST['updateChatGroup'])) {

        $qtc=$d->select("chat_group_member_master","group_id='$group_id' AND user_id='$user_id'");
        if (mysqli_num_rows($qtc)==0) {
           $a2 = array(
            'group_id' => $group_id,
            'society_id' => $society_id,
            'user_id' => $user_id,
            ); 

            $d->insert("chat_group_member_master", $a2);
        }

        $m->set_data('society_id', $society_id);
        $m->set_data('group_name', $group_name);
        $m->set_data('created_by', $user_id);


        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'group_name' => $m->get_data('group_name'),
            'created_by' => $m->get_data('created_by'),

        );

        $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES['group_icon']['tmp_name'];
        $ext = pathinfo($_FILES['group_icon']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES["group_icon"]["size"];
        $maxsize = 2097152;
        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                if ($filesize > $maxsize) {
                    $_SESSION['msg1'] = "Please Upload Photo Smaller Than 2MB.";
                    header("location:../chatGroupMaster");
                    exit();
                }
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../../img/users/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1200) {
                    $newWidthPercentage = 1200 * 100 / $imageWidth; //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                 
                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    default:
                        $_SESSION['msg1'] = "Invalid Document ";
                        header("location:../chatGroupMaster");
                        exit;
                        break;
                }
                $image = $newFileName . "_group." . $ext;
                $notiUrl = $base_url . 'img/users/' . $image;
            } else {
                $image_Arr = $_FILES['group_icon'];   
                $temp = explode(".", $_FILES["group_icon"]["name"]);
                $image ='Society_'.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["group_icon"]["tmp_name"], "../../img/users/".$image);
            }
               
                $m->set_data('group_icon', $image);
                $a1['group_icon'] = $m->get_data('group_icon');
            } else {
                $_SESSION['msg1'] = "Invalid Document ";
                header("location:../chatGroupMaster");
                exit();
            }
        }

        if (isset($group_id) && $group_id > 0) {
            $q = $d->update("chat_group_master", $a1, "group_id ='$group_id'");
            $_SESSION['msg'] = "Chat Group Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Chat Group Updated Successfully");
        }
        if ($q == true) {
            header("Location: ../chatGroupMaster");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../chatGroupMaster");
        }

    }

    if (isset($_POST['deleteAutoGroup'])) {
          
          $q=$d->delete("chat_group_master","group_id='$group_id'");
          $q=$d->delete("chat_group_unread_message","group_id='$group_id'");
          $q=$d->delete("chat_master_group","group_id='$group_id'");

          if($q>0) {
             $_SESSION['msg'] = "Chat Group Deleted";
             $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group $group_id Deleted");
             header("Location: ../chatGroupMaster");
          } else {
             $_SESSION['msg1'] = "Something Wrong";
             header("Location: ../chatGroupMaster");
          }
    }

    if (isset($_POST['createAutoGroup'])) {

          if (count($_POST['group_common_id'])==0) {
            $_SESSION['msg1'] = "Please Select at least 1 Checkbox";
            header("Location: ../chatGroupMaster");
            exit();
          }
          
          for ($i=0; $i <count($_POST['group_common_id']) ; $i++) { 
            
            $group_common_id = $_POST['group_common_id'][$i];
            $alq = $d->select("chat_group_master","group_type='$group_type' AND group_common_id='$group_common_id'");
            if (mysqli_num_rows($alq)==0) {
                  // code...
                $m->set_data('society_id', $society_id);
                $m->set_data('group_name', $group_name);
                $m->set_data('group_type', $group_type);
                $m->set_data('group_common_id',$group_common_id);
                $m->set_data('created_date',date("Y-m-d H:i:s"));


                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'group_name' => $m->get_data('group_name'),
                    'group_type' => $m->get_data('group_type'),
                    'group_common_id' => $m->get_data('group_common_id'),
                    'created_date' => $m->get_data('created_date'),
                );

                $q = $d->insert("chat_group_master", $a1);
            }  

          }
         

          if($q>0) {
             $_SESSION['msg'] = "Auto Chat Group Created";
             $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Auto Group Created");
             header("Location: ../chatGroupMaster");
          } else {
             $_SESSION['msg1'] = "Something Wrong";
             header("Location: ../chatGroupMaster");
          }
    }

    if (isset($_POST['addGroupMember'])) {

        $userIdCount = count($_POST['user_id']);

        for ($i=0; $i < $userIdCount; $i++) { 
            $m->set_data('group_id', $group_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('user_id', $_POST['user_id'][$i]);

            $a1 = array(
                'group_id' => $m->get_data('group_id'),
                'society_id' => $m->get_data('society_id'),
                'user_id' => $m->get_data('user_id'),
            );

            $q=$d->insert("chat_group_member_master",$a1);
        }

        if($q){
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Group Member Added Successfully");
            $_SESSION['msg'] = "Group Member Added Successfully";
			header("location:../chatGroupMember?gId=$group_id");
        }else{
            $_SESSION['msg1']="Something Wrong";
			header("location:../chatGroupMember");
        }
    }

}
