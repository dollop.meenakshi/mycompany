<?php
include '../common/objectController.php';
$language_id = $_COOKIE['language_id'];
$xml = simplexml_load_file("../../img/$language_id.xml");
$societyLngName = $xml->string->society;

if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    $extension = array("jpeg","jpg","png","gif","webp","heic");
    if (isset($_POST['addSisterCompany']))
    {
        $scnus = str_replace(' ', '_', $sister_company_name);
        $uploadedFile = $_FILES['sister_company_logo']['tmp_name'];
        $ext = pathinfo($_FILES['sister_company_logo']['name'], PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        if (file_exists($uploadedFile))
        {
            if (in_array($ext, $extension))
            {
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = $scnus."_".rand()."_".$society_id;
                $dirPath = "../../img/society/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1800)
                {
                    $newWidthPercentage = 1800 * 100 / $imageWidth; //for maximum 1800 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    switch ($imageType)
                    {
                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagegif($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        default:
                            $_SESSION['msg1'] = "Invalid Logo";
                            header("Location: ../addSisterCompanies");exit;
                        break;
                    }
                    $sister_company_logo = $newFileName . "_logo." . $ext;
                }
                else
                {
                    $sister_company_logo = $newFileName . "_logo." . $ext;
                    move_uploaded_file($_FILES["sister_company_logo"]["tmp_name"], $dirPath . $sister_company_logo);
                }
            }
            else
            {
                $_SESSION['msg1'] = "Invalid Logo";
                header("location:../addSisterCompanies");exit;
            }
        }
        else
        {
            $sister_company_logo = "";
        }

        $uploadedFile = $_FILES['sister_company_stamp']['tmp_name'];
        $ext = pathinfo($_FILES['sister_company_stamp']['name'], PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        if (file_exists($uploadedFile))
        {
            if (in_array($ext, $extension))
            {
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = $scnus."_".rand()."_".$society_id;
                $dirPath = "../../img/society/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1800)
                {
                    $newWidthPercentage = 1800 * 100 / $imageWidth; //for maximum 1800 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    switch ($imageType)
                    {
                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagegif($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        default:
                            $_SESSION['msg1'] = "Invalid Stamp Photo";
                            header("Location: ../addSisterCompanies");exit;
                        break;
                    }
                    $sister_company_stamp = $newFileName . "_stamp." . $ext;
                }
                else
                {
                    $sister_company_stamp = $newFileName . "_stamp." . $ext;
                    move_uploaded_file($_FILES["sister_company_stamp"]["tmp_name"], $dirPath . $sister_company_stamp);
                }
            }
            else
            {
                $_SESSION['msg1'] = "Invalid Stamp Photo";
                header("location:../addSisterCompanies");exit;
            }
        }
        else
        {
            $sister_company_stamp = "";
        }

        $m->set_data('sister_company_name', test_input($sister_company_name));
        $m->set_data('sister_company_address', test_input($sister_company_address));
        $m->set_data('sister_company_pincode', test_input($sister_company_pincode));
        $m->set_data('sister_company_phone', test_input($sister_company_phone));
        $m->set_data('sister_company_email', test_input($sister_company_email));
        $m->set_data('sister_company_website', test_input($sister_company_website));
        $m->set_data('sister_company_gst_no', test_input($sister_company_gst_no));
        $m->set_data('sister_company_pan', test_input($sister_company_pan));
        $m->set_data('sister_company_latitude', test_input($sister_company_latitude));
        $m->set_data('sister_company_longitude', test_input($sister_company_longitude));

        $a = array(
            'society_id' => $society_id,
            'sister_company_name' => $m->get_data('sister_company_name'),
            'sister_company_address' => $m->get_data('sister_company_address'),
            'sister_company_pincode' => $m->get_data('sister_company_pincode'),
            'sister_company_phone' => $m->get_data('sister_company_phone'),
            'sister_company_email' => $m->get_data('sister_company_email'),
            'sister_company_website' => $m->get_data('sister_company_website'),
            'sister_company_stamp' => $sister_company_stamp,
            'sister_company_logo' => $sister_company_logo,
            'sister_company_gst_no' => $m->get_data('sister_company_gst_no'),
            'sister_company_pan' => $m->get_data('sister_company_pan'),
            'sister_company_latitude' => $m->get_data('sister_company_latitude'),
            'sister_company_longitude' => $m->get_data('sister_company_longitude'),
            'added_by' => $_COOKIE['bms_admin_id'],
            'added_date' => date("Y-m-d H:i:s")
        );
        $q = $d->insert("sister_company_master",$a);
        if($q)
        {
            $_SESSION['msg'] = "Sister company added.";
            $d->insert_log("", "$society_id",$_COOKIE['bms_admin_id'],"$created_by","$sister_company_name Sister Company Added.");
            header("location:../manageSisterCompanies");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../addSisterCompanies");exit;
        }
    }
    elseif (isset($_POST['updateSisterCompany']))
    {
        $scnus = str_replace(' ', '_', $sister_company_name);
        $uploadedFile = $_FILES['sister_company_logo']['tmp_name'];
        $ext = pathinfo($_FILES['sister_company_logo']['name'], PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        if (file_exists($uploadedFile))
        {
            if (in_array($ext, $extension))
            {
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = $scnus."_".rand()."_".$society_id;
                $dirPath = "../../img/society/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1800)
                {
                    $newWidthPercentage = 1800 * 100 / $imageWidth; //for maximum 1800 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    switch ($imageType)
                    {
                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagegif($tmp, $dirPath . $newFileName . "_logo." . $ext);
                        break;

                        default:
                            $_SESSION['msg1'] = "Invalid Logo";
                            header("Location: ../manageSisterCompanies");exit;
                        break;
                    }
                    $sister_company_logo = $newFileName . "_logo." . $ext;
                }
                else
                {
                    $sister_company_logo = $newFileName . "_logo." . $ext;
                    move_uploaded_file($_FILES["sister_company_logo"]["tmp_name"], $dirPath . $sister_company_logo);
                }
                unlink("../../img/society/".$sister_company_logo_old);
            }
            else
            {
                $_SESSION['msg1'] = "Invalid Logo";
                header("location:../manageSisterCompanies");exit;
            }
        }
        else
        {
            $sister_company_logo = $sister_company_logo_old;
        }

        $uploadedFile = $_FILES['sister_company_stamp']['tmp_name'];
        $ext = pathinfo($_FILES['sister_company_stamp']['name'], PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        if (file_exists($uploadedFile))
        {
            if (in_array($ext, $extension))
            {
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = $scnus."_".rand()."_".$society_id;
                $dirPath = "../../img/society/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1800)
                {
                    $newWidthPercentage = 1800 * 100 / $imageWidth; //for maximum 1800 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                    switch ($imageType)
                    {
                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagepng($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagejpeg($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile);
                            $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                            imagegif($tmp, $dirPath . $newFileName . "_stamp." . $ext);
                        break;

                        default:
                            $_SESSION['msg1'] = "Invalid Stamp Photo";
                            header("Location: ../manageSisterCompanies");exit;
                        break;
                    }
                    $sister_company_stamp = $newFileName . "_stamp." . $ext;
                }
                else
                {
                    $sister_company_stamp = $newFileName . "_stamp." . $ext;
                    move_uploaded_file($_FILES["sister_company_stamp"]["tmp_name"], $dirPath . $sister_company_stamp);
                }
                unlink("../../img/society/".$sister_company_stamp_old);
            }
            else
            {
                $_SESSION['msg1'] = "Invalid Stamp Photo";
                header("location:../manageSisterCompanies");exit;
            }
        }
        else
        {
            $sister_company_stamp = $sister_company_stamp_old;
        }

        $m->set_data('sister_company_name', test_input($sister_company_name));
        $m->set_data('sister_company_address', test_input($sister_company_address));
        $m->set_data('sister_company_pincode', test_input($sister_company_pincode));
        $m->set_data('sister_company_phone', test_input($sister_company_phone));
        $m->set_data('sister_company_email', test_input($sister_company_email));
        $m->set_data('sister_company_website', test_input($sister_company_website));
        $m->set_data('sister_company_gst_no', test_input($sister_company_gst_no));
        $m->set_data('sister_company_pan', test_input($sister_company_pan));
        $m->set_data('sister_company_latitude', test_input($sister_company_latitude));
        $m->set_data('sister_company_longitude', test_input($sister_company_longitude));

        $a = array(
            'society_id' => $society_id,
            'sister_company_name' => $m->get_data('sister_company_name'),
            'sister_company_address' => $m->get_data('sister_company_address'),
            'sister_company_pincode' => $m->get_data('sister_company_pincode'),
            'sister_company_phone' => $m->get_data('sister_company_phone'),
            'sister_company_email' => $m->get_data('sister_company_email'),
            'sister_company_website' => $m->get_data('sister_company_website'),
            'sister_company_stamp' => $sister_company_stamp,
            'sister_company_logo' => $sister_company_logo,
            'sister_company_gst_no' => $m->get_data('sister_company_gst_no'),
            'sister_company_pan' => $m->get_data('sister_company_pan'),
            'sister_company_latitude' => $m->get_data('sister_company_latitude'),
            'sister_company_longitude' => $m->get_data('sister_company_longitude'),
            'modified_by' => $_COOKIE['bms_admin_id'],
            'modified_date' => date("Y-m-d H:i:s")
        );
        $q = $d->update("sister_company_master",$a,"sister_company_id = '$sister_company_id'");
        if($q)
        {
            $_SESSION['msg'] = "Sister company details updated.";
            $d->insert_log("","$society_id",$_COOKIE['bms_admin_id'],"$created_by","$sister_company_name Sister Company Details Updated.");
            header("location:../manageSisterCompanies");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../manageSisterCompanies");exit;
        }
    }
    elseif (isset($_POST['getSisterComInfo']))
    {
        $q = $d->selectRow("sister_company_name,sister_company_address,sister_company_gst_no,sister_company_website,sister_company_pincode,sister_company_pan,sister_company_latitude,sister_company_longitude","sister_company_master","sister_company_id = '$sister_company_id'");
        $data = $q->fetch_assoc();
        echo json_encode($data);exit;
    }
}
else
{
    header('location:../login');
}
?>