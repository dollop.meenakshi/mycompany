<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addAttributes'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('dimensional_id', $dimensional_id);
        $m->set_data('attribute_name', $attribute_name);
        $m->set_data('attribute_type', $attribute_type);
        $m->set_data('attribute_added_by', $_COOKIE['bms_admin_id']);
        $m->set_data('attribute_created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'dimensional_id' => $m->get_data('dimensional_id'),
            'attribute_name' => $m->get_data('attribute_name'),
            'attribute_type' => $m->get_data('attribute_type'),
            'attribute_added_by' => $m->get_data('attribute_added_by'),
            'attribute_created_date' => $m->get_data('attribute_created_date'),
        );

        if (isset($attribute_id) && $attribute_id   > 0) {
            $q = $d->update("attribute_master", $a1, "attribute_id   ='$attribute_id'");
            $_SESSION['msg'] = "Attributes Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attributes Updated Successfully");
        } else {
            $q = $d->insert("attribute_master", $a1);
            $_SESSION['msg'] = "Attributes Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attributes Added Successfully");

        }
        if ($q == true) {
            header("Location: ../attributes");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../attributes");
        }

    }

}
