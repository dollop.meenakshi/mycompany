<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));
$bms_admin_id = $_COOKIE['bms_admin_id'];

$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
 //IS_605
	if(isset($_POST['addTask'])){
		
		mysqli_autocommit($con,FALSE);

		$m->set_data('society_id',$society_id);
		$m->set_data('task_name',$task_name);
		$m->set_data('task_note',$task_note);
		$m->set_data('task_due_date',$task_due_date);
		$m->set_data('task_add_by',$_COOKIE['bms_admin_id']);
		$m->set_data('task_assign_to',$task_assign_to);
		$m->set_data('task_created_date',date("Y-m-d H:i:s"));
		
		$a1 = array(
		'society_id'=>$m->get_data('society_id'),
		'task_name'=>$m->get_data('task_name'), 
		'task_note'=>$m->get_data('task_note'),  
		'task_due_date'=>$m->get_data('task_due_date'),  
		'task_add_by_type'=>1,  
		'task_add_by'=>$m->get_data('task_add_by'),  
		'task_assign_to'=>$m->get_data('task_assign_to'),  
		'task_created_date'=>$m->get_data('task_created_date'),  
		);

		$file = $_FILES['task_attachment']['tmp_name'];
		if(file_exists($file)) {
		$extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
		$extId = pathinfo($_FILES['task_attachment']['name'], PATHINFO_EXTENSION);

		$errors     = array();
		$maxsize    = 12097152;
			
			if(($_FILES['task_attachment']['size'] >= $maxsize) || ($_FILES["task_attachment"]["size"] == 0)) {
				$_SESSION['msg1']="Attachment too large. Must be less than 12 MB.";
				header("location:../circularAdd");
				exit();
			}
			if(!in_array($extId, $extensionResume) && (!empty($_FILES["task_attachment"]["type"]))) {
				$_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
				header("location:../circularAdd");
				exit();
			}
		if(count($errors) === 0) {
			$image_Arr = $_FILES['task_attachment'];   
			$temp = explode(".", $_FILES["task_attachment"]["name"]);
			$task_attachment = 'Task_'.round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["task_attachment"]["tmp_name"], "../../img/task_attachment/".$task_attachment);
			$task_attachment_url = $base_url.'img/task_attachment/'.$task_attachment;
		} 
		$m->set_data('task_attachment',$task_attachment);
		$a1['task_attachment'] = $m->get_data('task_attachment');
		} 
		
		$q=$d->insert("task_master",$a1);
		$task_id = $con->insert_id;

		if($task_id){
			$a2 = array(
				'society_id'=>$society_id,
				'task_id'=>$task_id, 
				'user_id'=>$task_assign_to,  
				'task_assign_date'=>date("Y-m-d H:i:s"), 
			);
			$q1=$d->insert("task_user_history",$a2);
		}

		if(isset($no_of_task_step) && $no_of_task_step>0){
			for ($i=1; $i <=$no_of_task_step ; $i++) {

				$m->set_data('society_id',$society_id);
				$m->set_data('task_id',$task_id);
				$m->set_data('task_step',$_POST['task_step'][$i]);
				$m->set_data('task_step_note',$_POST['task_step_note'][$i]);
				$m->set_data('task_step_due_date',$_POST['task_step_due_date'][$i]);
				$m->set_data('task_step_created_date',date("Y-m-d H:i:s"));
				
				$a3 =array(
					'society_id'=> $m->get_data('society_id'),
					'task_id'=> $m->get_data('task_id'),
					'task_step'=> $m->get_data('task_step'),
					'task_step_note'=> $m->get_data('task_step_note'),
					'task_step_due_date'=> $m->get_data('task_step_due_date'),
					'task_step_created_date'=> $m->get_data('task_step_created_date'),
				);
				$q2=$d->insert("task_step_master",$a3);
			}
		}
		
		if(($q && $q1) || $q2) {
			$user = $d->selectRow('user_token,device',"users_master", "user_id='$task_assign_to'");
			$userData = mysqli_fetch_assoc($user); 
			$title = "New Task Assign";
			$description = "By Admin $created_by";
			$menuClick = "";
			$image = "";
			$activity = "0";
			$user_token = $userData['user_token'];
			$device = $userData['device'];
			if ($device=='android') {
				$nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			} else {
				$nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			}

			$d->insertUserNotification($society_id,$title,$description,"","","user_id = '$task_assign_to'");

			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Added Successfully");
			//$con -> commit();
			mysqli_commit($con);
			$_SESSION['msg'] = "Task Added Successfully";
			header("Location: ../adminTask");
		} else {
			//mysqli_query("ROLLBACK");
			mysqli_rollback($con);
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../adminTask");
		}
	}

	if(isset($_POST['updateTask'])){

		mysqli_autocommit($con,FALSE);

		$m->set_data('task_name',$task_name);
		$m->set_data('task_note',$task_note);
		$m->set_data('task_due_date',$task_due_date);
		
		$a1 = array(
		'task_name'=>$m->get_data('task_name'), 
		'task_note'=>$m->get_data('task_note'),  
		'task_due_date'=>$m->get_data('task_due_date'),
		);

		$file = $_FILES['task_attachment']['tmp_name'];
		if(file_exists($file)) {
			$extensionResume=array("pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
			$extId = pathinfo($_FILES['task_attachment']['name'], PATHINFO_EXTENSION);

			$errors     = array();
			$maxsize    = 12097152;
				
				if(($_FILES['task_attachment']['size'] >= $maxsize) || ($_FILES["task_attachment"]["size"] == 0)) {
					$_SESSION['msg1']="Attachment too large. Must be less than 12 MB.";
					header("location:../circularAdd");
					exit();
				}
				if(!in_array($extId, $extensionResume) && (!empty($_FILES["task_attachment"]["type"]))) {
					$_SESSION['msg1']="Invalid Attachment format, Only  JPG,PDF,PNG,Doc,ods & Csv are allowed.";
					header("location:../circularAdd");
					exit();
				}
			if(count($errors) === 0) {
				$image_Arr = $_FILES['task_attachment'];   
				$temp = explode(".", $_FILES["task_attachment"]["name"]);
				$task_attachment = 'Task_'.round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["task_attachment"]["tmp_name"], "../../img/task_attachment/".$task_attachment);
				$task_attachment_url = $base_url.'img/task_attachment/'.$task_attachment;
			} 
			$m->set_data('task_attachment',$task_attachment);
			$a1['task_attachment'] = $m->get_data('task_attachment');
		} 
		
		if(isset($task_id) && $task_id>0){
			$q=$d->update("task_master",$a1,"task_id='$task_id'");
		}

		if($q){

			$user = $d->selectRow('user_token,device',"users_master", "user_id='$task_assign_to_old'");
			$userData = mysqli_fetch_assoc($user); 
			$title = "Assign Task Update";
			$description = "By Admin $created_by";
			$menuClick = "";
			$image = "";
			$activity = "0";
			$user_token = $userData['user_token'];
			$device = $userData['device'];
			if ($device=='android') {
				$nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			} else {
				$nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			}

			$d->insertUserNotification($society_id,$title,$description,"","","user_id = '$task_assign_to_old'");
			
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Update Successfully");
			mysqli_commit($con);
			$_SESSION['msg'] = "Task Update Successfully";
			header("Location: ../adminTask");
		} else {
			mysqli_rollback($con);
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../adminTask");
		}
	}

	if (isset($_POST['addTaskStep'])) {

		$m->set_data('society_id',$society_id);
		$m->set_data('task_id',$task_id);
		$m->set_data('task_step',$task_step);
		$m->set_data('task_step_note',$task_step_note);
		$m->set_data('task_step_due_date',$task_step_due_date);
		$m->set_data('task_step_created_date',date("Y-m-d H:i:s"));
		
		$a1 =array(
			'society_id'=> $m->get_data('society_id'),
			'task_id'=> $m->get_data('task_id'),
			'task_step'=> $m->get_data('task_step'),
			'task_step_note'=> $m->get_data('task_step_note'),
			'task_step_due_date'=> $m->get_data('task_step_due_date'),
			'task_step_created_date'=> $m->get_data('task_step_created_date'),
		);
		if(isset($task_step_id) && $task_step_id){
			$q = $d->update("task_step_master", $a1, "task_step_id ='$task_step_id'");
            $_SESSION['msg'] = "Task Step Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Step Updated Successfully");
		}else{
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Step Added Successfully");
			$_SESSION['msg'] = "Task Step Added Successfully";
			$q=$d->insert("task_step_master",$a1);
		}
		

		if($q) {
			header("Location: ../taskDetail?id=$task_id");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../taskDetail?id=$task_id");
		}
	}

	if(isset($_POST['deleteTaskStep'])) {
		$q=$d->delete("task_step_master","task_step_id='$task_step_id'");
		
		if($q>0) {
		$d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Step Deleted.");
		$_SESSION['msg']="Task Step Deleted successfully.";
		header("Location: ../taskDetail?id=$task_id");
		} else {
		$_SESSION['msg1']="Something Wrong";
		header("Location: ../taskDetail?id=$task_id");
		}
	}

	if (isset($_POST['addTaskAssignUser'])) {

		mysqli_autocommit($con,FALSE);

		$taskq = $d->selectRow('*',"task_user_history", "task_id='$task_id' AND (task_pull_back_date IS NULL OR task_pull_back_date='0000-00-00 00:00:00')");
		$taskData = mysqli_fetch_assoc($taskq); 

		$m->set_data('society_id',$society_id);
		$m->set_data('task_id',$task_id);
		$m->set_data('user_id',$task_assign_to);
		$m->set_data('task_assign_date',date("Y-m-d H:i:s"));
		
		$a =array(
			'society_id'=> $m->get_data('society_id'),
			'task_id'=> $m->get_data('task_id'),
			'user_id'=> $m->get_data('user_id'),
			'task_assign_date'=> $m->get_data('task_assign_date'),
		);
		$a1 =array(
			'task_assign_to'=> $task_assign_to,
		);
	
		$q=$d->insert("task_user_history",$a);
		$q1 = $d->update("task_master", $a1, "task_id='$task_id'");

		if($taskData){
			$a2 =array(
				'task_pull_back_date'=> date("Y-m-d H:i:s"),
			);
			$q2 = $d->update("task_user_history", $a2, "task_user_history_id='$taskData[task_user_history_id]'");
		}

		if(($q && $q1) || $q2) {
			$user = $d->selectRow('user_token,device',"users_master", "user_id='$task_assign_to'");
			$userData = mysqli_fetch_assoc($user); 
			$title = "New Task Assign";
			$description = "By Admin $created_by";
			$menuClick = "";
			$image = "";
			$activity = "0";
			$user_token = $userData['user_token'];
			$device = $userData['device'];
			if ($device=='android') {
				$nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			} else {
				$nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
			}

			$d->insertUserNotification($society_id,$title,$description,"","","user_id = '$task_assign_to'");
			if($taskData){
				$user = $d->selectRow('user_token,device',"users_master", "user_id='$taskData[user_id]'");
				$userData = mysqli_fetch_assoc($user); 
				$title = "Assign Task Pull Back";
				$description = "By Admin $created_by";
				$menuClick = "";
				$image = "";
				$activity = "0";
				$user_token = $userData['user_token'];
				$device = $userData['device'];
				if ($device=='android') {
					$nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
				} else {
					$nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
				}

				$d->insertUserNotification($society_id,$title,$description,"","","user_id = '$taskData[user_id]'");
			}
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Step Added Successfully");
			mysqli_commit($con);
			$_SESSION['msg'] = "Task Step Added Successfully";
			header("Location: ../taskDetail?id=$task_id");
		} else {
			mysqli_rollback($con);
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../taskDetail?id=$task_id");
		}
	}

	if(isset($_POST['taskCompleteStatus'])){

		$a1 =array(
			'task_complete'=> $task_complete,
		);
		if($task_complete == 2){
			$a1['task_complete_date'] = date("Y-m-d H:i:s");
		}
		$q = $d->update("task_master", $a1, "task_id='$task_id'");

		if($q) {
			header("Location: ../adminTask");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../adminTask");
		}
	}

	if(isset($_POST['addTaskPriority'])){

		$m->set_data('society_id',$society_id);
		$m->set_data('task_priority_name',$task_priority_name);
		$m->set_data('task_priority_created_by',$_COOKIE['bms_admin_id']);
		$m->set_data('task_priority_created_date',date("Y-m-d H:i:s"));

		$a =array(
			'society_id'=>$m->get_data('society_id'),
			'task_priority_name'=>$m->get_data('task_priority_name'),
			'task_priority_created_by'=>$m->get_data('task_priority_created_by'),
			'task_priority_created_date'=>$m->get_data('task_priority_created_date'),
		);

		$q=$d->insert("task_priority_master",$a);

		if($q){
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Priority Added Successfully");
			$_SESSION['msg'] = "Task Priority Added Successfully";
			header("Location: ../taskPriority");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../taskPriority");
		}
	}

	if(isset($_POST['editTaskPriority'])){
		
		$m->set_data('society_id',$society_id);
		$m->set_data('task_priority_name',$task_priority_name);
		$m->set_data('task_priority_created_by',$_COOKIE['bms_admin_id']);

		$a =array(
			'society_id'=>$m->get_data('society_id'),
			'task_priority_name'=>$m->get_data('task_priority_name'),
			'task_priority_created_by'=>$m->get_data('task_priority_created_by'),
		);

		if($task_priority_id > 0){
			$q = $d->update("task_priority_master", $a, "task_priority_id='$task_priority_id'");
		}

		if($q){
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Priority Updated Successfully");
			$_SESSION['msg'] = "Task Priority Updated Successfully";
			header("Location: ../taskPriority");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../taskPriority");
		}
	}

	if(isset($_POST['addTaskAccess'])){
	
		echo "<pre>";
		//print_r($_POST);die;
		$uq = $d->selectRow('block_id,floor_id',"users_master","user_id='$user_id'");
        $uData = mysqli_fetch_array($uq);

		if($task_access_type == "Branch Wise" && $_POST['task_access_ids'][0]==0){
			$bq = $d->selectRow('block_id',"block_master","society_id='$society_id' $blockAppendQueryOnly");
			$blockArr = array();
			while($bData = mysqli_fetch_array($bq)){
				array_push($blockArr, $bData['block_id']);
			}
			$task_access_ids = implode(',', $blockArr);
		}
		else if($task_access_type == "Department Wise" && $_POST['task_access_ids'][0]==0){
			$task_access_type = "Branch Wise";
			$task_access_ids = implode(',', $_POST['blockId']);
		}
		else if($task_access_type == "Employee Wise" && $_POST['task_access_ids'][0]==0){
			$task_access_type = "Department Wise";
			$task_access_ids = implode(',', $_POST['floorId']);
		}else{
			$task_access_ids = implode(',', $_POST['task_access_ids']);
		}

		$m->set_data('society_id',$society_id);
		$m->set_data('user_id',$user_id);
		$m->set_data('task_access_type',$task_access_type);
		$m->set_data('task_access_ids',$task_access_ids);

		$a =array(
			'society_id'=>$m->get_data('society_id'),
			'user_id'=>$m->get_data('user_id'),
			'task_access_type'=>$m->get_data('task_access_type'),
			'task_access_ids'=>$m->get_data('task_access_ids'),
		);
		//print_r($a);die;
		if($task_access_id !='' && $task_access_id>0){
			$q = $d->update("task_access_master", $a, "task_access_id='$task_access_id'");
		}else{
			$q = $d->insert("task_access_master",$a);
		}

		if($q){
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Task Priority Updated Successfully");
			$_SESSION['msg'] = "Task Priority Updated Successfully";
			header("Location: ../taskAccess?bId=$uData[block_id]&dId=$uData[floor_id]&uId=$user_id");
		} else {
			$_SESSION['msg1']="Something Wrong";
			header("Location: ../taskAccess?bId=$uData[block_id]&dId=$uData[floor_id]&uId=$user_id");
		}
	}
}
?>