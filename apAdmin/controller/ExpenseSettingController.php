<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    //add expense department wise
    if (isset($_POST['addExpenseAmount']))
    {
        $m->set_data('max_expense_amount', $max_expense_amount);
        $a1 = array(
            'max_expense_amount' => $m->get_data('max_expense_amount') ,
        );
        if (isset($expense_floor_id) && $expense_floor_id > 0)
        {
            $q = $d->update("floors_master", $a1, "floor_id ='$expense_floor_id'");
            $_SESSION['msg'] = "Expense Amount Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expense Amount Added Successfully");
            if ($q == true)
            {
                header("Location: ../expenseSetting");
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../expenseSetting");
            }
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../expenseSetting");
        }
    }
    //pay expense with payment mode and reference no
    elseif (isset($_POST['payExpenseAmount']))
    {
        mysqli_autocommit($con, false);
        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('amount', $amount);
        $m->set_data('expense_payment_mode', $expense_payment_mode);
        $m->set_data('reference_no', $reference_no);
        $m->set_data('remark', $remark);
        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $user_expense_ids = explode(",", $user_expense_id);
        $a1 = array(
            'user_id' => $m->get_data('user_id') ,
            'expense_id' => implode(",", $user_expense_ids) ,
            'society_id' => $m->get_data('society_id') ,
            'amount' => $m->get_data('amount') ,
            'expense_payment_mode' => $m->get_data('expense_payment_mode') ,
            'reference_no' => $m->get_data('reference_no') ,
            'remark' => $m->get_data('remark') ,
            'created_at' => $m->get_data('created_at') ,
        );
        $user_expense_id = explode(",", $user_expense_id);
        if (isset($user_id) && $user_id > 0)
        {
            $a1['paid_by_type'] = 0;
            $a1['paid_by'] = $_COOKIE['bms_admin_id'];
            $q = $d->insert("user_expense_history", $a1);
            $transaction_id = $con->insert_id;
            foreach ($user_expense_id as $row)
            {
                $ueq = $d->selectRow('user_id,expense_title,amount,date', "user_expenses", "user_expense_id='$row'");
                $data1 = mysqli_fetch_assoc($ueq);
                $a2 = array(
                    'expense_paid_status' => 1,
                    'expense_paid_by_type' => 1,
                    'transaction_id' => $transaction_id,
                    'expense_paid_by_id' => $_COOKIE['bms_admin_id'],
                );
                $q1 = $d->update("user_expenses", $a2, "user_expense_id ='$row'");
                
            }

            if ($q && $q1)
            {

                $uq = $d->selectRow('user_token,device', "users_master", "user_id='$user_id'");
                $data2 = mysqli_fetch_assoc($uq);
                $title = "Your expenses paid";
                $description = "By $created_by, Paid Amount is : $amount";
                $menuClick = "expense_page";
                $image = "";
                $activity = "0";
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device == 'android')
                {
                    $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
                }
                else
                {
                    $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
                }
                $d->insertUserNotificationWithJsonData($society_id, $title, $description, "expense_page", "my_expense.png", "$activity", "users_master.user_id = '$user_id'");


                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expense Paid Detail Added Successfully");
                mysqli_commit($con);
                $_SESSION['msg'] = "Expense Paid Detail Added Successfully";
                header("Location: ../unpaidEmployeeExpenses?bId=$redirect_b_id&dId=$redirect_f_id&ueMonth=$redirect_month&ueYear=$redirect_year");
            }
            else
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../unpaidEmployeeExpenses?bId=$redirect_b_id&dId=$redirect_f_id&ueMonth=$redirect_month&ueYear=$redirect_year");
            }
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../unpaidEmployeeExpenses?bId=$redirect_b_id&dId=$redirect_f_id&ueMonth=$redirect_month&ueYear=$redirect_year");
        }
    }
    elseif(isset($_POST['approveExpenses']) && $_POST['approveExpenses'] == "approveExpenses")
    {
        for($i = 0;$i < count($user_expense_ids);$i++)
        {
            $uei = $user_expense_ids[$i];
            $a1 = array(
                'expense_status' => 1,
                'expense_approved_by_id' => $_COOKIE['bms_admin_id'],
                'expense_approved_by_type'=> 1
            );
            $q = $d->update('user_expenses',$a1,"user_expense_id = '$uei'");
            if ($q)
            {
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Expenses Approved");
                $q1 = $d->selectRow('ue.user_id,ue.expense_title,ue.date,ue.amount,um.user_token,um.device',"user_expenses AS ue JOIN users_master AS um ON um.user_id = ue.user_id","user_expense_id = '$uei'");
                $data1 = mysqli_fetch_assoc($q1);
                // $q2 = $d->selectRow('user_token,device',"users_master","user_id = '$data1[user_id]'");
                // $data2 = mysqli_fetch_assoc($q2);
                $title = "Your expense request is approved";
                $description ="By Admin, ".$created_by."\nExpense : ".$data1['expense_title']."\nAmount : ".$data1['amount']."\nFor Date : ".date('d F Y',strtotime($data1['date']));
                $menuClick = "expense_page";
                $image = "";
                $activity[] = "0";
                $user_token = $data1['user_token'];
                $device = $data1['device'];
                if ($device == 'android')
                {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                else
                {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                $activity = "0";
                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png","$activity","users_master.user_id = '$data1[user_id]'");
            }
        }
        if($q)
        {
            $_SESSION['msg'] = "Employee Expenses Approved Successfully.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
        }
    }
    elseif(isset($_POST['rejectExpenses']) && $_POST['rejectExpenses'] == "rejectExpenses")
    {
        for($i = 0;$i < count($user_expense_ids);$i++)
        {
            $uei = $user_expense_ids[$i];
            $a1 = array(
                'expense_status' => 2,
                'expense_reject_by_id'=> $_COOKIE['bms_admin_id'],
                'expense_reject_by_type'=> 1,
                'expense_reject_reason' => $expense_reject_reason_new
            );
            $q = $d->update('user_expenses', $a1, "user_expense_id = '$uei'");
            if ($q > 0)
            {
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Expenses Declined");
                $_SESSION['msg'] = "Employee Expenses Declined Successfully.";
                $q1 = $d->selectRow('ue.user_id,ue.expense_title,ue.date,ue.amount,um.user_token,um.device',"user_expenses AS ue JOIN users_master AS um ON um.user_id = ue.user_id","user_expense_id = '$uei'");
                $data1 = mysqli_fetch_assoc($q1);
                $title = "Your expense request is Declined";
                $description ="By Admin, ".$created_by."\nExpense : ".$data1['expense_title']."\nAmount : ".$data1['amount']."\nFor Date : ".date('d F Y',strtotime($data1['date']));
                $menuClick = "expense_page";
                $image = "";
                $activity[] = "0";
                $user_token = $data1['user_token'];
                $user_id = $data1['user_id'];
                $device = $data1['device'];
                if ($device == 'android')
                {
                    $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                else
                {
                    $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                }
                $activity = "0";
                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png","$activity","users_master.user_id = '$data1[user_id]'");
                header("Location: ../employeeExpenses?bId=$bId&dId=$dId&uId=$user_id&ueMonth=$ueMonth&ueYear=$ueYear");
            }
            else
            {
              $_SESSION['msg1'] = "Something Wrong!";
              header("Location: ../employeeExpenses?bId=$bId&dId=$dId&uId=$user_id&ueMonth=$ueMonth&ueYear=$ueYear");
            }
        }
    }
    elseif(isset($_POST['addExpenseCategory']) && $_POST['addExpenseCategory'] == "addExpenseCategory")
    {
        $m->set_data('expense_category_name', $expense_category_name);
        $m->set_data('expense_category_type', $expense_category_type);
        $m->set_data('expense_category_unit_name', $expense_category_unit_name);
        $m->set_data('expense_category_unit_price', $expense_category_unit_price);
        $a1 = array(
            'society_id' => $society_id,
            'expense_category_name' => $m->get_data('expense_category_name'),
            'expense_category_type' => $m->get_data('expense_category_type'),
            'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
            'expense_category_unit_price' => $m->get_data('expense_category_unit_price'),
            'added_by' => $_COOKIE['bms_admin_id'],
            'added_date' => date("Y-m-d H:i:s")
        );
        $q = $d->insert("expense_category_master",$a1);
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$expense_category_name Expense Category Added");
            $_SESSION['msg'] = "Expense Category Successfully Added.";
            header("Location: ../expenseCategory");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../expenseCategory");exit;
        }
    }
    elseif(isset($_POST['getExpenseInfo']) && $_POST['getExpenseInfo'] == "getExpenseInfo")
    {
        $q = $d->select("expense_category_master","expense_category_id = '$expense_category_id'");
        $data = $q->fetch_assoc();
        echo json_encode($data);exit;
    }
    elseif(isset($_POST['getCategoryName']) && $_POST['getCategoryName'] == "getCategoryName")
    {
        if(isset($category_id) && $category_id != "" && !empty($category_id))
        {
            $q = $d->selectRow("expense_category_name","expense_category_master","expense_category_name = '$edit_expense_category_name' AND expense_category_id != '$category_id'");
        }
        else
        {
            $q = $d->selectRow("expense_category_name","expense_category_master","expense_category_name = '$expense_category_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";
        }
        else
        {
            echo "true";
        }
    }
    elseif(isset($_POST['updateExpenseCategory']) && $_POST['updateExpenseCategory'] == "updateExpenseCategory")
    {
        $m->set_data('expense_category_name', $edit_expense_category_name);
        $m->set_data('expense_category_type', $edit_expense_category_type);
        $m->set_data('expense_category_unit_name', $edit_expense_category_unit_name);
        $m->set_data('expense_category_unit_price', $edit_expense_category_unit_price);
        $a1 = array(
            'society_id' => $society_id,
            'expense_category_name' => $m->get_data('expense_category_name'),
            'expense_category_type' => $m->get_data('expense_category_type'),
            'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
            'expense_category_unit_price' => $m->get_data('expense_category_unit_price'),
            'modified_by' => $_COOKIE['bms_admin_id'],
            'modified_date' => date("Y-m-d H:i:s")
        );
        $q = $d->update("expense_category_master",$a1,"expense_category_id = '$edit_expense_category_id'");
        if($q)
        {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$edit_expense_category_name Expense Category Updated");
            $_SESSION['msg'] = "Expense Category Updated Added.";
            header("Location: ../expenseCategory");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../expenseCategory");exit;
        }
    }
    elseif(isset($_POST['getCategoryDetails']) && $_POST['getCategoryDetails'] == "getCategoryDetails")
    {
        $q = $d->select("expense_category_master","expense_category_id = '$id'");
        $data = $q->fetch_assoc();
        echo json_encode($data);exit;
    }
    elseif(isset($_POST['getExpenseCategoryList']) && $_POST['getExpenseCategoryList'] == "getExpenseCategoryList")
    {
        $n = [];
        $q = $d->selectRow("expense_category_id,expense_category_name","expense_category_master");
        while($data = $q->fetch_assoc())
        {
            $n[] = $data;
        }
        echo json_encode($n);exit;
    }
    elseif(isset($_POST['addExpense']) && $_POST['addExpense'] == "addExpense")
    {
        ob_start();
        mysqli_autocommit($con, false);
        $years = [];
        foreach($expense_title AS $key => $value)
        {
            $years[$key] = date("Y",strtotime($date[$key]));
            $m->set_data('society_id', $society_id);
            $m->set_data('floor_id', $floor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('expense_category_id', $expense_category_id[$key]);
            $m->set_data('expense_category_type', $expense_category_type[$key]);
            if($expense_category_type[$key] == 1)
            {
                $m->set_data('expense_category_unit_name', $expense_category_unit_name[$key]);
                $m->set_data('expense_category_unit_price', $expense_category_unit_price[$key]);
                $amount_unit = $unit[$key] * $expense_category_unit_price[$key];
                $m->set_data('amount', $amount_unit);
            }
            else
            {
                $m->set_data('expense_category_unit_name', '');
                $m->set_data('expense_category_unit_price', '');
                $m->set_data('amount', $amount[$key]);
            }
            $m->set_data('expense_title', $expense_title[$key]);
            $m->set_data('description', $description[$key]);
            $m->set_data('date', $date[$key]);
            if(isset($unit[$key]))
            {
                $m->set_data('unit', $unit[$key]);
            }
            else
            {
                $m->set_data('unit', 0);
            }
            if (isset($_FILES["expense_document"]["tmp_name"][$key]) && !empty($_FILES["expense_document"]["tmp_name"][$key]))
            {
                $file11 = $_FILES["expense_document"]["tmp_name"][$key];
                $extId = pathinfo($_FILES['expense_document']['name'][$key], PATHINFO_EXTENSION);
                $extAllow = array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");
                $maxsize = 2097152;
                if(($_FILES['expense_document']['size'][$key] >= $maxsize) || ($_FILES["expense_document"]["size"][$key] == 0))
                {
                    mysqli_rollback($con);
                    $_SESSION['msg1'] = "Document too large. Must be less than 2 MB.";
                    header("location:../addExpenses");
                    exit();
                }
                if(in_array($extId,$extAllow))
                {
                    $temp = explode(".", $_FILES["expense_document"]["name"][$key]);
                    $expense_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["expense_document"]["tmp_name"][$key], "../../img/expense_document/" . $expense_document);
                }
                else
                {
                    mysqli_rollback($con);
                    $_SESSION['msg1'] = "Invalid document type!";
                    header("Location: ../addExpenses");exit;
                }
                $m->set_data('expense_document', $expense_document);
            }
            else
            {
                $m->set_data('expense_document', '');
            }
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'floor_id' => $m->get_data('floor_id'),
                'user_id' => $m->get_data('user_id'),
                'expense_category_id' => $m->get_data('expense_category_id'),
                'expense_category_type' => $m->get_data('expense_category_type'),
                'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
                'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
                'expense_category_unit_price' => $m->get_data('expense_category_unit_price'),
                'expense_title' => $m->get_data('expense_title'),
                'description' => $m->get_data('description'),
                'date' => $m->get_data('date'),
                'unit' => $m->get_data('unit'),
                'amount' => $m->get_data('amount'),
                'expense_status' => 1,
                'expense_document' => $m->get_data('expense_document'),
                'expense_approved_by_type' => 1,
                'expense_approved_by_id' => $_COOKIE['bms_admin_id'],
                'created_at' => date("Y-m-d H:i:s")
            );
            $q = $d->insert("user_expenses",$a1);
            if($q)
            {
                $d->insert_log($user_id, "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expense Added Successfully");
                $title = "New Expense Added";
                $description_noti = "Expense by, ".$created_by;
                $expense_access = $d->getAccessExpense();
                $access_type = $expense_access;
                include '../../residentApiNew/check_access_data_user.php';
                if(count($userIDArray) > 0)
                {
                    $fcmArrayPA = $d->get_android_fcm("users_master","delete_status = 0 AND user_token != '' AND society_id = '$society_id' AND device = 'android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA = $d->get_android_fcm("users_master","delete_status = 0 AND user_token != '' AND society_id = '$society_id' AND device = 'ios' AND user_id IN ('$userFcmIds')");
                    $tabPosition = [];
                    $tabPosition[] = "1";
                    $nResident->noti("expense_page","",$society_id,$fcmArrayPA,$title,$description_noti,$tabPosition);
                    $nResident->noti_ios("expense_page","",$society_id,$fcmArrayIosPA,$title,$description_noti,$tabPosition);
                    $tabPosition = "1";
                    $d->insertUserNotificationWithJsonData($society_id,$title,$description_noti,"expense_page","my_expense.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                    $block_id = $d->getBlockid($user_id);
                    $fcmArray = $d->selectAdminBlockwise("15",$block_id,"android");
                    $fcmArrayIos = $d->selectAdminBlockwise("15",$block_id,"ios");
                    $nAdmin->noti_new($society_id,"",$fcmArray,$title, $description_noti,"employeeExpenses");
                    $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,$title, $description_noti,"employeeExpenses");
                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>$title,
                      'notification_description'=>$description_noti,
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"employeeExpenses",
                      'admin_click_action '=>"employeeExpenses",
                      'notification_logo'=>'my_expense.png'
                    );
                    $d->insert("admin_notification",$notiAry);
                }
            }
        }
        $maxYear = max($years);
        if($q)
        {
            mysqli_commit($con);
            $_SESSION['msg'] = "Expense added successfully";
            header("Location: ../unpaidEmployeeExpenses?bId=$block_id&dId=$floor_id&uId=$user_id&ueMonth=0&ueYear=$maxYear&csrf=$csrf&getReport='Get Data'");exit;
        }
        else
        {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something went wrong!";
            header("Location: ../addExpenses");exit;
        }
    }
    elseif(isset($_POST['updateExpense']) && $_POST['updateExpense'] == "updateExpense")
    {
        ob_start();
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('expense_category_id', $expense_category_id[0]);
        $m->set_data('expense_category_type', $expense_category_type);
        if($expense_category_type == 1)
        {
            $m->set_data('expense_category_unit_name', $expense_category_unit_name[0]);
            $m->set_data('expense_category_unit_price', $expense_category_unit_price[0]);
            $amount_unit = $unit[0] * $expense_category_unit_price[0];
            $m->set_data('amount', $amount_unit);
        }
        else
        {
            $m->set_data('expense_category_unit_name', '');
            $m->set_data('expense_category_unit_price', '');
            $m->set_data('amount', $amount[0]);
        }
        $m->set_data('expense_title', $expense_title[0]);
        $m->set_data('description', $description[0]);
        $m->set_data('date', $date[0]);
        if(isset($unit))
        {
            $m->set_data('unit', $unit[0]);
        }
        else
        {
            $m->set_data('unit', 0);
        }
        if (isset($_FILES["expense_document"]["tmp_name"][0]) && !empty($_FILES["expense_document"]["tmp_name"][0]))
        {
            $file11 = $_FILES["expense_document"]["tmp_name"][0];
            $extId = pathinfo($_FILES['expense_document']['name'][0], PATHINFO_EXTENSION);
            $extAllow = array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");
            $maxsize = 2097152;
            if(($_FILES['expense_document']['size'][0] >= $maxsize) || ($_FILES["expense_document"]["size"][0] == 0))
            {
                $_SESSION['msg1'] = "Document too large. Must be less than 2 MB.";
                header("Location: ../$f?bId=$bId&dId=$dId&uId=$uId&ueMonth=$ueMonth&ueYear=$ueYear&csrf=$csrf&getReport=$getReport");exit;
                exit();
            }
            if(in_array($extId,$extAllow))
            {
                unlink("../../img/expense_document/" . $old_document);
                $temp = explode(".", $_FILES["expense_document"]["name"][0]);
                $expense_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["expense_document"]["tmp_name"][0], "../../img/expense_document/" . $expense_document);
            }
            else
            {
                $_SESSION['msg1'] = "Invalid document type!";
                header("Location: ../$f?bId=$bId&dId=$dId&uId=$uId&ueMonth=$ueMonth&ueYear=$ueYear&csrf=$csrf&getReport=$getReport");exit;
            }
            $m->set_data('expense_document', $expense_document);
        }
        else
        {
            $m->set_data('expense_document', $old_document);
        }
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'floor_id' => $m->get_data('floor_id'),
            'user_id' => $m->get_data('user_id'),
            'expense_category_id' => $m->get_data('expense_category_id'),
            'expense_category_type' => $m->get_data('expense_category_type'),
            'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
            'expense_category_unit_name' => $m->get_data('expense_category_unit_name'),
            'expense_category_unit_price' => $m->get_data('expense_category_unit_price'),
            'expense_title' => $m->get_data('expense_title'),
            'description' => $m->get_data('description'),
            'date' => $m->get_data('date'),
            'unit' => $m->get_data('unit'),
            'amount' => $m->get_data('amount'),
            'expense_document' => $m->get_data('expense_document'),
            'modify_date' => date("Y-m-d H:i:s")
        );
        $q = $d->update("user_expenses",$a1,"user_expense_id = '$user_expense_id'");
        if($q)
        {
            $d->insert_log($user_id, "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Expense Updated Successfully");
            $title = "Expense Updated";
            $description_noti = "Expense by, ".$created_by;
            $expense_access = $d->getAccessExpense();
            $access_type = $expense_access;
            include '../../residentApiNew/check_access_data_user.php';
            if(count($userIDArray) > 0)
            {
                $fcmArrayPA = $d->get_android_fcm("users_master","delete_status = 0 AND user_token != '' AND society_id = '$society_id' AND device = 'android' AND user_id IN ('$userFcmIds')");
                $fcmArrayIosPA = $d->get_android_fcm("users_master","delete_status = 0 AND user_token != '' AND society_id = '$society_id' AND device = 'ios' AND user_id IN ('$userFcmIds')");
                $tabPosition = [];
                $tabPosition[] = "1";
                $nResident->noti("expense_page","",$society_id,$fcmArrayPA,$title,$description_noti,$tabPosition);
                $nResident->noti_ios("expense_page","",$society_id,$fcmArrayIosPA,$title,$description_noti,$tabPosition);
                $tabPosition = "1";
                $d->insertUserNotificationWithJsonData($society_id,$title,$description_noti,"expense_page","my_expense.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                $block_id = $d->getBlockid($user_id);
                $fcmArray = $d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos = $d->selectAdminBlockwise("15",$block_id,"ios");
                $nAdmin->noti_new($society_id,"",$fcmArray,$title, $description_noti,"employeeExpenses");
                $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,$title, $description_noti,"employeeExpenses");
                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description_noti,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"employeeExpenses",
                  'admin_click_action '=>"employeeExpenses",
                  'notification_logo'=>'my_expense.png'
                );
                $d->insert("admin_notification",$notiAry);
            }
            $_SESSION['msg'] = "Expense Updated successfully";
            header("Location: ../$f?bId=$bId&dId=$dId&uId=$uId&ueMonth=$ueMonth&ueYear=$ueYear&csrf=$csrf&getReport=$getReport");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went wrong!";
            header("Location: ../$f?bId=$bId&dId=$dId&uId=$uId&ueMonth=$ueMonth&ueYear=$ueYear&csrf=$csrf&getReport=$getReport");exit;
        }
    }

    elseif(isset($_POST['giveExpense']) && $_POST['giveExpense'] == "giveExpense")
    {

        ob_start();
        mysqli_autocommit($con, false);

        $m->set_data('society_id', $society_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('expense_title', $expense_title);
        $m->set_data('date', $date);
        $m->set_data('description', $description);
        $m->set_data('amount', $amount);

        if (isset($_FILES["expense_document"]["tmp_name"]) && !empty($_FILES["expense_document"]["tmp_name"]))
        {
            $file11 = $_FILES["expense_document"]["tmp_name"];
            $extId = pathinfo($_FILES['expense_document']['name'], PATHINFO_EXTENSION);
            $extAllow = array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");
            $maxsize = 2097152;
            if(($_FILES['expense_document']['size'] >= $maxsize) || ($_FILES["expense_document"]["size"] == 0))
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Document too large. Must be less than 2 MB.";
                header("location:../addAdvanceExpense");
                exit();
            }
            if(in_array($extId,$extAllow))
            {
                $temp = explode(".", $_FILES["expense_document"]["name"]);
                $expense_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["expense_document"]["tmp_name"], "../../img/expense_document/" . $expense_document);
            }
            else
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Invalid document type!";
                header("Location: ../addAdvanceExpense");exit;
            }
            $m->set_data('expense_document', $expense_document);
        }
        else
        {
            $m->set_data('expense_document', '');
        }
        
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            //'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'user_id' => $m->get_data('user_id'),
            'expense_title' => $m->get_data('expense_title'),
            'date' => $m->get_data('date'),
            'amount' => '-'.$m->get_data('amount'),
            'expense_type' => '1',
            'expense_status' => '1',
            'expense_approved_by_type' => '1',
            'expense_approved_by_id' => $_COOKIE['bms_admin_id'],
            'description' => $m->get_data('description'),
            'expense_document' => $m->get_data('expense_document'),
            'modify_date' => date("Y-m-d H:i:s")
        );
        $q = $d->insert("user_expenses",$a1);
        if($q)
        {
            $d->insert_log($user_id, "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Give Expense Added Successfully");
            mysqli_commit($con);
            $_SESSION['msg'] = "Give Expense added successfully";
            header("Location: ../manageAdvanceExpense");exit;        }
        else
        {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something went wrong!";
            header("Location: ../addAdvanceExpense");exit;
        }
    }

    elseif(isset($_POST['updateGiveExpense']) && $_POST['updateGiveExpense'] == "updateGiveExpense")
    {
       
        ob_start();
        mysqli_autocommit($con, false);

        $m->set_data('society_id', $society_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('expense_title', $expense_title);
        $m->set_data('date', $date);
        $m->set_data('description', $description);
        $m->set_data('amount', $amount);

        if (isset($_FILES["expense_document"]["tmp_name"]) && !empty($_FILES["expense_document"]["tmp_name"]))
        {
            $file11 = $_FILES["expense_document"]["tmp_name"];
            $extId = pathinfo($_FILES['expense_document']['name'], PATHINFO_EXTENSION);
            $extAllow = array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");
            $maxsize = 2097152;
            if(($_FILES['expense_document']['size'] >= $maxsize) || ($_FILES["expense_document"]["size"] == 0))
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Document too large. Must be less than 2 MB.";
                header("location:../manageAdvanceExpense");
                exit();
            }
            if(in_array($extId,$extAllow))
            {
                $temp = explode(".", $_FILES["expense_document"]["name"]);
                $expense_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["expense_document"]["tmp_name"], "../../img/expense_document/" . $expense_document);
            }
            else
            {
                mysqli_rollback($con);
                $_SESSION['msg1'] = "Invalid document type!";
                header("Location: ../manageAdvanceExpense");exit;
            }
            $m->set_data('expense_document', $expense_document);
        }
        else
        {
            $m->set_data('expense_document', $old_document);
        }
        
        $a1 = array(
            'floor_id' => $m->get_data('floor_id'),
            'user_id' => $m->get_data('user_id'),
            'expense_title' => $m->get_data('expense_title'),
            'date' => $m->get_data('date'),
            'amount' => '-'.$m->get_data('amount'),
            'expense_type' => '1',
            'description' => $m->get_data('description'),
            'expense_document' => $m->get_data('expense_document'),
        );
        $q = $d->update("user_expenses",$a1,"user_expense_id = '$user_expense_id'");
        if($q)
        {
            $d->insert_log($user_id, "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Give Expense Updated Successfully");
            mysqli_commit($con);
            $_SESSION['msg'] = "Give expense updated successfully";
            header("Location: ../manageAdvanceExpense");exit;        }
        else
        {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something went wrong!";
            header("Location: ../manageAdvanceExpense");exit;
        }
    }
}
