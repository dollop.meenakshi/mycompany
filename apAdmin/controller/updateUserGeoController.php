<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    if (isset($_POST['updateUser'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('user_id_old', $user_id_old);
        $m->set_data('user_geofence_range', $user_geofence_range);
        $m->set_data('user_latitude', $user_latitude);
        $m->set_data('user_longitude', $user_longitude);
        $m->set_data('is_app_attendance_on', $is_app_attendance_on);
     
        if (isset($society_id) && $society_id > 0) {
           
            if(isset($user_id) && $user_id >0)
            {
                $user_id = $user_id;
            } 
            if(isset($user_id_old) && $user_id_old >0)
            {
                $user_id =  $user_id_old;
            } 
            
            $UserAddress2 = array(
                'is_app_attendance_on'=>$m->get_data('is_app_attendance_on'),
            );
             $q = $d->update("users_master", $UserAddress2, "user_id ='".$user_id."'");
             if(isset($geo_fance_id) && $geo_fance_id>0){
                $cnt = COUNT($_POST['user_geofence_range']);
                    for ($i=0; $i <$cnt ; $i++) { 
                        $UserAddress = array(
                            'user_geofence_latitude'=>$_POST['user_latitude'][$i],
                            'user_geofence_longitude'=>$_POST['user_longitude'][$i],
                            'user_geo_address'=>test_input($_POST['location_name'][$i]),
                            'user_geofence_range'=>test_input($_POST['user_geofence_range'][$i]),
                            'work_location_name'=>test_input($_POST['work_location_name'][$i]),
                            'user_id'=>$user_id,
                        );
        
                         $q2 = $d->update("user_geofence", $UserAddress,"geo_fance_id ='".$geo_fance_id."'");
                    }
             }
             else
             {
                 if(!empty($_POST['user_geofence_range']) ){
                     $cnt = COUNT($_POST['user_geofence_range']);
                    for ($i=0; $i <$cnt ; $i++) { 
                        $UserAdd = array(
                            'user_geofence_latitude'=>$_POST['user_latitude'][$i],
                            'user_geofence_longitude'=>$_POST['user_longitude'][$i],
                            'user_geo_address'=>test_input($_POST['location_name'][$i]),
                            'user_geofence_range'=>test_input($_POST['user_geofence_range'][$i]),
                            'work_location_name'=>test_input($_POST['work_location_name'][$i]),
                            'user_id'=>$user_id,
                        );
        
                         $q2 = $d->insert("user_geofence", $UserAdd);
                    }
                 }
             }
             $_SESSION['msg'] = "User lat long range Updated Successfully";
             $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Updated Successfully");
        } 
        if ($q == true && $q2 ==true) {
            header("Location: ../companyAttendanceType");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyAttendanceType");
        }
    }

}
