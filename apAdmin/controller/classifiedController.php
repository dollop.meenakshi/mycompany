<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// print_r($_POST);
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  // add main menu
  if(isset($classified_cat_id_delete)) {

     $q=$d->delete("classified_category","classified_category_id='$classified_cat_id_delete' ");
     $q=$d->delete("classified_sub_category","classified_category_id='$classified_cat_id_delete' ");

     if($q>0) {
      echo 1;
     } else {
      echo 0;
     }
  }


  if(isset($classified_sub_id_delete)) {

     $q=$d->delete("classified_sub_category","classified_sub_category_id='$classified_sub_id_delete' ");

     if($q>0) {
      echo 1;
     } else {
      echo 0;
     }
  }



  if (isset($addCategory)) {

      $file = $_FILES['classified_category_image']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['classified_category_image']['size'] >= $maxsize) || ($_FILES["classified_category_image"]["size"] == 0)) {
                 $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
                 header("location:../buySellCategory");
                 exit();
            }
            if(!in_array($_FILES['classified_category_image']['type'], $acceptable) && (!empty($_FILES["classified_category_image"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../buySellCategory");
                 exit();
            }
            $ddd=date("ymdhis");
            $image_Arr = $_FILES['classified_category_image'];   
            $temp = explode(".", $_FILES["classified_category_image"]["name"]);
            $classified_category_image = 'classCate'.$ddd.'.' . end($temp);
            move_uploaded_file($_FILES["classified_category_image"]["tmp_name"], "../../img/classified/classified_cat/".$classified_category_image);
            $m->set_data('feed_type',1);
       }
  
      $m->set_data('classified_category_name',$classified_category_name);
      $m->set_data('classified_category_image',$classified_category_image);

      $a1 = array(
        'classified_category_name'=>$m->get_data('classified_category_name'),
        'classified_category_image'=>$m->get_data('classified_category_image'),
        'classified_category_status'=>'0',
     );

    $q=$d->insert("classified_category",$a1);

    if($q==TRUE) {
      $_SESSION['msg']="News Category Added";
      header("Location: ../buySellCategory");
    } else {
      header("Location: ../buySellCategory");
    }

  }



if (isset($addSubCategory)) {

      $file = $_FILES['classified_sub_category_image']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['classified_sub_category_image']['size'] >= $maxsize) || ($_FILES["classified_sub_category_image"]["size"] == 0)) {
                 $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
                 header("location:../buySellSubCategory");
                 exit();
            }
            if(!in_array($_FILES['classified_sub_category_image']['type'], $acceptable) && (!empty($_FILES["classified_sub_category_image"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../buySellSubCategory");
                 exit();
            }
            $ddd=date("ymdhis");
            $image_Arr = $_FILES['classified_sub_category_image'];   
            $temp = explode(".", $_FILES["classified_sub_category_image"]["name"]);
            $classified_sub_category_image = 'classCate'.$ddd.'.' . end($temp);
            move_uploaded_file($_FILES["classified_sub_category_image"]["tmp_name"], "../../img/classified/classified_cat/".$classified_sub_category_image);
            $m->set_data('feed_type',1);
       }
  
      $m->set_data('classified_category_id',$classified_category_id);
      $m->set_data('classified_sub_category_name',$classified_sub_category_name);
      $m->set_data('classified_sub_category_image',$classified_sub_category_image);

      $a1 = array(
        'classified_category_id'=>$m->get_data('classified_category_id'),
        'classified_sub_category_name'=>$m->get_data('classified_sub_category_name'),
        'classified_sub_category_image'=>$m->get_data('classified_sub_category_image'),
     );

    $q=$d->insert("classified_sub_category",$a1);

    if($q==TRUE) {
      $_SESSION['msg']="New Sub Category Added";
      header("Location: ../buySellSubCategory");
    } else {
      header("Location: ../buySellSubCategory");
    }

}


  if (isset($editCategory)) {

      $file = $_FILES['classified_category_image']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['classified_category_image']['size'] >= $maxsize) || ($_FILES["classified_category_image"]["size"] == 0)) {
                 $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
                 header("location:../buySellCategory");
                 exit();
            }
            if(!in_array($_FILES['classified_category_image']['type'], $acceptable) && (!empty($_FILES["classified_category_image"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../buySellCategory");
                 exit();
            }
            $ddd=date("ymdhis");
            $image_Arr = $_FILES['classified_category_image'];   
            $temp = explode(".", $_FILES["classified_category_image"]["name"]);
            $classified_category_image = 'classCate'.$ddd.'.' . end($temp);
            move_uploaded_file($_FILES["classified_category_image"]["tmp_name"], "../../img/classified/classified_cat/".$classified_category_image);
            $m->set_data('feed_type',1);
       } else {
        $classified_category_image=$classified_category_image_old;
       }
  
      $m->set_data('classified_category_name',$classified_category_name);
      $m->set_data('classified_category_image',$classified_category_image);

      $a1 = array(
        'classified_category_name'=>$m->get_data('classified_category_name'),
        'classified_category_image'=>$m->get_data('classified_category_image'),
     );

    $q=$d->update("classified_category",$a1,"classified_category_id='$classified_category_id'");

    if($q==TRUE) {
      $_SESSION['msg']=" Category Updated";
      header("Location: ../buySellCategory");
    } else {
      header("Location: ../buySellCategory");
    }

  }



  if (isset($editSubCategory)) {

      $file = $_FILES['classified_sub_category_image']['tmp_name'];
       if(file_exists($file)) {
     // checking if main category value was changed
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );
            if(($_FILES['classified_sub_category_image']['size'] >= $maxsize) || ($_FILES["classified_sub_category_image"]["size"] == 0)) {
                 $_SESSION['msg1']="Photo too large. File must be less than 1 MB.";
                 header("location:../buySellSubCategory");
                 exit();
            }
            if(!in_array($_FILES['classified_sub_category_image']['type'], $acceptable) && (!empty($_FILES["classified_sub_category_image"]["type"]))) {
                $_SESSION['msg1']="Invalid Photo type. Only  JPG and PNG types are accepted.";
                 header("location:../buySellSubCategory");
                 exit();
            }
            $ddd=date("ymdhis");
            $image_Arr = $_FILES['classified_sub_category_image'];   
            $temp = explode(".", $_FILES["classified_sub_category_image"]["name"]);
            $classified_sub_category_image = 'classCate'.$ddd.'.' . end($temp);
            move_uploaded_file($_FILES["classified_sub_category_image"]["tmp_name"], "../../img/classified/classified_cat/".$classified_sub_category_image);
            $m->set_data('feed_type',1);
       } else {
        $classified_sub_category_image=$classified_sub_category_image_old;
       }
  
      $m->set_data('classified_sub_category_name',$classified_sub_category_name);
      $m->set_data('classified_sub_category_image',$classified_sub_category_image);

      $a1 = array(
        'classified_sub_category_name'=>$m->get_data('classified_sub_category_name'),
        'classified_sub_category_image'=>$m->get_data('classified_sub_category_image'),
     );

    $q=$d->update("classified_sub_category",$a1,"classified_sub_category_id='$classified_sub_category_id'");

    if($q==TRUE) {
      $_SESSION['msg']="Sub Category Updated";
      header("Location: ../buySellSubCategory");
    } else {
      header("Location: ../buySellSubCategory");
    }

}


 if (isset($classified_master_id)) {
      $q=$d->delete("classified_master","classified_master_id='$classified_master_id'");
      if($q==TRUE){
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Buy/Sell ($iteamName) iteam deleted");
        
        $_SESSION['msg']="Deleted Successfully";
        header("location:../buySellItems");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../buySellItems");
      }
 }

}
else{
  header('location:../login');
 }
 ?>
