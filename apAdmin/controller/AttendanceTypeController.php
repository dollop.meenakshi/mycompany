<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    //IS_605
    if (isset($_POST['addAttendanceType']))
    {
        $m->set_data('society_id', $society_id);
        $m->set_data('attendance_type_name', $attendance_type_name);
        $m->set_data('is_multipletime_use', $is_multipletime_use);
        $m->set_data('attendance_type_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('attendance_type_created_date',date("Y-m-d H:i:s"));
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'attendance_type_name' => $m->get_data('attendance_type_name'),
            'is_multipletime_use' => $m->get_data('is_multipletime_use'),
            'attendance_type_created_by' => $m->get_data('attendance_type_created_by'),
            'attendance_type_created_date' => $m->get_data('attendance_type_created_date')
        );
        if(isset($attendance_type_id) && $attendance_type_id > 0)
        {
            $q = $d->update("attendance_type_master", $a1, "attendance_type_id ='$attendance_type_id'");
            $_SESSION['msg'] = "Attendance Type Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Updated Successfully");
        }
        else
        {
            $q = $d->insert("attendance_type_master", $a1);
            $_SESSION['msg'] = "Attendance Type Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Added Successfully");
        }
        if($q == true)
        {
            $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0'");
            $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1'");
            $title = "sync_data";
            $description = "Attendance Type Update on ".date('d-M-y h:i A');
            $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
            $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");
            header("Location: ../attendanceType");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../attendanceType");
        }
    }
    elseif(isset($_POST['getAttendanceinfo']) && $_POST['getAttendanceinfo'] == "getAttendanceinfo")
    {
        $q = $d->selectRow("multiple_punch_in_out_data,attendance_id,attendance_date_start,attendance_date_end,punch_in_time,punch_out_time,late_in, early_out,late_time_start,early_out_time,shift_type,attendance_master.shift_time_id,attendance_master.user_id,attendance_master.unit_id,user_full_name,modified_remark","attendance_master LEFT JOIN shift_timing_master AS stm ON stm.shift_time_id = attendance_master.shift_time_id JOIN users_master AS um ON um.user_id = attendance_master.user_id","attendance_id = '$attendance_id'");
        $data = $q->fetch_assoc();
        $multiple_punch_in_out_data =$data['multiple_punch_in_out_data'];
        $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

        if (count($multiplePunchDataArray)>1) {
            echo "000";
        } else {
        echo json_encode($data);exit;
        }
    }
    elseif(isset($_POST['editAttendance']) && $_POST['editAttendance'] == "editAttendance")
    {
        if ($attendance_start_date != '0000-00-00' && $attendance_start_date != 'null') {
            $day = date("w", strtotime($attendance_start_date));
        }
       
        // error_reporting(E_ALL);
        // ini_set('display_errors', '1');
        $punchInDateTime = $attendance_start_date." ".$punch_in_time.":00";
        $datePunchOUT = $attendance_end_date." ".$edit_punch_out_time.":00";
        $shiftQry = $d->selectRow("shift_day_master.*, shift_timing_master.is_multiple_punch_in, shift_timing_master.maximum_in_out, shift_timing_master.max_punch_out_time, shift_timing_master.max_shift_hour","shift_day_master,shift_timing_master","shift_day_master.shift_time_id=$shift_time_id AND shift_day_master.shift_day='$day' AND shift_timing_master.shift_time_id=shift_day_master.shift_time_id");
        $shiftData = mysqli_fetch_array($shiftQry);

        $max_punch_out_time= $attendance_end_date." ".date('H:i:s', strtotime($shiftData['max_punch_out_time']));
        $max_shift_hour= date('H:i:s', strtotime($shiftData['max_shift_hour']));
        $time=explode(":",$max_shift_hour);
        $convertedTime = date('H:i:s',strtotime(+$time[0].' hour '.+$time[1].' minutes '.+$time[2].' seconds',strtotime($punch_in_time)));

        $perdayHours = $shiftData['per_day_hour'];
        $shift_start_time = $shiftData['shift_start_time'];
        $shift_end_time = $shiftData['shift_end_time'];
        $late_time_start = $shiftData['late_time_start'];
        $early_out_time = $shiftData['early_out_time'];
        $half_day_time_start = $shiftData['half_day_time_start'];
        $halfday_before_time = $shiftData['halfday_before_time'];
        $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
        $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
        $shift_type = $shiftData['shift_type'];
        $maximum_in_out = $shiftData['maximum_in_out'];
        $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];

        $qol=$d->select("attendance_master","attendance_id = '$update_attendance_id'");
        $oldAttadanceData= mysqli_fetch_array($qol);
        $old_attendance_start_date = $oldAttadanceData['attendance_date_start'];
        $old_attendance_end_date = $oldAttadanceData['attendance_date_end'];
        $old_punch_in_time = $oldAttadanceData['punch_in_time'];
        $old_punch_out_time = $oldAttadanceData['punch_out_time'];
        $modified_data_old = $oldAttadanceData['modified_data'];
        $attendance_user_id = $oldAttadanceData['user_id'];
        $attendance_date = $attendance_end_date;
        $attendance_time = $edit_punch_out_time;
        $attendance_date_start = $old_attendance_start_date;
        $partLI = explode(':', $late_time_start);
        $min = ($partLI[0]*60) + ($partLI[1]) + ($partLI[2]/60);
        $defaultINTime = $attendance_start_date." ".$shift_start_time;
        $strMin = "+".$min." minutes";
        $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));
        $qryLeave = $d->selectRow("leave_day_type,half_day_session,leave_id,leave_type_id,paid_unpaid,leave_reason","leave_master","user_id = '$user_id' AND leave_start_date = '$attendance_start_date'");
        $hasLeave = false;
        if(mysqli_num_rows($qryLeave) > 0)
        {
            $hasLeave = true;
            $leaveData = mysqli_fetch_array($qryLeave);
            $leave_id = $leaveData['leave_id'];
            $leave_type_id = $leaveData['leave_type_id'];
            $paid_unpaid = $leaveData['paid_unpaid'];
            $leave_reason = $leaveData['leave_reason'];
            $leave_day_type = $leaveData['leave_day_type'];
            $half_day_session = $leaveData['half_day_session'];
        }
        if($punchInDateTime > $defaultTime)
        {
            $late_in = "1";
        }
        else
        {
            $late_in = "0";
        }
        if($leave_day_type == 1 && $half_day_session != null && $half_day_session == 1)
        {
            $late_in = "0";
        }
        

        $multiple_punch_in_out_data = $oldAttadanceData['multiple_punch_in_out_data'];
        $is_extra_day = $oldAttadanceData['is_extra_day'];
        $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

            $finalTotalMinute = 0;

            $dataArray = array();

            if (isset($multiplePunchDataArray) &&  $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

                for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                   

                        $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                $punch_in_time,$attendance_time);

                        $timeHr = explode(':', $totalHours.":00");
                        $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                        $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                            "punch_in_time" => $punch_in_time,
                            "punch_out_date" => $attendance_date,
                            "punch_out_time" =>$attendance_time,
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                            "punch_out_image" => "",
                            "working_hour" => $totalHours.'',
                            "working_hour_minute" => $totalMinutes.'',
                        );

                        array_push($dataArray,$oldAry);
                    
                }  

                $multPunchDataJson = json_encode($dataArray).'';
            }else{

                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                        
                            $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                    $punch_in_time,$attendance_time);

                            $timeHr = explode(':', $totalHours.":00");
                            $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                            $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                            $oldAry = array(
                                "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                                "punch_in_time" => $punch_in_time,
                                "punch_out_date" => $attendance_date,
                                "punch_out_time" =>$attendance_time,
                                "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                                "punch_out_image" => "",
                                "working_hour" => $totalHours,
                                "working_hour_minute" => $finalTotalMinute,
                            );
                            array_push($dataArray,$oldAry);
                        
                    }

                    $multPunchDataJson = json_encode($dataArray).'';
                }
            }
            
            $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);

                $leave_id = $leaveData['leave_id'];
                $leave_type_id = $leaveData['leave_type_id'];
                $paid_unpaid = $leaveData['paid_unpaid'];
                $leave_reason = $leaveData['leave_reason'];
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $datePunchOUT = $attendance_date ." ".$attendance_time;
            $datePunchIN = $attendance_date_start ." ".$punch_in_time;

            $partEO = explode(':', $early_out_time);
            $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            $defaultOutTime = $attendance_date." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

            if ($datePunchOUT >= $defaultOutTime) {
                $early_out = "0";
            }else{
                $early_out = "1";              
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            if ($is_multiple_punch_in == 0) {

                $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

                $totalHR = getTotalHours($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);


                $time = explode(':', $totalHR.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
            }else{
                $total_minutes =  $finalTotalMinute;

                $hoursPM  = floor($total_minutes/60);
                $minutesPM = $total_minutes % 60;

                $totalHR = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                $times[] = $totalHR;

                if ($hoursPM > 0 && $minutesPM) {
                    $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                }else if ($hoursPM > 0 && $minutesPM <= 0) {
                    $totalHoursNames = sprintf('%02d hr', $hoursPM);
                }else if ($hoursPM <= 0 && $minutesPM > 0) {
                    $totalHoursNames = sprintf('%02d min', $minutesPM);
                }else{
                    $totalHoursNames = "No Data";
                }
            }

            $avgWorkingDays = $total_minutes/$perDayHourMinute;

            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;

            if ($total_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
            }

            $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

            $qryBreakData = mysqli_fetch_array($qryBreak);

            $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

            if ($total_minutes > $totalBreakinutes) {
                $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }else{
                $productive_working_hours_minutes = $total_minutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }

            $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT' AND gps_accuracy<100");

            $query11Data=mysqli_fetch_array($query11);

            $total_travel_meter = "0";
            
            if ($query11Data['SUM(last_distance)'] > 0) {
                $total_travel_meter = $query11Data['SUM(last_distance)']."";
            }



       

        if ($old_punch_in_time!=$punch_in_time) {
            $modified_data1  = "Punch In Time modified from $old_punch_in_time to $punch_in_time, changed by $created_by\n";
        }

        if ($old_punch_out_time!=$edit_punch_out_time) {
            $modified_data2  = "Punch Out Time modified from $old_punch_out_time to $edit_punch_out_time, changed by $created_by\n";
        }

        // if ($modified_data_old!="" && ($modified_data1!='' || $modified_data1!='') ) {
        //     $modified_data_old = $modified_data_old."\n............\n";
        // }

        if ($modified_data_old!='' || $modified_data1!='' || $modified_data2!='') {
            // code...
            $modified_data = $modified_data_old.$modified_data1.$modified_data2;
        }

        

        $m->set_data('attendance_start_date', $attendance_start_date);
        $m->set_data('attendance_end_date', $attendance_end_date);
        $m->set_data('punch_in_time', $punch_in_time);
        $m->set_data('punch_out_time', $edit_punch_out_time);
        $m->set_data('late_in', $late_in);
        $m->set_data('early_out', $early_out);
        $m->set_data('modified_remark', $modified_remark);
        $m->set_data('modified_data', $modified_data);
        $m->set_data('total_working_hours',$totalHR);
        $m->set_data('extra_working_hours',$extra_working_hours);
        $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
        $m->set_data('avg_working_days',$avg_working_days);
        $m->set_data('productive_working_hours',$productive_working_hours);
        $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
        $m->set_data('total_working_minutes',$total_minutes);
        $m->set_data('total_travel_meter',$total_travel_meter);
        $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);

               
        $a1 = array(
            'attendance_date_start' => $m->get_data('attendance_start_date'),
            'last_punch_in_date' => $m->get_data('attendance_start_date'),
            'attendance_date_end' => $m->get_data('attendance_end_date'),
            'punch_in_time' => $m->get_data('punch_in_time'),
            'last_punch_in_time' => $m->get_data('punch_in_time'),
            'punch_out_time' => $m->get_data('punch_out_time'),
            'late_in' => $m->get_data('late_in'),
            'early_out' => $m->get_data('early_out'),
            'is_modified' => 1,
            'modified_by_type' => 1,
            'modified_id' => $_COOKIE['bms_admin_id'],
            'modified_date' => date("Y-m-d H:i:s"),
            'modified_remark' => $m->get_data('modified_remark'),
            'modified_data' => $m->get_data('modified_data'),
            'extra_working_hours'=>$m->get_data('extra_working_hours'),
            'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
            'avg_working_days'=>$m->get_data('avg_working_days'),
            'productive_working_hours'=>$m->get_data('productive_working_hours'),                
            'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),                
            'total_working_hours'=>$m->get_data('total_working_hours'),                
            'total_working_minutes'=>$m->get_data('total_working_minutes'),                    
            'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),                    
            'total_travel_meter'=>$m->get_data('total_travel_meter')
        );
       
        if($max_punch_out_time > $datePunchOUT || $convertedTime > $edit_punch_out_time){
            $q = $d->update("attendance_master", $a1,"attendance_id = '$update_attendance_id'");

            if($q)
            {
                // Check Half Day
                $leaveValue = false;
                if($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00")
                {
                    $leaveValue = true;
                }
                elseif($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $edit_punch_out_time)
                {
                    $leaveValue = true;
                }
                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);
                if($total_minutes >= $minimum_hours_for_full_day_min)
                {
                    $noLeave = true;
                }
                else
                {
                    $noLeave = false;
                }

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];
                $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                $alreadyLeaveQryCheck = false;
                if($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0)
                {
                    $leave_total_days = 1;
                    // check half day and full day
                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $avgCount = "0";
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $is_leave = "1";
                        $extra_day_leave_type = "1";
                        $half_day_session = "0";                
                    }else{
                        $avgCount = "0.5";
                        $leaveType = "1";
                        $is_leave = "2";
                        $extra_day_leave_type = "2";
                        $leaveTypeName = "Half Day";
                    }

                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }

                    if($hasLeave == true)
                    {
                        // Change already applied leave
                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed (".$totalHR.")";
                        $description = "For Date : ".$attendance_start_date."\n".$titleMessage;
                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'half_day_session'=>$m->get_data('half_day_session'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason')
                        );
                        $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");
                        if($device == 'android')
                        {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
                        elseif($device == 'ios')
                        {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                    }
                    else
                    {
                        // Auto Leave
                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed (".$totalHR.")";
                        $description = "For Date : ".$attendance_start_date."\n".$titleMessage;
                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_start_date);
                        $m->set_data('leave_end_date',$attendance_end_date);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));
                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date')
                        );
                        $leaveQry = $d->insert("leave_master",$a);
                        $alreadyLeaveQryCheck = true;
                        if($device == 'android')
                        {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
                        elseif($device == 'ios')
                        {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                    }
                    
                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$avgCount);
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);


                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),

                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$update_attendance_id'");
                }
                elseif($noLeave == true && $hasLeave == true)
                {
                    // Remove leave if present and full time
                    $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");
                    if($leaveQry)
                    {
                        $m->set_data('auto_leave',"0");
                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave')
                        );
                        $d->update("attendance_master",$a1,"attendance_id = '$update_attendance_id'");
                    }
                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");
                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_start_date;
                    if($device == 'android')
                    {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
                    elseif($device == 'ios')
                    {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                }
                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave
                $startMonthDate = date('Y-m-01');
                $endMonthDate = date('Y-m-t');
                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");
                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");
                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;
                if($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && ($todayLateIn == "1" || $early_out == "1"))
                {
                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";
                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$attendance_start_date);
                    $m->set_data('leave_end_date',$attendance_end_date);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));
                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date')
                    );
                    $leaveQry = $d->insert("leave_master",$a);
                    $title = "Auto Leave Applied - Half Day";
                    $description = "For Date : ".$attendance_start_date."\n".$titleMessage;
                    if($device == 'android')
                    {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
                    elseif($device == 'ios')
                    {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                    
                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',"0.5");
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$update_attendance_id'");
                }
                // END checking half day
                $_SESSION['msg'] = "Attendance Updated Successfully";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Updated Successfully");
            }
            else
            {
                $_SESSION['msg1'] = "Something went wrong!";
            }
        }else{
            $_SESSION['msg1'] = "Punch out time exceeds maximum punch out time!";
        }
    }

    if (isset($_POST['update_attendance_setting']) && $_POST['update_attendance_setting'] == "update_attendance_setting")
    {
        $update_arr = ['take_attendance_from' => $take_attendance_from];
        $q = $d->update("users_master", $update_arr, "user_id = '$user_id'");
        if ($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance setting successfully updated");
            $_SESSION['msg'] = "Attendance setting successfully updated.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }

    if (isset($_POST['updateAttendanceSettingMulti']) && $_POST['updateAttendanceSettingMulti'] == "updateAttendanceSettingMulti")
    {
        $update_arr = ['take_attendance_from' => $take_attendance_from];
        foreach ($user_id AS $key => $value)
        {
            $q = $d->update("users_master", $update_arr, "user_id = '$value'");
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance setting successfully updated");
        }
        if ($q)
        {
            $_SESSION['msg'] = "Attendance setting successfully updated.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }
}

function getTotalHours($startDate, $endDate, $startTime, $endTime)
{
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }

}


?>