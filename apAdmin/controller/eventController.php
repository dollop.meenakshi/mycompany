<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if(isset($_POST) && !empty($_POST) ){
  if(isset($addevent)) {

    /* echo "<pre>";
    print_r($_POST);die; */

      mysqli_autocommit($con,FALSE);


    /* if (date('Y-m-d',strtotime($event_end_time)) < date('Y-m-d',strtotime($event_start_time))) {
      $_SESSION['msg1'] = 'Event cannot end before Starting. Please add proper dates';
      header('location:../event');
      exit();
    } */
    if(count($_POST['event_date_day']) !== count(array_unique($_POST['event_date_day'])))
    {
      $_SESSION['msg1'] = 'Please select different dates for all days';
      header('location:../event');exit;
    }
    foreach ($_POST['event_date_day'] as $key => $value)
    {
      foreach ($_POST['event_date_day'] as $key1 => $value1)
      {
        if($key1 > $key && $value1 < $value)
        {
          $_SESSION['msg1'] = 'Please select different dates for all days';
          header('location:../event');exit;
        }
      }
    }
    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['event_image']['tmp_name'];
    $ext = pathinfo($_FILES['event_image']['name'], PATHINFO_EXTENSION);
    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {

        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/event_image/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        if ($imageWidth>1000) {
          $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
          $newImageWidth = $imageWidth * $newWidthPercentage /100;
          $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
          $newImageWidth = $imageWidth;
          $newImageHeight = $imageHeight;
        }

        switch ($imageType) {

          case IMAGETYPE_PNG:
          $imageSrc = imagecreatefrompng($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagepng($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;           

          case IMAGETYPE_JPEG:
          $imageSrc = imagecreatefromjpeg($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagejpeg($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;

          case IMAGETYPE_GIF:
          $imageSrc = imagecreatefromgif($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagegif($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;

          default:
          $_SESSION['msg1']="Invalid Event Photo";
          header("Location: ../event");
          exit;
          break;
        }
        $event_image= $newFileName."_event.".$ext;
        $notiUrl= $base_url.'img/event_image/'.$event_image;
      } else {
        $_SESSION['msg1']="Invalid Event Photo";
        header("location:../event");
        exit();
      }
    } else {
      $event_image= "eventDefault.png";
    }

    /* $event_start_time = date('Y-m-d',strtotime($event_start_time));
    $event_end_time = date('Y-m-d',strtotime($event_end_time)); */

    $event_start = $_POST['event_date_day'][1];
    $event_end = end($_POST['event_date_day']);

    $block_id = join(",",$_POST['block_id']); 
    $floor_id = join(",",$_POST['floor_id']); 

    $event_title =html_entity_decode($event_title);
    $m->set_data('society_id',$society_id);
    $m->set_data('event_title',$event_title);
    $m->set_data('event_status',1);
    $m->set_data('event_description',$event_description);
    $m->set_data('eventMom',$eventMom);//Event Address
    $m->set_data('event_start_date',$event_start);
    $m->set_data('event_end_date',$event_end);
    $m->set_data('event_image',$event_image);
     $m->set_data('gst',$gst);
     $m->set_data('tax_slab',$tax_slab);
     $m->set_data('taxble_type',$taxble_type);
     $m->set_data('is_taxble',$is_taxble);
     $m->set_data('block_id',$block_id);
     $m->set_data('floor_id',$floor_id);
    //27march2020

    $a1= array (
      'society_id'=> $m->get_data('society_id'),
      'event_title'=> $m->get_data('event_title'),
      'event_description'=> $m->get_data('event_description'),
      'event_image'=> $m->get_data('event_image'),
      'eventMom'=> $m->get_data('eventMom'),
      'event_start_date'=> $m->get_data('event_start_date'),
      'event_end_date'=> $m->get_data('event_end_date'),
      'event_status'=> $m->get_data('event_status'),
      'gst'=> $m->get_data('gst'),
      'tax_slab'=> $m->get_data('tax_slab'),
      'taxble_type'=> $m->get_data('taxble_type'),
      'is_taxble'=> $m->get_data('is_taxble'),
      'block_id'=> $m->get_data('block_id'),
      'floor_id'=> $m->get_data('floor_id'),
      //27march2020
    );

    $q=$d->insert("event_master",$a1);

    $event_id = $con->insert_id;
    //27march2020

    $checkZero= false;

    for ($cnt=0; $cnt < $event_days ; $cnt++) {
      $n_cnt = $cnt + 1;
              if ($is_taxble=='1' && $gst==1) {
                 $gst_amount_adult =  $_POST['adult_charge'][$cnt] *$tax_slab/100;  //only for Exclude
                 $adult_charge = $_POST['adult_charge'][$cnt] + $gst_amount_adult;

                 $gst_amount_children =  $_POST['child_charge'][$cnt] *$tax_slab/100;  //only for Exclude
                 $child_charge = $_POST['child_charge'][$cnt] + $gst_amount_children;

                 $gst_amount_guest =  $_POST['guest_charge'][$cnt] *$tax_slab/100;  //only for Exclude
                 $guest_charge = $_POST['guest_charge'][$cnt] + $gst_amount_guest;

                 $gst_amount_adult_tenant =  $_POST['adult_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
                 $adult_charge_tenant = $_POST['adult_charge_tenant'][$cnt] + $gst_amount_adult_tenant;

                 $gst_amount_children_tenant =  $_POST['child_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
                 $child_charge_tenant = $_POST['child_charge_tenant'][$cnt] + $gst_amount_children_tenant;

                 $gst_amount_guest_tenant =  $_POST['guest_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
                 $guest_charge_tenant = $_POST['guest_charge_tenant'][$cnt] + $gst_amount_guest_tenant;

              }  else {
                $adult_charge =  $_POST['adult_charge'][$cnt];
                $child_charge = $_POST['child_charge'][$cnt];
                $guest_charge = $_POST['guest_charge'][$cnt];
                $adult_charge_tenant =  $_POST['adult_charge_tenant'][$cnt];
                $child_charge_tenant = $_POST['child_charge_tenant'][$cnt];
                $guest_charge_tenant = $_POST['guest_charge_tenant'][$cnt];
              }

              $m->set_data('event_day_name',$_POST['event_day_name'][$cnt]);
              $m->set_data('event_type_day',$_POST['event_type_day'][$cnt]);
              $m->set_data('event_allow',$_POST['event_allow'][$cnt]);
              $m->set_data('maximum_pass_adult',$_POST['maximum_pass_adult'][$cnt]);
              $m->set_data('maximum_pass_children',$_POST['maximum_pass_children'][$cnt]);
              $m->set_data('maximum_pass_guests',$_POST['maximum_pass_guests'][$cnt]);
              $m->set_data('maximum_user_pass_adult',$_POST['maximum_user_pass_adult'][$cnt]);
              $m->set_data('maximum_user_pass_children',$_POST['maximum_user_pass_children'][$cnt]);
              $m->set_data('maximum_user_pass_guests',$_POST['maximum_user_pass_guests'][$cnt]);
              $m->set_data('balancesheet_id',$_POST['balancesheet_id'][$cnt]);
              $m->set_data('event_time',$_POST['event_time'][$cnt]);

               $m->set_data('adult_charge','0');
                 $m->set_data('child_charge','0');
                 $m->set_data('guest_charge','0');
                 $m->set_data('adult_charge_tenant','0');
                 $m->set_data('child_charge_tenant','0');
                 $m->set_data('guest_charge_tenant','0');

              if($_POST['event_type_day'][$cnt]=="1" && $_POST['adult_charge'][$cnt]>0 || $_POST['child_charge'][$cnt]>0 || $_POST['guest_charge'][$cnt]>0 || $_POST['adult_charge_tenant'][$cnt]>0 || $_POST['child_charge_tenant'][$cnt]>0 || $_POST['guest_charge_tenant'][$cnt]>0 ){

                 $m->set_data('adult_charge',$adult_charge);
                 $m->set_data('child_charge',$child_charge);
                 $m->set_data('guest_charge',$guest_charge);
                 $m->set_data('adult_charge_tenant',$adult_charge_tenant);
                 $m->set_data('child_charge_tenant',$child_charge_tenant);
                 $m->set_data('guest_charge_tenant',$guest_charge_tenant);
              } else if ($_POST['event_type_day'][$cnt]=="1") {
                    $_SESSION['msg1']="Need atleast 1 price greater than 0 in paid event";
                    header("Location: ../event");
                    exit();
              }
              //$event_date = date("Y-m-d", strtotime($event_start_time. ' +'.$cnt.' day'));
              $event_date = $_POST['event_date_day'][$n_cnt];
              $m->set_data('event_date',$event_date);
              $m->set_data('event_id',$event_id);

              $event_days_master_array= array (
              'society_id'=> $m->get_data('society_id'),
              'event_id'=> $m->get_data('event_id'),
              'event_date'=> $m->get_data('event_date'),
              'event_time'=> $m->get_data('event_time'),
              'balancesheet_id'=> $m->get_data('balancesheet_id'),
              'event_day_name'=> $m->get_data('event_day_name'),
              'event_type'=> $m->get_data('event_type_day'),
              'event_allow'=> $m->get_data('event_allow'),
              'adult_charge'=> $m->get_data('adult_charge'),
              'child_charge'=> $m->get_data('child_charge'),
              'guest_charge'=> $m->get_data('guest_charge'),
              'adult_charge_tenant'=> $m->get_data('adult_charge_tenant'),
              'child_charge_tenant'=> $m->get_data('child_charge_tenant'),
              'guest_charge_tenant'=> $m->get_data('guest_charge_tenant'),
              'maximum_pass_adult'=> $m->get_data('maximum_pass_adult'),
              'maximum_pass_children'=> $m->get_data('maximum_pass_children'),
              'maximum_pass_guests'=> $m->get_data('maximum_pass_guests'),
              'maximum_user_pass_adult'=> $m->get_data('maximum_user_pass_adult'),
              'maximum_user_pass_children'=> $m->get_data('maximum_user_pass_children'),
              'maximum_user_pass_guests'=> $m->get_data('maximum_user_pass_guests')
            );
            
          if ($_POST['maximum_pass_adult'][$cnt]>0) {
           $q1= $event_days_master_result=$d->insert("event_days_master",$event_days_master_array);
          }else {
                $_SESSION['msg1']="Need atleast 1 Person in Maximum Allowed";
                header("Location: ../event");
                exit();
          }
    }
    //27march2020
    if($q and $q1) {

      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$event_title Event Added");

      $block_id = explode (",", $block_id); 
      $block_id = implode ("','", $block_id); 
      $floor_id = explode (",", $floor_id); 
      $floor_id = implode ("','", $floor_id); 
      if ($block_id>0) {
          if($floor_id>0){
            $appendBlockQUery = " users_master.floor_id IN ('$floor_id')";
            $appendBlockQUeryAnd = " AND floor_id IN ('$floor_id')";
          }else{
            $appendBlockQUery = " users_master.block_id IN ('$block_id')";
            $appendBlockQUeryAnd = " AND block_id IN ('$block_id')";
          }    
      }

      $title="New Event Created";
      $description= "$event_title on $event_start";
      $d->insertUserNotification($society_id,$title,$description,"events","Events_1xxxhdpi.png","$appendBlockQUery");


      $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $appendBlockQUeryAnd");
      $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $appendBlockQUeryAnd");
      $nResident->noti("EventDetailsFragment","$notiUrl",$society_id,$fcmArray,"New Event Created","$event_title on $event_start",'events');
      $nResident->noti_ios("EventsVC","$notiUrl",$society_id,$fcmArrayIos,"New Event Created","$event_title on $event_start",'events');
     mysqli_commit($con);

      $_SESSION['msg']="Event Added";
      header("Location: ../events");
    } else {
      mysqli_rollback($con);
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../events");
    }
  }

  if(isset($editevent)) {

     mysqli_autocommit($con,FALSE);

    /* if (date('Y-m-d',strtotime($event_end_time)) < date('Y-m-d',strtotime($event_start_time))) {
      $_SESSION['msg1'] = 'Event cannot end before Starting. Please add proper dates';
      header('location:../events');
      exit();
    } */
    if(count($_POST['event_date_day']) !== count(array_unique($_POST['event_date_day'])))
    {
      $_SESSION['msg1'] = 'Please select different dates for all days';
      header('location:../event');exit;
    }
    foreach ($_POST['event_date_day'] as $key => $value)
    {
      foreach ($_POST['event_date_day'] as $key1 => $value1)
      {
        if($key1 > $key && $value1 < $value)
        {
          $_SESSION['msg1'] = 'Please select dates in ascending order';
          header('location:../event');exit;
        }
      }
    }
    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
    $uploadedFile = $_FILES['event_image']['tmp_name'];
    $ext = pathinfo($_FILES['event_image']['name'], PATHINFO_EXTENSION);
    if(file_exists($uploadedFile)) {
      if(in_array($ext,$extension)) {

        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = rand().$user_id;
        $dirPath = "../../img/event_image/";
        $imageType = $sourceProperties[2];
        $imageHeight = $sourceProperties[1];
        $imageWidth = $sourceProperties[0];
        if ($imageWidth>1000) {
          $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
          $newImageWidth = $imageWidth * $newWidthPercentage /100;
          $newImageHeight = $imageHeight * $newWidthPercentage /100;
        } else {
          $newImageWidth = $imageWidth;
          $newImageHeight = $imageHeight;
        }

        switch ($imageType) {

          case IMAGETYPE_PNG:
          $imageSrc = imagecreatefrompng($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagepng($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;           

          case IMAGETYPE_JPEG:
          $imageSrc = imagecreatefromjpeg($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagejpeg($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;

          case IMAGETYPE_GIF:
          $imageSrc = imagecreatefromgif($uploadedFile); 
          $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
          imagegif($tmp,$dirPath. $newFileName. "_event.". $ext);
          break;

          default:
          $_SESSION['msg1']="Invalid Event Photo";
          header("Location: ../events");
          exit;
          break;
        }
        $event_image= $newFileName."_event.".$ext;
        $notiUrl= $base_url.'img/event_image/'.$event_image;
      } else {
        $_SESSION['msg1']="Invalid Event Photo";
        header("location:../events");
        exit();
      }
    } else {
      $event_image=$event_image_old;
    }

   

    /* $event_start_time = date('Y-m-d',strtotime($event_start_time));
    $event_end_time = date('Y-m-d',strtotime($event_end_time)); */

    $event_start = $_POST['event_date_day'][1];
    $event_end = end($_POST['event_date_day']);

    $block_id = join(",",$_POST['block_id']); 
    $floor_id = join(",",$_POST['floor_id']); 

    $event_title =html_entity_decode($event_title);

    $m->set_data('society_id',$society_id);
    $m->set_data('event_title',$event_title);
    $m->set_data('event_description',$event_description);
    $m->set_data('eventMom',$eventMom); //event Address
    $m->set_data('event_start_date',$event_start);
    $m->set_data('event_end_date',$event_end);
    $m->set_data('event_image',$event_image);
    $m->set_data('block_id',$block_id);
     $m->set_data('floor_id',$floor_id);
  
//27march2020
     $m->set_data('gst',$gst);
     $m->set_data('tax_slab',$tax_slab);
     $m->set_data('taxble_type',$taxble_type);
     $m->set_data('is_taxble',$is_taxble);
    //27march2020

    $a1= array (

      'society_id'=> $m->get_data('society_id'),
      'event_title'=> $m->get_data('event_title'),
      'event_description'=> $m->get_data('event_description'),
      'event_image'=> $m->get_data('event_image'),
      'eventMom'=> $m->get_data('eventMom'),
      'event_start_date'=> $m->get_data('event_start_date'),
      'event_end_date'=> $m->get_data('event_end_date'),
     
      //27march2020
      'gst'=> $m->get_data('gst'),
      'tax_slab'=> $m->get_data('tax_slab'),
      'taxble_type'=> $m->get_data('taxble_type'),
      'is_taxble'=> $m->get_data('is_taxble'), 
      'block_id'=> $m->get_data('block_id'),
      'floor_id'=> $m->get_data('floor_id'),
      //27march2020
    );
  

   $q=$d->update("event_master",$a1,"event_id='$event_id'");



    //27march2020
    $BookingCheck = $d->count_data_direct("event_attend_id","event_attend_list","event_id='$event_id' AND society_id='$society_id'");
      if ($BookingCheck==0) {
       $q2 = $d->delete("event_days_master","event_id='$event_id'");
      }
      for ($cnt=0; $cnt < $total_days ; $cnt++) { 

        $n_cnt = $cnt + 1;
          if ($is_taxble=='1' && $gst==1) {
             $gst_amount_adult =  $_POST['adult_charge'][$cnt] *$tax_slab/100;  //only for Exclude
             $adult_charge = $_POST['adult_charge'][$cnt] + $gst_amount_adult;

             $gst_amount_children =  $_POST['child_charge'][$cnt] *$tax_slab/100;  //only for Exclude
             $child_charge = $_POST['child_charge'][$cnt] + $gst_amount_children;

             $gst_amount_guest =  $_POST['guest_charge'][$cnt] *$tax_slab/100;  //only for Exclude
             $guest_charge = $_POST['guest_charge'][$cnt] + $gst_amount_guest;

             $gst_amount_adult_tenant =  $_POST['adult_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
             $adult_charge_tenant = $_POST['adult_charge_tenant'][$cnt] + $gst_amount_adult_tenant;

             $gst_amount_children_tenant =  $_POST['child_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
             $child_charge_tenant = $_POST['child_charge_tenant'][$cnt] + $gst_amount_children_tenant;

             $gst_amount_guest_tenant =  $_POST['guest_charge_tenant'][$cnt] *$tax_slab/100;  //only for Exclude
             $guest_charge_tenant = $_POST['guest_charge_tenant'][$cnt] + $gst_amount_guest_tenant;

          }  else {
            $adult_charge =  $_POST['adult_charge'][$cnt];
            $child_charge = $_POST['child_charge'][$cnt];
            $guest_charge = $_POST['guest_charge'][$cnt];
            $adult_charge_tenant =  $_POST['adult_charge_tenant'][$cnt];
            $child_charge_tenant = $_POST['child_charge_tenant'][$cnt];
            $guest_charge_tenant = $_POST['guest_charge_tenant'][$cnt];
          }


              $m->set_data('event_day_name',$_POST['event_day_name'][$cnt]);
              $m->set_data('event_type_day',$_POST['event_type_day'][$cnt]);
              $m->set_data('event_allow',$_POST['event_allow'][$cnt]);
              $m->set_data('maximum_pass_adult',$_POST['maximum_pass_adult'][$cnt]);
              $m->set_data('maximum_pass_children',$_POST['maximum_pass_children'][$cnt]);
              $m->set_data('maximum_pass_guests',$_POST['maximum_pass_guests'][$cnt]);
              $m->set_data('maximum_user_pass_adult',$_POST['maximum_user_pass_adult'][$cnt]);
              $m->set_data('maximum_user_pass_children',$_POST['maximum_user_pass_children'][$cnt]);
              $m->set_data('maximum_user_pass_guests',$_POST['maximum_user_pass_guests'][$cnt]);
             $m->set_data('balancesheet_id',$_POST['balancesheet_id'][$cnt]);
             $m->set_data('event_time',$_POST['event_time'][$cnt]);

              $m->set_data('adult_charge','0');
              $m->set_data('child_charge','0');
              $m->set_data('guest_charge','0');
              $m->set_data('adult_charge_tenant','0');
              $m->set_data('child_charge_tenant','0');
              $m->set_data('guest_charge_tenant','0');

              $adult_charge = floatval($adult_charge);
              $child_charge = floatval($child_charge);
              $guest_charge = floatval($guest_charge);
              $adult_charge_tenant = floatval($adult_charge_tenant);
              $child_charge_tenant = floatval($child_charge_tenant);
              $guest_charge_tenant = floatval($guest_charge_tenant);

              if($_POST['event_type_day'][$cnt]=="1" && $adult_charge>0 || $child_charge>0 || $guest_charge>0 || $adult_charge_tenant>0 || $child_charge_tenant>0 || $guest_charge_tenant>0 ){
                 
                 $m->set_data('adult_charge',$adult_charge);
                 $m->set_data('child_charge',$child_charge);
                 $m->set_data('guest_charge',$guest_charge);
                 $m->set_data('adult_charge_tenant',$adult_charge_tenant);
                 $m->set_data('child_charge_tenant',$child_charge_tenant);
                 $m->set_data('guest_charge_tenant',$guest_charge_tenant);
              }  else if ($_POST['event_type_day'][$cnt]=="1") {
                    $_SESSION['msg1']="Need atleast 1 price greater than 0 in paid event";
                    header("Location: ../events");
                    exit();
              }  
           
              //$event_date = date("Y-m-d", strtotime($event_start_time. ' +'.$cnt.' day'));
              $event_date = $_POST['event_date_day'][$n_cnt];
              $m->set_data('event_date',$event_date); 
              $m->set_data('event_id',$event_id); 

              $event_days_master_array= array (
              'society_id'=> $m->get_data('society_id'),
              'event_id'=> $m->get_data('event_id'),
              'event_date'=> $m->get_data('event_date'),
              'event_time'=> $m->get_data('event_time'),
              'balancesheet_id'=> $m->get_data('balancesheet_id'),
              'event_day_name'=> $m->get_data('event_day_name'),
              'event_type'=> $m->get_data('event_type_day'),
              'event_allow'=> $m->get_data('event_allow'),
              'adult_charge'=> $m->get_data('adult_charge'),
              'child_charge'=> $m->get_data('child_charge'),
              'guest_charge'=> $m->get_data('guest_charge'),
              'adult_charge_tenant'=> $m->get_data('adult_charge_tenant'),
              'child_charge_tenant'=> $m->get_data('child_charge_tenant'),
              'guest_charge_tenant'=> $m->get_data('guest_charge_tenant'),
              'maximum_pass_adult'=> $m->get_data('maximum_pass_adult'),
              'maximum_pass_children'=> $m->get_data('maximum_pass_children'),
              'maximum_pass_guests'=> $m->get_data('maximum_pass_guests'),
              'maximum_user_pass_adult'=> $m->get_data('maximum_user_pass_adult'),
              'maximum_user_pass_children'=> $m->get_data('maximum_user_pass_children'),
              'maximum_user_pass_guests'=> $m->get_data('maximum_user_pass_guests'),
            );
            $events_day_id  = $_POST['events_day_id'][$cnt];
        
          if ($_POST['maximum_pass_adult'][$cnt]>0) {
             $qch=$d->select("event_days_master","events_day_id='$events_day_id' AND event_id='$event_id'");
             if (mysqli_num_rows($qch)>0) {
              $q1= $event_days_master_result=$d->update("event_days_master",$event_days_master_array,"events_day_id='$events_day_id' AND event_id='$event_id'");
              
             } else {
              $q1= $event_days_master_result=$d->insert("event_days_master",$event_days_master_array);
             }
          }else {
                $_SESSION['msg1']="Need atleast 1 Person in Maximum Allowed";
                header("Location: ../events");
                exit();
          }  
           
    }
    //27march2020
    if($q and $q1) {

      $block_id = explode (",", $block_id); 
      $block_id = implode ("','", $block_id); 
      $floor_id = explode (",", $floor_id); 
      $floor_id = implode ("','", $floor_id); 
      if ($block_id>0) {
          if($floor_id>0){
            $appendBlockQUery = " users_master.floor_id IN ('$floor_id')";
            $appendBlockQUeryAnd = " AND floor_id IN ('$floor_id')";
          }else{
            $appendBlockQUery = " users_master.block_id IN ('$block_id')";
            $appendBlockQUeryAnd = " AND block_id IN ('$block_id')";
          }    
      }

      $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $appendBlockQUeryAnd");
      $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $appendBlockQUeryAnd");
      $nResident->noti("EventDetailsFragment","$notiUrl",$society_id,$fcmArray,"Event Data Updated","$event_title on $event_start",'events');
      $nResident->noti_ios("EventsVC","$notiUrl",$society_id,$fcmArrayIos,"Event Data Updated","$event_title on $event_start",'events');

      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$event_title Event Updated");
     mysqli_commit($con);
      $_SESSION['msg']="Event Updated";
      header("Location: ../events");
    } else {
      mysqli_rollback($con);
      $_SESSION['msg1']="Something Wrong";
      header("Location: ../events");
    }
  }

if(isset($bookEvent) ){

  /* echo "<pre>";
  print_r($_POST); */
  $qmaint = $d->select("event_days_master","events_day_id ='$events_day_id'");
  $maintData=mysqli_fetch_array($qmaint);
  $event_allow=$maintData['event_allow'];
  if($event_allow == 0){
    $going_person = 1;
    $going_child = 0;
    $going_guest = 0;
  }
  $maxiAdult=$maintData['maximum_pass_adult'];
  $maxiChild=$maintData['maximum_pass_children'];
  $maxiGuest=$maintData['maximum_pass_guests'];

  $q111 = $d->sum_data("going_person","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'");
  $adultData = mysqli_fetch_array($q111);
  $q222 = $d->sum_data("going_child","event_attend_list","book_status=1 AND events_day_id ='$events_day_id' ");
  $childData  = mysqli_fetch_array($q222);
  $q333 = $d->sum_data("going_guest","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'  ");
  $guestData  = mysqli_fetch_array($q333);
  $totalBookedAdult=$adultData['SUM(going_person)']+$going_person;
  $totalBookedChild=$childData['SUM(going_child)']+$going_child;
  $totalBookedGuest=$guestData['SUM(going_guest)']+$going_guest;

  if ($maxiAdult<$totalBookedAdult ||  $maxiChild<$totalBookedChild && $going_child!=0 || $maxiGuest<$totalBookedGuest && $going_guest!=0) {
    $_SESSION['msg1']="No More Booking Available";
    header("Location: ../viewEvents?id=$event_id");
      exit();
  }

    $event_name =html_entity_decode($event_name);

   $m->set_data('event_id',$event_id);
   $m->set_data('events_day_id',$events_day_id);
   
   $m->set_data('society_id',$society_id);
   $m->set_data('user_id',$user_id);

   $users_master_qry=$d->select("users_master","delete_status=0 AND society_id='$society_id'   and user_id ='$user_id' ",""); 
   $users_master_data = mysqli_fetch_array($users_master_qry);
 
   $user_type= $users_master_data['user_type'];

   $m->set_data('userType',$users_master_data['user_type']);
   $m->set_data('unit_id',$users_master_data['unit_id']);
   $unit_id=$users_master_data['unit_id'];

   $m->set_data('recived_amount',$recived_amount);
   $m->set_data('payment_received_date','');
   $m->set_data('bank_name','');
   $m->set_data('payment_ref_no','');
   $m->set_data('notes',$notes);
   $m->set_data('going_person',$going_person);
   $m->set_data('going_child',$going_child);
   $m->set_data('going_guest',$going_guest);


   $event_days_master_qry=$d->select("event_days_master","society_id='$society_id'   and events_day_id ='$events_day_id' ",""); 
   $event_days_master_data = mysqli_fetch_array($event_days_master_qry);
   $m->set_data('balancesheet_id',$event_days_master_data['balancesheet_id']);

   if($event_days_master_data['event_type'] =="1"){
    
    if ($user_type==1) {
      $adult_charge=$event_days_master_data['adult_charge_tenant'];
      $child_charge=$event_days_master_data['child_charge_tenant'];
      $guest_charge=$event_days_master_data['guest_charge_tenant'];
    } else {
      $adult_charge=$event_days_master_data['adult_charge'];
      $child_charge=$event_days_master_data['child_charge'];
      $guest_charge=$event_days_master_data['guest_charge'];
    }


    $recived_amount = 0;
    $recived_amount += ($going_person * $adult_charge);
    $recived_amount += ($going_child * $child_charge);
    $recived_amount += ($going_guest * $guest_charge);

    $event_master_qry=$d->select("event_master","society_id='$society_id'   and event_id ='$event_id' ",""); 
   $event_master_data = mysqli_fetch_array($event_master_qry);
     $tax_slab = $event_master_data['tax_slab'];
    $gst = $event_master_data['gst'];

    $GSTAmount = 0;
    $gst_amount = (($recived_amount * $tax_slab));
            if($gst_amount > 0 ){
              $gst_amount  = $gst_amount /100; 
            } else {
              $gst_amount  = 0 ;
            }
       if($gst=="1"){ 
        
           $GSTAmount =  $recived_amount - ($recived_amount * (100/(100+$tax_slab)));  
            $payableamountwithGST = $recived_amount;
          }

      
      $payment_received_date=date("Y-m-d H:i:s");


       $m->set_data('recived_amount',$payableamountwithGST);
       $m->set_data('payment_received_date',$payment_received_date);
       $m->set_data('payment_type',$payment_type);
       if($payment_type != "0"){
           $m->set_data('bank_name',$bank_name);
           $m->set_data('payment_ref_no',$payment_ref_no);
       }
       
   }
    $m->set_data('created_by',$created_by);


      if ($amount_with_gst>$recived_amount) {
        $credit_amount = $amount_with_gst -$recived_amount;
        $qqq=$d->selectRow("user_mobile","users_master","user_id='$user_id' ");
          $userData= mysqli_fetch_array($qqq);
          $user_mobile = $userData['user_mobile'];
          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
          $row=mysqli_fetch_array($count6);
          $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

          $remark= "Overpaid Payment of Event Booking $event_name";      

          $m->set_data('society_id',$society_id);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('unit_id',$unit_id);
          $m->set_data('remark',$remark);
          $m->set_data('credit_amount',$credit_amount);
          $m->set_data('avl_balance',$totalDebitAmount1+$credit_amount);


          $aWallet = array(
            'society_id'=>$m->get_data('society_id'),
            'user_mobile'=>$m->get_data('user_mobile'),
            'unit_id'=>$m->get_data('unit_id'),
            'remark'=>$m->get_data('remark'),
            'credit_amount'=>$m->get_data('credit_amount'),
            'avl_balance'=>$m->get_data('avl_balance'),
            'created_date'=>date("Y-m-d H:i:s"), 
            'admin_id'=>$_COOKIE['bms_admin_id'],
          );

         $wallet_amount_type = 0;
         $wallet_amount = $credit_amount;
         $mailRemark = "& $credit_amount Credited in wallet";
         

      }

      $payment_received_date=date("Y-m-d H:i:s");
      $m->set_data('payment_received_date',$payment_received_date);
      $m->set_data('recived_amount',$recived_amount);
      $m->set_data('paid_by',$user_id);
      $m->set_data('wallet_amount',$wallet_amount);
      $m->set_data('wallet_amount_type',$wallet_amount_type);

     $a1= array (
      'event_id'=> $m->get_data('event_id'),
      'events_day_id' => $m->get_data('events_day_id'),
      'balancesheet_id'=> $m->get_data('balancesheet_id'),
      'society_id'=> $m->get_data('society_id'),
      'user_id'=> $m->get_data('user_id'),
      'userType'=> $m->get_data('userType'),
      'unit_id'=> $m->get_data('unit_id'),
      'recived_amount'=> $m->get_data('recived_amount'),
      'payment_received_date'=> $m->get_data('payment_received_date'),
      'payment_type'=> $m->get_data('payment_type'),
      'bank_name'=> $m->get_data('bank_name'),
      'going_person'=> $m->get_data('going_person'),
      'going_child'=> $m->get_data('going_child'),
      'going_guest'=> $m->get_data('going_guest'),
      'notes'=> $m->get_data('notes') ,
      'payment_ref_no'=> $m->get_data('payment_ref_no') ,
      'book_status' =>"1",
      'created_by' => $m->get_data('created_by') ,
      'paid_by' => $m->get_data('paid_by') ,
      'wallet_amount' => $m->get_data('wallet_amount') ,
      'wallet_amount_type' => $m->get_data('wallet_amount_type') ,

    );
 
    /* print_r($a1);die; */
   
    $q=$d->insert("event_attend_list",$a1);
    $event_attend_id = $con->insert_id;

   
    $m->set_data('event_attend_id',$event_attend_id);


    for ($gp=0; $gp < $going_person; $gp++) { 
   
    $pass_no= "TKT".$user_id;
    $m->set_data('pass_no',$pass_no);
    $m->set_data('pass_amount',$event_days_master_data['adult_charge']);
    $m->set_data('pass_type','0');
    $m->set_data('entry_status','0');

     $event_passes_master_qry= array (
      'event_id'=> $m->get_data('event_id'),
      'events_day_id' => $m->get_data('events_day_id'),
      'society_id'=> $m->get_data('society_id'),
      'user_id'=> $m->get_data('user_id'),
      'unit_id'=> $m->get_data('unit_id'),
      'userType'=> $m->get_data('userType'),
      'pass_no'=> $m->get_data('pass_no'),
      'pass_amount'=> $m->get_data('pass_amount'),
      'pass_type'=> $m->get_data('pass_type'),
      'entry_status'=> $m->get_data('entry_status'),
      'event_attend_id'=> $m->get_data('event_attend_id')
    );
     $qp=$d->insert("event_passes_master",$event_passes_master_qry);


    }


    for ($gp=0; $gp < $going_child; $gp++) { 
   
    $pass_no= "TKT".$user_id;
    $m->set_data('pass_no',$pass_no);
    $m->set_data('pass_amount',$event_days_master_data['child_charge']);
    $m->set_data('pass_type','1');
    $m->set_data('entry_status','0');

     $event_passes_master_qry= array (
      'event_id'=> $m->get_data('event_id'),
      'events_day_id' => $m->get_data('events_day_id'),
      'society_id'=> $m->get_data('society_id'),
      'user_id'=> $m->get_data('user_id'),
      'unit_id'=> $m->get_data('unit_id'),
      'userType'=> $m->get_data('userType'),
      'pass_no'=> $m->get_data('pass_no'),
      'pass_amount'=> $m->get_data('pass_amount'),
      'pass_type'=> $m->get_data('pass_type'),
      'entry_status'=> $m->get_data('entry_status'),
      'event_attend_id'=> $m->get_data('event_attend_id')
    );
     $qc=$d->insert("event_passes_master",$event_passes_master_qry);


    }
  
  for ($gp=0; $gp < $going_guest; $gp++) { 
    
    $pass_no= "TKT".$user_id;
    
    $m->set_data('pass_no',$pass_no);
    $m->set_data('pass_amount',$event_days_master_data['guest_charge']);
    $m->set_data('pass_type','2');
    $m->set_data('entry_status','0');

     $event_passes_master_qry= array (
      'event_id'=> $m->get_data('event_id'),
      'events_day_id' => $m->get_data('events_day_id'),
      'society_id'=> $m->get_data('society_id'),
      'user_id'=> $m->get_data('user_id'),
      'unit_id'=> $m->get_data('unit_id'),
      'userType'=> $m->get_data('userType'),
      'pass_no'=> $m->get_data('pass_no'),
      'pass_amount'=> $m->get_data('pass_amount'),
      'pass_type'=> $m->get_data('pass_type'),
      'entry_status'=> $m->get_data('entry_status'),
      'event_attend_id'=> $m->get_data('event_attend_id')
    );
     $qg=$d->insert("event_passes_master",$event_passes_master_qry);


    }

    if($q==TRUE) {

        if ($wallet_amount>0) {
          $wq=$d->insert("user_wallet_master",$aWallet);
        }

         $qb=$d->select("event_master,event_days_master","event_master.event_id=$event_id AND event_master.event_id=event_days_master.event_id AND  event_days_master.events_day_id='$events_day_id'");
        $bData=mysqli_fetch_array($qb);
        $eventDayName= $bData['event_day_name'];
        $eventBookDate= $bData['event_date'];

        $qUserToken=$d->select("users_master,block_master,unit_master,floors_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id = users_master.floor_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id' ");
        $data_notification=mysqli_fetch_array($qUserToken);
        $sos_user_token=$data_notification['user_token'];
        $user_mobile=$data_notification['user_mobile'];
        $user_email=$data_notification['user_email'];
        $user_first_name=$data_notification['user_full_name'];
        $unit_name=$data_notification['floor_name'].'-'.$data_notification['block_name'];
        $device=$data_notification['device'];
        if ($device=='android') {
           $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,"$event_name Event Booked","By Admin $created_by",'Maintenance');
         }  else if($device=='ios') {
          $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,"$event_name Event Booked","By Admin $created_by",'Maintenance');
        }

      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$user_id,
        'notification_title'=>"$event_name-$eventDayName Event Booked",
        'notification_desc'=>"By Admin $created_by",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'events',
        'notification_logo'=>'Events_1xxxhdpi.png',
      );
      $d->insert("user_notification",$notiAry);
      if ($user_email!='' && $recived_amount!=0) {
        $to=$user_email;
        $paid_by= $user_first_name;
        $received_by= $created_by;
        $received_amount=$recived_amount;
        $invoice_number= "INVEV".$event_attend_id;
        $description= $event_name.' '.$eventDayName .' Booking for Date: '. $eventBookDate .' '.$mailRemark;
        $subject=$event_name. ' Event Payment Acknowledgement #'.$invoice_number;
        $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=E&societyid=$society_id&id=$event_id&event_attend_id=$event_attend_id";
         $receive_date = $payment_received_date;
        $category = "Event";
        $txt_status= "Success";
        $txt_id = "NA";
        switch ($payment_type) {
          case '1':
            $payment_mode = 'Cheque';
            break;
          case '2':
            $payment_mode = 'Online Payment';
            break;
          default:
            $payment_mode = 'Cash';
            break;
        }
        include '../mail/paymentReceipt.php';
        include '../mail.php';
      }

      $_SESSION['msg']="Event Booked";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Booked");
      header("Location: ../viewEvents?id=$event_id");
    } else {
      header("Location: ../viewEvents?id=$event_id");
    }
  
}
if(isset($approveEvent) && $approveEvent=="approveEvent" ){
   $a1= array (
     'book_status'=> "1" 
   );


   $q=$d->update("event_attend_list",$a1,"event_attend_id='$event_attend_id'");
    if($q==TRUE) {
      $_SESSION['msg']="Approved Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Approved Successfully");
      header("Location: ../viewEvents?id=$event_id");
    } else {
      header("Location: ../viewEvents?id=$event_id");
    }

}

if(isset($cancelEventBooking) && $cancelEventBooking=="cancelEventBooking" ){
   $a1= array (
     'book_status'=> "3" 
   );
   $q=$d->update("event_attend_list",$a1,"event_attend_id='$event_attend_id'");
    if($q==TRUE) {
    $event_day_name =html_entity_decode($event_day_name);


      $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND user_token!=''");
      $data_notification=mysqli_fetch_array($qUserToken);
      $sos_user_token=$data_notification['user_token'];
      $user_mobile=$data_notification['user_mobile'];
      $user_first_name=$data_notification['user_first_name'];
      $device=$data_notification['device'];
      $to=$data_notification['user_email'];
      $title= "Your booked $event_day_name($event_date)  cancelled successfully";
      if ($device=='android') {
        $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }  else if($device=='ios') {
        $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }


      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$data_notification['user_id'],
        'notification_title'=>$title,
        'notification_desc'=>"By Admin $created_by",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'events',
        'notification_logo'=>'Events_1xxxhdpi.png'  
      );
      $d->insert("user_notification",$notiAry);
      if ($to!='') {
       /*  $subject=$event_day_name. 'Event Booking  Cancelled';
        $message= "Hi $user_first_name\n$title\nBy Admin $created_by";
        include '../apAdmin/mail.php'; */
        $subject=$event_day_name. ' Event Booking  Cancelled';
        $user_name = $user_first_name;
        $msg= $title;
        include '../mail/eventBookingCancelled.php';
        // print_r($message);
        // exit();
        include '../mail.php';
      }

      $_SESSION['msg']="Event Booking Cancelled Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Cancelled Successfully");
      header("Location: ../viewEvents?id=$event_id");
    } else {
      header("Location: ../viewEvents?id=$event_id");
    }

}

if(isset($cancelEventDayBooking) && $cancelEventDayBooking=="cancelEventDayBooking" ){

  $uq=$d->select("event_attend_list","society_id='$society_id' AND events_day_id='$events_day_id'");
  $userDataArray = array();
  while($uData = mysqli_fetch_array($uq)){
    array_push($userDataArray, $uData['user_id']);
  }
  $userDataArray = array_unique($userDataArray);
  $userDataArray = array_values($userDataArray);
  
  $a1= array (
    'book_status'=> "3",
    'refund_remark'=> $refund_remark 
  );
  $q=$d->update("event_attend_list",$a1,"events_day_id='$events_day_id'");
   if($q==TRUE) {
   $event_day_name =html_entity_decode($event_day_name);

    for($i=0; count($userDataArray)>$i; $i++){
      $user_id = $userDataArray[$i];
      $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND user_token!=''");
      $data_notification=mysqli_fetch_array($qUserToken);
      $sos_user_token=$data_notification['user_token'];
      $user_mobile=$data_notification['user_mobile'];
      $user_first_name=$data_notification['user_first_name'];
      $device=$data_notification['device'];
      $to=$data_notification['user_email'];
      $title= "Your booked $event_day_name($event_date)  cancelled successfully";
      if ($device=='android') {
        $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }  else if($device=='ios') {
        $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }


      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$data_notification['user_id'],
        'notification_title'=>$title,
        'notification_desc'=>"By Admin $created_by",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'events',
        'notification_logo'=>'Events_1xxxhdpi.png'  
      );
      $d->insert("user_notification",$notiAry);
      if ($to!='') {
       /*  $subject=$event_day_name. 'Event Booking  Cancelled';
        $message= "Hi $user_first_name\n$title\nBy Admin $created_by";
        include '../apAdmin/mail.php'; */
        $subject=$event_day_name. ' Event Booking  Cancelled';
        $user_name = $user_first_name;
        $msg= $title;
        include '../mail/eventBookingCancelled.php';
        // print_r($message);
        // exit();
        include '../mail.php';
      }
    }
     $_SESSION['msg']="Event Booking Cancelled Successfully";
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Cancelled Successfully");
     header("Location: ../viewEvents?id=$event_id");
   } else {
     header("Location: ../viewEvents?id=$event_id");
   }

}

if(isset($cancelPaidEventDayBooking) && $cancelPaidEventDayBooking=="cancelPaidEventDayBooking" ){
  mysqli_autocommit($con,FALSE);
  $eq=$d->select("event_master,event_attend_list,event_days_master","event_days_master.events_day_id=event_attend_list.events_day_id AND event_attend_list.event_id=event_master.event_id AND  event_master.society_id = '$society_id' AND  event_days_master.events_day_id='$events_day_id' AND event_attend_list.book_status=1");
  while($data=mysqli_fetch_array($eq)){
    
    $expenses_amount = $data['recived_amount'];
    $expenses_title = $refund_remark;
    $event_attend_id = $data['event_attend_id'];
    $transaction_charges = $data['transaction_charges'];
    $event_day_name = $data['event_day_name'];
    $balancesheet_id = $data['balancesheet_id'];
    $gst = $data['gst'];
    $tax_slab = $data['tax_slab'];
    $is_taxble = $data['is_taxble'];
    $taxble_type = $data['taxble_type'];
    $user_id = $data['user_id'];
    $a1= array (
      'book_status'=> "3", 
      'refund_amount'=> $expenses_amount-$transaction_charges, 
      'refund_remark'=> $expenses_title, 
    );
    
    $q=$d->update("event_attend_list",$a1,"event_attend_id='$event_attend_id'");
     
       $event_day_name =html_entity_decode($event_day_name);
 
       $expenses_add_date = date('Y-m-d h:i:s');
       $expDateAry = explode("-", $expenses_add_date);
       $exYear = $expDateAry[0];
       $exMonth = $expDateAry[1];
       $dateObj   = DateTime::createFromFormat('!m', $exMonth);
       $monthName = $dateObj->format('F'); 
       $expenses_created_date = $monthName.'-'.$exYear;
 
       $m->set_data('expenses_amount',$expenses_amount);
       $m->set_data('expenses_add_date',$expenses_add_date);
       $m->set_data('balancesheet_id',$balancesheet_id);
       $m->set_data('expenses_created_date',$expenses_created_date);
       //IS_1686
       $m->set_data('gst',$gst);
       $m->set_data('tax_slab',$tax_slab);
       $m->set_data('is_taxble',$is_taxble);
       $m->set_data('taxble_type',$taxble_type);
       $gst_amount = (($expenses_amount * $tax_slab));
 

       $m->set_data('expenses_title',$event_day_name.' Event Cancelation Invoice Number : INEV'.$event_attend_id. ' '.$expenses_title);
       $m->set_data('expenses_amount',$expenses_amount);
       $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND user_token!=''");
       $data_notification=mysqli_fetch_array($qUserToken);
        
      
       //IS_1686
         $a2 = array(
           'expenses_title'=>$m->get_data('expenses_title'),
           'expenses_amount'=>$m->get_data('expenses_amount'),
           'expenses_add_date'=>$m->get_data('expenses_add_date'),
           'balancesheet_id'=>$m->get_data('balancesheet_id'),
           'society_id'=>$society_id,
           'expenses_created_date'=>$m->get_data('expenses_created_date'),
           'gst'=>$m->get_data('gst'),
           'tax_slab'=>$m->get_data('tax_slab'), 
           'amount_without_gst'=>$m->get_data('amount_without_gst'), 
           'is_taxble'=>$m->get_data('is_taxble'), 
           'taxble_type'=>$m->get_data('taxble_type'), 
         );
         
       
          $q1 = $d->insert("expenses_balance_sheet_master",$a2);
       
       $sos_user_token=$data_notification['user_token'];
       $user_mobile=$data_notification['user_mobile'];
       $user_first_name=$data_notification['user_first_name'];
       $device=$data_notification['device'];
       // $to=$data_notification['user_email'];
       //$to = "dollop.meenakshi@gmail.com";
       $title= "Your booked $event_day_name($event_date) cancelled successfully";
       if ($device=='android') {
         $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
       }  else {
         $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
       }
 
 
       $notiAry = array(
         'society_id'=>$society_id,
         'user_id'=>$data_notification['user_id'],
         'notification_title'=>$title,
         'notification_desc'=>"By Admin $created_by",    
         'notification_date'=>date('Y-m-d H:i'),
         'notification_action'=>'event',
         'notification_logo'=>'Events_1xxxhdpi.png'  
       );
       
       $d->insert("user_notification",$notiAry);
 
        if ($to!='') {
        /*  $subject=$event_day_name. ' Event Booking Cancelled';
         $message= "Hi $user_first_name\n$title\nBy Admin $created_by";
         include '../mail.php'; */
         $subject=$event_day_name. ' Event Booking  Cancelled';
         $user_name = $user_first_name;
         $msg= $title;
         include '../mail/eventBookingCancelled.php';
         // print_r($message);
         // exit();
         include '../mail.php';
       } 
  }
  if($q==TRUE) {
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Cancelled Successfully");
    mysqli_commit($con);
    $_SEScSION['msg']="Event Booking Cancelled Successfully";
     header("Location: ../viewEvents?id=$event_id");
   } else {
    mysqli_rollback($con);
     header("Location: ../viewEvents?id=$event_id");
   }
}

if(isset($rescheduleEvent) && $rescheduleEvent=="rescheduleEvent" ){
  
  mysqli_autocommit($con,FALSE);

  $a1= array (
    'event_date'=> $event_start_time, 
    'event_time'=> $event_time, 
  );
  
  $q=$d->update("event_days_master",$a1,"events_day_id='$events_day_id' AND event_id='$event_id'");

  $eq=$d->selectRow("event_days_master.event_date,event_master.floor_id,event_master.block_id,event_master.event_id","event_days_master LEFT JOIN event_master ON event_master.event_id =event_days_master.event_id ","event_days_master.event_id='$event_id'");
  $event_dates = array();
  while($data=mysqli_fetch_array($eq)){
    array_push($event_dates,$data['event_date']);
    $fId  = $data['floor_id'];
    $BId  = $data['block_id'];
  }
  $start_date = min($event_dates);
  $end_date = max($event_dates);
  $a2= array (
    'event_start_date'=> $start_date, 
    'event_end_date'=> $end_date, 
  );
 /*  print_r($fId );
  print_r($BId );
  die; */
  $q01=$d->update("event_master",$a2,"event_id='$event_id'");

  if($q==TRUE && $q01) {
    $title="$event_title Rescheduled on $event_start_time";
    $description= "By Admin $created_by";
    $d->insertUserNotification($society_id,$title,$description,"events","Events_1xxxhdpi.png","");

      if($fId >0 && $fId !=""){
        $fltFlter = " AND floor_id = $fId";
      }
      if($BId >0 && $BId !=""){
        $blkFlter = " AND block_id = $BId";
      }
    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' $fltFlter $blkFlter");
    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $fltFlter $blkFlter");
    $nResident->noti("EventDetailsFragment","$notiUrl",$society_id,$fcmArray,$title,$description,'events');
    $nResident->noti_ios("EventsVC","$notiUrl",$society_id,$fcmArrayIos,$title,$description,'events');

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Cancelled Successfully");
    mysqli_commit($con);
    $_SESSION['msg']="Event Booking Cancelled Successfully";
     header("Location: ../viewEvents?id=$event_id");
   } else {
     mysqli_rollback($con);
     $_SESSION['msg']="Something Wrong";
     header("Location: ../viewEvents?id=$event_id");
   }

}

if(isset($cancelEventPaid) && $cancelEventPaid=="cancelEventPaid" ){
    if ($expenses_amount>$recived_amount) {
      $_SESSION['msg1']="Invalid Refund Amount";
      header("Location: ../viewEvents?id=$event_id");
      exit();
    }

    if ($refund_type==1) {
      $expenses_title  = $expenses_title ." (Refund in User Wallet)";
    }

   $a1= array (
     'book_status'=> "3", 
     'refund_amount'=> $expenses_amount, 
     'refund_remark'=> $expenses_title, 
   );
   $q=$d->update("event_attend_list",$a1,"event_attend_id='$event_attend_id'");
    if($q==TRUE) {

      $event_day_name =html_entity_decode($event_day_name);

    

      $expenses_add_date = date('Y-m-d h:i:s');
      $expDateAry = explode("-", $expenses_add_date);
      $exYear = $expDateAry[0];
      $exMonth = $expDateAry[1];
      $dateObj   = DateTime::createFromFormat('!m', $exMonth);
      $monthName = $dateObj->format('F'); 
      $expenses_created_date = $monthName.'-'.$exYear;

      $m->set_data('expenses_amount',$expenses_amount);
      $m->set_data('expenses_add_date',$expenses_add_date);
      $m->set_data('balancesheet_id',$balancesheet_id);
      $m->set_data('expenses_created_date',$expenses_created_date);
      //IS_1686
      $m->set_data('gst',$gst);
      $m->set_data('tax_slab',$tax_slab);
      $m->set_data('is_taxble',$is_taxble);
      $m->set_data('taxble_type',$taxble_type);
      $gst_amount = (($expenses_amount * $tax_slab));

      
      

      $m->set_data('expenses_title',$event_day_name.' Event Cancelation Invoice Number : INEV'.$event_attend_id. ' '.$expenses_title);
      $m->set_data('expenses_amount',$expenses_amount);
      $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND user_token!=''");
      $data_notification=mysqli_fetch_array($qUserToken);

     
      //IS_1686
        $a1 = array(
          'expenses_title'=>$m->get_data('expenses_title'),
          'expenses_amount'=>$m->get_data('expenses_amount'),
          'expenses_add_date'=>$m->get_data('expenses_add_date'),
          'balancesheet_id'=>$m->get_data('balancesheet_id'),
          'society_id'=>$society_id,
          'expenses_created_date'=>$m->get_data('expenses_created_date'),
          'gst'=>$m->get_data('gst'),
          'tax_slab'=>$m->get_data('tax_slab'), 
          'amount_without_gst'=>$m->get_data('amount_without_gst'), 
          'is_taxble'=>$m->get_data('is_taxble'), 
          'taxble_type'=>$m->get_data('taxble_type'), 
        );
            
        $q = $d->insert("expenses_balance_sheet_master",$a1);
       if ($refund_type==1) {
          $user_mobile=$data_notification['user_mobile'];
          $unit_id=$data_notification['unit_id'];

          $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
          $row=mysqli_fetch_array($count6);
          $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

          $remark = "$event_day_name Event Booking Cancel";
        
          $m->set_data('society_id',$society_id);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('unit_id',$unit_id);
          $m->set_data('remark',$remark);
          $m->set_data('credit_amount',$expenses_amount);
          $m->set_data('avl_balance',$totalDebitAmount1+$expenses_amount);


          $aEx = array(
            'society_id'=>$m->get_data('society_id'),
            'user_mobile'=>$m->get_data('user_mobile'),
            'unit_id'=>$m->get_data('unit_id'),
            'remark'=>$m->get_data('remark'),
            'credit_amount'=>$m->get_data('credit_amount'),
            'avl_balance'=>$m->get_data('avl_balance'),
            'created_date'=>date("Y-m-d H:i:s"), 
             'admin_id'=>$_COOKIE['bms_admin_id'],
          );

           // print_r($a1);
          $d->insert("user_wallet_master",$aEx);
      }
      
      $sos_user_token=$data_notification['user_token'];
      $user_mobile=$data_notification['user_mobile'];
      $user_first_name=$data_notification['user_first_name'];
      $device=$data_notification['device'];
       $to=$data_notification['user_email'];
      $title= "Your booked $event_day_name($event_date) cancelled successfully";
      if ($device=='android') {
        $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }  else if($device=='ios') {
        $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,$title,"By Admin $created_by",'event');
      }


      $notiAry = array(
        'society_id'=>$society_id,
        'user_id'=>$data_notification['user_id'],
        'notification_title'=>$title,
        'notification_desc'=>"By Admin $created_by",    
        'notification_date'=>date('Y-m-d H:i'),
        'notification_action'=>'event',
        'notification_logo'=>'Events_1xxxhdpi.png'  
      );
      $d->insert("user_notification",$notiAry);
     // dollop.meenakshi@gmail.com

       if ($to!='') {
        /* $subject=$event_day_name. ' Event Booking  Cancelled';
        $message= "Hi $user_first_name\n$title\nBy Admin $created_by";
        include '../mail.php'; */
        $subject=$event_day_name. ' Event Booking  Cancelled';
        $user_name = $user_first_name;
        $msg= $title;
        include '../mail/eventBookingCancelled.php';
        // print_r($message);
        // exit();
        include '../mail.php';
      }


      $_SESSION['msg']="Event Booking Cancelled Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Cancelled Successfully");
      header("Location: ../viewEvents?id=$event_id");
    } else {
      header("Location: ../viewEvents?id=$event_id");
    }

}

if(isset($payEventAmount)){
  $m->set_data('payment_type',$payment_type);
  $m->set_data('bank_name',$bank_name);
  $m->set_data('payment_ref_no',$payment_ref_no); 
   $a1= array (
     'payment_type'=> $m->get_data('payment_type'),
     'bank_name'=> $m->get_data('bank_name'),
     'payment_ref_no'=> $m->get_data('payment_ref_no'),
   );
   $q=$d->update("event_attend_list",$a1,"event_attend_id='$event_attend_id'");
    if($q==TRUE) {
      $_SESSION['msg']="Payment Done Successfully";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Done Successfully");
      header("Location: ../viewEvents?id=$event_id");
    } else {
      header("Location: ../viewEvents?id=$event_id");
    }
}
  // if(isset($updateevent)) {

  //   $m->set_data('society_id',$society_id);
  //   $m->set_data('event_title',$event_title);
  //   $m->set_data('event_description',$event_description);
  //   $m->set_data('event_start_date',$event_start_date." ".$event_start_time);
  //   $m->set_data('event_end_date',$event_end_date." ".$event_end_time);

  //   $a1= array (

  //     'society_id'=> $m->get_data('society_id'),
  //     'event_title'=> $m->get_data('event_title'),
  //     'event_description'=> $m->get_data('event_description'),
  //     'event_start_date'=> $m->get_data('event_start_date'),
  //     'event_end_date'=> $m->get_data('event_end_date')

  //   );


  //   $q=$d->update("event_master",$a1,"event_id='$event_id'");
  //   if($q==TRUE) {
  //     $_SESSION['msg']="Event Updated";
  //     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Updated");
  //     header("Location: ../events");
  //   } else {
  //     header("Location: ../events");
  //   }
  // }

  if(isset($event_id_delete)) {
     $q1=$d->select("event_master","event_id='$event_id_delete'");
     $data=mysqli_fetch_array($q1);
     $profileUrl= "../../img/event_image/" . $data['event_image'];
     unlink ($profileUrl);  


    $q=$d->delete("event_master","event_id='$event_id_delete'");
    $q=$d->delete("event_attend_list","event_id='$event_id_delete'");

    if($q==TRUE) {
      $_SESSION['msg']="Event Deleted";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Deleted");
      header("Location: ../events?t=$t");
    } else {
      header("Location: ../events");
    }
  }

  if(isset($sndEventReminder)) {

      $q=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$events_day_id' AND event_attend_list.book_status=1","ORDER BY event_attend_list.event_attend_id DESC");
      while($row=mysqli_fetch_array($q))
      {
          $event_name =html_entity_decode($event_name);

    
           $device=$row['device'];
           $sos_user_token=$row['user_token'];

           $title = "Event Reminder";
           $desc = "$event_date on our $event_day_name-$event_name event !";

           if ($device=='android') {
             $nResident->noti("EventDetailsFragment","",$society_id,$sos_user_token,$title,$desc,'events');
           }  else if($device=='ios') {
             $nResident->noti_ios("EventsVC","",$society_id,$sos_user_token,$title,$desc,'events');
           }

           
           $notiAry = array(
            'society_id'=>$society_id,
            'user_id'=>$row['user_id'],
            'notification_title'=>$title,
            'notification_desc'=>$desc,    
            'notification_date'=>date('Y-m-d H:i'),
            'notification_action'=>'events',
            'notification_logo'=>'Events_1xxxhdpi.png',
          );

           $d->insert("user_notification",$notiAry);

      }


      $_SESSION['msg']="Event Reminder Sent Successfully !";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Reminder Sent");
      header("Location: ../viewEvents?id=$event_id");

  }


}

?>