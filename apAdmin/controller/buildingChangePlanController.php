<?php 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
$base_url=$m->base_url();

header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

 //print_r($_POST);exit;
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
    $response = array();

  // add main menu
  if(isset($_POST['society_id']) && $society_id!=0 && $society_id!='' && $changePlan=='changePlan'){
     $qcb=$d->select("society_master","society_id='$society_id'");
     $dataqcb=mysqli_fetch_array($qcb);
     if ($dataqcb==0) {
        $response["message"]="Society Not Found ";
        $response["status"]="201";
        echo json_encode($response);
        exit();
     } else {
      
      $m->set_data('plan_expire_date',$plan_expire_date);
      $m->set_data('last_renew_date',$last_renew_date);
      $m->set_data('package_id',$package_id);

      $a =array(
          'plan_expire_date'=>$m->get_data('plan_expire_date'),
          'last_renew_date'=>$m->get_data('last_renew_date'),
          'package_id'=>$m->get_data('package_id'),
      );

      $q=$d->update("society_master",$a,"society_id='$society_id'");

      if($q>0) {
        
        // $d->insert_log("","0","$_COOKIE[bms_admin_id]","$created_by","Society Added .");
        $response["message"]="Plan Date Changed Successfully";
        $response["status"]="200";
        echo json_encode($response);
        exit();
      } else {
        $response["message"]="Something Went Wrong";
        $response["status"]="201";
        echo json_encode($response);
        exit();
      }
    }
  }
    

  if(isset($_POST['society_id']) && $society_id!=0 && $society_id!='' && $changePasswordMainAdmin=='changePasswordMainAdmin'){
      
       $qs=$d->select('bms_admin_master',"admin_mobile='9687271071' AND role_id=2");
       if (mysqli_num_rows($qs)==0) {
          $response["message"]="No User Found";
          $response["status"]="201";
          echo json_encode($response);
          exit();
       
       } 

       $m->set_data('password',$new_password);
       $a1= array (
          'admin_password'=> $m->get_data('password'),
       );

       $q=$d->update('bms_admin_master',$a1,"admin_mobile='9687271071' AND role_id=2"); 

      if($q>0) {
        $reArray = array('status'=>'200','message'=>'Password Changed '); 
        echo json_encode($reArray);
        // $d->insert_log("","0","$_COOKIE[bms_admin_id]","$created_by","Society Added .");
        // $response["message"]="Password Changed Successfully";
        // $response["status"]="200";
        // echo json_encode($response);
        exit();
      } else {
        $reArray = array('status'=>'201','message'=>'Something Went Wrong'); 
        echo json_encode($reArray);
        // $response["message"]="Something Went Wrong";
        // $response["status"]="201";
        // echo json_encode($response);
        exit();
      }

  }

   if(isset($_POST['society_id']) && $society_id!=0 && $society_id!='' && $validateMainAdmin=='validateMainAdmin'){


       $q=$d->select('bms_admin_master',"admin_mobile='9687271071' AND role_id=2 AND admin_password='$admin_password'"); 

      if(mysqli_num_rows($q)>0) {
         $reArray = array('status'=>'200','message'=>'Everything is fine'); 
        echo json_encode($reArray);
        // $d->insert_log("","0","$_COOKIE[bms_admin_id]","$created_by","Society Added .");
        exit();
      } else {

       $m->set_data('password',$admin_password);
       $a1= array (
          'admin_password'=> $m->get_data('password'),
       );

       $q=$d->update('bms_admin_master',$a1,"admin_mobile='9687271071' AND role_id=2"); 

        $reArray = array('status'=>'200','message'=>'Password Mismatch & New Updated'); 
        echo json_encode($reArray);
        
        exit();
      }

  }



}
else{
  header('location:../login');
 }
 ?>
