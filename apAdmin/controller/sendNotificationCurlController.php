<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
extract($_POST);
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
// add new Notice Board
$base_url=$m->base_url();

$base_url=$m->base_url();
if(isset($_POST) && !empty($_POST) ){
	extract($_POST);
	if($_POST['send_notofication']=="send_notofication"){
		if($society_id!='' && $title!='' && $description!=''  && $sendto!=''){
			$title = ucfirst($title);
			$description = ucfirst($description);
			if($sendto=='Admin'){
				$fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android' AND admin_active_status=0");
				$fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android' AND admin_active_status=0");
				$nAdmin->noti_new($society_id,"$notiUrl", $fcmArray, $title,$description, "");
				$nAdmin->noti_ios_new($society_id,"$notiUrl", $fcmArrayIos, $title, $description, "");
			} else if($sendto=='Users'){
				$clickAray = array(
					'title' => $title,
					'description' => $description,
					'img_url' =>  $notiUrl,
					'notification_time' => date("d M Y h:i A"),
				);
				$fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");
				$fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
				$nResident->noti("custom_notification",$notiUrl,$society_id,$fcmArray,$title,$description,$clickAray);
				$nResident->noti_ios("custom_notification",$notiUrl,$society_id,$fcmArrayIos,$title,$description,$clickAray);
			}else{
				
			}
			$reArray = array('status'=>'200','message'=>'Notification Send');
		}else{ $reArray = array('status'=>'201','message'=>'Please Complete All Mandatory Fields.'); }
	}else if($_POST['send_notofication']=="send_notofication_module"){
		if($society_id!='' && $title!='' && $description!=''  && $sendto!=''){
			$title = ucfirst($title);
			$description = ucfirst($description);
			if($sendto=='Admin'){
				$fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android' AND admin_active_status=0");
				$fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android' AND admin_active_status=0");
				$nAdmin->noti_new($society_id,"$notiUrl", $fcmArray, $title,$description, "");
				$nAdmin->noti_ios_new($society_id,"$notiUrl", $fcmArrayIos, $title, $description, "");
			}else if($sendto=='Users'){
				$fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'");
				$fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios'");
				$nResident->noti($menu_click,$notiUrl,$society_id,$fcmArray,$title,$description,'');
				$nResident->noti_ios($menu_click,$notiUrl,$society_id,$fcmArrayIos,$title,$description,'');
			}else{
				$reArray = array('status'=>'201','message'=>'Something Went Wrong'); 
			}
			$reArray = array('status'=>'200','message'=>'Send');
		}else{ $reArray = array('status'=>'201','message'=>'Please Complete All Mandatory Fields.'); }
	}else{ $reArray = array('status'=>'201','message'=>'Wrong Tag.'); }
}else{ $reArray = array('status'=>'201','message'=>'Invalid request.'); }
echo json_encode($reArray);