<?php
include '../common/objectController.php';
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addRoute']))
    {
        $ci_st_id = explode("-",$_POST['state_city_id']);
        $city_id = $ci_st_id[0];
        $state_id = $ci_st_id[1];
        $m->set_data('society_id', $society_id);
        $m->set_data('country_id', $country_id);
        $m->set_data('city_id', $city_id);
        $m->set_data('state_id', $state_id);
        $m->set_data('route_name', $route_name);
        $m->set_data('route_description', $route_description);
        if(isset($route_id) && $route_id > 0)
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'state_id' => $m->get_data('state_id'),
                'city_id' => $m->get_data('city_id'),
                'route_name' => $m->get_data('route_name'),
                'route_description' => $m->get_data('route_description'),
                'route_modified_date' => date("Y-m-d H:i:s"),
                'route_updated_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->update("route_master",$a1,"route_id = '$route_id'");
            $_SESSION['msg'] = "Route Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Route Updated Successfully");
            if($q)
            {
                if (isset($retailer_id)) {
                    foreach($retailer_id AS $key => $value)
                    {
                        $check = $d->selectRow("route_retailer_id","route_retailer_master","route_id = '$route_id' AND retailer_id = '$value'");
                        if(mysqli_num_rows($check) == 0)
                        {
                            $m->set_data('society_id', $society_id);
                            $m->set_data('route_id', $route_id);
                            $m->set_data('retailer_id', $value);
                            $a12 = array(
                                'society_id' => $m->get_data('society_id'),
                                'route_id' => $m->get_data('route_id'),
                                'retailer_id' => $m->get_data('retailer_id'),
                                'route_retailer_created_date' => date("Y-m-d H:i:s"),
                                'route_retailer_created_by' => $_COOKIE['bms_admin_id'],
                                'route_retailer_order_by' => $key
                            );
                            $q12 = $d->insert("route_retailer_master", $a12);
                        }
                    }
                }
                foreach($old_retailer_id AS $key1 => $value1)
                {
                    if(!in_array($value1,$retailer_id))
                    {
                        $d->delete("route_retailer_master","route_id = '$route_id' AND retailer_id = '$value1'");
                    }
                }

                $_SESSION['msg'] = "Route Successfully Updated.";
                header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addRoute");
            }
        }
        else
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'state_id' => $m->get_data('state_id'),
                'city_id' => $m->get_data('city_id'),
                'route_name' => $m->get_data('route_name'),
                'route_description' => $m->get_data('route_description'),
                'route_created_date' => date("Y-m-d H:i:s"),
                'route_created_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->insert("route_master", $a1);
            $route_id = $con->insert_id;
            $_SESSION['msg'] = "Route Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Route Added Successfully");
            if($q)
            {   
                if (isset($retailer_id)) {
                    foreach($retailer_id AS $key => $value)
                    {
                        $m->set_data('society_id', $society_id);
                        $m->set_data('route_id', $route_id);
                        $m->set_data('retailer_id', $value);
                        $a12 = array(
                            'society_id' => $m->get_data('society_id'),
                            'route_id' => $m->get_data('route_id'),
                            'retailer_id' => $m->get_data('retailer_id'),
                            'route_retailer_created_date' => date("Y-m-d H:i:s"),
                            'route_retailer_created_by' => $_COOKIE['bms_admin_id'],
                            'route_retailer_order_by' => $key
                        );
                        $q12 = $d->insert("route_retailer_master", $a12);
                    }
                }
                $_SESSION['msg'] = "Route Successfully Added.";
                header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addRoute");
            }
        }
    }
    elseif(isset($_POST['getStateCityTag']))
    {
        $q = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['getStateCityTagRoute']))
    {
        $q = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['deleteRoute']))
    {
        $q = $d->delete("route_master","route_id = '$route_id'");
        if($q)
        {
            $q = $d->delete("route_employee_assign_master","route_id = '$route_id'");
            $q = $d->delete("route_retailer_master","route_id = '$route_id'");
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Route deleted");
            $_SESSION['msg'] = "Route Successfully Delete.";
            header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
    }
    elseif(isset($_POST['checkRouteName']))
    {
        if(isset($route_id))
        {
            $q = $d->selectRow("route_name","route_master","route_name = '$route_name' AND route_id != '$route_id'");
        }
        else
        {
            $q = $d->selectRow("route_name","route_master","route_name = '$route_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['getRetailerTag']))
    {
        $q = $d->selectRow("rm.retailer_id,rm.retailer_name,amn.area_name,rm.retailer_contact_person","retailer_master AS rm JOIN area_master_new AS amn ON amn.area_id = rm.area_id","rm.country_id = '$country_id' AND rm.state_id = '$state_id' AND rm.city_id = '$city_id'");
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['getRetailerListTag']))
    {
        $q = $d->selectRow("rrm.route_retailer_id,rrm.route_retailer_order_by,rm.retailer_name","route_retailer_master AS rrm JOIN retailer_master AS rm ON rm.retailer_id = rrm.retailer_id","route_id = '$route_id'","ORDER BY rrm.route_retailer_order_by ASC");
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['changeRetailerOrder']))
    {
        $ci_st_id = explode("-",$_POST['state_city_id']);
        $city_id = $ci_st_id[0];
        $state_id = $ci_st_id[1];
        $arr = explode(",", $row_order);
        $orn = 1;
        foreach($arr AS $key => $value)
        {
            $a1 = array(
                'route_retailer_order_by' => $orn
            );
            $q = $d->update("route_retailer_master",$a1,"route_retailer_id = '$value'");
            $orn++;
        }
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Retailer order change in route");
            $_SESSION['msg'] = "Retailer Order Successfully Updated.";
            header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageRoute?CId=$country_id&SCId=$city_id-$state_id");exit;
        }
    }
    elseif(isset($_POST['getEmpFromRoute']))
    {
        $q = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","route_employee_assign_master AS ream JOIN users_master AS um ON um.user_id = ream.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","ream.route_id = '$route_id' AND um.society_id = '$society_id' AND um.delete_status = 0");
        $data = [];
        $u_data = [];
        while($row = $q->fetch_assoc())
        {
            extract($row);
            $u_data['u_name'] = $user_full_name . " (" . $block_name . "-" . $floor_name . ")";
            $u_data['user_id'] = $user_id;
            $data[] = $u_data;
        }
        echo json_encode($data);exit;
    }
}
?>