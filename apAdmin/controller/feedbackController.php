<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$base_url=$m->base_url();

$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$bms_admin_id=$_COOKIE['bms_admin_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract($_POST);


if(isset($_POST) && !empty($_POST) ) {
     if(isset($replyFeedback) ){
       $query=$d->select("feedback_master","feedback_id='$feedback_id'   ","LIMIT 1");
       $query_data = mysqli_fetch_assoc($query);
       $to = $query_data['email'];
       $user_name =  $query_data['name'];
       $feedback_society_id =  $query_data['society_id'];
       $subject =$query_data['feedback_msg'];
       $reply= $reply;
       include '../mail/feedbackReply.php';
       include '../mail.php';
       $_SESSION['msg']="Reply Sent Successfully to '$to'..!";
       header("Location: ../feedback");
     }
     if(isset($deleteFeedback)) {
      $q=$d->delete("feedback_master","feedback_id='$feedback_id'    ");
      if($q==TRUE) {
        $_SESSION['msg']="Feedback Deleted";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Feedback Deleted");
        header("Location: ../feedback");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../feedback");
      }
    }


     if(isset($feedback_msg) ){

      $aq=$d->select("bms_admin_master","admin_id='$bms_admin_id'");
      $adminData= mysqli_fetch_array($aq);
      $admin_email = $adminData['admin_email'];
      $admin_mobile = $adminData['admin_mobile'];

       $file_name_with_full_path = $_FILES['attachment']['tmp_name'];
       $fileName = $_FILES['attachment']['name'];
       $fileType = pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);
      if ($fileName!='') {
          # code...
        if (function_exists('curl_file_create')) { // php 5.5+
          $cFile = curl_file_create($file_name_with_full_path);
        } else { // 
          $cFile = '@' . realpath($file_name_with_full_path);
        }

      } else {
        $cFile = "";
      }
      // print_r($cFile);

      $target_url = "https://master.my-company.app/commonApi/contact_fincasysteam_controller.php";
      $post = array('send_feedback_admin' => 'send_feedback_admin','society_id' => $society_id,'name' => $_COOKIE['admin_name'],'mobile' => $admin_mobile,'email' => $admin_email,'subject' => $subject,'feedback_msg' => $feedback_msg,'file_contents'=> $cFile,'fileType'=>$fileType ,'filename'=>$fileName);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$target_url);
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result=curl_exec ($ch);
      curl_close ($ch);
      $json = json_decode($result, true);


      if(count($json)>0) {

        if ($json['status']==200) {
          $_SESSION['msg']= $json['message'];
        } else {
          $_SESSION['msg1']= $json['message'];
        }
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Feedback Message Send");
        header("Location: ../contactSupport");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../contactSupport");
      }
    }


    if(isset($addPaymentGetwatRequest) ){
      
      // registrationProof
       $file = $_FILES['registrationProof']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['registrationProof']['size'] >= $maxsize) || ($_FILES["registrationProof"]["size"] == 0)) {
                $_SESSION['msg1']="Registration Proof too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
            }
            if(!in_array($_FILES['registrationProof']['type'], $acceptable) && (!empty($_FILES["registrationProof"]["type"]))) {
                 $_SESSION['msg1']="Invalid  Registration Proof. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['registrationProof'];   
            $temp = explode(".", $_FILES["registrationProof"]["name"]);
            $registrationProof = 'document_registrationProof_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["registrationProof"]["tmp_name"], "../../img/documents/".$registrationProof);
            $registrationProof = $base_url.'img/documents/'.$registrationProof;
          } 
        } else{
           $_SESSION['msg1']="Upload Registration Proof File";
                header("location:../paymentGatewaySetting");
                exit();

        } 


         // panCard
       $filePan = $_FILES['panCard']['tmp_name'];
        if(file_exists($filePan)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['panCard']['size'] >= $maxsize) || ($_FILES["panCard"]["size"] == 0)) {
                $_SESSION['msg1']="PAN card too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
                
            }
            if(!in_array($_FILES['panCard']['type'], $acceptable) && (!empty($_FILES["panCard"]["type"]))) {
                 $_SESSION['msg1']="Invalid PAN card. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['panCard'];   
            $temp = explode(".", $_FILES["panCard"]["name"]);
            $panCard = 'document_panCard_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["panCard"]["tmp_name"], "../../img/documents/".$panCard);
            $panCard = $base_url.'img/documents/'.$panCard;
          } 
        } else{
           $_SESSION['msg1']="Upload PAN card";
           header("location:../paymentGatewaySetting");
                exit();

        }

           // panCard
       $fileCheque = $_FILES['cancelledCheque']['tmp_name'];
        if(file_exists($fileCheque)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['cancelledCheque']['size'] >= $maxsize) || ($_FILES["cancelledCheque"]["size"] == 0)) {
                $_SESSION['msg1']="Cancelled Cheque too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
                
            }
            if(!in_array($_FILES['cancelledCheque']['type'], $acceptable) && (!empty($_FILES["cancelledCheque"]["type"]))) {
                 $_SESSION['msg1']="Invalid Cancelled Cheque. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['cancelledCheque'];   
            $temp = explode(".", $_FILES["cancelledCheque"]["name"]);
            $cancelledCheque = 'document_cancelledCheque_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["cancelledCheque"]["tmp_name"], "../../img/documents/".$cancelledCheque);
            $cancelledCheque = $base_url.'img/documents/'.$cancelledCheque;
          } 
        } else{
           $_SESSION['msg1']="Upload Cancelled Cheque";
           header("location:../paymentGatewaySetting");
                exit();

        }


        $fileGst = $_FILES['gstCerty']['tmp_name'];
        if(file_exists($fileGst)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['gstCerty']['size'] >= $maxsize) || ($_FILES["gstCerty"]["size"] == 0)) {
                $_SESSION['msg1']="Tax Certificate too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
                
            }
            if(!in_array($_FILES['gstCerty']['type'], $acceptable) && (!empty($_FILES["gstCerty"]["type"]))) {
                 $_SESSION['msg1']="Invalid Tax Certificate. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['gstCerty'];   
            $temp = explode(".", $_FILES["gstCerty"]["name"]);
            $gstCerty = 'document_gstCerty_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["gstCerty"]["tmp_name"], "../../img/documents/".$gstCerty);
            $gstCerty = $base_url.'img/documents/'.$gstCerty;
          } 
        } else{
           $gstCerty = "";
        }


            // authorizedAddress
       $fileAddress = $_FILES['authorizedAddress']['tmp_name'];
        if(file_exists($fileAddress)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['authorizedAddress']['size'] >= $maxsize) || ($_FILES["authorizedAddress"]["size"] == 0)) {
                $_SESSION['msg1']="Address proof too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
                
            }
            if(!in_array($_FILES['authorizedAddress']['type'], $acceptable) && (!empty($_FILES["authorizedAddress"]["type"]))) {
                 $_SESSION['msg1']="Invalid address proof . Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['authorizedAddress'];   
            $temp = explode(".", $_FILES["authorizedAddress"]["name"]);
            $authorizedAddress = 'document_authorizedAddress_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["authorizedAddress"]["tmp_name"], "../../img/documents/".$authorizedAddress);
            $authorizedAddress = $base_url.'img/documents/'.$authorizedAddress;
          } 
        } else{
           $_SESSION['msg1']="Upload Caddress proof ";
           header("location:../paymentGatewaySetting");
                exit();

        }

              // authorize Pan
       $fileAuthPan= $_FILES['authorizedPancard']['tmp_name'];
        if(file_exists($fileAuthPan)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['authorizedPancard']['size'] >= $maxsize) || ($_FILES["authorizedPancard"]["size"] == 0)) {
                $_SESSION['msg1']="Authorized signatory Pancard too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting");
                exit();
                
            }
            if(!in_array($_FILES['authorizedPancard']['type'], $acceptable) && (!empty($_FILES["authorizedPancard"]["type"]))) {
                 $_SESSION['msg1']="Invalid Authorized signatory Pancard. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['authorizedPancard'];   
            $temp = explode(".", $_FILES["authorizedPancard"]["name"]);
            $authorizedPancard = 'document_authorizedPancard_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["authorizedPancard"]["tmp_name"], "../../img/documents/".$authorizedPancard);
            $authorizedPancard = $base_url.'img/documents/'.$authorizedPancard;
          } 
        } else{
           $_SESSION['msg1']="Upload Authorized signatory Pancard";
           header("location:../paymentGatewaySetting");
                exit();

        }

      

      $target_url = "https://master.my-company.app/commonApi/contact_fincasysteam_controller.php";
    
      $post = array('addPaymentGetwatRequest' => 'addPaymentGetwatRequest','society_id' => $society_id,'payment_getway_name' => $payment_getway_name,'contact_name' => $name,'contact_mobile' => $mobile,'email_id' => $email,'registrationProof' => $registrationProof,'created_by'=> $_COOKIE['admin_name'],'panCard'=>$panCard ,'gstCerty'=>$gstCerty,'cancelledCheque'=>$cancelledCheque,'authorizedAddress'=>$authorizedAddress,'authorizedPancard'=>$authorizedPancard);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$target_url);
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result=curl_exec ($ch);
      curl_close ($ch);
      $json = json_decode($result, true);


      if(count($json)>0) {

        if ($json['status']==200) {
          $_SESSION['msg']= $json['message'];
        }else if ($json['status']==202) {
          $_SESSION['msg1']= $json['message'];
        }  else {
          $_SESSION['msg1']= $json['message'];
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Request Send");
        }
        header("Location: ../paymentGatewaySetting");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../paymentGatewaySetting");
      }
    } 


    if(isset($changeStausPaymentStatus) ){
     

        $fileGst = $_FILES['file_name']['tmp_name'];
        if(file_exists($fileGst)) {
           $errors     = array();
            $maxsize    = 4097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            );
            if(($_FILES['file_name']['size'] >= $maxsize) || ($_FILES["file_name"]["size"] == 0)) {
                $_SESSION['msg1']="Document too large. Must be less than 10 MB.";
                header("location:../paymentGatewaySetting?viewRequest=yes&id=$requiest_id");
                exit();
                
            }
            if(!in_array($_FILES['file_name']['type'], $acceptable) && (!empty($_FILES["file_name"]["type"]))) {
                 $_SESSION['msg1']="Invalid Document. Only  JPG,PNG,Doc & PDF are allowed.";            
                header("location:../paymentGatewaySetting?viewRequest=yes&id=$requiest_id");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['file_name'];   
            $temp = explode(".", $_FILES["file_name"]["name"]);
            $file_name = 'document_file_name_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["file_name"]["tmp_name"], "../../img/documents/".$file_name);
            $file_name = $base_url.'img/documents/'.$file_name;
          } 
        } else{
           $file_name = "";
        }



      
      $target_url = "https://master.my-company.app/commonApi/contact_fincasysteam_controller.php";
     
      $post = array('changeStausPaymentStatus' => 'changeStausPaymentStatus','society_id' => $society_id,'log_name' => $log_name,'requiest_id' => $requiest_id,'file_name' => $file_name,'user_name' => $_COOKIE['admin_name'] );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$target_url);
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result=curl_exec ($ch);
      curl_close ($ch);
      $json = json_decode($result, true);


      if(count($json)>0) {

        if ($json['status']==200) {
          $_SESSION['msg']= $json['message'];
        } else {
          $_SESSION['msg1']= $json['message'];
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Request Reply Send");
        }
         header("location:../paymentGatewaySetting?viewRequest=yes&id=$requiest_id");
      } else {
        $_SESSION['msg1']="Something Wrong";
         header("location:../paymentGatewaySetting?viewRequest=yes&id=$requiest_id");
      }
    }  


    if(isset($deletePgRequiest) ){

      $target_url = "https://master.my-company.app/commonApi/contact_fincasysteam_controller.php";
      
      $post = array('deletePaymentGetwatRequest' => 'deletePaymentGetwatRequest','society_id' => $society_id,'requiest_id' => $requiest_id_delete);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$target_url);
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result=curl_exec ($ch);
      curl_close ($ch);
      $json = json_decode($result, true);

      if(count($json)>0) {

        if ($json['status']==200) {
          $_SESSION['msg']= $json['message'];
        } else {
          $_SESSION['msg1']= $json['message'];
          $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Payment Gateway Request Deleted");
        }
        header("Location: ../paymentGatewaySetting");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../paymentGatewaySetting");
      }


    }

      if($_POST['changeStausPaymentStatusAdmin']=="changeStausPaymentStatusAdmin" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($requiest_id, FILTER_VALIDATE_INT) == true) {

           $maxsize    = 4097152;
           $file11=$_FILES["file_contents"]["tmp_name"];
           if (file_exists($file11)) {

                   $temp = explode(".", $_FILES["file_contents"]["name"]);
                    $attachment = "$requiest_id_docuement_".$filename;
                    move_uploaded_file($_FILES["file_contents"]["tmp_name"], "../../img/documents/" . $attachment);
                echo   $file_name_url = $base_url.'img/documents/'.$attachment;
              } else {
                 $file_name_url="";
             }

           $title = ucfirst('New Reply For Payment Gateway Request');
           $description = $log_name;

           $notiAry = array(
                'admin_id'=>0,
                'society_id'=>$society_id,
                'notification_tittle'=>$title,
                'notification_description'=>$description,
                'notifiaction_date'=>date('Y-m-d H:i'),
                'notification_action'=>"paymentGatewaySetting?viewRequest=yes&id=$requiest_id",
                'admin_click_action '=>"paymentGatewaySetting?viewRequest=yes&id=$requiest_id",
                'notification_logo'=>'complain.png',
              );
                      
            $d->insert("admin_notification",$notiAry);

          $fcmArray = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device='android'");
          $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "token!='' AND society_id='$society_id' AND device!='android'");
          $nAdmin->noti_new($society_id,"", $fcmArray, $title,$description, "");
          $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, $title, $description, "");

      }
}
?>