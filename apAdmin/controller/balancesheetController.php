<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


// delete user
if(isset($deleteemployee)) {
  $q=$d->delete("employee_master","emp_id='$emp_id'");
  if($q==TRUE) {
    $_SESSION['msg']="Employee Deleted";
    header("Location: ../employees");
  } else {
    header("Location: ../employees");
  }
}



if(isset($income_balance_sheet_id_delete)) {
  $q=$d->delete("expenses_balance_sheet_master","expenses_balance_sheet_id='$income_balance_sheet_id_delete'");
  if($q==TRUE) {
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Income $expenses_title_delete ($expenses_amount_delete) Deleted");
    $_SESSION['msg']="Income Deleted";
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
  } else {
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
  }
}


if(isset($expenses_balance_sheet_id_delete)) {
  $q=$d->delete("expenses_balance_sheet_master","expenses_balance_sheet_id='$expenses_balance_sheet_id_delete'");
  if($q==TRUE) {
     $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense $expenses_title_delete ($expenses_amount_delete) Deleted");
    $_SESSION['msg']="Expense Deleted";
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  } else {
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  }
}

 if (isset($addExpenseCategory)) {
       
        $m->set_data('expense_category_name',test_input($expense_category_name));
       $m->set_data('category_type',test_input($category_type));

      $a2 = array(
          'expense_category_name'=>$m->get_data('expense_category_name'),
          'category_type'=>$m->get_data('category_type'),

      );

      if(isset($expense_category_id)){
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense $expense_category_name Category Updated");

        $_SESSION['msg'] = "Category Updated Successfully";
        $q2 = $d->update("expense_category_master",$a2,"expense_category_id='$expense_category_id'");
      }else{
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense $expense_category_name Category Added");
        $_SESSION['msg'] = "Category Added Successfully";
        $q2 = $d->insert("expense_category_master",$a2);
      }
     
      if($q2==TRUE) {
            
        header("Location: ../expenseCategories");
      } else {
          $_SESSION['msg1']="Something Wrong";
         header("Location: ../expenseCategories");
      }
  }

  

if(isset($balancesheet_id_delete)) {
  // maintenace
   
  $toatlPenalty=$d->count_data_direct("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id_delete' ");
  $toatlPenaltyUsed=$d->count_data_direct("penalty_id","penalty_master"," society_id='$society_id' AND balancesheet_id='$balancesheet_id_delete' ");

  // event
  $totalEvent=$d->count_data_direct("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$balancesheet_id_delete'");
  $totalEventUsed=$d->count_data_direct("events_day_id","event_days_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id_delete'");

 

  $totalIncome=$d->count_data_direct("expenses_balance_sheet_id","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id_delete'");

    $totalUsed= $MaintenaceUsed+$totalBillUsed+$toatlPenaltyUsed+$totalEventUsed+$totalFacilityUsed;
    $totalTraction= $totalMaintenace+$totalBill+$toatlPenalty+$totalEvent+$totalFacility+$totalIncome;
   if ($totalTraction>0 || $totalUsed>0) {
    $_SESSION['msg1']="This Balancesheet Is Used.";
    header("Location: ../balancesheets");
    exit();
   }

  $q=$d->delete("balancesheet_master","balancesheet_id='$balancesheet_id_delete'");
  if($q==TRUE) {

    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Balancesheet $balancesheet_name_delete Deleted");

    $_SESSION['msg']="Balancesheet Deleted";
    header("Location: ../balancesheets");
  } else {
    header("Location: ../balancesheets");
  }
}

// add new employee
if(isset($balancesheet_name_add)) {

    $balancesheet_name_addCheck=mysqli_real_escape_string($con, $balancesheet_name_add);
   $qc=$d->selectRow("balancesheet_id","balancesheet_master","balancesheet_name='$balancesheet_name_addCheck' AND society_id='$society_id'");
    if (mysqli_num_rows($qc)>0) {
       $_SESSION['msg1']="This Balancesheet Name Already Exits.";
       header("Location: ../balancesheet");
        exit();
    }

  $society_payment_getway_id = implode(",", $_POST['society_payment_getway_id']);
    

  $m->set_data('society_id',$_COOKIE['society_id']);
  $m->set_data('balancesheet_name_add',$balancesheet_name_add);
  $m->set_data('society_payment_getway_id',$society_payment_getway_id);
  $m->set_data('society_payment_getway_id_upi',$society_payment_getway_id_upi);
  $m->set_data('created_date',date("Y-m-d H:i:s"));
  $m->set_data('balncesheet_created_by',$_COOKIE['bms_admin_id']);
  $m->set_data('block_id',$block_id);

  $a1= array (

    'society_id'=> $m->get_data('society_id'),
    'balancesheet_name'=> $m->get_data('balancesheet_name_add'),
    'society_payment_getway_id'=> $m->get_data('society_payment_getway_id'),
    'society_payment_getway_id_upi'=> $m->get_data('society_payment_getway_id_upi'),
    'created_date'=> $m->get_data('created_date'),
    'balncesheet_created_by'=> $m->get_data('balncesheet_created_by'),
    'block_id'=> $m->get_data('block_id'),
  );


  $q=$d->insert("balancesheet_master",$a1);
  if($q==TRUE) {
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Balancesheet Added");
    $_SESSION['msg']="Balancesheet Added";
    header("Location: ../balancesheets");
  } else {
    header("Location: ../balancesheets");
  }
}



// add new employee
if(isset($balancesheet_id_edit)) {

  $balancesheet_nameCheck=mysqli_real_escape_string($con, $balancesheet_name);
  
  $qc=$d->selectRow("balancesheet_id","balancesheet_master","balancesheet_name='$balancesheet_nameCheck' AND society_id='$society_id' AND balancesheet_id!='$balancesheet_id_edit'");
    if (mysqli_num_rows($qc)>0) {
       $_SESSION['msg1']="This Balancesheet Name Already Exits.";
       header("Location: ../balancesheets");
        exit();
    }
  $society_payment_getway_id = implode(",", $_POST['society_payment_getway_id']);

  $m->set_data('society_id',$_COOKIE['society_id']);
  $m->set_data('balancesheet_name',$balancesheet_name);
  $m->set_data('society_payment_getway_id',$society_payment_getway_id);
  $m->set_data('society_payment_getway_id_upi',$society_payment_getway_id_upi);
  $m->set_data('block_id',$block_id);
  

  $a1= array (

    'society_id'=> $m->get_data('society_id'),
    'balancesheet_name'=> $m->get_data('balancesheet_name'),
    'society_payment_getway_id'=> $m->get_data('society_payment_getway_id'),
    'society_payment_getway_id_upi'=> $m->get_data('society_payment_getway_id_upi'),
    'block_id'=> $m->get_data('block_id'),
  );


  $q=$d->update("balancesheet_master",$a1,"balancesheet_id='$balancesheet_id_edit'");
  if($q==TRUE) {
    $_SESSION['msg']="Balancesheet Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Balancesheet Updated");
    header("Location: ../balancesheets");
  } else {
    header("Location: ../balancesheets");
  }
}

// delete user
if(isset($deleteemployeeType)) {
  $q=$d->delete("emp_type_master","emp_id='$deleteemployeeType'");
  if($q==TRUE) {
    $_SESSION['msg']="Employee Type Deleted";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Type Deleted");
    header("Location: ../employeeType");
  } else {
    header("Location: ../employeeType");
  }
}

// Add Expense
if($_POST['padiMainExpense']) {
  $file = $_FILES['expenses_photo']['tmp_name'];
  $_FILES["expenses_photo"]["type"];

  if(file_exists($file)) {
   $errors     = array();
   $maxsize    = 4097152;
   $extensionResume=array("xlsx","csv","pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
   $extId = pathinfo($_FILES['expenses_photo']['name'], PATHINFO_EXTENSION);

   if(($_FILES['expenses_photo']['size'] >= $maxsize) || ($_FILES["expenses_photo"]["size"] == 0)) {
    $_SESSION['msg1']='File too large. Must be less than 4 MB.';
    header("Location: ../viewBalancesheet");  
    exit();       
  }
  if(!in_array($extId, $extensionResume) && (!empty($_FILES["expenses_photo"]["type"]))) {
    $_SESSION['msg1']='Invalid  File. Only  JPG, PNG,doc & PDF are allowed.';
    header("Location: ../viewBalancesheet");
    exit();       
  }
  if(count($errors) === 0) {
    $image_Arr = $_FILES['expenses_photo'];   
    $temp = explode(".", $_FILES["expenses_photo"]["name"]);
    $expenses_photo = round(microtime(true)) . '.' . end($temp);
    move_uploaded_file($_FILES["expenses_photo"]["tmp_name"], "../../img/expenses/".$expenses_photo);
  } 
} else{
  $expenses_photo="";
}
$expenses_photo;

$expDateAry = explode("-", $expenses_add_date);
$exYear = $expDateAry[0];
$exMonth = $expDateAry[1];
$dateObj   = DateTime::createFromFormat('!m', $exMonth);
$monthName = $dateObj->format('F'); 
$expenses_created_date = $monthName.'-'.$exYear;

$m->set_data('expenses_title',$expenses_title);
$m->set_data('expenses_amount',$expenses_amount);
$m->set_data('expenses_add_date',$expenses_add_date);
$m->set_data('balancesheet_id',$balancesheet_id);
$m->set_data('expenses_photo',$expenses_photo);
$m->set_data('expenses_created_date',$expenses_created_date);
//IS_1686
$m->set_data('amount_without_gst',$expenses_amount);
$m->set_data('gst',$gst);
$m->set_data('tax_slab',$tax_slab);
$m->set_data('is_taxble',$is_taxble);
$m->set_data('taxble_type',$taxble_type);

$m->set_data('expense_category_id',$expense_category_id);

$gst_amount = (($expenses_amount * $tax_slab));

if($gst_amount > 0 ){
  $gst_amount  = $gst_amount /100; 
} else {
  $gst_amount  = 0 ;
}
if($gst=="1"){ 

  $gst_amount =  $expenses_amount *$tax_slab/100;  //only for Exclude
  $expenses_amount = $expenses_amount + $gst_amount;
} else {
  $gst_amount =  $expenses_amount - ($expenses_amount * (100/(100+$tax_slab)));  //only for Include
  $expenses_amount = $expenses_amount;
  $m->set_data('amount_without_gst',($expenses_amount -$gst_amount ));
}


  $countev=$d->sum_data("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($rowev=mysqli_fetch_array($countev))
     {
          $asif=$rowev['SUM(recived_amount)'];
        $totalEventBooking=number_format($asif,2,'.','');
             
      }

  $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row=mysqli_fetch_array($count7))
   {
        $asif=$row['SUM(penalty_amount)'];
       $totalPenalaty=number_format($asif,2,'.','');
           
    }

   $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
  while($row2=mysqli_fetch_array($icnome))
 {
      $asif5=$row2['SUM(income_amount)'];
    $totalIncome=number_format($asif5,2,'.','');
         
  }
   $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

  $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
    while($row5=mysqli_fetch_array($count4))
   {
        $asif5=$row5['SUM(expenses_amount)'];
      $totalExp=number_format($asif5,2,'.','');
           
    }

    $amount= $totalIncome-$totalExp;

   if ($amount<$expenses_amount) {
        $_SESSION['msg1']="Not enough Balance in this balance sheet ";
        header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
        exit();
      }

  $m->set_data('expenses_amount',$expenses_amount);
  //IS_1686
  $a1 = array(
    'expenses_title'=>$m->get_data('expenses_title'),
    'expense_category_id'=>$m->get_data('expense_category_id'),
    'expenses_amount'=>$m->get_data('expenses_amount'),
    'expenses_add_date'=>$m->get_data('expenses_add_date'),
    'balancesheet_id'=>$m->get_data('balancesheet_id'),
    'expenses_photo'=>$m->get_data('expenses_photo'),
    'society_id'=>$society_id,
    'expenses_created_date'=>$m->get_data('expenses_created_date'),
    'received_by'=>$_COOKIE['bms_admin_id'],
    //IS_1686
        'gst'=>$m->get_data('gst'),
        'tax_slab'=>$m->get_data('tax_slab'), 
        'amount_without_gst'=>$m->get_data('amount_without_gst'), 
        'is_taxble'=>$m->get_data('is_taxble'), 
        'taxble_type'=>$m->get_data('taxble_type'), 
        'created_date'=>date("Y-m-d H:i:s"), 
       //IS_1686
  );
      // print_r($a1);
  $q = $d->insert("expenses_balance_sheet_master",$a1);

  if($q==TRUE) {

   
    $_SESSION['msg']="Expense Added";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense Added");
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  } else {
   $_SESSION['msg1']="Something Wrong";
   header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  }
}




// edit Expense
if($_POST['EditpadiMainExpense']) {
      $file = $_FILES['expenses_photo']['tmp_name'];
      $_FILES["expenses_photo"]["type"];

      if(file_exists($file)) {
       $errors     = array();
       $maxsize    = 4097152;
       $extensionResume=array("xlsx","csv","pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
       $extId = pathinfo($_FILES['expenses_photo']['name'], PATHINFO_EXTENSION);

       if(($_FILES['expenses_photo']['size'] >= $maxsize) || ($_FILES["expenses_photo"]["size"] == 0)) {
        $_SESSION['msg1']='File too large. Must be less than 4 MB.';
        header("Location: ../viewBalancesheet");  
        exit();       
      }
      if(!in_array($extId, $extensionResume) && (!empty($_FILES["expenses_photo"]["type"]))) {
        $_SESSION['msg1']='Invalid  File. Only  JPG, PNG,doc & PDF are allowed.';
        header("Location: ../viewBalancesheet");
        exit();       
      }
      if(count($errors) === 0) {
        $image_Arr = $_FILES['expenses_photo'];   
        $temp = explode(".", $_FILES["expenses_photo"]["name"]);
        $expenses_photo = round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["expenses_photo"]["tmp_name"], "../../img/expenses/".$expenses_photo);
      } 
    } else{
      $expenses_photo=$expenses_photo_old;
    }
    $expenses_photo;

    $expDateAry = explode("-", $expenses_add_date);
    $exYear = $expDateAry[0];
    $exMonth = $expDateAry[1];
    $dateObj   = DateTime::createFromFormat('!m', $exMonth);
    $monthName = $dateObj->format('F'); 
    $expenses_created_date = $monthName.'-'.$exYear;

    $m->set_data('expenses_title',$expenses_title);
    $m->set_data('expenses_amount',$expenses_amount);
    $m->set_data('expenses_add_date',$expenses_add_date);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('expenses_photo',$expenses_photo);
    $m->set_data('expenses_created_date',$expenses_created_date);
    //IS_1686
    $m->set_data('amount_without_gst',$expenses_amount);
    $m->set_data('gst',$gst);
    $m->set_data('tax_slab',$tax_slab);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('taxble_type',$taxble_type);

    $m->set_data('expense_category_id',$expense_category_id);

    $gst_amount = (($expenses_amount * $tax_slab));

    if($gst_amount > 0 ){
      $gst_amount  = $gst_amount /100; 
    } else {
      $gst_amount  = 0 ;
    }
    if($gst=="1"){ 

      $gst_amount =  $expenses_amount *$tax_slab/100;  //only for Exclude
      $expenses_amount = $expenses_amount + $gst_amount;
    } else {
      $gst_amount =  $expenses_amount - ($expenses_amount * (100/(100+$tax_slab)));  //only for Include
      $expenses_amount = $expenses_amount;
      $m->set_data('amount_without_gst',($expenses_amount -$gst_amount ));
    }

  $m->set_data('expenses_amount',$expenses_amount);
  //IS_1686
  $a1 = array(
    'expenses_title'=>$m->get_data('expenses_title'),
    'expense_category_id'=>$m->get_data('expense_category_id'),
    'expenses_amount'=>$m->get_data('expenses_amount'),
    'expenses_add_date'=>$m->get_data('expenses_add_date'),
    'balancesheet_id'=>$m->get_data('balancesheet_id'),
    'expenses_photo'=>$m->get_data('expenses_photo'),
    'society_id'=>$society_id,
    'expenses_created_date'=>$m->get_data('expenses_created_date'),
    'received_by'=>$_COOKIE['bms_admin_id'],
    //IS_1686
        'gst'=>$m->get_data('gst'),
        'tax_slab'=>$m->get_data('tax_slab'), 
        'amount_without_gst'=>$m->get_data('amount_without_gst'), 
        'is_taxble'=>$m->get_data('is_taxble'), 
        'taxble_type'=>$m->get_data('taxble_type'), 
        'created_date'=>date("Y-m-d H:i:s"), 
       //IS_1686
  );
      // print_r($a1);
  $q = $d->update("expenses_balance_sheet_master",$a1,"expenses_balance_sheet_id='$expenses_balance_sheet_id'");

  if($q==TRUE) {

   

    
    $_SESSION['msg']="Expense Data Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense $expenses_title ($expenses_amount) Data Updated");
    header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  } else {
   $_SESSION['msg1']="Something Wrong";
   header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id&tab=tabe-14");
  }
}


// Add Income
if($_POST['addMainIncome']) {
      $file = $_FILES['expenses_photo']['tmp_name'];
      $_FILES["expenses_photo"]["type"];
      if(file_exists($file)) {
       $errors     = array();
       $maxsize    = 4097152;

       $extensionResume=array("xlsx","csv","pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
       $extId = pathinfo($_FILES['expenses_photo']['name'], PATHINFO_EXTENSION);


       if(($_FILES['expenses_photo']['size'] >= $maxsize) || ($_FILES["expenses_photo"]["size"] == 0)) {
              //IS_680
        $_SESSION['msg1']='File too large. Must be less than 4 MB.';
        header("Location: ../viewBalancesheet");    
        exit();       

      }
      if(!in_array($extId, $extensionResume) && (!empty($_FILES["expenses_photo"]["type"]))) {
        $_SESSION['msg1']='Invalid  File. Only  JPG, PNG,Doc & PDF are allowed.';
        header("Location: ../viewBalancesheet");
        exit();       


      }
      if(count($errors) === 0) {
        $image_Arr = $_FILES['expenses_photo'];   
        $temp = explode(".", $_FILES["expenses_photo"]["name"]);
        $expenses_photo = round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["expenses_photo"]["tmp_name"], "../../img/expenses/".$expenses_photo);
      } 
    } else{
      $expenses_photo="";
    }
    $expenses_photo;

    $expDateAry = explode("-", $expenses_add_date);
    $exYear = $expDateAry[0];
    $exMonth = $expDateAry[1];
    $dateObj   = DateTime::createFromFormat('!m', $exMonth);
    $monthName = $dateObj->format('F'); 
    $expenses_created_date = $monthName.'-'.$exYear;

    $m->set_data('expenses_title',$expenses_title);
    $m->set_data('income_amount',$income_amount);
     $m->set_data('expense_category_id',$expense_category_id);
    $m->set_data('expenses_add_date',$expenses_add_date);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('expenses_photo',$expenses_photo);
    $m->set_data('expenses_created_date',$expenses_created_date);
    $m->set_data('cash_on_hand',$cash_on_hand);


    //IS_1686
    $m->set_data('amount_without_gst',$income_amount);
    $m->set_data('gst',$gst);
    $m->set_data('tax_slab',$tax_slab);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('taxble_type',$taxble_type);
    $gst_amount = (($income_amount * $tax_slab));

    if($gst_amount > 0 ){
      $gst_amount  = $gst_amount /100; 
    } else {
      $gst_amount  = 0 ;
    }
    if($gst=="1"){ 

      $gst_amount =  $income_amount *$tax_slab/100;  //only for Exclude

      $income_amount = $income_amount + $gst_amount;
    } else {
       $gst_amount =  $income_amount - ($income_amount * (100/(100+$tax_slab)));  //only for Include
      $income_amount = $income_amount;
      $m->set_data('amount_without_gst',($income_amount -$gst_amount ));
    }

    $m->set_data('income_amount',$income_amount);
    //IS_1686


    $a1 = array(
      'expenses_title'=>$m->get_data('expenses_title'),
       'expense_category_id'=>$m->get_data('expense_category_id'),
      'income_amount'=>$m->get_data('income_amount'),
      'expenses_add_date'=>$m->get_data('expenses_add_date'),
      'balancesheet_id'=>$m->get_data('balancesheet_id'),
      'expenses_photo'=>$m->get_data('expenses_photo'),
      'society_id'=>$society_id,
      'expenses_created_date'=>$m->get_data('expenses_created_date'),
      'received_by'=>$_COOKIE['bms_admin_id'],
       //IS_1686
        'gst'=>$m->get_data('gst'),
        'tax_slab'=>$m->get_data('tax_slab'), 
        'amount_without_gst'=>$m->get_data('amount_without_gst'), 
        'is_taxble'=>$m->get_data('is_taxble'), 
        'taxble_type'=>$m->get_data('taxble_type'), 
       //IS_1686
        'created_date'=>date("Y-m-d H:i:s"), 
        'cash_on_hand'=>$m->get_data('cash_on_hand'), 
    );
        // print_r($a1);
    $q = $d->insert("expenses_balance_sheet_master",$a1);

    if($q==TRUE) {

      if ($cash_on_hand==1) {
      $_SESSION['msg']="Cash On Hand Amount Added";
      } else {
      $_SESSION['msg']="Income Added";
      }
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by",$_SESSION['msg']);
      header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
    }
}


// Add Income
if($_POST['EditIncome']) {
      $file = $_FILES['expenses_photo']['tmp_name'];
      $_FILES["expenses_photo"]["type"];
      if(file_exists($file)) {
       $errors     = array();
       $maxsize    = 4097152;

       $extensionResume=array("xlsx","csv","pdf","doc","docx","png","jpg","jpeg","PNG","JPG","JPEG");
       $extId = pathinfo($_FILES['expenses_photo']['name'], PATHINFO_EXTENSION);


       if(($_FILES['expenses_photo']['size'] >= $maxsize) || ($_FILES["expenses_photo"]["size"] == 0)) {
              //IS_680
        $_SESSION['msg1']='File too large. Must be less than 4 MB.';
        header("Location: ../viewBalancesheet");    
        exit();       

      }
      if(!in_array($extId, $extensionResume) && (!empty($_FILES["expenses_photo"]["type"]))) {
        $_SESSION['msg1']='Invalid  File. Only  JPG, PNG,Doc & PDF are allowed.';
        header("Location: ../viewBalancesheet");
        exit();       


      }
      if(count($errors) === 0) {
        $image_Arr = $_FILES['expenses_photo'];   
        $temp = explode(".", $_FILES["expenses_photo"]["name"]);
        $expenses_photo = round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["expenses_photo"]["tmp_name"], "../../img/expenses/".$expenses_photo);
      } 
    } else{
      $expenses_photo=$expenses_photo_old;
    }
    $expenses_photo;

    $expDateAry = explode("-", $expenses_add_date);
    $exYear = $expDateAry[0];
    $exMonth = $expDateAry[1];
    $dateObj   = DateTime::createFromFormat('!m', $exMonth);
    $monthName = $dateObj->format('F'); 
    $expenses_created_date = $monthName.'-'.$exYear;

    $m->set_data('expense_category_id',$expense_category_id);
    $m->set_data('expenses_title',$expenses_title);
    $m->set_data('income_amount',$income_amount);
    $m->set_data('expenses_add_date',$expenses_add_date);
    $m->set_data('balancesheet_id',$balancesheet_id);
    $m->set_data('expenses_photo',$expenses_photo);
    $m->set_data('expenses_created_date',$expenses_created_date);
    $m->set_data('cash_on_hand',$cash_on_hand);

    //IS_1686
    $m->set_data('amount_without_gst',$income_amount);
    $m->set_data('gst',$gst);
    $m->set_data('tax_slab',$tax_slab);
    $m->set_data('is_taxble',$is_taxble);
    $m->set_data('taxble_type',$taxble_type);
    $gst_amount = (($income_amount * $tax_slab));

    if($gst_amount > 0 ){
      $gst_amount  = $gst_amount /100; 
    } else {
      $gst_amount  = 0 ;
    }
    if($gst=="1"){ 

      $gst_amount =  $income_amount *$tax_slab/100;  //only for Exclude

      $income_amount = $income_amount + $gst_amount;
    } else {
       $gst_amount =  $income_amount - ($income_amount * (100/(100+$tax_slab)));  //only for Include
      $income_amount = $income_amount;
      $m->set_data('amount_without_gst',($income_amount -$gst_amount ));
    }

    $m->set_data('income_amount',$income_amount);
    //IS_1686


    $a1 = array(
      'expenses_title'=>$m->get_data('expenses_title'),
      'expense_category_id'=>$m->get_data('expense_category_id'),
      'income_amount'=>$m->get_data('income_amount'),
      'expenses_add_date'=>$m->get_data('expenses_add_date'),
      'balancesheet_id'=>$m->get_data('balancesheet_id'),
      'expenses_photo'=>$m->get_data('expenses_photo'),
      'society_id'=>$society_id,
      'expenses_created_date'=>$m->get_data('expenses_created_date'),
      'received_by'=>$_COOKIE['bms_admin_id'],
       //IS_1686
        'gst'=>$m->get_data('gst'),
        'tax_slab'=>$m->get_data('tax_slab'), 
        'amount_without_gst'=>$m->get_data('amount_without_gst'), 
        'is_taxble'=>$m->get_data('is_taxble'), 
        'taxble_type'=>$m->get_data('taxble_type'), 
       //IS_1686
        'created_date'=>date("Y-m-d H:i:s"), 
        'cash_on_hand'=>$m->get_data('cash_on_hand'), 
    );
        // print_r($a1);
    $q = $d->update("expenses_balance_sheet_master",$a1,"expenses_balance_sheet_id='$expenses_balance_sheet_id'");

    if($q==TRUE) {

      
      $_SESSION['msg']="Income Data Updated";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Income $expenses_title ($income_amount) Data Updated");
      header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
    } else {
     $_SESSION['msg1']="Something Wrong";
     header("Location: ../viewBalancesheet?balancesheet_id=$balancesheet_id");
    }
}

// Add Income
if($_POST['addBalaceFile']) {
  $file = $_FILES['file_name']['tmp_name'];
  $_FILES["file_name"]["type"];
  if(file_exists($file)) {
   $errors     = array();
   $maxsize    = 35097152;
   $acceptable = array(
            // 'application/pdf',

    'application/pdf'
  );
   if(($_FILES['file_name']['size'] >= $maxsize) || ($_FILES["file_name"]["size"] == 0)) {
    $_SESSION['msg1']='File too large. Must be less than 35 MB.';
    header("Location: ../balancesheetsFiles");    
    exit();       

  }
  if(!in_array($_FILES['file_name']['type'], $acceptable) && (!empty($_FILES["file_name"]["type"]))) {
    $_SESSION['msg1']='Invalid  File, Only PDF are allowed.';
    header("Location: ../balancesheetsFiles");
    exit();       
  }
  if(count($errors) === 0) {
    $image_Arr = $_FILES['file_name'];   
    $temp = explode(".", $_FILES["file_name"]["name"]);
    $balancesheet_name = str_replace("/", "_", $balancesheet_name);
    $file_name = $balancesheet_name.round(microtime(true)) . '.' . end($temp);
    move_uploaded_file($_FILES["file_name"]["tmp_name"], "../../img/balancesheet/".$file_name);
  } 
} 


$m->set_data('society_id',$society_id);
$m->set_data('balancesheet_name',$balancesheet_name);
$m->set_data('file_name',$file_name);
$m->set_data('created_by',$_COOKIE['bms_admin_id']);
$m->set_data('created_date',date("d M Y"));

$a1 = array(
  'society_id'=>$m->get_data('society_id'),
  'balancesheet_name'=>$m->get_data('balancesheet_name'),
  'file_name'=>$m->get_data('file_name'),
  'created_date'=>$m->get_data('created_date'),
  'created_by'=>$m->get_data('created_by'),
);
    // print_r($a1);
$q = $d->insert("balancesheet_pdf_master",$a1);

if($q==TRUE) {
         

          $title='New Balancesheet Added';
          $description= "Added By Admin ".$_COOKIE['admin_name'];

         $d->insertUserNotification($society_id,$title,$description,"Balancesheet","BalanceSheetNew.png","");

         $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android'  ");
         $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' ");
         $nResident->noti("BalancesheetFragment","",$society_id,$fcmArray,"New Balancesheet Added","Added By Admin ".$_COOKIE['admin_name'],'Balancesheet');
         $nResident->noti_ios("BalanceSheetVc","",$society_id,$fcmArrayIos,"New Balancesheet Added","Added By Admin ".$_COOKIE['admin_name'],'Balancesheet');


      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$balancesheet_name Balancesheet File Added");
     

  $_SESSION['msg']="Balancesheet Added";
  header("Location: ../balancesheetsFiles");
} else {
 $_SESSION['msg1']="Something Wrong";
 header("Location: ../balancesheetsFiles");
}
}
?>