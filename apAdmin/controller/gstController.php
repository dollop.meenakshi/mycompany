<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


  
  
if(isset($addGST)) {

  $m->set_data('slab_percentage',$slab_percentage);
  $m->set_data('description',$description);   
  $a1= array (
    'slab_percentage'=> $m->get_data('slab_percentage'),
    'description'=> $m->get_data('description') 
  );
 
  $q=$d->insert("gst_master",$a1);

  if($q==TRUE) {
    $_SESSION['msg']="TAX Slab Added";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","TAX Slab Added");
    header("Location: ../gstSlab");
  } else {
    header("Location: ../gstSlab");
  }
}


if(isset($editGST)) {
  $m->set_data('slab_percentage',$slab_percentage);
  $m->set_data('description',$description);   
  $a1= array (
    'slab_percentage'=> $m->get_data('slab_percentage'),
    'description'=> $m->get_data('description') 
  );


  $q=$d->update("gst_master",$a1,"slab_id='$slab_id'");
  if($q==TRUE) {

    $_SESSION['msg']="Tax Slab Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Tax Slab Updated");
    header("Location: ../gstSlab");
  } else {
    header("Location: ../gstSlab");
  }
}
 

if(isset($updateGSTNumber)) {
  $m->set_data('gst_no',$gst_no);
  $m->set_data('pan_number',$pan_number);   
  $a1= array (
    'gst_no'=> $m->get_data('gst_no'),
    'pan_number'=> $m->get_data('pan_number') 
  );


  $q=$d->update("society_master",$a1,"society_id='$society_id'");
  if($q==TRUE) {

    $_SESSION['msg']="Tax Number Updated";
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Tax Number Updated");
    header("Location: ../gstSlab");
  } else {
    header("Location: ../gstSlab");
  }
}

 
?>