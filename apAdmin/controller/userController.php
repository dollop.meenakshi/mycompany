<?php
include '../common/objectController.php';

if (isset($_POST) && !empty($_POST))
{
    error_reporting(0);
    extract(array_map("test_input", $_POST));

    if (isset($_POST['viewLocationLog'])) {

        $uq = $d->select("users_master,attendance_master,shift_timing_master","shift_timing_master.shift_time_id=users_master.shift_time_id AND attendance_master.user_id=users_master.user_id AND attendance_master.attendance_date_end='0000-00-00' AND users_master.user_id='$uId' AND users_master.track_user_location=1","ORDER BY attendance_master.attendance_id DESC LIMIT 1");
        $lastUserData= mysqli_fetch_array($uq);
        $user_token = $lastUserData['user_token'];
        $device = $lastUserData['device'];
        $phone_model = $lastUserData['phone_model'].'-'.$lastUserData['phone_brand'];
        $app_version_code = $lastUserData['app_version_code'];
        $mobile_os_version = $lastUserData['mobile_os_version'];
        $attendance_id = $lastUserData['attendance_id'];
        $last_tracking_latitude = $lastUserData['last_tracking_latitude'];
        $last_tracking_longitude = $lastUserData['last_tracking_longitude'];
        $last_tracking_address = $lastUserData['last_tracking_address'];
        $last_tracking_location_time = $lastUserData['last_tracking_location_time'];
        $last_tracking_location_date = date("Y-m-d",strtotime($lastUserData['last_tracking_location_time']));
        $last_tracking_area = $lastUserData['last_tracking_area'];
        $last_tracking_locality = $lastUserData['last_tracking_locality'];
        $last_tracking_battery_status = $lastUserData['last_tracking_battery_status'];
        $last_tracking_gps_accuracy = $lastUserData['last_tracking_gps_accuracy'];
        $is_multiple_punch_in = $lastUserData['is_multiple_punch_in'];

        if ($device=='android' && $user_token!="" && $attendance_id>0) {

            if ($is_multiple_punch_in == 0) { 

                $attStartDate = $lastUserData["attendance_date_start"];
                $punchInTime = $lastUserData["punch_in_time"];
                $totalWorkingMinutes = $lastUserData['total_working_minutes'];
                $lastPunchInDate = $lastUserData['last_punch_in_date'];
                $lastPunchInTime = $lastUserData['last_punch_in_time']; 
                $tHours = $d->getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));
                $tm = explode(':', $tHours.":00");
                $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                $tMin = $totalWorkingMinutes + $tMinutes;
            }else{
                $multiplePunchDataArray = json_decode($lastUserData['multiple_punch_in_out_data'],true);
                $isMultiOut = false;
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray) ; $i++) {
                        if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){ 
                            $isMultiOut = true;
                            $attStartDate = $multiplePunchDataArray[$i]["punch_in_date"];
                            $punchInTime = $multiplePunchDataArray[$i]["punch_in_time"];
                            break;
                        }else{
                            $attStartDate = $lastUserData["attendance_date_start"];
                            $punchInTime = $lastUserData["punch_in_time"];
                        }
                    }  
                }else{
                    $isMultiOut = true;
                    $attStartDate = $lastUserData["attendance_date_start"];
                    $punchInTime = $lastUserData["punch_in_time"];
                }
                if ($isMultiOut == true) {
                    $totalWorkingMinutes = $lastUserData['total_working_minutes'];
                    $lastPunchInDate = $lastUserData['last_punch_in_date'];
                    $lastPunchInTime = $lastUserData['last_punch_in_time']; 
                    $tHours = $d->getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));
                    $tm = explode(':', $tHours.":00");
                    $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                    $tMin = $totalWorkingMinutes + $tMinutes;
                }else{
                    $tMin = 0;
                }
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) <= 0) {
                    $attStartDate = $lastUserData["attendance_date_start"];
                    $punchInTime = $lastUserData["punch_in_time"];
                    $totalWorkingMinutes = $lastUserData['total_working_minutes'];
                    $m->set_data('last_punch_in_date',$attStartDate);
                    $m->set_data('last_punch_in_time',$punchInTime);
                    $tHours = $d->getTotalHours($attStartDate,date('Y-m-d'),$punchInTime,date('H:i:s'));
                    $tm = explode(':', $tHours.":00");
                    $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                    $tMin = $totalWorkingMinutes + $tMinutes;
                }
            }

            if ($tMin == 0) {
                $pDate = $attStartDate." ".$punchInTime;
                $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));
                $date_a = new DateTime($inTimeAMPM);
                $date_b = new DateTime($dateTimeCurrent);
                $interval = $date_a->diff($date_b);
                $day = $interval->format('%days');
                if ($day > 0) {
                    $hour = $day * 24;
                }
                $hours = $hour + $interval->format('%h');
                $minutes = $interval->format('%i');
                $sec = $interval->format('%s');
                /*if ($sec < 45) {
                   $sec += 5;
                }*/
            }else{
                $hours  = floor($tMin/60);
                $minutes = $tMin % 60;
            }
            
            $finalTime =  $attStartDate." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);
            $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

        
            
            $nResident->noti("current_location","",$society_id,$user_token,"Location Reload","Updated on ".date("d M Y, h:i A"),"$time_diff");
            sleep(3);
        }

        if ($mapView=="mapView") {
            header("location:../trackDetailUser?viewPlayBack=yes&bId=$bId&dId=$dId&uId=$uId");
        } else {
            header("location:../trackDetailUser?bId=$bId&dId=$dId&uId=$uId");
        }

        exit;
    }

    if (isset($_POST['user_first_name_add'])) {

        $user_mobile = (int)$user_mobile;
        if (strlen($user_mobile) < 8) {
            $_SESSION['msg1'] = "Invalid Mobile Number";
            header("location:../companyEmployees?bId=$block_id");
            exit;
        }

        $qf = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile' ");
        if (mysqli_num_rows($qf) > 0) {
            $_SESSION['msg1'] = "Mobile Number Already Register !";
            header("location:../companyEmployees?bId=$block_id");
            exit;
        }

        // add new unit first

        // $dataCount = count($_POST['block_name']);
        $file = $_FILES['user_profile_pic']['tmp_name'];
        if (file_exists($file)) {
            $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['user_profile_pic']['size'] >= $maxsize) || ($_FILES["user_profile_pic"]["size"] == 0)) {
                $_SESSION['msg1'] = "Profile photo too large. Must be less than 2 MB.";
                header("location:../companyEmployees?bId=$block_id");
                exit();
            }
            if (!in_array($_FILES['user_profile_pic']['type'], $acceptable) && (!empty($_FILES["user_profile_pic"]["type"]))) {
                $_SESSION['msg1'] = "Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../companyEmployees?bId=$block_id");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['user_profile_pic'];
                $temp = explode(".", $_FILES["user_profile_pic"]["name"]);
                $user_profile_pic = $user_first_name_add . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["user_profile_pic"]["tmp_name"], "../../img/users/recident_profile/" . $user_profile_pic);
            }
        } else {
            $user_profile_pic = "user.png";
        }
        $get_sort = $d->selectRow("user_sort", "users_master", "floor_id = '$floor_id' AND block_id = '$block_id'", "ORDER BY user_sort DESC LIMIT 1");
        $sort = $get_sort->fetch_assoc();
        $sort_value = $sort['user_sort'] + 1;
        $societyName = $_COOKIE['society_name'];

        $m->set_data('society_id', $society_id);
        $m->set_data('sister_company_id', $sister_company_id);
        $m->set_data('shift_time_id', $shift_time_id);
        $m->set_data('user_profile_pic', $user_profile_pic);
        $m->set_data('user_full_name', ucfirst($user_first_name_add) . " " . ucfirst($user_last_name));
        $m->set_data('user_first_name_add', $user_first_name_add);
        $m->set_data('user_last_name', $user_last_name);
        $m->set_data('gender', $gender);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('user_email', $user_email);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('last_address', $last_address);
        $m->set_data('owner_name', $owner_first_name . " " . $owner_last_name);
        $m->set_data('owner_mobile', $owner_mobile);
        $m->set_data('user_token', $user_token);
        $m->set_data('last_address', $last_address);
        $m->set_data('tenant_doc', $tenant_doc);
        $m->set_data('country_code', $country_code);
        $m->set_data('user_type', 0);
        $m->set_data('unit_status', $unit_status);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('sub_department_id', $sub_department_id);
        $m->set_data('zone_id', $zone_id);
        $m->set_data('level_id', $level_id);
        $m->set_data('user_status', 1);
        $m->set_data('user_sort', $sort_value);
        $m->set_data('company_name', $company_name);
        $m->set_data('company_address', $company_address);
        $m->set_data('designation', $designation);
        $m->set_data('joining_date', $joining_date);
        /* $m->set_data('user_latitude',$user_latitude);
        $m->set_data('user_longitude',$user_longitude); */
        $m->set_data('member_date_of_birth', $member_date_of_birth);

       
        $au = array(
            'society_id' =>  $m->get_data('society_id'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'unit_name' => $unit_name,
        );

        $qunit = $d->insert("unit_master", $au);
        $unit_id = $con->insert_id;
        $m->set_data('unit_id', $unit_id);
        $m->set_data('register_date', date("Y-m-d H:i:s"));

        $a = array(
            'society_id' => $m->get_data('society_id'),
            'sister_company_id' => $m->get_data('sister_company_id'),
            'shift_time_id' => $m->get_data('shift_time_id'),
            'user_profile_pic' => $m->get_data('user_profile_pic'),
            'user_full_name' => $m->get_data('user_full_name'),
            'user_first_name' => $m->get_data('user_first_name_add'),
            'user_last_name' => $m->get_data('user_last_name'),
            'gender' => $m->get_data('gender'),
            'user_mobile' => $m->get_data('user_mobile'),
            'user_email' => $m->get_data('user_email'),
            'last_address' => $m->get_data('last_address'),
            'user_type' => $m->get_data('user_type'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'sub_department_id' => $m->get_data('sub_department_id'),
            'zone_id' => $m->get_data('zone_id'),
            'level_id' => $m->get_data('level_id'),
            'unit_id' => $m->get_data('unit_id'),
            'user_status' => $m->get_data('user_status'),
            'user_sort' => $m->get_data('user_sort'),
            'country_code' => $m->get_data('country_code'),
            'register_date' => $m->get_data('register_date'),
            'user_designation' => $m->get_data('designation'),
            'register_date' => $m->get_data('register_date'),
            /* 'user_latitude'=> $m->get_data('user_latitude'),
      'user_longitude'=> $m->get_data('user_longitude'), */
            'member_date_of_birth' => $m->get_data('member_date_of_birth'),
        );

        $q = $d->insert("users_master", $a);
        $user_id = $con->insert_id;
        $m->set_data('user_id', $user_id);

        if ($q > 0) {

            /*$user_sort_arr = [
        'user_sort' => $m->get_data('user_id')
      ];
      $d->update("users_master",$user_sort_arr,"user_id='$user_id'");*/

            $a22 = array(
                'unit_status' => $unit_status
            );

            $d->update("unit_master", $a22, "unit_id='$unit_id'");
            $_SESSION['msg'] = "User Added";

            $compArray = array(
                'user_id' => $m->get_data('user_id'),
                'society_id' => $m->get_data('society_id'),
                'designation' => $m->get_data('designation'),
                'unit_id' => $m->get_data('unit_id'),
                'joining_date' => $m->get_data('joining_date'),
            );
            $d->insert("user_employment_details", $compArray);

            $smsObj->send_welcome_message($society_id, $user_mobile, $user_first_name_add, $society_name, $country_code);
            $d->add_sms_log($user_mobile, "User Welcome Message", $society_id, $country_code, 4);

            $forgotLink = "https://my-company.app/";
            $forgotLink1 = "";
            $user_name = $user_first_name_add;
            $to = $user_email;
            $subject = "Account Created for $society_name - MyCo";
            if ($to != '') {
                include '../mail/welcomeUserMail.php';
                include '../mail.php';
            }
            // send email for username & Password to user 
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "New User ($user_first_name_add $user_last_name) Added");
            header("location:../companyEmployees?bId=$block_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../companyEmployees?bId=$block_id");
        }
    }

    /* if (isset($_POST['edit_user_id'])) {

        $user_mobile = (int)$user_mobile;
        if (strlen($user_mobile) < 8) {
            $_SESSION['msg1'] = "Invalid Mobile Number";
            header("location:../companyEmployees?bId=$block_id");
            exit;
        }

        $qf = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile' AND user_id!='$edit_user_id'");
        if (mysqli_num_rows($qf) > 0) {
            $_SESSION['msg1'] = "Mobile Number Already Register !";
            header("location:../companyEmployees?id=$edit_user_id");
            exit;
        }


        // $dataCount = count($_POST['block_name']);
        $file = $_FILES['user_profile_pic']['tmp_name'];
        if (file_exists($file)) {
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['user_profile_pic']['size'] >= $maxsize) || ($_FILES["user_profile_pic"]["size"] == 0)) {
                $_SESSION['msg1'] = "Profile photo too large. Must be less than 1 MB.";
                header("location:../employeeDetails?id=$edit_user_id");
                exit();
            }
            if (!in_array($_FILES['user_profile_pic']['type'], $acceptable) && (!empty($_FILES["user_profile_pic"]["type"]))) {
                $_SESSION['msg1'] = "Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employeeDetails?id=$edit_user_id");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['user_profile_pic'];
                $temp = explode(".", $_FILES["user_profile_pic"]["name"]);
                $user_profile_pic = $user_first_name_add . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["user_profile_pic"]["tmp_name"], "../../img/users/recident_profile/" . $user_profile_pic);
            }
        } else {
            $user_profile_pic = $user_profile_pic_old;
        }


        //IS_3241
        if ($user_type == 1) {
            $file11 = $_FILES["tenant_doc"]["tmp_name"];
            if (file_exists($file11)) {
                $temp = explode(".", $_FILES["tenant_doc"]["name"]);
                $tenant_doc = "Tenant_" . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["tenant_doc"]["tmp_name"], "../../img/documents/" . $tenant_doc);
            } else {
                $tenant_doc = $tenant_doc_old;
            }

            $file22 = $_FILES["prv_doc"]["tmp_name"];
            if (file_exists($file22)) {
                $temp = explode(".", $_FILES["prv_doc"]["name"]);
                $prv_doc = "Tenant_Pvr" . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["prv_doc"]["tmp_name"], "../../img/documents/" . $prv_doc);
            } else {
                $prv_doc = $prv_doc_old;
            }


            $m->set_data('tenant_doc', $tenant_doc);


            if (isset($tenant_agreement_start_date) && $tenant_agreement_start_date != "") {
                $tenant_agreement_start_date = date('Y-m-d', strtotime($tenant_agreement_start_date));
            } else {
                $tenant_agreement_start_date = NULL;
            }

            if (isset($tenant_agreement_end_date) && $tenant_agreement_end_date != "") {
                $tenant_agreement_end_date = date('Y-m-d', strtotime($tenant_agreement_end_date));
            } else {
                $tenant_agreement_end_date = NULL;
            }
            $m->set_data('tenant_agreement_start_date', $tenant_agreement_start_date);
            $m->set_data('tenant_agreement_end_date', $tenant_agreement_end_date);
            $m->set_data('prv_doc', $prv_doc);

            $update_tenant_arr = array(
                'tenant_doc' => $m->get_data('tenant_doc'),
                'tenant_agreement_start_date' => $m->get_data('tenant_agreement_start_date'),
                'tenant_agreement_end_date' => $m->get_data('tenant_agreement_end_date'),
                'prv_doc' => $m->get_data('prv_doc')
            );

            $q_tenant = $d->update("users_master", $update_tenant_arr, "user_id='$edit_user_id'");
        }
        $filesss = $_FILES["resume_cv_doc"]["tmp_name"];
        if (file_exists($filesss)) {
            $temp = explode(".", $_FILES["resume_cv_doc"]["name"]);
            $resume_cv_doc = "resume_cv_doc" . round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["resume_cv_doc"]["tmp_name"], "../../img/documents/" . $resume_cv_doc);
        } else {
            $resume_cv_doc = $resume_cv_doc_old;
        }

        $m->set_data('resume_cv_doc', $resume_cv_doc);

        //IS_3241
        $nationality = ($nationality_drop == "other") ? $nationality : "indian";

        $m->set_data('user_profile_pic', $user_profile_pic);
        $m->set_data('user_password', $user_password);
        $m->set_data('resume_cv_doc', $resume_cv_doc);
        $m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
        $m->set_data('user_first_name_add', $user_first_name);
        $m->set_data('user_last_name', $user_last_name);
        $m->set_data('user_middle_name', $user_middle_name);
        $m->set_data('gender', $gender);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('emergency_number', $emergency_number);
        $m->set_data('whatsapp_number', $whatsapp_number);
        $m->set_data('user_email', $user_email);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('alt_mobile', $alt_mobile);
        $m->set_data('member_date_of_birth', $member_date_of_birth);
        $m->set_data('member_access_denied', $member_access_denied);
        $m->set_data('chat_access_denied', $chat_access_denied);
        $m->set_data('timline_access_denied', $timline_access_denied);
        $m->set_data('sub_department_id', $sub_department_id);
        $m->set_data('zone_id', $zone_id);
        $m->set_data('level_id', $level_id);
        $m->set_data('blood_group', $blood_group);
        $m->set_data('country_code', $country_code);
        $m->set_data('country_code_emergency', $country_code_emergency);
        $m->set_data('country_code_whatsapp', $country_code_whatsapp);
        $m->set_data('country_code_alt', $country_code_alt);
        $m->set_data('is_demo', $is_demo);
        $m->set_data('permanent_address', $permanent_address);
        $m->set_data('marital_status', $marital_status);
        $m->set_data('wedding_anniversary_date', $wedding_anniversary_date);
        $m->set_data('total_family_members', $total_family_members);
        $m->set_data('nationality', $nationality);
        $m->set_data('last_address', $last_address);
        $m->set_data('company_employee_id', $company_employee_id);
        $m->set_data('public_mobile', $public_mobile);
        $m->set_data('facebook', $facebook);
        $m->set_data('linkedin', $linkedin);
        $m->set_data('twitter', $twitter);
        $m->set_data('personal_email', $personal_email);
        $m->set_data('instagram', $instagram);
        $m->set_data('intrest_hobbies', $intrest_hobbies);
        $m->set_data('professional_skills', $professional_skills);
        $m->set_data('special_skills', $special_skills);
        $m->set_data('language_known', $language_known);
        $a = array(
            'user_profile_pic' => $m->get_data('user_profile_pic'),
            'user_password' => $m->get_data('user_password'),
            'user_full_name' => $m->get_data('user_full_name'),
            'user_first_name' => $m->get_data('user_first_name_add'),
            'user_last_name' => $m->get_data('user_last_name'),
            'user_middle_name' => $m->get_data('user_middle_name'),
            'gender' => $m->get_data('gender'),
            'user_mobile' => $m->get_data('user_mobile'),
            'resume_cv_doc' => $m->get_data('resume_cv_doc'),
            'emergency_number' => $m->get_data('emergency_number'),
            'whatsapp_number' => $m->get_data('whatsapp_number'),
            'user_email' => $m->get_data('user_email'),
            'alt_mobile' => $m->get_data('alt_mobile'),
            'country_code_alt' => $m->get_data('country_code_alt'),
            'member_date_of_birth' => $m->get_data('member_date_of_birth'),
            'sub_department_id' => $m->get_data('sub_department_id'),
            'zone_id' => $m->get_data('zone_id'),
            'level_id' => $m->get_data('level_id'),
            'blood_group' => $m->get_data('blood_group'),
            'country_code' => $m->get_data('country_code'),
            'country_code_emergency' => $m->get_data('country_code_emergency'),
            'country_code_whatsapp' => $m->get_data('country_code_whatsapp'),
            'is_demo' => $m->get_data('is_demo'),
            'last_address' => $m->get_data('last_address'),
            'permanent_address' => $m->get_data('permanent_address'),
            'marital_status' => $m->get_data('marital_status'),
            'wedding_anniversary_date' => $m->get_data('wedding_anniversary_date'),
            'total_family_members' => $m->get_data('total_family_members'),
            'nationality' => $m->get_data('nationality'),
            'company_employee_id' => $m->get_data('company_employee_id'),
            'public_mobile' => $m->get_data('public_mobile'),
            'facebook' => $m->get_data('facebook'),
            'linkedin' => $m->get_data('linkedin'),
            'twitter' => $m->get_data('twitter'),
            'personal_email' => $m->get_data('personal_email'),
            'instagram' => $m->get_data('instagram'),
            'modify_date' => date("Y-m-d H:i:s")
        );
        if ($marital_status != 2) {
            $a['wedding_anniversary_date'] = "";
        }
        $a11 = array(
            'member_access_denied' => $m->get_data('member_access_denied'),
            'chat_access_denied' => $m->get_data('chat_access_denied'),
            'timline_access_denied' => $m->get_data('timline_access_denied'),
            'unit_name' => $m->get_data('company_employee_id'),
        );
        $a13 = array(
            'intrest_hobbies' => $m->get_data('intrest_hobbies'),
            'professional_skills' => $m->get_data('professional_skills'),
            'special_skills' => $m->get_data('special_skills'),
            'language_known' => $m->get_data('language_known'),
        );


        $q = $d->update("users_master", $a, "user_id='$edit_user_id'");
        $q = $d->update("user_employment_details", $a13, "user_id='$edit_user_id'");
        $d->update("unit_master", $a11, "unit_id='$unit_id'");

        if ($q > 0) {
            if ($user_mobile != $user_mobile_old || $country_code_get != $country_code) {
                $nResident->noti("", "", $society_id, $user_fcm, "logout", "mobile number changed by admin", 'logout');
                $aWallet = array(
                    'user_mobile' => $user_mobile,
                );

                $d->update("user_wallet_master", $aWallet, "user_mobile='$user_mobile_old' AND user_mobile!='' AND user_mobile!='0'");
                $new_arr = [
                    'user_token' => ''
                ];
                $d->update("users_master", $new_arr, "user_id='$edit_user_id'");
            }


            $_SESSION['msg'] = "User Data Updated.";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User ($user_first_name) Data Updated");
            // if($user_mobile!=$user_mobile_old || $country_code_get != $country_code){
            //   header("location:../logout.php");
            // }else{
            header("location:../employeeDetails?id=$edit_user_id");
            //}
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../employeeDetails?id=$edit_user_id");
        }
    } */

    if (isset($_POST['contact_detail']) && $_POST['contact_detail'] == 'contact_detail') {
        $user_mobile = (int)$user_mobile;
        if (strlen($user_mobile) < 8) {
            $_SESSION['msg1'] = "Invalid Mobile Number";
            header("location:../companyEmployees?bId=$block_id");
            exit;
        }

        $qf = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile' AND user_id!='$edit_user_id'");
        if (mysqli_num_rows($qf) > 0) {
            $_SESSION['msg1'] = "Mobile Number Already Register !";
            header("location:../companyEmployees?id=$edit_user_id");
            exit;
        }

        $oldDataQ = $d->select("users_master", "user_id='$edit_user_id'");
        $oldData = $oldDataQ->fetch_assoc();
        $oldDataArr = array(
            'country_code' => $oldData['country_code'],
            'user_mobile' => $oldData['user_mobile'],
            'country_code_emergency' => $oldData['country_code_emergency'],
            'emergency_number' => $oldData['emergency_number'],
            'country_code_alt' => $oldData['country_code_alt'],
            'alt_mobile' => $oldData['alt_mobile'],
            'country_code_whatsapp' => $oldData['country_code_whatsapp'],
            'whatsapp_number' => $oldData['whatsapp_number'],
            'last_address' => $oldData['last_address'],
            'permanent_address' => $oldData['permanent_address'],
            'user_email' => $oldData['user_email'],
            'personal_email' => $oldData['personal_email'],
            'public_mobile' => $oldData['public_mobile'],
        );

        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('emergency_number', $emergency_number);
        $m->set_data('user_email', $user_email);
        $m->set_data('alt_mobile', $alt_mobile);
        $m->set_data('whatsapp_number', $whatsapp_number);
        $m->set_data('country_code_whatsapp', $country_code_whatsapp);
        $m->set_data('country_code', $country_code);
        $m->set_data('country_code_emergency', $country_code_emergency);
        $m->set_data('country_code_whatsapp', $country_code_whatsapp);
        $m->set_data('country_code_alt', $country_code_alt);
        $m->set_data('permanent_address', $permanent_address);
        $m->set_data('last_address', $last_address);
        $m->set_data('public_mobile', $public_mobile);
        $m->set_data('personal_email', $personal_email);

        $a = array(
            'user_mobile' => $m->get_data('user_mobile'),
            'emergency_number' => $m->get_data('emergency_number'),
            'user_email' => $m->get_data('user_email'),
            'alt_mobile' => $m->get_data('alt_mobile'),
            'whatsapp_number' => $m->get_data('whatsapp_number'),
            'country_code_whatsapp' => $m->get_data('country_code_whatsapp'),
            'country_code_alt' => $m->get_data('country_code_alt'),
            'country_code' => $m->get_data('country_code'),
            'country_code_emergency' => $m->get_data('country_code_emergency'),
            'last_address' => $m->get_data('last_address'),
            'permanent_address' => $m->get_data('permanent_address'),
            'public_mobile' => $m->get_data('public_mobile'),
            'personal_email' => $m->get_data('personal_email'),
            'modify_date' => date("Y-m-d H:i:s")
        );

        $q = $d->update("users_master", $a, "user_id='$edit_user_id'");

        $newDataArr = array(
            'country_code' => $m->get_data('country_code'),
            'user_mobile' => $m->get_data('user_mobile'),
            'country_code_emergency' => $m->get_data('country_code_emergency'),
            'emergency_number' => $m->get_data('emergency_number'),
            'country_code_alt' => $m->get_data('country_code_alt'),
            'alt_mobile' => $m->get_data('alt_mobile'),
            'country_code_whatsapp' => $m->get_data('country_code_whatsapp'),
            'whatsapp_number' => $m->get_data('whatsapp_number'),
            'last_address' => $m->get_data('last_address'),
            'permanent_address' => $m->get_data('permanent_address'),
            'user_email' => $m->get_data('user_email'),
            'personal_email' => $m->get_data('personal_email'),
            'public_mobile' => $m->get_data('public_mobile'),
        );

        $historyArr = array(
            'user_id' => $edit_user_id,
            'old_data' => json_encode($oldDataArr),
            'new_data' => json_encode($newDataArr),
            'user_details_menu_type' => '0',
            'created_date' => date('Y-m-d H:i:s'),
        );
        $q = $d->insert("user_details_change_history_master", $historyArr);

        if ($q > 0) {
            if ($user_mobile != $user_mobile_old || $country_code_get != $country_code) {
                $nResident->noti("", "", $society_id, $user_fcm, "logout", "mobile number changed by admin", 'logout');
                $aWallet = array(
                    'user_mobile' => $user_mobile,
                );

                $d->update("user_wallet_master", $aWallet, "user_mobile='$user_mobile_old' AND user_mobile!='' AND user_mobile!='0'");
                $new_arr = [
                    'user_token' => ''
                ];
                $d->update("users_master", $new_arr, "user_id='$edit_user_id'");
            }
            $_SESSION['msg'] = "User Data Updated.";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User ($user_first_name) Data Updated");
            // if($user_mobile!=$user_mobile_old || $country_code_get != $country_code){
            //   header("location:../logout.php");
            // }else{
            header("location:../employeeDetails?id=$edit_user_id");
            //}
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../employeeDetails?id=$edit_user_id");
        }
    }

    if (isset($_POST['personal_info']) && $_POST['personal_info'] == 'personal_info') {
        $file = $_FILES['user_profile_pic']['tmp_name'];
        if (file_exists($file)) {
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['user_profile_pic']['size'] >= $maxsize) || ($_FILES["user_profile_pic"]["size"] == 0)) {
                $_SESSION['msg1'] = "Profile photo too large. Must be less than 1 MB.";
                header("location:../employeeDetails?id=$edit_user_id");
                exit();
            }
            if (!in_array($_FILES['user_profile_pic']['type'], $acceptable) && (!empty($_FILES["user_profile_pic"]["type"]))) {
                $_SESSION['msg1'] = "Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employeeDetails?id=$edit_user_id");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['user_profile_pic'];
                $temp = explode(".", $_FILES["user_profile_pic"]["name"]);
                $user_profile_pic = $user_first_name . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["user_profile_pic"]["tmp_name"], "../../img/users/recident_profile/" . $user_profile_pic);
            }
        } else {
            $user_profile_pic = $user_profile_pic_old;
        }
        //IS_3241
        if ($user_type == 1) {
            $file11 = $_FILES["tenant_doc"]["tmp_name"];
            if (file_exists($file11)) {
                $temp = explode(".", $_FILES["tenant_doc"]["name"]);
                $tenant_doc = "Tenant_" . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["tenant_doc"]["tmp_name"], "../../img/documents/" . $tenant_doc);
            } else {
                $tenant_doc = $tenant_doc_old;
            }

            $file22 = $_FILES["prv_doc"]["tmp_name"];
            if (file_exists($file22)) {
                $temp = explode(".", $_FILES["prv_doc"]["name"]);
                $prv_doc = "Tenant_Pvr" . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["prv_doc"]["tmp_name"], "../../img/documents/" . $prv_doc);
            } else {
                $prv_doc = $prv_doc_old;
            }

            $m->set_data('tenant_doc', $tenant_doc);

            if (isset($tenant_agreement_start_date) && $tenant_agreement_start_date != "") {
                $tenant_agreement_start_date = date('Y-m-d', strtotime($tenant_agreement_start_date));
            } else {
                $tenant_agreement_start_date = NULL;
            }

            if (isset($tenant_agreement_end_date) && $tenant_agreement_end_date != "") {
                $tenant_agreement_end_date = date('Y-m-d', strtotime($tenant_agreement_end_date));
            } else {
                $tenant_agreement_end_date = NULL;
            }
            $m->set_data('tenant_agreement_start_date', $tenant_agreement_start_date);
            $m->set_data('tenant_agreement_end_date', $tenant_agreement_end_date);
            $m->set_data('prv_doc', $prv_doc);

            $update_tenant_arr = array(
                'tenant_doc' => $m->get_data('tenant_doc'),
                'tenant_agreement_start_date' => $m->get_data('tenant_agreement_start_date'),
                'tenant_agreement_end_date' => $m->get_data('tenant_agreement_end_date'),
                'prv_doc' => $m->get_data('prv_doc')
            );

            $q_tenant = $d->update("users_master", $update_tenant_arr, "user_id='$edit_user_id'");
        }
        $filesss = $_FILES["resume_cv_doc"]["tmp_name"];
        if (file_exists($filesss)) {
            $temp = explode(".", $_FILES["resume_cv_doc"]["name"]);
            $resume_cv_doc = "resume_cv_doc" . round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["resume_cv_doc"]["tmp_name"], "../../img/documents/" . $resume_cv_doc);
        } else {
            $resume_cv_doc = $resume_cv_doc_old;
        }

        $m->set_data('resume_cv_doc', $resume_cv_doc);

        $oldDataQ = $d->select("users_master", "user_id='$edit_user_id'");
        $oldData = $oldDataQ->fetch_assoc();
        $skillsOldDataQ = $d->select("user_employment_details", "user_id='$edit_user_id'");
        $skillsOldData = $skillsOldDataQ->fetch_assoc();
        $oldDataArr = array(
            'user_first_name' => $oldData['user_first_name'],
            'user_middle_name' =>  $oldData['user_middle_name'],
            'user_last_name' =>  $oldData['user_last_name'],
            'member_date_of_birth' =>  $oldData['member_date_of_birth'],
            'blood_group' => $oldData['blood_group'],
            'marital_status' =>  $oldData['marital_status'],
            'wedding_anniversary_date' =>  $oldData['wedding_anniversary_date'],
            'total_family_members' =>  $oldData['total_family_members'],
            'nationality' =>  $oldData['nationality'],
            'gender' =>  $oldData['gender'],
            'intrest_hobbies' =>  $skillsOldData['intrest_hobbies'],
            'professional_skills' =>  $skillsOldData['professional_skills'],
            'language_known' =>  $skillsOldData['language_known'],
            'special_skills' =>  $skillsOldData['special_skills'],
            'user_profile_pic' =>  $oldData['user_profile_pic'],
            'resume_cv_doc' =>  $oldData['resume_cv_doc'],
        );

        //IS_3241
        $nationality = ($nationality_drop == "other") ? $nationality : "indian";

        $m->set_data('user_profile_pic', $user_profile_pic);
        // $m->set_data('user_password',$user_password);
        $m->set_data('resume_cv_doc', $resume_cv_doc);
        $m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
        $m->set_data('user_first_name_add', $user_first_name);
        $m->set_data('user_last_name', $user_last_name);
        $m->set_data('user_middle_name', $user_middle_name);
        $m->set_data('gender', $gender);
        $m->set_data('member_date_of_birth', $member_date_of_birth);

        // $m->set_data('member_access_denied',$member_access_denied);
        // $m->set_data('chat_access_denied',$chat_access_denied);
        // $m->set_data('timline_access_denied',$timline_access_denied);
        // $m->set_data('sub_department_id',$sub_department_id);
        // $m->set_data('zone_id',$zone_id);
        // $m->set_data('level_id',$level_id);
        if ($wedding_anniversary_date!="") {
            $marital_status = 2;
        }

        $m->set_data('blood_group', $blood_group);
        $m->set_data('is_demo', $is_demo);
        $m->set_data('marital_status', $marital_status);
        $m->set_data('wedding_anniversary_date', $wedding_anniversary_date);
        $m->set_data('total_family_members', $total_family_members);
        $m->set_data('nationality', $nationality);
        $m->set_data('intrest_hobbies', $intrest_hobbies);
        $m->set_data('professional_skills', $professional_skills);
        $m->set_data('special_skills', $special_skills);
        $m->set_data('language_known', $language_known);
        $a = array(
            'user_profile_pic' => $m->get_data('user_profile_pic'),
             'user_full_name' => $m->get_data('user_full_name'),
            'user_first_name' => $m->get_data('user_first_name_add'),
            'user_last_name' => $m->get_data('user_last_name'),
            'user_middle_name' => $m->get_data('user_middle_name'),
            'gender' => $m->get_data('gender'),
            'resume_cv_doc' => $m->get_data('resume_cv_doc'),
            'member_date_of_birth' => $m->get_data('member_date_of_birth'),
            'blood_group' => $m->get_data('blood_group'),
            'is_demo' => $m->get_data('is_demo'),
            'marital_status' => $m->get_data('marital_status'),
            'wedding_anniversary_date' => $m->get_data('wedding_anniversary_date'),
            'total_family_members' => $m->get_data('total_family_members'),
            'nationality' => $m->get_data('nationality'),
            'modify_date' => date("Y-m-d H:i:s")
        );
        
       
        $a13 = array(
            'intrest_hobbies' => $m->get_data('intrest_hobbies'),
            'professional_skills' => $m->get_data('professional_skills'),
            'special_skills' => $m->get_data('special_skills'),
            'language_known' => $m->get_data('language_known'),
        );


        $q = $d->update("users_master", $a, "user_id='$edit_user_id'");
        $q = $d->update("user_employment_details", $a13, "user_id='$edit_user_id'");
        
        $newDataArr = array(
            'user_first_name' => $m->get_data('user_first_name_add'),
            'user_middle_name' => $m->get_data('user_middle_name'),
            'user_last_name' => $m->get_data('user_last_name'),
            'member_date_of_birth' => $m->get_data('member_date_of_birth'),
            'blood_group' => $m->get_data('blood_group'),
            'marital_status' => $m->get_data('marital_status'),
            'wedding_anniversary_date' => $m->get_data('wedding_anniversary_date'),
            'total_family_members' => $m->get_data('total_family_members'),
            'nationality' => $m->get_data('nationality'),
            'gender' => $m->get_data('gender'),
            'intrest_hobbies' => $m->get_data('intrest_hobbies'),
            'professional_skills' => $m->get_data('professional_skills'),
            'language_known' => $m->get_data('language_known'),
            'special_skills' => $m->get_data('special_skills'),
            'user_profile_pic' => $m->get_data('user_profile_pic'),
            'resume_cv_doc' => $m->get_data('resume_cv_doc'),
        );

        $historyArr = array(
            'user_id' => $edit_user_id,
            'old_data' => json_encode($oldDataArr),
            'new_data' => json_encode($newDataArr),
            'user_details_menu_type' => '1',
            'created_date' => date('Y-m-d H:i:s'),
        );
        $q = $d->insert("user_details_change_history_master", $historyArr);

        if ($q > 0) {
            $_SESSION['msg'] = "User Data Updated...";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User ($user_first_name) Data Updated");
            // if($user_mobile!=$user_mobile_old || $country_code_get != $country_code){
            //   header("location:../logout.php");
            // }else{
            header("location:../employeeDetails?id=$edit_user_id");
            //}
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../employeeDetails?id=$edit_user_id");
        }
    }

    if (isset($_POST['social_links']) && $_POST['social_links'] == 'social_links') {

        $oldDataQ = $d->select("users_master", "user_id='$edit_user_id'");
        $oldData = $oldDataQ->fetch_assoc();
        $oldDataArr = array(
            'facebook' => $oldData['facebook'],
            'linkedin' => $oldData['linkedin'],
            'twitter' => $oldData['twitter'],
            'instagram' => $oldData['instagram'],
        );

        $user_first_name = $oldData['facebook'];

        $m->set_data('facebook', $facebook);
        $m->set_data('linkedin', $linkedin);
        $m->set_data('twitter', $twitter);
        $m->set_data('instagram', $instagram);
        $a = array(

            'facebook' => $m->get_data('facebook'),
            'linkedin' => $m->get_data('linkedin'),
            'twitter' => $m->get_data('twitter'),
            'instagram' => $m->get_data('instagram'),
            'modify_date' => date("Y-m-d H:i:s")
        );

        $q = $d->update("users_master", $a, "user_id='$edit_user_id'");

        $newDataArr = array(
            'facebook' => $m->get_data('facebook'),
            'linkedin' => $m->get_data('linkedin'),
            'twitter' => $m->get_data('twitter'),
            'instagram' => $m->get_data('instagram'),
        );

        $historyArr = array(
            'user_id' => $edit_user_id,
            'old_data' => json_encode($oldDataArr),
            'new_data' => json_encode($newDataArr),
            'user_details_menu_type' => '3',
            'created_date' => date('Y-m-d H:i:s'),
        );
        $q = $d->insert("user_details_change_history_master", $historyArr);

        if ($q > 0) {
            $_SESSION['msg'] = "User Data Updated.";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User ($user_first_name) Data Updated");
            // if($user_mobile!=$user_mobile_old || $country_code_get != $country_code){
            //   header("location:../logout.php");
            // }else{
             header("location:../employeeDetails?id=$edit_user_id");
            //}
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../employeeDetails?id=$edit_user_id");
        }
    }


    if (isset($delete_user_id)) {
        /*  */
        $count7 = $d->sum_data("penalty_master.penalty_amount", "penalty_master,users_master", "penalty_master.user_id=users_master.user_id AND penalty_master.paid_status=0 AND penalty_master.user_id='$delete_user_id'");
        $row3 = mysqli_fetch_array($count7);
        $totalPenalyy = $row3['SUM(penalty_master.penalty_amount)'];
        $asif1331 = $totalMain + $asif11 + $totalPenalyy;
        $totalUnpadiDue =   number_format($asif1331, 2, '.', '');

        $LoanSql = $d->selectRow("employee_loan_master.*,(SELECT COUNT(*) FROM loan_emi_master WHERE loan_id = employee_loan_master.loan_id AND is_emi_paid = 0 AND user_id = $delete_user_id) AS emi_count,(SELECT COUNT(*) FROM advance_salary WHERE  is_paid = 0 AND user_id = $delete_user_id) AS advance_salary_count", "employee_loan_master", "employee_loan_master.user_id=$delete_user_id");
        $getLoan = mysqli_fetch_assoc($LoanSql);
        if (isset($getLoan['emi_count']) && $getLoan['emi_count'] > 0) {
            $emi_count = $getLoan['emi_count'];
        } else {
            $emi_count = 0;
        }
        if (isset($getLoan['advance_salary_count']) && $getLoan['advance_salary_count'] > 0) {
            $advance_salary_count = $getLoan['advance_salary_count'];
        } else {
            $advance_salary_count = 0;
        }

        if ($totalUnpadiDue == 0 && $emi_count == 0 && $advance_salary_count == 0) {

            if ($assets_count == "0") {

                $gu = $d->select("users_master", "user_id='$delete_user_id' AND society_id='$society_id'");
                $userData = mysqli_fetch_array($gu);
                $user_id = $userData['user_id'];
                $user_fcm = $userData['user_token'];

                $device = $userData['device'];
                if ($device == 'android') {
                    $nResident->noti("", "", $society_id, $user_fcm, "Logout", "Marked as Ex Employee", 'approved');
                } else if ($device == 'ios') {
                    $nResident->noti_ios("", "", $society_id, $user_fcm, "Logout", "Marked as Ex Employee", 'approved');
                }

                $q = $d->update("users_master", array("delete_status" => 1, 'deleted_date' => date('Y-m-d')), "user_id='$delete_user_id' AND society_id='$society_id'");
                $q = $d->update("unit_master", array("unit_delete_status" => 1), "unit_id='$unit_id' AND society_id='$society_id'");


                if ($q > 0) {

                    $_SESSION['msg'] = "Employee Removed Successfully";
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$unit_name Marked as Ex Employee");

                    $fcmArray = $d->get_android_fcm("face_app_device_master", " user_token!='' AND society_id='$society_id' AND device_type='0' ");
                    $fcmArrayIos = $d->get_android_fcm("face_app_device_master", " user_token!='' AND society_id='$society_id' AND device_type='1' ");
                    $title = "sync_data";
                    $description = "Face Data Updated on " . date('d-M-y h:i A');
                    $nResident->noti("", '', $society_id, $fcmArray, $title, $description, "");
                    $nResident->noti_ios("", '', $society_id, $fcmArrayIos, $title, $description, "");

                    header("location:../companyEmployees?bId=$block_id");
                } else {
                    $_SESSION['msg1'] = "Something Wrong";
                    header("location:../companyEmployees?bId=$block_id");
                }
            } else {
                $_SESSION['msg1'] = "Please Take over all assets first";
                header("location:../employeeDetails?id=$delete_user_id");
            }
        } else {
            $_SESSION['msg1'] = "Please Pay All Due Amount, Then Mark As Ex Employee";
            header("location:../employeeDetails?id=$delete_user_id");
        }
    }

    if (isset($approve_user_id)) {

        $qf = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile' AND user_id!='$approve_user_id'");
        if (mysqli_num_rows($qf) > 0) {
            $_SESSION['msg1'] = "Mobile Number Already Register !";
            header("location:../pendingUser?id=$approve_user_id");
            exit;
        }


        $unit_status = 1;


        $a22 = array(
            'unit_status' => $unit_status
        );

        $a33 = array(
            'user_status' => 1,
            'register_date' => date("Y-m-d H:i:s"),
        );

        $q = $d->update("unit_master", $a22, "unit_id='$unit_id'");
        $d->update("users_master", $a33, "unit_id='$unit_id' AND  user_id='$approve_user_id'");

        if ($q > 0) {
            if ($unit_status == 3 && $owner_id != 0) {
                $d->update("users_master", $a33, "unit_id='$unit_id' AND  user_id='$owner_id'");
                $smsObj->send_approval_message($society_id, $owner_mobile, $owner_name, $owner_country_code);
                $d->add_sms_log($owner_mobile, "User Approval Confirmation Message", $society_id, $owner_country_code, 1);
            }

            $d->delete("users_master", "unit_id='$unit_id' AND user_id!='$approve_user_id' AND parent_id!='$approve_user_id' AND user_status=0");

            $q = $d->select("users_master", "society_id='$society_id' AND unit_id='$unit_id' AND member_status=0");

            while ($data_notification = mysqli_fetch_array($q)) {
                $name = $data_notification['user_full_name'];
                $mobile = $data_notification['user_mobile'];
                $token = $data_notification['user_token'];
                $device = $data_notification['device'];


                $notiAry = array(
                    'society_id' => $society_id,
                    'user_id' => $data_notification['user_id'],
                    'notification_title' => 'Employee Account is Approved',
                    'notification_desc' => "for $unit_name by Admin $created_by",
                    'notification_date' => date('Y-m-d H:i'),
                    'notification_action' => '',
                    'notification_logo' => 'Members_1xxxhdpi.png'
                );
                $d->insert("user_notification", $notiAry);
                if ($device == 'android') {
                    $nResident->noti("", "", $society_id, $token, "Employee Account is Approved", "for $unit_name by Admin $created_by", 'approved');
                } else if ($device == 'ios') {
                    $nResident->noti_ios("", "", $society_id, $token, "Employee Account is Approved", "for $unit_name by Admin $created_by", 'approved');
                }
            }


            $societyName = $_COOKIE['society_name'];

            $smsObj->send_approval_message($society_id, $user_mobile, $name, $country_code);
            $d->add_sms_log($user_mobile, "User Approval Confirmation Message", $society_id, $country_code, 1);


            $_SESSION['msg'] = "Employee Approved Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee $user_full_name - $unit_name Registration Request Approved");
            header("location:../pendingUser?pendingUser=yes");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pendingUser?pendingUser=yes");
        }
    }




    if (isset($approveUserId)) {


        $a22 = array(
            'unit_status' => 1
        );


        $a33 = array(
            'user_status' => 1,
            'register_date' => date("Y-m-d H:i:s"),
        );

        $q = $d->update("unit_master", $a22, "unit_id='$unit_id'");
        $d->update("users_master", $a33, "unit_id='$unit_id' AND user_id='$user_idApprove'");

        if ($q > 0) {
            $_SESSION['msg'] = "User Approved";

            $d->delete("users_master", "unit_id='$unit_id' AND user_id!='$user_idApprove'");
            $qUserToken = $d->select("users_master", "society_id='$society_id' AND unit_id='$unit_id' AND user_id='$user_idApprove'");
            $data_notification = mysqli_fetch_array($qUserToken);
            $sos_user_token = $data_notification['user_token'];
            $device = $data_notification['device'];
            if ($device == 'android') {
                $nResident->noti("", "", $society_id, $sos_user_token, "Your Account is Approved", "Login Now", 'approved');
            } else if ($device == 'ios') {
                $nResident->noti_ios("", "", $society_id, $sos_user_token, "Your Account is Approved", "Login Now", 'approved');
            }

            $notiAry = array(
                'society_id' => $society_id,
                'user_id' => $data_notification['user_id'],
                'notification_title' => 'Your Account is Approved',
                'notification_desc' => "Login Now",
                'notification_date' => date('Y-m-d H:i'),
                'notification_action' => '',
                'notification_logo' => 'Members_1xxxhdpi.png'
            );
            $d->insert("user_notification", $notiAry);

            $societyName = $_COOKIE['society_name'];

            $smsObj->send_approval_message($society_id, $user_mobile, $user_full_name, $country_code);
            $d->add_sms_log($user_mobile, "User Approval Confirmation Message", $society_id, $country_code, 1);



            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee $user_full_name - $unit_name Registration Request Approved");
            header("location:../pendingUser?pendingUser=yes");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pendingUser?pendingUser=yes");
        }
    }


    if (isset($rejectUserId)) {


        $a22 = array(
            'unit_status' => 0
        );

        $q = $d->delete("users_master", "user_id='$user_id' AND unit_id='$unit_id'");
        $q = $d->delete("users_master", "parent_id='$user_id' AND unit_id='$unit_id'");
        $d->delete("user_employment_details", "user_id='$user_id' AND unit_id='$unit_id'");

        $countOhterUser = $d->count_data_direct("user_id", "users_master", "society_id='$society_id' AND unit_id='$unit_id' AND user_id!='$user_id'");
        if ($countOhterUser == 0) {
            $d->delete("unit_master", "unit_id='$unit_id'");
        }

        if ($q > 0) {
            $_SESSION['msg'] = "Employee Rejected";

            if ($device == 'android') {
                $nResident->noti("", "", $society_id, $user_token, "Your Registration Request Is Rejected", "By Admin $created_by", 'approved');
            } else if ($device == 'ios') {
                $nResident->noti_ios("", "", $society_id, $user_token, "Your Registration Request is Rejected", "By Admin $created_by", 'approved');
            }



            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee $user_full_name - $unit_name Registration Request Rejected");

            header("location:../pendingUser?pendingUser=yes");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../pendingUser?pendingUser=yes");
        }
    }



    ///////add new experience
    if (isset($_POST['add_letest_experience'])) {
        $sql = $d->selectRow('employee_history.*', "employee_history", "employee_history.user_id=$user_id", "ORDER BY employee_history.history_id  DESC");
        $last_data = mysqli_fetch_assoc($sql);
        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('joining_date', $joining_date);
        $m->set_data('designation', $designation);
        // $m->set_data('end_date', $end_date);
        $m->set_data('salary_incrment', $salary_incrment);
        $m->set_data('remark', $remark);
        $m->set_data('created_date', date("Y-m-d H:i:s"));
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'user_id' => $m->get_data('user_id'),
            'joining_date' => $m->get_data('joining_date'),
            // 'end_date' => $m->get_data('end_date'),
            'designation' => $m->get_data('designation'),
            'remark' => $m->get_data('remark'),
            'salary_incrment' => $m->get_data('salary_incrment'),
            'created_date' => $m->get_data('created_date'),
        );
        $q = $d->insert("employee_history", $a1);
        if ($q == true) {

            if ($last_data) {
                $qq = $d->update('employee_history', array('end_date' => $a1['joining_date']), "history_id='$last_data[history_id]'");
                $a2 = array(
                    'user_id' => $user_id,
                    'society_id' => $society_id,
                    'exp_company_name' => $_COOKIE['society_name'],
                    'designation' => $last_data['designation'],
                    'work_from' => $last_data['joining_date'],
                    'work_to' => $a1['joining_date'],
                    'company_location' => $_COOKIE['city_name'],
                    'add_date' => date('Y-m-d H:i:s')
                );
                $q = $d->insert("employee_experience", $a2);
            }
            header("Location: ../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../employeeDetails?id=$user_id");
        }
    }
    if (isset($_POST['add_promotions'])) {
        $sql = $d->selectRow('employee_history.*', "employee_history", "employee_history.user_id=$user_id", "ORDER BY employee_history.history_id  DESC");
        $last_data = mysqli_fetch_assoc($sql);
        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('joining_date', $joining_date);
        $m->set_data('designation', $designation);
        //$m->set_data('end_date', $end_date);
        $m->set_data('salary_incrment', $salary_incrment);
        $m->set_data('remark', $remark);
        $m->set_data('created_date', date("Y-m-d H:i:s"));


        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'user_id' => $m->get_data('user_id'),
            'joining_date' => $m->get_data('joining_date'),
            // 'end_date' => $m->get_data('end_date'),
            'designation' => $m->get_data('designation'),
            'remark' => $m->get_data('remark'),
            'salary_incrment' => $m->get_data('salary_incrment'),
            'created_date' => $m->get_data('created_date'),
        );
        $q = $d->insert("employee_history", $a1);
        if ($q == true) {

            if ($last_data) {
                $qq = $d->update('employee_history', array('end_date' => $a1['joining_date']), "history_id='$last_data[history_id]'");
                $a2 = array(
                    'user_id' => $user_id,
                    'society_id' => $society_id,
                    'exp_company_name' => $_COOKIE['society_name'],
                    'designation' => $last_data['designation'],
                    'work_from' => $last_data['joining_date'],
                    'work_to' => $a1['joining_date'],
                    'company_location' => $_COOKIE['city_name'],
                    'add_date' => date('Y-m-d H:i:s')
                );
                $q = $d->insert("employee_experience", $a2);
            }
            header("Location: ../employeepromotion?bId=$block_id&dId=$floor_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../employeepromotion?bId=$block_id&dId=$floor_id");
        }
    }
    //////////////////////////Update Professional Start//////////////////
    if (isset($employment_description) || (isset($updateProfessional) && $updateProfessional == "updateProfessional")) {
        $oldDataQ = $d->select("user_employment_details", "user_id='$prof_user_id'");
        $oldEmpDataQ = $d->select("users_master", "user_id='$prof_user_id'");
        $oldData = $oldDataQ->fetch_assoc();
        $oldEmpData = $oldEmpDataQ->fetch_assoc();
        $oldDataArr = array(
            'designation' => $oldData['designation'],
            'employment_type' => $oldData['employment_type'],
            'company_employee_id' => $oldEmpData['company_employee_id'],
            'joining_date' => $oldData['joining_date'],
            'block_id' => $oldEmpData['block_id'],
            'floor_id' => $oldEmpData['floor_id'],
            'sub_department_id' =>$oldEmpData['sub_department_id'],
            'shift_time_id' => $oldEmpData['shift_time_id'],
            'zone_id' => $oldEmpData['zone_id'],
            'level_id' => $oldEmpData['level_id'],
        );

        $m->set_data('user_id', $prof_user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('sister_company_id', $sister_company_id);
        $m->set_data('unit_id', $unit_id);
        $m->set_data('employment_description', $employment_description);
        $m->set_data('employment_type', $employment_type);
        $m->set_data('company_employee_id', $company_employee_id);
        $m->set_data('joining_date', $joining_date);
        $m->set_data('total_experience', $total_experience);
        $m->set_data('shift_timing', $shift_timing);
        $m->set_data('company_name', $company_name);
        $m->set_data('designation', $designation);
        $m->set_data('company_address', $company_address);
        // $m->set_data('visiting_card',$visiting_card);
        $m->set_data('shift_time_id', $shift_time_id);
        $m->set_data('sub_department_id', $sub_department_id);
        $m->set_data('zone_id', $zone_id);
        $m->set_data('level_id', $level_id);
        $m->set_data('member_access_denied', $member_access_denied);
        $m->set_data('chat_access_denied', $chat_access_denied);
        $m->set_data('timline_access_denied', $timline_access_denied);
        // $m->set_data('user_visiting_card',$user_visiting_card);

        $a = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $m->get_data('society_id'),
            'unit_id' => $m->get_data('unit_id'),
            'employment_description' => $m->get_data('employment_description'),
            'employment_type' => $m->get_data('employment_type'),
            'joining_date' => $m->get_data('joining_date'),
            'total_experience' => $m->get_data('total_experience'),
            'shift_timing' => $m->get_data('shift_timing'),
            'designation' => $m->get_data('designation'),

        );

        $a1 = array(
            'sister_company_id' => $m->get_data('sister_company_id'),
            'user_designation' => $m->get_data('designation'),
            'shift_time_id' => $m->get_data('shift_time_id'),
            'company_employee_id' => $m->get_data('company_employee_id'),
            'sub_department_id' => $m->get_data('sub_department_id'),
            'zone_id' => $m->get_data('zone_id'),
            'level_id' => $m->get_data('level_id'),
            'member_access_denied' => $m->get_data('member_access_denied'),
            'chat_access_denied' => $m->get_data('chat_access_denied'),
            'timline_access_denied' => $m->get_data('timline_access_denied'),
            // 'user_visiting_card'=> $m->get_data('user_visiting_card'),
        );


        $q1 = $d->select("user_employment_details", "user_id='$prof_user_id'");
        $q2 = $d->update("users_master", $a1, "user_id='$prof_user_id'");
        

        if (mysqli_num_rows($q1) > 0) {
            $q = $d->update("user_employment_details", $a, "user_id='$prof_user_id'");
        } else {
            $q = $d->insert("user_employment_details", $a);
        }

        $newDataArr = array(
            'designation' => $m->get_data('designation'),
            'employment_type' => $m->get_data('employment_type'),
            'company_employee_id' => $m->get_data('company_employee_id'),
            'joining_date' => $m->get_data('joining_date'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'sub_department_id' =>$m->get_data('sub_department_id'),
            'shift_time_id' => $m->get_data('shift_time_id'),
            'zone_id' => $m->get_data('zone_id'),
            'level_id' => $m->get_data('level_id'),
        );


        $historyArr = array(
            'user_id' => $prof_user_id,
            'old_data' => json_encode($oldDataArr),
            'new_data' => json_encode($newDataArr),
            'user_details_menu_type' => '2',
            'created_date' => date('Y-m-d H:i:s'),
        );
        $q = $d->insert("user_details_change_history_master", $historyArr);



        if ($q > 0) {
            if ($hidden_block != $block_id || $hidden_floor_id != $floor_id) {
                $m->set_data('block_id', $block_id);
                $m->set_data('floor_id', $floor_id);
                $m->set_data('sub_department_id', $sub_department_id);
                $m->set_data('shift_time_id', $shift_time_id);

                $a1 = array(
                    'block_id' => $m->get_data('block_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'sub_department_id' => $m->get_data('sub_department_id'),
                    'shift_time_id' => $m->get_data('shift_time_id'),
                );

                $a2 = array(
                    'block_id' => $m->get_data('block_id'),
                    'floor_id' => $m->get_data('floor_id'),
                );

                $a3 = array(
                    'floor_id' => $m->get_data('floor_id'),
                );

                $discussion = array(
                    'discussion_block_id' => $m->get_data('block_id'),
                    'discussion_forum_for' => $m->get_data('floor_id'),
                );

                $q1 = $d->update("app_access_master", $a2, "access_for_id = '$prof_user_id'");
                $q2 = $d->update("assets_detail_inventory_master", $a3, "user_id = '$prof_user_id'");
                $q3 = $d->update("hr_document_master", $a3, "user_id = '$prof_user_id'");
                $q4 = $d->update("idea_master", $a3, "user_id = '$prof_user_id'");
                $q5 = $d->update("parking_master", $a2, "unit_id = '$unit_id'");
                $q6 = $d->update("salary", $a2, "user_id = '$prof_user_id'");
                $q7 = $d->update("unit_master", $a2, "unit_id = '$unit_id'");
                $q8 = $d->update("users_master", $a1, "user_id = '$prof_user_id'");
                $q9 = $d->update("user_bank_master", $a3, "user_id = '$prof_user_id'");
                $q10 = $d->update("user_expenses", $a3, "user_id = '$prof_user_id'");
                $q11 = $d->update("visitors_master", $a2, "user_id = '$prof_user_id'");
                $q12 = $d->update("work_report_master", $a3, "user_id = '$prof_user_id'");
                $q13 = $d->update("wfh_master", $a3, "user_id = '$prof_user_id'");
                $q14 = $d->update("discussion_forum_master", $discussion, "user_id = '$prof_user_id'");
                $q15 = $d->update("salary", $a2, "user_id = '$prof_user_id'");
                $q16 = $d->update("salary_slip_master", $a2, "user_id = '$prof_user_id'");
                $q16 = $d->update("advance_salary", $a2, "user_id = '$prof_user_id'");
            }

            if ($level_id > 0) {
                $performanceAssignQuery = $d->select("performance_dimensional_assign", "find_in_set('$level_id', level_ids) AND (find_in_set('$block_id', block_ids) OR block_ids = 0) AND (find_in_set('$floor_id', floor_ids) OR floor_ids =0)");

                while ($performanceAssignData = mysqli_fetch_array($performanceAssignQuery)) {

                    $d->delete("pms_schedule_master", "dimensional_id='$performanceAssignData[dimensional_id]' AND user_id='$prof_user_id' AND schedule_status=0");

                    if ($performanceAssignData['pms_type'] == 0) {
                        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                        $day = $performanceAssignData['day'];
                        $to_date = date('Y-m-d');
                        $schedule_date = $weekly_date = date('Y-m-d', strtotime($days[$day], strtotime($to_date)));
                    } elseif ($performanceAssignData['pms_type'] == 1) {
                        $month = date('F Y');
                        $month_days = explode(',', $performanceAssignData['day']);
                        $month_day = $day . ' ' . $month;
                        if (date('Y-m-d', strtotime($month_day)) < $currentDate) {
                            $month_day = date('Y-m-d', strtotime($month_day . ' + 1 months'));
                        }
                        $schedule_date = $month_day;
                    } elseif ($performanceAssignData['pms_type'] == 2) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $performanceAssignData['day'];
                        if ($currentDate <= date('Y-m-d', strtotime($date))) {
                            $monthNum  = $performanceAssignData['start_month'];
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $quarterly_date = $day . ' ' . $monthName;
                        } else {
                            $newDate = date('m', strtotime($date . ' + 3 months'));
                            $monthNum  = $newDate;
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $quarterly_date = $day . ' ' . $monthName;
                        }

                        if (date('Y-m-d', strtotime($quarterly_date)) < $currentDate) {
                            $quarterly_date = date('Y-m-d', strtotime($quarterly_date . ' + 3 months'));
                            while (date('Y-m-d', strtotime($quarterly_date)) < $currentDate) {
                                $quarterly_date = date('Y-m-d', strtotime($quarterly_date . ' + 3 months'));
                            }
                        }
                        $schedule_date = $quarterly_date;
                    } elseif ($performanceAssignData['pms_type'] == 3) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $day;
                        if ($currentDate <= date('Y-m-d', strtotime($date))) {
                            $monthNum  = $performanceAssignData['start_month'];
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $half_year_date = $day . ' ' . $monthName;
                        } else {
                            $newDate = date('m', strtotime($date . ' + 6 months'));
                            $monthNum  = $newDate;
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $half_year_date = $day . ' ' . $monthName;
                        }

                        if (date('Y-m-d', strtotime($half_year_date)) < $currentDate) {
                            $half_year_date = date('Y-m-d', strtotime($half_year_date . ' + 6 months'));
                            while (date('Y-m-d', strtotime($half_year_date)) < $currentDate) {
                                $half_year_date = date('Y-m-d', strtotime($half_year_date . ' + 6 months'));
                            }
                        }
                        $schedule_date = $half_year_date;
                    } elseif ($performanceAssignData['pms_type'] == 4) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $day;
                        $monthNum  = $performanceAssignData['start_month'];
                        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                        $monthName = $dateObj->format('F') . ' ' . date('Y');
                        $year_date = $day . ' ' . $monthName;

                        if (date('Y-m-d', strtotime($year_date)) < $currentDate) {
                            $year_date = date('Y-m-d', strtotime($year_date . ' + 1 year'));
                        }
                        $schedule_date = $year_date;
                    }

                    $m->set_data('society_id', $society_id);
                    $m->set_data('user_id', $prof_user_id);
                    $m->set_data('schedule_date', $schedule_date);
                    $m->set_data('dimensional_id', $performanceAssignData['dimensional_id']);
                    $m->set_data('schedule_created_date', date("Y-m-d H:i:s"));

                    $a2 = array(
                        'society_id' => $m->get_data('society_id'),
                        'user_id' => $m->get_data('user_id'),
                        'schedule_date' => $m->get_data('schedule_date'),
                        'dimensional_id' => $m->get_data('dimensional_id'),
                        'schedule_created_date' => $m->get_data('schedule_created_date'),
                    );
                    $q1 = $d->insert("pms_schedule_master", $a2);
                }
            }

            $_SESSION['msg'] = "User Data Updated";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User ($user_full_name) Data Updated");
            header("location:../employeeDetails?id=$prof_user_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("location:../employeeDetails?id=$prof_user_id");
        }
    }
    ///////////////////////////Update Professional End///////////////////

    if (isset($logoutDevice)) {

        if ($device == 'android') {
            $nResident->noti("", "", $society_id, $user_token, "Logout", "Logout", '');
        } else if ($device == 'ios') {
            $nResident->noti_ios("", "", $society_id, $user_token, "Logout", "Logout", '');
        }

        $m->set_data('user_token', '');

        $a = array(
            'user_token' => $m->get_data('user_token')
        );

        $qdelete = $d->update("users_master", $a, "user_id='$user_id'");


        $_SESSION['msg'] = "User logout successfully";
        header("location:../employeeDetails?id=$user_id");
        exit();
    }


    if (isset($addComercialUser)) {

        $qq = $d->select("users_entry_login", "login_mobile='$phone' AND unit_id='$unit_id'");
        $oldData = mysqli_fetch_array($qq);
        if ($oldData > 0) {
            $_SESSION['msg1'] = "Already Registered";
            header("location:../employeeDetails?id=$user_id");
            exit();
        }


        $m->set_data("name", $name);
        $m->set_data("unit_id", $unit_id);
        $m->set_data("society_id", $society_id);
        $m->set_data("phone", $phone);
        $m->set_data("password", $password);
        $m->set_data("login_created_date", date('Y-m-d H:i:s'));

        $entry = array(
            'login_name' => $m->get_data("name"),
            'unit_id' => $m->get_data("unit_id"),
            'society_id' => $m->get_data("society_id"),
            'login_mobile' => $m->get_data("phone"),
            'login_password' => $m->get_data("password"),
            'login_created_date' => $m->get_data("login_created_date"),
        );
        $query = $d->insert("users_entry_login", $entry);
        if ($query === true) {
            $_SESSION['msg'] = "Successfully Added";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }


    if (isset($addNote)) {
        $note_title = html_entity_decode($note_title);
        $note_description =  html_entity_decode($note_description);
        $m->set_data('society_id', $society_id);
        $m->set_data('user_id', $user_id);
        $m->set_data('note_title', $note_title);
        $m->set_data('note_description', $note_description);
        $m->set_data('share_with_admin', 1);
        $m->set_data('created_date', date("Y-m-d H:i:s"));
        $m->set_data('admin_id', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'user_id' => $m->get_data('user_id'),
            'note_title' => $m->get_data('note_title'),
            'note_description' => $m->get_data('note_description'),
            'share_with_admin' => $m->get_data('share_with_admin'),
            'created_date' => $m->get_data('created_date'),
            'admin_id' => $m->get_data('admin_id'),
        );


        $query = $d->insert("user_notes", $a1);
        if ($query === true) {
            $nResident->noti("notes", "", $society_id, $user_token, "$created_by shared new note", $note_title, 'notes');


            // $notiAry = array(
            //   'society_id'=>$society_id,
            //   'user_id'=>$user_id,
            //   'notification_title'=>"$created_by shared new note",
            //   'notification_desc'=>"$note_title",    
            //   'notification_date'=>date('Y-m-d H:i'),
            //   'notification_action'=>'notes',
            // );
            // $d->insert("user_notification",$notiAry);

            $_SESSION['msg'] = "Successfully Added";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }


    if (isset($deleteNote)) {

        $query = $d->delete("user_notes", "note_id='$note_id'");

        if ($query === true) {
            $_SESSION['msg'] = "Note Deleted";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }
    if (isset($addFamilyMember) && $addFamilyMember == 'addFamilyMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($parent_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

        if (strlen($user_mobile) < 7 && $user_status == 1) {
            $_SESSION['msg1'] = "Please add mobile number for app access";
            header("location:../employeeDetails?id=$parent_id");
            exit();
        }


        $qSociety = $d->select("society_master", "society_id ='$society_id'");
        $societyData = mysqli_fetch_array($qSociety);
        $family_member_approval = $societyData['family_member_approval'];

        $qselect = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0' AND user_status=1 ");
        $user_data = mysqli_fetch_array($qselect);
        if ($user_data == true && $user_data['delete_status'] == 0) {
            $_SESSION['msg1'] = "Mobile number is already register.";
            header("location:../employeeDetails?id=$parent_id");
            exit();
        }


        $qc = $d->select("users_master,unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id' AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=users_master.block_id AND users_master.user_id='$parent_id'");
        $userTypeData = mysqli_fetch_array($qc);
        $user_type = $userTypeData['user_type'];
        $tenant_view = $userTypeData['tenant_view'];
        $parent_name = $userTypeData['user_full_name'];
        $blockName = $userTypeData['block_name'];
        $unitName = $userTypeData['unit_name'];


        $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
        $uploadedFile = $_FILES["family_profile_pic"]["tmp_name"];
        $ext = pathinfo($_FILES['family_profile_pic']['name'], PATHINFO_EXTENSION);
        if (file_exists($uploadedFile)) {

            $sourceProperties = getimagesize($uploadedFile);
            $newFileName = rand() . $user_id;
            $dirPath = "../../img/users/recident_profile/";
            $imageType = $sourceProperties[2];
            $imageHeight = $sourceProperties[1];
            $imageWidth = $sourceProperties[0];
            if ($imageWidth > 500) {
                $newWidthPercentage = 500 * 100 / $imageWidth; //for maximum 500 widht
                $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                $newImageHeight = $imageHeight * $newWidthPercentage / 100;
            } else {
                $newImageWidth = $imageWidth;
                $newImageHeight = $imageHeight;
            }
            switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_profile." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_profile." . $ext);
                    break;

                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagegif($tmp, $dirPath . $newFileName . "_profile." . $ext);
                    break;

                default:

                    break;
            }
            $family_profile_pic = $newFileName . "_profile." . $ext;
        } else {

            $family_profile_pic  = "";
        }

        $m->set_data('user_profile_pic', $family_profile_pic);


        $m->set_data('parent_id', $parent_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('unit_id', $unit_id);

        $m->set_data('member_status', '1');

        $family_user_first_name = ucfirst($family_user_first_name);
        $user_last_name = ucfirst($user_last_name);

        $m->set_data('user_password', $password);
        $m->set_data('user_first_name', $family_user_first_name);
        $m->set_data('user_last_name', $user_last_name);
        $m->set_data('country_code', $country_code);
        $m->set_data('user_full_name', $family_user_first_name . " " . $user_last_name);

        if (strlen($user_mobile) > 7 && $user_status == 1) {
            $user_status = 1;
        } else {
            $user_status = 2;
        }


        $m->set_data('user_status', $user_status);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('user_email', $user_email);

        // user status    =>  0 pending approval 1 approved 2 for login deny.
        $m->set_data('user_type', $user_type);
        $m->set_data('member_relation_name', $member_relation);
        $m->set_data('tenant_view', $tenant_view);
        $m->set_data('designation', $designation);
        $m->set_data('register_date', date('Y-m-d H:i:s'));

        $a1 = array(
            'parent_id' => $m->get_data('parent_id'),
            'society_id' => $m->get_data('society_id'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'unit_id' => $m->get_data('unit_id'),
            'user_first_name' => $m->get_data('user_first_name'),
            'user_last_name' => $m->get_data('user_last_name'),
            'user_full_name' => $m->get_data('user_full_name'),
            'user_mobile' => $m->get_data('user_mobile'),
            'user_email' => $m->get_data('user_email'),
            'user_type' => $m->get_data('user_type'),
            'user_status' => $m->get_data('user_status'),
            'user_profile_pic' => $m->get_data('user_profile_pic'),
            'member_status' => $m->get_data('member_status'),
            'member_relation_name' => "Team",
            'register_date' => $m->get_data('register_date'),
            'country_code' => $m->get_data('country_code'),
            'delete_status' => 0,
        );



        if ($user_data['delete_status'] == 1) {
            $q = $d->update("users_master", $a1, "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0' ");
            $user_id = $user_data['user_id'];
            $d->delete("user_employment_details", "user_id='$user_id'");
        } else {
            $q = $d->insert("users_master", $a1);
            $user_id = $con->insert_id;
            $m->set_data('user_id', $user_id);
        }
        if ($user_status == 1) {
            $societyName = $_COOKIE['society_name'];
            $smsObj->send_welcome_message($society_id, $user_mobile, $family_user_first_name, $societyName, $country_code);
            $d->add_sms_log($user_mobile, "User Welcome Message", $society_id, $country_code, 4);
        }



        if ($q == true) {
            $compArray = array(
                'user_id' => $m->get_data('user_id'),
                'society_id' => $m->get_data('society_id'),
                'unit_id' => $m->get_data('unit_id'),
                'designation' => $m->get_data('designation'),
            );
            $d->insert("user_employment_details", $compArray);


            $_SESSION['msg'] = "Employee Added Successfully";
            header("location:../employeeDetails?id=$parent_id");
            exit();
        } else {
            $_SESSION['msg1'] = "Something Wrong.";
            header("location:../employeeDetails?id=$parent_id");
            exit();
        }
    }

    if (isset($addEducation)) {
        $getEmployment_id = $d->selectRow("employment_id", "user_employment_details", "user_id = '$user_id'");
        $employment_id_arr = mysqli_fetch_array($getEmployment_id);
        $employment_id = $employment_id_arr['employment_id'];

        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('class_name', $class_name);
        $m->set_data('university_name', $university_name);
        $m->set_data('pass_year', $pass_year);
        $m->set_data('status_type_select', $status_type_select);
        $m->set_data('created_date', date("Y-m-d H:i:s"));

        $m->set_data('admin_id', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $m->get_data('society_id'),
            'achievement_name' => $m->get_data('class_name'),
            'university_board_name' => $m->get_data('university_name'),
            'achievement_date' => $m->get_data('pass_year'),
            'add_date' => $m->get_data('created_date'),
            'archi_type' => $m->get_data('status_type_select'),
        );

        $query = $d->insert("employee_achievements_education", $a1);
        if ($query === true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User added education/achievements details");
            $_SESSION['msg'] = "Education/Achievement Details Added";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }

    if (isset($deleteEducation)) {

        $query = $d->delete("employee_achievements_education", "employee_achievement_id='$employee_achievement_id'");

        if ($query === true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User education/achievements details deleted");
            $_SESSION['msg'] = "User education/achievement details Deleted";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }

    if (isset($addExperience)) {
        $getEmployment_id = $d->selectRow("employment_id", "user_employment_details", "user_id = '$user_id'");
        $employment_id_arr = mysqli_fetch_array($getEmployment_id);
        $employment_id = $employment_id_arr['employment_id'];

        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('exp_company_name', $company_name);
        $m->set_data('designation', $designation);
        $m->set_data('work_from', $work_from);
        $m->set_data('work_to', $work_to);
        $m->set_data('company_location', $company_location);
        $m->set_data('add_date', date("Y-m-d H:i:s"));

        $m->set_data('admin_id', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $m->get_data('society_id'),
            'exp_company_name' => $m->get_data('exp_company_name'),
            'designation' => $m->get_data('designation'),
            'work_from' => $m->get_data('work_from'),
            'work_to' => $m->get_data('work_to'),
            'company_location' => $m->get_data('company_location'),
            'add_date' => $m->get_data('add_date')
        );

        $query = $d->insert("employee_experience", $a1);
        if ($query === true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User added experience details");
            $_SESSION['msg'] = "Experience Details Added";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }

    if (isset($deleteExperience)) {
        $query = $d->delete("employee_experience", "employee_experience_id='$employee_experience_id'");
        if ($query === true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User experience details deleted");
            $_SESSION['msg'] = "User experience details Deleted";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }
    if (isset($deleteExperienceHistory)) {

        $query = $d->delete("employee_history", "history_id='$history_id'");
        if ($query === true) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User experience History deleted");
            $_SESSION['msg'] = "User experience History Deleted";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }

    if (isset($updateUserSort) && $updateUserSort == "updateUserSort") {
        $usr_master_qry = $d->selectRow("user_full_name", "users_master", "user_sort = " . $user_sort . " AND user_id != '$user_id'");
        if (mysqli_num_rows($usr_master_qry) > 0) {
            $get_user_name = $usr_master_qry->fetch_assoc();
            $msg['status'] = "duplicate";
            $msg['user'] = $get_user_name['user_full_name'];
            echo json_encode($msg);
            exit;
        } else {
            $m->set_data('user_sort', $user_sort);
            $a5 = array(
                'user_sort' => $m->get_data('user_sort'),
            );

            $q = $d->update("users_master", $a5, "user_id='$user_id'");
            if ($q > 0) {
                $user_data = $d->selectRow("user_full_name", "users_master", "user_id = '$user_id'");
                $user_arr = $user_data->fetch_assoc();
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$user_arr[user_full_name] User Order Updated");
                $msg['status'] = "success";
                echo json_encode($msg);
                exit;
            } else {
                $msg['status'] = "error";
                echo json_encode($msg);
                exit;
            }
        }
    }

    if (isset($updateUserLocation) && $updateUserLocation == "updateUserLocation") {
        $user_latitude = (!empty($user_latitude) && $user_latitude != "") ? $user_latitude : $old_user_latitude;
        $user_longitude = (!empty($user_longitude) && $user_longitude != "") ? $user_longitude : $old_user_longitude;

        $update_arr = [
            "user_latitude" => $user_latitude,
            "user_longitude" => $user_longitude
        ];

        $update_user_location = $d->update("users_master", $update_arr, "user_id = '$user_id'");

        if ($update_user_location) {
            $_SESSION['msg'] = "User's Location Updated Successfully";
            header("location:../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }
    }

    if (isset($changeDepartment) && $changeDepartment == "changeDepartment") {
        mysqli_autocommit($con, FALSE);
        $currentDate = date('Y-m-d');
        if (isset($shift_time_id) && $shift_time_id != '' && $shift_time_id > 0) {
            $aq = $d->selectRow("attendance_id,shift_time_id", "attendance_master", "user_id = '$user_id' AND attendance_date_start = '$currentDate'");
            $aData = mysqli_fetch_array($aq);
            if (mysqli_num_rows($aq) > 0) {
                $attendance_id = $aData['attendance_id'];
                $attendance = array('shift_time_id' => $shift_time_id);
                $q = $d->update("attendance_master", $attendance, "attendance_id = '$attendance_id'");
            }
        }

        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('sub_department_id', $sub_department_id);
        $m->set_data('shift_time_id', $shift_time_id);

        $a1 = array(
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'sub_department_id' => $m->get_data('sub_department_id'),
            'shift_time_id' => $m->get_data('shift_time_id'),
        );

        $a2 = array(
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
        );

        $a3 = array(
            'floor_id' => $m->get_data('floor_id'),
        );

        $discussion = array(
            'discussion_block_id' => $m->get_data('block_id'),
            'discussion_forum_for' => $m->get_data('floor_id'),
        );

        $q1 = $d->update("app_access_master", $a2, "access_for_id = '$user_id'");
        $q2 = $d->update("assets_detail_inventory_master", $a3, "user_id = '$user_id'");
        $q3 = $d->update("hr_document_master", $a3, "user_id = '$user_id'");
        $q4 = $d->update("idea_master", $a3, "user_id = '$user_id'");
        $q5 = $d->update("parking_master", $a2, "unit_id = '$unit_id'");
        $q6 = $d->update("salary", $a2, "user_id = '$user_id'");
        $q7 = $d->update("unit_master", $a2, "unit_id = '$unit_id'");
        $q8 = $d->update("users_master", $a1, "user_id = '$user_id'");
        $q9 = $d->update("user_bank_master", $a3, "user_id = '$user_id'");
        $q10 = $d->update("user_expenses", $a3, "user_id = '$user_id'");
        $q11 = $d->update("visitors_master", $a2, "user_id = '$user_id'");
        $q12 = $d->update("work_report_master", $a3, "user_id = '$user_id'");
        $q13 = $d->update("wfh_master", $a3, "user_id = '$user_id'");
        $q14 = $d->update("discussion_forum_master", $discussion, "user_id = '$user_id'");
        $q15 = $d->update("salary", $a2, "user_id = '$user_id'");
        $q16 = $d->update("salary_slip_master", $a2, "user_id = '$user_id'");
        $q16 = $d->update("advance_salary", $a2, "user_id = '$user_id'");
        /*  print_r("1".$q1);
        print_r("user_id".$user_id);
        print_r("2".$q2);
        print_r("3".$q3);
        print_r("4".$q4);
        print_r("5".$q5);
        print_r("6".$q6);
        print_r("7".$q7);
        print_r("8".$q8);
        print_r("9".$q9);
        print_r("10".$q10);
        print_r("11".$q11);
        print_r("12".$q12);
        print_r("13".$q13);
        print_r("14".$q14);
        print_r("15".$q15);
        print_r("16".$q16);
        die; */
        if (($q1 && $q2 && $q3 && $q4 && $q5 && $q6 && $q7 && $q8 && $q9 && $q10 && $q11 && $q12 && $q12 && $q14 && $q15 && $q16) || $q) {
            $q = $d->selectRow('user_token,device', "users_master", "user_id='$user_id'");
            $userData = mysqli_fetch_assoc($q);
            $user_token = $userData['user_token'];
            $device = $userData['device'];
            $description = "Your Department Is Changed By Admin $created_by";
            if ($device == 'android') {
                $nResident->noti("", "", $society_id, $user_token, "Logout", $description, '');
            } else if ($device == 'ios') {
                $nResident->noti_ios("", "", $society_id, $user_token, "Logout", $description, '');
            }

            $m->set_data('user_token', '');
            $a = array(
                'user_token' => $m->get_data('user_token')
            );
            $qdelete = $d->update("users_master", $a, "user_id='$user_id'");

            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Department Change Successfully");
            mysqli_commit($con);
            $_SESSION['msg'] = "User Department Change Successfully";
            header("location:../employeeDetails?id=$user_id");
        } else {
            mysqli_rollback($con);
            $_SESSION['msg1'] = "Something Wents Wrong";
            header("location:../employeeDetails?id=$user_id");
        }

        //SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME IN ('floor_id','user_id') AND TABLE_SCHEMA='company_developer';
    }

    if (isset($approveBirthdateRequest) && $approveBirthdateRequest == "approveBirthdateRequest") {
        $up = [
            'approved_by_type' => 1,
            'approved_by_id' => $_COOKIE['bms_admin_id'],
            'modified_datetime' => date("Y-m-d H:i:s"),
            'birthdate_status' => 1
        ];
        $q = $d->update("birthdate_change_request", $up, "birthdate_change_request_id = '$birthdate_change_request_id'");
        if ($q) {
            $up_user = [
                'member_date_of_birth' => $new_birthdate,
                'modify_date' => date("Y-m-d H:i:s")
            ];
            $q1 = $d->update("users_master", $up_user, "user_id = '$user_id'");
            if ($q1) {
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Birthdate Change Request Approved");
                $_SESSION['msg'] = "Birthdate Change Request Approved Successfully";
            } else {
                $_SESSION['msg1'] = "Something Went Wrong!";
            }
        } else {
            $_SESSION['msg1'] = "Something Went Wrong!";
        }
    }

    if (isset($rejectBirthdateRequest) && $rejectBirthdateRequest == "rejectBirthdateRequest") {
        if (is_array($birthdate_change_request_id)) {
            for ($i = 0; $i < count($birthdate_change_request_id); $i++) {
                $up = [
                    'reject_by_type' => 1,
                    'reject_by_id' => $_COOKIE['bms_admin_id'],
                    'reject_reason' => $reject_reason,
                    'modified_datetime' => date("Y-m-d H:i:s"),
                    'birthdate_status' => 2
                ];
                $q = $d->update("birthdate_change_request", $up, "birthdate_change_request_id = '$birthdate_change_request_id[$i]'");
            }
        } else {
            $up = [
                'reject_by_type' => 1,
                'reject_by_id' => $_COOKIE['bms_admin_id'],
                'reject_reason' => $reject_reason,
                'modified_datetime' => date("Y-m-d H:i:s"),
                'birthdate_status' => 2
            ];
            $q = $d->update("birthdate_change_request", $up, "birthdate_change_request_id = '$birthdate_change_request_id'");
        }
        if ($q) {
            $_SESSION['msg'] = "Birthdate Change Request Rejected Successfully";
        } else {
            $_SESSION['msg1'] = "Something Went Wrong!";
        }
        header("location:../birthdateRequest");
    }

    if (isset($_POST['approveRequestMulti']) && $_POST['approveRequestMulti'] == "approveRequestMulti") {
        $idCount = count($ids);
        for ($i = 0; $i < $idCount; $i++) {
            $up = [
                'approved_by_type' => 1,
                'approved_by_id' => $_COOKIE['bms_admin_id'],
                'modified_datetime' => date("Y-m-d H:i:s"),
                'birthdate_status' => 1
            ];
            $q = $d->update("birthdate_change_request", $up, "birthdate_change_request_id = '$ids[$i]'");
            if ($q) {
                $up_user = [
                    'member_date_of_birth' => $birth_dates[$i],
                    'modify_date' => date("Y-m-d H:i:s")
                ];
                $q1 = $d->update("users_master", $up_user, "user_id = '$user_id[$i]'");
            }
        }
        if ($q1 > 0) {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Birthdate Change Request Approved");
            $_SESSION['msg'] = "Birthdate Change Request Approved Successfully";
        } else {
            $_SESSION['msg1'] = "Something Went Wrong!";
        }
    }

    if (isset($_POST['getCelebrationList']) && $_POST['getCelebrationList'] == "getCelebrationList") {
        $response = [];
        $response['birthdays'] = [];
        $response['wedding'] = [];
        $response['joining'] = [];
        $q = $d->selectRow("user_id,user_profile_pic,member_date_of_birth,user_full_name,user_designation", "users_master", "delete_status = 0 AND user_status = '1' AND active_status = '0' AND DATE_ADD(member_date_of_birth, INTERVAL YEAR(CURDATE())-YEAR(member_date_of_birth) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(member_date_of_birth),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 16 DAY) AND DATE_ADD(member_date_of_birth, INTERVAL YEAR(CURDATE())-YEAR(member_date_of_birth) YEAR) <> CURDATE()", "ORDER BY Month(member_date_of_birth) ASC,day( member_date_of_birth) ASC");
        $bd = [];
        while ($data = $q->fetch_assoc()) {
            array_push($response["birthdays"], $data);
        }

        $wa = $d->selectRow("user_id,user_profile_pic,user_full_name,user_designation,wedding_anniversary_date", "users_master", "delete_status = 0 AND user_status = '1' AND active_status = '0' AND DATE_ADD(wedding_anniversary_date, INTERVAL YEAR(CURDATE())-YEAR(wedding_anniversary_date) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(wedding_anniversary_date),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 16 DAY) AND DATE_ADD(wedding_anniversary_date, INTERVAL YEAR(CURDATE())-YEAR(wedding_anniversary_date) YEAR) <> CURDATE()", "ORDER BY Month(wedding_anniversary_date) ASC,day( wedding_anniversary_date) ASC");
        $wd = [];
        while ($wad = $wa->fetch_assoc()) {
            array_push($response["wedding"], $wad);
        }

        $jd = $d->selectRow("users_master.user_id,user_profile_pic,user_full_name,user_designation,joining_date", "users_master JOIN user_employment_details ON user_employment_details.user_id = users_master.user_id", "delete_status = 0 AND user_status = '1' AND active_status = '0' AND DATE_ADD(joining_date, INTERVAL YEAR(CURDATE())-YEAR(joining_date) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(joining_date),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 16 DAY) AND DATE_ADD(joining_date, INTERVAL YEAR(CURDATE())-YEAR(joining_date) YEAR) <> CURDATE()", "ORDER BY Month(joining_date) ASC,day( joining_date) ASC");
        $j = [];
        while ($jdd = $jd->fetch_assoc()) {
            array_push($response["joining"], $jdd);
        }
        echo json_encode($response);
        exit;
    }

    /*01 Aug 2022*/
    if (isset($_POST['employeeOnboarding'])) {

        $user_mobile = (int)$user_mobile;
        if (strlen($user_mobile) < 8) {
            $_SESSION['msg1'] = "Invalid Mobile Number";
            header("location:../companyEmployees?bId=$block_id");
            exit;
        }

        // add new unit first

        // $dataCount = count($_POST['block_name']);
        $file = $_FILES['user_profile_pic']['tmp_name'];
        if (file_exists($file)) {
            $errors     = array();
            $maxsize    = 2097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if (($_FILES['user_profile_pic']['size'] >= $maxsize) || ($_FILES["user_profile_pic"]["size"] == 0)) {
                $_SESSION['msg1'] = "Profile photo too large. Must be less than 2 MB.";
                header("location:../companyEmployees?bId=$block_id");
                exit();
            }
            if (!in_array($_FILES['user_profile_pic']['type'], $acceptable) && (!empty($_FILES["user_profile_pic"]["type"]))) {
                $_SESSION['msg1'] = "Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../companyEmployees?bId=$block_id");
                exit();
            }
            if (count($errors) === 0) {
                $image_Arr = $_FILES['user_profile_pic'];
                $temp = explode(".", $_FILES["user_profile_pic"]["name"]);
                $user_profile_pic = $user_first_name_add . '_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["user_profile_pic"]["tmp_name"], "../../img/users/recident_profile/" . $user_profile_pic);
            }
        } else {
            $user_profile_pic = "user.png";
        }

        $filesss = $_FILES["resume_cv_doc"]["tmp_name"];
        if (file_exists($filesss)) {
            $temp = explode(".", $_FILES["resume_cv_doc"]["name"]);
            $resume_cv_doc = "resume_cv_doc" . round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["resume_cv_doc"]["tmp_name"], "../../img/documents/" . $resume_cv_doc);
        }


        $get_sort = $d->selectRow("user_sort", "users_master", "floor_id = '$floor_id' AND block_id = '$block_id'", "ORDER BY user_sort DESC LIMIT 1");
        $sort = $get_sort->fetch_assoc();
        $sort_value = $sort['user_sort'] + 1;
        $societyName = $_COOKIE['society_name'];

        $m->set_data('society_id', $society_id);
        $m->set_data('sister_company_id', $sister_company_id);
        $m->set_data('shift_time_id', $shift_time_id);
        $m->set_data('user_profile_pic', $user_profile_pic);
        $m->set_data('resume_cv_doc', $resume_cv_doc);
        $m->set_data('user_full_name', ucfirst($user_first_name) . " " . ucfirst($user_last_name));
        $m->set_data('user_first_name', ucfirst($user_first_name));
        $m->set_data('user_middle_name', ucfirst($user_middle_name));
        $m->set_data('user_last_name', ucfirst($user_last_name));
        $m->set_data('gender', $gender);
        $m->set_data('user_mobile', $user_mobile);
        $m->set_data('user_email', $user_email);
        $m->set_data('last_address', $last_address);
        $m->set_data('owner_name', $owner_first_name . " " . $owner_last_name);
        $m->set_data('owner_mobile', $owner_mobile);
        $m->set_data('user_token', $user_token);
        $m->set_data('last_address', $last_address);
        $m->set_data('tenant_doc', $tenant_doc);
        $m->set_data('country_code', $country_code);
        $m->set_data('user_type', 0);
        $m->set_data('unit_status', $unit_status);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('sub_department_id', $sub_department_id);
        $m->set_data('zone_id', $zone_id);
        $m->set_data('level_id', $level_id);
        $m->set_data('user_status', 1);
        $m->set_data('user_sort', $sort_value);
        $m->set_data('company_name', $company_name);
        $m->set_data('company_address', $company_address);
        $m->set_data('designation', $designation);
        $m->set_data('joining_date', $joining_date);
        $m->set_data('member_date_of_birth', $member_date_of_birth);
        $m->set_data('blood_group', $blood_group);
        $m->set_data('employment_type', $employment_type);
        $m->set_data('country_code_whatsapp', $country_code_whatsapp);
        $m->set_data('whatsapp_number', $whatsapp_number);
        $m->set_data('country_code_alt', $country_code_alt);
        $m->set_data('alt_mobile', $alt_mobile);
        $m->set_data('country_code_emergency', $country_code_emergency);
        $m->set_data('emergency_number', $emergency_number);
        $m->set_data('last_address', $last_address);
        $m->set_data('permanent_address', $permanent_address);
        $m->set_data('personal_email', $personal_email);
        $m->set_data('facebook', $facebook);
        $m->set_data('linkedin', $linkedin);
        $m->set_data('twitter', $twitter);
        $m->set_data('instagram', $instagram);
        $m->set_data('professional_skills', $professional_skills);
        $m->set_data('intrest_hobbies', $intrest_hobbies);
        $m->set_data('language_known', $language_known);
        $m->set_data('special_skills', $special_skills);
        $m->set_data('marital_status', $marital_status);
        $m->set_data('wedding_anniversary_date', $wedding_anniversary_date);
        $m->set_data('total_family_members', $total_family_members);
        $m->set_data('nationality', $nationality);
        $m->set_data('bank_branch_name', $bank_branch_name);
        $m->set_data('account_type', $account_type);
        $m->set_data('account_holders_name', ucfirst($account_holders_name));
        $m->set_data('bank_name', ucfirst($bank_name));
        $m->set_data('account_no', $account_no);
        $m->set_data('ifsc_code', strtoupper($ifsc_code));
        $m->set_data('crn_no', $crn_no);
        $m->set_data('esic_no', $esic_no);
        $m->set_data('pan_card_no', strtoupper($pan_card_no));
        $m->set_data('bank_created_at', date("Y-m-d H:i:s"));

        
        $au = array(
            'society_id' =>  $m->get_data('society_id'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'unit_name' => $unit_name
        );

        $qunit = $d->insert("unit_master", $au);
        $unit_id = $con->insert_id;
        $m->set_data('unit_id', $unit_id);
        $m->set_data('register_date', date("Y-m-d H:i:s"));

        $a = array(
            'society_id' => $m->get_data('society_id'),
            'sister_company_id' => $m->get_data('sister_company_id'),
            'shift_time_id' => $m->get_data('shift_time_id'),
            'user_profile_pic' => $m->get_data('user_profile_pic'),
            'user_full_name' => $m->get_data('user_full_name'),
            'user_first_name' => $m->get_data('user_first_name'),
            'user_middle_name' => $m->get_data('user_middle_name'),
            'user_last_name' => $m->get_data('user_last_name'),
            'gender' => $m->get_data('gender'),
            'user_mobile' => $m->get_data('user_mobile'),
            'user_email' => $m->get_data('user_email'),
            'last_address' => $m->get_data('last_address'),
            'user_type' => $m->get_data('user_type'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'sub_department_id' => $m->get_data('sub_department_id'),
            'zone_id' => $m->get_data('zone_id'),
            'level_id' => $m->get_data('level_id'),
            'unit_id' => $m->get_data('unit_id'),
            'user_status' => $m->get_data('user_status'),
            'user_sort' => $m->get_data('user_sort'),
            'country_code' => $m->get_data('country_code'),
            'register_date' => $m->get_data('register_date'),
            'user_designation' => $m->get_data('designation'),
            'register_date' => $m->get_data('register_date'),
            'member_date_of_birth' => $m->get_data('member_date_of_birth'),
            'blood_group' => $m->get_data('blood_group'),
            'country_code_whatsapp' => $m->get_data('country_code_whatsapp'),
            'whatsapp_number' => $m->get_data('whatsapp_number'),
            'country_code_alt' => $m->get_data('country_code_alt'),
            'alt_mobile' => $m->get_data('alt_mobile'),
            'country_code_emergency' => $m->get_data('country_code_emergency'),
            'last_address' => $m->get_data('last_address'),
            'permanent_address' => $m->get_data('permanent_address'),
            'personal_email' => $m->get_data('personal_email'),
            'facebook' => $m->get_data('facebook'),
            'linkedin' => $m->get_data('linkedin'),
            'twitter' => $m->get_data('twitter'),
            'instagram' => $m->get_data('instagram'),
            'marital_status' => $m->get_data('marital_status'),
            'total_family_members' => $m->get_data('total_family_members'),
            'wedding_anniversary_date' => $m->get_data('wedding_anniversary_date'),
            'nationality' => $m->get_data('nationality'),
        );

        $q = $d->insert("users_master", $a);
        $user_id = $con->insert_id;
        $m->set_data('user_id', $user_id);

        if ($q > 0) {

            /*$user_sort_arr = [
        'user_sort' => $m->get_data('user_id')
      ];
      $d->update("users_master",$user_sort_arr,"user_id='$user_id'");*/

            if ($employee_id_generate == 1) {
                $unit_name = 'MyCo' . $user_id;
            }

            $a22 = array(
                'unit_status' => $unit_status,
                'unit_name' => $unit_name,
            );

            $d->update("unit_master", $a22, "unit_id='$unit_id'");
            $_SESSION['msg'] = "User Added";

            $compArray = array(
                'user_id' => $m->get_data('user_id'),
                'society_id' => $m->get_data('society_id'),
                'designation' => $m->get_data('designation'),
                'unit_id' => $m->get_data('unit_id'),
                'joining_date' => $m->get_data('joining_date'),
                'employment_type' => $m->get_data('employment_type'),
                'professional_skills' => $m->get_data('professional_skills'),
                'intrest_hobbies' => $m->get_data('intrest_hobbies'),
                'language_known' => $m->get_data('language_known'),
                'special_skills' => $m->get_data('special_skills'),
            );
            $d->insert("user_employment_details", $compArray);

            if ($id_proof_id > 0) {

                $extension = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG", "pdf");
                $uploadedFile = $_FILES['id_proof']['tmp_name'][0];
                $ext = pathinfo($_FILES['id_proof']['name'][0], PATHINFO_EXTENSION);
                $ext = strtolower($ext);
                $filesize = $_FILES["id_proof"]["size"][0];
                $maxsize = 20971520;


                $uploadedFileBack = $_FILES['id_proof']['tmp_name'][1];
                $extBack = pathinfo($_FILES['id_proof']['name'][1], PATHINFO_EXTENSION);
                $extBack = strtolower($extBack);

                if (file_exists($uploadedFile)) {
                    if (in_array($extBack, $extension)) {
                        $temp = explode(".", $_FILES["id_proof"]["name"][0]);
                        $id_proof = "IdProof$user_first_name" . date("YmdHis") . 'Front.' . end($temp);
                        move_uploaded_file($_FILES["id_proof"]["tmp_name"][0], "../../img/id_proof/" . $id_proof);
                    }
                }
                if (file_exists($uploadedFileBack)) {
                    if (in_array($extBack, $extension)) {
                        $temp = explode(".", $_FILES["id_proof"]["name"][1]);
                        $id_proof_back = "IdProof$user_first_name" . date("YmdHis") . 'Back.' . end($temp);
                        move_uploaded_file($_FILES["id_proof"]["tmp_name"][1], "../../img/id_proof/" . $id_proof_back);
                    }
                }

                $m->set_data('user_id', $user_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('id_proof_id', $id_proof_id);
                $m->set_data('id_proof', $id_proof);
                $m->set_data('id_proof_back', $id_proof_back);
                $m->set_data('created_date', date("Y-m-d H:i:s"));

                $a2 = array(
                    'user_id' => $m->get_data('user_id'),
                    'society_id' => $m->get_data('society_id'),
                    'id_proof_id' => $m->get_data('id_proof_id'),
                    'id_proof' => $m->get_data('id_proof'),
                    'id_proof_back' => $m->get_data('id_proof_back'),
                    'created_date' => $m->get_data('created_date'),
                );

                $q1 = $d->insert("user_id_proofs", $a2);
            }


            if ($add_bank_detail == 'yes') {
                $bankArray = array(
                    'society_id' => $m->get_data('society_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'block_id' => $m->get_data('block_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'user_id' => $m->get_data('user_id'),
                    'bank_branch_name' => $m->get_data('bank_branch_name'),
                    'account_type' => $m->get_data('account_type'),
                    'account_holders_name' => $m->get_data('account_holders_name'),
                    'bank_name' => $m->get_data('bank_name'),
                    'account_no' => $m->get_data('account_no'),
                    'ifsc_code' => strtoupper($m->get_data('ifsc_code')),
                    'crn_no' => $m->get_data('crn_no'),
                    'esic_no' => $m->get_data('esic_no'),
                    'pan_card_no' => strtoupper($m->get_data('pan_card_no')),
                    'bank_created_at' => $m->get_data('bank_created_at'),
                    'is_primary' => '1',
                );
                $d->insert("user_bank_master", $bankArray);
            }

            if ($level_id > 0) {
                $performanceAssignQuery = $d->select("performance_dimensional_assign", "find_in_set('$level_id', level_ids) AND (find_in_set('$block_id', block_ids) OR block_ids = 0) AND (find_in_set('$floor_id', floor_ids) OR floor_ids =0)");

                while ($performanceAssignData = mysqli_fetch_array($performanceAssignQuery)) {

                    $d->delete("pms_schedule_master", "dimensional_id='$performanceAssignData[dimensional_id]' AND user_id='$prof_user_id' AND schedule_status=0");

                    if ($performanceAssignData['pms_type'] == 0) {
                        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                        $day = $performanceAssignData['day'];
                        $to_date = date('Y-m-d');
                        $schedule_date = $weekly_date = date('Y-m-d', strtotime($days[$day], strtotime($to_date)));
                    } elseif ($performanceAssignData['pms_type'] == 1) {
                        $month = date('F Y');
                        $month_days = explode(',', $performanceAssignData['day']);
                        $month_day = $day . ' ' . $month;
                        if (date('Y-m-d', strtotime($month_day)) < $currentDate) {
                            $month_day = date('Y-m-d', strtotime($month_day . ' + 1 months'));
                        }
                        $schedule_date = $month_day;
                    } elseif ($performanceAssignData['pms_type'] == 2) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $performanceAssignData['day'];
                        if ($currentDate <= date('Y-m-d', strtotime($date))) {
                            $monthNum  = $performanceAssignData['start_month'];
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $quarterly_date = $day . ' ' . $monthName;
                        } else {
                            $newDate = date('m', strtotime($date . ' + 3 months'));
                            $monthNum  = $newDate;
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $quarterly_date = $day . ' ' . $monthName;
                        }

                        if (date('Y-m-d', strtotime($quarterly_date)) < $currentDate) {
                            $quarterly_date = date('Y-m-d', strtotime($quarterly_date . ' + 3 months'));
                            while (date('Y-m-d', strtotime($quarterly_date)) < $currentDate) {
                                $quarterly_date = date('Y-m-d', strtotime($quarterly_date . ' + 3 months'));
                            }
                        }
                        $schedule_date = $quarterly_date;
                    } elseif ($performanceAssignData['pms_type'] == 3) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $day;
                        if ($currentDate <= date('Y-m-d', strtotime($date))) {
                            $monthNum  = $performanceAssignData['start_month'];
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $half_year_date = $day . ' ' . $monthName;
                        } else {
                            $newDate = date('m', strtotime($date . ' + 6 months'));
                            $monthNum  = $newDate;
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F') . ' ' . date('Y');
                            $half_year_date = $day . ' ' . $monthName;
                        }

                        if (date('Y-m-d', strtotime($half_year_date)) < $currentDate) {
                            $half_year_date = date('Y-m-d', strtotime($half_year_date . ' + 6 months'));
                            while (date('Y-m-d', strtotime($half_year_date)) < $currentDate) {
                                $half_year_date = date('Y-m-d', strtotime($half_year_date . ' + 6 months'));
                            }
                        }
                        $schedule_date = $half_year_date;
                    } elseif ($performanceAssignData['pms_type'] == 4) {
                        $date = date('Y') . '-' . $performanceAssignData['start_month'] . '-' . $day;
                        $monthNum  = $performanceAssignData['start_month'];
                        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                        $monthName = $dateObj->format('F') . ' ' . date('Y');
                        $year_date = $day . ' ' . $monthName;

                        if (date('Y-m-d', strtotime($year_date)) < $currentDate) {
                            $year_date = date('Y-m-d', strtotime($year_date . ' + 1 year'));
                        }
                        $schedule_date = $year_date;
                    }

                    $m->set_data('society_id', $society_id);
                    $m->set_data('user_id', $prof_user_id);
                    $m->set_data('schedule_date', $schedule_date);
                    $m->set_data('dimensional_id', $performanceAssignData['dimensional_id']);
                    $m->set_data('schedule_created_date', date("Y-m-d H:i:s"));

                    $a2 = array(
                        'society_id' => $m->get_data('society_id'),
                        'user_id' => $m->get_data('user_id'),
                        'schedule_date' => $m->get_data('schedule_date'),
                        'dimensional_id' => $m->get_data('dimensional_id'),
                        'schedule_created_date' => $m->get_data('schedule_created_date'),
                    );
                    $q1 = $d->insert("pms_schedule_master", $a2);
                }
            }

            $smsObj->send_welcome_message($society_id, $user_mobile, $user_first_name, $society_name, $country_code);
            $d->add_sms_log($user_mobile, "User Welcome Message", $society_id, $country_code, 4);

            $forgotLink = "https://my-company.app/";
            $forgotLink1 = "";
            $user_name = $user_first_name;
            $to = $user_email;
            $subject = "Account Created for $society_name - MyCo";
            if ($to != '') {
                include '../mail/welcomeUserMail.php';
                include '../mail.php';
            }
            // send email for username & Password to user 
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "New Employee ($user_first_name $user_last_name) Added");
            $result['message'] = "Employee Added Successfully.";
            $result['status'] = "200";
        } else {
            $result['message'] = "Something Wrong";
            $result['status'] = "1";
        }
        //print_r($result);
        echo json_encode($result);
    }


    if (isset($_POST['addIdProof']) && $_POST['addIdProof']) {

        $d->delete("user_id_proofs", "user_id='$user_id' AND id_proof_id='$id_proof_id'");

        $extension = array("jpeg", "jpg", "png", "pdf");
        $uploadedFile = $_FILES['id_proof']['tmp_name'][0];
        $ext = pathinfo($_FILES['id_proof']['name'][0], PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        $filesize = $_FILES["id_proof"]["size"][0];
        $maxsize = 20971520;

        $uploadedFileBack = $_FILES['id_proof']['tmp_name'][1];
        $extBack = pathinfo($_FILES['id_proof']['name'][1], PATHINFO_EXTENSION);
        $extBack = strtolower($extBack);

        if (file_exists($uploadedFile)) {
            if (in_array($ext, $extension)) {
                $temp = explode(".", $_FILES["id_proof"]["name"][0]);
                $id_proof = "IdProof$user_first_name" . date("YmdHis") . 'Front.' . end($temp);
                move_uploaded_file($_FILES["id_proof"]["tmp_name"][0], "../../img/id_proof/" . $id_proof);
            } else {
                $_SESSION['msg1'] = "Invalid Document Type";
                header("Location: ../employeeDetails?id=$user_id");
                exit();
            }
        }
        if (file_exists($uploadedFileBack)) {
            if (in_array($extBack, $extension)) {
                $temp = explode(".", $_FILES["id_proof"]["name"][1]);
                $id_proof_back = "IdProof$user_first_name" . date("YmdHis") . 'Back.' . end($temp);
                move_uploaded_file($_FILES["id_proof"]["tmp_name"][1], "../../img/id_proof/" . $id_proof_back);
            } else {
                $_SESSION['msg1'] = "Invalid Document Type";
                header("Location: ../employeeDetails?id=$user_id");
                exit();
            }
        }

        $m->set_data('user_id', $user_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('id_proof_id', $id_proof_id);
        $m->set_data('id_proof', $id_proof);
        $m->set_data('id_proof_back', $id_proof_back);
        $m->set_data('created_date', date("Y-m-d H:i:s"));

        $a2 = array(
            'user_id' => $m->get_data('user_id'),
            'society_id' => $m->get_data('society_id'),
            'id_proof_id' => $m->get_data('id_proof_id'),
            'id_proof' => $m->get_data('id_proof'),
            'id_proof_back' => $m->get_data('id_proof_back'),
            'created_date' => $m->get_data('created_date'),
        );
        $q1 = $d->insert("user_id_proofs", $a2);



        if ($q1 == TRUE) {
            $_SESSION['msg'] = "ID Proof Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "ID Proof Added Successfully");
            header("Location: ../employeeDetails?id=$user_id");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../employeeDetails?id=$user_id");
        }
    }

    if (isset($_POST['deleteIdProof']) && $_POST['deleteIdProof']) {
        $query = $d->delete("user_id_proofs", "user_id='$user_id' AND id_proof_id='$id_proof_id'");

        if ($query === true) {
            echo '1';
        } else {
            echo '0';
        }
    }
    /*30 Nov 2022*/
    if (isset($_POST['changeEmployeeDetail'])) {

        if($user_details_menu_type == 1){
            $newData = json_decode($_POST['new_data'], true);
            $oldData = json_decode($_POST['old_data'], true);
            $userDetailArr = array(
                "blood_group"=>$newData['blood_group'],
                "gender"=>$newData['gender'],
                "marital_status"=>$newData['marital_status'],
                "member_date_of_birth"=>$newData['member_date_of_birth'],
                "nationality"=>$newData['nationality'],
                "total_family_members"=>$newData['total_family_members'],
                "wedding_anniversary_date"=>$newData['wedding_anniversary_date'],
                "personal_info_change_by_type" => '1',
                "personal_info_change_by_id" => $_COOKIE['bms_admin_id'],
                "personal_info_req_update_date" => date('Y-m-d H:i:s'),
                "personal_info_change_req" => null,
                "personal_info_req_date" => null,
            );

            $empDetailArr = array(
                "intrest_hobbies"=>$newData['intrest_hobbies'],
                "professional_skills"=>$newData['professional_skills'],
                "special_skills"=>$newData['special_skills'],
                "language_known"=>$newData['language_known'],
            );

            $historyArr = array(
                "user_id"=>$user_id,
                'old_data' => json_encode($oldData),
                'new_data' => json_encode($newData),
                "user_details_menu_type"=>$user_details_menu_type,
                "created_date"=>date('Y-m-d H:i:s'),
            );
            $q = $d->update("users_master", $userDetailArr, "user_id='$user_id'");
            $q = $d->update("user_employment_details", $empDetailArr, "user_id='$user_id'");
            $q = $d->insert("user_details_change_history_master", $historyArr);
            
            if ($q > 0) {
                $_SESSION['msg'] = "User Data Updated.";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Data Updated");
                header("location:../employeeDetailChangeRequest");exit();
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("location:../employeeDetailChangeRequest");exit();
            }
        }elseif($user_details_menu_type == 0){
            $newData = json_decode($_POST['newData'], true);
            $oldData = json_decode($_POST['oldData'], true);
            $userDetailArr = array(
                "alt_mobile"=>$newData['alt_mobile'],
                "country_code_alt"=>$newData['country_code_alt'],
                "country_code_emergency"=>$newData['country_code_emergency'],
                "country_code_whatsapp"=>$newData['country_code_whatsapp'],
                "emergency_number"=>$newData['emergency_number'],
                "last_address"=>$newData['last_address'],
                "permanent_address"=>$newData['permanent_address'],
                "personal_email"=>$newData['personal_email'],
                "user_email"=>$newData['user_email'],
                "whatsapp_number"=>$newData['whatsapp_number'],
                "contact_details_change_by_type" => '1',
                "contact_details_change_by_id" => $_COOKIE['bms_admin_id'],
                "contact_details_change_req_update_date" => date('Y-m-d H:i:s'),
                "contact_details_change_req" => null,
                "contact_details_change_req_date" => null,
            );

            $historyArr = array(
                "user_id"=>$user_id,
                'old_data' => json_encode($oldData),
                'new_data' => json_encode($newData),
                "user_details_menu_type"=>$user_details_menu_type,
                "created_date"=>date('Y-m-d H:i:s'),
            );
            $q = $d->update("users_master", $userDetailArr, "user_id='$user_id'");
            $q = $d->insert("user_details_change_history_master", $historyArr);
            if ($q > 0) {
                $_SESSION['msg'] = "User Data Updated.";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Data Updated");
                header("location:../employeeDetailChangeRequest");exit();
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("location:../employeeDetailChangeRequest");exit();
            }
        }
    }
    if (isset($_POST['rejectEmployeeDetailChangeRequest'])) {
        
        if($user_details_menu_type == 1){
            $userDetailArr = array(
                "personal_info_change_req" => null,
                "personal_info_req_date" => null,
            );

            $q = $d->update("users_master", $userDetailArr, "user_id='$user_id'");
            
            if ($q > 0) {
                $_SESSION['msg'] = "User Data Updated.";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Data Updated");
                header("location:../employeeDetailChangeRequest");exit();
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("location:../employeeDetailChangeRequest");exit();
            }
        }elseif($user_details_menu_type == 0){
            
            $userDetailArr = array(
                "contact_details_change_req" => null,
                "contact_details_change_req_date" => null,
            );

            $q = $d->update("users_master", $userDetailArr, "user_id='$user_id'");
            if ($q > 0) {
                $_SESSION['msg'] = "User Data Updated.";
                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Data Updated");
                header("location:../employeeDetailChangeRequest");exit();
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("location:../employeeDetailChangeRequest");exit();
            }
        }
    }
} else {
    header('location:../login');
}
