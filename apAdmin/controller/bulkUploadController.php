<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));



if (isset($_REQUEST["ExportUsers"]))
{
	$contents = "No,Department,Employee Id,Designation,First Name,Last Name,Country Code,Mobile,Email,Joining Date,Zone,Level,Shift Code,Sister Company Code\n";
	$contents = strip_tags($contents);
	header("Content-Disposition: attachment; filename=UsersImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;exit;
}

if (isset($_POST['importUser'])) {
	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");
			$invalidDataAry = array();
		 	$i=1;
			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				$rowNo = $i++;
				if ($flag) {
					$flag = false;
					// $_SESSION['msg1']="Enter Valid Data";
					// header("location:../companyEmployees?bId=$block_id");
					continue;
				}

				$department_name = $getData[1];
				$unit_name = $getData[2];
				$designation = $getData[3];
				$user_first_name = $getData[4];
				$user_last_name = $getData[5];
				$country_code = $getData[6];
				$user_mobile = $getData[7];
				$user_email = $getData[8];
				$joining_date = $getData[9];
				$zone_name = $getData[10];
				$level_name = $getData[11];
				$shift_time_id = $getData[12];
				$shift_time_id = str_replace("S", "", $shift_time_id);
				$shift_time_id = str_replace("s", "", $shift_time_id);

				$sister_company_id = $getData[13];
				$sister_company_id = str_replace("C", "", $sister_company_id);
				$sister_company_id = str_replace("c", "", $sister_company_id);

				if ($unit_name == "") {
					$unit_name = "No Number";
				}

				$user_mobile = (int)$user_mobile;


				$q3 = $d->select("floors_master", "society_id='$society_id' AND floor_name='$department_name' AND block_id='$block_id'", "");
				$areaData = mysqli_fetch_array($q3);
				$floor_id = $areaData['floor_id'];

				$qu=$d->select("users_master","user_mobile='$user_mobile' AND user_mobile!='0' ");

				if($sister_company_id != "")
				{
					$qscm = $d->selectRow("sister_company_id","sister_company_master","sister_company_id = '$sister_company_id' AND society_id = '$society_id'");
					if(mysqli_num_rows($qscm) > 0)
					{
						$chk = true;
					}
					else
					{
						$chk = false;
					}
				}
				else
				{
					$chk = true;
				}

				if ($block_id != 0 && $floor_id != "" && $floor_id != 0 && $user_mobile != '' && $designation != "" && strlen($user_mobile)>=8 && strlen($user_mobile)<=15 && mysqli_num_rows($qu) == 0 && $chk)
				{
					if ($zone_name>0) {
						$q5 = $d->select("zone_master", "society_id='$society_id' AND zone_name='$zone_name' ", "");
						$zoneData = mysqli_fetch_array($q5);
						$zone_id = $areaData['zone_id'];
					}

					if ($level_name>0) {
						$q6 = $d->select("employee_level_master", "society_id='$society_id' AND level_name='$level_name' ", "");
						$levelData = mysqli_fetch_array($q6);
						$level_id = $levelData['level_id'];
					}


					if ($joining_date!="") {
						$joining_date = date("Y-m-d", strtotime($joining_date));

					}
					$m->set_data('society_id', $society_id);
					$m->set_data('sister_company_id', $sister_company_id);
					$m->set_data('shift_time_id', $shift_time_id);
					$m->set_data('user_full_name', $user_first_name . ' ' . $user_last_name);
					$m->set_data('user_first_name', $user_first_name);
					$m->set_data('user_last_name', $user_last_name);
					$m->set_data('user_mobile', $user_mobile);
					$m->set_data('user_email', $user_email);
					$m->set_data('user_type', 1);
					$m->set_data('block_id', $block_id);
					$m->set_data('floor_id', $floor_id);
					$m->set_data('user_status', 1);
					$m->set_data('unit_status', 1);
					$m->set_data('user_type', 0);
					$m->set_data('country_code', '+' . $country_code);
					$m->set_data('designation', $designation);
					$m->set_data('joining_date', $joining_date);
					$m->set_data('zone_id', $zone_id);
					$m->set_data('level_id', $level_id);

					$au = array(
						'society_id' =>  $m->get_data('society_id'),
						'block_id' => $m->get_data('block_id'),
						'floor_id' => $m->get_data('floor_id'),
						'unit_name' => $unit_name,
						'unit_status' => 1,
					);

					$qunit = $d->insert("unit_master", $au);
					$unit_id = $con->insert_id;
					$m->set_data('unit_id', $unit_id); 
					

					$a = array(
						'society_id' => $m->get_data('society_id'),
						'sister_company_id' => $m->get_data('sister_company_id'),
						'shift_time_id' => $m->get_data('shift_time_id'),
						'user_full_name' => $m->get_data('user_full_name'),
						'user_first_name' => $m->get_data('user_first_name'),
						'user_last_name' => $m->get_data('user_last_name'),
						'user_mobile' => $m->get_data('user_mobile'),
						'user_email' => $m->get_data('user_email'),
						'user_type' => $m->get_data('user_type'),
						'user_type' => $m->get_data('user_type'),
						'block_id' => $m->get_data('block_id'),
						'floor_id' => $m->get_data('floor_id'),
						'unit_id' => $m->get_data('unit_id'),
						'user_status' => $m->get_data('user_status'),
						'country_code' => $m->get_data('country_code'),
						'register_date' => date("Y-m-d H:i:s"),
						'user_designation' => $m->get_data('designation'),
						'zone_id' => $m->get_data('zone_id'),
						'level_id' => $m->get_data('level_id'),
					);


					$q = $d->insert("users_master", $a);
					$user_id = $con->insert_id;
					$m->set_data('user_id', $user_id);
					$user_sort_arr = [
						'user_sort' => $m->get_data('user_id')
					];
					$d->update("users_master", $user_sort_arr, "user_id='$user_id'");


					$compArray = array(
						'user_id' => $user_id,
						'society_id' => $m->get_data('society_id'),
						'unit_id' => $m->get_data('unit_id'),
						'designation' => $m->get_data('designation'),
						'joining_date' => $m->get_data('joining_date'),
					);

					$d->insert("user_employment_details", $compArray);


					if ($country_code == "91") {
						$country_code = "+" . $country_code;
						$smsObj->send_welcome_message($society_id, $user_mobile, $user_first_name, $societyName, $country_code);
						$d->add_sms_log($user_mobile, "Member Welcome Message", $society_id, $country_code, 4);
					}

				}
				else
				{
					if (mysqli_num_rows($qu)>0)
					{
						$data=mysqli_fetch_array($qu);
						if ($data['delete_status']==1) {
							$remark  ="Register in Ex Employee";
						} else if ($data['user_status']==0) {
							$remark  ="Request Pending";
						} else {
							$remark  ="Already Register";
						}
					}
					else if($block_id == 0)
					{
						$remark ="Invalid Branch";
					}
					else if($floor_id == 0)
					{
						$remark ="Invalid Department";
					}
					else if($user_mobile == ''  )
					{
						$remark ="Mobile Number Missing";
					}
					else if($user_mobile == 0)
					{
						$remark ="Invadid 0 Mobile Number";
					}
					else if(strlen($user_mobile)<8)
					{
						$remark ="Invalid Mobile Min. Length ";
					}
					else if(strlen($user_mobile)>15 )
					{
						$remark ="Invalid Mobile Max. Length";
					}
					else if(mysqli_num_rows($qscm) == 0)
					{
						$remark = "Sister company code is invalid";
					}
					else
					{
						$remark = mysqli_num_rows($qu);
					}
					// invalid data array
	      			$invalidData = array(
	      				"full_name" => $getData[4].' '.$getData[5]."-".$getData[3],
	      				"user_mobile" => $user_mobile,
	      				"rowNo" => $rowNo,
	      				"remark" => $remark,
	      			);
	      			array_push($invalidDataAry, $invalidData);
				}

				 $_SESSION['invalidDataAry'] = $invalidDataAry;
				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../companyEmployees?bId=$block_id");
				// $sql = "UPDATE  questions SET question_name='$getData[1]',answer1='$getData[2]',answer2='$getData[3]',answer3='$getData[4]',answer4='$getData[5]',answer='$getData[6]'  WHERE id='$getData[0]'";
				//     $result = mysqli_query($con, $sql);

			}
			
			$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk User Csv Added");

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../companyEmployees");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../companyEmployees");
	}
}


if (isset($_REQUEST["ExportParking"])) {

	$contents = "No,Parking Id,Parking Area,Parking Name,Parking Type,Employee Mobile,Employee Name,Vehicle No 1,Vehicle No 2,Vehicle No 3\n";

	$q = $d->select("parking_master,society_parking_master", "parking_master.society_parking_id=society_parking_master.society_parking_id AND parking_master.society_id='$society_id'", "");


	$i = 1;
	//While loop to fetch the records
	while ($row = mysqli_fetch_array($q)) {
		$row = array_map("html_entity_decode", $row);

		$uq = $d->select("users_master", "delete_status=0 AND unit_id='$row[unit_id]' AND user_status=1");
		$userData = mysqli_fetch_array($uq);

		if ($row['parking_type'] == 0) {
			$status = "CAR";
		} else if ($row['parking_type'] == 1) {
			$status = "BIKE";
		}
		$contents .= $i++ . ",";
		$contents .= $row['parking_id'] . ",";
		$contents .= $row['socieaty_parking_name'] . ",";
		$contents .= $row['parking_name'] . ",";
		$contents .= $status . ",";
		$contents .= $userData['user_mobile'] . ",";
		$contents .= $userData['user_full_name'] . ",";
		$contents .= $row['vehicle_no'] . "\n";
	}

	$contents = strip_tags($contents);

	header("Content-Disposition: attachment; filename=ParkingImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}



if (isset($_POST['importParking'])) {
	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {
			$extension = array("csv");
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

			if (!in_array($ext, $extension)) {
				$_SESSION['msg1'] = "Invalid File Format";
				header("location: ../parkings");
				exit;
			}
			$user_mobile_q = $d->selectRow("user_mobile", "users_master", "delete_status = 0 AND society_id='$society_id' AND user_status=1", "");
			$user_mobile_arr = [];
			while ($user_mobile_data = $user_mobile_q->fetch_assoc()) {
				$user_mobile_arr[] = $user_mobile_data['user_mobile'];
			}

			$file = fopen($filename, "r");
			$ln = 1;
			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../companyEmployees?bId=$block_id");
					continue;
				}
				$user_mobile = $getData[5];
				if (!in_array($user_mobile, $user_mobile_arr) && !empty($user_mobile) && $user_mobile != "") {
					$_SESSION['msg1'] = "Mobile number is not registered";
					header("location:../parkings");
					exit;
				}
				$ln++;
			}

			$flag = true;
			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

				if ($flag) {
					$flag = false;
					// $_SESSION['msg1']="Enter Valid Data";
					header("location:../companyEmployees?bId=$block_id");
					continue;
				}

				$parking_id = $getData[1];
				$parking_name = $getData[3];
				$user_mobile = $getData[5];
				$user_full_name = $getData[6];
				if ($getData[8] != '' && $getData[8] != '' && $getData[9] != '') {
					$vehicle_no = $getData[7] . ',' . $getData[8] . ',' . $getData[9];
				} else if ($getData[7] != '' && $getData[8] != '') {
					$vehicle_no = $getData[7] . ',' . $getData[8];
				} else {
					$vehicle_no = $getData[7];
				}

				// find block & Block id

				$q3 = $d->select("users_master", "delete_status=0 AND society_id='$society_id' AND user_mobile='$user_mobile' AND user_status=1", "");

				$blockData = mysqli_fetch_array($q3);
				$block_id = $blockData['block_id'];
				$floor_id = $blockData['floor_id'];
				$unit_id = $blockData['unit_id'];
				$user_fcm = $blockData['user_token'];
				$device = $blockData['device'];

				$user_password = bin2hex(openssl_random_pseudo_bytes(4));

				$m->set_data('society_id', $society_id);
				$m->set_data('block_id', $block_id);
				$m->set_data('floor_id', $floor_id);
				$m->set_data('parking_name', $parking_name);
				$m->set_data('unit_id', $unit_id);
				if ($user_mobile != '') {
					$m->set_data('parking_status', 1);
				} else {
					$m->set_data('parking_status', 0);
				}
				$m->set_data('vehicle_no', $vehicle_no);

				$a = array(
					'society_id' => $m->get_data('society_id'),
					'block_id' => $m->get_data('block_id'),
					'floor_id' => $m->get_data('floor_id'),
					'unit_id' => $m->get_data('unit_id'),
					'parking_name' => $m->get_data('parking_name'),
					'parking_status' => $m->get_data('parking_status'),
					'vehicle_no' => $m->get_data('vehicle_no'),
				);
				// print_r($a);
				$q = $d->update("parking_master", $a, "parking_id='$parking_id'");

				if ($blockData > 0) {

					$q = $d->update("parking_master", $a, "parking_id='$parking_id'");
					if ($device == 'android') {
						$nResident->noti("", "", $society_id, $user_fcm, "Parking Allocated", "by Admin", 'myparking');
					} else if ($device == 'ios') {
						$nResident->noti_ios("", "", $society_id, $user_fcm, "Parking Allocated", "by Admin", 'myparking');
					}
					// $nResident->noti("","",$society_id,$user_fcm,"Parking Allocated","by Admin",'myparking');

					$notiAry = array(
						'society_id' => $society_id,
						'user_id' => $data_notification['user_id'],
						'notification_title' => 'Parking Allocated',
						'notification_desc' => "by Admin",
						'notification_date' => date('Y-m-d H:i'),
						'notification_action' => 'myparking',
					);
					$d->insert("user_notification", $notiAry);
					// echo "<br>";
				}
				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../parkings");
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../parkings");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../parkings");
	}
}

/* Dollop infortech 29-10-2021  */
if (isset($_REQUEST["ExportProduct"])) {

	/* $contents="No,Product Code,Vendor Name,Product Name,Alias Name,Short Name,Category,Sub Category,Brand,Alert QTY,Unit,Other Remark\n"; */
	$contents = "No,Brand,Category,Sub Category,Product Name,Variant,Unit,Other Remark\n";

	$contents = strip_tags($contents);

	header("Content-Disposition: attachment; filename=ProductImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}


if (isset($_POST['importProduct'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {


				if ($flag) {
					$flag = false;
					//header("location:../manageProducts");
					continue;
				}
				////no	Brand	Category	Sub Category	Product Name	Variant	Unit	Other Remark	
				$Brand = $getData[1];
				$category_name = $getData[2];
				$sub_category_name = $getData[3];
				$product_name = $getData[4];
				$Variant = $getData[5];
				//$alert_qty= $getData[9];
				$unit_name = $getData[6];
				$other = $getData[7];


				/*  $qVendor = $d->select("local_service_provider_users","society_id='$society_id' AND service_provider_name='$vendor_name'","service_provider_delete_status=0 AND service_provider_status=0");
					 $vendorData=mysqli_fetch_array($qVendor); */
				//	if($vendorData){

				$qUnit = $d->select("unit_measurement_master", "society_id='$society_id' AND unit_measurement_name='$unit_name'");
				$unitData = mysqli_fetch_array($qUnit);
				$unit_measurement_id = $unitData['unit_measurement_id'];

				$qCategory = $d->select("product_category_master", "society_id='$society_id' AND category_name='$category_name'");
				$categoryData = mysqli_fetch_array($qCategory);
				$product_category_id = $categoryData['product_category_id'];
				$qSubCategory = $d->select("product_sub_category_master", "society_id='$society_id' AND sub_category_name='$sub_category_name' AND product_category_id='$product_category_id'");
				$subCategoryData = mysqli_fetch_array($qSubCategory);
				$product_sub_category_id = $subCategoryData['product_sub_category_id'];

				if ($product_name != "" && $unit_measurement_id != "" && $unit_measurement_id != 0 && $product_category_id != "" && $product_category_id != 0) {
					$m->set_data('society_id', $society_id);
					$m->set_data('product_name', $product_name);
					$m->set_data('product_brand', $Brand);
					$m->set_data('product_category_id', $product_category_id);
					$m->set_data('product_sub_category_id', $product_sub_category_id);
					$m->set_data('unit_measurement_id', $unit_measurement_id);
					$m->set_data('other', $other);
					$product_aar = array(
						'society_id' =>  $m->get_data('society_id'),
						'product_name' => $m->get_data('product_name'),
						'product_brand' => $m->get_data('product_brand'),
						'product_category_id' => $m->get_data('product_category_id'),
						'product_sub_category_id' => $m->get_data('product_sub_category_id'),
						'unit_measurement_id' => $m->get_data('unit_measurement_id'),
						'other' => $m->get_data('other'),
						'product_created_date' => date('Y-m-d H:i:s'),
					);
					/* print_r($product_aar);
					print_r($product_name);
					print_r($subCategoryData); */
					$checkProduct = $d->selectRow('product_master.*',"product_master","product_name='$product_name'");
					if(mysqli_num_rows($checkProduct)){
						$checkProductData = mysqli_fetch_assoc($checkProduct);
						$product_id = $checkProductData['product_id'];
						$qPr = $d->update("product_master", $product_aar, "product_id =' $product_id'");
					}else
					{
						$qunit = $d->insert("product_master", $product_aar);
						$product_id = $con->insert_id;
					}

					if ($Variant != "") {
						$Variant = $Variant;
					} else {
						$Variant = "Regular";
					}
					$prAr = array(
						'product_id' => $product_id,
						'product_variant_name' => $Variant,
						'created_date' => date('Y-m-d H:i:s'),
					);
					$checkProductVer = $d->selectRow('product_variant_master.*', 'product_variant_master', "product_variant_name='$Variant' AND product_id= $product_id");

					$product_variant_master = mysqli_fetch_assoc($checkProductVer);

					if ($product_variant_master) {
						$qPr = $d->update("product_variant_master", $prAr, "product_variant_id =' $product_variant_master [product_variant_id]'");
					} else {
						$qPr = $d->insert("product_variant_master", $prAr);
					}
				}
				$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Product CSV Added");
				//die;
				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../manageProducts?cId=$product_category_id");
				//}
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../manageProducts");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../manageProducts");
	}
}

if (isset($_REQUEST["ExportProductPrice"])) {

	/* $contents="No,Product Code,Vendor Name,Product Name,Alias Name,Short Name,Category,Sub Category,Brand,Alert QTY,Unit,Other Remark\n"; */
	$contents = "No,Category Name,Subcategory Name,Product Name,Variant,Vendor,Minimum Order Quantity,Product Price";

	$contents = strip_tags($contents);

	header("Content-Disposition: attachment; filename=ProductPricesImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}
/* Dollop infotech 29-10-2021  */

if (isset($_POST['importProductPrices'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../productPrices");
					continue;
				}

				$category_name = $getData[1];
				$sub_category_name = $getData[2];
				$product_name = $getData[3];
				$Variant = $getData[4];
				$vendor_name = $getData[5];
				$minimum_order_quantity = $getData[6];
				$product_price = $getData[7];

				$qVendor = $d->select("local_service_provider_users", "society_id='$society_id' AND service_provider_name='$vendor_name' AND service_provider_delete_status=0 AND service_provider_status=0");
				$vendorData = mysqli_fetch_array($qVendor);

				if ($vendorData) {
					$vendor_id = $vendorData['service_provider_users_id'];
					$qCategory = $d->select("product_category_master", "society_id='$society_id' AND category_name='$category_name'");
					$categoryData = mysqli_fetch_array($qCategory);
					$product_category_id = $categoryData['product_category_id'];

					$qSubCategory = $d->select("product_sub_category_master", "society_id='$society_id' AND sub_category_name='$sub_category_name' AND product_category_id='$product_category_id'");
					$subCategoryData = mysqli_fetch_array($qSubCategory);
					$product_sub_category_id = $subCategoryData['product_sub_category_id'];


					$checkProduct = $d->selectRow('product_master.*', 'product_master', "product_name='$product_name'");
					$productmaster = mysqli_fetch_assoc($checkProduct);
					$product_id = $productmaster['product_id'];

					$checkProductVer = $d->selectRow('product_variant_master.*', 'product_variant_master', "product_variant_name='$Variant' AND product_id= $product_id");
					$product_variant_master = mysqli_fetch_assoc($checkProductVer);
					$product_variant_id = $product_variant_master['product_variant_id'];

					if ($product_name != "" && $product_price != "" && $product_price != 0 && $product_category_id != "" && $product_category_id != 0) {
						//$m->set_data('society_id', $society_id);
						$m->set_data('product_variant_id', $product_variant_id);
						//$m->set_data('product_category_id', $product_category_id);
						//$m->set_data('product_sub_category_id', $product_sub_category_id);
						$m->set_data('product_id', $product_id);
						$m->set_data('vendor_id', $vendor_id);
						$m->set_data('minimum_order_quantity', $minimum_order_quantity);
						$m->set_data('product_price', $product_price);

						$product_aar = array(

							'product_id' => $m->get_data('product_id'),
							'product_variant_id' => $m->get_data('product_variant_id'),
							'vendor_id' => $m->get_data('vendor_id'),
							'minimum_order_quantity' => $m->get_data('minimum_order_quantity'),
							'product_price' => $m->get_data('product_price'),
							'price_added_date' => date('Y-m-d H:i:s'),
						);

						$qunit = $d->insert("product_price_master", $product_aar);

						if ($product_sub_category_id != "") {
							$checkSubCatVendor = $d->selectRow('product_sub_category_vendor_master.*', 'product_sub_category_vendor_master', " product_sub_category_id= $product_sub_category_id AND vendor_id = $vendor_id");
							if (mysqli_num_rows($checkSubCatVendor) == 0) {
								$SubCatVnda1 = array(
									'society_id' => $society_id,
									'product_sub_category_id' => $product_sub_category_id,
									'vendor_id' => $vendor_id,
									'created_date' => date("Y-m-d H:i:s"),
								);
								$q2 = $d->insert("product_sub_category_vendor_master", $SubCatVnda1);
								$_SESSION['msg'] = "vendor Sub Category Added Successfully";
								$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor Sub Category Added Successfully");
							}
						}
						$checkCatVendor = $d->selectRow('product_category_vendor_master.*', 'product_category_vendor_master', " product_category_id= $product_category_id AND vendor_id = $vendor_id");
						if (mysqli_num_rows($checkCatVendor) == 0) {
							$CatVnda1 = array(
								'society_id' => $society_id,
								'product_category_id' => $product_category_id,
								'vendor_id' => $vendor_id,
								'created_date' => date("Y-m-d H:i:s"),
							);
							$q3 = $d->insert("product_category_vendor_master", $CatVnda1);
							$_SESSION['msg'] = "vendor  Category Added Successfully";
							$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor  Category Added Successfully");
						}
					}
					$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Product Price CSV Added");

					$_SESSION['msg'] = "CSV Uploaded Successfully";
					header("location:../productPrices?cId=$product_category_id");
				}
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../productPrices");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../productPrices");
	}
}
if (isset($_REQUEST["ExportProductCategory"])) {

	$contents = "No,Category Name,Description";
	$contents = strip_tags($contents);
	header("Content-Disposition: attachment; filename=ProductCategoryImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}


if (isset($_POST['importProductCategory'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../categoryMaster");
					continue;
				}
				$category_name = $getData[1];
				$category_description = $getData[2];
				if ($category_name != "") {
					$m->set_data('category_name', $category_name);
					$m->set_data('society_id', $society_id);
					$m->set_data('category_description', $category_description);
					$catAr = array(
						'category_name' => $m->get_data('category_name'),
						'category_description' => $m->get_data('category_description'),
						'society_id' => $m->get_data('society_id'),
						'product_category_created_date' => date('Y-m-d H:i:s'),
					);
					$qCategory = $d->select("product_category_master", "society_id='$society_id' AND category_name='$category_name'");
					$categoryData = mysqli_fetch_array($qCategory);
					if ($categoryData) {
						$product_category_id = $categoryData['product_category_id'];
						$qunit = $d->update("product_category_master", $catAr, "product_category_id=$product_category_id");
					} else {
						$qunit = $d->insert("product_category_master", $catAr);
					}
				}
				$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Product Category CSV Added");

				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../categoryMaster");
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../categoryMaster");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../categoryMaster");
	}
}

if (isset($_REQUEST["ExportProductSubCategory"])) {

	$contents = "No,Category Name,Subcategory Name,Description";
	$contents = strip_tags($contents);
	header("Content-Disposition: attachment; filename=ProductSubCategoryImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}
if (isset($_REQUEST["ExportUnitMeasurement"])) {

	$contents = "No,Unit Measurement Name,Description";
	$contents = strip_tags($contents);
	header("Content-Disposition: attachment; filename=ProductUnitMeasuerementImport" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}

if (isset($_POST['importSubProductCategory'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../subCategoryMaster");
					continue;
				}
				$category_name = $getData[1];
				$sub_category_name = $getData[2];
				$sub_category_description = $getData[3];
				if ($sub_category_name != "" && $category_name != "") {

					$qCategory = $d->select("product_category_master", "society_id='$society_id' AND category_name='$category_name'");
					$categoryData = mysqli_fetch_array($qCategory);
					$product_category_id = $categoryData['product_category_id'];
					$m->set_data('product_category_id', $product_category_id);
					$m->set_data('sub_category_name', $sub_category_name);
					$m->set_data('society_id', $society_id);
					$m->set_data('sub_category_description', $sub_category_description);
					$catAr = array(
						'sub_category_name' => $m->get_data('sub_category_name'),
						'product_category_id' => $m->get_data('product_category_id'),
						'society_id' => $m->get_data('society_id'),
						'sub_category_description' => $m->get_data('sub_category_description'),
						'product_sub_category_created_date' => date('Y-m-d H:i:s'),
					);

					$sql = $d->selectRow('product_sub_category_master.*', 'product_sub_category_master', " product_category_id=$product_category_id AND sub_category_name='$sub_category_name'");
					$subcategoryData = mysqli_fetch_assoc($sql);
					if ($subcategoryData) {
						$product_sub_category_id = $subcategoryData['product_sub_category_id'];
						$qunit = $d->update("product_sub_category_master", $catAr, "product_sub_category_id=$product_sub_category_id");
					} else {
						$qunit = $d->insert("product_sub_category_master", $catAr);
					}
				}
				$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Product Category CSV Added");

				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../subCategoryMaster");
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../subCategoryMaster");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../subCategoryMaster");
	}
}


if (isset($_POST['importUnitMeasurement'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../unitMeasurement");
					continue;
				}
				$unit_measurement_name = $getData[1];
				$unit_measurement_description = $getData[2];

				if ($unit_measurement_name != "") {

					$m->set_data('unit_measurement_name', $unit_measurement_name);
					$m->set_data('unit_measurement_description', $unit_measurement_description);
					$m->set_data('society_id', $society_id);
					$m->set_data('unit_measurement_description', $unit_measurement_description);
					$catAr = array(
						'unit_measurement_name' => $m->get_data('unit_measurement_name'),
						'society_id' => $m->get_data('society_id'),
						'unit_measurement_description' => $m->get_data('unit_measurement_description'),
						'unit_measurement_created_date' => date('Y-m-d H:i:s'),
					);
					$checkUnit = $d->selectRow('unit_measurement_master.*', 'unit_measurement_master', "unit_measurement_name = '$unit_measurement_name'");
					$unit_measurement_data = mysqli_fetch_assoc($checkUnit);
					if ($unit_measurement_data) {
						$unit_measurement_id = $unit_measurement_data['unit_measurement_id'];
						$q = $d->update("unit_measurement_master", $catAr, "unit_measurement_id ='$unit_measurement_id'");
					} else {
						$q = $d->insert("unit_measurement_master", $catAr);
						$_SESSION['msg'] = "Unit Measurement Added Successfully";
					}
				}
				$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Product Category CSV Added");
				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../unitMeasurement");
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../unitMeasurement");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../unitMeasurement");
	}
}

if (isset($_REQUEST["ExporLocalserviceProvider"])) {

	$contents = "Comapany Name,Contact Person Name,Phone Number,Alternate Num,Email Id,Address";
	$contents = strip_tags($contents);
	header("Content-Disposition: attachment; filename=ServiceProvider" . date('Y-m-d-h-i') . ".csv");
	print $contents;
}


if (isset($_POST['importServiceProvider'])) {

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if ($ext == 'csv') {
		$flag = true;
		$filename = $_FILES["file"]["tmp_name"];

		if ($_FILES["file"]["size"] > 0) {

			$file = fopen($filename, "r");

			while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
				if ($flag) {
					$flag = false;
					header("location:../vendors");
					continue;
				}
				//"Comapany Name,Contact Person Name,Phone Number,Alternate Num,Email Id,Address,";
				$service_provider_name = $getData[0];
				$contact_person_name = $getData[1];
				$service_provider_phone = $getData[2];
				$service_provider_alternate_phone = $getData[3];
				$service_provider_email = $getData[4];
				$service_provider_address = $getData[5];
				
				if ($service_provider_name != "" && $contact_person_name !="" && $service_provider_phone !="") {

					$m->set_data('service_provider_name', $service_provider_name);
					$m->set_data('contact_person_name', $contact_person_name);
					$m->set_data('society_id', $_COOKIE['society_id']);
					$m->set_data('service_provider_phone', $service_provider_phone);
					$m->set_data('service_provider_email', $service_provider_email);
					$m->set_data('service_provider_address', $service_provider_address);
					$m->set_data('service_provider_alternate_phone', $service_provider_alternate_phone);
					$catAr = array(
						'service_provider_alternate_phone' => $m->get_data('service_provider_alternate_phone'),
						'society_id' => $m->get_data('society_id'),
						'service_provider_name' => $m->get_data('service_provider_name'),
						'service_provider_phone' => $m->get_data('service_provider_phone'),
						'service_provider_email' => $m->get_data('service_provider_email'),
						'contact_person_name' => $m->get_data('contact_person_name'),
						'service_provider_address' => $m->get_data('service_provider_address'),
						
					);
			
						$q = $d->insert("local_service_provider_users", $catAr);
						$_SESSION['msg'] = "Service Add Successfully";
					
				}
				$d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Bulk Service Category CSV Added");
				$_SESSION['msg'] = "CSV Uploaded Successfully";
				header("location:../vendors");
			}

			fclose($file);
		} else {
			$_SESSION['msg1'] = "Please Upload Valid CSV File";
			header("location:../vendors");
		}
	} else {
		$_SESSION['msg1'] = "Please Upload CSV File";
		header("location:../vendors");
	}
}