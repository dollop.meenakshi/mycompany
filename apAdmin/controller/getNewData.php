<?php
session_start();
if(isset($_COOKIE['bms_admin_id']))
{
   header("location:../welcome");
}
$url=$_SESSION['url'] ; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
$base_url=$m->base_url();
if(isset($getData)) {


    $today = date('Y-m-d');
    $day = date('w');
    $week_start = date("Y-m-d", strtotime('monday this week')); 
    $month_start = date("Y-m-01"); 
    $prev_month_start = date("Y-m-01",strtotime('-1 month')); 
    $prev_month_end = date("Y-m-t",strtotime('-1 month')); 

    // $society_id=59;
	$q=$d->select("society_master","society_id='$society_id'");
    $data=mysqli_fetch_array($q);

    // $serviceProviders=$d->count_data_direct("service_provider_users_id","local_service_provider_users","society_id='$data[society_id]'");
    $newsFeed=$d->count_data_direct("feed_id","news_feed","society_id='$data[society_id]'");
    $newsFeedToday=$d->count_data_direct("feed_id","news_feed","society_id='$data[society_id]' AND DATE(modify_date) = CURDATE()");
    $todayActiveUser=$d->count_data_direct("user_id","users_master","society_id='$data[society_id]' AND DATE(last_login) = CURDATE()");
    $weeklyActiveUser=$d->count_data_direct("user_id","users_master","society_id='$data[society_id]' AND last_login > DATE_SUB(NOW(), INTERVAL 1 WEEK) ");
    $monthActiveUser=$d->count_data_direct("user_id","users_master","society_id='$data[society_id]' AND last_login > DATE_SUB(NOW(), INTERVAL 1 MONTH) ");

    $todayRegisterUser=$d->count_data_direct("user_id","users_master","society_id='$data[society_id]' AND DATE(register_date) = CURDATE() AND user_status=1");

    $totalOwners=$d->count_data_direct("user_id","unit_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND unit_master.unit_status=1 AND users_master.user_type=0 AND users_master.member_status=0 "); 

    $totalTenants=$d->count_data_direct("user_id","unit_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND unit_master.unit_status=3 AND users_master.user_type=0 AND users_master.member_status=1 "); 
    $androidUser=$d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device='android' AND users_master.user_status=1");
   
    $iosUsers = $d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device='ios'  AND users_master.user_status=1 ");

    $nqCount=$d->select("users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND users_master.unit_id=unit_master.unit_id  AND unit_master.unit_status=4 AND users_master.society_id='$society_id' ","ORDER BY users_master.user_id DESC ");
    $toatlPendingPrimary = mysqli_num_rows($nqCount);

    $pendingUsers = $toatlPendingPrimary;

    
    $totalWorkFromHome=$d->count_data_direct("wfh_id","wfh_master","society_id='$society_id'");
    $totalSalarySlip=$d->count_data_direct("salary_slip_id","salary_slip_master","society_id='$society_id'");
    
    $totalLeaves=$d->count_data_direct("leave_id","leave_master","society_id='$society_id'");
    $totalWorkReport=$d->count_data_direct("work_report_id","work_report_master","society_id='$society_id'");

    $totalWorkReportWeek=$d->count_data_direct("work_report_id","work_report_master","society_id='$society_id' AND work_report_date BETWEEN '$week_start' AND '$today'");
    
    $totalWorkReportMonth=$d->count_data_direct("work_report_id","work_report_master","society_id='$society_id' AND work_report_date BETWEEN '$month_start_date' AND '$today'");

    $totalWorkReportPrvMonth=$d->count_data_direct("work_report_id","work_report_master","society_id='$society_id' AND work_report_date BETWEEN '$prev_month_start' AND '$prev_month_end'");

    $totalDarWorkReport=$d->count_data_direct("work_report_employee_ans_id","work_report_employee_ans","society_id='$society_id'");
    $totalDarWorkReportWeek=$d->count_data_direct("work_report_employee_ans_id","work_report_employee_ans","society_id='$society_id' AND work_report_date BETWEEN '$week_start' AND '$today'");
    $totalDarWorkReportMonth=$d->count_data_direct("work_report_employee_ans_id","work_report_employee_ans","society_id='$society_id' AND work_report_date BETWEEN '$month_start_date' AND '$today'");
    $totalDarWorkReportPrvMonth=$d->count_data_direct("work_report_employee_ans_id","work_report_employee_ans","society_id='$society_id' AND work_report_date BETWEEN '$prev_month_start' AND '$prev_month_end'");



    $totalAttendance=$d->count_data_direct("attendance_id","attendance_master","society_id='$society_id'");
    $totalAttendanceWeekly=$d->count_data_direct("attendance_id","attendance_master","society_id='$society_id' AND attendance_date_start BETWEEN '$week_start' AND '$today'");
    $totalAttendanceThisMonth=$d->count_data_direct("attendance_id","attendance_master","society_id='$society_id' AND attendance_date_start BETWEEN '$month_start' AND '$today'");
    $totalAttendancePrevMonth=$d->count_data_direct("attendance_id","attendance_master","society_id='$society_id' AND attendance_date_start BETWEEN '$prev_month_start' AND '$prev_month_end'");

    $totalAssets=$d->count_data_direct("assets_id","assets_item_detail_master","society_id='$society_id'");



     $f = '../../img';
    $io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
    $size = fgets ( $io, 4096);
    $size = substr ( $size, 0, strpos ( $size, "\t" ) );
    $sizeMb = round($size/1024);
    pclose ( $io );
    // echo 'Directory: ' . $f . ' => Size: ' . $sizeMb;



     $a = array(
                'society_id' => $data['society_id'],
                'society_name' => $data['society_name'],
                'plan_expire_date' => $data['plan_expire_date'],
                'parkings' => $parkings,
                'androidUser' => $androidUser,
                'iosUsers' => $iosUsers,
                'totalOwners' => $totalOwners,
                'totalTenants' => $totalTenants,
                'pendingUsers' => $pendingUsers,
                'serviceProviders' => "",
                'totalUnits' => $totalUnits,
                'newsFeed' => $newsFeed,
                'newsFeedToday' => $newsFeedToday,
                'todayActiveUser' => $todayActiveUser,
                'todayRegisterUser' => $todayRegisterUser,
                'weeklyActiveUser' => $weeklyActiveUser,
                'monthActiveUser' => $monthActiveUser,
            );


} ?>
    <h4><?php echo $data['society_name'];?></h4>
<table class="table table-bordered">
    <tr>
        <th>Plan Expire Date</th>
        <td><?php echo $data['plan_expire_date'];?></td>
    </tr>
    <tr>
        <th>Employees</th>
        <td><?php echo $totalOwners;?></td>
    </tr>
    <tr>
        <th>Android Employees</th>
        <td><?php echo $androidUser;?></td>
    </tr>
    <tr>
        <th>IOS Employees</th>
        <td><?php echo $iosUsers;?></td>
    </tr>
    <tr>
        <th>Today Active Employees</th>
        <td><?php echo $todayActiveUser;?></td>
    </tr>
    <tr>
        <th>Weekly Active Employees</th>
        <td><?php echo $weeklyActiveUser;?></td>
    </tr>
    <tr>
        <th>Monthly Active Employees</th>
        <td><?php echo $monthActiveUser;?></td>
    </tr>
     <tr>
        <th>Today Register Employees</th>
        <td><?php echo $todayRegisterUser;?></td>
    </tr>
    <tr>
        <th>Pending Employees</th>
        <td><?php echo $pendingUsers;?></td>
    </tr>
    <tr>
        <th>Timeline Feed</th>
        <td><?php echo $newsFeed;?></td>
    </tr>
    <tr>
        <th>Timeline Feed Today</th>
        <td><?php echo $newsFeedToday;?></td>
    </tr>
    <tr>
        <th>Timeline Feed Today</th>
        <td><?php echo $newsFeedToday;?></td>
    </tr>
    <tr>
        <th>Total Attendance </th>
        <td><?php echo $totalAttendance;?></td>
    </tr>
    <tr>
        <th>This Week Attendance</th>
        <td><?php echo $totalAttendanceWeekly;?></td>
    </tr>
    <tr>
        <th>This Month Attendance</th>
        <td><?php echo $totalAttendanceThisMonth;?></td>
    </tr>
    <tr>
        <th>Prev. Month Attendance</th>
        <td><?php echo $totalAttendancePrevMonth;?></td>
    </tr>
    <tr>
        <th>Total Work From Home</th>
        <td><?php echo $totalWorkFromHome;?></td>
    </tr>
    <tr>
        <th>Total Salary Generated</th>
        <td><?php echo $totalSalarySlip;?></td>
    </tr>
    <tr>
        <th>Total Leaves</th>
        <td><?php echo $totalLeaves;?></td>
    </tr>
    <tr>
        <th>Total Work Report</th>
        <td><?php echo $totalWorkReport;?></td>
    </tr>
    <tr>
        <th>This Week Work Report</th>
        <td><?php echo $totalWorkReportWeek;?></td>
    </tr>
    <tr>
        <th>This Month Work Report</th>
        <td><?php echo $totalWorkReportMonth;?></td>
    </tr>
    <tr>
        <th>Prev. Month Work Report</th>
        <td><?php echo $totalWorkReportPrvMonth;?></td>
    </tr>
    <tr>
        <th>Total DAR Work Report</th>
        <td><?php echo $totalDarWorkReport;?></td>
    </tr>
    <tr>
        <th>This DAR Week Work Report</th>
        <td><?php echo $totalDarWorkReportWeek;?></td>
    </tr>
    <tr>
        <th>This DAR Month Work Report</th>
        <td><?php echo $totalDarWorkReportMonth;?></td>
    </tr>
    <tr>
        <th>Prev. DAR Month Work Report</th>
        <td><?php echo $totalDarWorkReportPrvMonth;?></td>
    </tr>
    <tr>
        <th>Total Assets</th>
        <td><?php echo $totalAssets;?></td>
    </tr>
    <tr>
        <th>Photo Storage</th>
        <td><?php echo $sizeMb,' MB' ;?></td>
    </tr>
</table>
