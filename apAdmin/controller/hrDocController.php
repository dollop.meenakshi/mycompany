<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);

    if (isset($_POST['deleteDocument']) && $hr_document_id>0)
    {

        $q = $d->update("hr_document_master", array("hr_doc_deleted" =>'1'), "hr_document_id ='$hr_document_id'");

        if ($q == true)
        {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Deleted ($hr_document_id)");

            $_SESSION['msg'] = "Document Deleted Successfully";
            header("Location: ../hrDoc?cId=$cId");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../hrDoc");
        }
    }

    if (isset($_POST['addHrDocument']))
    {
        $m->set_data('user_id', $user_id);
        $m->set_data('unit_id', $unit_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('hr_document_category_id', $hr_document_category_id);
        $m->set_data('hr_document_sub_category_id', $hr_document_sub_category_id);
        $m->set_data('document_type', $document_type);
        $m->set_data('hr_document_name', $hr_document_name);
        $m->set_data('is_terms_and_conditions', $is_terms_and_conditions);
        $m->set_data('hr_document_uploaded_date', date("Y-m-d H:i:s"));
        $m->set_data('hr_document_uploaded_by', $_COOKIE['bms_admin_id']);

        if ($document_type == 0)
        {
            $file = $_FILES['hr_document_file']['tmp_name'];
            if (file_exists($file))
            {
                $extensionResume = array(
                    "pdf",
                    "PDF"
                );
                $extId = pathinfo($_FILES['hr_document_file']['name'], PATHINFO_EXTENSION);

                $errors = array();
                $maxsize = 26214400;

                if (($_FILES['hr_document_file']['size'] >= $maxsize) || ($_FILES["hr_document_file"]["size"] == 0))
                {
                    $_SESSION['msg1'] = "Document too large. Must be less than 25 MB.";
                    header("location:../hrDoc");
                    exit();
                }
                if (!in_array($extId, $extensionResume) && (!empty($_FILES["hr_document_file"]["type"])))
                {
                    $_SESSION['msg1'] = "Invalid Document format, Only Pdf and Doc is allowed.";
                    header("location:../hrDoc");
                    exit();
                }
                if (count($errors) === 0)
                {
                    $image_Arr = $_FILES['hr_document_file'];
                    $temp = explode(".", $_FILES["hr_document_file"]["name"]);
                    $hr_document_name_temp = str_replace(" ", "_", $hr_document_name);
                    $hr_document = $hr_document_name_temp . '_' . date("Y_m_d_H_i") . '.' . end($temp);

                    move_uploaded_file($_FILES["hr_document_file"]["tmp_name"], "../../img/hrdoc/" . $hr_document);
                }
            }
            else
            {
                $hr_document = "";
            }
            if (isset($hr_document) && $hr_document != "")
            {
                $m->set_data('hr_document_file', $hr_document);
            }
            else
            {
                $m->set_data('hr_document_file', $hr_document_file_old);
            }
        }
        else
        {
            if (!filter_var($hr_document_url, FILTER_VALIDATE_URL) === false)
            {
                $m->set_data('hr_document_file', $hr_document_url);
            }
            else
            {
                $_SESSION['msg1'] = "Url is not valid";
                header("location:../addHrDoc");
            }
            $m->set_data('hr_document_file', $hr_document_url);
        }

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'floor_id' => $m->get_data('floor_id'),
            'hr_document_category_id' => $m->get_data('hr_document_category_id'),
            'hr_document_sub_category_id' => $m->get_data('hr_document_sub_category_id'),
            'hr_document_file' => $m->get_data('hr_document_file'),
            'hr_document_name' => $m->get_data('hr_document_name'),
            'document_type' => $m->get_data('document_type'),
            'is_terms_and_conditions' => $m->get_data('is_terms_and_conditions'),
            'hr_document_uploaded_date' => $m->get_data('hr_document_uploaded_date'),
            'hr_document_uploaded_by' => $m->get_data('hr_document_uploaded_by'),
        );
        $getHrCat = $d->selectRow('*', "hr_document_category_master", "hr_document_category_id='$hr_document_category_id'");
        $getHrCatdata = mysqli_fetch_assoc($getHrCat);
        $activityData = array(
            'hr_document_category_id' => $getHrCatdata['hr_document_category_id'],
            'hr_document_category_name' => $getHrCatdata['hr_document_category_name'],
        );

        if (isset($hr_document_id) && $hr_document_id > 0)
        {
            $a2 = array(
                'hr_document_file' => $m->get_data('hr_document_file'),
                'hr_document_name' => $m->get_data('hr_document_name'),
                'document_type' => $m->get_data('document_type'),
                'is_terms_and_conditions' => $m->get_data('is_terms_and_conditions'),
            );
            $q = $d->update("hr_document_master", $a2, "hr_document_id ='$hr_document_id'");
            $_SESSION['msg'] = "Document Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$hr_document_name Document Updated Successfully");
            if ($user_id_old > 0)
            {
                $q2 = $d->selectRow('user_token,device,user_full_name', "users_master", "user_id='$user_id_old'");
                $data2 = mysqli_fetch_assoc($q2);
                $title = "Document";
                $category_name = $getHrCatdata['hr_document_category_name'];
                $description = "$category_name has shared by $created_by";
                $menuClick = "hr_doc";
                $image = "";
                $activity = json_encode($activityData);
                $user_token = $data2['user_token'];
                $device = $data2['device'];
                if ($device == 'android')
                {
                    $nResident->noti($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
                }
                else
                {
                    $nResident->noti_ios($menuClick, $image, $society_id, $user_token, $title, $description, $activity);
                }
                $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.user_id = '$user_id_old'");
                $user_notification_id = $con->insert_id;

                $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.user_id = '$user_id_old'");
                $arr = [];
                for($nc = 1;$nc <= $cnt;$nc++)
                {
                    $arr[] = $user_notification_id++;
                }
                $user_notification_id_str = implode(",",$arr);
                $a212 = array(
                    'notification_data_ids' => $activity
                );
                $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
            }
            else
            {
                $activity = json_encode($activityData);
                $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id = '$floor_id_old'");
                $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id > '$floor_id_old' ");
                $title = "Document";
                $category_name = $getHrCatdata['hr_document_category_name'];
                $description = "$category_name has shared by $created_by";
                $menuClick = "hr_doc";
                $image = "";
                $nResident->noti($menuClick, $image, $society_id, $fcmArray, $title, $description, $activity);
                $nResident->noti_ios($menuClick, $image, $society_id, $fcmArrayIos, $title, $description, $activity);
                $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.floor_id = '$floor_id_old'");
                $user_notification_id = $con->insert_id;

                $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.floor_id = '$floor_id_old'");
                $arr = [];
                for($nc = 1;$nc <= $cnt;$nc++)
                {
                    $arr[] = $user_notification_id++;
                }
                $user_notification_id_str = implode(",",$arr);
                $a212 = array(
                    'notification_data_ids' => $activity
                );
                $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
            }
        }
        else
        {
            if (count($_POST['block_id']) > 0)
            {
                if (in_array('all_branch', $_POST['block_id']))
                {
                    $a1['floor_id'] = 0;
                    $a1['block_id'] = 0;
                    $a1['user_id'] = 0;
                    $q = $d->insert("hr_document_master", $a1);
                    $_SESSION['msg'] = "Document Added Successfully";
                    $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$hr_document_name Document Added Successfully");
                    $activity = json_encode($activityData);
                    $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id >0");
                    $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id >0 ");
                    $title = "Document";
                    $category_name = $getHrCatdata['hr_document_category_name'];
                    $description = "$category_name has shared by $created_by";
                    $menuClick = "hr_doc";
                    $image = "";
                    $nResident->noti($menuClick, $image, $society_id, $fcmArray, $title, $description, $activity);
                    $nResident->noti_ios($menuClick, $image, $society_id, $fcmArrayIos, $title, $description, $activity);
                    $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.floor_id > 0");
                    $user_notification_id = $con->insert_id;

                    $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.floor_id > 0");
                    $arr = [];
                    for($nc = 1;$nc <= $cnt;$nc++)
                    {
                        $arr[] = $user_notification_id++;
                    }
                    $user_notification_id_str = implode(",",$arr);
                    $a212 = array(
                        'notification_data_ids' => $activity
                    );
                    $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
                }
                else
                {
                    if (count($_POST['floor_id']) > 0)
                    {
                        if (in_array('All', $_POST['floor_id']))
                        {
                            $block_id = join("','", $_POST['block_id']);
                            $dpts2 = $d->select("floors_master", "society_id='$society_id' AND floor_status = 0 AND block_id IN ('$block_id')");
                            while ($HrCatdata2 = mysqli_fetch_array($dpts2))
                            {
                                $a1['floor_id'] = 0;
                                $a1['user_id'] = 0;
                                $a1['block_id'] = $HrCatdata2['block_id'];
                                $checkblock = $d->selectRow('hr_document_master.*', 'hr_document_master', "block_id='$HrCatdata2[block_id]' AND floor_id=0");
                                if (mysqli_num_rows($checkblock) == 0)
                                {

                                    $q = $d->insert("hr_document_master", $a1);
                                }
                                $_SESSION['msg'] = "Document Added Successfully";
                                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$hr_document_name Document Added Successfully");
                            }
                            $activity = json_encode($activityData);
                            $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id >0  AND block_id IN ('$block_id')");
                            $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id >0 AND block_id IN ('$block_id') ");
                            $title = "Document";
                            $category_name = $getHrCatdata['hr_document_category_name'];
                            $description = "$category_name has shared by $created_by";
                            $menuClick = "hr_doc";
                            $image = "";
                            $nResident->noti($menuClick, $image, $society_id, $fcmArray, $title, $description, $activity);
                            $nResident->noti_ios($menuClick, $image, $society_id, $fcmArrayIos, $title, $description, $activity);
                            $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.floor_id >0 AND users_master.block_id IN ('$block_id') ");
                            $user_notification_id = $con->insert_id;

                            $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.floor_id > 0 AND um.block_id IN ($block_id)");
                            $arr = [];
                            for($nc = 1;$nc <= $cnt;$nc++)
                            {
                                $arr[] = $user_notification_id++;
                            }
                            $user_notification_id_str = implode(",",$arr);
                            $a212 = array(
                                'notification_data_ids' => $activity
                            );
                            $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
                        }
                        else
                        {
                            if (isset($_POST['user_id']) && count($_POST['user_id']) > 0)
                            {
                                if (in_array('all_employee', $_POST['user_id']))
                                {
                                    $floor_id = join("','", $_POST['floor_id']);
                                    $q4 = $d->select("floors_master", "society_id='$society_id' AND floor_status = 0 AND floor_id IN ('$floor_id')");
                                    while ($q4data = mysqli_fetch_array($q4))
                                    {
                                        $a1['floor_id'] = $q4data['floor_id'];
                                        $a1['block_id'] = $q4data['block_id'];
                                        $a1['unit_id'] = 0;
                                        $a1['user_id'] = 0;
                                        $checkFloor = $d->selectRow('hr_document_master.*', 'hr_document_master', "floor_id='$q4data[floor_id]' AND block_id='$q4data[block_id]' AND user_id=0");
                                        if (mysqli_num_rows($checkFloor) == 0)
                                        {

                                            $q = $d->insert("hr_document_master", $a1);
                                        }

                                        $_SESSION['msg'] = "Document Added Successfully";
                                        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$hr_document_name Document Added Successfully");
                                    }
                                    $activity = json_encode($activityData);
                                    $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id >0  AND floor_id IN ('$floor_id')");
                                    $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id >0 AND floor_id IN ('$floor_id') ");
                                    $title = "Document";
                                    $category_name = $getHrCatdata['hr_document_category_name'];
                                    $description = "$category_name has shared by $created_by";
                                    $menuClick = "hr_doc";
                                    $image = "";
                                    $nResident->noti($menuClick, $image, $society_id, $fcmArray, $title, $description, $activity);
                                    $nResident->noti_ios($menuClick, $image, $society_id, $fcmArrayIos, $title, $description, $activity);
                                    $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.floor_id >0 AND users_master.floor_id IN ('$floor_id') ");
                                    $user_notification_id = $con->insert_id;

                                    $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.floor_id > 0 AND um.floor_id IN ($floor_id)");
                                    $arr = [];
                                    for($nc = 1;$nc <= $cnt;$nc++)
                                    {
                                        $arr[] = $user_notification_id++;
                                    }
                                    $user_notification_id_str = implode(",",$arr);
                                    $a212 = array(
                                        'notification_data_ids' => $activity
                                    );
                                    $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
                                }
                                else
                                {
                                    if (count($_POST['user_id']) > 0)
                                    {
                                        $user_ids = join("','", $_POST['user_id']);
                                        $q6 = $d->selectRow('user_id,user_token,device,user_full_name,floor_id,block_id,unit_id', "users_master", "user_id IN ('$user_ids')");
                                        while ($q6data = mysqli_fetch_array($q6))
                                        {
                                            $a1['floor_id'] = $q6data['floor_id'];
                                            $a1['block_id'] = $q6data['block_id'];
                                            $a1['unit_id'] = $q6data['unit_id'];
                                            $a1['user_id'] = $q6data['user_id'];
                                            $q = $d->insert("hr_document_master", $a1);
                                            $_SESSION['msg'] = "Document Added Successfully";
                                            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$hr_document_name Document Added Successfully");
                                        }
                                        $activity = json_encode($activityData);
                                        $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id >0  AND user_id IN ('$user_id')");
                                        $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id >0 AND user_id IN ('$user_id') ");
                                        $title = "Document";
                                        $category_name = $getHrCatdata['hr_document_category_name'];
                                        $description = "$category_name has shared by $created_by";
                                        $menuClick = "hr_doc";
                                        $image = "";
                                        $nResident->noti($menuClick, $image, $society_id, $fcmArray, $title, $description, $activity);
                                        $nResident->noti_ios($menuClick, $image, $society_id, $fcmArrayIos, $title, $description, $activity);
                                        $d->insertUserNotification($society_id, $title, $description, "hr_doc", "HR-docsxxxhdpi.png", "users_master.floor_id >0 AND users_master.user_id IN ('$user_id')");
                                        $user_notification_id = $con->insert_id;

                                        $cnt = $d->count_data_direct("user_id","users_master AS um JOIN unit_master AS unim ON unim.unit_id = um.unit_id JOIN block_master AS bm ON bm.block_id = unim.block_id","um.delete_status = 0 AND um.user_status = 1 AND um.user_token != '' AND um.society_id = '$society_id' AND um.floor_id > 0 AND um.user_id IN ($user_id)");
                                        $arr = [];
                                        for($nc = 1;$nc <= $cnt;$nc++)
                                        {
                                            $arr[] = $user_notification_id++;
                                        }
                                        $user_notification_id_str = implode(",",$arr);
                                        $a212 = array(
                                            'notification_data_ids' => $activity
                                        );
                                        $q = $d->update("user_notification", $a212, "user_notification_id IN ($user_notification_id_str)");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($q == true)
        {
            header("Location: ../hrDoc");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../hrDoc");
        }
    }
}
?>