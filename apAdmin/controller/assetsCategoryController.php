<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_POST) && !empty($_POST) )
{

            if(isset($submit_assets))
            {
              $assets_categoryTemp=mysqli_real_escape_string($con, $assets_category);
              
              $get_cat = $d->selectRow("assets_category_id","assets_category_master","assets_category = '$assets_categoryTemp'");
              if(mysqli_num_rows($get_cat) > 0)
              {
                $_SESSION['msg1']="Assets Category Already Exists";
                header("Location: ../assetsCategory");
                exit;
              }

                $m->set_data('assets_category',$assets_category);
                $m->set_data('society_id',$society_id);

                $a = array(
                  'society_id'=> $m->get_data('society_id'),
                  'assets_category'=>$m->get_data('assets_category')
                );



                $q=$d->insert("assets_category_master",$a);
                if($q==TRUE)
                {
                      $_SESSION['msg']="Assets Category Added successfully";
                      $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Category $assets_category Added");

                      header("Location: ../assetsCategory");
                }
                else{
                      $_SESSION['msg1']="Assets Category Added Unsuccessfull";
                      header("Location: ../assetsCategory");
                }
            }


          if(isset($delete_assets)) 
          {

              $q= $d->delete("assets_category_master","assets_category_id='$assets_category_id'");
              if($q==TRUE)
              {

                    $_SESSION['msg']="Delete Successfully";
                    $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Category $assets_category_id Delete");
                    header("Location:../assetsCategory");
              }
              else{

                    $_SESSION['msg1']="Somthing Wrong";
                    header("Location:../assetsCategory");
                }

          }

          if (isset($update_assets))  {

             $assets_categoryTemp=mysqli_real_escape_string($con, $assets_category);
              
              
              $get_cat = $d->selectRow("assets_category_id","assets_category_master","assets_category = '$assets_categoryTemp' AND assets_category_id!='$assets_category_id'");
              if(mysqli_num_rows($get_cat) > 0)
              {
                $_SESSION['msg1']="Assets Category Already Exists";
                header("Location: ../assetsCategory");
                exit;
              }

              $m->set_data('assets_category',$assets_category);
              $m->set_data('society_id',$society_id);

              $a = array(
              'assets_category'=>$m->get_data('assets_category'),
               'society_id'=> $m->get_data('society_id')
              );


              $q=$d->update("assets_category_master",$a,"assets_category_id='$assets_category_id'");

              if($q==TRUE)
              {

                    $_SESSION['msg']="Assets Category Update successfully";
                    $d->insert_log("","$society_id","$_SESSION[bms_admin_id]","$created_by","Assets Category $assets_category Update");
                    header("Location:../assetsCategory");
              }
              else
              {
                    $_SESSION['msg1']="Assets Category Update Unsuccessfull";
                    header("Location:../assetsCategory");
              }
          }
          
  }

else
{
header('location:../login');
}
?>
