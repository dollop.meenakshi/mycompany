<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();

$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract($_POST);

if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  $stringIds = '('.implode(',', $ids).')';
  if($_POST['deleteValue']=="deleteAssetsMaintenance") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("assets_maintenance_master","assets_maintenance_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Assets Maintenance Deleted $stringIds");
        $_SESSION['msg']="Assets Maintenance Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteEmployeeAppAccess") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("app_access_master","app_access_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Access Deleted $stringIds");
        $_SESSION['msg']="Employee Access Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  if($_POST['deleteValue']=="deleteMultiAssignDptBlkUser") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("employee_multiple_department_branch","employee_multiple_department_branch_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Assign Branch/Department Deleted $stringIds");
        $_SESSION['msg']="Employee Assign Branch/Department Deleted";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteTaskPriority") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("task_priority_master","task_priority_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Priority Deleted $stringIds");
        $_SESSION['msg']="Task Priority Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteAdminTask") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("task_master","task_id='$ids[$i]'");
      $q=$d->delete("task_step_master","task_id='$ids[$i]'");
      $q=$d->delete("task_user_history","task_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Task Deleted $stringIds");
        $_SESSION['msg']="Task Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteChatGroup") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("chat_group_master","group_id='$ids[$i]'");
      $q=$d->delete("chat_group_member_master","group_id='$ids[$i]'");
      $q=$d->delete("chat_group_unread_message","group_id='$ids[$i]'");
      $q=$d->delete("chat_master_group","group_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Deleted $stringIds");
        $_SESSION['msg']="Chat Group Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteChatGroupMember") {

    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q = $d->update("chat_group_member_master", array("chat_group_member_delete" =>'1'), "chat_group_member_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chat Group Member Deleted $stringIds");
        $_SESSION['msg']="Chat Group Member Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  if($_POST['deleteValue']=="deleteProductVerient") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q = $d->update("product_variant_master", array("variant_delete_status" =>'1'), "product_variant_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Variant Deleted $stringIds");
        $_SESSION['msg']="Product Variant Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  if($_POST['deleteValue']=="deleteEarnDeduction") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q = $d->update("salary_earning_deduction_type_master", array("earn_deduct_is_delete" =>'1'), "salary_earning_deduction_id  ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Earn/Deduction Type Deleted $stringIds");
        $_SESSION['msg']="Salary Earn/Deduction Type Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  if($_POST['deleteValue']=="deleteProductPrice") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q = $d->update("product_price_master", array("product_price_delete_status" =>'1'), "product_price_id  ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Price Deleted Successfully $stringIds");
        $_SESSION['msg']="Product Price Deleted Successfully.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteSalaryCommonValue") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("salary_common_value_master","salary_common_value_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Salary Common Value Deleted $stringIds");
        $_SESSION['msg']="Salary Common Value Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteEscalation") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $a['escalation_is_delete']=1;
      $q=$d->update("escalation_master",$a,"escalation_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Escalation Deleted $stringIds");
        $_SESSION['msg']="Escalation Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteCompanyServiceMoreDetail") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_service_more_details_master","company_service_more_details_id='$ids[$i]'");
      $q=$d->delete("company_service_more_details_list_master","company_service_more_details_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Service More Detail Deleted $stringIds");
        $_SESSION['msg']="Company Service More Detail Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteCompanyServiceDetail") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_service_details_master","company_service_details_id='$ids[$i]'");
      $q=$d->delete("company_service_detail_list_master","company_service_details_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Service Detail Deleted $stringIds");
        $_SESSION['msg']="Company Service Detail Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteCompanyService") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_service_master","company_service_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Service Deleted $stringIds");
        $_SESSION['msg']="Company Service Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  ///////////////////////////////////////////////////////////////////////////////


  if($_POST['deleteValue']=="deleteCompanyOpeningApply") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_current_opening_apply_master","company_current_opening_apply_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Current Opening Apply Deleted $stringIds");
        $_SESSION['msg']="Company Current Opening Apply Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteCompanyCurrentOpening") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_current_opening_master","company_current_opening_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Current Opening Deleted $stringIds");
        $_SESSION['msg']="Company Current Opening Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteCompanyHome") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_home_master","company_home_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company Home Deleted $stringIds");
        $_SESSION['msg']="Company Home Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteGetInTouch") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_get_in_touch_master","company_get_in_touch_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Get In Touch Deleted $stringIds");
        $_SESSION['msg']="Get In Touch Us Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  if($_POST['deleteValue']=="deleteCompanyAboutUs") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("company_about_us_master","company_about_us_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Company About Us Deleted $stringIds");
        $_SESSION['msg']="Company About Us Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  if($_POST['deleteValue']=="deleteEmployeeAccess") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("app_access_master","app_access_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Access Deleted $stringIds");
        $_SESSION['msg']="Employee Access Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


   if($_POST['deleteValue']=="deleteBlockCompalaine") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
        
      $bq=$d->select("users_master,block_master","block_master.block_id=users_master.block_id AND  users_master.unit_id='$ids[$i]' ");
      $unitData= mysqli_fetch_array($bq);
      $unitName = $unitData['user_full_name'].'-'.$unitData['user_designation'];


      $q=$d->delete("complains_block_master","unit_id='$ids[$i]'");
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Complaint Employee ($unitName) Deleted $stringIds");
    }
      if($q>0) {
        echo 1;
        $_SESSION['msg']="Deleted Successfully"; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }


  if($_POST['deleteValue']=="deleteDiscussion") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("discussion_forum_master","discussion_forum_id='$ids[$i]'");
      $q=$d->delete("discussion_forum_comment","discussion_forum_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Discussion Deleted $stringIds");
        $_SESSION['msg']="Discussion Deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  } 


   if($_POST['deleteValue']=="deleteMasterQrNew") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("qr_code_master","qr_code_master_id='$ids[$i]'");
      $q=$d->delete("gatekeeper_eye_report","qr_code_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","QR Code Deleted $stringIds");
        $_SESSION['msg']="QR Code Deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }


  



   if($_POST['deleteValue']=="deleteKBGQuetion") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("kbg_question_master","kbg_question_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Question Deleted $stringIds");
        $_SESSION['msg']="Question deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }

   if($_POST['deleteValue']=="deleteHousieQuetion") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("housie_questions_master","que_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Question Deleted $stringIds");
        $_SESSION['msg']="Question deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }

  if($_POST['deleteValue']=="deleteHousieGame") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
     

      $q=$d->delete("housie_room_master","room_id='$ids[$i]'");
      $q=$d->delete("housie_questions_master","room_id='$ids[$i]'");
      $q=$d->delete("housie_join_room_master","room_id='$ids[$i]'");
      $q=$d->delete("housie_winner_master","room_id='$ids[$i]'");
      $q=$d->delete("housie_claim_rules_master","room_id='$ids[$i]'");
      $qc=$d->delete("housie_participate_player","room_id='$room_id'");
      
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Housie Question Deleted $stringIds");
        $_SESSION['msg']="Game deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }


  if($_POST['deleteValue']=="deleteKBGGame") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("kbg_game_master","kbg_game_id='$ids[$i]'");
      $q=$d->delete("kbg_question_master","kbg_game_id='$ids[$i]'");
      $q=$d->delete("kbg_result_master","kbg_game_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","KBG Question Deleted $stringIds");
        $_SESSION['msg']="Game deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }

  if($_POST['deleteValue']=="deleteRequest") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("request_master","request_id='$ids[$i]'");
      $q=$d->delete("request_track_master","request_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Request Deleted $stringIds");
        $_SESSION['msg']="request deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }
  //IS_1546 23march2020
  if($_POST['deleteValue']=="deleteSlab") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("gst_master","slab_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Tax Slab Deleted $stringIds");
        $_SESSION['msg']="Tax Slab Deleted."; 
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong"; 
      }
  }
  //IS_1546 23march2020

 

  if($_POST['deleteValue']=="deleteSessionlog") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("session_log","sessionId='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Session Log Deleted $stringIds");
        $_SESSION['msg']="Session Log Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


 if($_POST['deleteValue']=="deleteOtherlog") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("log_master","log_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Other Log Deleted $stringIds");
        $_SESSION['msg']="Other Log Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  


  if($_POST['deleteValue']=="deleteNotification") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("notifcation_master","notification_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Notification Deleted $stringIds");
        $_SESSION['msg']="Notification Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

   if($_POST['deleteValue']=="deleteEmpType") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("emp_type_master","emp_type_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Type  Deleted $stringIds");
        $_SESSION['msg']="Employee Type  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

 

   if($_POST['deleteValue']=="deleteBalance") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("balancesheet_master","balancesheet_id='$ids[$i]' AND society_id='$society_id' ");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Balancesheet Deleted $stringIds");
        $_SESSION['msg']="Balancesheet  Deleted";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteFeedback") {
    $idCount = count($ids);

    //18march2020
    $qry = "";
         $qry .= "AND society_id='$society_id'";
    
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("feedback_master","feedback_id='$ids[$i]' $qry ");
    }
    //18march2020
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Feedback Deleted $stringIds");
        $_SESSION['msg']="Feedback  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  } 

   if($_POST['deleteValue']=="deleteEvent") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q1=$d->select("event_master","event_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/event_image/" . $data['event_image'];
       unlink ($profileUrl);      
      $q=$d->delete("event_master","event_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Event Deleted $stringIds");
        $_SESSION['msg']="Event  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }  


   if($_POST['deleteValue']=="deleteEyeReport") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q1=$d->select("gatekeeper_eye_report","eye_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/gatekeeper_eye/" . $data['eye_photo'];
       unlink ($profileUrl);      
      $q=$d->delete("gatekeeper_eye_report","eye_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Eye Report Deleted $stringIds");
        $_SESSION['msg']="Data Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }  

  if($_POST['deleteValue']=="deletePoll") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("voting_master","voting_id='$ids[$i]' AND society_id='$society_id'");
      $q=$d->delete("voting_option_master","voting_id='$ids[$i]'");
      $q=$d->delete("voting_result_master","voting_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Poll Deleted $stringIds");
        $_SESSION['msg']="Poll  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

   if($_POST['deleteValue']=="deleteDocumentType") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("document_type_master","  document_type_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Type Deleted $stringIds");
        $_SESSION['msg']="Document Type   Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }  

  if($_POST['deleteValue']=="deleteDocument") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q1=$d->select("document_master","document_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/documents/" . $data['document_file'];
       unlink ($profileUrl);
      $q=$d->delete("document_master","document_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Deleted $stringIds");
        $_SESSION['msg']="Document  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }  


 if($_POST['deleteValue']=="deleteElection") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q11=$d->select("election_master","society_id='$society_id'","ORDER BY election_id DESC");
       $row=mysqli_fetch_array($q11);
       $electionName = $row['election_name'];
      $q=$d->delete("election_master","election_id='$ids[$i]' AND society_id='$society_id'");
      $q=$d->delete("election_users","election_id='$ids[$i]' AND society_id='$society_id'");
      $q=$d->delete("election_result_master","election_id='$ids[$i]' AND society_id='$society_id'");
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","$electionName Election Deleted $stringIds");
    }
      if($q>0) {
        echo 1;
        $_SESSION['msg']="Election  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  } 

  if($_POST['deleteValue']=="deletePackage") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q=$d->delete("package_master","package_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Package Deleted $stringIds");
        $_SESSION['msg']="Package  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

 if($_POST['deleteValue']=="deleteEmergency") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("emergemcy_number_list","emergency_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Emergency Number Deleted $stringIds");
        $_SESSION['msg']="Emergency Number  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  if($_POST['deleteValue']=="deleteAdminNotification") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("admin_notification","notification_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Admin Notification Deleted $stringIds");
        $_SESSION['msg']="Notification  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

   
  if($_POST['deleteValue']=="deleteSos") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("sos_events_master","sos_event_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","SOS Deleted $stringIds");
        $_SESSION['msg']="SOS Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteCompalaine") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $a = array('flag_delete' =>2 , );
      $q=$d->update("complains_master",$a,"complain_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","complaint Deleted $stringIds");
        $_SESSION['msg']="complaint Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  if($_POST['deleteValue']=="deleteBalanceFile") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q1=$d->select("balancesheet_pdf_master","balancesheet_file_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $name = $data['balancesheet_name'];
       $profileUrl= "../../img/balancesheet/" . $data['file_name'];
       unlink ($profileUrl);

      $q=$d->delete("balancesheet_pdf_master","balancesheet_file_id='$ids[$i]' AND society_id='$society_id'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Balancesheet $name File Deleted $stringIds");
        $_SESSION['msg']="File Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteServiceProvider") 
  {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) 
    { 
      // $q=$d->delete("local_service_provider_users","service_provider_users_id='$ids[$i]' ");
       $q = $d->update("local_service_provider_users", array("service_provider_delete_status" =>'1','service_provider_delete_by'=>$_COOKIE['bms_admin_id']), "service_provider_users_id ='$ids[$i]'");

     /* $q=$d->delete("local_service_providers_ratings","local_service_provider_id='$ids[$i]' ");
      $q=$d->delete("local_service_provider_complains","local_service_provider_id='$ids[$i]' ");
      $q=$d->delete("local_service_provider_users_category","service_provider_users_id='$ids[$i]' "); */
    }
    if($q>0) 
    {
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Deleted $stringIds");
      $_SESSION['msg']="Vendor Deleted Successfully";
      header("location:../vendors");
    } 
    else 
    {
      $_SESSION['msg1']="Something Wrong";
      header("location:../vendors");
    }
  }

  if($_POST['deleteValue']=="deletePenalty") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
       $q1=$d->select("penalty_master","penalty_id='$ids[$i]'");
        $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/billReceipt/" . $data['penalty_photo'];
       unlink ($profileUrl);
      $q=$d->delete("penalty_master","penalty_id='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Penalty Deleted $stringIds");
        $_SESSION['msg']="Penalty Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteBillCategory") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q1=$d->select("bill_category_master","bill_category_id='$ids[$i]'");
       $data=mysqli_fetch_array($q1);
       $profileUrl= "../../img/bill_category/".$data['bill_category_image'];
       unlink ($profileUrl);


      $q=$d->delete("bill_category_master","bill_category_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Bill Category Deleted $stringIds");
        $_SESSION['msg']="Bill Category Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteComplaintCategory") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("complaint_category","complaint_category_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Complaint Category Deleted $stringIds");
        $_SESSION['msg']="Complaint Category Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteComplaintEmail") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("complaint_email_receipt","email_receipt_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Complaint Email Receipt Deleted $stringIds");
        $_SESSION['msg']="Email Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  if($_POST['deleteValue']=="deleteExpenseCategory") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("expense_category_master","expense_category_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
       $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Expense Category Deleted $stringIds");
        $_SESSION['msg']="Category Deleted Successfully.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  //26MARCH2020 IS_1652
  if($_POST['deleteValue']=="deleteSurvey") {

    $survey_id_arr = implode(",", $ids);
      $survey_master_qry=$d->select("survey_result_master","survey_id IN ('$survey_id_arr') AND society_id='$society_id'");

     $result_found = mysqli_num_rows($survey_master_qry);
     if($result_found  > 0 ){
       
        $_SESSION['msg1']="Can't Delete This Survey, THere are already Result for it";
         echo 0;exit;
     }

    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("survey_master","survey_id='$ids[$i]' AND society_id='$society_id'");
      $q=$d->delete("survey_option_master","survey_id='$ids[$i]'");
      $q=$d->delete("survey_question_master","survey_id='$ids[$i]'");
      $q=$d->delete("survey_result_master","survey_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Survey Deleted $stringIds");
        $_SESSION['msg']="Survey  Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }

  if($_POST['deleteValue']=="deleteSurveyQue") {
      $survey_question_id_array = implode(",", $ids);
      $survey_result_master_qry=$d->select("survey_result_master","survey_question_id IN ('$survey_question_id_array') AND society_id='$society_id'");

     $result_found = mysqli_num_rows($survey_result_master_qry);
     if($result_found  > 0 ){
       
        $_SESSION['msg1']="Can't Delete This Question, THere are already Result for it";
         echo 0;exit;
     }


    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) { 
      $q=$d->delete("survey_question_master","survey_question_id='$ids[$i]'");
     }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Survey Question Deleted $stringIds");
        $_SESSION['msg']="Survey Question Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }
  //26MARCH2020 IS_1652


  //////////////////////delete holiday

  if($_POST['deleteValue']=="deleteHoliday") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {     
      $q2 = $d->selectRow('*',"holiday_master", "holiday_id='$ids[$i]'");
      $data = mysqli_fetch_assoc($q2);   
      if($data)
      {
        
        $originalDate = $data['holiday_start_date'];
        $newDate = date("m-Y", strtotime($originalDate));
        $q3 = $d->selectRow('*',"working_days_master", "month_year='$newDate'");
        $data3 = mysqli_fetch_assoc($q3); 
       
        if($data3)
        {
          $holidayCount = ($data3['total_month_holidays']-1);
          $working_days = ($data3['working_days']+1);
          $q = $d->update("working_days_master", array("total_month_holidays" =>$holidayCount,'working_days'=>$working_days), "working_day_id ='$data3[working_day_id]'");

        }
         $q=$d->delete("holiday_master","holiday_id='$ids[$i]'");

      }
    }
      if($q2>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Holiday Deleted $stringIds");
        $_SESSION['msg']="Holiday Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  //////////////////////delete bank user

  if($_POST['deleteValue']=="deleteUserBank") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("user_bank_master","bank_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Bank Deleted $stringIds");
        $_SESSION['msg']=" User Bank Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  /////////////delete chapter////////////

  if($_POST['deleteValue']=="deleteCourseChapter") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("course_chapter_master", array("course_chapter_is_deleted" =>'1'), "course_chapter_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Course Chapter Deleted $stringIds");
        $_SESSION['msg']="Course Chapter Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  if($_POST['deleteValue']=="deleteCourseLessonType") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("lesson_type_master", array("is_deleted" =>'1'), "lesson_type_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Lesson Type Deleted $stringIds");
        $_SESSION['msg']="Lesson Type Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  /////////////delete Idea Category////////////

  if($_POST['deleteValue']=="deleteIdeaCategory") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("idea_category_master", array("idea_category_delete" =>'1'), "idea_category_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Idea Box Deleted $stringIds");
        $_SESSION['msg']="Idea Category Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  /////////////delete Course////////////
  if($_POST['deleteValue']=="deleteZones") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("zone_master","zone_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Zone Deleted $stringIds");
        $_SESSION['msg']="Zone Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteEmployeeLevels") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("employee_level_master","level_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Level Deleted $stringIds");
        $_SESSION['msg']="Employee Level Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  if($_POST['deleteValue']=="deleteCourse") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("course_master", array("course_is_deleted" =>'1'), "course_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Course Deleted $stringIds");
        $_SESSION['msg']="Course Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  /////////////delete Course////////////

  if($_POST['deleteValue']=="deletesalary") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("salary", array("is_delete" =>'1'), "salary_id ='$ids[$i]'");
      $q=$d->delete("salary_master","salary_id='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee salary Deleted $stringIds");
        $_SESSION['msg']="salary Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  /////////////delete Idea////////////

  if($_POST['deleteValue']=="deleteIdea") {
           
      $q=$d->delete("idea_master","idea_id='$id'");
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Idea Deleted $stringIds");
        $_SESSION['msg']="Idea Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  /////////////delete Hr Doc category////////////

  if($_POST['deleteValue']=="deleteHrDoc") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("hr_document_category_master", array("hr_document_category_deleted" =>'1'), "hr_document_category_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Category Deleted $stringIds");
        $_SESSION['msg']="Document Category Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }



  //////////////////////delete deleteHrDocument
  
  if($_POST['deleteValue']=="deleteHrDocument") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q = $d->update("hr_document_master", array("hr_doc_deleted" =>'1'), "hr_document_id ='$ids[$i]'");
      $getHrDoc = $d->selectRow('*', "hr_document_master", "hr_document_id='$ids[$i]'");
      $getHrDocdata = mysqli_fetch_assoc($getHrDoc);
      $getHrCat = $d->selectRow('*', "hr_document_category_master", "hr_document_category_id='$getHrDocdata[hr_document_category_id]'");
      $getHrCatdata = mysqli_fetch_assoc($getHrCat);
      $activityData = array(
          'hr_document_category_id' => $getHrCatdata['hr_document_category_id'],
          'hr_document_category_name' => $getHrCatdata['hr_document_category_name'],
      );
      
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Document Deleted $stringIds");
        $_SESSION['msg']="Document  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteAdvanceSalary") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {
     
        $q = $d->update("advance_salary", array("advance_salary_delete_status" =>'1'), "advance_salary_id ='$ids[$i]'");
    }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Advance Salary Deleted $stringIds");
      $_SESSION['msg']="Advance Salary Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
  }
  if($_POST['deleteValue']=="deleteEmployeeLoan") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {
     
        $q = $d->delete("loan_emi_master",  "loan_id ='$ids[$i]'");
        $q2 = $d->delete("employee_loan_master",  "loan_id ='$ids[$i]'");
    }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Loan Deleted $stringIds");
      $_SESSION['msg']="Employee Loan Deleted Sucessfully";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
  }


  //////////////////////delete leave type////////////
  
  if($_POST['deleteValue']=="deleteLeaveType") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {
      $totalAssign = $d->count_data_direct("user_leave_id","leave_assign_master","leave_type_id='$ids[$i]'");
      $totalDefault = $d->count_data_direct("leave_default_count_id","leave_default_count_master","leave_type_id='$ids[$i]'");
      $totalLeave = $d->count_data_direct("leave_id","leave_master","leave_type_id='$ids[$i]'");
      if($totalAssign==0 &&  $totalDefault==0 &&  $totalLeave==0) {        
        $q = $d->update("leave_type_master", array("leave_type_delete" =>'1'), "leave_type_id ='$ids[$i]'");
      }
    }

      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Type Deleted $stringIds");
        $_SESSION['msg']="Leave Type Deleted.";
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
      }
  }


  //////////////////////delete leave assign////////////
  
  if($_POST['deleteValue']=="deleteLeaveAssign") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("leave_assign_master","user_leave_id='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Assign Deleted $stringIds");
        $_SESSION['msg']="Leave Assign Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  if($_POST['deleteValue']=="deletePromotions") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("employee_history","history_id='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Promotion History Deleted $stringIds");
        $_SESSION['msg']="Employee Promotion History Deleted";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  if($_POST['deleteValue']=="deleteLeaves") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("leave_master","leave_id='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Deleted $stringIds");
        $_SESSION['msg']="Leave  Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  //////////////delete shift timing////////////
  
  if($_POST['deleteValue']=="deleteShiftTiming") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {    
      $q = $d->update("shift_timing_master", array("is_deleted" =>'1','shift_delete_by'=>$_COOKIE['bms_admin_id']), "shift_time_id ='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Shift Deleted $stringIds");
        $_SESSION['msg']="Shift Deleted Successfully.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

   //////////////delete shift timing////////////
  
   if($_POST['deleteValue']=="deleteLeaveDefaultCount") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {        
      $q=$d->delete("leave_default_count_master","leave_default_count_id='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Leave Default Count Deleted $stringIds");
        $_SESSION['msg']="Leave Default Count Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }


  /////////////delete Course Lesson////////////

  if($_POST['deleteValue']=="deleteCourseLesson") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {  
      $q = $d->update("course_lessons_master", array("lesson_is_deleted" =>'1'), "lessons_id ='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Course Lesson Deleted $stringIds");
        $_SESSION['msg']="Course Lesson Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

  /////////////delete Attendance Type////////////

  if($_POST['deleteValue']=="deleteAttendanceType") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount ; $i++) {  
      $q = $d->update("attendance_type_master", array("attendance_type_delete" =>'1'), "attendance_type_id ='$ids[$i]'");

    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attendance Type Deleted $stringIds");
        $_SESSION['msg']="Attendance Type Deleted.";
        $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0'");
          $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1'");
          $title = "sync_data";
          $description = "Face Data Updated on ".date('d-M-y h:i A');
          $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
          $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }

    /////////////delete site////////////
  if($_POST['deleteValue']=="deleteSite") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount; $i++) {        
      $q = $d->update("site_master", array("site_delete" =>'1'), "site_id ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Site Deleted $stringIds");
        $_SESSION['msg']="Site Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
  
  /////////////delete Unit Measure////////////
  if($_POST['deleteValue']=="deleteUnitMeasure") {
    $idCount = count($ids);
    for ($i=0; $i <$idCount; $i++) {        
      $q = $d->update("unit_measurement_master", array("unit_measurement_delete" =>'1'), "unit_measurement_id  ='$ids[$i]'");
    }
      if($q>0) {
        echo 1;
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Unit Measurement Deleted $stringIds");
        $_SESSION['msg']="Unit Measurement Deleted.";
        // header("location:../categories");
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        // header("location:../categories");
      }
  }
}

/////////////delete Category////////////
if($_POST['deleteValue']=="deleteproductCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("product_category_master", array("product_category_delete" =>'1'), "product_category_id  ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Category Deleted $stringIds");
      $_SESSION['msg']="Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}
///////////////////////delete product vendor category
if($_POST['deleteValue']=="deleteproductVendorCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->delete("product_category_vendor_master", "product_category_vendor_id='$ids[$i]'");
  }
  if($q>0) {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Product Category Deleted $stringIds");
    $_SESSION['msg']=" Vendor Product Category Deleted.";
    // header("location:../categories");
  } else {
    echo 0;
    $_SESSION['msg1']="Something Wrong";
    // header("location:../categories");
  }
}
/////////////delete Sub Category////////////
if($_POST['deleteValue']=="deleteSubCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $totalVariant = $d->count_data_direct("product_sub_category_vendor_master", "product_sub_category_vendor_master", "product_sub_category_id='$ids[$i]'");
      if(mysqli_num_rows($getDatasql)<=0){
        $q = $d->update("product_sub_category_master", array("product_sub_category_delete" =>'1'), "product_sub_category_id  ='$ids[$i]'");
      }
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Sub Category Deleted $stringIds");
      $_SESSION['msg']="sub Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}
if($_POST['deleteValue']=="deleteproductVendorSubCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->delete("product_sub_category_vendor_master", "product_sub_category_vendor_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Sub Category Deleted $stringIds");
      $_SESSION['msg']="Vendor sub Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

if($_POST['deleteValue']=="deleteProduct") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("product_master", array("product_delete" =>'1'), "product_id  ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Deleted $stringIds");
      $_SESSION['msg']="Product Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}
if($_POST['deleteValue']=="course_chapter_delete") {
    $q = $d->delete("course_chapter_master", "course_chapter_id  ='$id'");
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Chapter Deleted $stringIds");
      $_SESSION['msg']="Chapter Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}
if($_POST['deleteValue']=="lesson_delete") {
  
    $q = $d->delete("course_lessons_master",  "lessons_id  ='$id'");
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Lesson Deleted $stringIds");
      $_SESSION['msg']="Lesson Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}

if($_POST['deleteValue']=="deleteBankDetail") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("service_provider_bank_detail", array("sp_bank_deleted" =>'1'), "service_provider_bank_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Bank Details Deleted $stringIds");
      $_SESSION['msg']="Bank Details Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

if($_POST['deleteValue']=="deletePaperSize") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("xerox_paper_size", array("is_delete" =>'1'), "xerox_paper_size_id  ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Paper Size Deleted $stringIds");
      $_SESSION['msg']="Paper Size Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}
if($_POST['deleteValue']=="deleteXeroxType") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("xerox_type_category", array("is_delete" =>'1'), "xerox_type_id  ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Xerox Category Deleted $stringIds");
      $_SESSION['msg']="Xerox Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

if($_POST['deleteValue']=="deletevendorProductCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount; $i++) {        
    $q = $d->update("vendor_product_category_master", array("is_delete" =>'1'), "vendor_product_category_id  ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Vendor Product Category Deleted $stringIds");
      $_SESSION['msg']="Vendor Product Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

/* Dollop Infotech date 16-Nov-2021 -   */
if ($_POST['deleteValue'] == "deleteStationaryProductCategory") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_category_master", array("is_delete" => '1'), "vendor_product_category_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Stationery Product Category Deleted $stringIds");
        $_SESSION['msg'] = "Stationery Product Category Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}
if ($_POST['deleteValue'] == "deleteCanteenProductCategory") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_category_master", array("is_delete" => '1'), "vendor_product_category_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Canteen Product Category Deleted $stringIds");
        $_SESSION['msg'] = "Canteen Product Category Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}
/* Dollop Infotech date 16-Nov-2021 -   */

/* Dollop Infotech date 15-Nov-2021 -   */
if ($_POST['deleteValue'] == "deleteCanteenVendor") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_master", array("vendor_master_delete" => '1'), "vendor_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Canteen Vendor Deleted $stringIds");
        $_SESSION['msg'] = "Canteen Vendor Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}
if ($_POST['deleteValue'] == "deleteStationaryVendor") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_master", array("vendor_master_delete" => '1'), "vendor_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Stationery Vendor Deleted $stringIds");
        $_SESSION['msg'] = "Stationery Vendor Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deleteCanteenProduct") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_master", array("vendor_product_delete" => '1'), "vendor_product_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Canteen Product Deleted $stringIds");
        $_SESSION['msg'] = "Canteen Product Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}
if ($_POST['deleteValue'] == "deleteStationaryProduct") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_master", array("vendor_product_delete" => '1'), "vendor_product_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Stationery Product Deleted $stringIds");
        $_SESSION['msg'] = "Stationery Product Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deleteStationaryProductVariant") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_variant_master", array("vendor_product_variant_delete" => '1'), "vendor_product_variant_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Stationery Product Variant Deleted $stringIds");
        $_SESSION['msg'] = "Stationery Product Variant Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deleteCanteenProductVariant") {
    $idCount = count($ids);
    for ($i = 0; $i < $idCount; $i++) {
        $q = $d->update("vendor_product_variant_master", array("vendor_product_variant_delete" => '1'), "vendor_product_variant_id  ='$ids[$i]'");
    }
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Canteen Product Variant Deleted $stringIds");
        $_SESSION['msg'] = "Canteen Product Variant Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deleteProductVariantImage") {
    
    $q = $d->delete("vendor_product_image_master", "vendor_product_image_id='$id'");
    unlink("../../img/product_variant/".$image);
    
    if ($q > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Product variant deleted $stringIds");
        $_SESSION['msg'] = " Product Variant Image Deleted.";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}


if ($_POST['deleteValue'] == "removeBLockGeoLocation") {
    
        $q = $d->update("block_master", array("block_geofence_range" => '','block_geofence_longitude'=>"",'block_geofence_latitude'=>""), "block_id ='$block_id'");
       
        if ($q > 0) {
            echo 1;
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Idea Box Deleted $stringIds");
            $_SESSION['msg'] = "Removed Successfully.";
        } else {
            echo 0;
            $_SESSION['msg1'] = "Something Wrong";
        }
}
if ($_POST['deleteValue'] == "removUserGeoLocation") {
   
      $q1=$d->delete("user_geofence","geo_fance_id='$geo_fance_id'");
   
    if ($q1 > 0) {
        echo 1;
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Geo Facing removed $stringIds");
        $_SESSION['msg'] = "User geo Facing remove .";
        // header("location:../categories");
    } else {
        echo 0;
        $_SESSION['msg1'] = "Something Wrong";
        // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deletePurchase") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $qpp=$d->select("purchase_product_master","purchase_id='$ids[$i]'");
    while ($data=mysqli_fetch_array($qpp)) {     
      $q1=$d->delete("purchase_product_master","puchase_product_id='$data[puchase_product_id]'");
    }
    $q=$d->delete("purchase_master","purchase_id='$ids[$i]'");

  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Purchase Deleted $stringIds");
      $_SESSION['msg']="Purchase Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

if ($_POST['deleteValue'] == "deleteUserFaceData") {
  $a = array(
      'user_face_id'=>"",
      'user_face_data'=>"",
      'face_data_image'=>"",
      'face_data_image_two'=>"",
      'face_added_date'=>"",
  );
  $usql=$d->selectRow('users_master.*',"users_master","user_id=$id");
  $user = mysqli_fetch_assoc($usql);
  $image = "../../img/attendance_face_image/".$user['face_data_image'];
  $image2 = "../../img/attendance_face_image/".$user['face_data_image_two'];
 /*  echo "<pre>";
  print_r($image);
  print_r($image2);die; */
  if(file_exists($image)){unlink($image);}
  if(file_exists($image2)){unlink($image2);}
  $q = $d->update("users_master",$a, "user_id ='$id'");
 
  if ($q > 0) {
      echo 1;

      $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' ");
      $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' ");
      $title = "sync_data";
      $description = "Face Data Updated on ".date('d-M-y h:i A');
      $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
      $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");
      $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "User Face Data Deleted ($id)");
      $_SESSION['msg'] = "User Face Data Deleted.";
      // header("location:../categories");
  } else {
      echo 0;
      $_SESSION['msg1'] = "Something Wrong";
      // header("location:../categories");
  }
}


if($_POST['deleteValue']=="deleteHrDocSubCategory") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) {        
    $q = $d->update("hr_document_sub_category_master", array("hr_document_sub_category_deleted" =>'1'), "hr_document_sub_category_id ='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","HR Document Sub Category Deleted $stringIds");
      $_SESSION['msg']="HR Document Sub Category Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

/* PMS Module 07/07/2022 */
if($_POST['deleteValue']=="deleteDimensionals") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("dimensional_master","dimensional_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Dimensional Deleted $stringIds");
      $_SESSION['msg']="Dimensional Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}

if($_POST['deleteValue']=="deleteAttributes") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("attribute_master","attribute_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attribute Deleted $stringIds");
      $_SESSION['msg']="Attribute Deleted.";
      // header("location:../categories");
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
      // header("location:../categories");
    }
}


 /* DAR Module 11/07/2022 */
 if($_POST['deleteValue']=="deleteTemplate") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("template_master","template_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Template Deleted $stringIds");
      $_SESSION['msg']="Template Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}

if($_POST['deleteValue']=="deleteQuestionTemplate") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("template_question_master","template_question_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Template Question Deleted $stringIds");
      $_SESSION['msg']="Template Question Deleted.";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}

/* 13/07/2022 */
if($_POST['deleteValue']=="deleteAssignedTemplate") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("template_assign_master","template_assign_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Assigned Template Deleted $stringIds");
      $_SESSION['msg']="Assigned Template Deleted";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}

/*25 July 2022*/
if($_POST['deleteValue']=="deleteAssignDimensionals") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("performance_dimensional_assign","performance_dimensional_assign_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Assigned Dimensional Deleted $stringIds");
    $_SESSION['msg']="Assigned Dimensional Deleted";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}


if($_POST['deleteValue']=="deleteIdProof") {
  $idCount = count($ids);
  for ($i=0; $i <$idCount ; $i++) { 
    $q=$d->delete("id_proof_master","id_proof_id='$ids[$i]'");
  }
    if($q>0) {
      echo 1;
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","ID Proof Deleted $stringIds");
    $_SESSION['msg']="ID Proof Deleted";
    } else {
      echo 0;
      $_SESSION['msg1']="Something Wrong";
    }
}

if($_POST['deleteValue'] == "deleteArea")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q = $d->delete("area_master_new","area_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Area Deleted $stringIds");
    $_SESSION['msg'] = "Area Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteRoute")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q = $d->delete("route_master","route_id = '$ids[$i]'");
    $d->delete("route_employee_assign_master","route_id = '$ids[$i]'");
    $d->delete("route_retailer_master","route_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Routes Deleted $stringIds");
    $_SESSION['msg'] = "Route Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteRetailer")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("retailer_photo","retailer_master","retailer_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['retailer_photo'] != "")
    {
      unlink("../../img/users/recident_profile/".$data['retailer_photo']);
    }
    $q = $d->delete("retailer_master","retailer_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Retailers Deleted $stringIds");
    $_SESSION['msg'] = "Retailers Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteDistributor")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("distributor_photo","distributor_master","distributor_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['distributor_photo'] != "")
    {
      unlink("../../img/users/recident_profile/".$data['distributor_photo']);
    }
    $q = $d->delete("distributor_master","distributor_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Distributors Deleted $stringIds");
    $_SESSION['msg'] = "Distributors Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteEmployeeRoute")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q = $d->delete("route_employee_assign_master","route_employee_assign_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Employee Route Deleted $stringIds");
    $_SESSION['msg'] = "Employee Route Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteProducts")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("product_image","retailer_product_master","product_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['product_image'] != "")
    {
      unlink("../../img/vendor_product/".$data['product_image']);
    }
    $q12 = $d->selectRow("variant_photo","retailer_product_variant_master","product_id = '$ids[$i]'");
    $data1 = $q12->fetch_assoc();
    if($data1['variant_photo'] != "")
    {
      unlink("../../img/vendor_product/".$data1['variant_photo']);
    }
    $q = $d->delete("retailer_product_master","product_id = '$ids[$i]'");
    $q = $d->delete("retailer_product_variant_master","product_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Products Deleted $stringIds");
    $_SESSION['msg'] = "Products Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteProductVariant")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("variant_photo","retailer_product_variant_master","product_variant_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['variant_photo'] != "")
    {
      unlink("../../img/vendor_product/".$data['variant_photo']);
    }
    $q = $d->delete("retailer_product_variant_master","product_variant_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Product Variants Deleted $stringIds");
    $_SESSION['msg'] = "Products Variants Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

/* 28 Nov 2022 SHUBHAM*/

if($_POST['deleteValue'] == "deleteGiveExpense")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("expense_document","user_expenses","user_expense_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['expense_document'] != "" && file_exists("../../img/expense_document/".$data['expense_document']))
    {
      unlink("../../img/expense_document/".$data['expense_document']);
    }
    $q = $d->delete("user_expenses","user_expense_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Give Expense Deleted $stringIds");
    $_SESSION['msg'] = "Give Expense Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

if($_POST['deleteValue'] == "deleteSisterCompanies")
{
  $idCount = count($ids);
  for ($i = 0; $i < $idCount; $i++)
  {
    $q1 = $d->selectRow("sister_company_stamp,sister_company_logo","sister_company_master","sister_company_id = '$ids[$i]'");
    $data = $q1->fetch_assoc();
    if($data['sister_company_stamp'] != "" && file_exists("../../img/society/".$data['sister_company_stamp']))
    {
      unlink("../../img/society/".$data['sister_company_stamp']);
    }
    if($data['sister_company_logo'] != "" && file_exists("../../img/society/".$data['sister_company_logo']))
    {
      unlink("../../img/society/".$data['sister_company_logo']);
    }
    $q = $d->delete("sister_company_master","sister_company_id = '$ids[$i]'");
  }
  if($q)
  {
    echo 1;
    $d->insert_log("","$society_id",$_COOKIE['bms_admin_id'],"$created_by","Multiple Sister Companies Deleted");
    $_SESSION['msg'] = "Give Expense Deleted";
  }
  else
  {
    echo 0;
    $_SESSION['msg1'] = "Something Wrong";
  }
}

else{
  header('location:../logout');
}
 ?>
