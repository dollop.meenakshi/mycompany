<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{
 
   
    $salary_start_date = date("$salary_year-$salary_month-01"); // hard-coded '01' for first day
    $salary_end_date =   date('Y-m-t', strtotime($salary_start_date));
    $expIdsAr  = array();
    
    if($is_expanse_clear =="1" && $expense_amount>0){
        $expense_amount = $expense_amount;
    }else
    {
        $expense_amount = 0;
    }
    if($is_advance_clear =="1" && $advance_salary_paid_amount>0){
        $advance_salary_paid_amount = $advance_salary_paid_amount;
    }else
    {
        $advance_salary_paid_amount = 0;
    }

    if($loan_emi =="1" && $emi_deduction>0){
        $emi_deduction = $emi_deduction;
    }else
    {
        $emi_deduction = 0;
    }
   
    if (isset($_POST['addsalarySlip'])) {

        if ($salary_slip_status==1) {
            $checked_by = $_COOKIE['bms_admin_id'];
            $messageReturn = "Salary Slip Generated & Checked Successfully";
        } else  if ($salary_slip_status==2) {
            $checked_by = $_COOKIE['bms_admin_id'];
            $authorised_by = $_COOKIE['bms_admin_id'];
            $messageReturn = "Salary Slip Generated & Published Successfully";
        } else {
            $checked_by  = 0;
            $authorised_by  = 0;
            $messageReturn = "Salary Slip Generated Successfully";
        }

        if ($employee_net_salary<1) {
            $_SESSION['msg1'] = "Invalid Salry Amount";
            header("Location: ../salarySlip");
            exit();
        }

        $user_id =$user_id_old;
        
        $m->set_data('user_id', $user_id_old);
        $m->set_data('checked_by', $checked_by);
        $m->set_data('authorised_by', $authorised_by);
        $m->set_data('society_id', $society_id);
        $m->set_data('salary_slip_status', $salary_slip_status);
        $m->set_data('share_with_user', $share_with_user);
        $m->set_data('salary_type', test_input($salary_type));
        $m->set_data('other_deduction', $other_deduction);
        $m->set_data('extra_days_per_day_salary', $extra_days_per_day_salary);
        $m->set_data('extra_per_hour_salary', $extra_per_hour_salary);
        $m->set_data('advance_salary_paid_amount', $advance_salary_paid_amount);
        $m->set_data('overtime_amount', $overtime_amount);
        $m->set_data('emi_deduction', $emi_deduction);
        $m->set_data('floor_id', $floor_id_old);
        $m->set_data('block_id', $block_id_old);
        $m->set_data('salary_id', $salary_id);
        $m->set_data('total_holidays', $total_holidays);
        $m->set_data('leave_days', $leave_days);
        $m->set_data('unpaid_leave_days', $unpaid_leave_days);
        $m->set_data('paid_leave_days', $paid_leave_days);
        $m->set_data('paid_week_off', $total_paid_week_off);
        $m->set_data('paid_holidays', $total_paid_holiday);
        $m->set_data('salary_mode_description', test_input($salary_mode_description));
        $m->set_data('salary_mode', $salary_mode);
        $m->set_data('employee_net_salary', ($employee_net_salary));
        $m->set_data('total_ctc_cost', $employee_net_salary+$totalContribution);
        $m->set_data('month_net_salary', $month_net_salary);
        $m->set_data('total_working_hours', $total_working_hours);
        $m->set_data('total_month_hours', $total_month_hours);
       // $m->set_data('month_working_days', $month_working_days);
        $m->set_data('per_hour_salary', $per_hour_salary);
        $m->set_data('per_day_salary', $per_day_salary);
        $m->set_data('total_working_days', $employee_working_days);
        $m->set_data('total_month_days',  $month_working_days);
        $m->set_data('total_deduction_salary', $total_deduction_salary);
        $m->set_data('total_earning_salary', $total_earning_salary);
        $m->set_data('salary_increment_remark', $salary_increment_remark);
        $m->set_data('expense_amount', $expense_amount);
        $m->set_data('salary_increment_date', $salary_increment_date);
        $m->set_data('other_earning', $other_earning);
        $m->set_data('net_salary', $net_salary);
        $m->set_data('extra_days', $extra_days);
        $m->set_data('extra_hours', $extra_hours);
        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $user = $d->selectRow("users_master.unit_id,users_master.user_designation,block_master.block_name,floors_master.floor_name", "users_master,block_master,floors_master", "block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND block_master.block_id=floors_master.block_id AND users_master.user_id='$user_id_old'");
        $maData = mysqli_fetch_array($user);
        $present_designation_name = $maData['user_designation'];
        $present_branch_name = $maData['block_name'];
        $present_department_name = $maData['floor_name'];
        $m->set_data('present_branch_name', $present_branch_name);
        $m->set_data('present_department_name', $present_department_name);
        $m->set_data('present_designation_name', $present_designation_name);

        $checkSalary = $d->selectRow('*', "salary", " salary_id = '$salary_id' ");
        $checkSalaryData = mysqli_fetch_assoc($checkSalary);
        $attendaceArrayData = array();
       
        if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {
                    $valTemp =  number_format($val,2,'.','');
                    $questionTypeValue[$key] = $valTemp;
                }
        }
        if (isset($_POST['contribution_amount']) && !empty($_POST['contribution_amount'])) {
                foreach ($_POST['contribution_amount'] as $k => $v) {
                    $vlTemp =  number_format($v,2,'.','');
                    $contributionValue[$k] = $vlTemp;
                }
        }
       
         $salary_json = !empty($questionTypeValue)?json_encode($questionTypeValue):'';
         $contri_json = !empty($contributionValue)?json_encode($contributionValue):'';

        $a1 = array(
            'society_id' => $society_id,
            'salary_id' => $salary_id,
            'salary_type_current' => $checkSalaryData['salary_type'],
            'user_id' => $m->get_data('user_id'),
            'floor_id' => $m->get_data('floor_id'),
            'block_id' => $m->get_data('block_id'),
            'total_ctc_cost' => $m->get_data('total_ctc_cost'),
            'present_branch_name'=>$m->get_data('present_branch_name'),
            'present_department_name'=>$m->get_data('present_department_name'),
            'present_designation_name'=>$m->get_data('present_designation_name'),
            'salary_start_date' => $salary_start_date,
            'extra_days_per_day_salary' => $extra_days_per_day_salary,
            'overtime_amount' => $overtime_amount,
            'extra_per_hour_salary' => $extra_per_hour_salary,
            'expense_amount' => $expense_amount,
            'other_deduction' => $other_deduction,
            'other_earning' => $other_earning,
            'salary_end_date' => $salary_end_date,
            'salary_month_name' => ($salary_month . "-" . $salary_year),
            'advance_salary_paid_amount' => $m->get_data('advance_salary_paid_amount'),
            'emi_deduction' => $m->get_data('emi_deduction'),
            'total_deduction_salary' => $m->get_data('total_deduction_salary'),
            'total_earning_salary' => $m->get_data('total_earning_salary'),
            'total_net_salary' => $m->get_data('employee_net_salary'),
            'salary_slip_status' => $m->get_data('salary_slip_status'),
            'share_with_user' => $m->get_data('share_with_user'),
            'other_earning' => $m->get_data('other_earning'),
            'salary_mode_description' => $m->get_data('salary_mode_description'),
            'salary_mode' => $m->get_data('salary_mode'),
            'total_month_days' => $m->get_data('total_month_days'),
            'leave_days' => $m->get_data('leave_days'),
            'unpaid_leave_days' => $m->get_data('unpaid_leave_days'),
            'paid_leave_days' => $m->get_data('paid_leave_days'),
            'paid_week_off' => $m->get_data('paid_week_off'),
            'paid_holidays' => $m->get_data('paid_holidays'),
            'total_holidays' => $m->get_data('total_holidays'),
            'total_working_days' => $m->get_data('total_working_days'),
            'extra_days' => $m->get_data('extra_days'),
            'month_net_salary' => $m->get_data('month_net_salary'),
            'prepared_by' => $_COOKIE['bms_admin_id'],
            'checked_by' => $m->get_data('checked_by'),
            'authorised_by' => $m->get_data('authorised_by'),
            'created_date' => date('Y-m-d H:i:s'),
            'total_working_hours'=>$m->get_data('total_working_hours'),
            'total_month_hours'=>$m->get_data('total_month_hours'),
          //  'month_working_days'=>$m->get_data('month_working_days'),
            'per_hour_salary'=>$m->get_data('per_hour_salary'),
            'per_day_salary'=>$m->get_data('per_day_salary'),
            'extra_hours'=>$m->get_data('extra_hours'),
            'salary_json'=>$salary_json,
            'contribution_json'=>$contri_json,
           // 'employee_working_days'=>$m->get_data('employee_working_days'),
        );
        // echo "<pre>";
        // print_r($a1);
        // die;
        $checkSalarySlip = $d->selectRow('*', 'salary_slip_master', "salary_start_date = '$salary_start_date' AND salary_end_date='$salary_end_date' AND user_id = '$user_id_old'");
        $checkSalarySlipData = mysqli_fetch_assoc($checkSalarySlip);
        
        if ($checkSalarySlipData) {
            $_SESSION['msg1'] = "Salary Slip Already Prepared";
            header("Location: ../manageSalarySlip");
        } else {
            $q = $d->insert("salary_slip_master", $a1);
            $salary_slip_id = $con->insert_id;
          //  $q=true;
            
            if ($q == true) {
                if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                    foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {
                      //  echo $key ."<br>";

                        $salary_earning_deduction_master = $d->selectRow('*', 'salary_earning_deduction_type_master', "salary_earning_deduction_id = '$key'");
                        $salary_earning_deduction_master_data = mysqli_fetch_assoc($salary_earning_deduction_master);
                        $employer_contribution_amount = 0;
                        if(isset($_POST['contribution_amount'][$key])){
                            $employer_contribution_amount = $_POST['contribution_amount'][$key];
                        }
                        
                        
                        $salarySlipSubValueData = array(
                            'salary_earning_deduction_id' => $key,
                            'earning_deduction_name_current' => $salary_earning_deduction_master_data['earning_deduction_name'],
                            'earning_deduction_type_current' => $salary_earning_deduction_master_data['earning_deduction_type'],
                            'earning_deduction_amount' => $val,
                            'earning_deduction_parcent' =>$_POST['earning_deduction_parcent'][$key] ,
                            'created_date' => date('Y-m-d H:i:s'),
                            'salary_slip_id' => $salary_slip_id,
                            'society_id' => $society_id,
                            'employer_contribution_amount' => $employer_contribution_amount,
                            'user_id' => $a1['user_id'],
                        );

                        $checkSalarySub = $d->selectRow('*', "salary_slip_sub_master", " salary_slip_id = '$salary_slip_id' AND salary_earning_deduction_id = $key");
                       // print_r($salarySlipSubValueData);
                        $checkSalaryData = mysqli_fetch_assoc($checkSalarySub);
                        if ($checkSalaryData) {
                            $q2 = $d->update("salary_slip_sub_master", $salarySlipSubValueData, "salary_slip_sub_id='$checkSalaryData[salary_slip_sub_id]'");
                        } else {
                            $q2 = $d->insert("salary_slip_sub_master", $salarySlipSubValueData);
                        }
                    }
                }
                
                $q2 = $d->update("leave_master", array('is_salary_generated'=>1), "leave_master.user_id='$a1[user_id])'  AND DATE_FORMAT(leave_master.leave_start_date,'%c-%Y')='$a1[salary_month_name]' ");


                /* expense calculation */

                if($is_expanse_clear =="1"){
              
                    $expQ = $d->selectRow('user_expenses.*','user_expenses',"user_expenses.user_id=$a1[user_id] AND user_expenses.expense_status=1 AND user_expenses.expense_paid_status=0 AND user_expenses.date  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                    while ($expanceData= mysqli_fetch_array($expQ)) {
                      ///  print_r($expanceData);
                        array_push($expIdsAr,$expanceData['user_expense_id']);
                    }
                    $expData = implode(",",$expIdsAr);
                    $salary_month_name = $salary_month . "-" . $salary_year;
                    $expaneHistryData = array('expense_id'=>$expData,
                                                'amount'=>$expense_amount,
                                                'user_id'=>$a1['user_id'],
                                                'expense_payment_mode'=>"Settled in $salary_month_name Salary",
                                                'society_id'=>$society_id,
                                                'remark'=> "Reimbursed in salary of ".date("F-Y", strtotime('01-'.$salary_month_name)) 
                                             );
                        
                   // $expData = 'user_expense_history' 
                    $expaneHistryData['paid_by_type']=0;
                    $expaneHistryData['paid_by']=$_COOKIE['bms_admin_id'];       
                    $UsrExpHistory = $d->insert("user_expense_history", $expaneHistryData);
                    $expiDs = join("','",$expIdsAr);
                    
                    $q2 = $d->update("user_expenses",array('expense_paid_status'=>1), "user_expense_id IN ('$expiDs')");
                   
                }

                $salary_month_name = date('F', strtotime($salary_start_date));

                if($share_with_user==1){
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$a1[user_id]'");
                  $data2 = mysqli_fetch_assoc($q2);
                  $title = "Salary Slip Published";
                  
                  $description = "For Month of $salary_month_name, $salary_year";
                  $menuClick = "Salary_Slip";
                  $image = "";
                  $activity = "Salary_Slip";
                  $user_token = $data2['user_token'];
                  $device = $data2['device'];
                    if ($data2['user_token']!="") {
                      if ($device=='android') {
                            $v =   $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                      } else {
                          $v =  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                      }

                        $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$a1['user_id'],
                            'notification_title'=>$title,
                            'notification_desc'=>$description,    
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'Salary_Slip',
                            'notification_logo'=>'payslip.png',
                            'notification_type'=>'0',
                        );
                        
                        $d->insert("user_notification",$notiAry);
                    }

                }

                if($is_advance_clear==1 && $advance_salary_paid_amount>0){
                    $user_id = $a1['user_id'];
                    $crMonthYr =date('Y-m',strtotime($salary_start_date));
                    $salary_month_name =date('M Y',strtotime($salary_start_date));
                    //print_r($crMonthYr);die;
                    $advance_salary_paid_remark = "deducted from $salary_month_name month salary";
                    
                    $q2 = $d->update("advance_salary",array('is_paid'=>1,'paid_date'=>date('Y-m-d H:i'),'amount_receive_by'=>$_COOKIE['bms_admin_id'],'advance_salary_paid_remark'=>$advance_salary_paid_remark), "advance_salary_date BETWEEN '$salary_start_date' AND '$salary_end_date' AND user_id = $user_id");
                    
                    $q2 = $d->update("monthly_advance_salary",array('is_complete_paid'=>1,'paid_by'=>$_COOKIE['bms_admin_id'],'total_paid_amount'=>$advance_salary_paid_amount,'single_paid_date'=>date('Y-m-d H:i'),'single_paid_date_remark'=>$advance_salary_paid_remark), " month='$crMonthYr' AND user_id=$user_id");
                   
                }
                

                if($loan_emi==1 && $emi_deduction>0){
                     $user_id = $a1['user_id'];
                   
                    $salary_month_name =date('M Y',strtotime($salary_start_date));

                    $loan_emi_ids = array();
                    $loadAmount = array();
                    $expQ = $d->selectRow('loan_emi_master.*','loan_emi_master',"loan_emi_master.user_id='$user_id'  AND loan_emi_master.is_emi_paid=0 AND loan_emi_master.emi_created_at  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                      while ($loanData= mysqli_fetch_array($expQ)) {
                          array_push($loan_emi_ids,$loanData['loan_emi_id']);
                          array_push($loadAmount,$loanData['emi_amount']);
                      }

                    $emi_paid_remark = "deducted from $salary_month_name month salary";
                   echo $emiDs = join("','",$loan_emi_ids);
                    $d->update("loan_emi_master",array('is_emi_paid'=>1,'emi_paid_by'=>$bms_admin_id,'emi_paid_remark'=>$emi_paid_remark,'received_date'=>date('Y-m-d H:i')), "loan_emi_id IN ('$emiDs') AND user_id='$user_id'");


                }


                $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$messageReturn");

                $_SESSION['msg'] = "$messageReturn";
                header("Location: ../manageSalarySlip?dId=$a1[floor_id]&bId=$a1[block_id]&month_year=$salary_month&laYear=$salary_year");
            } else {
                $_SESSION['msg1'] = "Something Wrong";
                header("Location: ../salarySlip");
            }
        }
    }
    if (isset($_POST['editSalarySlip'])) {
      
       // print_r($_POST);die;
        if ($employee_net_salary<1 || $employee_net_salary=='NaN') {
            $_SESSION['msg1'] = "Invalid Salry Amount";
            header("Location: ../salarySlip");
            exit();
        }

        $m->set_data('user_id', $user_id_old);
        $m->set_data('society_id', $society_id);
        $m->set_data('salary_type', $salary_type);
        $m->set_data('salary_slip_status', $salary_slip_status);
        $m->set_data('share_with_user', $share_with_user);
        $m->set_data('other_deduction', $other_deduction);
        $m->set_data('overtime_amount', $overtime_amount);
        $m->set_data('advance_salary_paid_amount', $advance_salary_paid_amount);
        $m->set_data('emi_deduction', $emi_deduction);
        $m->set_data('floor_id', $floor_id_old);
        $m->set_data('block_id', $block_id_old);
        $m->set_data('salary_id', $salary_id);
        $m->set_data('total_holidays', $total_holidays);
        $m->set_data('extra_days_per_day_salary', $extra_days_per_day_salary);
        $m->set_data('extra_per_hour_salary', $extra_per_hour_salary);
        $m->set_data('leave_days', $leave_days);
        $m->set_data('unpaid_leave_days', $unpaid_leave_days);
        $m->set_data('paid_leave_days', $paid_leave_days);
        $m->set_data('salary_mode_description', test_input($salary_mode_description));
        $m->set_data('salary_mode', $salary_mode);
        $m->set_data('employee_net_salary', ($employee_net_salary));
        $m->set_data('total_ctc_cost', $employee_net_salary+$totalContribution);        
        $m->set_data('total_working_days', $employee_working_days);
        $m->set_data('total_month_days',  $month_working_days);
        $m->set_data('total_deduction_salary', $total_deduction_salary);
        $m->set_data('total_earning_salary', $total_earning_salary);
        $m->set_data('salary_increment_remark', $salary_increment_remark);
        $m->set_data('salary_increment_date', $salary_increment_date);
        $m->set_data('other_earning', $other_earning);
        $m->set_data('net_salary', $net_salary);
        $m->set_data('extra_days', $extra_days);
        $m->set_data('expense_amount', $expense_amount);
        $m->set_data('extra_hours', $extra_hours);
        $m->set_data('created_at', date("Y-m-d H:i:s"));
        $user = $d->selectRow("users_master.unit_id,users_master.user_designation,block_master.block_name,floors_master.floor_name", "users_master,block_master,floors_master", "block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND block_master.block_id=floors_master.block_id AND users_master.user_id='$user_id_old'");
        $maData = mysqli_fetch_array($user);
        $present_designation_name = $maData['user_designation'];
        $present_branch_name = $maData['block_name'];
        $present_department_name = $maData['floor_name'];
        $m->set_data('present_branch_name', $present_branch_name);
        $m->set_data('present_department_name', $present_department_name);
        $m->set_data('present_designation_name', $present_designation_name);

        $checkSalary = $d->selectRow('*', "salary", " salary_id = '$salary_id' ");
        $checkSalaryData = mysqli_fetch_assoc($checkSalary);
        $attendaceArrayData = array();

        if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {
                    $valTemp =  number_format($val,2,'.','');
                    $questionTypeValue[$key] = $valTemp;
                }
        }

        if (isset($_POST['contribution_amount']) && !empty($_POST['contribution_amount'])) {
            foreach ($_POST['contribution_amount'] as $k => $v) {
                $vlTemp =  number_format($v,2,'.','');
                $contributionValue[$k] = $vlTemp;
            }
    }

         $salary_json = !empty($questionTypeValue)?json_encode($questionTypeValue):'';
         $contri_json = !empty($contributionValue)?json_encode($contributionValue):'';
        
        $a1 = array(
            'society_id' => $society_id,
            'salary_id' => $salary_id,
            'salary_type_current' => $checkSalaryData['salary_type'],
            'user_id' => $m->get_data('user_id'),
            'floor_id' => $m->get_data('floor_id'),
            'block_id' => $m->get_data('block_id'),
            'present_branch_name'=>$m->get_data('present_branch_name'),
            'present_department_name'=>$m->get_data('present_department_name'),
            'present_designation_name'=>$m->get_data('present_designation_name'),
            'salary_start_date' => $salary_start_date,
            'extra_days_per_day_salary' => $extra_days_per_day_salary,
            'extra_per_hour_salary' => $extra_per_hour_salary,
            'other_deduction' => $other_deduction,
            'overtime_amount' => $overtime_amount,
            'other_earning' => $other_earning,
            'salary_end_date' => $salary_end_date,
            'salary_month_name' => ($salary_month . "-" . $salary_year),
            'total_deduction_salary' => $m->get_data('total_deduction_salary'),
            'total_earning_salary' => $m->get_data('total_earning_salary'),
            'advance_salary_paid_amount' => $m->get_data('advance_salary_paid_amount'),
            'emi_deduction' => $m->get_data('emi_deduction'),
            'total_ctc_cost' => $m->get_data('total_ctc_cost'),
            'total_net_salary' => $m->get_data('employee_net_salary'),
            'expense_amount'=>$m->get_data('expense_amount'),
            //'other_earning' => $m->get_data('other_earning'),
            'salary_mode_description' => $m->get_data('salary_mode_description'),
            'salary_mode' => $m->get_data('salary_mode'),
            'salary_slip_status' => $m->get_data('salary_slip_status'),
            'share_with_user' => $m->get_data('share_with_user'),
            'total_month_days' => $m->get_data('total_month_days'),
            'leave_days' => $m->get_data('leave_days'),
            'unpaid_leave_days' => $m->get_data('unpaid_leave_days'),
            'paid_leave_days' => $m->get_data('paid_leave_days'),
            'total_holidays' => $m->get_data('total_holidays'),
            'total_working_days' => $m->get_data('total_working_days'),
            'extra_days' => $m->get_data('extra_days'),
            'extra_hours' => $m->get_data('extra_hours'),
            'salary_json' => $salary_json,
            'contribution_json' => $contri_json,
            'prepared_by' => $_COOKIE['bms_admin_id'],
            'created_date' => date('Y-m-d H:i:s')
        );

        $checkSalarySlip = $d->selectRow('*', 'salary_slip_master', "salary_start_date = '$salary_start_date' AND salary_end_date='$salary_end_date' AND user_id = '$user_id_old'");
        $checkSalarySlipData = mysqli_fetch_assoc($checkSalarySlip);

        $q = $d->update("salary_slip_master", $a1, "salary_slip_id=$salary_slip_id");

        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Slip Added Successfully");
        if ($q == true) {
            $questionTypeValue = array();
            if (isset($_POST['salary_earning_deduction_id']) && !empty($_POST['salary_earning_deduction_id'])) {
                foreach ($_POST['salary_earning_deduction_id'] as $key => $val) {

                    $salary_earning_deduction_master = $d->selectRow('*', 'salary_earning_deduction_type_master', "salary_earning_deduction_id = '$key'");
                    $employer_contribution_amount = 0;
                    if(isset($_POST['contribution_amount'][$key])){
                        $employer_contribution_amount = $_POST['contribution_amount'][$key];
                    }
                    $salary_earning_deduction_master_data = mysqli_fetch_assoc($salary_earning_deduction_master);
                    $salarySlipSubValueData = array(
                        'salary_earning_deduction_id' => $key,
                        'earning_deduction_name_current' => $salary_earning_deduction_master_data['earning_deduction_name'],
                        'earning_deduction_type_current' => $salary_earning_deduction_master_data['earning_deduction_type'],
                        'earning_deduction_amount' => $val,
                        'earning_deduction_parcent' =>$_POST['earning_deduction_parcent'][$key] ,
                        'created_date' => date('Y-m-d H:i:s'),
                        'salary_slip_id' => $salary_slip_id,
                        'society_id' => $society_id,
                        'employer_contribution_amount' => $employer_contribution_amount,
                        'user_id' => $a1['user_id'],
                    );
                    $checkSalarySub = $d->selectRow('*', "salary_slip_sub_master", " salary_slip_id = '$salary_slip_id' AND salary_earning_deduction_id = $key");
                    $checkSalaryData = mysqli_fetch_assoc($checkSalarySub);
                    if ($checkSalaryData) {
                        $q2 = $d->update("salary_slip_sub_master", $salarySlipSubValueData, "salary_slip_sub_id='$checkSalaryData[salary_slip_sub_id]'");
                    } else {
                        $q2 = $d->insert("salary_slip_sub_master", $salarySlipSubValueData);
                    }


               
                }
               
            }

                if($is_expanse_clear =="1" && $expense_amount>0 && $newExpenceClear==1){
              
                    $expQ = $d->selectRow('user_expenses.*','user_expenses',"user_expenses.user_id=$a1[user_id] AND user_expenses.expense_status=1 AND user_expenses.expense_paid_status=0 AND user_expenses.date  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                    while ($expanceData= mysqli_fetch_array($expQ)) {
                      ///  print_r($expanceData);
                        array_push($expIdsAr,$expanceData['user_expense_id']);
                    }
                    $expData = implode(",",$expIdsAr);
                    $salary_month_name = $salary_month . "-" . $salary_year;
                    $expaneHistryData = array('expense_id'=>$expData,
                                                'amount'=>$expense_amount,
                                                'user_id'=>$a1['user_id'],
                                                'expense_payment_mode'=>"Settled in $salary_month_name Salary",
                                                'society_id'=>$society_id,
                                                'remark'=> "Reimbursed in salary of ".date("F-Y", strtotime('01-'.$salary_month_name)) 
                                             );
                        
                   // $expData = 'user_expense_history' 
                    $expaneHistryData['paid_by_type']=0;
                    $expaneHistryData['paid_by']=$_COOKIE['bms_admin_id'];       
                    $UsrExpHistory = $d->insert("user_expense_history", $expaneHistryData);
                    $expiDs = join("','",$expIdsAr);
                    
                    $q2 = $d->update("user_expenses",array('expense_paid_status'=>1), "user_expense_id IN ('$expiDs')");
                   
                }

                if($is_advance_clear==1 && $advance_salary_paid_amount>0 && $newAdvanceSalary==1){
                    $user_id = $a1['user_id'];
                    $crMonthYr =date('Y-m',strtotime($salary_start_date));
                    $salary_month_name =date('M Y',strtotime($salary_start_date));
                    //print_r($crMonthYr);die;
                    $advance_salary_paid_remark = "deducted from $salary_month_name month salary";
                    
                    $q2 = $d->update("advance_salary",array('is_paid'=>1,'paid_date'=>date('Y-m-d H:i'),'amount_receive_by'=>$_COOKIE['bms_admin_id'],'advance_salary_paid_remark'=>$advance_salary_paid_remark), "advance_salary_date BETWEEN '$salary_start_date' AND '$salary_end_date' AND user_id = $user_id");
                    
                    $q2 = $d->update("monthly_advance_salary",array('is_complete_paid'=>1,'paid_by'=>$_COOKIE['bms_admin_id'],'total_paid_amount'=>$advance_salary_paid_amount,'single_paid_date'=>date('Y-m-d H:i'),'single_paid_date_remark'=>$advance_salary_paid_remark), " month='$crMonthYr' AND user_id=$user_id");
                   
                }
                

                if($loan_emi==1 && $emi_deduction>0 && $newEmiClear==1){
                     $user_id = $a1['user_id'];
                   
                    $salary_month_name =date('M Y',strtotime($salary_start_date));

                    $loan_emi_ids = array();
                    $loadAmount = array();
                    $expQ = $d->selectRow('loan_emi_master.*','loan_emi_master',"loan_emi_master.user_id='$user_id'  AND loan_emi_master.is_emi_paid=0 AND loan_emi_master.emi_created_at  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                      while ($loanData= mysqli_fetch_array($expQ)) {
                          array_push($loan_emi_ids,$loanData['loan_emi_id']);
                          array_push($loadAmount,$loanData['emi_amount']);
                      }

                    $emi_paid_remark = "deducted from $salary_month_name month salary";
                   echo $emiDs = join("','",$loan_emi_ids);
                    $d->update("loan_emi_master",array('is_emi_paid'=>1,'emi_paid_by'=>$bms_admin_id,'emi_paid_remark'=>$emi_paid_remark,'received_date'=>date('Y-m-d H:i')), "loan_emi_id IN ('$emiDs') AND user_id='$user_id'");


                }

            if($share_with_user==1){
                $q2 = $d->selectRow('user_token,device',"users_master","user_id='$a1[user_id]'");
                  $data2 = mysqli_fetch_assoc($q2);
                  $title = "Salary Slip Share With User ";
                  $description = "By Admin $created_by";
                  $menuClick = "";
                  $image = "";
                  $activity = "Salary_Slip";
                  $user_token = $data2['user_token'];
                  $device = $data2['device'];
                  if ($device=='android') {
                    
                    $v =   $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                  } else {
                      $v =  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                  }
            }
          //  print_r($salary_month);die;
            $_SESSION['msg'] = "Salary Slip Update Successfully";
            header("Location: ../manageSalarySlip?dId=$a1[floor_id]&bId=$a1[block_id]&month_year=$salary_month&laYear=$salary_year");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../salarySlip");
        }
    }


   }
