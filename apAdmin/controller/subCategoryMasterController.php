<?php
include '../common/objectController.php';
extract(array_map("test_input", $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

  //IS_605
  if (isset($_POST['addSubCategory'])) {
    $m->set_data('product_sub_category_id', $product_sub_category_id);
    $m->set_data('society_id', $society_id);
    $m->set_data('sub_category_name', $sub_category_name);
    $m->set_data('sub_category_description', $sub_category_description);
    $m->set_data('product_category_id', $product_category_id);
    $m->set_data('product_sub_category_created_date', date("Y-m-d H:i:s"));
    //
    $a1 = array(
      'society_id' => $m->get_data('society_id'),
      'sub_category_name' => $m->get_data('sub_category_name'),
      'sub_category_description' => $m->get_data('sub_category_description'),
      'product_category_id' => $m->get_data('product_category_id'),
      'product_sub_category_created_date' => $m->get_data('product_sub_category_created_date'),
    );

    if (isset($product_sub_category_id) && $product_sub_category_id > 0) {
      $sql = $d->selectRow('product_sub_category_master.*', 'product_sub_category_master', "  product_category_id=$cat_id AND sub_category_name='$sub_category_name'");

      $getData = mysqli_fetch_assoc($sql);

      if ($getData) {
        if ($product_sub_category_id == $getData['product_sub_category_id']) {
          $a1['product_category_id'] = $cat_id;
          $q = $d->update("product_sub_category_master", $a1, "product_sub_category_id ='$product_sub_category_id'");

          $_SESSION['msg'] = "Sub Category Updated Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Updated Successfully");
        } else {
          $_SESSION['msg'] = "Sub Category Already Exist ";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Already Exist");
        }
      } else {
        $a1['product_category_id'] = $cat_id;
        $q = $d->update("product_sub_category_master", $a1, "product_sub_category_id ='$product_sub_category_id'");
        $_SESSION['msg'] = "Sub Category Updated Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Updated Successfully");
      }
    } else {
      $sql = $d->selectRow('product_sub_category_master.*', 'product_sub_category_master', " product_category_id=$product_category_id AND sub_category_name='$sub_category_name'");
      $getData = mysqli_fetch_assoc($sql);
      if ($getData) {
        $_SESSION['msg'] = "Sub Category Already Exist ";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Already Exist");
      } else {
        $q = $d->insert("product_sub_category_master", $a1);
        $_SESSION['msg'] = "Sub Category Added Successfully";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Sub Category Added Successfully");
      }
    }
    if ($q == TRUE) {

      header("Location: ../subCategoryMaster");
    } else {
      $_SESSION['msg1'] = "Something Wrong";
      header("Location: ../subCategoryMaster");
    }
  }

  if (isset($_POST['addVendorSubCategory'])) {
    if (count($_POST['vendor_id']) > 0) {
      for ($j = 0; $j < count($_POST['vendor_id']); $j++) {
        $vendor_id = $_POST['vendor_id'][$j];
        $m->set_data('product_sub_category_id', $product_sub_category_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('vendor_id', $vendor_id);

        $a1 = array(
          'society_id' => $m->get_data('society_id'),
          'product_sub_category_id' => $m->get_data('product_sub_category_id'),
          'vendor_id' => $m->get_data('vendor_id'),
          'created_date' => date("Y-m-d H:i:s"),
        );
        $sql = $d->selectRow('product_sub_category_vendor_master.*', 'product_sub_category_vendor_master', " product_sub_category_id=$product_sub_category_id AND vendor_id='$vendor_id'");
        $getData = mysqli_fetch_assoc($sql);
        if ($getData) {
          $_SESSION['msg'] = "vendor Sub Category Already Exist ";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor Sub Category Already Exist");
        } else {
          $q = $d->insert("product_sub_category_vendor_master", $a1);
          $_SESSION['msg'] = "vendor Sub Category Added Successfully";
          $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", " vendor Sub Category Added Successfully");
        }
        if ($q == TRUE) {
          header("Location: ../productVendorSubCatMaster?scId=$product_sub_category_id");
        } else {
          $_SESSION['msg1'] = "Something Wrong";
          header("Location: ../productVendorSubCatMaster?scId=$product_sub_category_id");
        }
      }
    }
  }
}
