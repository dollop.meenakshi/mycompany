<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['addTemplete'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('templete_name', $templete_name);
        $m->set_data('templete_description', $templete_description);
        $m->set_data('templete_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('templete_created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'templete_name' => $m->get_data('templete_name'),
            'templete_description' => $m->get_data('templete_description'),
            'templete_created_by' => $m->get_data('templete_created_by'),
            'templete_created_date' => $m->get_data('templete_created_date'),
        );

        if (isset($templete_id ) && $templete_id  > 0) {
            $q = $d->update("template_master", $a1, "templete_id  ='$templete_id'");
            $_SESSION['msg'] = "Templete Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Templete Updated Successfully");
        } else {
            $q = $d->insert("template_master", $a1);
            $_SESSION['msg'] = "Templete Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Templete Added Successfully");

        }
        if ($q == true) {
            header("Location: ../workReportTemplete");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../workReportTemplete");
        }

    }

    if (isset($_POST['addTemplateQuestion'])) {

        $questionTypeValue = array();
        $question_value_count = count($_POST['question_type_value']);
        for ($i=0; $i < $question_value_count; $i++) { 
            if($_POST['question_type_value'][$i] != ''){
                $questionTypeValue['option_'.$i] = $_POST['question_type_value'][$i];
            }
        }
        
        $question_type_value = !empty($questionTypeValue)?json_encode($questionTypeValue):'';
        
        $m->set_data('society_id', $society_id);
        $m->set_data('templete_id', $templete_id);
        $m->set_data('templete_question', $templete_question);
        $m->set_data('question_type', $question_type);
        $m->set_data('is_required', $is_required);
        $m->set_data('question_type_value', $question_type_value);
        $m->set_data('question_value_count', $question_value_count);
        $m->set_data('templete_question_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('templete_question_created_date',date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'templete_id' => $m->get_data('templete_id'),
            'templete_question' => $m->get_data('templete_question'),
            'question_type' => $m->get_data('question_type'),
            'is_required' => $m->get_data('is_required'),
            'question_type_value' => $m->get_data('question_type_value'),
            'question_value_count' => $m->get_data('question_value_count'),
            'templete_question_created_by' => $m->get_data('templete_question_created_by'),
            'templete_question_created_date' => $m->get_data('templete_question_created_date'),
        );
        
        if (isset($template_question_id) && $template_question_id  > 0) {
            $q = $d->update("template_question_master", $a1, "template_question_id='$template_question_id'");
            $_SESSION['msg'] = "Templete Question Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Templete Question Updated Successfully");
        } else {
            $q = $d->insert("template_question_master", $a1);
            $_SESSION['msg'] = "Templete Question Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Templete Question Added Successfully");
        }
        if ($q == true) {
            if(isset($_POST['addMoreTemplateQuestion'])  && $_POST['addMoreTemplateQuestion'] == 'addMoreTemplateQuestion'){
                header("Location: ../addTempleteQuestion?id=$templete_id");
            }else{
                header("Location: ../templeteQuestions?id=$templete_id&type=edit");
            }
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../templeteQuestions?id=$templete_id&type=edit");
        }

    }

    if(isset($_POST['assignWorkReportTemplete'])){


        if($template_assign_for == 0){
			$template_assign_ids = 0;
		}
    	else if($template_assign_for == 1 && $_POST['template_assign_ids'][0]==0){
			$template_assign_for = 0;
			$template_assign_ids = 0;
		}
		else if($template_assign_for == 2 && $_POST['template_assign_ids'][0]==0){
			$template_assign_for = 1;
			$template_assign_ids = implode(',', $_POST['blockId']);
		}
		else if($template_assign_for == 3 && $_POST['template_assign_ids'][0]==0){
			$template_assign_for = 2;
			$template_assign_ids = implode(',', $_POST['floorId']);
		}else{
			$template_assign_ids = implode(',', $_POST['template_assign_ids']);
		}

        $m->set_data('templete_id',$templete_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('template_assign_for',$template_assign_for);
        $m->set_data('template_assign_ids',$template_assign_ids);
        $m->set_data('template_assign_by',$_COOKIE['bms_admin_id']);
        $m->set_data('template_assign_date',date("Y-m-d H:i:s"));

        $a =array(
            'templete_id'=>$m->get_data('templete_id'),
            'society_id'=>$m->get_data('society_id'),
            'template_assign_for'=>$m->get_data('template_assign_for'),
            'template_assign_ids'=>$m->get_data('template_assign_ids'),
            'template_assign_by'=>$m->get_data('template_assign_by'),
            'template_assign_date'=>$m->get_data('template_assign_date'),
        );

        $aq = $d->selectRow('template_assign_id',"template_assign_master","templete_id='$templete_id'");
        $assignData = mysqli_fetch_array($aq);

		if($assignData){
			$q = $d->update("template_assign_master", $a, "template_assign_id='$assignData[template_assign_id]'");
            $message = "Template Assign Data Updated Successfully";
		}else{
			$q = $d->insert("template_assign_master",$a);
            $message = "Template Assigned Successfully";
		}

        if($q){
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$message");
            $_SESSION['msg'] = "$message";
            header("Location: ../manageAssignTemplate");
        } else {
            $_SESSION['msg1']="Something Wrong";
            header("Location: ../manageAssignTemplate");
        }

    }

    if(isset($_POST['delete_template_question'])){

        $q=$d->delete("template_question_master","template_question_id='$template_question_id'");

        if($q>0) {
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Templete Question Deleted");
            $_SESSION['msg']="Templete Question Deleted.";
            header("Location: ../templeteQuestions?id=$templete_id&type=edit");
          } else {
            $_SESSION['msg1']="Something Wrong";
            header("Location: ../templeteQuestions?id=$templete_id&type=edit");
          }
    }
}
