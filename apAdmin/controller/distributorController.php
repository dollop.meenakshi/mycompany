<?php
include '../common/objectController.php';

if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
    if(isset($_POST['addDistributor']))
    {
        $file = $_FILES['distributor_photo']['tmp_name'];
        if(file_exists($file))
        {
            $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png',
                'image/webp'
            );
            if(($_FILES['distributor_photo']['size'] >= $maxsize) || ($_FILES["distributor_photo"]["size"] == 0))
            {
                $_SESSION['msg1']=" Photo too large. File must be less than 1 MB.";
                header("location:../addDistributor");exit;
            }
            if(!in_array($_FILES['distributor_photo']['type'], $acceptable) && (!empty($_FILES["distributor_photo"]["type"])))
            {
                $_SESSION['msg1']="Invalid Photo type. Only JPG and PNG types are accepted.";
                header("location:../addDistributor");exit;
            }
            if(isset($distributor_id) && $distributor_id > 0 && $old_photo != "")
            {
                unlink("../../img/users/recident_profile/".$old_photo);
            }
            $distributor_name_image = str_replace(' ', '_', $distributor_name);
            $temp = explode(".", $_FILES["distributor_photo"]["name"]);
            $distributor_photo = "Distributor_".$distributor_name_image."_".rand().'.' . end($temp);
            move_uploaded_file($_FILES["distributor_photo"]["tmp_name"], "../../img/users/recident_profile/".$distributor_photo);
        }
        else
        {
            if(isset($old_photo))
            {
                $distributor_photo = $old_photo;
            }
            else
            {
                $distributor_photo = "";
            }
        }
        $m->set_data('society_id', $society_id);
        $m->set_data('country_id', $country_id);
        $m->set_data('distributor_name', $distributor_name);
        $m->set_data('distributor_code', $distributor_code);
        $m->set_data('distributor_contact_person', $distributor_contact_person);
        $m->set_data('distributor_contact_person_number', $distributor_contact_person_number);
        $m->set_data('distributor_contact_person_country_code', $distributor_contact_person_country_code);
        $m->set_data('distributor_alt_contact_number', $distributor_alt_contact_number);
        $m->set_data('distributor_alt_contact_number_country_code', $distributor_alt_contact_number_country_code);
        $m->set_data('distributor_email', $distributor_email);
        $m->set_data('distributor_email_main', $distributor_email_main);
        $m->set_data('distributor_address', $distributor_address);
        $m->set_data('distributor_pincode', $distributor_pincode);
        $m->set_data('distributor_gst_no', $distributor_gst_no);
        $m->set_data('distributor_google_address', $location_name);
        $m->set_data('distributor_latitude', $distributor_latitude);
        $m->set_data('distributor_longitude', $distributor_longitude);
        $m->set_data('distributor_photo', $distributor_photo);
        if(isset($distributor_id) && $distributor_id > 0)
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'distributor_name' => $m->get_data('distributor_name'),
                'distributor_code' => $m->get_data('distributor_code'),
                'distributor_contact_person' => $m->get_data('distributor_contact_person'),
                'distributor_contact_person_number' => $m->get_data('distributor_contact_person_number'),
                'distributor_contact_person_country_code' => $m->get_data('distributor_contact_person_country_code'),
                'distributor_alt_contact_number' => $m->get_data('distributor_alt_contact_number'),
                'distributor_alt_contact_number_country_code' => $m->get_data('distributor_alt_contact_number_country_code'),
                'distributor_email' => $m->get_data('distributor_email'),
                'distributor_email_main' => $m->get_data('distributor_email_main'),
                'distributor_address' => $m->get_data('distributor_address'),
                'distributor_google_address' => $m->get_data('distributor_google_address'),
                'distributor_pincode' => $m->get_data('distributor_pincode'),
                'distributor_gst_no' => $m->get_data('distributor_gst_no'),
                'distributor_latitude' => $m->get_data('distributor_latitude'),
                'distributor_longitude' => $m->get_data('distributor_longitude'),
                'distributor_photo' => $m->get_data('distributor_photo'),
                'distributor_modified_date' => date("Y-m-d H:i:s"),
                'distributor_updated_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->update("distributor_master",$a1,"distributor_id = '$distributor_id'");
            $_SESSION['msg'] = "Distributor Update Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Distributor Updated Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Distributor Successfully Updated.";
                header("Location: ../manageDistributor?CId=$country_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../manageDistributor?CId=$country_id");exit;
            }
        }
        else
        {
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'country_id' => $m->get_data('country_id'),
                'distributor_name' => $m->get_data('distributor_name'),
                'distributor_code' => $m->get_data('distributor_code'),
                'distributor_contact_person' => $m->get_data('distributor_contact_person'),
                'distributor_contact_person_number' => $m->get_data('distributor_contact_person_number'),
                'distributor_contact_person_country_code' => $m->get_data('distributor_contact_person_country_code'),
                'distributor_alt_contact_number' => $m->get_data('distributor_alt_contact_number'),
                'distributor_alt_contact_number_country_code' => $m->get_data('distributor_alt_contact_number_country_code'),
                'distributor_email' => $m->get_data('distributor_email'),
                'distributor_email_main' => $m->get_data('distributor_email_main'),
                'distributor_address' => $m->get_data('distributor_address'),
                'distributor_google_address' => $m->get_data('distributor_google_address'),
                'distributor_pincode' => $m->get_data('distributor_pincode'),
                'distributor_gst_no' => $m->get_data('distributor_gst_no'),
                'distributor_latitude' => $m->get_data('distributor_latitude'),
                'distributor_longitude' => $m->get_data('distributor_longitude'),
                'distributor_photo' => $m->get_data('distributor_photo'),
                'distributor_created_date' => date("Y-m-d H:i:s"),
                'distributor_created_by' => $_COOKIE['bms_admin_id']
            );
            $q = $d->insert("distributor_master", $a1);
            $_SESSION['msg'] = "Distributor Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Distributor Added Successfully");
            if($q)
            {
                $_SESSION['msg'] = "Distributor Successfully Added.";
                header("Location: ../manageDistributor?CId=$country_id");exit;
            }
            else
            {
                $_SESSION['msg1'] = "Something Wrong!";
                header("Location: ../addDistributor");
            }
        }
    }
    elseif(isset($_POST['getStateCityTag']))
    {
        $q = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['getAreaTag']))
    {
        $q = $d->selectRow("area_id,area_name","area_master_new","country_id = '$country_id' AND state_id = '$state_id' AND city_id = '$city_id'");
        $all_data = [];
        while($data = $q->fetch_assoc())
        {
            $all_data[] = $data;
        }
        echo json_encode($all_data);exit;
    }
    elseif(isset($_POST['deleteDistributor']))
    {
        if($distributor_photo != "")
        {
            unlink("../../img/users/recident_profile/".$distributor_photo);
        }
        $q = $d->delete("distributor_master","distributor_id = '$distributor_id'");
        if($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Distributor deleted");
            $_SESSION['msg'] = "Distributor Successfully Delete.";
            header("Location: ../manageDistributor?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong!";
            header("Location: ../manageDistributor?CId=$country_id&SCId=$city_id-$state_id&AId=$area_id");exit;
        }
    }
    elseif(isset($_POST['checkDistributorName']))
    {
        if(isset($distributor_id))
        {
            if(isset($distributor_name) && !empty($distributor_name) && isset($distributor_contact_person_country_code) && !empty($distributor_contact_person_country_code) && isset($distributor_contact_person_number) && !empty($distributor_contact_person_number))
            {
                $q = $d->selectRow("distributor_name","distributor_master","distributor_name = '$distributor_name' AND distributor_id != '$distributor_id' AND distributor_contact_person_country_code = '$distributor_contact_person_country_code' AND distributor_contact_person_number = '$distributor_contact_person_number'");
            }
        }
        else
        {
            if(isset($distributor_name) && !empty($distributor_name) && isset($distributor_contact_person_country_code) && !empty($distributor_contact_person_country_code) && isset($distributor_contact_person_number) && !empty($distributor_contact_person_number))
            {
                $q = $d->selectRow("distributor_name","distributor_master","distributor_name = '$distributor_name' AND distributor_contact_person_country_code = '$distributor_contact_person_country_code' AND distributor_contact_person_number = '$distributor_contact_person_number'");
            }
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['getAreaLatLon']))
    {
        $response = [];
        $q1 = $d->selectRow("latitude,longitude","area_master_new","area_id = '$area_id'");
        $fetch = $q1->fetch_assoc();
        if($fetch['latitude'] == "" || empty($fetch['latitude']) || $fetch['latitude'] == 0)
        {
            $response['area_latitude'] = 23.037786;
        }
        else
        {
            $response['area_latitude'] = $fetch['latitude'];
        }

        if($fetch['longitude'] == "" || empty($fetch['longitude']) || $fetch['longitude'] == 0)
        {
            $response['area_longitude'] = 72.512043;
        }
        else
        {
            $response['area_longitude'] = $fetch['longitude'];
        }
        echo json_encode($response);exit;
    }
}
?>