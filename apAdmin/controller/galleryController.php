<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// add new Notice Board
if(isset($_POST) && !empty($_POST) )

{
$temDate = date("Y-m-d h:i A");
  if(isset($addGallery)) {
    //echo "<pre>";
    //print_r($_POST);
    //print_r($floor_id);
    //die;
   //$m->set_data('gallary_group_id', '0'); 
    
    if((isset($event_id) && $event_id!= '') || (isset($album_title) && $album_title!= '')){
        $block_id = join(",",$_POST['block_id']); 
        $floor_id = join(",",$_POST['floor_id']); 
        if ($event_id>0) {
            $qc = $d->selectRow("event_id,event_title,event_start_date","event_master","event_id='$event_id'");
            $eventData=mysqli_fetch_array($qc);
            $album_title=$eventData['event_title'];
            $event_date=$eventData['event_start_date'];
        } else {
            $album_title=$album_title;
            $album_title_filter = str_replace("'", "\'", $album_title);
            $eqm = $d->select("gallery_album_master","event_id!='0' AND society_id = '$society_id' and album_title =  '".$album_title_filter."' ");
            if (mysqli_num_rows($eqm)>0) {
                $_SESSION['msg1']="$album_title event already exists, please select this event and upload photo.";
                header("Location: ../gallery");
                exit();
            }
            $event_id =0;
        }
  
        $m->set_data('society_id', $society_id);
        $m->set_data('album_title', $album_title);
        $m->set_data('event_id', $event_id);
        $m->set_data('event_date', $event_date);
        $m->set_data('block_id', $block_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('gallery_album_created_at', date("Y-m-d H:i:s"));

        $a = array(
            'society_id' => $m->get_data('society_id'),
            'album_title' => $m->get_data('album_title'),
            'event_id' => $m->get_data('event_id'),
            'event_date' => $m->get_data('event_date'),
            'block_id' => $m->get_data('block_id'),
            'floor_id' => $m->get_data('floor_id'),
            'gallery_album_created_at' => $m->get_data('gallery_album_created_at'),
            'gallery_album_update_at' => $m->get_data('gallery_album_created_at'),
        );
        
        $q = $d->insert("gallery_album_master", $a);
        $gallery_album_id = $con->insert_id;
    }

    if($gallery_album_id){
        $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
        $notiUrl="";
        foreach($_FILES["gallery_photo"]["tmp_name"] as $key=>$tmp_name) {
            $file_name=$_FILES["gallery_photo"]["name"][$key];
            $file_tmp=$_FILES["gallery_photo"]["tmp_name"][$key];
            $uploadedFile=$_FILES["gallery_photo"]["tmp_name"][$key];
            $ext=pathinfo($file_name,PATHINFO_EXTENSION);
            $fileSize=$_FILES["gallery_photo"]["size"][$key];
            $KBSize = $fileSize/1024;
            $image_size = $KBSize/1024;
            $sourceProperties = getimagesize($uploadedFile);
            $newFileName = rand();
            $dirPath = "../../img/gallery/";
            $imageType = $sourceProperties[2];
            $imageHeight = $sourceProperties[1];
            $imageWidth = $sourceProperties[0];
            
            if ($imageWidth>1800) {
                $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1800 widht
                $newImageWidth = $imageWidth * $newWidthPercentage /100;
                $newImageHeight = $imageHeight * $newWidthPercentage /100;
            } else {
                $newImageWidth = $imageWidth;
                $newImageHeight = $imageHeight;
            }



            if(in_array($ext,$extension)) {
                switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagepng($tmp,$dirPath. $newFileName. "_gal.". $ext);
                    break;           

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagejpeg($tmp,$dirPath. $newFileName. "_gal.". $ext);
                    break;
                
                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile); 
                    $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                    imagegif($tmp,$dirPath. $newFileName. "_gal.". $ext);
                    break;

                default:
                    $_SESSION['msg1']="Invalid Image type.";
                    header("Location: ../gallery");
                    exit;
                    break;
                }
                $newFileName= $newFileName."_gal.".$ext;


                $notiUrl= $base_url.'img/gallery/'.$newFileName;

                $m->set_data('society_id', $society_id);
                $m->set_data('gallery_album_id', $gallery_album_id);
                $m->set_data('gallery_photo', $newFileName);
                $m->set_data('image_size', $image_size);

                $a1 = array(
                    'society_id' => $m->get_data('society_id'),
                    'gallery_album_id' => $m->get_data('gallery_album_id'),
                    'gallery_photo' => $m->get_data('gallery_photo'),
                    'image_size' => $m->get_data('image_size'),
                );
                $q = $d->insert("gallery_master", $a1);
                $_SESSION['temp_event_id']=$event_id;

                $m->set_data('gallery_album_update_at', date("Y-m-d H:i:s"));

                $aAlbum = array(
                    'gallery_album_update_at' => $m->get_data('gallery_album_update_at'),
                );
        
                $d->update("gallery_album_master", $aAlbum,"gallery_album_id='$gallery_album_id'");
            }
            else {
                array_push($error,"$file_name, ");
            }
        }
       //IS_1013  " .$_COOKIE['admin_name']
    }else{
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../gallery");
    }
    if($q==TRUE) {
        $block_id = explode (",", $block_id); 
        $block_id = implode ("','", $block_id); 
        $floor_id = explode (",", $floor_id); 
        $floor_id = implode ("','", $floor_id); 
          if ($block_id>0) {
              if($floor_id>0){
                $appendBlockQUery = " users_master.floor_id IN ('$floor_id')";
                $appendBlockQUeryAnd = " AND floor_id IN ('$floor_id')";
              }else{
                $appendBlockQUery = " users_master.block_id IN ('$block_id')";
                $appendBlockQUeryAnd = " AND block_id IN ('$block_id')";
              }    
          }
            $qa = $d->selectRow("album_title","gallery_album_master","gallery_album_id='$gallery_album_id'");
            $albumData=mysqli_fetch_array($qa);
            $album_title = $albumData['album_title'];
          $dataAry = array(
            "gallery_album_id"=>$gallery_album_id,
            "album_title"=>$album_title,
            );

          $dataJson = json_encode($dataAry);
          $title="$album_title Photo Added";
          $description= "In Gallery by Admin " .$_COOKIE['admin_name'];
          //$d->insertUserNotificationWithId($society_id,$title,$description,"gallery","Gallery_1xxxhdpi.png","$appendBlockQUery","$gallery_album_id");
          
          $d->insertUserNotificationWithJsonData($society_id,$title,$description,"gallery","Gallery_1xxxhdpi.png",$dataJson,"$appendBlockQUery");

          $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND  user_token!='' AND society_id='$society_id' AND device='android' $appendBlockQUeryAnd");
          $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' $appendBlockQUeryAnd");
          $nResident->noti("GalleryFragment","$notiUrl",$society_id,$fcmArray,$title,$description,$dataJson);
          $nResident->noti_ios("GalleryVC","$notiUrl",$society_id,$fcmArrayIos,$title,$description,$dataJson);

        $_SESSION['msg']="Photo Added";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Photo Added");
        if (isset($backUrl)) {
          header("Location: ../galleries?gallery_album_id=$gallery_album_id");
        }else {
          header("Location: ../galleries");
        }
    } else {
        $_SESSION['msg1']="Something Wrong";
      header("Location: ../gallery");
    }
  }
}

// delete gallery photo
if(isset($deleteGalleryPhoto)) {
    // print_r($_POST);
    // exit();
    $gallery_id=$_POST['gallery_id'];
     $q=$d->delete("gallery_master","gallery_id='$gallery_id'");
     unlink($gallery_path);
    if($q==TRUE   ) {
        $_SESSION['msg']="Photo Deleted";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Photo Deleted");
            //IS_570 ?event_id=".$_POST['event_id'] added
        header("Location: ../galleries?gallery_album_id=".$_POST['gallery_album_id']);
    } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../galleries");
    }
}

if(isset($deleteGalleryAlbum)) {
       if($gallery_album_id) {
          $eq = $d->select("gallery_master","gallery_album_id='$gallery_album_id' AND society_id = '$society_id'");
          while ($pData = mysqli_fetch_array($eq)) {
            $gallery_id= $pData['gallery_id'];
            $q=$d->delete("gallery_master","gallery_id='$gallery_id'");
            unlink("../../img/gallery/$pData[gallery_photo]");
         }
         $q=$d->delete("gallery_album_master","gallery_album_id='$gallery_album_id'");
        } 
              
     // $q=$d->delete("gallery_master","gallery_id='$gallery_id'");
     // unlink($gallery_path);

    if($q==TRUE   ) {
        $_SESSION['msg']="Photo Album Deleted";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Photo Album Deleted");
            //IS_570 ?event_id=".$_POST['event_id'] added
        header("Location: ../galleries");
    } else {
        $_SESSION['msg1']="Something Wrong";
        header("Location: ../galleries");
    }
}

 ?>