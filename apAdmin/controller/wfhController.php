<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    if(isset($_POST['action']) && $_POST['action'] == "wfhStatusApproved"){

        $q3 = $d->selectRow('user_id,wfh_start_date',"wfh_master","wfh_id='$wfh_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $user_id = $data3['user_id'];
        $wfh_date = date("d M Y", strtotime($data3['wfh_start_date']));
        $status = $wfh_status;

        $m->set_data('wfh_take_selfie', $wfh_take_selfie);
        $m->set_data('wfh_status', $wfh_status);
        $m->set_data('wfh_attendance_range', $wfh_attendance_range);
        $m->set_data('wfh_status_changed_by', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'wfh_take_selfie' => $m->get_data('wfh_take_selfie'),
            'wfh_status' => $m->get_data('wfh_status'),
            'wfh_attendance_range' => $m->get_data('wfh_attendance_range'),
            'wfh_status_changed_by' => $m->get_data('wfh_status_changed_by'),
            'wfh_status_changed_type' => 1,
        );

        if(isset($wfh_id) && $wfh_id>0){
            $q = $d->update('wfh_master', $a1, "wfh_id='$wfh_id'");
        }
        if ($q == true) {
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your Work From Home Request is Approved";
            $description = "By Admin $created_by for date $wfh_date";
            $menuClick = "wfh_approved";
            $image = "wfh.png";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotification($society_id,$title,$description,"wfh_approved","wfh.png","user_id = '$user_id'");

            $_SESSION['msg'] = "WFH Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "WFH Status Change Successfully");
            header("Location: ../wfhMaster?bId=$bId&dId=$dId&uId=&status=all");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../wfhMaster");
        }
    }

    if(isset($_POST['action']) && $_POST['action'] == "wfhEdit"){

        //print_r($_POST);die;

        $q3 = $d->selectRow('user_id,wfh_start_date',"wfh_master","wfh_id='$wfh_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $user_id = $data3['user_id'];
        $wfh_date = date("d M Y", strtotime($data3['wfh_start_date']));
        $status = $wfh_status;

        $m->set_data('wfh_take_selfie', $wfh_take_selfie);
        $m->set_data('wfh_attendance_range', $wfh_attendance_range);
        $m->set_data('wfh_status_changed_by', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'wfh_take_selfie' => $m->get_data('wfh_take_selfie'),
            'wfh_attendance_range' => $m->get_data('wfh_attendance_range'),
            'wfh_status_changed_by' => $m->get_data('wfh_status_changed_by'),
            'wfh_status_changed_type' => 1,
        );

        if(isset($wfh_id) && $wfh_id>0){
            $q = $d->update('wfh_master', $a1, "wfh_id='$wfh_id'");
        }
        if ($q == true) {
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your Work From Home Range is Updated";
            $description = "By Admin $created_by for date $wfh_date";
            $menuClick = "wfh_approved";
            $image = "wfh.png";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotification($society_id,$title,$description,"wfh_approved","wfh.png","user_id = '$user_id'");

            $_SESSION['msg'] = "WFH Range Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "WFH Range Updated Successfully");
            header("Location: ../wfhMaster?bId=$bId&dId=$dId&uId=$user_id&status=$status");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../wfhMaster");
        }
    }


    if(isset($_POST['wfhStatusReject'])){

        $q3 = $d->selectRow('user_id,wfh_start_date',"wfh_master","wfh_id='$wfh_id'");
        $data3 = mysqli_fetch_assoc($q3);
        $user_id = $data3['user_id'];
        $wfh_date = date("d M Y", strtotime($data3['wfh_start_date']));
        $status = $wfh_status;
        
        $m->set_data('wfh_status', $wfh_status);
        $m->set_data('wfh_declined_reason', $wfh_declined_reason);
        $m->set_data('wfh_status_changed_by', $_COOKIE['bms_admin_id']);

        $a1 = array(
            'wfh_status' => $m->get_data('wfh_status'),
            'wfh_declined_reason' => $m->get_data('wfh_declined_reason'),
            'wfh_status_changed_by' => $m->get_data('wfh_status_changed_by'),
            'wfh_status_changed_type' => 1,
        );

        if(isset($wfh_id) && $wfh_id>0){
            $q = $d->update('wfh_master', $a1, "wfh_id='$wfh_id'");
        }
        if ($q == true) {
            $q2 = $d->selectRow('user_token,device',"users_master","user_id='$user_id'");
            $data2 = mysqli_fetch_assoc($q2);
            $title = "Your Work From Home Request is Declined";
            $description = "By Admin $created_by for date $wfh_date";
            $menuClick = "wfh_rejected";
            $image = "wfh.png";
            $activity = "0";
            $user_token = $data2['user_token'];
            $device = $data2['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotification($society_id,$title,$description,"wfh_rejected","wfh.png","user_id = '$user_id'");

            $_SESSION['msg'] = "WFH Status Change Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "WFH Status Change Successfully");
            header("Location: ../wfhMaster?bId=$bId&dId=$dId&uId=&status=all");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../wfhMaster");
        }
    }

    
}
