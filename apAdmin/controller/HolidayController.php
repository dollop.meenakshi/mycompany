<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    //IS_605
     
    if (isset($_POST['addHoliday']))
    {
        $m->set_data('society_id', $society_id);
        $m->set_data('floor_id', $floor_id);
        $m->set_data('holiday_name', $holiday_name);
        $m->set_data('holiday_start_date', date("Y-m-d", strtotime($holiday_start_date)));
        $m->set_data('holiday_end_date', $holiday_end_date);
        $m->set_data('holiday_description', $holiday_description);
        $m->set_data('holiday_created_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id') ,
            'floor_id' => $m->get_data('floor_id') ,
            'holiday_name' => $m->get_data('holiday_name') ,
            'holiday_days' => $m->get_data('holiday_days') ,
            'holiday_start_date' => $m->get_data('holiday_start_date') ,
            'holiday_end_date' => $m->get_data('holiday_end_date') ,
            'holiday_description' => $m->get_data('holiday_description') ,
            'holiday_created_date' => $m->get_data('holiday_created_date') ,
        );
        $holiday_end_date = new DateTime($holiday_end_date);
        $holiday_start_date = new DateTime($holiday_start_date);
        $diff = date_diff($holiday_start_date, $holiday_end_date);
        $holiday_days = $diff->format("%a");
        // $holiday_days = $holiday_days+1;
        $a1['holiday_days'] = $holiday_days;

        $file = $_FILES['holiday_image']['tmp_name'];
        if (file_exists($file))
        {
            $extensionResume = array("pdf","PDF",'jpeg','JPEG',"gif","GIF","jpg","JPG","png","PNG");
            $extId = pathinfo($_FILES['holiday_image']['name'], PATHINFO_EXTENSION);

            $errors = array();
            $maxsize = 26214400;

            if (($_FILES['holiday_image']['size'] >= $maxsize) || ($_FILES["holiday_image"]["size"] == 0))
            {
                $_SESSION['msg1'] = "Video Lesson too large. Must be less than 25 MB.";
                header("location:../holiday");
                exit();
            }
            if (!in_array($extId, $extensionResume) && (!empty($_FILES["holiday_image"]["type"])))
            {
                $_SESSION['msg1'] = "Invalid File format, Only Image, Pdf and Doc is allowed.";
                header("location:../holiday");
                exit();
            }
            if (count($errors) === 0)
            {
                $image_Arr = $_FILES['holiday_image'];
                $temp = explode(".", $_FILES["holiday_image"]["name"]);
                $holiday_image = 'holiday_image_' . round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["holiday_image"]["tmp_name"], "../../img/holiday/" . $holiday_image);
            }
        }
        else
        {
            $holiday_image = "";
        }

        if (isset($holiday_image) && $holiday_image != "")
        {
            $m->set_data('holiday_image', $holiday_image);

        }
        else
        {
            $m->set_data('holiday_image', $holiday_image_old);
        }
        $a1['holiday_image'] = $m->get_data('holiday_image');
        
        if (isset($holiday_id) && $holiday_id > 0)
        {
            $a1['floor_id'] = $_POST['floor_id'][0];
            $q = $d->update("holiday_master", $a1, "holiday_id ='$holiday_id'");

            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Updated Successfully");
        }
        else
        {
            $startDate = $a1['holiday_start_date'];
            if (isset($_POST['floor_id']) && count($_POST['floor_id']) > 0)
            {
                if ($holiday_days >= 1)
                {
                    $holiday_dayss = $holiday_days + 1;
                    $a1['holiday_days'] = $holiday_dayss;
                    for ($i = 0;$i < $holiday_days;$i++)
                    {
                        for ($k = 0;$k < count($_POST['floor_id']);$k++)
                        {
                            $a1['floor_id'] = $_POST['floor_id'][$k];
                            $date = strtotime("+$i day", strtotime($startDate));
                            $a1['holiday_start_date'] = date("Y-m-d", $date);
                            $q = $d->insert("holiday_master", $a1);
                        }
                    }
                }
                else
                {
                    for ($k = 0;$k < count($_POST['floor_id']);$k++)
                    {
                        $a1['floor_id'] = $_POST['floor_id'][$k];
                        $holiday_dayss = $holiday_days + 1;
                        $a1['holiday_days'] = $holiday_dayss;
                        $q = $d->insert("holiday_master", $a1);
                    }
                }
            }
            else
            {
                if ($holiday_days >= 1)
                {
                    $holiday_days = $holiday_days + 1;
                    $a1['holiday_days'] = $holiday_days;
                    for ($i = 0;$i < $holiday_days;$i++)
                    {
                        $date = strtotime("+$i day", strtotime($startDate));
                        $a1['holiday_start_date'] = date("Y-m-d", $date);
                        $q = $d->insert("holiday_master", $a1);
                    }
                }
                else
                {
                    $holiday_days = $holiday_days + 1;
                    $q = $d->insert("holiday_master", $a1);
                }
            }
            $_SESSION['msg'] = "Holiday Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Holiday Added Successfully");
        }
        if ($q == true)
        {
            $q = $d->Select("working_days_master", "work_days_deleted=0");
            while ($data = mysqli_fetch_assoc($q))
            {
            }
            header("Location: ../holidays");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../holiday");
        }
    }
    elseif(isset($_POST['getHolidayGroupName']))
    {
        if(isset($holiday_group_id))
        {
            $q = $d->selectRow("holiday_group_id","holiday_group_master","holiday_group_name = '$holiday_group_name_edit' AND holiday_group_id != '$holiday_group_id'");
        }
        else
        {
            $q = $d->selectRow("holiday_group_id","holiday_group_master","holiday_group_name = '$holiday_group_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
    elseif(isset($_POST['addHolidayGroup']))
    {
        if($holiday_group_name == "")
        {
            $_SESSION['msg1'] = "Please fill all the required fields";
            header("Location: ../manageHolidayGroup");exit;
        }
        $q1 = $d->selectRow("holiday_group_id","holiday_group_master","holiday_group_name = '$holiday_group_name'");
        if(mysqli_num_rows($q1) > 0)
        {
            $_SESSION['msg1'] = "Holiday group name already added";
            header("Location: ../manageHolidayGroup");exit;
        }
        $m->set_data('holiday_group_name', $holiday_group_name);
        $a = array(
            'holiday_group_name' => $m->get_data('holiday_group_name'),
            'holiday_group_active_status' => 0,
            'holiday_group_created_date' => date("Y-m-d H:i:s"),
            'society_id' => $society_id
        );
        $q = $d->insert("holiday_group_master",$a);
        if($q)
        {
            $_SESSION['msg'] = "Holiday group added successfully";
            header("Location: ../manageHolidayGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went Wrong";
            header("Location: ../manageHolidayGroup");exit;
        }
    }
    elseif(isset($_POST['getHolidayGroupDetails']))
    {
        $q = $d->select("holiday_group_master","holiday_group_id = '$holiday_group_id'");
        $data = $q->fetch_assoc();
        echo json_encode($data);exit;
    }
    elseif(isset($_POST['updateHolidayGroup']))
    {
        if($holiday_group_name_edit == "")
        {
            $_SESSION['msg1'] = "Please fill all the required fields";
            header("Location: ../manageHolidayGroup");exit;
        }
        $q1 = $d->selectRow("holiday_group_id","holiday_group_master","holiday_group_name = '$holiday_group_name_edit' AND holiday_group_id != '$holiday_group_id'");
        if(mysqli_num_rows($q1) > 0)
        {
            $_SESSION['msg1'] = "Holiday group name already added";
            header("Location: ../manageHolidayGroup");exit;
        }
        $m->set_data('holiday_group_name', $holiday_group_name_edit);
        $a = array(
            'holiday_group_name' => $m->get_data('holiday_group_name')
        );
        $q = $d->update("holiday_group_master",$a,"holiday_group_id = '$holiday_group_id'");
        if($q)
        {
            $_SESSION['msg'] = "Holiday group updated successfully";
            header("Location: ../manageHolidayGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went Wrong";
            header("Location: ../manageHolidayGroup");exit;
        }
    }
    elseif(isset($_POST['deleteHolidayGroup']))
    {
        $q = $d->delete("holiday_group_master","holiday_group_id = '$holiday_group_id'");
        if($q)
        {
            $_SESSION['msg'] = "Holiday group deleted successfully";
            header("Location: ../manageHolidayGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went Wrong";
            header("Location: ../manageHolidayGroup");exit;
        }
    }
    elseif(isset($_POST['updateHolidayGroupIdInMaster']))
    {
        $m->set_data('holiday_group_id', $holiday_group_id);
        $a = array(
            'holiday_group_id' => $m->get_data('holiday_group_id')
        );
        $q = $d->update("holiday_master",$a,"holiday_id = '$holiday_id'");
        if($q)
        {
            $_SESSION['msg'] = "Group assigned successfully";
        }
        else
        {
            $_SESSION['msg1'] = "Something went Wrong!";
        }
    }
    elseif(isset($_POST['getEmpLevel']))
    {
        $all = [];
        if(is_array($level_id))
        {
            $level_id_str = implode(",",$level_id);
        }
        else
        {
            echo json_encode($all);exit;
        }
        if(is_array($floor_id))
        {
            $floor_id_str = implode(",",$floor_id);
        }
        else
        {
            echo json_encode($all);exit;
        }
        $q1 = $d->selectRow("common_assign_id","holiday_group_assign_master","common_assign_type = 3 AND holiday_group_id = '$holiday_group_id'");
        if(mysqli_num_rows($q1) > 0)
        {
            $da = $q1->fetch_assoc();
            $all_ids = $da['common_assign_id'];
            $q = $d->selectRow("user_id,user_full_name","users_master","level_id IN ($level_id_str) AND floor_id IN ($floor_id_str) AND user_id NOT IN ($all_ids)");
        }
        else
        {
            $q = $d->selectRow("user_id,user_full_name","users_master","level_id IN ($level_id_str) AND floor_id IN ($floor_id_str)");
        }
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['getFloorByBlockId']))
    {
        if(is_array($block_ids))
        {
            $block_id_str = implode(",",$block_ids);
        }
        else
        {
            $block_ids = [0];
            $block_id_str = implode(",",$block_ids);
        }
        $q1 = $d->selectRow("common_assign_id","holiday_group_assign_master","common_assign_type = 1 AND holiday_group_id = '$holiday_group_id'");
        if(mysqli_num_rows($q1) > 0)
        {
            $da = $q1->fetch_assoc();
            $all_ids = $da['common_assign_id'];
            $q = $d->selectRow("floor_id,floor_name,block_name","floors_master,block_master", "block_master.block_id = floors_master.block_id AND floors_master.block_id IN ($block_id_str) AND floors_master.floor_id NOT IN ($all_ids)");
        }
        else
        {
            $q = $d->selectRow("floor_id,floor_name,block_name","floors_master,block_master", "block_master.block_id = floors_master.block_id AND floors_master.block_id IN ($block_id_str)");
        }
        $all = [];
        while ($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['getLevelByDepartmentId']))
    {
        if(is_array($floor_ids))
        {
            $floor_id_str = implode(",",$floor_ids);
        }
        else
        {
            $floor_ids = [0];
            $floor_id_str = implode(",",$floor_ids);
        }
        $q1 = $d->selectRow("common_assign_id","holiday_group_assign_master","common_assign_type = 2 AND holiday_group_id = '$holiday_group_id'");
        if(mysqli_num_rows($q1) > 0)
        {
            $da = $q1->fetch_assoc();
            $all_ids = $da['common_assign_id'];
            $q = $d->selectRow("elm.level_id,elm.level_name","employee_level_master AS elm LEFT JOIN users_master AS um ON um.level_id = elm.level_id","um.floor_id IN ($floor_id_str) AND elm.level_id NOT IN ($all_ids)","GROUP BY elm.level_id");
        }
        else
        {
            $q = $d->selectRow("elm.level_id,elm.level_name","employee_level_master AS elm LEFT JOIN users_master AS um ON um.level_id = elm.level_id","um.floor_id IN ($floor_id_str)","GROUP BY elm.level_id");
        }
        $all = [];
        while ($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['addAssignGroup']))
    {
        if($holiday_group_id == "" || empty($block_id_assign))
        {
            $_SESSION['msg1'] = "Please fill all the required fields";
            header("Location: ../addAssignGroup");exit;
        }
        if($block_id_assign[0] == 0)
        {
            $getOld = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 0");
            if(mysqli_num_rows($getOld) > 0)
            {
                $old = $getOld->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $qd = $d->selectRow("block_id","block_master AS bm","bm.society_id = '$society_id' AND bm.block_status = 0 AND bm.block_id NOT IN ($old_ids)");
                if(mysqli_num_rows($qd) > 0)
                {
                    while ($depaData = $qd->fetch_assoc())
                    {
                        $all_id[] = $depaData['block_id'];
                    }
                    $new_id_str = implode(",",$all_id);
                    $holiday_group_assign_id = $old['holiday_group_assign_id'];
                    $merge = $old_ids.",".$new_id_str;
                    $a = array(
                        'common_assign_id' => $merge
                    );
                    $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
                }
                else
                {
                    $_SESSION['msg1'] = "All branches already added in this holiday group";
                    header("Location: ../addAssignGroup");exit;
                }
            }
            else
            {
                $qd = $d->selectRow("block_id","block_master AS bm","bm.society_id = '$society_id' AND bm.block_status = 0 AND bm.block_id NOT IN (SELECT common_assign_id FROM holiday_group_assign_master WHERE common_assign_type = 0 AND holiday_group_id = '$holiday_group_id')");
                if(mysqli_num_rows($qd) > 0)
                {
                    while ($depaData = $qd->fetch_assoc())
                    {
                        $all_id[] = $depaData['block_id'];
                    }
                    $all_id_str = implode(",",$all_id);
                    $a = array(
                        'society_id' => $society_id,
                        'common_assign_id' => $all_id_str,
                        'holiday_group_id' => $holiday_group_id,
                        'common_assign_type' => 0,
                        'holiday_assign_date' => date("Y-m-d H:i:s")
                    );
                    $q = $d->insert("holiday_group_assign_master",$a);
                }
                else
                {
                    $_SESSION['msg1'] = "All branches already added in this holiday group";
                    header("Location: ../addAssignGroup");exit;
                }
            }
        }
        elseif($block_id_assign[0] != 0 && !isset($floor_id))
        {
            $getOld = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 0");
            if(mysqli_num_rows($getOld) > 0)
            {
                $old = $getOld->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $new_id_str = implode(",",$block_id_assign);
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $new_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$new_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$block_id_assign);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 0,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($floor_id) && $floor_id[0] == 0 && !isset($level_id))
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 0");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$block_id_assign);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$block_id_assign);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 0,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($floor_id) && $floor_id[0] != 0 && !isset($level_id))
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 1");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$floor_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$floor_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 1,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($level_id) && $level_id[0] == 0)
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 1");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$floor_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$floor_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 1,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($level_id) && $level_id[0] != 0 && !isset($user_id))
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 2");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$level_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$level_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 2,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($user_id) && $user_id[0] == 0)
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 2");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$level_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$level_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 2,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($user_id) && $user_id[0] != 0)
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 3");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$user_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                if(isset($editAssignGroup))
                {
                    $merge = $all_id_str;
                }
                else
                {
                    $merge = $old_ids.",".$all_id_str;
                }
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$user_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 3,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }

        if($q)
        {
            $_SESSION['msg'] = "Group assigned successfully";
            header("Location: ../assignGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went wrong";
            header("Location: ../addAssignGroup");exit;
        }
    }
    elseif(isset($_POST['getBranchByGroup']))
    {
        $q1 = $d->selectRow("common_assign_id","holiday_group_assign_master","common_assign_type = 0 AND holiday_group_id = '$holiday_group_id'");
        if(mysqli_num_rows($q1) > 0)
        {
            $da = $q1->fetch_assoc();
            $all_ids = $da['common_assign_id'];
            $q = $d->selectRow("block_id,block_name","block_master AS bm","bm.block_id NOT IN ($all_ids)");
        }
        else
        {
            $q = $d->selectRow("block_id,block_name","block_master AS bm","");
        }
        $all = [];
        while($data = $q->fetch_assoc())
        {
            $all[] = $data;
        }
        echo json_encode($all);exit;
    }
    elseif(isset($_POST['deleteGroupAssign']))
    {
        $q = $d->delete("holiday_group_assign_master","holiday_group_assign_id = '$holiday_group_assign_id'");
        if($q)
        {
            $_SESSION['msg'] = "Group assign deleted successfully";
            header("Location: ../assignGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went Wrong";
            header("Location: ../assignGroup");exit;
        }
    }
    elseif(isset($_POST['updateAssignGroup']))
    {
        if($holiday_group_id == "" || empty($block_id_assign))
        {
            $_SESSION['msg1'] = "Please fill all the required fields";
            header("Location: ../assignGroup");exit;
        }
        if($block_id_assign[0] == 0)
        {
            $qd = $d->selectRow("block_id","block_master AS bm","bm.society_id = '$society_id' AND bm.block_status = 0 AND bm.block_id");
            if(mysqli_num_rows($qd) > 0)
            {
                while ($depaData = $qd->fetch_assoc())
                {
                    $all_id[] = $depaData['block_id'];
                }
                $new_id_str = implode(",",$all_id);
                $a = array(
                    'common_assign_id' => $new_id_str,
                    'common_assign_type' => 0
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $_SESSION['msg1'] = "All branches already added in this holiday group";
                header("Location: ../addAssignGroup");exit;
            }
        }
        elseif($block_id_assign[0] != 0 && !isset($floor_id))
        {
            $new_id_str = implode(",",$block_id_assign);
            $a = array(
                'common_assign_id' => $new_id_str,
                'common_assign_type' => 0
            );
            $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
        }
        elseif(isset($floor_id) && $floor_id[0] == 0 && !isset($level_id))
        {
            $all_id_str = implode(",",$block_id_assign);
            $a = array(
                'common_assign_id' => $all_id_str,
                'common_assign_type' => 0
            );
            $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
        }
        elseif(isset($floor_id) && $floor_id[0] != 0 && !isset($level_id))
        {
            $all_id_str = implode(",",$floor_id);
            $a = array(
                'common_assign_id' => $all_id_str,
                'common_assign_type' => 1
            );
            $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
        }
        elseif(isset($level_id) && $level_id[0] == 0)
        {
            $all_id_str = implode(",",$floor_id);
            $a = array(
                'common_assign_id' => $all_id_str,
                'common_assign_type' => 1
            );
            $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
        }
        elseif(isset($level_id) && $level_id[0] != 0 && !isset($user_id))
        {
            $all_id_str = implode(",",$level_id);
            $a = array(
                'common_assign_id' => $all_id_str,
                'common_assign_type' => 2
            );
            $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
        }
        elseif(isset($user_id) && $user_id[0] == 0)
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 2");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$level_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                $merge = $old_ids.",".$all_id_str;
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$level_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 2,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }
        elseif(isset($user_id) && $user_id[0] != 0)
        {
            $qd = $d->selectRow("holiday_group_assign_id,common_assign_id","holiday_group_assign_master","society_id = '$society_id' AND holiday_group_id = '$holiday_group_id' AND common_assign_type = 3");
            if(mysqli_num_rows($qd) > 0)
            {
                $all_id_str = implode(",",$user_id);
                $old = $qd->fetch_assoc();
                $old_ids = $old['common_assign_id'];
                $holiday_group_assign_id = $old['holiday_group_assign_id'];
                $merge = $old_ids.",".$all_id_str;
                $a = array(
                    'common_assign_id' => $merge
                );
                $q = $d->update("holiday_group_assign_master",$a,"holiday_group_assign_id = '$holiday_group_assign_id'");
            }
            else
            {
                $all_id_str = implode(",",$user_id);
                $a = array(
                    'society_id' => $society_id,
                    'common_assign_id' => $all_id_str,
                    'holiday_group_id' => $holiday_group_id,
                    'common_assign_type' => 3,
                    'holiday_assign_date' => date("Y-m-d H:i:s")
                );
                $q = $d->insert("holiday_group_assign_master",$a);
            }
        }

        if($q)
        {
            $_SESSION['msg'] = "Group assigned updated successfully";
            header("Location: ../assignGroup");exit;
        }
        else
        {
            $_SESSION['msg1'] = "Something went wrong";
            header("Location: ../addAssignGroup");exit;
        }
    }
}
?>