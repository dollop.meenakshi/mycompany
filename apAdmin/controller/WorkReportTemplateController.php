<?php
include '../common/objectController.php';
if (isset($_POST) && !empty($_POST))
{
    extract($_POST);
    //IS_605
    if (isset($_POST['addTemplate']))
    {
        $m->set_data('society_id', $society_id);
        $m->set_data('template_name', test_input($template_name));
        $m->set_data('template_description', test_input($template_description));
        $m->set_data('allow_muliple_time', test_input($allow_muliple_time));
        $m->set_data('template_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('template_created_date', date("Y-m-d H:i:s"));
        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'template_name' => $m->get_data('template_name'),
            'template_description' => $m->get_data('template_description'),
            'allow_muliple_time' => $m->get_data('allow_muliple_time'),
            'template_created_by' => $m->get_data('template_created_by'),
            'template_created_date' => $m->get_data('template_created_date')
        );
        if (isset($template_id) && $template_id > 0)
        {
            $check = $d->selectRow("template_id","template_master","template_name = '$template_name' AND template_id != '$template_id'");
            if(mysqli_num_rows($check) > 0)
            {
                $_SESSION['msg1'] = "Template name already used!";
                header("Location: ../workReportTemplate");exit;
            }
            $q = $d->update("template_master", $a1, "template_id  ='$template_id'");
            $_SESSION['msg'] = "Template Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Template Updated Successfully");
        }
        else
        {
            $check = $d->selectRow("template_id","template_master","template_name = '$template_name'");
            if(mysqli_num_rows($check) > 0)
            {
                $_SESSION['msg1'] = "Template name already used!";
                header("Location: ../workReportTemplate");exit;
            }
            $q = $d->insert("template_master", $a1);
            $_SESSION['msg'] = "Template Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Template Added Successfully");
        }
        if ($q == true)
        {
            header("Location: ../workReportTemplate");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../workReportTemplate");
        }
    }

    if (isset($_POST['addTemplateQuestion']))
    {
        $questionTypeValue = array();
        if (isset($_POST['question_type_value']))
        {
            $question_value_count = count($_POST['question_type_value']);
            for ($i = 0;$i < $question_value_count;$i++)
            {
                if ($_POST['question_type_value'][$i] != '')
                {
                    $questionTypeValue['option_' . $i] = $_POST['question_type_value'][$i];
                }
            }
        }
        $question_type_value = !empty($questionTypeValue) ? json_encode($questionTypeValue) : '';
        $m->set_data('society_id', $society_id);
        $m->set_data('template_id', $template_id);
        $m->set_data('template_question', test_input($template_question));
        $m->set_data('question_type', $question_type);
        $m->set_data('is_required', $is_required);
        $m->set_data('question_type_value', $question_type_value);
        $m->set_data('question_value_count', $question_value_count);
        $m->set_data('template_multiple_file', $template_multiple_file);
        $m->set_data('template_question_created_by', $_COOKIE['bms_admin_id']);
        $m->set_data('template_question_created_date', date("Y-m-d H:i:s"));

        $a1 = array(
            'society_id' => $m->get_data('society_id'),
            'template_id' => $m->get_data('template_id'),
            'template_question' => $m->get_data('template_question'),
            'question_type' => $m->get_data('question_type'),
            'is_required' => $m->get_data('is_required'),
            'question_type_value' => $m->get_data('question_type_value'),
            'question_value_count' => $m->get_data('question_value_count'),
            'template_question_created_by' => $m->get_data('template_question_created_by'),
            'template_multiple_file' => $m->get_data('template_multiple_file'),
            'template_question_created_date' => $m->get_data('template_question_created_date')
        );

        if (isset($template_question_id) && $template_question_id > 0)
        {
            $check = $d->selectRow("template_question_id","template_question_master","template_question = '$template_question' AND template_id = '$template_id' AND template_question_id != '$template_question_id'");
            if(mysqli_num_rows($check) > 0)
            {
                $_SESSION['msg1'] = "Question already added in this template";
                if (isset($_POST['addMoreTemplateQuestion']) && $_POST['addMoreTemplateQuestion'] == 'addMoreTemplateQuestion')
                {
                    header("Location: ../addTemplateQuestion?id=$template_id");
                }
                else
                {
                    header("Location: ../templateQuestions?id=$template_id&type=edit");
                }exit;
            }
            $q = $d->update("template_question_master", $a1, "template_question_id='$template_question_id'");
            $_SESSION['msg'] = "Template Question Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Template Question Updated Successfully");
        }
        else
        {
            $check = $d->selectRow("template_question_id","template_question_master","template_question = '$template_question' AND template_id = '$template_id'");
            if(mysqli_num_rows($check) > 0)
            {
                $_SESSION['msg1'] = "Question already added in this template";
                if (isset($_POST['addMoreTemplateQuestion']) && $_POST['addMoreTemplateQuestion'] == 'addMoreTemplateQuestion')
                {
                    header("Location: ../addTemplateQuestion?id=$template_id");
                }
                else
                {
                    header("Location: ../templateQuestions?id=$template_id&type=edit");
                }exit;
            }
            $q = $d->insert("template_question_master", $a1);
            $_SESSION['msg'] = "Template Question Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Template Question Added Successfully");
        }
        if ($q)
        {
            if (isset($_POST['addMoreTemplateQuestion']) && $_POST['addMoreTemplateQuestion'] == 'addMoreTemplateQuestion')
            {
                header("Location: ../addTemplateQuestion?id=$template_id");
            }
            else
            {
                header("Location: ../templateQuestions?id=$template_id&type=edit");
            }
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../templateQuestions?id=$template_id&type=edit");
        }
    }

    if (isset($_POST['assignWorkReportTemplate']))
    {
        if ($template_assign_for == 0)
        {
            $template_assign_ids = 0;
        }
        else if ($template_assign_for == 1 && $_POST['template_assign_ids'][0] == 0)
        {
            $template_assign_for = 0;
            $template_assign_ids = 0;
        }
        else if ($template_assign_for == 2 && $_POST['template_assign_ids'][0] == 0)
        {
            $template_assign_for = 1;
            $template_assign_ids = implode(',', $_POST['blockId']);
        }
        else if ($template_assign_for == 3 && $_POST['template_assign_ids'][0] == 0)
        {
            $template_assign_for = 2;
            $template_assign_ids = implode(',', $_POST['floorId']);
        }
        else
        {
            $template_assign_ids = implode(',', $_POST['template_assign_ids']);
        }

        $m->set_data('template_id', $template_id);
        $m->set_data('society_id', $society_id);
        $m->set_data('template_assign_for', $template_assign_for);
        $m->set_data('template_assign_ids', $template_assign_ids);
        $m->set_data('template_assign_by', $_COOKIE['bms_admin_id']);
        $m->set_data('template_assign_date', date("Y-m-d H:i:s"));

        $a = array(
            'template_id' => $m->get_data('template_id'),
            'society_id' => $m->get_data('society_id'),
            'template_assign_for' => $m->get_data('template_assign_for'),
            'template_assign_ids' => $m->get_data('template_assign_ids'),
            'template_assign_by' => $m->get_data('template_assign_by'),
            'template_assign_date' => $m->get_data('template_assign_date'),
        );

        $aq = $d->selectRow('template_assign_id', "template_assign_master", "template_id='$template_id'");
        $assignData = mysqli_fetch_array($aq);
        if ($assignData)
        {
            $q = $d->update("template_assign_master", $a, "template_assign_id='$assignData[template_assign_id]'");
            $message = "Template Assign Data Updated Successfully";
        }
        else
        {
            $q = $d->insert("template_assign_master", $a);
            $message = "Template Assigned Successfully";
        }

        if ($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "$message");
            $_SESSION['msg'] = "$message";
            header("Location: ../manageAssignTemplate");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../manageAssignTemplate");
        }
    }

    if (isset($_POST['delete_template_question']))
    {
        $q = $d->delete("template_question_master", "template_question_id='$template_question_id'");
        if ($q > 0)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Template Question Deleted");
            $_SESSION['msg'] = "Template Question Deleted.";
            header("Location: ../templateQuestions?id=$template_id&type=edit");
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../templateQuestions?id=$template_id&type=edit");
        }
    }

    if (isset($_POST['update_user_work_report']) && $_POST['update_user_work_report'] == "update_user_work_report")
    {
        $update_arr = ['user_work_report_on' => $user_work_report_on];
        $q = $d->update("users_master", $update_arr, "user_id = '$user_id'");
        if ($q)
        {
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work report successfully updated");
            $_SESSION['msg'] = "Work report successfully updated.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }

    if (isset($_POST['updateUsersWorkReportMulti']) && $_POST['updateUsersWorkReportMulti'] == "updateUsersWorkReportMulti")
    {
        $update_arr = ['user_work_report_on' => $user_work_report_on];
        foreach ($user_id AS $key => $value)
        {
            $q = $d->update("users_master", $update_arr, "user_id = '$value'");
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Work report successfully updated");
        }
        if ($q)
        {
            $_SESSION['msg'] = "Work report successfully updated.";
        }
        else
        {
            $_SESSION['msg1'] = "Something Wrong";
        }
    }

    if(isset($_POST['checkTemplateName']))
    {
        if(isset($template_id) && $template_id > 0)
        {
            $q = $d->selectRow("template_id","template_master","template_name = '$template_name' AND template_id != '$template_id'");
        }
        else
        {
            $q = $d->selectRow("template_id","template_master","template_name = '$template_name'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }

    if(isset($_POST['checkTemplateQuestion']))
    {
        if(isset($template_question_id) && $template_question_id > 0)
        {
            $q = $d->selectRow("template_question_id","template_question_master","template_question = '$template_question' AND template_id = '$template_id' AND template_question_id != '$template_question_id'");
        }
        else
        {
            $q = $d->selectRow("template_question_id","template_question_master","template_question = '$template_question' AND template_id = '$template_id'");
        }
        if(mysqli_num_rows($q) > 0)
        {
            echo "false";exit;
        }
        else
        {
            echo "true";exit;
        }
    }
}
?>