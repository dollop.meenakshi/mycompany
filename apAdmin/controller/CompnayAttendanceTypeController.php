<?php
include '../common/objectController.php';
extract($_POST);


if (isset($_POST) && !empty($_POST)) //it can be $_GET doesn't matter
{

    //IS_605
    if (isset($_POST['UpdateCompanyAttendanceType'])) {

        $m->set_data('society_id', $society_id);
        $m->set_data('society_attendace_type', $society_attendace_type);
        $m->set_data('block_id', $block_id);
        $m->set_data('is_emp', $is_emp);
        $m->set_data('user_id', $user_id);
        $m->set_data('block_geofence_range', $block_geofence_range);
        $m->set_data('block_latitude', $block_latitude);
        $m->set_data('block_longitude', $block_longitude);
        $m->set_data('take_attendance_selfie', $take_attendance_selfie);
        $m->set_data('work_report_on', $work_report_on);
        $m->set_data('branch_geo_fencing', $branch_geo_fencing);
        $m->set_data('attendance_with_matching_face', $attendance_with_matching_face);

        if($work_report_on == 2 &&  $_POST['access_for_id']!=''){
            if ($_POST['access_for_id'][0]==0) {
               $work_report_on_ids = "";
               $m->set_data('work_report_on', 1);
            } else {
                $work_report_on_ids = implode(',', $_POST['access_for_id']);
            }
        }
        else if($work_report_on == 3 &&  $_POST['access_for_id']!=''){
            $work_report_on_ids = implode(',', $_POST['access_for_id']);
        }else{
            $work_report_on_ids ="";
        }
      
        if ($attendance_with_matching_face==1) {
            $hide_attendance_face_menu = 0;
        } else {
            $hide_attendance_face_menu =1;
        }
        $m->set_data('work_report_on_ids', $work_report_on_ids);
        $m->set_data('hide_attendance_face_menu', $hide_attendance_face_menu);
       
        $a1 = array(
            'attendance_type' => $m->get_data('society_attendace_type'),
            'branch_geo_fencing' => $m->get_data('branch_geo_fencing'),
            'take_attendance_selfie' => $m->get_data('take_attendance_selfie'),
            'work_report_on' => $m->get_data('work_report_on'),
            'work_report_on_ids' => $m->get_data('work_report_on_ids'),
            'hide_attendance_face_menu' => $m->get_data('hide_attendance_face_menu'),
            'attendance_with_matching_face' => $m->get_data('attendance_with_matching_face'),
        );
        
        if (isset($society_id) && $society_id > 0) {
            if($society_attendace_type !="")
            {
                $q = $d->update("society_master", $a1, "society_id ='$society_id'");
            }
            $_SESSION['msg'] = "Company Attendance Type Updated Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Attendance Type Updated Successfully");
           

        } 
        if ($q == true) {
            header("Location: ../companyAttendanceType");
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            header("Location: ../companyAttendanceType");
        }

    }

}
