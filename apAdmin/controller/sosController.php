<?php 
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

// print_r($_POST);
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
  // add main menu

  if(isset($_POST['event_name'])){

        $file = $_FILES['sos_image']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if(($_FILES['sos_image']['size'] >= $maxsize) || ($_FILES["sos_image"]["size"] == 0)) {
                $_SESSION['msg1']="photo too large. Must be less than 1 MB.";
                header("location:../sos");
                exit();
                
            }
            if(!in_array($_FILES['sos_image']['type'], $acceptable) && (!empty($_FILES["sos_image"]["type"]))) {
                 $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employee");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['sos_image'];   
            $temp = explode(".", $_FILES["sos_image"]["name"]);
            $sos_image = $event_name.'_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["sos_image"]["tmp_name"], "../../img/sos/".$sos_image);
          } 
        } else{
          $sos_image="sos_default.png";
        }


      $m->set_data('society_id',$society_id);
      $m->set_data('event_name',$event_name);
      $m->set_data('sos_duration',$sos_duration);
      $m->set_data('sos_for',$sos_for);
      $m->set_data('sos_image',$sos_image);

      $a =array(
        'society_id'=> $m->get_data('society_id'),
        'event_name'=> $m->get_data('event_name'),
        'sos_duration'=> $m->get_data('sos_duration'),
        'sos_for'=> $m->get_data('sos_for'),
        'sos_image'=> $m->get_data('sos_image'),
      );
    
      $q=$d->insert("sos_events_master",$a);
    
      if($q>0) {

        $_SESSION['msg']="SOS Added";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","SOS Added");
        header("location:../manageSOS");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../sos");
      }
  }


  if(isset($_POST['event_name_edit'])){

      $file = $_FILES['sos_image']['tmp_name'];
        if(file_exists($file)) {
           $errors     = array();
            $maxsize    = 1097152;
            $acceptable = array(
                // 'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            if(($_FILES['sos_image']['size'] >= $maxsize) || ($_FILES["sos_image"]["size"] == 0)) {
                $_SESSION['msg1']="photo too large. Must be less than 1 MB.";
                header("location:../sos");
                exit();
                
            }
            if(!in_array($_FILES['sos_image']['type'], $acceptable) && (!empty($_FILES["sos_image"]["type"]))) {
                 $_SESSION['msg1']="Invalid  photo. Only  JPG and PNG are allowed.";
                header("location:../employee");
                exit();

            }
           if(count($errors) === 0) {
            $image_Arr = $_FILES['sos_image'];   
            $temp = explode(".", $_FILES["sos_image"]["name"]);
            $sos_image = $event_name_edit.'_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["sos_image"]["tmp_name"], "../../img/sos/".$sos_image);
          } 
        } else{
          $sos_image=$sos_image_old;
        }

      $m->set_data('event_name',$event_name_edit);
      $m->set_data('sos_duration',$sos_duration);
      $m->set_data('sos_for',$sos_for);
      $m->set_data('sos_image',$sos_image);

      $a =array(
        'event_name'=> $m->get_data('event_name'),
        'sos_duration'=> $m->get_data('sos_duration'),
        'sos_for'=> $m->get_data('sos_for'),
        'sos_image'=> $m->get_data('sos_image'),
      );
    
      $q=$d->update("sos_events_master",$a,"sos_event_id='$sos_event_id'");
    
      if($q>0) {

        $_SESSION['msg']="SOS Updated";
        $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","SOS Updated");
        header("location:../manageSOS");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../sos");
      }
  }



  if (isset($deleteParking)) {

       $q=$d->delete("parking_master","society_parking_id='$society_parking_id' AND society_id='$society_id'");
       $q=$d->delete("society_parking_master","society_parking_id='$society_parking_id' AND society_id='$society_id'");
    
      if($q>0) {
         $_SESSION['msg']="Parking Deleted";
        header("location:../parkings");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../parkings");
      }
  }


   if (isset($sosDelete)) {

       $q=$d->delete("sos_events_master","society_id='$society_id' AND sos_event_id='$sos_event_id'");
    
      if($q>0) {
         $_SESSION['msg']="Sos Event Deleted";
        header("location:../manageSOS");
      } else {
        $_SESSION['msg1']="Something Wrong";
        header("location:../manageSOS");
      }
  }

  // add common type
  if (isset($addBulk)) {

    if (count($_POST['sos_event_id'])==0) {
      $_SESSION['msg1']="Please choose at least 1 option";
      header("Location: ../manageSOS");
      exit();
    }

    for ($i=0; $i <count($_POST['sos_event_id']); $i++) { 
        $sos_event_id = $_POST['sos_event_id'][$i];
        $qqq=$d->select("sos_events_common","sos_event_id='$sos_event_id'");
        $empData=mysqli_fetch_array($qqq);  


       $m->set_data('society_id',$_COOKIE['society_id']);
       $m->set_data('event_name',$empData['event_name']);
       $m->set_data('sos_image',$empData['sos_image']);
       $m->set_data('sos_for',$empData['sos_for']);

           $a1= array (
              
              'society_id'=> $m->get_data('society_id'),
              'event_name'=> $m->get_data('event_name'),
              'sos_image'=> $m->get_data('sos_image'),
              'sos_for'=> $m->get_data('sos_for'),
          );

        $q11=$d->select("sos_events_master","society_id='$society_id' AND event_name='$empData[event_name]'");
        if (mysqli_num_rows($q11)==0) {
           $q11=$d->insert("sos_events_master",$a1);
        } 
    }

    if($q11==TRUE) {
      $_SESSION['msg']="SOS Type Added";
      $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","SOS Type Added");
      header("Location: ../manageSOS");
    } else {
      $_SESSION['msg1']="Please choose at least 1 option";
      header("Location: ../manageSOS");
    }
  }

  
}
else{
  header('location:../login');
}
 ?>
