<?php 
extract($_REQUEST);


if(isset($election_id)){
 
$q = $d->select("election_master,election_users","election_master.election_id=election_users.election_id AND election_master.society_id = '$society_id' AND election_master.election_id = '$election_id'");
$data = mysqli_fetch_array($q);
extract($data);
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Election Result</h4>
        <?php  $q1=$d->select("master_menu","parent_menu_id='47' AND menu_link='$_GET[f]'","");
            $dataSub=mysqli_fetch_array($q1);
             $actStatus=$dataSub['menu_link'];
              if($_GET['f'] ==$actStatus) {
                echo 'active';
              }
              ?>
      </div>

    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
       <div class="card profile-card-2">
        <?php $count5=$d->sum_data("given_vote","election_users","election_id='$election_id' ");


        while($row=mysqli_fetch_array($count5))
        {
          $totalVote=$row['SUM(given_vote)'];

        }?>
        <div class="card-body">
          <h5 class="card-title">Election: <?php echo $election_name; ?></h5>
          <p class="card-text"><b>Description:</b> <?php echo $election_description; ?></p>
          <p class="card-text"><b>Date:</b> <?php echo $election_date; ?></p>
<?php 
 // /IS_243
          $ele_result_array = array();
          $election_users_qry=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
          $isTieinElection = "No";
          $maxVotes = 0;
          while($election_users_data = mysqli_fetch_array($election_users_qry)){
            if($maxVotes==0){
              $maxVotes = $election_users_data['given_vote'];
            } else if($maxVotes == $election_users_data['given_vote'] ){
             $isTieinElection = "Yes";
           } 


           $ele_result_array[$election_users_data['user_id']][] = $election_users_data['given_vote'];


         }
           ?>



           <?php //IS_664
         
           if( ($totalVote == 0 || $isTieinElection=="Yes" )   ) { ?>
            <div class="row">
             <div class="col-md-8"> </div>
             <div class="col-md-4 text-center"  >
               <form action="controller/pullingController.php" method="post" id="personal-info1">
                <input type="hidden" name="election_id" value="<?php echo $data['election_id']; ?>">
                <input type="hidden" name="election_for" value="<?php echo $data['election_for']; ?>">
                <input type="hidden" name="election_status" value="2" >
                <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                <input type="hidden" name="resultPublish" value="resultPublish" >
                <?php //IS_850    ?> 
                <?php if($data['election_status']=="2"){
                  ?>  <span  name="" class="btn  btn-info btn-sm" title="Result Published"   >Result Published</span>  <?php
                } else {?> 
                <input type="submit" name="resultPublish" class="btn form-btn btn-danger btn-sm" value="Publish Result ?" >
              <?php } ?>
              </form>
            </div>
          </div>
        <?php } //IS_664?>


      </div>

      <div class="card-body border-top">

      
          <script src="assets/js/jquery.min.js"></script>
          <script src="assets/plugins/Chart.js/Chart.min.js"></script>
          <script type="text/javascript">
            $(function() {
              "use strict";
              var ctx = document.getElementById("incomeStatus").getContext('2d');
              var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: [<?php
                $sq=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
                while($sData = mysqli_fetch_array($sq)){
              // echo "['".$sData['option_name']."', ".$sData['option_count']."],";
                  echo "['".$sData['user_full_name']."(".$sData['given_vote'].")'],";

                    
                }  $q2=$d->select("election_users"," is_nota = '1' and  election_id='$election_id'","");
                  
                   $row2=mysqli_fetch_assoc($q2);  
                   if(!empty($row2)){
                     echo "['NOTA:".$row2['given_vote']."'],";
                   } ?> ],
                    datasets: [{
                      backgroundColor: [
                        "#1a4089",
                        "#2dce89",
                      ],
                      data: [
                      <?php
                $sq11=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
                while($sData = mysqli_fetch_array($sq11)){
              // echo "['".$sData['option_name']."', ".$sData['option_count']."],";
                  echo $sData['given_vote'].",";
                    
                }  $q2=$d->select("election_users"," is_nota = '1' and  election_id='$election_id'","");
                  
                   $row2=mysqli_fetch_assoc($q2);  
                   if(!empty($row2)){
                     echo $row2['given_vote'];
                   } ?>
                      ]
                  }]
                },
                    options: {
                        legend: {
                    position: 'bottom',
                          display: true,
                    labels: {
                            boxWidth:40
                          }
                        }
                    }
              });

              

            });
          </script>
         <div class="row">
          <?php  if($isTieinElection == "Yes" ) { ?>
            <div class="col-md-8">
              <canvas id="incomeStatus" height="150"></canvas>
            </div>
            <div class="col-md-4 text-center" >
              <h3 style="padding-bottom: 40px;">Tie</h3>
               <img class="rounded-circle" width="180" height="180" onerror="this.src='img/user.png'"  src="img/tie.jpg">
              <h4 class="text-uppercase">Tie in Election</h4>
              
          </div>
        <?php }  else 
 // /IS_243
         if($totalVote>0 ) { ?>
            <div class="col-md-8">
              <!-- <div id="incomeStatus" align='center'></div> -->
              <canvas id="incomeStatus" height="150"></canvas>
            </div>
            <div class="col-md-4 text-center" style="background-image: url(img/source.gif);">
              <h3 style="padding-bottom: 40px;">Winner</h3>
              <?php 
              $wq=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
              $i = 0;
              $winnerData=mysqli_fetch_array($wq);
              $winnerName= $winnerData['user_full_name'];
              $userWinnerVote= $winnerData['given_vote'];
              
              $nq=$d->select("election_users","election_id='$election_id' AND election_user_status=1 AND is_nota=1","");
              $notData=mysqli_fetch_array($nq);
              $notaVote= $notData['given_vote'];
                 // echo "<pre>";print_r($winnerData);echo "</pre>";
              if ($userWinnerVote>$notaVote) {
              
              ?><img class="rounded-circle" width="180" height="180" onerror="this.src='img/user.png'"  src="../img/users/recident_profile/<?php echo $winnerData['user_profile_pic']; ?>">
              <h4 class="text-uppercase"><?php echo $winnerData['user_full_name']; ?></h4>
              <?php
              }  else if ($userWinnerVote<$notaVote) {
                echo "Nota Win $notaVote votes";
              } else if ($userWinnerVote==$notaVote){
                echo "Nota & $winnerName are same votes";
              } ?>
              <?php  if ($election_status!=2 ):  ?>

               <form action="controller/pullingController.php" method="post" id="statusForm">
                <input type="hidden" name="election_id" value="<?php echo $data['election_id']; ?>">
                <input type="hidden" name="election_for" value="<?php echo $data['election_for']; ?>">
                <input type="hidden" name="election_status" value="2" >
                <input type="hidden" name="election_name" value="<?php echo $data['election_name']; ?>" >
                <input type="hidden" name="resultPublish" value="resultPublish" >
                 <?php //IS_850     ?> 
                <?php if($data['election_status']=="2"){
                  ?> <span  name="" class="btn   btn-info btn-sm" title="Result Published"   >Result Published</span><?php
                } else {?> 
                <input type="submit" name="resultPublish" class="btn form-btn btn-danger btn-sm" value="Publish Result ?" >
              <?php } ?>
              </form>
            <?php endif ?>
          </div>
        <?php }   else { ?>




         <div class="col-md-8">
          <h3>There are 0 votes in this election</h3>  
        </div>
      <?php } ?>
    </div>
    <!-- Pie Chart -->



  </div>

  <div class="table-responsive">
    <table id="example" class="table table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th><?php echo $xml->string->block; ?>-<?php echo $xml->string->unit; ?></th>
          <th>Votes</th>

        </tr>
      </thead>
      <tbody>
        <?php
        $q=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
        $i = 0;
        $cnt = 1;
        while($row=mysqli_fetch_array($q))
        { 
          $cnt++;
                  // extract($row);
          $i++;

          ?>
          <tr>

            <td><?php echo $i; ?></td>
            <td><?php echo $row['user_full_name']; ?> </td>
            <td><?php echo $row['user_designation']; ?>-<?php echo $row['block_name']; ?></td>
            <td><?php echo $row['given_vote']; ?></td>

          </tr>
        <?php }
          $q2=$d->select("election_users"," is_nota = '1' and  election_id='$election_id'","");
                  
                   $row2=mysqli_fetch_assoc($q2);  
                   if(!empty($row2)){ ?>
                  <tr>
                    <td><?php echo $cnt; ?></td>
                    <td>NOTA</td>
                    <td>-</td>
                     
                    <td><?php echo $row2['given_vote']; ?></td>
                  </tr>
                <?php }
        ?>
      </tbody>
    </table>
  </div>



</div>


</div>




</div>

</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->

