<?php error_reporting(0);
$gId = $_REQUEST['gId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Chat Group Member</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

        <form action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <label  class="form-control-label">Group Name</label>
                    <select name="gId" class="form-control single-select" required="">
                        <option value="">-- Select Group --</option> 
                        <?php 
                            $qb=$d->select("chat_group_master","society_id='$society_id'");  
                            while ($groupData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($gId==$groupData['group_id']) { echo 'selected';} ?> value="<?php echo $groupData['group_id'];?>" ><?php echo $groupData['group_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="col-md-3 form-group mt-auto">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                    if (isset($gId) && $gId > 0) {
                        $groupFilterQuery = "AND chat_group_member_master.group_id='$gId'";
                    }
                    $q=$d->selectRow("chat_group_member_master.*,chat_group_master.group_name, users_master.user_full_name,users_master.user_designation,users_master.user_token,users_master.device","chat_group_member_master,chat_group_master,users_master","chat_group_member_master.society_id='$society_id' AND chat_group_member_master.user_id=users_master.user_id AND chat_group_member_master.group_id=chat_group_master.group_id AND users_master.delete_status=0  AND chat_group_member_master.chat_group_member_delete=0 $groupFilterQuery");

                    if(isset($gId) && $gId > 0){
                ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>Name</th>                      
                        <th>Designation</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['user_designation']; ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                          <?php if($data['active_status']==0){
                              $status = "chatGroupMemberStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "chatGroupMemberStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['chat_group_member_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       
                        <form action="controller/ChatGroupController.php" method="post">
                            <input type="hidden" name="group_name" value="<?php echo $data['group_name']; ?>">
                            <input type="hidden" name="group_id" value="<?php echo $data['group_id']; ?>">
                            <input type="hidden" name="chat_group_member_id" value="<?php echo $data['chat_group_member_id']; ?>">
                            <input type="hidden" name="employee_name" value="<?php echo $data['user_full_name']." (".$data['user_designation'].") "; ?>">
                            <input type="hidden" name="registrationIds" value="<?php echo $data['user_token']; ?>">
                            <input type="hidden" name="device" value="<?php echo $data['device']; ?>">
                            <input type="hidden" name="removeEmployeeFromGroup" value="removeEmployeeFromGroup">
                              <button type="submit" class="btn ml-2 form-btn btn-sm form-btn btn-danger mb-2" > Remove </button>
                          </form>
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } else {  ?>
                  <div class="" role="alert">
                    <span><strong>Note :</strong> Please Select Group</span>
                  </div>
                <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Group Member</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addGroupMemberFrom" action="controller/ChatGroupController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Group Name <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <select name="group_id" id="group_id" class="form-control single-select" required="">
                    <option value="">-- Select Group --</option> 
                    <?php 
                        $qb=$d->select("chat_group_master","society_id='$society_id' AND active_status=0");  
                        while ($groupData=mysqli_fetch_array($qb)) {
                    ?>
                    <option <?php if($gId==$groupData['group_id']) { echo 'selected';} ?> value="<?php echo $groupData['group_id'];?>" ><?php echo $groupData['group_name'];?></option>
                    <?php } ?>
                </select>
              </div>  
           </div>
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                  <option value="">-- Select Branch --</option> 
                  <?php 
                  $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                  while ($blockData=mysqli_fetch_array($qb)) {
                  ?>
                  <option value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                  <?php } ?>

                </select>
              </div>  
           </div>
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorIdForGroup(this.value);" required="">
                    <option value="">-- Select Department --</option> 
                    
                </select>
              </div>  
           </div>
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <select name="user_id[]" id="user_id" class="form-control multiple-select" required="" multiple>
                    <option value="">-- Select Employee --</option> 
        
                </select>
              </div>  
           </div>                    
           <div class="form-footer text-center">
              <input type="hidden" name="addGroupMember"  value="addGroupMember">
             <button id="addGroupMemberBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addGroupMemberFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
