<?php 
session_start();
include 'common/object.php';
include 'common/checkLogin.php';
include 'common/checkLanguage.php';

// include 'accessControlPage.php';
extract(array_map("test_input" , $_POST));
$society_id = $_COOKIE['society_id'];
$bms_admin_id = $_COOKIE['bms_admin_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
//print_r($_POST);die();
$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}


$aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
$adminData= mysqli_fetch_array($aq);


?>
  <div class="table-responsive">
    <table id="example" class="table table-bordered">
      <thead>
          <tr>
            
            <th>#</th>
            <th><?php echo $xml->string->name; ?></th>
            <th><?php echo $xml->string->committee; ?> </th>
            <th><?php echo $xml->string->date; ?></th>
          </tr>
      </thead>
      <tbody>
          <?php 
          $i=1;
          $query = "";
          if($startDate!="" && $endDate!="")
          {
          $startDate = date("Y-m-d", strtotime($startDate));
          $endDate = date("Y-m-d", strtotime($endDate));
            $query = " AND DATE_FORMAT(log_time,'%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(log_time,'%Y-%m-%d') <= '$endDate'";
          }
          
          if ($adminData['role_id']==1){ 
            $q = $d->select("log_master" ,"society_id='0' AND recident_user_id=0 $query","order by log_id  DESC limit 2000");
          } else {
            $q = $d->select("log_master,bms_admin_master" ,"log_master.user_id=bms_admin_master.admin_id AND log_master.society_id='$society_id' AND log_master.recident_user_id=0 AND bms_admin_master.role_id!='2' $query","order by log_master.log_id  DESC limit 2000");
          }
          while ($data=mysqli_fetch_array($q)) {
            // print_r($data);
           ?>
          <tr>
        
            <td><?php echo $i++; ?></td>
              <td><?php echo $data["log_name"]; ?></td>
              <td><?php echo $data["admin_name"]; ?></td>
              <td><?php echo $data["log_time"]; ?></td>
              <!-- <td></td> -->
          </tr>
          <?php } ?>
          
      </tbody>
      
    </table>
  </div>

  <script type="text/javascript">
    
    var table = $('#example').DataTable( {
      drawCallback: function(){
              $("img.lazyload").lazyload();
           },
      lengthChange: true,
      pageLength: 25,
      lengthMenu: [[5,10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            // buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
    } );
  </script>
