<?php 
extract($_POST); 
session_start();
// error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

include 'common/checkLanguage.php';

  $data = $d->selectArray("expenses_balance_sheet_master","expenses_balance_sheet_id='$expenses_balance_sheet_id'");
  extract($data);
   $qb=$d->select("balancesheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
  $bbData=mysqli_fetch_array($qb);

  if($gst=="1"){ 

      $gstAmount =  $expenses_amount - ($expenses_amount  * (100/(100+$tax_slab)));  
       $expenses_amount = number_format($expenses_amount-$gstAmount,2,'.','');

    } 

    $countev=$d->sum_data("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
        while($rowev=mysqli_fetch_array($countev))
       {
            $asif=$rowev['SUM(recived_amount)'];
          $totalEventBooking=number_format($asif,2,'.','');
               
        }

    $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      while($row=mysqli_fetch_array($count7))
     {
          $asif=$row['SUM(penalty_amount)'];
         $totalPenalaty=number_format($asif,2,'.','');
             
      }

     $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
    while($row2=mysqli_fetch_array($icnome))
   {
        $asif5=$row2['SUM(income_amount)'];
      $totalIncome=number_format($asif5,2,'.','');
           
    }
     $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

    $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
      while($row5=mysqli_fetch_array($count4))
     {
          $asif5=$row5['SUM(expenses_amount)'];
        $totalExp=number_format($asif5,2,'.','');
             
      }

      $totalBlance= number_format($totalIncome-$totalExp,2);
?>

  <form novalidate id="expenseEdit" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
    <input type="hidden" name="EditpadiMainExpense" value="EditpadiMainExpense">
    <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
    <input type="hidden" name="expenses_balance_sheet_id" value="<?php echo $expenses_balance_sheet_id ?>">
    <div class="form-group row">
      <label for="expenses_title" class="col-sm-5 col-form-label">Expense Title <span class="required">*</span></label>
      <div class="col-sm-7">
        <input maxlength="100" value="<?php echo $expenses_title ?>" required="" type="text" class="form-control" name="expenses_title" id="expenses_title">
      </div>
      
    </div>
    <div class="form-group row">
      <label for="expAmoint" class="col-sm-5 col-form-label">Expense Amount <span class="required">*</span></label>
      <div class="col-sm-7">
        <input required="" value="<?php echo $expenses_amount ?>" type="text" maxlength="20" id="expAmoint" min="1" max="<?php echo ($totalIncome-$totalExp)+$expenses_amount; ?>" class="form-control onlyNumber" inputmode="numeric" name="expenses_amount">
      </div>
      
    </div>
    <div class="form-group row">
      <label for="input-10" class="col-sm-5 col-form-label">Expense Date <span class="required">*</span></label>
      <div class="col-sm-7">
        <?php //IS_579  id="autoclose-datepicker1" to  expenses_add_date class expense-income-cls?>
        <input required="" autocomplete="off" value="<?php echo $expenses_add_date ?>" type="text" class="form-control expense-income-cls"  id="expenses_add_date" name="expenses_add_date">
      </div>
    </div>

    <div class="form-group row">
      <label for="input-10" class="col-sm-5 col-form-label">Expense Category </label>
      <div class="col-sm-7">
        <select type="text" class="form-control single-select"  id="expense_category_id" name="expense_category_id">
          <option value=""> -- Select --</option>
          <?php 
           $qcc=$d->select("expense_category_master","active_status=0","");
           while($row=mysqli_fetch_array($qcc))
           {?>
          <option <?php if($expense_category_id==$row['expense_category_id']) { echo 'selected';}  ?> value="<?php echo $row['expense_category_id']; ?>"><?php echo $row['expense_category_name']; ?></option>
          <?php  }?>
        </select>
      </div>
    </div>
    
    <div class="form-group row">
      <label for="input-13" class="col-sm-5 col-form-label">Select Image</label>
      
      <div class="col-sm-7">
        <input required="" value="<?php echo $expenses_photo ?>" type="hidden" class="form-control expense-income-cls"  id="expenses_photo_old" name="expenses_photo_old">
        <input type="file" class="form-control-file border docOnly" id="input-14" name="expenses_photo">
      </div>
    </div>

    
    <div class="form-footer text-center">
      <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="editExpenses"></i> Update </button>
      <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
    </div>
        </form>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">


 $(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


$().ready(function() {
   $('#is_taxble').on('change', function() {
    if(this.value=="1"){
      $('#gst_detail_div_edit').css('display','block');
    } else {
      $('#gst_detail_div_edit').css('display','none');
    }
    
});

  $.validator.addMethod("noSpace", function(value, element) { 
        return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");

   $.validator.addMethod("alphaRestSpeChartor", function(value, element) {
        return this.optional(element) || value == value.match(/^[A-Za-z 0-9\d=!,\n\-@&()/?%._*]*$/);
    }, jQuery.validator.format("special characters not allowed"));


        $("#expenseEdit").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            expenses_amount:{
                required:true
            }
            ,expenses_add_date:{
                required: true
            }
            ,is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#expense :input[name="is_taxble"]').val()=="1" ;
                    }
                }
            }

            //24feb2020
            
        },
        messages: {
            expenses_title: {
                required : "Please enter title",
                noSpace: "No space please and don't leave it empty",
            },
            balancesheet_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

            //24feb2020
            expenses_add_date:{
                required : "Please select expense date"
            } ,
            income_amount:{
                required : "Please enter income amount"
            },
            expenses_amount:{
                required : "Please enter expense amount"
            },is_taxble:{
                required: "Please select Bill Type"
            }
            ,taxble_type:{
                required:  "Please select Tax Type"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          }
    });

        var date = new Date();
 date.setDate(date.getDate());

    $('.expense-income-cls').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
      });
      $('.expense-income-cls').datepicker('setEndDate', date);
});
</script>