<?php

    // error_reporting(E_ALL);
// ini_set('display_errors', '1');

    function validateDateMonth($date, $format = 'm')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function validateDateYear($date, $format = 'Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    if(isset($_GET['bId']) && isset($_GET['dId']) && isset($_GET['uId']) && !empty($_GET['getReport']) && !empty($_GET['bId']) && !empty($_GET['dId']) && !empty($_GET['uId']) && isset($_GET['filter_month']) && !empty($_GET['filter_month']))
    {
        $check_d = validateDateMonth($_GET['filter_month']);
        if($check_d != 1)
        {
            $_SESSION['msg1'] = "Invalid Month!";
        ?>
            <script>
                window.location = "addAttendance";
            </script>
        <?php
            exit;
        }
    }

    if(isset($_GET['bId']) && isset($_GET['dId']) && isset($_GET['uId']) && !empty($_GET['getReport']) && !empty($_GET['bId']) && !empty($_GET['dId']) && !empty($_GET['uId']) && isset($_GET['filter_year']) && !empty($_GET['filter_year']))
    {
        $check_d = validateDateYear($_GET['filter_year']);
        if($check_d != 1)
        {
            $_SESSION['msg1'] = "Invalid Year!";
        ?>
            <script>
                window.location = "addAttendance";
            </script>
        <?php
            exit;
        }

        if($_GET['filter_year'] > date("Y"))
        {
            $_SESSION['msg1'] = "Future Year Filter Not Available!";
        ?>
            <script>
                window.location = "addAttendance";
            </script>
        <?php
            exit;
        }
    }
    $bId = $_REQUEST['bId'];
    $dId = $_REQUEST['dId'];
    $uId = $_REQUEST['uId'];
    $currentYear = date('Y');
    $currentMonth = date('m');
    $nextYear = date('Y', strtotime('+1 year'));
    $onePreviousYear = date('Y', strtotime('-1 year'));
    $twoPreviousYear = date('Y', strtotime('-2 year'));
    if(isset($_GET['bId']) && isset($_GET['dId']) && isset($_GET['uId']) && !empty($_GET['getReport']) && !empty($_GET['bId']) && !empty($_GET['dId']) && !empty($_GET['uId']) && (!ctype_digit($_GET['bId']) || !ctype_digit($_GET['dId']) || !ctype_digit($_GET['uId'])))
    {
        $_SESSION['msg1'] = "Invalid Request!";
    ?>
        <script>
            window.location = "addAttendance";
        </script>
    <?php
        exit;
    }
?>
<style>
    .attendance_status
    {
        min-width: 113px;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row ">
            <div class="col-sm-9 col-12">
                <h4 class="page-title">Add Past Date Attendances</h4>
            </div>
        </div>
        <form class="branchDeptFilter">
            <div class="row">
                <div class="col-md-3 form-group col-6">
                    <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockIdAll(this.value,'na')" required>
                        <option value="">--Select--</option>
                        <?php
                        $qb = $d->select("block_master","society_id = '$society_id' $blockAppendQueryOnly");
                        while ($blockData = mysqli_fetch_array($qb))
                        {
                        ?>
                        <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3 form-group col-6">
                    <select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required>
                        <option value="">--Select--</option>
                        <?php
                            $qd=$d->select("block_master,floors_master","floors_master.society_id = '$society_id' AND block_master.block_id = floors_master.block_id AND floors_master.block_id = '$bId' $blockAppendQuery");
                            while ($depaData = mysqli_fetch_array($qd))
                            {
                        ?>
                        <option <?php if($dId == $depaData['floor_id']) { echo 'selected';} ?> value="<?php echo $depaData['floor_id'];?>"><?php echo $depaData['floor_name'];?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-3 form-group col-6">
                    <select name="uId" id="uId" class="form-control single-select" required>
                        <option value="">All Employee </option>
                        <?php
                            $user = $d->select("users_master","society_id = '$society_id' AND delete_status = 0 AND user_status != 0 AND floor_id = '$dId' $blockAppendQueryUser");
                            while ($userdata = mysqli_fetch_array($user))
                            {
                        ?>
                        <option <?php if($uId==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <select class="form-control single-select" name="filter_month" id="filter_month" required>
                        <option value="">--Select--</option>
                        <?php
                          $selected = "";
                          for ($m = 1; $m <= 12; $m++) {
                            $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                            $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                            if (isset($_GET['month'])  && $_GET['month'] !="") {
                              if($_GET['month'] == $m)
                              {
                                $selected = "selected";
                              }else{
                                $selected = "";
                              }
                              
                            } else {
                              $selected = "";
                              if($m==date('n'))
                              { 
                                $selected = "selected";
                              }
                              else
                              {
                                $selected = "";
                              }
                            }
                          ?>
                          <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
                        <?php }?>
                    </select>
                </div>
                <div class="col-md-1 form-group">
                    <select class="form-control single-select" name="filter_year" id="filter_year" required>
                        <option value="">--Select--</option>
                        <option <?php if (isset($_GET['filter_year']) && $_GET['filter_year'] == $twoPreviousYear) {
                              echo 'selected';
                            } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                        <option <?php if (isset($_GET['filter_year']) && $_GET['filter_year'] == $onePreviousYear) {
                                  echo 'selected';
                                } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                        <option <?php if (isset($_GET['filter_year']) && $_GET['filter_year'] == $currentYear) {
                                  echo 'selected';
                                }else{ echo ''; } ?> <?php if (!isset($_GET['filter_year'])) {
                                        echo 'selected';
                                      } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                        <option <?php if (isset($_GET['filter_year']) && $_GET['filter_year'] == $nextYear) {
                                  echo 'selected';
                                } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <input type="hidden" name="getReport" value="Get"/>
                    <input class="btn btn-sm btn-success" value="GET" type="submit" class="form-control">
                </div>
            </div>
        </form>
        <div class="row mt-2">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                        <?php
                        if(isset($_GET['getReport']) && isset($_GET['bId']) && isset($_GET['dId']) && isset($_GET['uId']) && isset($_GET['filter_month']) && isset($_GET['filter_year']) && !empty($_GET['getReport']) && !empty($_GET['bId']) && !empty($_GET['dId']) && !empty($_GET['uId']) && !empty($_GET['filter_month']) && !empty($_GET['filter_year']) && ctype_digit($_GET['bId']) && ctype_digit($_GET['dId']) && ctype_digit($_GET['uId']))
                        {
                            $q1 = $d->selectRow("stm.shift_start_time,stm.shift_end_time,stm.early_out_time,stm.late_time_start,um.unit_id,stm.per_day_hour,um.shift_time_id,stm.shift_type,stm.week_off_days,um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id JOIN shift_timing_master AS stm ON stm.shift_time_id = um.shift_time_id","um.user_id = '$uId'");
                            $data = $q1->fetch_assoc();
                            if (mysqli_num_rows($q1)>0) {
                                // code...
                                extract($data);
                            }


                            $i = 1;
                            if (isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND users_master.floor_id='$dId'";
                            }
                            if(isset($bId) && $bId>0)
                            {
                                $blockFilterQuery = " AND users_master.block_id='$bId'";
                            }
                            if (isset($_GET['filter_month']) && $_GET['filter_month'] != '' && isset($_GET['filter_year']) && $_GET['filter_year'] != '')
                            {
                                $year = $_GET['filter_year'];
                                $month = $_GET['filter_month'];

                                $dateFilterQuery = " AND YEAR(attendance_master.attendance_date_start) = ".$year." AND MONTH(attendance_master.attendance_date_start) = $month";
                            }
                            if (isset($uId) && $uId > 0)
                            {
                                $userFilterQuery = " AND attendance_master.user_id = '$uId'";
                            }
                            $q = $d->selectRow("attendance_master.*,users_master.*,block_master.block_name,floors_master.floor_name,shift_timing_master.shift_type,leave_master.leave_type_id,leave_master.leave_day_type,leave_master.leave_id","attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id
                            LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date = attendance_master.attendance_date_start,users_master,block_master,floors_master"," users_master.block_id = block_master.block_id AND users_master.floor_id = floors_master.floor_id AND users_master.user_id = attendance_master.user_id AND attendance_master.society_id = '$society_id' AND users_master.delete_status = 0 $blockFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser","ORDER BY attendance_master.attendance_date_start DESC");
                            $counter = 1;
                            if (mysqli_num_rows($q1)>0) {
                            ?>
                            <table class="table table-bordered example_table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        <th>Unattended Dates</th>
                                        <th>Day Type</th>
                                        <th>Punch In Date</th>
                                        <th>Punch In</th>
                                        <th>Punch Out Date</th>
                                        <th>Punch Out</th>
                                        <th>Hours</th>
                                        <th>is modified</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $all_data = [];
                                    $attend_days = [];
                                    while ($data = $q->fetch_assoc())
                                    {
                                        $attend_days[] = substr($data['attendance_date_start'], -2);
                                        $all_data[] = [
                                           'attendance_id' => $data['attendance_id'],
                                           'shift_time_id' => $data['shift_time_id'],
                                           'attendance_date_start' => $data['attendance_date_start'],
                                           'attendance_date_end' => $data['attendance_date_end']
                                        ];
                                        if ($data['attendance_in_from'] != null)
                                        {
                                            if ($data['attendance_in_from'] == 0)
                                            {
                                                $attendance_in_from = " <i class='fa fa-eye' aria-hidden='true'></i>";
                                            }
                                            else
                                            {
                                                $attendance_in_from = " <i class='fa fa-mobile' aria-hidden='true'></i>";
                                            }
                                        }
                                        else
                                        {
                                            $attendance_in_from = "";
                                        }
                                        if ($data['attendance_out_from'] != null)
                                        {
                                            if ($data['attendance_out_from'] == 0)
                                            {
                                                $attendance_out_from = " <i class='fa fa-eye' aria-hidden='true'></i>";
                                            }
                                            else
                                            {
                                                $attendance_out_from = " <i class='fa fa-mobile' aria-hidden='true'></i>";
                                            }
                                        }
                                        else
                                        {
                                            $attendance_out_from = "";
                                        }
                                    ?>
                                    <tr <?php if ($data['is_modified'] == "1") { echo "class='text-danger'";  } ?>>
                                        <td><?php echo $counter++; ?></td>
                                        
                                        
                                        <td>
                                        <?php
                                            if ($data['attendance_status'] == 0)
                                            {
                                                if ($data['attendance_range_in_km'] != "")
                                                {
                                                    $data['attendance_range_in_km'] = $data['attendance_range_in_km'];
                                                }
                                                else
                                                {
                                                    $data['attendance_range_in_km'] = 0;
                                                }
                                                if ($data['attendance_range_in_meter'] != "0.00")
                                                {
                                                    $data['attendance_range_in_meter'] = $data['attendance_range_in_km'];
                                                }
                                                else
                                                {
                                                    $data['attendance_range_in_meter'] = 0;
                                                }
                                                if ($data['punch_out_latitude'] != "")
                                                {
                                                    $out_lat = $data['punch_out_latitude'];
                                                }
                                                else
                                                {
                                                    $out_lat = 0;
                                                }
                                                if ($data['punch_out_longitude'] != "")
                                                {
                                                    $out_lng = $data['punch_out_longitude'];
                                                }
                                                else
                                                {
                                                    $out_lng = 0;
                                                }
                                            ?>
                                            <span class="badge badge-pill badge-warning m-1">Pending </span>
                                            <?php
                                            }
                                            else if ($data['attendance_status'] == 1)
                                            {
                                            ?>
                                            <span class="badge badge-pill badge-success m-1">Approved </span>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <span class="badge badge-pill badge-danger m-1">Declined </span>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td>
                                                <button type="button" class="btn btn-sm btn-primary" onclick="allAttendaceSet(<?php echo $data['attendance_id']; ?>)"> <i class="fa fa-eye"></i></button>
                                                <input type="hidden" name="user_id_attendace" value="<?php if ($data['user_id'] != "") { echo $data['user_id'];}   ?>">
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td><?php if ($data['attendance_date_start'] != '0000-00-00' && $data['attendance_date_start'] != 'null') {
                                            echo date("d M Y", strtotime($data['attendance_date_start'])) . " (" . date("D", strtotime($data['attendance_date_start'])) . ")";
                                        } ?></td>
                                        <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                                            echo date("h:i A", strtotime($data['punch_in_time'])) . $attendance_in_from;
                                            } ?>
                                        </td>
                                        <td><?php if ($data['attendance_date_end'] != '0000-00-00' && $data['attendance_date_end'] != 'null') { echo date("d M Y", strtotime($data['attendance_date_end']));
                                        } ?></td>
                                        <td><?php if ($data['punch_out_time'] != '00:00:00' && $data['punch_out_time'] != 'null') {
                                            echo date("h:i A", strtotime($data['punch_out_time'])).$attendance_out_from;
                                            } ?>
                                            <?php if ($data['punch_out_image'] != '') { ?>
                                            <?php } ?>
                                            <?php if ($data['punch_out_latitude'] != '' & $data['punch_out_longitude'] != '') { ?>
                                            <?php } ?>
                                        </td>
                                        <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                                            } else {
                                            echo $data['total_working_hours'];
                                            } ?>
                                        </td>
                                        <td><?php if ($data['is_modified'] == '1' ) {
                                            echo  "Yes";
                                            } else {
                                            echo "No";
                                            } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }


                                    if(isset($month) && isset($year) && ctype_digit($month) && ctype_digit($year) && $month != 0 && $year != 0)
                                    {
                                        $total_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                                    }
                                    $unattend_days = [];
                                    $pop = 0;
                                    $fil_mo_ye = $_GET['filter_month']."-".$_GET['filter_year'];
                                    $mf = date("Y-m",strtotime("01-".$fil_mo_ye));
                                    for($cn = 1;$cn <= $total_days;$cn++)
                                    {
                                        if(!in_array($cn,$attend_days))
                                        {
                                            $unattend_days[] = $mf."-".$cn;
                                            $pop++;
                                        }
                                    }
                                    $leave = $d->selectRow("lm.leave_start_date,lm.leave_reason,lm.auto_leave_reason,lm.leave_day_type,lm.leave_status,lm.half_day_session","leave_master AS lm","lm.user_id = '$uId'");
                                    $leave_all = [];
                                    $leave_all_reason = [];
                                    $leave_type = [];
                                    $leave_all_auto_reason = [];
                                    $full_app_leave = [];
                                    while($ld = $leave->fetch_assoc())
                                    {
                                        if($ld['leave_day_type'] == 0 && $ld['leave_status'] == 1)
                                        {
                                            $full_app_leave[] = $ld['leave_start_date'];
                                        }
                                        $leave_all[] = $ld['leave_start_date'];
                                        $leave_all_reason[$ld['leave_start_date']] = $ld['leave_reason'];
                                        if($ld['half_day_session'] == 1)
                                        {
                                            $leave_type[$ld['leave_start_date']] = ($ld['leave_day_type'] == 0) ? "Full Day" : "First Half";
                                        }
                                        elseif($ld['half_day_session'] == 2)
                                        {
                                            $leave_type[$ld['leave_start_date']] = ($ld['leave_day_type'] == 0) ? "Full Day" : "Second Half";
                                        }
                                        else
                                        {
                                            $leave_type[$ld['leave_start_date']] = ($ld['leave_day_type'] == 0) ? "Full Day" : "Half Day";
                                        }
                                        $leave_all_auto_reason[$ld['leave_start_date']] = $ld['auto_leave_reason'];
                                    }

                                    $holiday = $d->selectRow("hm.holiday_name,hm.holiday_days,hm.holiday_start_date,hm.holiday_end_date","holiday_master AS hm","hm.holiday_status = 0 AND hm.society_id = '$society_id'");
                                    $hol_data = [];
                                    $hol_date = [];
                                    while($all_hol = $holiday->fetch_assoc())
                                    {
                                        $hol_data[] = $all_hol;
                                        $hol_date[] = $all_hol['holiday_start_date'];
                                        $hol_date[$all_hol['holiday_start_date']] = $all_hol['holiday_name'];
                                    }
                                    $week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');


                                    $user_week_offs = explode(",",$data['week_off_days']);
                                    $user_week_offs_days = [];
                                    foreach($user_week_offs AS $k => $v)
                                    {
                                        $user_week_offs_days[] = $week_days[$v];
                                    }
                                    foreach ($unattend_days AS $key => $value)
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td></td>
                                        <td>
                                        <?php
                                            $ddd = new DateTime($value);
                                            $ddd_t = new DateTime();
                                            if($ddd < $ddd_t && $value != date("Y-m-j") && !in_array(date("Y-m-d",strtotime($value)),$full_app_leave))
                                            {
                                            ?>
                                            <div class="d-flex align-items-center">
                                                <button class="btn btn-primary btn-sm" onclick="addAttendance(<?php echo $uId; ?>,'<?php echo date("Y-m-d",strtotime($value)); ?>','<?php echo $shift_type; ?>',<?php echo $shift_time_id; ?>,'<?php echo $per_day_hour; ?>','<?php echo $unit_id; ?>','<?php echo $late_time_start; ?>','<?php echo $early_out_time; ?>','<?php echo $shift_start_time; ?>','<?php echo $shift_end_time; ?>')"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                            </div>
                                        <?php
                                            }
                                        ?>
                                        </td>
                                        <td><?php echo date("Y-m-d (D)",strtotime($value)); ?></td>
                                        <td>
                                        <?php
                                            if(in_array(date("l",strtotime($value)),$user_week_offs_days))
                                            {
                                            ?>
                                            <span class="badge badge-pill badge-info m-1">Week Off </span>
                                            <?php
                                            }

                                            if(in_array($value,$hol_date))
                                            {
                                            ?>
                                            <span class="badge badge-pill badge-secondary m-1" title="<?php echo $hol_date[$value]; ?>">Holiday </span>
                                            <?php
                                            }
                                            if(in_array(date("Y-m-d",strtotime($value)),$leave_all))
                                            {
                                                if($leave_all_reason[date("Y-m-d",strtotime($value))] != "")
                                                {
                                            ?>
                                            <span class="badge badge-pill badge-danger m-1" title="<?php echo $leave_all_reason[date("Y-m-d",strtotime($value))]; ?>">Leave (<?php echo $leave_type[date("Y-m-d",strtotime($value))]; ?>)</span>
                                            <?php
                                                }
                                                elseif($leave_all_reason[date("Y-m-d",strtotime($value))] == "" && $leave_all_auto_reason[date("Y-m-d",strtotime($value))] != "")
                                                {
                                                ?>
                                                <span class="badge badge-pill badge-danger m-1" title="<?php echo $leave_all_auto_reason[date("Y-m-d",strtotime($value))]; ?>">Leave (<?php echo $leave_type[date("Y-m-d",strtotime($value))]; ?>)</span>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <span class="badge badge-pill badge-danger m-1" title="">Leave (<?php echo $leave_type[date("Y-m-d",strtotime($value))]; ?>)</span>
                                                <?php
                                                }
                                            }
                                        ?>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php } else {
                                echo "Shift Not Assigned";
                            }
                            }
                            else
                            {
                            ?>
                                <label class="text-danger">Please use filters to get records</label>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>

<div class="modal fade" id="ChanngeAttendaceDataModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Update Attendance</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <div class="card-body">
                    <form id="changeAttendance" name="changeAttendance" enctype="multipart/form-data" method="post">
                        <div class="form-group row hidePunchDateForShift" id="hidePunchDateForShift">
                            <label for="input-10" class="col-sm-6 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                            <div class="col-lg-6 col-md-6" id="">
                                <select type="text" readonly maxlength="10" style="text-transform:uppercase;" name="punch_out_date" id="punch_out_date" class="form-control punch_out_date">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-sm-6 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                            <div class="col-lg-6 col-md-6" id="">
                                <input type="text" class="form-control time-picker-shft-Out_update" readonly id="punch_out_time" name="punch_out_time">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="updateUserAttendance" value="updateUserAttendance">
                            <input type="hidden" name="attendance_id" id="attendance_id">
                            <input type="hidden" class="indate" name="indate" id="indate">
                            <button id="addHrDocumentBtn" type="submit" onclick="updateUserAttendanceData()" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addAttendaceModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Attendance Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <div class="card-body">
                    <div class="row col-md-12 ">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody id="attendanceData">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row col-md-12 multiPunchInTable" style="display: none;">
                        <table class="table table-bordered">
                            <thead>
                                <th>Punch In</th>
                                <th>Punch Out</th>
                                <th>Hours</th>
                            </thead>
                            <tbody id="MulitPunchIn">
                            </tbody>
                        </table>
                    </div>
                    <div class="row col-md-12 wrkRpt">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="UserCheckInLocation">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Attendance Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content:center;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-6 in_reason_hide">
                            <span id="in_reason"></span>
                        </div>
                        <div class="col-md-6 col-6 out_reason_hide">
                            <span id="out_reason"></span>
                        </div>
                        <div class="col-md-6 col-12 out_reason_hide">
                            <label>Punch In Time</label>
                            <span id="approvInTime"></span>
                            <span class="badge badge-pill badge-primary m-1 InawayShow"></span>
                        </div>
                        <div class="col-md-6 col-12 out_reason_hide">
                            <label>Punch out Time</label>
                            <span id="approvOutTime"></span>
                            <span class="badge badge-pill badge-primary m-1 away"></span>
                        </div>
                        <div class="col-md-3 col-12 text-center">
                            <div class="punchInImage" id=""></div>
                        </div>
                        <div class="col-md-3 col-12 text-center">
                            <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                        </div>
                        <div class="col-md-3 col-12  text-center">
                            <div class="punchOutImage" id=""></div>
                        </div>
                        <div class="col-md-3  col-12 text-center">
                            <div class="map" id="aprovePunchOut" style="width: 100%; height: 300px;"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form id="attendanceDeclineReasonForm">
                            <div class="col-md-12 attendanceDeclineReason">
                                <label class="form-control-label">Decline Reason <span class="text-danger">*</span></label>
                                <textarea class="form-control" required name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
                            </div>
                            <div class="form-footer text-center">
                                <button type="submit" class="btn btn-primary attendanceDeclineReasonClass">Change Status</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="UserCheckInOutImage">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Attendance Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-6">
                            <div><b>Punch In :</b> <span id="in_time"></span></div>
                            <div ><b>Distance:</b> <span class="Inkm"></span></div>
                            <div ><b>Address:</b> <span class="InAddress"></span></div>
                        </div>
                        <div class="col-md-6">
                            <div><b>Punch Out :</b> <span id="out_time"></span></div>
                            <div ><b>Distance:</b> <span class="Outkm"></span></div>
                            <div ><b>Address:</b> <span class="OutAddress"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-center hideInData">
                            <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                            </div>
                        </div>
                        <div class="col-md-6 col-6 text-center hideOutData">
                            <div class="userCheckOutImageData " id="userCheckOutImageData">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12 float-left hideInData ">
                            <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="attendanceDeclineReason">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Declined Reason</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="holiday" style="align-content: center;">
                <textarea class="form-control" name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary attendanceDeclineReasonClass" data-dismiss="modal" aria-label="Close">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="leaveModal2">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Leave</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addLeaveForm" action="controller/leaveController.php" method="post">
                <div class="modal-body" id="leaveBoday" style="align-content: center;">
                </div>
                <input type="hidden" name="leave_date" id="leave_date">
                <input type="hidden" name="leave_user_id" id="leave_user_id">
                <input type="hidden" name="leave_id" id="leave_id">
                <input type="hidden" name="toDateTo" value="<?php echo $toDate; ?>">
                <input type="hidden" name="fromDate" value="<?php echo $from; ?>">
                <input type="hidden" name="branchId" value="<?php echo $_GET['bId']; ?>">
                <input type="hidden" name="dpId" value="<?php echo $_GET['dId']; ?>">
                <input type="hidden" name="leave_attendance_id" id="leave_attendance_id">
                <div class="modal-footer">
                    <div class="col-md-12 text-center">
                        <input type="hidden" name="MarkAsLeave" value="MarkAsLeave">
                        <button type="submit" class="btn btn-primary attendanceDeclineReasonClass">Save changes</button>
                        <button type="button" id="DeleteLeave" onclick="removeUserLeave()" class="btn btn-danger ">Remove Leave</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addAttendaceModalNew">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Attendance</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addAttendanceFormNew" action="controller/leaveController.php" method="post">
                <div class="modal-body" style="align-content: center;">
                    <div class="form-group row punch_out_date_div d-none">
                        <label for="punch_out_date" class="col-sm-4 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-group single-select" required name="punch_out_date" id="punch_out_date_new">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="punch_in_time" class="col-sm-4 col-form-label">Punch In Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control time-picker-shft-Out_update" readonly id="punch_in_time" name="punch_in_time">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="punch_out_time" class="col-sm-4 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control time-picker-shft-Out_update" readonly id="punch_out_time_new" name="punch_out_time">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="attendance_type" class="col-sm-4 col-form-label">Attendance Type <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control single-select attendance_drop" name="attendance_type" id="attendance_type" required>
                                <option value>--Select--</option>
                                <option value="0" selected>Full Day</option>
                                <option value="1">Half Day</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 text-center">
                        <input type="hidden" name="attendance_date" id="attendance_date_new">
                        <input type="hidden" name="shift_type" id="shift_type_new">
                        <input type="hidden" name="shift_time_id" id="shift_time_id_new">
                        <input type="hidden" name="per_day_hour" id="per_day_hour_new">
                        <input type="hidden" name="unit_id" id="unit_id_new">
                        <input type="hidden" name="late_time_start" id="late_time_start_new">
                        <input type="hidden" name="early_out_time" id="early_out_time_new">
                        <input type="hidden" name="shift_start_time" id="shift_start_time_new">
                        <input type="hidden" name="shift_end_time" id="shift_end_time_new">
                        <input type="hidden" name="block_id" value="<?php echo $_GET['bId']; ?>">
                        <input type="hidden" name="floor_id" value="<?php echo $_GET['dId']; ?>">
                        <input type="hidden" name="user_id" value="<?php echo $_GET['uId']; ?>">
                        <input type="hidden" name="filter_month" value="<?php echo $_GET['filter_month']; ?>">
                        <input type="hidden" name="filter_year" value="<?php echo $_GET['filter_year']; ?>">
                        <input type="hidden" name="addAttendance" value="addAttendance">
                        <button type="button" onclick="submitAddAttendanceForm();" class="btn btn-sm btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="assets/js/lazyload.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>

<script type="text/javascript">
function initialize(d_lat, d_long, blockRange, blockLat, blockLOng, out_lat, out_lng) {
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
    };
    var latitute = d_lat;
    var longitute = d_long;
    var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 13
    });
    ////User marker
    var marker = new google.maps.Marker({
        map: map,
        icon: "assets/images/placeholder.png",
        position: latlng,
        // draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company marker
    var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        //  draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
    });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
            center: {
                lat: latitute,
                lng: longitute
            },
            population: parkingRadition
        }
    };
    var infowindow = new google.maps.InfoWindow();
}

function initializeCommonMap(d_lat, d_long, out_lat, out_long) {
    var labels = 'IO';
    var labelIndex = 0;
    var locations = [
        ['Punch In', d_lat, d_long, 2, 'green'],
        ['Punch Out', out_lat, out_long, 1, 'red']
    ];
    var map = new google.maps.Map(document.getElementById('map3'), {
        zoom: 11,
        center: new google.maps.LatLng(d_lat, d_long),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for(i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            label: labels[labelIndex++ % labels.length]
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

function initialize3(d_lat, d_long) {
    var labels = 'IO';
    var labelIndex = 0;
    var locations = [
        ['Punch In', d_lat, d_long, 2]
    ];
    var map = new google.maps.Map(document.getElementById('map3'), {
        zoom: 11,
        center: new google.maps.LatLng(d_lat, d_long),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for(i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            label: labels[labelIndex++ % labels.length]
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

function initialize2(out_lat, out_long) {
    var labels = 'IO';
    var labelIndex = 0;
    var locations = [
        ['Punch Out', out_lat, out_long, 1]
    ];
    var map = new google.maps.Map(document.getElementById('map3'), {
        zoom: 11,
        center: new google.maps.LatLng(out_lat, out_long),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for(i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            label: labels[labelIndex++ % labels.length]
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

function approvePunchOutMap(d_lat, d_long, blockRange, blockLat, blockLOng) {
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
    };
    var latitute = d_lat;
    var longitute = d_long;
    var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlng,
        zoom: 13
    });
    ////User marker
    var marker = new google.maps.Marker({
        map: map,
        icon: "assets/images/placeholder.png",
        position: latlng,
        // draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company marker
    var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        //  draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
    });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
            center: {
                lat: latitute,
                lng: longitute
            },
            population: parkingRadition
        }
    };
    var infowindow = new google.maps.InfoWindow();
}
<?php if (!isset($_GET['month_year'])) { ?>
$('#month_year').val(<?php echo $currentMonth; ?>)
<?php } ?>

function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if(window.focus) {
        newwindow.focus()
    }
    return false;
}

function addAttendance(user_id,attendance_date,shift_type,shift_time_id,per_day_hour,unit_id,late_time_start,early_out_time,shift_start_time,shift_end_time)
{
    $.ajax({
        url: "controller/leaveController.php",
        cache: false,
        type: "POST",
        data:
        {
            checkApprovedHalfDay:"checkApprovedHalfDay",
            user_id:user_id,
            attendance_date:attendance_date,
            csrf:csrf
        },
        success: function(response)
        {
            response = JSON.parse(response);
            if(response == 1)
            {
                $('.attendance_drop').empty();
                $('.attendance_drop').append(`<option value="">--Select--</option>`);
                $('.attendance_drop').append(`<option value="1" selected>Half Day</option>`);
            }
            else
            {
                $('.attendance_drop').empty();
                $('.attendance_drop').append(`<option value="">--Select--</option>`);
                $('.attendance_drop').append(`<option value="0" selected>Full Day</option>`);
                $('.attendance_drop').append(`<option value="1">Half Day</option>`);
            }
        }
    });
    $('#attendance_date_new').val(attendance_date);
    $('#shift_type_new').val(shift_type);
    $('#shift_time_id_new').val(shift_time_id);
    $('#per_day_hour_new').val(per_day_hour);
    $('#unit_id_new').val(unit_id);
    $('#late_time_start_new').val(late_time_start);
    $('#early_out_time_new').val(early_out_time);
    $('#shift_start_time_new').val(shift_start_time);
    $('#shift_end_time_new').val(shift_end_time);
    if(shift_type == "Night")
    {
        var dt = $.datepicker.parseDate('yy-mm-dd', attendance_date);
        dt.setDate(dt.getDate() + 1)
        var dtNew = $.datepicker.formatDate('yy-mm-dd', dt);
        $('#punch_out_date_new').empty();
        $('#punch_out_date_new').append($("<option></option>").attr("value","").text("--Select--"));
        $('#punch_out_date_new').append($("<option></option>").attr("value", attendance_date).text(attendance_date));
        $('#punch_out_date_new').append($("<option></option>").attr("value", dtNew).text(dtNew));
        $('.punch_out_date_div').removeClass('d-none');
    }
    else
    {
        $('.punch_out_date_div').addClass('d-none');
    }
    $('#addAttendaceModalNew').modal();
}

$('#addAttendaceModalNew').on('hidden.bs.modal', function ()
{
    $('#addAttendanceFormNew').trigger("reset");
    $("#addAttendanceFormNew").validate().resetForm();
});

function submitAddAttendanceForm()
{
    if($('#addAttendanceFormNew').valid())
    {
        var timefrom = new Date();
        temp = $('#punch_in_time').val().split(":");
        timefrom.setHours((parseInt(temp[0]) - 1 + 24) % 24);
        timefrom.setMinutes(parseInt(temp[1]));
        var timeto = new Date();
        temp = $('#punch_out_time_new').val().split(":");
        timeto.setHours((parseInt(temp[0]) - 1 + 24) % 24);
        timeto.setMinutes(parseInt(temp[1]));
        shift_type = $('#shift_type_new').val();
        attendance_date = $('#attendance_date_new').val();
        punch_out_date = $('#punch_out_date_new').val();
        if(timeto < timefrom && shift_type == "Day")
        {
            swal("Punch in time should be smaller than punch out time!", {icon: "error",});
        }
        else if (timeto < timefrom && shift_type == "Night" && attendance_date == punch_out_date)
        {
            swal("Punch in time should be smaller than punch out time!", {icon: "error",});
        }
        else
        {
            $('#addAttendanceFormNew').submit();
        }
    }
    else
    {
        $('#addAttendanceFormNew').validate();
    }
}

var table = $('.example_table').DataTable( {
  drawCallback: function(){
          $("img.lazyload").lazyload();
       },
  lengthChange: true,
  pageLength: 50,
  lengthMenu: [[5,10, 25, 50, -1], [5, 10, 25, 50, "All"]]
      } );
</script>