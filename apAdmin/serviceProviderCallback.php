<?php
error_reporting(0);
extract($_GET);
if (!isset($_GET['filter'])) {
  $filter = 6;
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-4">
        <h4 class="page-title">Service Provider Call Back </h4>
       
      </div>
      <div class="col-sm-8">
        <form method="GET" >

            <select onchange="this.form.submit();" class="form-control single-select" name="service_provider_users_id">
                    <option value="0">All</option>
                    <?php 
                      $query = $d->select("local_service_provider_users"," local_service_provider_users.service_provider_status=0 $countryAppendQuerySP order by service_provider_name asc");
                      while ($local_service_provider_users = mysqli_fetch_array($query)) { ?>
                        <option <?php if($service_provider_users_id==$local_service_provider_users['service_provider_users_id']){echo "selected";} ?> value="<?php echo $local_service_provider_users['service_provider_users_id'] ?>"><?php echo $local_service_provider_users['category_name'].' ('.$local_service_provider_users['service_provider_name'].') ' ?></option>
                    <?php  } ?>
                  </select>


             
          </form>
      </div>
    </div>
    <!-- End Breadcrumb-->
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="reportTable" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Company</th>
                  <th>service provider category</th>
                  <th>Service provider name</th>
                  <th>User Name</th>
                  <th>User Phone</th>
                   <th>Request Date</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th class="no-search-box"></th>
                 
                  <th>Company</th>
                  <th>service provider category</th>
                  <th>Service provider name</th>
                  <th>User Name</th>
                  <th>User Phone</th>
                  <th>Request Date</th>
                </tr>
              </tfoot>
              <tbody>
                <?php 
                $where="";
                if(isset($service_provider_users_id) && $service_provider_users_id !=0){
                  $where=" and local_service_provider_users.service_provider_users_id = '$service_provider_users_id' ";
                }
                   $qcd=$d->select("local_service_provider_users,local_serviceproviders_call_request,society_master"," society_master.society_id = local_serviceproviders_call_request.society_id and   local_service_provider_users.service_provider_users_id = local_serviceproviders_call_request.local_service_provider_users_id $where  $countryAppendQuerySociety");
                   
                  while ($data=mysqli_fetch_array($qcd)) {
                    extract($data);
                ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td>
                    <?php echo $society_name; ?>
                  </td>
                   <td>
                    <?php echo $category_name; ?>
                  </td>

                   <td>
                    <?php echo $service_provider_name; ?>
                  </td>
                   <td>
                    <?php echo $user_name; ?>
                  </td>

                   <td>
                    <?php echo $user_mobile; ?>
                  </td>
                  <td>
                    <?php 
                    if(!is_null($request_date)){
                      echo date("d-m-Y h:i:s A", strtotime($request_date));
                    } else {
                      echo "NA";
                    }
                     ?>
                    
                  </td>
                 
                </tr>

                <?php } ?> 
              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>