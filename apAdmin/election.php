<?php
error_reporting(0);
extract($_REQUEST);
//echo "<pre>";print_r($_REQUEST);

if($action =="Edit"){
       
        extract(array_map("test_input" , $_POST));
        if (isset($election_id)) {
          $q=$d->select("election_master","society_id='$society_id' AND election_id='$election_id'");
          $row=mysqli_fetch_array($q);
          extract($row);
        }
}

?>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="election" action="controller/pullingController.php" method="post">
                <?php if(isset($election_id) ){  ?>
                  <input type="hidden" name="election_id" value="<?php echo $election_id; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bullhorn"></i>
                   <?php echo $xml->string->society; ?> Election <?php echo $action?>
                </h4>
                <div class="form-group row">
                  <label for="input-122" class="col-sm-2 col-form-label">Election Name <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" required="" maxlength="100" class="form-control" id="input-122" name="election_name" value="<?php if(isset($election_id) ){ echo $election_name;} ?>">
                  </div>
                  
                  <div class="col-sm-4">
                   
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Election Description <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <textarea maxlength="300" class="form-control" id="input-12" required="" name="election_description"><?php if(isset($election_id) ){ echo $election_description;} ?></textarea>
                  </div>
                  
                </div>
               

                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Election  Date <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" required="" readonly="" class="form-control" id="autoclose-datepicker2"  name="election_date" value="<?php if(isset($election_id) ){ echo $election_date;} ?>">
                  </div>
                  <label for="input-13333" class="col-sm-2 col-form-label">Election  For <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <select required=""  class="form-control"  name="election_for">
                      <option value="0"> <?php echo $xml->string->all; ?>  <?php echo $xml->string->floors; ?></option>
                    <?php 
                    $fq=$d->select("floors_master","society_id='$society_id' ");
                      while ($floorData=mysqli_fetch_array($fq)) { ?>
                      <option <?php if(isset($election_id) && $election_for==$floorData['floor_id']  ) { echo "selected";} ?> value="<?php echo $floorData['floor_id'];?>"><?php echo $floorData['floor_name'];?></option>
                    <?php  }?>
                    
                    </select>
                  </div>
                 
                </div>
                
                <?php //IS_108 
                if (isset($election_id)) {
                  $election_users_qry=$d->select("election_users","society_id='$society_id' AND election_id='$election_id'");
                  $election_users_data=mysqli_fetch_array($election_users_qry);
                   
                }
                ?>

                <div class="form-group row">
                  <label for="is_nota" class="col-sm-2 col-form-label">Is NOTA (N/A)? </label>
                   <div class="col-sm-10">
                    <div class=" icheck-inline">
                      <input <?php if(isset($election_id) && $election_users_data['is_nota']=="0"  ){ echo "checked";} else if (!isset($election_id)){ echo "checked";} ?>      type="radio"  id="inline-radio-info" value="0" name="is_nota">
                      <label for="inline-radio-info">No</label>
                    </div>
                    <div class=" icheck-inline">
                      <input <?php if(isset($election_id) && $election_users_data['is_nota']=="1"  ){ echo "checked";} ?>   type="radio" id="inline-radio-success" value="1" name="is_nota">
                      <label for="inline-radio-success">Yes</label>
                    </div>
                  </div>
                     
                  </div>
                  <div class="form-footer text-center">
                  <?php   if(isset($election_id)) { ?> 
                    <input type="hidden" name="editElection" value="editElection">
                     <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Update</button>
                  <?php } else {?>
                    <input type="hidden" name="addElection" value="addElection">
                    <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> SAVE</button>
                  <?php } ?>
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                </div>
                </div>
                <?php //IS_108 ?>
               
                
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
<?php //IS_933?>
   <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">

  $( "#event" ).submit(function( event ) {
   $("#myButton").attr("disabled", true);


   window.setTimeout(setDisabled, 10000);
 
  });

 function setDisabled() {
    document.getElementById('myButton').disabled = false;
}
 
    </script>