  <?php error_reporting(0);
  $vId = (int)$_REQUEST['vId'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Canteen Category</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addVendorProductBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteCanteenProductCategory');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
     </div>
      <form action="" >
        <div class="row pt-2 pb-2">
          <div class="col-md-12 form-group">
            <select name="vId" class="form-control single-select" onchange="this.form.submit()">
                <option value="">All Vendor</option> 
                <?php 
                  $qd=$d->select("vendor_master","society_id='$society_id' AND vendor_category_id=1 AND vendor_master_delete=0 AND vendor_active_status=0");  
                  while ($vendorData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($vId==$vendorData['vendor_id']) { echo 'selected';} ?> value="<?php echo  $vendorData['vendor_id'];?>" ><?php echo $vendorData['vendor_name'];?></option>
                <?php } ?>
              </select>
          </div>     
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Vendor Product Category Name</th>
                        <th>Vendor Name</th>
                        <th>Action</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      if(isset($vId) && $vId>0) {
                        $vendorFilterQuery = " AND vendor_master.vendor_id='$vId'";
                       }
                     
                      $q=$d->select("vendor_product_category_master,vendor_master,vendor_category_master","vendor_product_category_master.vendor_id = vendor_master.vendor_id AND vendor_product_category_master.vendor_category_id = vendor_category_master.vendor_category_id AND vendor_product_category_master.vendor_category_id='1' AND is_delete= 0 $vendorFilterQuery");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                        
                      <td class="text-center">
                          <?php $totalProduct = $d->count_data_direct("vendor_product_id","vendor_product_master","vendor_product_category_id='$data[vendor_product_category_id]'");
                            if( $totalProduct==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['vendor_product_category_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['vendor_product_category_id']; ?>">                      
                          <?php } ?>
                        </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['vendor_product_category_name']; ?></td>
                      <td><?php  echo $data['vendor_name']; ?></td>
                      <td>
                      <div class="d-flex align-items-center">
                        <form  method="post" accept-charset="utf-8">
                          <input type="hidden" name="vendor_product_category_id" value="<?php echo $data['vendor_product_category_id']; ?>">
                          <input type="hidden" name="editVendorProcduct" value="editVendorProcduct">
                         
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="vendorProductCategorySetData(<?php echo $data['vendor_product_category_id']; ?>)" data-toggle="modal" data-target="#addModal" > <i class="fa fa-pencil"></i></button> 
                        </form>

                        
                          <?php if($data['vendor_product_category_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_category_id']; ?>','vendorProductCategoryDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_category_id']; ?>','vendorProductCategoryActive');" data-size="small"/>
                          <?php } ?>
                        </div>
                      </td>
                      <td><?php if ($data['vendor_product_category_image']!='') { ?>
                          <a href="../img/vendor_category/<?php echo $data['vendor_product_category_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["vendor_product_category_image"]; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'" src="../img/ajax-loader.gif"  data-src="../img/vendor_category/<?php echo $data['vendor_product_category_image']; ?>"  href="#divForm<?php echo $data['vendor_product_category_id'];?>" class="btnForm lazyload" ></a>
                          <?php } else {
                          echo "Not Uploaded";
                          } ?>
                      </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->





<div class="modal fade" id="vendorProductCategoryDetailModel">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Vendor Product Category Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="vendorProductDetailModelDiv" style="align-content: center;">

      </div>
      
    </div>
  </div>
</div>
<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Canteen Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="addVendorProductCategoryForm" action="controller/vendorProductCategoryCont.php" enctype="multipart/form-data" method="post">
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Category Name <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text"  required="" name="vendor_product_category_name" id="vendor_product_category_name" class="form-control">
               </div>                   
           </div> 

           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Vandor<span class="required">*</span></label>
                   <div class="col-lg-8 col-md-8" id="">
                      <select name="vendor_id" id="vendor_id" class="form-control single-select restFrm">
                          <option value="">--Select Vandor--</option> 
                            <?php 
                              $qd=$d->select("vendor_master","society_id='$society_id' AND vendor_category_id='1' AND vendor_master_delete=0 AND vendor_active_status=0 ");  
                              while ($userData=mysqli_fetch_array($qd)) {
                            ?>
                          <option value="<?php echo  $userData['vendor_id'];?>" ><?php echo $userData['vendor_name'];?></option>
                          <?php } ?>
                  
                        </select>                 
                    </div>
           </div> 
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Category Image <span class="required" id="removeImageValid">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="file" name="vendor_product_category_image" id="vendor_product_category_image" class="form-control">
               <input type="hidden"  required="" name="vendor_product_category_image_old" id="vendor_product_category_image_old" class="form-control">
               </div>                   
           </div> 
                             
           <div class="form-footer text-center">
            
             <input type="hidden" id="vendor_product_category_id" name="vendor_product_category_id" value="" >
             <button id="addCanteenProductCategoryBtn" name="addCanteenProductCategoryBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addCanteenProductCategory" value="addCanteenProductCategory">
             
             <button id="addCanteenProductCategoryBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           
             <button type="button"  value="add" class="btn btn-danger cancel hideAdd" onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button>
            
           </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function resetForm()
  {
    $(".restFrm").val('').trigger('change');
    $('#vendor_product_category_name').val('');
    $('#vendor_product_category_image').val('');
  }
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>