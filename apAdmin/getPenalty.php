<?php
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}
include 'common/checkLanguage.php';
if ($_COOKIE['country_id']==101) { 
  $cgstName = $xml->string->cgst;
  $sgstName = $xml->string->sgst;
  $igstName = $xml->string->igst;
} else if ($_COOKIE['country_id']==161) {
  $igstName = $xml->string->igst;
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));

$checS=$d->selectRow("virtual_wallet,currency","society_master","society_id='$society_id'");
$sData=mysqli_fetch_array($checS);
$virtual_wallet= $sData['virtual_wallet'];
$currency= $sData['currency'];

$q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=users_master.block_id and penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id' AND penalty_master.penalty_id='$penalty_id'","ORDER BY penalty_master.penalty_date DESC");
$data=mysqli_fetch_array($q);
extract($data);
//IS_605
?>
  <form id="payPenaltyFrm" action="controller/penaltyController.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
    <input type="hidden" name="currency" id="currency" value="<?php echo $currency;?>">
    <input type="hidden" name="penalty_id" id="penalty_id" value="<?php echo $penalty_id;?>">
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
    <input type="hidden" name="unit_id" id="unit_id" value="<?php echo $unit_id;?>">
    <input type="hidden" name="user_name" id="user_id" value="<?php echo $user_full_name;?>">
    <input type="hidden" name="paidPanalty" value="paidPanalty">
    <input type="hidden" name="unit_name" value="<?php echo $block_name.'-'.$unit_name; ?>">
    <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id;?>" id="balancesheet_id">
    <div class="form-group row">
      <label for="input-10" class="col-sm-5 col-form-label">Employee </label>
      <div class="col-sm-7">
        <?php echo$user_full_name.'-'.$user_designation.' ('.$block_name.')'; ?>
      </div>
      
    </div>
    <div class="form-group row">
      <label for="payment_type" class="col-sm-5 col-form-label">Payment Method <span class="required">*</span></label>
      <div class="col-sm-7">
        <?php //IS_846  onchange="getBankDetails(this.value)" ?>
        <select required="" type="text" class="form-control check" id="payment_type" name="payment_type" onchange="getBankDetails(this.value)" >
          <option value="">-- Select --</option>
          <option value="0">Cash</option>
          <option value="1">Cheque</option>
          <option value="2">Online Payment</option>
        </select>
      </div>
      
    </div>
    <?php //IS_846 ?>
    <div id="addBankDetails"></div>
    <input type="hidden" name="isCash" id="isCash" value="">
    <div class="form-group row">
      <label for="date-time-picker4" class="col-sm-5 col-form-label">Payment Date <span class="required">*</span></label>
      <div class="col-sm-7">
        <!--  <input required="" readonly="" type="text" class="form-control"  id="received_date" name="received_date"> -->
        <input type="text" id="date-time-picker4" readonly="" maxlength="250" required="" name="received_date" class="form-control" value="<?php echo $penalty_date;?>" min="<?php echo $penalty_date;?>" max="<?php echo date("Y-m-d H:i");?>">
        
      </div>
      
    </div>
  <!--  <div class="form-group row">
    <label for="input-10" class="col-sm-5 col-form-label">Time <span class="required">*</span></label>
    <div class="col-sm-7" >
      <input type="text" id="time-picker" readonly="" maxlength="250" required="" name="penalty_date_time" class="form-control">
    </div>
  </div> -->
  <?php
  $now = time();
  $your_date = strtotime($penalty_date);
  $datediff = $now - $your_date;
  /* $today= date('Y-m-d');
  $date1 = new DateTime($today);
  $date2 = new DateTime($penalty_date);
  $interval = $date1->diff($date2);
  $difference_days=$interval->days;
  $difference_days=$difference_days+1;
  $difference_days= '-'.$difference_days.'d'; */
  ?>
  <input type="hidden" id="penaltydatediff" value="<?php echo round($datediff / (60 * 60 * 24)); ?>">
  
  <?php  if($is_taxble=='1'){ ?>
  <div class="form-group row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Penalty <?php if($is_taxble=="1") { ?>Without <?php echo $xml->string->tax; } ?></label>
    
    <div class="col-sm-7">
     <?php echo $currency;?> <?php echo $amount_without_gst; ?>
    </div>
  </div>
  <div class="form-group row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Tax Amount </label>
    
    <div class="col-sm-7">
      <?php
      if ($taxble_type=="1") {
        $gstValue = $penalty_amount - ($penalty_amount  * (100/(100+$tax_slab))); 
          echo $igstName." ($tax_slab%): ".$currency.' '.number_format($gstValue,2,'.','');
      } else {
         $gstValue = $penalty_amount - ($penalty_amount * (100/(100+$tax_slab))); 
          $sepGst = $gstValue/2;
          $gstPer = $tax_slab/2;
        echo $cgstName." ($gstPer%): " .$currency.' '.number_format($sepGst,2,'.','');
        echo "<br>";
        echo $sgstName." ($gstPer%): " .$currency.' '.number_format($sepGst,2,'.','');
      }
      
      ?>
    </div>
  </div>
  
  
  <?php } //IS_1470 ?>
  <div class="form-group row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Amount  <?php if($is_taxble=="1") { ?>with <?php echo $xml->string->tax; } ?>(<?php echo $currency;?>) <span class="required">*</span></label>
    
    <div class="col-sm-7">
      <input type="hidden" min="<?php echo $penalty_amount;?>" class="form-control" id="penalty_amount_org" value="<?php echo $penalty_amount;?>"  name="penalty_amount_org">
      <input <?php if($virtual_wallet==1) { echo 'readonly'; } ?>  onkeyup="chekOverpaid();" type="text" min="<?php echo $penalty_amount;?>" max="<?php echo $penalty_amount;?>" class="form-control" id="penalty_amount" value="<?php echo $penalty_amount;?>"  name="penalty_amount">
    </div>
  </div>
  <div class="form-group row" >
    <label for="" class="col-sm-5 col-form-label">  </label> 
     <div class="col-sm-7 text-danger"  id="walletMsg">
     </div>
  </div>
  
  <div class="form-footer text-center">
    <button id="PayPaneltyBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Paid </button>
    <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
  </div>
</form>
<script>


//IS_846
function getBankDetails(value){

  if(value!=""){
    $.ajax({
      url: 'ajaxPaymentData.php',
      type: 'POST',
      data: {payment_type_value: value},
    })
    .done(function(response) {
      $('#addBankDetails').html(response);
      $('#isCash').val('0');

    });
  }else{
    $('#addBankDetails').html('');
    $('#isCash').val('');

  }
}
//IS_846
var date = new Date();
var minDate= '<?php echo $penalty_date; ?>';
date.setDate(date.getDate());
$('#default-datepicker').datepicker({
  format: 'yyyy-mm-dd'
});
$('#autoclose-datepicker,#autoclose-datepicker1').datepicker({
  autoclose: true,
  todayHighlight: true,
  format: 'yyyy-mm-dd'
});
$('#autoclose-datepicker,#autoclose-datepicker2').datepicker({
  autoclose: true,
  todayHighlight: true,
  startDate: date,
  format: 'yyyy-mm-dd'
});
$('#autoclose-datepicker-dob').datepicker({
  autoclose: true,
  todayHighlight: true,
  format: 'yyyy-mm-dd'
});
var diff = $('#penaltydatediff').val();
$('#autoclose-datepickerFrom,#autoclose-datepickerTo').datepicker({
  autoclose: true,
  todayHighlight: true,
  format: 'yyyy-mm-dd',
  startDate:'-'+diff+'d',
  endDate: '+0d',
});
$('#inline-datepicker').datepicker({
  todayHighlight: true
});
$('#dateragne-picker .input-daterange').datepicker({
  autoclose: true,
  startDate: date,
  format: 'yyyy-mm-dd'
});

$('#received_date').datepicker({
  todayHighlight: true,
  format: 'yyyy-mm-dd',
  autoclose: true ,
  startDate: new Date('<?php echo date("Y-m-d",strtotime($penalty_date));?>'),
  endDate: new Date('<?php echo date("Y-m-d");?>')
});
//26feb2020
</script>
<script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript">
  $('#time-picker').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm:ss',
    minTime: new Date('<?php echo date("Y-m-d penaltiesH:i:s",strtotime($penalty_date));?>')
  });
  var FromEndDate = new Date();
  $('#date-time-picker4').bootstrapMaterialDatePicker({
/* showMeridian: true,
minuteStep: 1,*/
format: 'YYYY-MM-DD HH:mm',
minDate:  new Date('<?php echo date("Y-m-d 00:00:00", strtotime($penalty_date) ); ?>'),
todayHighlight: true,
maxDate:  new Date('<?php echo date("Y-m-d H:i:s");?>') ,
defaultTime :new Date('<?php echo date("Y-m-d H:i:s");?>')
});
 
</script>
<!--Form Validatin Script-->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function($){
    
    $.validator.addMethod("noSpace", function(value, element) {
      return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
    $("#payPenaltyFrm").validate({
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
    error.insertAfter(element.parent());      // radio/checkbox?
    } else if (element.hasClass('select2-hidden-accessible')) {
    error.insertAfter(element.next('span'));  // select2
    element.next('span').addClass('error').removeClass('valid');
    } else {
    error.insertAfter(element);               // default
    }
},
    rules: {
      payment_type: {
        required: true,
        noSpace:true,
      },
      payment_ref_no: {
        required: true,
        noSpace:true,
      },
      received_date: {
        required: true,
        noSpace:true,
        min:'<?php echo date("Y-m-d H:i",strtotime($penalty_date));?>'
      },
      
      penalty_amount:{
        required:true,
        noSpace:true
      }
    //IS_846
    ,bank_name:{
      required:{
        depends: function(element) {
          return $("#payment_type").val()=="1"  || $("#payment_type").val()=="2" ;
        }
      },
      noSpace:true
    }
    //IS_846

    },
    messages: {
      payment_type: {
        required : "Please select Payment Type",
        noSpace: "No space please and don't leave it empty",
      },
      received_date: {
        required : "Please select payment date",
        noSpace: "No space please and don't leave it empty"
      },
      payment_ref_no: {
        required : "Please Enter Cheque Number",
        noSpace: "No space please and don't leave it empty"
      },
      penalty_amount: {
        required : "Please enter penalty amount",
        noSpace: "No space please and don't leave it empty"
      }
    //IS_846
    ,
    bank_name:{
      required:"Please enter bank name"
    }
    //IS_846


    }
    });
  });
//IS_846
$( "#payPenaltyFrm" ).submit(function( event ) {
  
    $( "#payPenaltyFrm" ).submit();

});
//IS_846
function chekOverpaid() {

}
$('#penalty_amount').bind("cut copy paste",function(e) {
     e.preventDefault();
 });
</script>
