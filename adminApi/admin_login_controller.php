<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $today = date('Y-m-d');

    $temDate = date("Y-m-d h:i:s");

    if (isset($send_otp_new) && $send_otp_new=='send_otp_new' && $admin_mobile!='') {

        $admin_mobile=mysqli_real_escape_string($con, $admin_mobile);
        $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND bms_admin_master.admin_active_status=0 AND bms_admin_master.country_code='$country_code'");
        $user_data = mysqli_fetch_array($q);
        if(mysqli_num_rows($q)==1)
        {
            $admin_id = $user_data['admin_id'];
            $society_id = $user_data['society_id'];
            $country_code = $user_data['country_code'];
            if ($admin_mobile=='9687271071' && $society_id==2) {
                $otp=919191;
            } else {
                $digits = 6;
                $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            }
            $m->set_data('otp', $otp);
            $a = array(
                'otp' => $m->get_data('otp'),
            );
            $d->update("bms_admin_master",$a,"admin_id='$admin_id'");
            $society_name = html_entity_decode($user_data['society_name']);
            
            if ($otp_type == 1 && $country_code=="+91") {
                $smsObj->send_voice_otp($admin_mobile, $otp,$country_code);
                $d->add_sms_log($admin_mobile,"Admin App Voice OTP SMS",$society_id,$country_code,1);
                $you_will_receive_your_otp_on_call = $xml->string->you_will_receive_your_otp_on_call;
                $response["message"] = "$you_will_receive_your_otp_on_call";
                $response["is_firebase"] = false;
            }else if ($otp_type==2) {
                $to = $user_data['admin_email'];
                $subject= "OTP Verification for $app_name Admin App.";
                $admin_name = $user_data['admin_name'];
                $msg = $otp;
                include '../apAdmin/mail/adminPanelLoginOTP.php';
                include '../apAdmin/mail.php';
                $response["is_firebase"] = false;
            } else {
                $msgRespone = $smsObj->send_otp_admin($otp,$admin_mobile,$society_name,$country_code);
                if ($msgRespone==true) {
                        $d->add_sms_log($admin_mobile,"Admin OTP SMS",$society_id,$country_code,1);
                    $response["is_firebase"] = false;
                } else {
                    $response["is_firebase"] = true;
                }

            }
            
            if ($user_data['country_code'] != "+91") {
                $response["is_voice_otp"] = false;
            } else {
                $response["is_voice_otp"] = true;
            }
            if ($user_data['admin_email']!='') {
                $response["is_email_otp"] = true;
            } else {
                $response["is_email_otp"] = false;
            }

            $response["message"]="OTP Send Successfully";
            $response["status"]="200";
        
            echo json_encode($response);
        }
        else
        {
            $response["message"]="Mobile number is not registered";
            $response["status"]="201";
            echo json_encode($response);
        }
    } else if (isset($send_otp_new_attendance) && $send_otp_new_attendance=='send_otp_new_attendance' && $admin_mobile!='') {

        $admin_mobile=mysqli_real_escape_string($con, $admin_mobile);
        $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND bms_admin_master.admin_active_status=0 AND bms_admin_master.country_code='$country_code'");
        $user_data = mysqli_fetch_array($q);

        if ($user_data['is_attendance_taking_on'] != '1') {
            $response["message"]="Admin access denied";
            $response["status"]="201";
            echo json_encode($response);
            exit();
        }

        if(mysqli_num_rows($q)==1)
        {
            $admin_id = $user_data['admin_id'];
            $society_id = $user_data['society_id'];
            $country_code = $user_data['country_code'];
            if ($admin_mobile=='9687271071' && $society_id==2) {
                $otp=123456;
            } else {
                $digits = 6;
                $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            }
            $m->set_data('otp', $otp);
            $a = array(
                'otp' => $m->get_data('otp'),
            );
            $d->update("bms_admin_master",$a,"admin_id='$admin_id'");
            $society_name = html_entity_decode($user_data['society_name']);

            if ($otp_type == 1 && $country_code=="+91") {
                $smsObj->send_voice_otp($admin_mobile, $otp,$country_code);
                $d->add_sms_log($admin_mobile,"Face App Voice OTP SMS",$society_id,$country_code,1);
                $you_will_receive_your_otp_on_call = $xml->string->you_will_receive_your_otp_on_call;
                $response["message"] = "$you_will_receive_your_otp_on_call";
                $response["is_firebase"] = false;
            }else if ($otp_type==2) {
                $to = $user_data['admin_email'];
                $subject= "OTP Verification for $app_name Admin App.";
                $admin_name = $user_data['admin_name'];
                $msg = $otp;
                include '../apAdmin/mail/adminPanelLoginOTP.php';
                include '../apAdmin/mail.php';
                $response["is_firebase"] = false;
            } else {
                $msgRespone = $smsObj->send_otp_admin($otp,$admin_mobile,$society_name,$country_code);
                if ($msgRespone==true) {
                        $d->add_sms_log($admin_mobile,"Face App OTP SMS",$society_id,$country_code,1);
                    $response["is_firebase"] = false;
                } else {
                    $response["is_firebase"] = true;
                }

            }
            
            if ($user_data['country_code'] != "+91") {
                $response["is_voice_otp"] = false;
            } else {
                $response["is_voice_otp"] = true;
            }
            if ($user_data['admin_email']!='') {
                $response["is_email_otp"] = true;
            } else {
                $response["is_email_otp"] = false;
            }


            $response["message"]="OTP Send Successfully";
            $response["status"]="200";
        
            echo json_encode($response);

        }
        else
        {
            $response["message"]="Mobile number is not registered";
            $response["status"]="201";
            echo json_encode($response);
        }
    }  else if (isset($verify_otp) && $verify_otp=='verify_otp' && $admin_mobile!='' && $otp!='') {

        $admin_mobile=mysqli_real_escape_string($con, $admin_mobile);
        $otp=mysqli_real_escape_string($con, $otp);
        // $q=$d->select("bms_admin_master","admin_mobile ='$admin_mobile' AND admin_password='$admin_password' AND role_id='2'");
        if ($admin_mobile=='9687271071' && $society_id==2 && $society_id==2) {
             $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND role_master.society_id=society_master.society_id AND bms_admin_master.admin_active_status=0");
        } else {
            $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND bms_admin_master.otp='$otp' AND role_master.society_id=society_master.society_id AND bms_admin_master.admin_active_status=0");

        }

        $user_data = mysqli_fetch_array($q);

        if(mysqli_num_rows($q)==1){

            $country_id  = $user_data['country_id'];
            $society_id  = $user_data['society_id'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                      "getLanguageValuesAdmin=getLanguageValuesAdmin&country_id=$country_id&society_id=$society_id");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'key: bmsapikey'
            ));

            $server_output = curl_exec($ch);

            curl_close ($ch);
            $server_output=json_decode($server_output,true);
            
            $language_id = $server_output['language_id'];

            $response["admin_id"]=$user_data['admin_id'];
            $admin_id = $user_data['admin_id'];

            $tokenOld = $user_data['token'];
            if ($token!=$tokenOld) {
                if ($user_data['device']=='android' && $token!='') {
                $nAdmin->noti_new($society_id,"",$tokenOld,"Logout","Logout","Logout");
                } else {
                $nAdmin->noti_ios_new($society_id,"",$tokenOld,"Logout","Logout","Logout");
                }
            }

            $m->set_data('token', $token);
            $m->set_data('device', $device);
            $m->set_data('device_mac', $device_mac);
            $m->set_data('phone_model', $phone_model);
            $m->set_data('phone_brand', $phone_brand);
            $m->set_data('app_version', $app_version);

            $a = array(
                'token' => $m->get_data('token'),
                'device' => $m->get_data('device'),
                'updated_by' => $m->get_data('device_mac'),
                'phone_model' => $m->get_data('phone_model'),
                'phone_brand' => $m->get_data('phone_brand'),
                'app_version' => $m->get_data('app_version'),
            );
            
            $d->update("bms_admin_master",$a,"admin_id='$admin_id'");

            $ip_address=$d->get_client_ip(); # Save The IP Address
            $browser=$phone_model.'-'.$phone_brand.' ('.$device.')'; # Save The User Agent
            $loginTime=date("d M,Y h:i:sa");//Login Time
            $m->set_data('admin_id',$admin_id);
            $m->set_data('name',$user_data['admin_name']);
            $m->set_data('role_name','Admin');
            $m->set_data('ip_address',$ip_address);
            $m->set_data('browser',$browser);
            $m->set_data('loginTime',$loginTime);
            
            $a1= array ('admin_id'=> $m->get_data('admin_id'),
                'name'=> $m->get_data('name'),
                'role_name'=> $m->get_data('role_name'),
                'ip_address'=> $m->get_data('ip_address'),
                'browser'=> $m->get_data('browser'),
                'loginTime'=> $m->get_data('loginTime'),
            );

            $insert=$d->insert('session_log',$a1); 

            $response["role_id"]=$user_data['role_id'];
            $society_id=$user_data['society_id'];
            $response["society_id"]=$user_data['society_id'];
            $response["admin_name"]=$user_data['admin_name'];
            $response["admin_email"]="";
            $response["admin_mobile"]=$user_data['admin_mobile'];
            $response["country_code"]=$user_data['country_code'];
            $response["admin_address"]="";
            $response["admin_password"]="";
            $response["admin_profile"]="";
            $response["token"]="";
            $response["token_date"]="";
            $response["created_by"]="";
            $response["updated_by"]="";
            $response["created_date"]="";
            $response["updated_date"]="";
            $response["device"]="";
            $response["base_url"]=$base_url;
            $response["adminAppPrivilege"]="";
            $response["otp"]=$user_data['otp'];
            $response["language_id"]=$language_id;

            $qSociety = $d->select("society_master","society_id ='$society_id'");

            $user_data_society = mysqli_fetch_array($qSociety);
            $response["society_address"]=$user_data_society['society_address']."";
            $response["society_latitude"]=$user_data_society['society_latitude']."";
            $response["society_longitude"]=$user_data_society['society_longitude']."";
            $response["society_name"]=$user_data_society['society_name']."";
            $response["socieaty_logo"]=$base_url.'img/society/'.$user_data_society['socieaty_logo']."";

            $response["message"]="login success.";
            $response["status"]="200";
        
            echo json_encode($response);

    
        }else{

            $response["message"]="Invalid OTP";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }  else if (isset($verify_otp_attendance_app) && $verify_otp_attendance_app=='verify_otp_attendance_app' && $admin_mobile!='' && $otp!='') {

        $admin_mobile=mysqli_real_escape_string($con, $admin_mobile);
        $otp=mysqli_real_escape_string($con, $otp);
        // $q=$d->select("bms_admin_master","admin_mobile ='$admin_mobile' AND admin_password='$admin_password' AND role_id='2'");
        if ($admin_mobile=='9726686576'  || $is_firebase == "true") {
             $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND role_master.society_id=society_master.society_id AND bms_admin_master.admin_active_status=0");
        } else {
            $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_mobile='$admin_mobile' AND bms_admin_master.otp='$otp' AND role_master.society_id=society_master.society_id AND bms_admin_master.admin_active_status=0");

        }

        $user_data = mysqli_fetch_array($q);
        $user_data = array_map("html_entity_decode", $user_data);

        if(mysqli_num_rows($q)==1){

            $country_id  = $user_data['country_id'];
            $society_id  = $user_data['society_id'];

           
            $response["admin_id"]=$user_data['admin_id'];
            $admin_id = $user_data['admin_id'];

            

            $m->set_data('token', $token);
            $m->set_data('society_id', $society_id);
            $m->set_data('admin_id', $admin_id);
            $m->set_data('device_model', $model);
            $m->set_data('device_mac', $device_mac);
            $m->set_data('app_version', $app_version);
            $m->set_data('device_latitude', $device_latitude);
            $m->set_data('device_longitude', $device_longitude);
            $m->set_data('device_type', $device_type);
            $m->set_data('last_syc_date', date("Y-m-d H:i:s"));

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'admin_id' => $m->get_data('admin_id'),
                'user_token' => $m->get_data('token'),
                'device_mac' => $m->get_data('device_mac'),
                'device_model' => $m->get_data('device_model'),
                'app_version' => $m->get_data('app_version'),
                'device_latitude' => $m->get_data('device_latitude'),
                'device_longitude' => $m->get_data('device_longitude'),
                'device_type' => $m->get_data('device_type'),
                'last_syc_date' => $m->get_data('last_syc_date'),
            );
            

            $qt=$d->select("face_app_device_master","device_mac='$device_mac'");
            if (mysqli_num_rows($qt)>0) {
                $deviceData= mysqli_fetch_array($qt);
                $face_app_device_id = $deviceData['face_app_device_id'];
                $d->update("face_app_device_master",$a,"device_mac='$device_mac'");
            } else {
                $d->insert("face_app_device_master",$a);
                $face_app_device_id = $con->insert_id;
            }

            $ip_address=$d->get_client_ip(); # Save The IP Address
            $browser=$model. ' (Android)'; # Save The User Agent
            $loginTime=date("d M,Y h:i:sa");//Login Time
            $m->set_data('admin_id',$admin_id);
            $m->set_data('name',$user_data['admin_name']);
            $m->set_data('role_name','Face Admin');
            $m->set_data('ip_address',$ip_address);
            $m->set_data('browser',$browser);
            $m->set_data('loginTime',$loginTime);
            
            $a1= array ('admin_id'=> $m->get_data('admin_id'),
                'name'=> $m->get_data('name'),
                'role_name'=> $m->get_data('role_name'),
                'ip_address'=> $m->get_data('ip_address'),
                'browser'=> $m->get_data('browser'),
                'loginTime'=> $m->get_data('loginTime'),
                'login_type'=> 0,
            );

            $insert=$d->insert('session_log',$a1); 

            $response["face_app_device_id"]=$face_app_device_id;
            $response["role_id"]=$user_data['role_id'];
            $society_id=$user_data['society_id'];
            $response["society_id"]=$user_data['society_id'];
            $response["admin_name"]=$user_data['admin_name'];
            $response["admin_email"]="";
            $response["admin_mobile"]=$user_data['admin_mobile'];
            $response["country_code"]=$user_data['country_code'];
            $response["admin_address"]="";
            $response["admin_password"]="";
            $response["admin_profile"]="";
            $response["token"]="";
            $response["token_date"]="";
            $response["created_by"]="";
            $response["updated_by"]="";
            $response["created_date"]="";
            $response["updated_date"]="";
            $response["device"]="";
            $response["base_url"]=$base_url;
            $response["adminAppPrivilege"]="";
            $response["otp"]=$user_data['otp'];
            $response["language_id"]="1";
            $response["device_location"]=$deviceData['device_location'].'';

            $qSociety = $d->select("society_master","society_id ='$society_id'");
            $user_data_society = mysqli_fetch_array($qSociety);
            $user_data_society = array_map("html_entity_decode", $user_data_society);

            $response["society_address"]=$user_data_society['society_address']."";
            $response["society_latitude"]=$user_data_society['society_latitude']."";
            $response["society_longitude"]=$user_data_society['society_longitude']."";
            $response["society_name"]=$user_data_society['society_name']."";
            $response["company_website"]=$user_data_society['company_website']."";
            if ($user_data_society['socieaty_logo']!='') {
                $response["society_logo"]=$base_url.'img/society/'.$user_data_society['socieaty_logo'];
            } else {
                $response["society_logo"]="";
            }

            $response["message"]="login success.";
            $response["status"]="200";
        
            echo json_encode($response);

    
        }else{
            $response["message"]="Invalid OTP";
            $response["status"]="201";
            echo json_encode($response);

        }

    }  else if(isset($user_logout) && $user_logout=='user_logout'){

        $bq= $d->select("bms_admin_master","admin_id='$admin_id'");
        $adminData= mysqli_fetch_array($bq);
        $phone_model = $adminData['phone_model'];
        $phone_brand = $adminData['phone_brand'];
        $device = $adminData['device'];
        
        $m->set_data('token','');
      
        $a = array(
            'token'=>$m->get_data('token')
        );

        $qdelete = $d->update("bms_admin_master",$a,"admin_id='$admin_id'");
        
        if($qdelete==TRUE){

            $ip_address=$d->get_client_ip(); # Save The IP Address
            $browser=$phone_model.'-'.$phone_brand.' ('.$device.')'; # Save The User Agent
            $loginTime=date("d M,Y h:i:sa");//Login Time
            $m->set_data('admin_id',$admin_id);
            $m->set_data('name',$adminData['admin_name']);
            $m->set_data('role_name','Admin');
            $m->set_data('ip_address',$ip_address);
            $m->set_data('browser',$browser);
            $m->set_data('loginTime',$loginTime);
            
            $a1= array ('admin_id'=> $m->get_data('admin_id'),
                'name'=> $m->get_data('name'),
                'role_name'=> $m->get_data('role_name'),
                'ip_address'=> $m->get_data('ip_address'),
                'browser'=> $m->get_data('browser'),
                'loginTime'=> $m->get_data('loginTime'),
                'login_type'=> 1,
            );

            $insert=$d->insert('session_log',$a1);

            $response["message"]="Logout Sucessfully";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Something wrong..! Try Again";
            $response["status"]="201";
            echo json_encode($response);
        }
     
    } else if(isset($face_app_logout) && $face_app_logout=='face_app_logout'){

        $bq= $d->selectRow("bms_admin_master.admin_name,face_app_device_master.*","bms_admin_master,face_app_device_master","bms_admin_master.admin_id=face_app_device_master.admin_id AND bms_admin_master.admin_id='$admin_id' AND face_app_device_master.face_app_device_id='$face_app_device_id'");
        $adminData= mysqli_fetch_array($bq);
        $phone_model = $adminData['device_model'];
        $device = "Android";
        
        $m->set_data('user_token','');
      
        $a = array(
            'user_token'=>$m->get_data('user_token')
        );

        $qdelete = $d->update("face_app_device_master",$a,"face_app_device_id='$face_app_device_id'");
        
        if($qdelete==TRUE){

            $ip_address=$d->get_client_ip(); # Save The IP Address
            $browser=$phone_model.' ('.$device.')'; # Save The User Agent
            $loginTime=date("d M,Y h:i:sa");//Login Time
            $m->set_data('admin_id',$admin_id);
            $m->set_data('name',$adminData['admin_name']);
            $m->set_data('role_name','Face Admin');
            $m->set_data('ip_address',$ip_address);
            $m->set_data('browser',$browser);
            $m->set_data('loginTime',$loginTime);
            
            $a1= array ('admin_id'=> $m->get_data('admin_id'),
                'name'=> $m->get_data('name'),
                'role_name'=> $m->get_data('role_name'),
                'ip_address'=> $m->get_data('ip_address'),
                'browser'=> $m->get_data('browser'),
                'loginTime'=> $m->get_data('loginTime'),
                'login_type'=> 1,
            );

            $insert=$d->insert('session_log',$a1);

            $response["message"]="Logout Sucessfully";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Something wrong..! Try Again";
            $response["status"]="201";
            echo json_encode($response);
        }
     
    }  else{
        $response["message"]="wrong tag...";
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>