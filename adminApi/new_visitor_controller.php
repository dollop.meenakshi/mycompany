<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
        $CURRENT_TIMESTAMP=date('Y-m-d H:i:s');
        $last24Hours = date("Y-m-d H:i:s", strtotime('-6 hours', time()));

        $today = date('Y-m-d');

        $response = array();
        extract(array_map("test_input" , $_POST));

        if (isset($society_id)) {
            $qss=$d->selectRow("visitor_mobile_number_show_gatekeeper,auto_reject_vistor_minutes,visitor_otp_verify","society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];  
            $auto_reject_vistor_minutes= $societyData['auto_reject_vistor_minutes'];      
            $visitor_otp_verify= $societyData['visitor_otp_verify'];      
        }

        if ($_POST['addAllVisitor'] == "addAllVisitor"  && $society_id!='') {

            if ($country_code!="") {
                $appendQuery = " AND country_code='$country_code'";
            }

            if ($visitor_type == 0 || $visitor_type == 1) {
                $visit_date = date("Y-m-d");
                $visit_time = date("H:i:s");
                $visitor_name = html_entity_decode($visitor_name);

                $qSociety = $d->select("society_master","society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qSociety);
                $society_name = $societyData['society_name'];
                $digits = 4;
                $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
                $m->set_data('otp', $otp);
                
                $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' AND visitors_master.visit_date='$visit_date'AND visitor_status!='3'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  AND visitor_status!='3'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' AND visitor_status!='3'  $appendQuery");

                if (mysqli_num_rows($qq)>0) {
                    $oldData=mysqli_fetch_array($qq);
                    if ($oldData['visitor_type']==1 && $oldData['visitor_status']==1) {
                        $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                    } else if ($oldData['visitor_type']==4) {
                        $response["message"] = ''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                    } else if ($oldData['visitor_status']==2) {
                        $response["message"] = ''.$xml->string->mobileNumberisEnterinBuilding;
                    } else {
                        $response["message"] = ''.$xml->string->mobileAlradyRegister;
                    }
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }

                if ($isReinvite=='true' && $visitor_profile_photo=='') {
                    $profile_name = $visitor_profile_photo_name;
                    $notyUrl= $base_url.'img/visitor/'.$profile_name;
                } else {
                    if ($visitor_profile_photo != "") {
                        $ddd = date("ymdhi");
                        $profile_name = "Visitor_" . $visitor_mobile .$ddd. '.jpeg';
                        define('UPLOAD_DIR', '../img/visitor/');
                        $img = $visitor_profile_photo;
                        $img = str_replace('data: img/app/png;base64,', '', $img);
                        $img = str_replace(' ', '+', $img);
                        $data = base64_decode($img);
                        $file = UPLOAD_DIR . $profile_name;
                        $success = file_put_contents($file, $data);

                        $notyUrl= $base_url.'img/visitor/'.$profile_name;
                    } else {
                        $profile_name = "visitor_default.png";
                        $notyUrl= $base_url.'img/visitor/visitor_default.png';
                    }
                }

                $userIdAry=explode(",",$user_id);

                $userIdApporvalReq= array();
                $unitIdApporvalReq= array();
                for ($i=0; $i <count($userIdAry) ; $i++) { 
                    $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$userIdAry[$i]'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $deliveryApproval=$data_notification['visitor_approved'];
                    
                    array_push($userIdApporvalReq, $data_notification['user_id']);
                    array_push($unitIdApporvalReq, $data_notification['unit_id']);
                }

                $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitor_sub_type_id'");
                $vistLogo=mysqli_fetch_array($fd);
                
                if ($vistLogo['visitor_sub_image']!='') {
                    $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                } else {
                    $visit_logo="";
                }

                if ($number_of_visitor==0) {
                    $number_of_visitor =1;
                }

                $globleVariblePrntId = "";

                $currentDate = strtotime($CURRENT_TIMESTAMP);
                $futureDate = $currentDate+(60*$auto_reject_vistor_minutes);
                $valid_till_date = date("Y-m-d H:i:s", $futureDate);

                //  Apporval  req / not req

                $tempPrntIdAry=array();
                $tempUnitIdAry=array();
                $approve_button_status = 0;

                for ($i=0; $i <count($userIdApporvalReq) ; $i++) { 

                   
                    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$userIdApporvalReq[$i]'");

                    $data_notification=mysqli_fetch_array($qUserToken);
                    $visitor_approved=$data_notification['visitor_approved'];
                    $deliveryApproval=$data_notification['Delivery_cab_approval'];
                    $sos_user_token=$data_notification['user_token'];
                    $user_full_name=$data_notification['user_full_name'];
                    $device=$data_notification['device'];
                    $user_mobile=$data_notification['user_mobile'];
                    $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                    $visitor_approved=$data_notification['visitor_approved'];

                    if ($data_notification['block_id']=="") {
                        $block_id=$block_id;
                        $unit_id=$unit_id;
                        $floor_id=$floor_id;
                        $visitor_type = 5;
                    } else {
                        $block_id=$data_notification['block_id'];
                        $unit_id=$data_notification['unit_id'];
                        $floor_id=$data_notification['floor_id'];
                    }

                   
                    $visitor_name= $visitor_name.'';

                    $visit_time = date("H:i:s");
                    $visitor_type =0;

                    $m->set_data('parent_visitor_id', $visitorId);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('user_id', $userIdApporvalReq[$i]);
                    $m->set_data('emp_id', $emp_id);
                    $m->set_data('visitor_type', $visitor_type);
                    $m->set_data('visit_from', $visit_from);
                    $m->set_data('visitor_name', $visitor_name);
                    $m->set_data('visitor_mobile', $visitor_mobile);
                    $m->set_data('country_code', $country_code);
                    $m->set_data('number_of_visitor', $number_of_visitor);
                    $m->set_data('visit_date', $visit_date);
                    $m->set_data('visit_time', $visit_time);
                    $m->set_data('valid_till_date', $valid_till_date);
                    $m->set_data('visitor_added_from_face_app', "1");
                    $visitor_status =2;
                    $m->set_data('visitor_status', '2');
                    $m->set_data('approve_by_id', '');
                    
                    $m->set_data('visiting_reason', $visiting_reason);
                    $m->set_data('qr_code_master_id', $qrCode);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('visitor_vehicle_type', $vehicle_type);
                    $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                    $m->set_data('visiting_apartment', $block_unit_name);
                    
                    $a = array(
                        // 'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'floor_id' => $m->get_data('floor_id'),
                        'block_id' => $m->get_data('block_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'emp_id' => $m->get_data('emp_id'),
                        'visitor_type' => $m->get_data('visitor_type'),
                        'visit_from' => $m->get_data('visit_from'),
                        'visitor_name' => $m->get_data('visitor_name'),
                        'visitor_mobile' => $m->get_data('visitor_mobile'),
                        'country_code' => $m->get_data('country_code'),
                        'visitor_profile' => $profile_name,
                        'number_of_visitor' => $m->get_data('number_of_visitor'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'valid_till_date' => $m->get_data('valid_till_date'),
                        'visitor_added_from_face_app' => $m->get_data('visitor_added_from_face_app'),
                        'visitor_status' => $m->get_data('visitor_status'),
                        'otp' => $m->get_data('otp'),
                        'visiting_reason' => $m->get_data('visiting_reason'),
                        'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                        'parking_id'=>$m->get_data('parking_id'),
                        'visitor_vehicle_type'=>$m->get_data('visitor_vehicle_type'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                        'visiting_apartment'=>$m->get_data('visiting_apartment'),
                        'approve_by_id'=>$m->get_data('approve_by_id'),
                    );


                    $quserDataUpdate = $d->insert("visitors_master", $a);
                    $parent_visitor_id = $con->insert_id;
                    $visitorId = $parent_visitor_id;
                    if ($i==0) {
                        $aPrt = array(
                            'parent_visitor_id' => $parent_visitor_id,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                        $globleVariblePrntId=$parent_visitor_id;
                    } else {
                        $aPrt = array(
                            'parent_visitor_id' => $globleVariblePrntId,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                    }

                    if ($quserDataUpdate == true) {

                        $visit_time11= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));
                       
                        $notTitle= "Guest $visitor_name Arrived at Reception";
                        $notDesc= "Will meet you very soon."; 
                        if ($device=='android') {
                           $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                        }
                            
                   
                        $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$userIdApporvalReq[$i],
                            'notification_title'=>$notTitle,
                            'notification_desc'=>$notDesc,
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'Visitor',
                            'notification_logo'=>'Visitors-mgmtxxxhdpi.png',
                        );
                          
                        $d->insert("user_notification",$notiAry);
                    }                             
                } //for loop end

                $aGloble = array(
                    'approve_button_status' => $approve_button_status,
                );
                $d->update("visitors_master", $aGloble,"parent_visitor_id='$globleVariblePrntId'");

                if ($societyData['visitor_otp_verify']==1) {
                    
                    if ($country_code=='') {
                        $country_code = "+91";
                    }
                    $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                    $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);
                }

                $response["message"] =''.$xml->string->addVisitorSucess ;
                $response["status"] = "200";
                echo json_encode($response);
            }else if ($visitor_type == 5){
                $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' $appendQuery ");

                if (mysqli_num_rows($qq)>0) {
                    $oldData=mysqli_fetch_array($qq);
                    if ($oldData['visitor_type']==1 && $oldData['visitor_status']==0) {
                        $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                    } else if ($oldData['visitor_type']==4) {
                        $response["message"] =''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                    } else if ($oldData['visitor_status']==2) {
                        $response["message"] =''.$xml->string->mobileNumberisEnterinBuilding;
                    } else {
                        $response["message"] = ''.$xml->string->mobileAlradyRegister;
                    }
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }

                $visit_date = date("Y-m-d");
                $visit_time = date("H:i:s");

                if ($visitor_profile_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Visitor_" . $visitor_mobile .$ddd. '.png';
                    define('UPLOAD_DIR', '../img/visitor/');
                    $img = $visitor_profile_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notyUrl= $base_url.'img/visitor/'.$profile_name;
                } else {
                    $profile_name = "visitor_default.png";
                    $notyUrl='';
                }
                
                $qSociety = $d->select("society_master","society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qSociety);
                $society_name = $societyData['society_name'];
                $digits = 4;
                $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
                $m->set_data('otp', $otp);

                $visitor_name = html_entity_decode($visitor_name);
                if ($number_of_visitor=="") {
                    $number_of_visitor =1;
                }

                $currentDate = strtotime($CURRENT_TIMESTAMP);
                $futureDate = $currentDate+(60*$auto_reject_vistor_minutes);
                $valid_till_date = date("Y-m-d H:i:s", $futureDate);

                $m->set_data('parent_visitor_id', $visitor_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('floor_id', 0);
                $m->set_data('block_id',0);
                $m->set_data('unit_id', 0);
                $m->set_data('user_id', 0);
                $m->set_data('emp_id', 0);
                $m->set_data('visitor_type', 5);
                $m->set_data('visit_from', $visit_from);
                $m->set_data('visitor_name', $visitor_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('number_of_visitor',$number_of_visitor);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                $m->set_data('valid_till_date', $valid_till_date);
                $m->set_data('visitor_added_from_face_app', "1");
                $m->set_data('visitor_status', '2');
                $m->set_data('visiting_reason', $visiting_reason);
                $m->set_data('vehicle_no', $vehicle_no);
                $m->set_data('visitor_vehicle_type', $vehicle_type);
                $m->set_data('parking_id', $parking_id);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                $m->set_data('otp', $otp);
                $m->set_data('approve_button_status', 1);                

                $a = array(
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'block_id' => $m->get_data('block_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'emp_id' => $m->get_data('emp_id'),
                    'visitor_type' => $m->get_data('visitor_type'),
                    'visit_from' => $m->get_data('visit_from'),
                    'visitor_name' => $m->get_data('visitor_name'),
                    'visitor_mobile' => $m->get_data('visitor_mobile'),
                    'country_code' => $m->get_data('country_code'),
                    'visitor_profile' => $profile_name,
                    'number_of_visitor' => $m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'valid_till_date' => $m->get_data('valid_till_date'),
                    'visitor_added_from_face_app' => $m->get_data('visitor_added_from_face_app'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'visiting_reason' => $m->get_data('visiting_reason'),
                    'vehicle_no'=>$m->get_data('vehicle_no'),
                    'visitor_vehicle_type'=>$m->get_data('visitor_vehicle_type'),
                    'parking_id'=>$m->get_data('parking_id'),
                    'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                    'otp'=>$m->get_data('otp'),
                    'approve_button_status'=>$m->get_data('approve_button_status'),
                    'entry_time'=>date("Y-m-d H:i:s"),
                );

                $q = $d->insert("visitors_master", $a);
                $parent_visitor_id = $con->insert_id;

                if ($q>0) {
                    $aPrnt = array(
                        'parent_visitor_id' => $parent_visitor_id,
                    );
                    
                    $d->update("visitors_master", $aPrnt,"visitor_id='$parent_visitor_id'");
                    
                    if ($societyData['visitor_otp_verify']==1) {
                        
                        if ($country_code=='') {
                            $country_code = "+91";
                        }

                        $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                        $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);  
                    }

                    $response["parent_visitor_id"] =''.$xml->string->addVisitorSucess;
                    $response["message"] =''.$xml->string->addVisitorSucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();

                } else {
                    $response["message"] =''.$xml->string->somthingWrong;
                    $response["status"] = "2001";
                    echo json_encode($response);
                    exit();
                }
            }else{


                $visitor_name = html_entity_decode($visitor_name);
                $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' AND visitors_master.visit_date='$visit_date'AND visitor_status!='3' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  AND visitor_status!='3' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' AND visitor_status!='3' $appendQuery");
                if (mysqli_num_rows($qq)>0) {
                    $oldData=mysqli_fetch_array($qq);
                    if ($oldData['visitor_type']==1 && $oldData['visitor_status']==1) {
                        $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                    } else if ($oldData['visitor_type']==4) {
                        $response["message"] = ''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                    } else if ($oldData['visitor_status']==2) {
                        $response["message"] = ''.$xml->string->mobileNumberisEnterinBuilding;
                    } else {
                        $response["message"] = ''.$xml->string->mobileAlradyRegister;
                    }
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }


                $qSociety = $d->selectRow("society_name,visitor_otp_verify","society_master","society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qSociety);
                $society_name = $societyData['society_name'];
                $digits = 4;
                $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
                $m->set_data('otp', $otp);


                $visit_date = date("Y-m-d");
                $visit_time = date("H:i:s");
                if ($visitor_profile_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Visitor_" . $visitor_mobile.$ddd . '.png';
                    define('UPLOAD_DIR', '../img/visitor/');
                    $img = $visitor_profile_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notyUrl= $base_url.'img/visitor/'.$profile_name;
                } else {
                    if ($profile_img_name!="") {
                        $profile_name = $profile_img_name;
                    } else if($visitor_type==2) {
                        $profile_name = "delivery_boy.png";
                    } elseif ($visitor_type==3) {
                        $profile_name = "taxi.png";
                    }elseif ($visitor_type==6) {
                        $profile_name = "vendor.png";
                    } else {
                        $profile_name = "visitor_default.png";
                    }
                    $notyUrl=$base_url.'img/visitor/'.$profile_name;
                }


                $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitor_sub_type_id'");
                $vistLogo=mysqli_fetch_array($fd);
                if ($vistLogo['visitor_sub_image']!='') {
                    $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                } else {
                    $visit_logo="";
                }

                $userIdAry=explode(",",$user_id);

                $userIdApporvalReq= array();
                $unitIdApporvalReq= array();
                for ($i=0; $i <count($userIdAry) ; $i++) { 
                    $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$userIdAry[$i]'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $deliveryApproval=$data_notification['Delivery_cab_approval'];
                        array_push($userIdApporvalReq, $data_notification['user_id']);
                        array_push($unitIdApporvalReq, $data_notification['unit_id']);
                }

                $visit_from = $visit_from.'';

                $currentDate = strtotime($CURRENT_TIMESTAMP);
                $futureDate = $currentDate+(60*$auto_reject_vistor_minutes);
                $valid_till_date = date("Y-m-d H:i:s", $futureDate);

                // approval not req

                $globleVariblePrntId = "";

               
                //  Apporval  req / not req for cab & delivry

                $tempPrntIdAry=array();
                $tempUnitIdAry=array();
                $approve_button_status = 0;

                for ($i=0; $i <count($userIdApporvalReq) ; $i++) { 

                   
                    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$userIdApporvalReq[$i]'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $visitor_approved=$data_notification['visitor_approved'];
                    $deliveryApproval=$data_notification['Delivery_cab_approval'];
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                    $user_full_name=$data_notification['user_full_name'];
                    $user_mobile=$data_notification['user_mobile'];
                    $deliveryApproval=$data_notification['Delivery_cab_approval'];

                    if ($data_notification['floor_id']=='') {
                        $m->set_data('floor_id', $floor_id);
                        $m->set_data('block_id', $block_id);
                        $m->set_data('unit_id', $unit_id);
                        $m->set_data('visitor_type', 5);
                    } else if ($data_notification['floor_id']!="") {
                        $m->set_data('floor_id', $data_notification['floor_id']);
                        $m->set_data('block_id', $data_notification['block_id']);
                        $m->set_data('unit_id', $data_notification['unit_id']);
                        $m->set_data('visitor_type', $visitor_type);
                    } else {
                        $m->set_data('floor_id', $floor_id);
                        $m->set_data('block_id', $block_id);
                        $m->set_data('unit_id', $unit_id);
                        $m->set_data('visitor_type', 5);
                    }


                    $m->set_data('parent_visitor_id', $visitorId);
                    $m->set_data('society_id', $society_id);
                    // $m->set_data('floor_id', $data_notification['floor_id']);
                    // $m->set_data('block_id', $data_notification['block_id']);
                    // $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('user_id', $userIdApporvalReq[$i]);
                    $m->set_data('emp_id', $emp_id);
                    $m->set_data('visit_from', $visit_from);
                    $m->set_data('visitor_name', $visitor_name);
                    $m->set_data('visitor_mobile', $visitor_mobile);
                    $m->set_data('country_code', $country_code);
                    $m->set_data('number_of_visitor', $number_of_visitor);
                    $m->set_data('visit_date', $visit_date);
                    $m->set_data('visit_time', $visit_time);
                    $m->set_data('valid_till_date', $valid_till_date);
                    $m->set_data('visitor_added_from_face_app', "1");

                    $m->set_data('visitor_status', '2');
                    $m->set_data('approve_by_id', $userIdApporvalReq[$i]);
                    $approve_button_status = 2;
                   
                    
                    $m->set_data('number_of_visitor', 1);
                    $m->set_data('visiting_reason', $visiting_reason);
                    $m->set_data('qr_code_master_id', $qrCode);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('visitor_vehicle_type', $vehicle_type);
                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                    $m->set_data('visiting_apartment', $block_unit_name);

                    $a = array(
                        'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'floor_id' => $m->get_data('floor_id'),
                        'block_id' => $m->get_data('block_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'emp_id' => $m->get_data('emp_id'),
                        'visitor_type' => $m->get_data('visitor_type'),
                        'visit_from' => $m->get_data('visit_from'),
                        'visitor_name' => $m->get_data('visitor_name'),
                        'visitor_mobile' => $m->get_data('visitor_mobile'),
                        'country_code' => $m->get_data('country_code'),
                        'visitor_profile' => $profile_name,
                        'number_of_visitor' => $m->get_data('number_of_visitor'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'valid_till_date' => $m->get_data('valid_till_date'),
                        'visitor_added_from_face_app' => $m->get_data('visitor_added_from_face_app'),
                        'visitor_status' => $m->get_data('visitor_status'),
                        'otp' => $m->get_data('otp'),
                        'visiting_reason' => $m->get_data('visiting_reason'),
                        'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                        'visitor_vehicle_type'=>$m->get_data('visitor_vehicle_type'),
                        'parking_id'=>$m->get_data('parking_id'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                        'visiting_apartment'=>$m->get_data('visiting_apartment'),
                        'approve_by_id'=>$m->get_data('approve_by_id'),
                    );


                    $quserDataUpdate = $d->insert("visitors_master", $a);
                    $parent_visitor_id = $con->insert_id;
                    $visitorId = $parent_visitor_id;
                    if ($i==0) {
                        $aPrt = array(
                            'parent_visitor_id' => $parent_visitor_id,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                       $globleVariblePrntId=$parent_visitor_id;
                    } else {
                        $aPrt = array(
                            'parent_visitor_id' => $globleVariblePrntId,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                    }
                    if ($quserDataUpdate == true) {

                        $visit_timeNotificatin= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));

                            if($visitor_type==2) {
                                $notification_logo= "delivery_boy.png";
                                $notTitle= "Delivery boy Arrived at Reception Area"; 
                                $notDesc= "Please collect your parcel from reception area"; 
                                if ($device=='android') {
                                   $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                                }  else if($device=='ios') {
                                   $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                                }
                               
                            } else if($visitor_type==3) {
                                $notification_logo= "taxi.png";
                                $notTitle= "Cab Driver Arrived at Reception Area"; 
                                $notDesc= ""; 
                                if ($device=='android') {
                                   $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                                }  else if($device=='ios') {
                                   $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                                }
                            } 

                         $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$userIdApporvalReq[$i],
                            'notification_title'=>$notTitle,
                            'notification_desc'=>$notDesc,
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>"Visitor",
                            'notification_logo'=>"Visitors-mgmtxxxhdpi.png"
                        );
                          
                        $d->insert("user_notification",$notiAry);
                    }  
                } //for loop end

                $aGloble = array(
                    'approve_button_status' => $approve_button_status,
                );
                $d->update("visitors_master", $aGloble,"parent_visitor_id='$globleVariblePrntId'");
                

                if ($societyData['visitor_otp_verify']==1) {
                    
                    if ($country_code=='') {
                      $country_code = "+91";
                    }
                    $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                    $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);
                }

                $response["message"] =''.$xml->string->addVisitorSucess;
                $response["status"] = "200";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
