<?php
include_once 'lib.php'; 

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    $societyLngName=  $xml->string->society;

    if ($key==$keydb ) { 
    
        $response = array();
        extract(array_map("test_input" , $_POST));

        $currentDate = date('Y-m-d');

        if($_POST['getAttendanceData']=="getAttendanceData" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $location_name = $device_location;

            $dayNumber = date("w"); 

            $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
            $userData = mysqli_fetch_array($checkShift);
            $shift_time_id = $userData['shift_time_id'];
            $unit_id = $userData['unit_id'];
            $block_id = $userData['block_id'];
            $floor_id = $userData['floor_id'];
            $user_work_report_on = $userData['user_work_report_on'];
            $user_full_name = $userData['user_full_name'];
            $user_profile_pic = $userData['user_profile_pic'];
            $user_designation = $userData['user_designation'];
            $user_token = $userData['user_token'];
            $track_user_location = $userData['track_user_location'];
            $device = $userData['device'];
            $user_first_name = $userData['user_first_name'];


            if ($checkShift == true && $shift_time_id == '0') {
                $response["message"] = "Your shift is missing, please contact to admin";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");

            if (mysqli_num_rows($shiftQ) > 0) {
                $shiftData = mysqli_fetch_array($shiftQ);

                $perdayHours = $shiftData['per_day_hour'];
                $shift_start_time = $shiftData['shift_start_time'];
                $per_day_hour = $shiftData['per_day_hour'];
                $shift_end_time = $shiftData['shift_end_time'];
                $late_time_start = $shiftData['late_time_start'];
                $early_out_time = $shiftData['early_out_time'];
                $half_day_time_start = $shiftData['half_day_time_start'];
                $halfday_before_time = $shiftData['halfday_before_time'];
                $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                $shift_type = $shiftData['shift_type'];
                $maximum_in_out = $shiftData['maximum_in_out'];
                $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];
                $max_shift_hour = $shiftData['max_shift_hour'];
                $max_hour = $shiftData['max_hour'];
                $max_punch_out_time = $shiftData['max_punch_out_time'];
                $has_altenate_week_off = $shiftData['has_altenate_week_off'];
                $alternate_week_off = $shiftData['alternate_week_off'];
                $alternate_weekoff_days = $shiftData['alternate_weekoff_days'];

                if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                    $isMaxTimeSet = true;
                } else if ($max_shift_hour != '' && $max_shift_hour!='00:00:00') {
                    $avarageDay = round($max_shift_hour/24);
                    $isMaxHoursSet = true;
                }

                if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                    $checkNextDayDate = true;
                }

                if ($max_shift_hour != '' && $max_shift_hour != '00:00:00') {
                    $parts = explode(':', $max_shift_hour);
                    $max_shift_hour_minutes = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                }else{
                    $max_shift_hour_minutes = 0;
                }
               

                $parts1 = explode(':', $per_day_hour);
                $per_day_hour_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);

                $shiftData = array(
                    "shift_time_id" => $shift_time_id,
                    "per_day_hour" => $per_day_hour,
                    "shift_start_time" => $shift_start_time,
                    "shift_end_time" => $shift_end_time,
                );

                $response['shift_data'] = $shiftData;
            }

            $alternate_week_off = explode(",", $alternate_week_off);
            $alternate_weekoff_days = explode(",", $alternate_weekoff_days);

            $is_extra_day = "0"; 

            if ((isset($alternate_week_off) && in_array($dayNumber, $alternate_week_off)) || (isset($alternate_weekoff_days) && in_array($dayNumber, $alternate_weekoff_days))) {
                $is_extra_day = "1";
            }

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $punchOUTTime = date('H:i:s');
            $punchOUTDate = date('Y-m-d');

            $attendance_date_start = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            $holidayQry = $d->count_data_direct("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_status = 0 AND holiday_start_date = '$punchInDate'");

            if ($holidayQry > 0) {
                $is_extra_day = "1";
            }


            if ($shift_type == "Night") {

                $attendance_date_start = date('Y-m-d',strtotime('-1 days'));
                $aDt = date('Y-m-d');

                if ($is_multiple_punch_in == 0) {
                    $appQry = " AND (attendance_master.attendance_date_end = '0000-00-00' OR attendance_master.attendance_date_end = '$punchInDate')";
                }

                if ($isMaxHoursSet==true) {

                    if ($avarageDay<1) {
                        $avarageDay =1;
                    }

                    $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id
                        AND (attendance_master.attendance_date_start BETWEEN  '$date2DayPre' AND '$aDt') 
                        AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");                  

                } else if ($isMaxTimeSet==true) {
                    $date2DayPre = date("Y-m-d",strtotime("-1 days"));
                    

                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id
                        AND (attendance_master.attendance_date_start = '$attendance_date_start' OR attendance_master.attendance_date_start = '$date2DayPre')  AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                } else {
                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id
                        AND (attendance_master.attendance_date_start BETWEEN '$attendance_date_start' AND '$aDt') AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");
                }
            }else{


                $attendance_date_start = date('Y-m-d');

                if ($is_multiple_punch_in == 0) {
                    $appQry = " AND (attendance_master.attendance_date_end = '0000-00-00' OR attendance_master.attendance_date_end = '$attendance_date_start')";
                }

                if ($isMaxHoursSet==true) {

                    if ($avarageDay<1) {
                        $avarageDay =1;
                    }
                    $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id 
                        AND (attendance_master.attendance_date_start BETWEEN  '$date2DayPre' AND '$attendance_date_start') AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                } else if ($isMaxTimeSet==true) {
                    $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id
                        AND (attendance_master.attendance_date_start = '$attendance_date_start' OR attendance_master.attendance_date_start = '$date2DayPre')  AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                } else {
                    $getTodayAttendace = $d->selectRow("
                        attendance_master.*,
                        users_master.user_full_name,
                        users_master.block_id,
                        users_master.floor_id,
                        users_master.user_designation,
                        users_master.user_profile_pic,
                        work_report_master.work_report_id
                        ","
                        users_master,
                        attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                        ","
                        attendance_master.user_id = '$user_id' 
                        AND users_master.user_id = attendance_master.user_id
                        AND attendance_master.attendance_date_start = '$attendance_date_start' AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                }
            }


            if ($inOut == 1) {
               
                if (mysqli_num_rows($getTodayAttendace) > 0) {

                    $attData = mysqli_fetch_array($getTodayAttendace);

                    // Code for Punch Out
                   
                    $attendance_id = $attData['attendance_id'];
                    $extraDayAtt = $attData['is_extra_day'];
                    $attendance_date_start = $attData["attendance_date_start"];
                    $attendanceEndDATE = $attData["attendance_date_end"];
                    $punch_in_time = $attData["punch_in_time"];
                    $todayLateIn  = $attData['late_in'];

                    if (($attendanceEndDATE != '' && $attendanceEndDATE != '0000-00-00')) {
                        $response["message"] = "You have already punched out";               
                        $response["status"] = "202";
                        $response['user_full_name'] =$userData["user_full_name"];
                        $response['society_id'] =$userData["society_id"];
                        $response['block_id'] =$userData["block_id"];
                        $response['floor_id'] =$userData["floor_id"];
                        $response['unit_id'] =$userData["unit_id"];
                        $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                        $response['user_designation'] =$userData["user_designation"];
                        echo json_encode($response);
                        exit();
                    }
                   
                    $multiple_punch_in_out_data = $attData['multiple_punch_in_out_data'];
                    $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);
                    
                    $pDate = $attStartDate." ".$punch_in_time;

                    $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                    $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                    $date_a = new DateTime($inTimeAMPM);
                    $date_b = new DateTime($dateTimeCurrent);

                    $interval = $date_a->diff($date_b);
                   
                    $hours = $interval->format('%h');
                    $minutes = $interval->format('%i');
                    $sec = $interval->format('%s');

                    if ($sec < 45) {
                       $sec += 5;
                    }

                    $finalTime =  $attendance_date_start." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                    $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);


                    $datePunchIN = $attendance_date_start ." ".$punch_in_time;
                    $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                    $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

                    if ($datePunchOUT < $datePunchIN) {
                        $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                    }

                    $punch_image = "";

                    if ($_FILES["punch_image"]["tmp_name"] != null) {
                        // code...
                        $file11=$_FILES["punch_image"]["tmp_name"];
                        $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                        $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                        if(in_array($extId,$extAllow)) {
                            $temp = explode(".", $_FILES["punch_image"]["name"]);
                            $punch_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                            move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                        }
                    }

                    if ($early_out_time != '') {
                        $partEO = explode(':', $early_out_time);
                        $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
                    }else{
                        $min = 0;
                    }

                    $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                    $defaultOutTimeTemp = $punchOUTDate." ".$shift_end_time;
                    $strMin = "-".$min." minutes";
                    $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                    $qryLeave = $d->select("leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                    $hasLeave = false;

                    if (mysqli_num_rows($qryLeave) > 0) {

                        $hasLeave = true;

                        $leaveData = mysqli_fetch_array($qryLeave);

                        $leave_id = $leaveData['leave_id'];
                        $leave_type_id = $leaveData['leave_type_id'];
                        $paid_unpaid = $leaveData['paid_unpaid'];
                        $leave_reason = $leaveData['leave_reason'];
                        $leave_day_type = $leaveData['leave_day_type'];
                        $half_day_session = $leaveData['half_day_session'];
                        $auto_leave_reason = $leaveData['auto_leave_reason'];
                    }

                    // Total Working Hour Count

                    $parts = explode(':', $perdayHours);
                    $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                    $halfDayHourMinute = $perDayHourMinute/2;
                    $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;     

                    $dataArray = array();   

                    if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {


                        for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                            if($multiplePunchDataArray[$i]["punch_out_date"] == '' 
                                || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){

                                $totalHours1 = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$punchOUTDate,
                                        $multiplePunchDataArray[$i]["punch_in_time"],$punchOUTTime);

                                $timeHr = explode(':', $totalHours1.":00");
                                $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                                $finalTotalMinute = $finalTotalMinute + $totalMinutes;

                                $last_punch_in_date = $multiplePunchDataArray[$i]["punch_in_date"].'';
                                $last_punch_in_time = $multiplePunchDataArray[$i]["punch_in_time"].'';


                                $oldAry = array(
                                    "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"].'',
                                    "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"].'',
                                    "punch_out_date" => $punchOUTDate.'',
                                    "punch_out_time" =>$punchOUTTime.'',
                                    "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"].'',
                                    "punch_out_image" => $punch_image.'',
                                    "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"].'',
                                    "location_name_out" => $location_name.'',
                                    "working_hour" => $totalHours1.'',
                                    "working_hour_minute" => $totalMinutes.'',
                                );

                                array_push($dataArray,$oldAry);
                            }else{
                                $oldAry = array(); 
                                $oldAry['punch_in_date'] = $multiplePunchDataArray[$i]["punch_in_date"].'';
                                $oldAry['punch_in_time'] = $multiplePunchDataArray[$i]["punch_in_time"].'';
                                $oldAry['punch_out_date'] = $multiplePunchDataArray[$i]["punch_out_date"].'';
                                $oldAry['punch_out_time'] = $multiplePunchDataArray[$i]["punch_out_time"].'';
                                $oldAry['punch_in_image'] = $multiplePunchDataArray[$i]["punch_in_image"].'';
                                $oldAry['punch_out_image'] = $multiplePunchDataArray[$i]["punch_out_image"].'';
                                $oldAry['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"].'';
                                $oldAry['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"].'';
                                $oldAry['working_hour'] = $multiplePunchDataArray[$i]["working_hour"].'';
                                $oldAry['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';
                                
                                $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$i]["working_hour_minute"].'';
                                array_push($dataArray,$oldAry);
                            }
                        }  

                        $multPunchDataJson = json_encode($dataArray).'';
                    }else{

                        if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                            for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                                if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){

                                    $totalHours3 = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$punchOUTDate,$multiplePunchDataArray[$i]["punch_in_time"],$punchOUTTime);

                                    $timeHr = explode(':', $totalHours3.":00");
                                    $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                                    $finalTotalMinute = $finalTotalMinute + $totalMinutes;

                                    $last_punch_in_date = $multiplePunchDataArray[$i]["punch_in_date"].'';
                                    $last_punch_in_time = $multiplePunchDataArray[$i]["punch_in_time"].'';

                                    $oldAry = array(
                                        "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"].'',
                                        "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"].'',
                                        "punch_out_date" => $punchOUTDate.'',
                                        "punch_out_time" =>$punchOUTTime.'',
                                        "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"].'',
                                        "punch_out_image" => $punch_image.'',
                                        "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"].'',
                                        "location_name_out" => $location_name.'',
                                        "working_hour" => $totalHours3.'',
                                        "working_hour_minute" => $finalTotalMinute.'',
                                    );
                                    array_push($dataArray,$oldAry);
                                }
                            }

                            $multPunchDataJson = json_encode($dataArray).'';
                        }else{

                            $last_punch_in_date = $attendance_date_start;
                            $last_punch_in_time = $punch_in_time;

                            $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                    $punch_in_time,$punchOUTTime);

                            $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                            $punch_in_time,$punchOUTTime);

                            $times[] = $totalHours;

                            $time = explode(':', $totalHours.":00");
                            $finalTotalMinute = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                            $oldAry = array(
                                "punch_in_date" => $attendance_date_start.'',
                                "punch_in_time" => $punch_in_time.'',
                                "punch_out_date" => $punchOUTDate.'',
                                "punch_out_time" =>$punchOUTTime.'',
                                "punch_in_image" => '',
                                "punch_out_image" => $punch_image.'',
                                "location_name_in" => '',
                                "location_name_out" => $location_name.'',
                                "working_hour" => $totalHours.'',
                                "working_hour_minute" => $finalTotalMinute.'',
                            );
                            array_push($dataArray,$oldAry);

                            $multPunchDataJson = json_encode($dataArray).'';

                        }
                    }

                    if ($is_multiple_punch_in == 0) {

                        if ($early_out_time!='') {
                            // code...
                            if ($datePunchOUT >= $defaultOutTime) {
                                $early_out = "0";
                            }else{
                                $early_out = "1";              
                            }
                        }else{
                            $early_out = "0";
                        }

                        if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                            $early_out = "0";
                        }

                        $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                    $punch_in_time,$punchOUTTime);

                        $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                        $punch_in_time,$punchOUTTime);

                        $times[] = $totalHours;

                        $time = explode(':', $totalHours.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
                    }else{
                        $total_minutes =  $finalTotalMinute;

                        $hoursPM  = floor($total_minutes/60);
                        $minutesPM = $total_minutes % 60;

                        $totalHours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                        $times[] = $totalHours;

                        if ($hoursPM > 0 && $minutesPM) {
                            $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                        }else if ($hoursPM > 0 && $minutesPM <= 0) {
                            $totalHoursNames = sprintf('%02d hr', $hoursPM);
                        }else if ($hoursPM <= 0 && $minutesPM > 0) {
                            $totalHoursNames = sprintf('%02d min', $minutesPM);
                        }else{
                            $totalHoursNames = "No Data";
                        }
                    }

                    $rQ = false;

                    $response['total_hour'] = $totalHours;

                    $shiftETime =  date('Y-m-d'). ' ' .$shift_end_time;
                    $shiftETime =  strtotime($shiftETime);
                    $ctn =  strtotime(date('Y-m-d H:i:s'));

                    if ($shift_type == "Night") {

                        if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($curDate))).' '.$max_punch_out_time;
                        }else{
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($attendance_date_start))).' '.$max_punch_out_time;                        
                        }
                        $curTimeNew = strtotime($curTimeNew);

                        if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                            $newCTime = date('Y-m-d H:i:s');

                            $date_a = new DateTime($datePunchIN);
                            $date_b = new DateTime($newCTime);

                            $interval = $date_a->diff($date_b);

                            $day = $interval->format('%days');

                            if ($day > 0) {
                                $hour = $day * 24;
                            }
                            $hours = $hour + $interval->format('%h');

                            if ($ctn > $curTimeNew && $hours >= 18) {
                                $rQ = true;
                            }
                        }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                            if ($ctn > $curTimeNew && $ctn > $shiftETime) {
                                $rQ = true; 
                            }
                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                            if ($total_minutes > $max_shift_hour_minutes) {
                                $rQ = true; 
                            }
                        }
                    }else{

                        if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $attStartDate==$curDate) {
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                            $curTimeNew = strtotime($curTimeNew);
                        }else{
                            $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                            $curTimeNew = strtotime($curTimeNew);
                        }

                        if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                            if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                                if ($ctn > $curTimeNew) {
                                    $rQ = true; 
                                }
                            }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                                if ($total_minutes > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                    $rQ = true; 
                                }
                            }
                        }
                    }


                    if ($rQ == true) {
                        $response["message"] = "Punch out time limit over $test1";                
                        $response["status"] = "202";
                        $response['user_full_name'] =$userData["user_full_name"];
                        $response['society_id'] =$userData["society_id"];
                        $response['block_id'] =$userData["block_id"];
                        $response['floor_id'] =$userData["floor_id"];
                        $response['unit_id'] =$userData["unit_id"];
                        $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                        $response['user_designation'] =$userData["user_designation"];
                        echo json_encode($response);
                        exit();
                    }

                    if ($attData['work_report_id'] == null || $attData['work_report_id'] == '') {

                        $qSociety1 = $d->count_data_direct("society_id","society_master", "society_id ='$society_id' AND (work_report_on = '1' OR (work_report_on = '2' AND FIND_IN_SET('$block_id',work_report_on_ids)) OR (work_report_on = '3' AND FIND_IN_SET('$floor_id',work_report_on_ids)))");

                        $work_report_on = false;

                        if ($qSociety1 > 0) {
                            $work_report_on = true;
                        }

                        if ($user_work_report_on == "1") {
                            $work_report_on = true;
                        }else if ($user_work_report_on == "2"){
                            $work_report_on = false;
                        }

                        if($work_report_on == true){                        
                            $response['work_report_pending'] = $work_report_on;
                            $response['message'] = "Please add work report from employee app and then punch out";
                            $response['status'] = "201";

                            $title = "Face app punch out failed!";
                            $description = "Please submit your work report and try again!";

                            if ($device == 'android') {
                                $nResident->noti("add_work_report","",$society_id,$user_token,$title,$description,$society_id);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("add_work_report","",$society_id,$user_token,$title,$description,$society_id);
                            }

                            echo json_encode($response);
                            exit();
                        }
                    }


                    // Average Working days calculations

                    $avgWorkingDays = $total_minutes/$perDayHourMinute;

                    $avg_working_days = round($avgWorkingDays * 2) / 2;

                    $extra_working_hours_minutes = 0;

                    if ($total_minutes > $perDayHourMinute) {
                        $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                        $hours  = floor($extra_working_hours_minutes/60);
                        $minutes = $extra_working_hours_minutes % 60;

                        $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
                    }

                    $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

                    $qryBreakData = mysqli_fetch_array($qryBreak);

                    $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

                    if ($total_minutes > $totalBreakinutes) {
                        $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                        $hoursPM  = floor($productive_working_hours_minutes/60);
                        $minutesPM = $productive_working_hours_minutes % 60;

                        $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                    }else{
                        $productive_working_hours_minutes = $total_minutes;

                        $hoursPM  = floor($productive_working_hours_minutes/60);
                        $minutesPM = $productive_working_hours_minutes % 60;

                        $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                    }


                    if ($track_user_location == 1) {
                        $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                        $query11Data=mysqli_fetch_array($query11);

                        if ($query11Data['SUM(last_distance)'] > 0) {
                            $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                        }else{
                            $total_travel_meter = '0';
                        }
                    }

                    $m->set_data('attendance_date_end',$punchOUTDate);
                    $m->set_data('punch_out_time',$punchOUTTime);
                    $m->set_data('punch_out_latitude',$punch_in_latitude);
                    $m->set_data('punch_out_longitude',$punch_in_longitude);
                    $m->set_data('punch_out_image',$punch_image);
                    $m->set_data('early_out',$early_out);
                    $m->set_data('avg_working_days',$avg_working_days);
                    $m->set_data('total_working_hours',$totalHours);
                    $m->set_data('total_working_minutes',$total_minutes);
                    $m->set_data('total_travel_meter',$total_travel_meter);
                    $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                    $m->set_data('extra_working_hours',$extra_working_hours);
                    $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                    $m->set_data('punch_out_address',$google_address);
                    $m->set_data('punch_out_face_app_location',$device_location);
                    $m->set_data('productive_working_hours',$productive_working_hours);
                    $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                    $m->set_data('punch_out_branch',$location_name.'');
                    $m->set_data('last_punch_in_date',$last_punch_in_date.'');
                    $m->set_data('last_punch_in_time',$last_punch_in_time.'');
                    $m->set_data('attendance_out_from',"0");

                    $a = array(
                        'attendance_date_end'=>$m->get_data('attendance_date_end'),
                        'punch_out_time'=>$m->get_data('punch_out_time'),
                        'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                        'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                        'punch_out_image'=>$m->get_data('punch_out_image'),
                        'early_out'=>$m->get_data('early_out'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'total_working_hours'=>$m->get_data('total_working_hours'),
                        'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                        'total_working_minutes'=>$m->get_data('total_working_minutes'),
                        'attendance_out_from'=>$m->get_data('attendance_out_from'),
                        'total_travel_meter'=>$m->get_data('total_travel_meter'),
                        'extra_working_hours'=>$m->get_data('extra_working_hours'),
                        'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                        'punch_out_address'=>$m->get_data('punch_out_address'),
                        'punch_out_face_app_location'=>$m->get_data('punch_out_face_app_location'),
                        'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                        'productive_working_hours'=>$m->get_data('productive_working_hours'),
                        'punch_out_branch'=>$m->get_data('punch_out_branch'),
                        'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                        'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                    );

                    $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                    if ($punchOutQry == true) {

                        $title = "Punched OUT from face app";
                        $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                        if ($device == 'android') {
                            $nResident->noti("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                        }

                        $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$user_id,
                            'notification_title'=>$title,
                            'notification_desc'=>$description,    
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'punch_out',
                            'notification_logo'=>'attendance_tracker.png',
                            'notification_type'=>'0',
                        );
                        
                        $d->insert("user_notification",$notiAry);

                        // Check Half Day


                        $leaveValue = false;

                        if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                            $leaveValue = true;
                        } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                            $leaveValue = true;
                        }

                        $alreadyLeaveQryCheck = false; 

                        $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                        $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                        if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != '00:00:00') {
                            $noLeave = true;
                        }else{
                            $noLeave = false;
                        }

                        $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                        
                        if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                            $leave_total_days = 1;

                            if ($total_minutes < $maximum_halfday_hours_min) {
                                $leaveType = "0";
                                $half_day_session = "0"; 
                                $is_leave = "1";         
                                $extra_day_leave_type = "1";         
                            }else{
                                $leaveType = "1";
                                $is_leave = "2";
                                $extra_day_leave_type = "2";
                            }   

                            if ($extraDayAtt == 1) {
                                $is_leave = "0";
                            }else{
                                $extra_day_leave_type = "0";
                            }                     

                            if ($hasLeave == true) {

                                $title = "Auto Leave Applied"." - ".$leaveTypeName;
                                $titleMessage = "Working hours not completed (".timeFormat($totalHours).")";
                                $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                $m->set_data('leave_type_id',$leave_type_id);
                                $m->set_data('paid_unpaid',$paid_unpaid);
                                $m->set_data('leave_day_type',$leaveType);
                                $m->set_data('half_day_session',$half_day_session);

                                if ($auto_leave_reason != '') {
                                    $m->set_data('auto_leave_reason',$titleMessage);

                                    $a = array(
                                        'leave_type_id'=>$m->get_data('leave_type_id'),
                                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                        'leave_day_type'=>$m->get_data('leave_day_type'),
                                        'half_day_session'=>$m->get_data('half_day_session'),
                                        'auto_leave_reason'=>$m->get_data('auto_leave_reason')
                                    );
                                }else{
                                    $a = array(
                                        'leave_type_id'=>$m->get_data('leave_type_id'),
                                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                        'leave_day_type'=>$m->get_data('leave_day_type'),
                                        'half_day_session'=>$m->get_data('half_day_session')
                                    );
                                }

                                $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");

                                /*if ($device == 'android') {
                                    $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                } else if ($device == 'ios') {
                                    $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                }*/

                                //$d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                
                            }else{

                                if ($leaveType == 0) {
                                    $workingDaysTotal = "0";
                                    $title = "Auto Leave Applied - Full Day";
                                }else{
                                    $workingDaysTotal = "0.5";
                                    $title = "Auto Leave Applied - Half Day";
                                }
                                
                                $titleMessage = "Working hours not completed (".timeFormat($totalHours).")";
                                $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                $m->set_data('leave_type_id',"0");
                                $m->set_data('society_id',$society_id);
                                $m->set_data('paid_unpaid',"1");
                                $m->set_data('unit_id',$unit_id);
                                $m->set_data('user_id',$user_id);
                                $m->set_data('auto_leave_reason',$titleMessage);
                                $m->set_data('leave_start_date',$attendance_date_start);
                                $m->set_data('leave_end_date',$punchOUTDate);
                                $m->set_data('leave_total_days',$leave_total_days);
                                $m->set_data('leave_day_type',$leaveType);
                                $m->set_data('leave_status',"1");
                                $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                $a = array(
                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                    'society_id' =>$m->get_data('society_id'),
                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                    'unit_id'=>$m->get_data('unit_id'),
                                    'user_id'=>$m->get_data('user_id'),
                                    'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                    'leave_start_date'=>$m->get_data('leave_start_date'),
                                    'leave_end_date'=>$m->get_data('leave_end_date'),
                                    'leave_total_days'=>$m->get_data('leave_total_days'),
                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                    'leave_status'=>$m->get_data('leave_status'),
                                    'leave_created_date'=>$m->get_data('leave_created_date'),
                                );

                                $leaveQry = $d->insert("leave_master",$a);

                                $alreadyLeaveQryCheck = true;

                                
                                if ($device == 'android') {
                                    $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                } else if ($device == 'ios') {
                                    $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                }

                                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                            }

                            $m->set_data('auto_leave',"1");
                            $m->set_data('avg_working_days',$workingDaysTotal);
                            $m->set_data('is_leave',$is_leave);
                            $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                            $a1 = array(
                                'auto_leave'=>$m->get_data('auto_leave'),
                                'avg_working_days'=>$m->get_data('avg_working_days'),
                                'is_leave'=>$m->get_data('is_leave'),
                                'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                            );

                            $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                        }else if($noLeave == true && $hasLeave == true){
                            $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                            $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                            $title = "Applied Leave Cancelled";
                            $description = "For Date : ".$attendance_date_start;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker"," leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                        }

                        $startMonthDate = date('Y-m-01');
                        $endMonthDate = date('Y-m-t');


                        $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                        $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                        $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                        if ($hasLeave == false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                            $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                            $m->set_data('leave_type_id',"0");
                            $m->set_data('society_id',$society_id);
                            $m->set_data('paid_unpaid',"1");
                            $m->set_data('unit_id',$unit_id);
                            $m->set_data('user_id',$user_id);
                            $m->set_data('auto_leave_reason',$titleMessage);
                            $m->set_data('leave_start_date',$attendance_date_start);
                            $m->set_data('leave_end_date',$punchOUTDate);
                            $m->set_data('leave_total_days',"1");
                            $m->set_data('leave_day_type',"1");
                            $m->set_data('leave_status',"1");
                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'society_id' =>$m->get_data('society_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'leave_status'=>$m->get_data('leave_status'),
                                'leave_created_date'=>$m->get_data('leave_created_date'),
                            );

                            $leaveQry = $d->insert("leave_master",$a);

                            $title = "Auto Leave Applied - Half Day";

                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                            if ($extraDayAtt == 1) {
                                $is_leave = "0";
                            }else{
                                $extra_day_leave_type = "2";
                            }

                            $m->set_data('auto_leave',"1");
                            $m->set_data('avg_working_days',"0.5");
                            $m->set_data('is_leave',$is_leave);
                            $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                            $a1 = array(
                                'auto_leave'=>$m->get_data('auto_leave'),
                                'avg_working_days'=>$m->get_data('avg_working_days'),
                                'is_leave'=>$m->get_data('is_leave'),
                                'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                            );

                            $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                        }

                        // END checking half day

                        $attHisQry = $d->selectRow("attendance_break_history_id,break_end_date,break_out_time,break_start_date,break_in_time","attendance_break_history_master","attendance_id = '$attendance_id' AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");

                        if (mysqli_num_rows($attHisQry) > 0) {
                            while($attData = mysqli_fetch_array($attHisQry)){
                                $attendance_break_history_id = $attData['attendance_break_history_id'];
                                $break_start_date = $attData['break_start_date'];
                                $break_in_time = $attData['break_in_time'];
                                $break_end_date = date('Y-m-d');
                                $break_out_time = date('H:i:s');

                                $total_break_time = getTotalHours($break_start_date,$break_end_date,
                                    $break_in_time,$break_out_time);

                                $time = explode(':', $total_break_time.":00");
                                $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                                $m->set_data('break_end_date',$break_end_date);
                                $m->set_data('break_out_time',$break_out_time);
                                $m->set_data('total_break_time',$total_break_time);
                                $m->set_data('total_break_time_minutes',$total_break_time_minutes);

                                $a = array(
                                    'break_end_date'=>$m->get_data('break_end_date'),
                                    'break_out_time'=>$m->get_data('break_out_time'),
                                    'total_break_time'=>$m->get_data('total_break_time'),
                                    'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
                                );

                                $qry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id' AND attendance_id = '$attendance_id'");
                            }
                        }
                        

                        $response['user_full_name'] =$attData["user_full_name"];
                        $response['society_id'] =$attData["society_id"];
                        $response['block_id'] =$attData["block_id"];
                        $response['floor_id'] =$attData["floor_id"];
                        $response['unit_id'] =$attData["unit_id"];
                        $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$attData["user_profile_pic"];
                        $response['user_designation'] =$attData["user_designation"];
                        
                        $response['attendance_id'] =$attData["attendance_id"];
                        $response['punch_in_time'] =$attData["punch_in_time"];
                        $response['punch_in_time_view'] = date("h:i A",strtotime($attData["punch_in_time"]));
                        $response['date_time'] =$finalTime;
                        $response['time_diff'] =$time_diff;
                        $response["in_out_status"] = "1";
                        $response["message"] = "Goodbye $user_first_name";
                        $response["status"] = "200";
                        echo json_encode($response);
                        exit();
                    }else{
                        $response["message"] = "Something went wrong";
                        $response["status"] = "201";
                        echo json_encode($response);
                    } 
                
                }else{
                    $response["message"] = "You have already punched out";                
                    $response["status"] = "202";
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    echo json_encode($response);
                    exit();
                }
            }else{
                // Code For punch IN

                // Code for remove WFH request

                $d->delete("wfh_master","user_id = '$user_id' AND wfh_start_date = '$punchInDate' AND wfh_day_type = '0'");

                $punchInDateTime = $punchInDate." ".$punchInTime;
                $aData = mysqli_fetch_array($getTodayAttendace);

                $mpAry = json_decode($aData['multiple_punch_in_out_data'],true);

                $isMultiOut = false;
            
                if (count($mpAry) > 0) {

                    for ($i=0; $i < count($mpAry) ; $i++) {

                        if($mpAry[$i]["punch_out_date"] == '' || $mpAry[$i]["punch_out_date"] == '0000-00-00'){  
                            $isMultiOut = true;
                            $attStartDate = $mpAry[$i]["punch_in_date"];
                            $punchInTime = $mpAry[$i]["punch_in_time"];
                            break;
                        }
                    }  
                }

                if ($isMultiOut == true) {
                    $totalWorkingMinutes = $aData['total_working_minutes'];
                    $lastPunchInDate = $aData['last_punch_in_date'];
                    $lastPunchInTime = $aData['last_punch_in_time']; 

                    $tHours = getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));

                    $tm = explode(':', $tHours.":00");
                    $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                    $total_minutes = $totalWorkingMinutes + $tMinutes;
                }else{
                    $total_minutes = $aData['total_working_minutes'];
                }

                if (mysqli_num_rows($getTodayAttendace) > 0) {


                    $shiftETime =  date('Y-m-d'). ' ' .$shift_end_time;
                    $shiftETime =  strtotime($shiftETime);
                    $ctn =  strtotime(date('Y-m-d H:i:s'));
                    $aDate = $aData['attendance_date_start'];
                    $attendance_id = $aData['attendance_id'];

                    if ($shift_type == "Night") {

                        if ($shift_end_time > $max_punch_out_time  && $max_punch_out_time != '') {
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($punchInDate))).' '.$max_punch_out_time;
                        }else{
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($aDate))).' '.$max_punch_out_time;                        
                        }

                        $curTimeNew = strtotime($curTimeNew);
                    
                        if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                            $date_a = new DateTime($datePunchIN);
                            $date_b = new DateTime($defaultOutTimeTemp);

                            $interval = $date_a->diff($date_b);

                            $day = $interval->format('%days');

                            if ($day > 0) {
                                $hour = $day * 24;
                            }
                            $hours = $hour + $interval->format('%h');

                            if ($ctn > $curTimeNew && $hours > 18) {
                                $rQ = true;
                            }
                        }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                            if ($ctn > $curTimeNew && $ctn > $shiftETime) {

                                $rQ = true; 
                            }
                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                            if ($total_minutes > $max_shift_hour_minutes) {
                                $rQ = true; 
                            }
                        }
                    }else{

                        if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $aDate==$curDate) {
                            $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                            $curTimeNew = strtotime($curTimeNew);
                        }else{
                            $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                            $curTimeNew = strtotime($curTimeNew);
                        }

                        if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                            if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                                if ($ctn > $curTimeNew) {
                                    $rQ = true; 
                                }
                            }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                                if ($total_minutes > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                    $rQ = true; 
                                }
                            }
                        }
                    }
                }

                if ($is_multiple_punch_in == 0 && $shift_type == "Night") {
                    $tempStartDate = $aData['attendance_date_start'];
                    $tempPunchInTime = $aData['punch_in_time'];

                    if ($tempPunchInTime > $shift_start_time) {
                        $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($tempStartDate)));
                    }
                }else if ($is_multiple_punch_in == 1 && $shift_type == "Night") {
                    $tempStartDate = $aData['attendance_date_start'];
                    $tempPunchInTime = $aData['punch_in_time'];

                    if ($tempPunchInTime > $shift_start_time) {
                        $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($tempStartDate)));
                    }else{
                        $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($punchInDate)));
                    }
                }

                $approxShiftEndTime = $tempStartDate.' '.$shift_end_time;
                $finalMin = (1440 - $per_day_hour_minutes)/2;
                
                if ($finalMin < 1) {
                    $finalMin = 1;
                }

                $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
                $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
                $punchInDateTimeString = strtotime(date('Y-m-d H:i:s'));

                $attendance_id = $aData['attendance_id'];
                $multiple_punch_in_out_data = $aData['multiple_punch_in_out_data'];
              // echo "$approxShiftEndTimeNewTimeString > $punchInDateTimeString";
                if (mysqli_num_rows($getTodayAttendace) > 0  && ($approxShiftEndTimeNewTimeString > $punchInDateTimeString)) {

                    if ($aData['attendance_date_end'] != "0000-00-00") {
                        $response["message"] = "You have already punched out";               
                    }else if ($rQ == false){
                        $response["message"] = "You have already punched in";                
                    }

                    $response["status"] = "202";
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];

                    if ($rQ == false) {

                        if (($attendance_id == '' || $attendance_id == '0') && $is_multiple_punch_in == 0 && $aData['attendance_date_end'] == "0000-00-00") {
                            echo json_encode($response);
                            exit();
                        }else if ($attendance_id != '' && $is_multiple_punch_in == 1 && $aData['attendance_date_end'] == "0000-00-00") {
                            echo json_encode($response);
                            exit();
                        }else if ($is_multiple_punch_in == 0) {
                            echo json_encode($response);
                            exit();
                        }
                    }
                }

                // Image upload

                $punch_image = "";

                if ($_FILES["punch_image"]["tmp_name"] != null) {
                    // code...
                    $file11=$_FILES["punch_image"]["tmp_name"];
                    $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["punch_image"]["name"]);
                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                    }
                }

                // Late in

                if ($late_time_start != '') {
                    $partLI = explode(':', $late_time_start);
                    $min = ($partLI[0]*60) + ($partLI[1]) + ($partLI[2]/60);
                }else{
                    $min = 0;
                }

                $defaultINTime = $punchInDate." ".$shift_start_time;
                $strMin = "+".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));


                if ($late_time_start !='') {
                    // code...
                    if ($punchInDateTime > $defaultTime) {
                        $late_in = "1";
                    }else{
                        $late_in = "0";
                    }
                }else{
                    $late_in = "0";
                }   

                if ($rQ == true) {
                    $attendance_id = 0;
                } 

                $newDate = date('Y-m-d',strtotime("+1 days",strtotime($aDate)));
                $approxShiftEndTime = $newDate.' '.$shift_end_time;
                $finalMin = (1440 - $per_day_hour_minutes)/2;
                
                if ($finalMin < 1) {
                    $finalMin = 1;
                }

                $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
                $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
                $punchInDateTimeString = strtotime(date('Y-m-d H:i:s'));

                if ($is_multiple_punch_in == 1 && $shift_type == "Day" && $aDate != $punchInDate) {
                    $attendance_id = 0;
                }else if ($is_multiple_punch_in == 1 && $shift_type == "Night" && ($approxShiftEndTimeNewTimeString < $punchInDateTimeString)) {
                    $attendance_id = 0;
                }else if ($is_multiple_punch_in == 0 && $shift_type == "Night" && ($approxShiftEndTimeNewTimeString > $punchInDateTimeString)) {
                    $attendance_id = 0;
                }

                if ($attendance_id == 0 || $attendance_id == '') { 

                    $punchInTime = date('H:i:s');

                    $m->set_data('shift_time_id',$shift_time_id);
                    $m->set_data('total_shift_hours',$per_day_hour);
                    $m->set_data('society_id',$society_id);
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$user_id);
                    $m->set_data('attendance_date_start',$punchInDate);
                    $m->set_data('last_punch_in_date',$punchInDate);
                    $m->set_data('punch_in_time',$punchInTime);
                    $m->set_data('last_punch_in_time',$punchInTime);
                    $m->set_data('punch_in_latitude',$punch_in_latitude);
                    $m->set_data('punch_in_longitude',$punch_in_longitude);
                    $m->set_data('punch_in_image',$punch_image);
                    $m->set_data('punch_in_address',$google_address);
                    $m->set_data('punch_in_face_app_location',$device_location);
                    $m->set_data('late_in',$late_in);
                    $m->set_data('is_extra_day',$is_extra_day);
                    $m->set_data('punch_in_branch',$location_name.'');
                    $m->set_data('attendance_status',"1");
                    $m->set_data('attendance_in_from',"0");
                    $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

                    $dataArray = array();
                    $multPunchAry = array(
                        "punch_in_date" => $punchInDate.'',
                        "punch_in_time" => $punchInTime.'',
                        "punch_out_date" => "",
                        "punch_out_time" => "",
                        "punch_in_image" => $punch_image.'',
                        "punch_out_image" => "",
                        "location_name_in" => $location_name.'',
                        "location_name_out" => "",
                        "working_hour" => "",
                        "working_hour_minute" => "",
                    );

                    array_push($dataArray,$multPunchAry);
                    $multPunchDataJson = json_encode($dataArray);

                    $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);

                    $a = array(
                        'shift_time_id'=>$m->get_data('shift_time_id'),
                        'total_shift_hours'=>$m->get_data('total_shift_hours'),
                        'society_id' =>$m->get_data('society_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'attendance_date_start'=>$m->get_data('attendance_date_start'),
                        'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                        'punch_in_time'=>$m->get_data('punch_in_time'),
                        'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                        'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                        'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                        'punch_in_image'=>$m->get_data('punch_in_image'),
                        'late_in'=>$m->get_data('late_in'),
                        'is_extra_day'=>$m->get_data('is_extra_day'),
                        'attendance_status'=>$m->get_data('attendance_status'),
                        'attendance_in_from'=>$m->get_data('attendance_in_from'),
                        'attendance_created_time'=>$m->get_data('attendance_created_time'),
                        'punch_in_address'=>$m->get_data('punch_in_address'),
                        'punch_in_face_app_location'=>$m->get_data('punch_in_face_app_location'),
                        'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                        'punch_in_branch'=>$m->get_data('punch_in_branch'),
                    );


                    $punchInQry = $d->insert("attendance_master",$a);
                    $attendance_id = $con->insert_id;
                }else if ($is_multiple_punch_in == 1) {

                    $dataArray = array();
                    if ($multiple_punch_in_out_data != '') {

                        $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                        for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                            $oldAry = array(); 
                            $oldAry['punch_in_date'] = $multiplePunchDataArray[$i]["punch_in_date"].'';
                            $oldAry['punch_in_time'] = $multiplePunchDataArray[$i]["punch_in_time"].'';
                            $oldAry['punch_out_date'] = $multiplePunchDataArray[$i]["punch_out_date"].'';
                            $oldAry['punch_out_time'] = $multiplePunchDataArray[$i]["punch_out_time"].'';
                            $oldAry['punch_in_image'] = $multiplePunchDataArray[$i]["punch_in_image"].'';
                            $oldAry['punch_out_image'] = $multiplePunchDataArray[$i]["punch_out_image"].'';
                            $oldAry['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"].'';
                            $oldAry['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"].'';
                            $oldAry['working_hour'] = $multiplePunchDataArray[$i]["working_hour"].'';
                            $oldAry['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';
                            array_push($dataArray,$oldAry);
                        }

                        $multPunchAry = array(
                            "punch_in_date" => $punchInDate.'',
                            "punch_in_time" => $punchInTime.'',
                            "punch_out_date" => "",
                            "punch_out_time" => "",
                            "punch_in_image" => $punch_image.'',
                            "punch_out_image" => "",
                            "location_name_in" => $location_name.'',
                            "location_name_out" => "",
                            "working_hour" => "",
                            "working_hour_minute" => "",
                        );

                        array_push($dataArray,$multPunchAry);
                        $multPunchDataJson = json_encode($dataArray);
                    }else{
                        $multPunchAry = array(
                            "punch_in_date" => $punchInDate.'',
                            "punch_in_time" => $punchInTime.'',
                            "punch_out_date" => "",
                            "punch_out_time" => "",
                            "punch_in_image" => $punch_image.'',
                            "punch_out_image" => "",
                            "location_name_in" => $location_name.'',
                            "location_name_out" => "",
                            "working_hour" => "",
                            "working_hour_minute" => "",
                        );

                        array_push($dataArray,$multPunchAry);
                        $multPunchDataJson = json_encode($dataArray);
                    }

                    $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                    $m->set_data('last_punch_in_date',$punchInDate);
                    $m->set_data('last_punch_in_time',$punchInTime);
                    $m->set_data('punch_in_branch',$location_name);
                    $m->set_data('attendance_date_end',"");
                    $m->set_data('punch_out_time',"");

                    $a = array(
                        'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                        'punch_out_time'=>$m->get_data('punch_out_time'),
                        'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                        'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                        'attendance_date_end'=>$m->get_data('attendance_date_end'),
                        'punch_in_branch'=>$m->get_data('punch_in_branch'),
                    );

                    $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                }


                if ($punchInQry == true) {

                    $title = "Punched IN from face app";
                    $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                    if ($device == 'android') {
                        $nResident->noti("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'punch_in',
                        'notification_logo'=>'attendance_tracker.png',
                        'notification_type'=>'0',
                    );
                    
                    $d->insert("user_notification",$notiAry);
                
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    
                    $response['attendance_id'] =$attendance_id.'';
                    $response['punch_in_time'] =$punchInTime;
                    $response['punch_in_time_view'] = date("h:i A",strtotime($punchInTime));
                    $response['date_time'] ="";
                    $response['time_diff'] ="";
                    $response["in_out_status"] = "0";
                    $response["message"] = "Welcome $user_first_name";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }else{
                    $response["message"] = "Something went wrong";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            }

            
            $response["auto_api_call_time"] = 6000;
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);  
        }else if($_POST['syncRequest']=="syncRequest" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $location_name = $device_location;

            $dayNumber = date("w"); 

            $totalEntry  = count($_POST['user_id_raw']);

            $successEntry = array();

            for ($i=0; $i <$totalEntry ; $i++) { 
                $user_id = $_POST['user_id'][$i];

                $latitude = $_POST['latitude'][$i];
                $longitude = $_POST['longitude'][$i];
                $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
                $userData = mysqli_fetch_array($checkShift);
                $shift_time_id = $userData['shift_time_id'];
                $unit_id = $userData['unit_id'];
                $user_full_name = $userData['user_full_name'];
                $user_profile_pic = $userData['user_profile_pic'];
                $user_designation = $userData['user_designation'];
                $user_token = $userData['user_token'];
                $device = $userData['device'];
                $user_first_name = $userData['user_first_name'];

                if ($shift_time_id > 0) {

                    $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
                    $shiftData = mysqli_fetch_array($shiftQ);
                    $perdayHours = $shiftData['per_day_hour'];
                    $shift_start_time = $shiftData['shift_start_time'];
                    $per_day_hour = $shiftData['per_day_hour'];
                    $shift_end_time = $shiftData['shift_end_time'];
                    $late_time_start = $shiftData['late_time_start'];
                    $early_out_time = $shiftData['early_out_time'];
                    $half_day_time_start = $shiftData['half_day_time_start'];
                    $halfday_before_time = $shiftData['halfday_before_time'];
                    $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                    $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                    $shift_type = $shiftData['shift_type'];
                    $maximum_in_out = $shiftData['maximum_in_out'];
                    $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];
                    $max_shift_hour = $shiftData['max_shift_hour'];
                    $max_hour = $shiftData['max_hour'];
                    $max_punch_out_time = $shiftData['max_punch_out_time'];
                    $alternate_week_off = $shiftData['alternate_week_off'];
                    $alternate_weekoff_days = $shiftData['alternate_weekoff_days'];

                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                        $isMaxTimeSet = true;
                    } else if ($max_shift_hour != '' && $max_shift_hour!='00:00:00') {
                        $avarageDay = round($max_shift_hour/24);
                        $isMaxHoursSet = true;
                    }

                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                        $checkNextDayDate = true;
                    }

                    if ($max_shift_hour != '' && $max_shift_hour != '00:00:00') {
                        $parts = explode(':', $max_shift_hour);
                        $max_shift_hour_minutes = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                    }else{
                        $max_shift_hour_minutes = 0;
                    }
                   
                    $parts1 = explode(':', $perdayHours);
                    $per_day_hour_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);

                    $alternate_week_off = explode(",", $alternate_week_off);
                    $alternate_weekoff_days = explode(",", $alternate_weekoff_days);

                    $is_extra_day = "0"; 

                    if ((isset($alternate_week_off) && in_array($dayNumber, $alternate_week_off)) || (isset($alternate_weekoff_days) && in_array($dayNumber, $alternate_weekoff_days))) {
                        $is_extra_day = "1";
                    }

                    $punchInTime =$_POST['time'][$i];
                    $punchInTimeTemp =$_POST['time'][$i];
                    $punchInDate =$_POST['date'][$i];
                    $punchOUTTime = $_POST['time'][$i];
                    $punchOUTDate = $_POST['date'][$i];                    
                   
                    $punchInDateTime = $punchInDate.' '.$punchInTime;

                    $holidayQry = $d->count_data_direct("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_status = 0 AND holiday_start_date = '$punchInDate'");

                    if ($holidayQry > 0) {
                        $is_extra_day = "1";
                    }

                    $attendance_date_start =$_POST['date'][$i];
                    $attendance_type_id =$_POST['breakId'][$i];

                    $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$user_id' AND leave_start_date = '$punchInDate' AND leave_status = '1'");

                    $hasLeave = false;

                    if (mysqli_num_rows($qryLeave) > 0) {

                        $hasLeave = true;

                        $leaveData = mysqli_fetch_array($qryLeave);

                        $leave_id = $leaveData['leave_id'];
                        $leave_type_id = $leaveData['leave_type_id'];
                        $paid_unpaid = $leaveData['paid_unpaid'];
                        $leave_reason = $leaveData['leave_reason'];
                        $leave_day_type = $leaveData['leave_day_type'];
                        $half_day_session = $leaveData['half_day_session'];
                    }

                    if ($shift_type == "Night") {

                        $attendance_date_start = date('Y-m-d',strtotime('-1 days',strtotime($punchInDate)));

                        if ($is_multiple_punch_in == 0) {
                            $appQry = " AND (attendance_master.attendance_date_end = '0000-00-00' OR attendance_master.attendance_date_end = '$punchInDate')";
                        }

                        if ($isMaxHoursSet==true) {

                            if ($avarageDay<1) {
                                $avarageDay =1;
                            }

                            $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id
                                AND (attendance_master.attendance_date_start BETWEEN  '$date2DayPre' AND '$punchInDate') 
                                AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                        } else if ($isMaxTimeSet==true) {
                            $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id
                                AND (attendance_master.attendance_date_start = '$attendance_date_start' OR attendance_master.attendance_date_start = '$date2DayPre')  AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                        } else {
                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id
                                AND (attendance_master.attendance_date_start BETWEEN '$attendance_date_start' AND '$punchInDate') AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");
                        }
                    }else{

                        if ($is_multiple_punch_in == 0) {
                            $appQry = " AND (attendance_master.attendance_date_end = '0000-00-00' OR attendance_master.attendance_date_end = '$punchInDate')";
                        }

                        if ($isMaxHoursSet==true) {

                            if ($avarageDay<1) {
                                $avarageDay =1;
                            }
                            $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id 
                                AND (attendance_master.attendance_date_start BETWEEN  '$date2DayPre' AND '$punchInDate') AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                        } else if ($isMaxTimeSet==true) {
                            $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id
                                AND (attendance_master.attendance_date_start = '$punchInDate' OR attendance_master.attendance_date_start = '$date2DayPre')  AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                        } else {
                            $getTodayAttendace = $d->selectRow("
                                attendance_master.*,
                                users_master.user_full_name,
                                users_master.block_id,
                                users_master.floor_id,
                                users_master.user_designation,
                                users_master.user_profile_pic,
                                work_report_master.work_report_id
                                ","
                                users_master,
                                attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND work_report_master.user_id = attendance_master.user_id
                                ","
                                attendance_master.user_id = '$user_id' 
                                AND users_master.user_id = attendance_master.user_id
                                AND attendance_master.attendance_date_start = '$punchInDate' AND attendance_master.attendance_status != '2' $appQry","ORDER BY attendance_master.attendance_date_start DESC,attendance_master.attendance_id DESC LIMIT 1");

                        }
                    }


                    if ($_POST['inOut'][$i]==0) {
                        // for In Code
                        if ($_POST['breakId'][$i]>0) {
                            // brek in Code
                            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND unit_id = '$unit_id' AND shift_time_id = '$shift_time_id' AND attendance_date_start = '$punchInDate'");
                            $isPunchIn = mysqli_fetch_array($isPunchIn);
                            $attendance_id = $isPunchIn['attendance_id'];

                            $getTodayBreak = $d->selectRow("attendance_break_history_master.attendance_break_history_id,attendance_break_history_master.attendance_type_id,attendance_break_history_master.break_start_date,attendance_break_history_master.break_end_date,attendance_break_history_master.break_in_time,attendance_break_history_master.break_out_time,attendance_type_master.attendance_type_name,attendance_type_master.is_multipletime_use","attendance_type_master,attendance_break_history_master","attendance_break_history_master.attendance_id = '$attendance_id' AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.attendance_type_id = '$attendance_type_id'","ORDER BY attendance_break_history_master.attendance_break_history_id DESC LIMIT 1");
                            $breakData = mysqli_fetch_array($getTodayBreak);
                            $is_multipletime_use = $breakData['is_multipletime_use'];

                            if ($is_multipletime_use == '0' && mysqli_num_rows($getTodayBreak) > 0) {
                                // code...
                            } else {

                                $m->set_data('attendance_id',$attendance_id);
                                $m->set_data('attendance_type_id',$attendance_type_id);
                                $m->set_data('break_start_date',$punchInDate);
                                $m->set_data('break_in_time',$punchInTime);
                                $m->set_data('break_in_latitude',$latitude);
                                $m->set_data('break_in_longitude',$longitude);

                                $a = array(
                                    'attendance_id'=>$m->get_data('attendance_id'),
                                    'attendance_type_id' =>$m->get_data('attendance_type_id'),
                                    'break_start_date' =>$m->get_data('break_start_date'),
                                    'break_in_time'=>$m->get_data('break_in_time'),
                                    'break_in_latitude'=>$m->get_data('break_in_latitude'),
                                    'break_in_longitude'=>$m->get_data('break_in_longitude')
                                );
                                if ($attendance_id>0) {
                                    $punchInQry = $d->insert("attendance_break_history_master",$a);
                                }
                            }


                            array_push($successEntry, $_POST['user_id_raw'][$i]);

                        } else {
                            // attendance in code

                        
                            $runInsertQry = true;

                            $d->delete("wfh_master","user_id = '$user_id' AND wfh_start_date = '$punchInDate' AND wfh_day_type = '0'");

                            $punchInDateTime = $punchInDate." ".$punchInTime;
                            $aData = mysqli_fetch_array($getTodayAttendace);

                            $mpAry = json_decode($aData['multiple_punch_in_out_data'],true);

                            $isMultiOut = false;                            

                            if (count($mpAry) > 0) {
                                for ($k=0; $k < count($mpAry) ; $k++) {

                                    if($mpAry[$k]["punch_out_date"] == '' || $mpAry[$k]["punch_out_date"] == '0000-00-00'){  
                                        $isMultiOut = true;
                                        $attStartDate = $mpAry[$k]["punch_in_date"];
                                        $punchInTime = $mpAry[$k]["punch_in_time"];
                                        break;
                                    }
                                }  
                            }


                            if ($isMultiOut == true) {
                                $totalWorkingMinutes = $aData['total_working_minutes'];
                                $lastPunchInDate = $aData['last_punch_in_date'];
                                $lastPunchInTime = $aData['last_punch_in_time']; 

                                $tHours = getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));

                                $tm = explode(':', $tHours.":00");
                                $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                                $total_minutes = $totalWorkingMinutes + $tMinutes;
                            }else{
                                $total_minutes = $aData['total_working_minutes'];
                            }

                            if (mysqli_num_rows($getTodayAttendace) > 0) {


                                $shiftETime =  date('Y-m-d'). ' ' .$shift_end_time;
                                $shiftETime =  strtotime($shiftETime);
                                $ctn =  strtotime(date('Y-m-d H:i:s'));
                                $aDate = $aData['attendance_date_start'];
                                $attendance_id = $aData['attendance_id'];

                                if ($shift_type == "Night") {

                                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($punchInDate))).' '.$max_punch_out_time;
                                    }else{
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($aDate))).' '.$max_punch_out_time;                        
                                    }

                                    $curTimeNew = strtotime($curTimeNew);
                                
                                    if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                                        $date_a = new DateTime($datePunchIN);
                                        $date_b = new DateTime($defaultOutTimeTemp);

                                        $interval = $date_a->diff($date_b);

                                        $day = $interval->format('%days');

                                        if ($day > 0) {
                                            $hour = $day * 24;
                                        }
                                        $hours = $hour + $interval->format('%h');

                                        if ($ctn > $curTimeNew && $hours > 18) {
                                            $rQ = true;
                                        }
                                    }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                                        if ($ctn > $curTimeNew && $ctn > $shiftETime) {

                                            $rQ = true; 
                                        }
                                    }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                                        if ($total_minutes > $max_shift_hour_minutes) {
                                            $rQ = true; 
                                        }
                                    }
                                }else{

                                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $aDate==$curDate) {
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                                        $curTimeNew = strtotime($curTimeNew);
                                    }else{
                                        $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                                        $curTimeNew = strtotime($curTimeNew);
                                    }

                                    if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                        if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                                            if ($ctn > $curTimeNew) {
                                                $rQ = true; 
                                            }
                                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                                            if ($total_minutes > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                                $rQ = true; 
                                            }
                                        }
                                    }
                                }
                            }


                            if ($is_multiple_punch_in == 0 && $shift_type == "Night") {
                                $tempStartDate = $aData['attendance_date_start'];
                                $tempPunchInTime = $aData['punch_in_time'];

                                if ($tempPunchInTime > $shift_start_time) {
                                    $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($tempStartDate)));
                                }
                            }else if ($is_multiple_punch_in == 1 && $shift_type == "Night") {
                                $tempStartDate = $aData['attendance_date_start'];
                                $tempPunchInTime = $aData['punch_in_time'];

                                if ($tempPunchInTime > $shift_start_time) {
                                    $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($tempStartDate)));
                                }else{
                                    $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($punchInDate)));
                                }
                            }

                            $approxShiftEndTime = $tempStartDate.' '.$shift_end_time;
                            $finalMin = (1440 - $per_day_hour_minutes)/2;
                            
                            if ($finalMin < 1) {
                                $finalMin = 1;
                            }

                            $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
                            $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
                            $punchInDateTimeString = strtotime($punchInDateTime);

                            $attendance_id = $aData['attendance_id'];
                            $multiple_punch_in_out_data = $aData['multiple_punch_in_out_data'];

                            if (mysqli_num_rows($getTodayAttendace) > 0  && ($approxShiftEndTimeNewTimeString > $punchInDateTimeString)) {

                                if ($aData['attendance_date_end'] != "0000-00-00") {
                                    $response["message"] = "You have already punched out";               
                                }else if ($rQ == false){
                                    $response["message"] = "You have already punched in";                
                                }                                

                                if ($rQ == false) {

                                    if (($attendance_id == '' || $attendance_id == '0') && $is_multiple_punch_in == 0 && $aData['attendance_date_end'] == "0000-00-00") {
                                        $runInsertQry = false;
                                    }else if ($attendance_id != '' && $is_multiple_punch_in == 1 && $aData['attendance_date_end'] == "0000-00-00") {
                                        $runInsertQry = false;
                                    }else if ($is_multiple_punch_in == 0) {
                                        $runInsertQry = false;
                                    }
                                }
                            }


                            //if ($runInsertQry == true) {

                                $punch_image = "";

                                if ($_FILES["punch_image"]["tmp_name"][$i] != null) {
                                    $file11=$_FILES["punch_image"]["tmp_name"][$i];
                                    $extId = pathinfo($_FILES['punch_image']['name'][$i], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["punch_image"]["name"][$i]);
                                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                                        move_uploaded_file($_FILES["punch_image"]["tmp_name"][$i], "../img/attendance/" . $punch_image);
                                    } 
                                }

                                // Late in

                                $partLI = explode(':', $late_time_start);
                                $min = ($partLI[0]*60) + ($partLI[1]) + ($partLI[2]/60);

                                $defaultINTime = $punchInDate." ".$shift_start_time;
                                $strMin = "+".$min." minutes";
                                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                                if ($late_time_start !='') {
                                    if ($punchInDateTime > $defaultTime) {
                                        $late_in = "1";
                                    }else{
                                        $late_in = "0";
                                    }
                                }else{
                                    $late_in = "0";
                                }

                                if ($rQ == true) {
                                    $attendance_id = 0;
                                } 

                                $tmpD = date('Y-m-d',strtotime($aDate));

                                if ($tmpD == date('Y-m-d')) {
                                    $newDate = $aDate;
                                }else{
                                    $newDate = date('Y-m-d',strtotime("+1 days",strtotime($aDate)));
                                }

                                $approxShiftEndTime = $newDate.' '.$shift_end_time;
                                $finalMin = (1440 - $per_day_hour_minutes)/2;
                                
                                if ($finalMin < 1) {
                                    $finalMin = 1;
                                }

                                $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
                                $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
                                $punchInDateTimeString = strtotime($punchInDateTime);

                                if ($is_multiple_punch_in == 1 && $shift_type == "Day" && $aDate != $punchInDate) {
                                    $attendance_id = 0;
                                }else if ($is_multiple_punch_in == 1 && $shift_type == "Night" && ($approxShiftEndTimeNewTimeString < $punchInDateTimeString)) {
                                    $attendance_id = 0;
                                }else if ($is_multiple_punch_in == 0 && $shift_type == "Night" && ($approxShiftEndTimeNewTimeString < $punchInDateTimeString)) {
                                    $attendance_id = 0;
                                }

                                $offlineSyncTime = date('Y-m-d H:i:s');

                                if ($runInsertQry == true) {

                                    if ($attendance_id == 0 || $attendance_id == '') { 

                                        $punchInTime = $punchInTimeTemp;

                                        $m->set_data('shift_time_id',$shift_time_id);
                                        $m->set_data('total_shift_hours',$per_day_hour);
                                        $m->set_data('society_id',$society_id);
                                        $m->set_data('unit_id',$unit_id);
                                        $m->set_data('user_id',$user_id);
                                        $m->set_data('attendance_date_start',$punchInDate);
                                        $m->set_data('last_punch_in_date',$punchInDate);
                                        $m->set_data('punch_in_time',$punchInTime);
                                        $m->set_data('last_punch_in_time',$punchInTime);
                                        $m->set_data('punch_in_latitude',$punch_in_latitude);
                                        $m->set_data('punch_in_longitude',$punch_in_longitude);
                                        $m->set_data('punch_in_image',$punch_image);
                                        $m->set_data('punch_in_address',$google_address);
                                        $m->set_data('punch_in_face_app_location',$device_location);
                                        $m->set_data('late_in',$late_in);
                                        $m->set_data('is_extra_day',$is_extra_day);
                                        $m->set_data('attendance_status',"1");
                                        $m->set_data('attendance_in_from',"0");
                                        $m->set_data('offline_sync_in',"1");
                                        $m->set_data('offline_sync_in_time',$offlineSyncTime);
                                        $m->set_data('punch_in_branch',$location_name.'');
                                        $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

                                        $dataArray = array();
                                        $multPunchAry = array(
                                            "punch_in_date" => $punchInDate.'',
                                            "punch_in_time" => $punchInTime.'',
                                            "punch_out_date" => "",
                                            "punch_out_time" => "",
                                            "punch_in_image" => $punch_image.'',
                                            "punch_out_image" => "",
                                            "location_name_in" => $location_name.'',
                                            "location_name_out" => "",
                                            "working_hour" => "",
                                            "working_hour_minute" => "",
                                        );

                                        array_push($dataArray,$multPunchAry);
                                        $multPunchDataJson = json_encode($dataArray);

                                        $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);

                                        $a = array(
                                            'shift_time_id'=>$m->get_data('shift_time_id'),
                                            'total_shift_hours'=>$m->get_data('total_shift_hours'),
                                            'society_id' =>$m->get_data('society_id'),
                                            'unit_id'=>$m->get_data('unit_id'),
                                            'user_id'=>$m->get_data('user_id'),
                                            'attendance_date_start'=>$m->get_data('attendance_date_start'),
                                            'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                                            'punch_in_time'=>$m->get_data('punch_in_time'),
                                            'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                                            'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                                            'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                                            'punch_in_image'=>$m->get_data('punch_in_image'),
                                            'late_in'=>$m->get_data('late_in'),
                                            'is_extra_day'=>$m->get_data('is_extra_day'),
                                            'attendance_status'=>$m->get_data('attendance_status'),
                                            'attendance_in_from'=>$m->get_data('attendance_in_from'),
                                            'attendance_created_time'=>$m->get_data('attendance_created_time'),
                                            'punch_in_address'=>$m->get_data('punch_in_address'),
                                            'punch_in_face_app_location'=>$m->get_data('punch_in_face_app_location'),
                                            'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                                            'offline_sync_in'=>$m->get_data('offline_sync_in'),
                                            'offline_sync_in_time'=>$m->get_data('offline_sync_in_time'),
                                            'punch_in_branch'=>$m->get_data('punch_in_branch'),
                                        );



                                        $punchInQry = $d->insert("attendance_master",$a);
                                        $attendance_id = $con->insert_id;
                                    }else if ($is_multiple_punch_in == 1) {
                                        $dataArray = array();
                                        if ($multiple_punch_in_out_data != '') {

                                            $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                                            for ($j=0; $j < count($multiplePunchDataArray) ; $j++) { 
                                                $oldAry = array(
                                                    "punch_in_date" => $multiplePunchDataArray[$j]["punch_in_date"].'',
                                                    "punch_in_time" => $multiplePunchDataArray[$j]["punch_in_time"].'',
                                                    "punch_out_date" => $multiplePunchDataArray[$j]["punch_out_date"].'',
                                                    "punch_out_time" =>$multiplePunchDataArray[$j]["punch_out_time"].'',
                                                    "punch_in_image" => $multiplePunchDataArray[$j]["punch_in_image"].'',
                                                    "punch_out_image" => $multiplePunchDataArray[$j]["punch_out_image"].'',

                                                    "location_name_in" => $multiplePunchDataArray[$j]["location_name_in"].'',

                                                    "location_name_out" => $multiplePunchDataArray[$j]["location_name_out"].'',
                                                    "working_hour" => $multiplePunchDataArray[$j]["working_hour"].'',
                                                    "working_hour_minute" => $multiplePunchDataArray[$j]["working_hour_minute"].'',
                                                );
                                                array_push($dataArray,$oldAry);
                                            }

                                            $multPunchAry = array(
                                                "punch_in_date" => $punchInDate.'',
                                                "punch_in_time" => $punchInTime.'',
                                                "punch_out_date" => "",
                                                "punch_out_time" => "",
                                                "punch_in_image" => $punch_image.'',
                                                "punch_out_image" => "",
                                                "location_name_in" => $location_name."",
                                                "location_name_out" => "",
                                                "working_hour" => "",
                                                "working_hour_minute" => "",
                                            );

                                            array_push($dataArray,$multPunchAry);
                                            $multPunchDataJson = json_encode($dataArray);
                                        }else{
                                            $multPunchAry = array(
                                                "punch_in_date" => $punchInDate.'',
                                                "punch_in_time" => $punchInTime.'',
                                                "punch_out_date" => "",
                                                "punch_out_time" => "",
                                                "punch_in_image" => $punch_image.'',
                                                "punch_out_image" => "",
                                                "location_name_in" => $location_name."",
                                                "location_name_out" => "",
                                                "working_hour" => "",
                                                "working_hour_minute" => "",
                                            );

                                            array_push($dataArray,$multPunchAry);
                                            $multPunchDataJson = json_encode($dataArray);
                                        }

                                        $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                                        $m->set_data('last_punch_in_date',$punchInDate);
                                        $m->set_data('last_punch_in_time',$punchInTime);
                                        $m->set_data('punch_in_branch',$location_name.'');
                                        $m->set_data('attendance_date_end',"");
                                        $m->set_data('punch_out_time',"");
                                        $m->set_data('offline_sync_in',"1");
                                        $m->set_data('offline_sync_in_time',$offlineSyncTime);

                                        $a = array(
                                            'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                                            'punch_out_time'=>$m->get_data('punch_out_time'),
                                            'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                                            'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                                            'attendance_date_end'=>$m->get_data('attendance_date_end'),
                                            'offline_sync_in'=>$m->get_data('offline_sync_in'),
                                            'offline_sync_in_time'=>$m->get_data('offline_sync_in_time'),
                                            'punch_in_branch'=>$m->get_data('punch_in_branch'),
                                        );

                                        $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                                    }

                                    $title = "Attendance Synced";
                                    $description = "Punch In: ".date('h:i A, d M Y',strtotime($punchInDate.' '.$punchInTime))."\nSync Time: ".date('h:i A, d M Y',strtotime($offlineSyncTime));

                                    if ($device == 'android') {
                                        $nResident->noti("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                                    } else if ($device == 'ios') {
                                        $nResident->noti_ios("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                                    }

                                    $notiAry = array(
                                        'society_id'=>$society_id,
                                        'user_id'=>$user_id,
                                        'notification_title'=>$title,
                                        'notification_desc'=>$description,    
                                        'notification_date'=>date('Y-m-d H:i'),
                                        'notification_action'=>'punch_in',
                                        'notification_logo'=>'attendance_tracker.png',
                                        'notification_type'=>'0',
                                    );
                                    
                                    $d->insert("user_notification",$notiAry);
                                }

                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            /*}else{
                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            }*/                            
                        }
                    } else  if ($_POST['inOut'][$i]==1) {


                         // for out Code
                        if ($_POST['breakId'][$i]>0) {

                            /*$total_break_time = getTotalHours($breakStartDate,$breakEndDate,
                            $breakStartTime,$breakEndTime);

                            $time = explode(':', $total_break_time.":00");
                            $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);*/
                            // brek out Code

                            $m->set_data('break_end_date',$punchInDate);
                            $m->set_data('break_out_time',$punchInTime);
                            $m->set_data('break_out_latitude',$latitude);
                            $m->set_data('break_out_longitude',$longitude);

                            $a = array(
                                'break_end_date'=>$m->get_data('break_end_date'),
                                'break_out_time'=>$m->get_data('break_out_time'),
                                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                                'break_out_longitude'=>$m->get_data('break_out_longitude'),
                            );

                            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_type_id = '$attendance_type_id' AND break_end_date='0000-00-00'");
                             array_push($successEntry, $_POST['user_id_raw'][$i]);

                        } else {
                            if (mysqli_num_rows($getTodayAttendace) > 0) {

                                $alreadyPunchOut = false;
                                $immediatePunchOut = false;

                                $attData = mysqli_fetch_array($getTodayAttendace);

                                // Code for Punch Out
                               
                                $attendance_id = $attData['attendance_id'];
                                $extraDayAtt = $attData['is_extra_day'];
                                $attendance_date_start = $attData["attendance_date_start"];
                                $attendanceEndDATE = $attData["attendance_date_end"];
                                $punch_in_time = $attData["punch_in_time"];
                                $todayLateIn  = $attData['late_in'];

                                if (($attendanceEndDATE != '' && $attendanceEndDATE != '0000-00-00')) {
                                    $alreadyPunchOut = true;
                                    $response["message"] = "You have already punched out";               
                                    $response["status"] = "202";
                                    $response['user_full_name'] =$userData["user_full_name"];
                                    $response['society_id'] =$userData["society_id"];
                                    $response['block_id'] =$userData["block_id"];
                                    $response['floor_id'] =$userData["floor_id"];
                                    $response['unit_id'] =$userData["unit_id"];
                                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                                    $response['user_designation'] =$userData["user_designation"];
                                    
                                }
                               
                                $multiple_punch_in_out_data = $attData['multiple_punch_in_out_data'];
                                $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);
                                
                                $pDate = $attStartDate." ".$punch_in_time;

                                $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                                $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                                $date_a = new DateTime($inTimeAMPM);
                                $date_b = new DateTime($dateTimeCurrent);

                                $interval = $date_a->diff($date_b);
                               
                                $hours = $interval->format('%h');
                                $minutes = $interval->format('%i');
                                $sec = $interval->format('%s');

                                if ($sec < 45) {
                                   $sec += 5;
                                }

                                $finalTime =  $attendance_date_start." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                                $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);


                                $datePunchIN = $attendance_date_start ." ".$punch_in_time;
                                $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                                $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

                                if ($datePunchOUT < $datePunchIN) {
                                    $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                                    $response["status"] = "202";
                                    $immediatePunchOut = true;                                    
                                }

                                $punch_image = "";

                                if ($_FILES["punch_image"]["tmp_name"] != null) {
                                    // code...
                                    $file11=$_FILES["punch_image"]["tmp_name"];
                                    $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["punch_image"]["name"]);
                                        $punch_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                                        move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                                    }
                                }


                                if ($early_out_time != '') {
                                    $partEO = explode(':', $early_out_time);
                                    $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
                                }else{
                                    $min = 0;
                                }

                                $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                                $defaultOutTimeTemp = $punchOUTDate." ".$shift_end_time;
                                $strMin = "-".$min." minutes";
                                $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                                $qryLeave = $d->select("leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                                $hasLeave = false;

                                if (mysqli_num_rows($qryLeave) > 0) {

                                    $hasLeave = true;

                                    $leaveData = mysqli_fetch_array($qryLeave);

                                    $leave_id = $leaveData['leave_id'];
                                    $leave_type_id = $leaveData['leave_type_id'];
                                    $paid_unpaid = $leaveData['paid_unpaid'];
                                    $leave_reason = $leaveData['leave_reason'];
                                    $leave_day_type = $leaveData['leave_day_type'];
                                    $half_day_session = $leaveData['half_day_session'];
                                    $auto_leave_reason = $leaveData['auto_leave_reason'];
                                }

                                // Total Working Hour Count

                                $parts = explode(':', $perdayHours);
                                $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                                $halfDayHourMinute = $perDayHourMinute/2;
                                $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;     

                                $dataArray = array();   

                                if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

                                    for ($j=0; $j < count($multiplePunchDataArray) ; $j++) { 
                                        if($multiplePunchDataArray[$j]["punch_out_date"] == '' 
                                            || $multiplePunchDataArray[$j]["punch_out_date"] == '0000-00-00'){

                                            $totalHours1 = getTotalHours($multiplePunchDataArray[$j]["punch_in_date"],$punchOUTDate,
                                                    $multiplePunchDataArray[$j]["punch_in_time"],$punchOUTTime);

                                            $timeHr = explode(':', $totalHours1.":00");
                                            $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                                            $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                                            $oldAry = array(
                                                "punch_in_date" => $multiplePunchDataArray[$j]["punch_in_date"].'',
                                                "punch_in_time" => $multiplePunchDataArray[$j]["punch_in_time"].'',
                                                "punch_out_date" => $punchOUTDate.'',
                                                "punch_out_time" =>$punchOUTTime.'',
                                                "punch_in_image" => $multiplePunchDataArray[$j]["punch_in_image"].'',
                                                "punch_out_image" => $punch_image.'',
                                                "location_name_in" => $multiplePunchDataArray[$j]["location_name_in"].'',
                                                "location_name_out" => $location_name.'',
                                                "working_hour" => $totalHours1.'',
                                                "working_hour_minute" => $totalMinutes.'',
                                            );

                                            array_push($dataArray,$oldAry);
                                        }else{
                                            $oldAry = array(
                                                "punch_in_date" => $multiplePunchDataArray[$j]["punch_in_date"].'',
                                                "punch_in_time" => $multiplePunchDataArray[$j]["punch_in_time"].'',
                                                "punch_out_date" => $multiplePunchDataArray[$j]["punch_out_date"].'',
                                                "punch_out_time" =>$multiplePunchDataArray[$j]["punch_out_time"].'',
                                                "punch_in_image" => $multiplePunchDataArray[$j]["punch_in_image"].'',
                                                "punch_out_image" => $multiplePunchDataArray[$j]["punch_out_image"].'',
                                                "location_name_in" => $multiplePunchDataArray[$j]["location_name_in"].'',
                                                "location_name_out" => $multiplePunchDataArray[$j]["location_name_out"].'',
                                                "working_hour" => $multiplePunchDataArray[$j]["working_hour"].'',
                                                "working_hour_minute" => $multiplePunchDataArray[$j]["working_hour_minute"].'',
                                            );
                                            $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$j]["working_hour_minute"];
                                            array_push($dataArray,$oldAry);
                                        }
                                    }  

                                    $multPunchDataJson = json_encode($dataArray).'';
                                }else{
                                    if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                                        for ($p=0; $p < count($multiplePunchDataArray) ; $p++) { 
                                            if($multiplePunchDataArray[$p]["punch_out_date"] == '' || $multiplePunchDataArray[$p]["punch_out_date"] == '0000-00-00'){
                                                $totalHours3 = getTotalHours($multiplePunchDataArray[$p]["punch_in_date"],$punchOUTDate,
                                                        $multiplePunchDataArray[$p]["punch_in_time"],$punchOUTTime);

                                                $timeHr = explode(':', $totalHours3.":00");
                                                $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                                                $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                                                $oldAry = array(
                                                    "punch_in_date" => $multiplePunchDataArray[$p]["punch_in_date"].'',
                                                    "punch_in_time" => $multiplePunchDataArray[$p]["punch_in_time"].'',
                                                    "punch_out_date" => $punchOUTDate.'',
                                                    "punch_out_time" =>$punchOUTTime.'',
                                                    "punch_in_image" => $multiplePunchDataArray[$p]["punch_in_image"].'',
                                                    "punch_out_image" => $punch_image.'',
                                                    "location_name_in" => $multiplePunchDataArray[$j]["location_name_in"].'',
                                                    "location_name_out" => $location_name.'',
                                                    "working_hour" => $totalHours3.'',
                                                    "working_hour_minute" => $finalTotalMinute.'',
                                                );
                                                array_push($dataArray,$oldAry);
                                            }
                                        }

                                        $multPunchDataJson = json_encode($dataArray).'';
                                    }
                                }

                                if ($is_multiple_punch_in == 0) {

                                    if ($early_out_time!='') {
                                        // code...
                                        if ($datePunchOUT >= $defaultOutTime) {
                                            $early_out = "0";
                                        }else{
                                            $early_out = "1";              
                                        }
                                    }else{
                                        $early_out = "0";
                                    }

                                    if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                                        $early_out = "0";
                                    }

                                    $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                                $punch_in_time,$punchOUTTime);


                                    $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                                    $punch_in_time,$punchOUTTime);

                                    $times[] = $totalHours;

                                    $time = explode(':', $totalHours.":00");
                                    $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
                                }else{
                                    $total_minutes =  $finalTotalMinute;

                                    $hoursPM  = floor($total_minutes/60);
                                    $minutesPM = $total_minutes % 60;

                                    $totalHours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                                    $times[] = $totalHours;

                                    if ($hoursPM > 0 && $minutesPM) {
                                        $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                                    }else if ($hoursPM > 0 && $minutesPM <= 0) {
                                        $totalHoursNames = sprintf('%02d hr', $hoursPM);
                                    }else if ($hoursPM <= 0 && $minutesPM > 0) {
                                        $totalHoursNames = sprintf('%02d min', $minutesPM);
                                    }else{
                                        $totalHoursNames = "No Data";
                                    }
                                }

                                $rQ = false;

                                $response['total_hour'] = $totalHours;

                                $shiftETime =  date('Y-m-d'). ' ' .$shift_end_time;
                                $shiftETime =  strtotime($shiftETime);
                                $ctn =  strtotime(date('Y-m-d H:i:s'));

                                $curDate = date('Y-m-d');

                                if ($shift_type == "Night") {
                                   
                                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($curDate))).' '.$max_punch_out_time;
                                    }else{
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($attendance_date_start))).' '.$max_punch_out_time;                        
                                    }

                                    $curTimeNew = strtotime($curTimeNew);

                                    if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                                        $newCTime = date('Y-m-d H:i:s');

                                        $date_a = new DateTime($datePunchIN);
                                        $date_b = new DateTime($newCTime);

                                        $interval = $date_a->diff($date_b);

                                        $day = $interval->format('%days');

                                        if ($day > 0) {
                                            $hour = $day * 24;
                                        }
                                        $hours = $hour + $interval->format('%h');

                                        if ($ctn > $curTimeNew && $hours >= 18) {
                                            $rQ = true;
                                        }
                                    }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                                        if ($ctn > $curTimeNew && $ctn > $shiftETime) {
                                            $rQ = true; 
                                        }
                                    }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                                        if ($total_minutes > $max_shift_hour_minutes) {
                                            $rQ = true; 
                                        }
                                    }
                                }else{

                                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $attStartDate==$curDate) {
                                        $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                                        $curTimeNew = strtotime($curTimeNew);
                                    }else{
                                        $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                                        $curTimeNew = strtotime($curTimeNew);
                                    }

                                    if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                        if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                                            if ($ctn > $curTimeNew) {
                                                $rQ = true; 
                                            }
                                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                                            if ($total_minutes > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                                $rQ = true; 
                                            }
                                        }
                                    }
                                }


                                if ($rQ == true) {
                                    $response["message"] = "Punch out time limit over $test1";                
                                    $response["status"] = "202";                                   
                                    //echo json_encode($response);
                                    //exit();
                                }

                                /*if ($attData['work_report_id'] == null || $attData['work_report_id'] == '') {

                                    $qSociety1 = $d->count_data_direct("society_id","society_master", "society_id ='$society_id' AND (work_report_on = '1' OR (work_report_on = '2' AND FIND_IN_SET('$block_id',work_report_on_ids)) OR (work_report_on = '3' AND FIND_IN_SET('$floor_id',work_report_on_ids)))");

                                    $work_report_on = false;

                                    if ($qSociety1 > 0) {
                                        $work_report_on = true;
                                    }

                                    if ($user_work_report_on == "1") {
                                        $work_report_on = true;
                                    }else if ($user_work_report_on == "2"){
                                        $work_report_on = false;
                                    }

                                    if($work_report_on == true){                        
                                        $response['work_report_pending'] = $work_report_on;
                                        $response['message'] = "Please add work report from employee app and then punch out";
                                        $response['status'] = "201";

                                        $title = "Face app punch out failed!";
                                        $description = "Please submit your work report and try again!";

                                        if ($device == 'android') {
                                            $nResident->noti("add_work_report","",$society_id,$user_token,$title,$description,$society_id);
                                        } else if ($device == 'ios') {
                                            $nResident->noti_ios("add_work_report","",$society_id,$user_token,$title,$description,$society_id);
                                        }

                                        echo json_encode($response);
                                        exit();
                                    }
                                }*/


                                // Average Working days calculations

                                $avgWorkingDays = $total_minutes/$perDayHourMinute;

                                $avg_working_days = round($avgWorkingDays * 2) / 2;

                                $extra_working_hours_minutes = 0;

                                if ($total_minutes > $perDayHourMinute) {
                                    $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                                    $hours  = floor($extra_working_hours_minutes/60);
                                    $minutes = $extra_working_hours_minutes % 60;

                                    $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
                                }

                                $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

                                $qryBreakData = mysqli_fetch_array($qryBreak);

                                $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

                                if ($total_minutes > $totalBreakinutes) {
                                    $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                                    $hoursPM  = floor($productive_working_hours_minutes/60);
                                    $minutesPM = $productive_working_hours_minutes % 60;

                                    $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                                }else{
                                    $productive_working_hours_minutes = $total_minutes;

                                    $hoursPM  = floor($productive_working_hours_minutes/60);
                                    $minutesPM = $productive_working_hours_minutes % 60;

                                    $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                                }


                                if ($track_user_location == 1) {
                                    $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                                    $query11Data=mysqli_fetch_array($query11);

                                    if ($query11Data['SUM(last_distance)'] > 0) {
                                        $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                                    }else{
                                        $total_travel_meter = '0';
                                    }
                                }

                                if ($rQ == false && $alreadyPunchOut == false && $immediatePunchOut == false) {
                                    // code...

                                    $offlineSyncTime = date('Y-m-d H:i:s');

                                    $m->set_data('attendance_date_end',$punchOUTDate);
                                    $m->set_data('punch_out_time',$punchOUTTime);
                                    $m->set_data('punch_out_latitude',$punch_in_latitude);
                                    $m->set_data('punch_out_longitude',$punch_in_longitude);
                                    $m->set_data('punch_out_image',$punch_image);
                                    $m->set_data('early_out',$early_out);
                                    $m->set_data('avg_working_days',$avg_working_days);
                                    $m->set_data('total_working_hours',$totalHours);
                                    $m->set_data('total_working_minutes',$total_minutes);
                                    $m->set_data('total_travel_meter',$total_travel_meter);
                                    $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                                    $m->set_data('extra_working_hours',$extra_working_hours);
                                    $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                                    $m->set_data('punch_out_address',$google_address);
                                    $m->set_data('punch_out_face_app_location',$device_location);
                                    $m->set_data('productive_working_hours',$productive_working_hours);
                                    $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                                    $m->set_data('attendance_out_from',"0");
                                    $m->set_data('offline_sync_out',"1");
                                    $m->set_data('offline_sync_out_time',$offlineSyncTime);
                                    $m->set_data('punch_out_branch',$location_name.'');

                                    $a = array(
                                        'attendance_date_end'=>$m->get_data('attendance_date_end'),
                                        'punch_out_time'=>$m->get_data('punch_out_time'),
                                        'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                                        'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                                        'punch_out_image'=>$m->get_data('punch_out_image'),
                                        'early_out'=>$m->get_data('early_out'),
                                        'avg_working_days'=>$m->get_data('avg_working_days'),
                                        'total_working_hours'=>$m->get_data('total_working_hours'),
                                        'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                                        'total_working_minutes'=>$m->get_data('total_working_minutes'),
                                        'attendance_out_from'=>$m->get_data('attendance_out_from'),
                                        'total_travel_meter'=>$m->get_data('total_travel_meter'),
                                        'extra_working_hours'=>$m->get_data('extra_working_hours'),
                                        'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                                        'punch_out_address'=>$m->get_data('punch_out_address'),
                                        'punch_out_face_app_location'=>$m->get_data('punch_out_face_app_location'),
                                        'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                                        'productive_working_hours'=>$m->get_data('productive_working_hours'),
                                        'offline_sync_out'=>$m->get_data('offline_sync_out'),
                                        'offline_sync_out_time'=>$m->get_data('offline_sync_out_time'),
                                        'punch_out_branch'=>$m->get_data('punch_out_branch'),
                                    );

                                    $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                                }

                                if ($punchOutQry == true) {

                                    array_push($successEntry, $_POST['user_id_raw'][$i]);

                                    $title = "Attendance Synced";
                                    $description = "Punch Out: ".date('h:i A, d M Y',strtotime($punchOUTDate.' '.$punchOUTTime))."\nSync Time: ".date('h:i A, d M Y',strtotime($offlineSyncTime));

                                    if ($device == 'android') {
                                        $nResident->noti("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                                    } else if ($device == 'ios') {
                                        $nResident->noti_ios("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                                    }

                                    $notiAry = array(
                                        'society_id'=>$society_id,
                                        'user_id'=>$user_id,
                                        'notification_title'=>$title,
                                        'notification_desc'=>$description,    
                                        'notification_date'=>date('Y-m-d H:i'),
                                        'notification_action'=>'punch_in',
                                        'notification_logo'=>'attendance_tracker.png',
                                        'notification_type'=>'0',
                                    );
                                    
                                    $d->insert("user_notification",$notiAry);

                                    // Check Half Day


                                    $leaveValue = false;

                                    if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                                        $leaveValue = true;
                                    } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                                        $leaveValue = true;
                                    }

                                    $alreadyLeaveQryCheck = false; 

                                    $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                                    $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                                    if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != '00:00:00') {
                                        $noLeave = true;
                                    }else{
                                        $noLeave = false;
                                    }

                                    $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                                    
                                    if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                                        $leave_total_days = 1;

                                        if ($total_minutes < $maximum_halfday_hours_min) {
                                            $leaveType = "0";
                                            $half_day_session = "0"; 
                                            $is_leave = "1";         
                                            $extra_day_leave_type = "1";         
                                        }else{
                                            $leaveType = "1";
                                            $is_leave = "2";
                                            $extra_day_leave_type = "2";
                                        }   

                                        if ($extraDayAtt == 1) {
                                            $is_leave = "0";
                                        }else{
                                            $extra_day_leave_type = "0";
                                        }                     

                                        if ($hasLeave == true) {

                                            $title = "Auto Leave Applied"." - ".$leaveTypeName;
                                            $titleMessage = "Working hours not completed (".timeFormat($totalHours).")";
                                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                            $m->set_data('leave_type_id',$leave_type_id);
                                            $m->set_data('paid_unpaid',$paid_unpaid);
                                            $m->set_data('leave_day_type',$leaveType);
                                            $m->set_data('half_day_session',$half_day_session);
                                            
                                            if ($auto_leave_reason != '') {
                                                $m->set_data('auto_leave_reason',$titleMessage);

                                                $a = array(
                                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                                    'half_day_session'=>$m->get_data('half_day_session'),
                                                    'auto_leave_reason'=>$m->get_data('auto_leave_reason')
                                                );
                                            }else{
                                                $a = array(
                                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                                    'half_day_session'=>$m->get_data('half_day_session')
                                                );
                                            }

                                            $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");

                                            /*if ($device == 'android') {
                                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            } else if ($device == 'ios') {
                                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            }*/

                                            //d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                            
                                        }else{

                                            if ($leaveType == 0) {
                                                $workingDaysTotal = "0";
                                                $title = "Auto Leave Applied - Full Day";
                                            }else{
                                                $workingDaysTotal = "0.5";
                                                $title = "Auto Leave Applied - Half Day";
                                            }
                                            
                                            $titleMessage = "Working hours not completed (".timeFormat($totalHours).")";
                                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                            $m->set_data('leave_type_id',"0");
                                            $m->set_data('society_id',$society_id);
                                            $m->set_data('paid_unpaid',"1");
                                            $m->set_data('unit_id',$unit_id);
                                            $m->set_data('user_id',$user_id);
                                            $m->set_data('auto_leave_reason',$titleMessage);
                                            $m->set_data('leave_start_date',$attendance_date_start);
                                            $m->set_data('leave_end_date',$punchOUTDate);
                                            $m->set_data('leave_total_days',$leave_total_days);
                                            $m->set_data('leave_day_type',$leaveType);
                                            $m->set_data('leave_status',"1");
                                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                            $a = array(
                                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                                'society_id' =>$m->get_data('society_id'),
                                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                                'unit_id'=>$m->get_data('unit_id'),
                                                'user_id'=>$m->get_data('user_id'),
                                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                                'leave_status'=>$m->get_data('leave_status'),
                                                'leave_created_date'=>$m->get_data('leave_created_date'),
                                            );

                                            $leaveQry = $d->insert("leave_master",$a);

                                            $alreadyLeaveQryCheck = true;

                                            
                                            if ($device == 'android') {
                                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            } else if ($device == 'ios') {
                                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            }

                                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                        }

                                        $m->set_data('auto_leave',"1");
                                        $m->set_data('avg_working_days',$workingDaysTotal);
                                        $m->set_data('is_leave',$is_leave);
                                        $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                                        $a1 = array(
                                            'auto_leave'=>$m->get_data('auto_leave'),
                                            'avg_working_days'=>$m->get_data('avg_working_days'),
                                            'is_leave'=>$m->get_data('is_leave'),
                                            'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                                        );

                                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                                    }else if($noLeave == true && $hasLeave == true){
                                        $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                                        $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                                        $title = "Applied Leave Cancelled";
                                        $description = "For Date : ".$attendance_date_start;

                                        if ($device == 'android') {
                                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                        } else if ($device == 'ios') {
                                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                        }

                                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker"," leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                    }

                                    $startMonthDate = date('Y-m-01');
                                    $endMonthDate = date('Y-m-t');


                                    $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                                    $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                                    $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                                    if ($hasLeave == false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                                        $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                                        $m->set_data('leave_type_id',"0");
                                        $m->set_data('society_id',$society_id);
                                        $m->set_data('paid_unpaid',"1");
                                        $m->set_data('unit_id',$unit_id);
                                        $m->set_data('user_id',$user_id);
                                        $m->set_data('auto_leave_reason',$titleMessage);
                                        $m->set_data('leave_start_date',$attendance_date_start);
                                        $m->set_data('leave_end_date',$punchOUTDate);
                                        $m->set_data('leave_total_days',"1");
                                        $m->set_data('leave_day_type',"1");
                                        $m->set_data('leave_status',"1");
                                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                        $a = array(
                                            'leave_type_id'=>$m->get_data('leave_type_id'),
                                            'society_id' =>$m->get_data('society_id'),
                                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                            'unit_id'=>$m->get_data('unit_id'),
                                            'user_id'=>$m->get_data('user_id'),
                                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                            'leave_start_date'=>$m->get_data('leave_start_date'),
                                            'leave_end_date'=>$m->get_data('leave_end_date'),
                                            'leave_total_days'=>$m->get_data('leave_total_days'),
                                            'leave_day_type'=>$m->get_data('leave_day_type'),
                                            'leave_status'=>$m->get_data('leave_status'),
                                            'leave_created_date'=>$m->get_data('leave_created_date'),
                                        );

                                        $leaveQry = $d->insert("leave_master",$a);

                                        $title = "Auto Leave Applied - Half Day";

                                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                        if ($device == 'android') {
                                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                        } else if ($device == 'ios') {
                                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                        }

                                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                                        if ($extraDayAtt == 1) {
                                            $is_leave = "0";
                                        }else{
                                            $extra_day_leave_type = "2";
                                        }

                                        $m->set_data('auto_leave',"1");
                                        $m->set_data('avg_working_days',"0.5");
                                        $m->set_data('is_leave',$is_leave);
                                        $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                                        $a1 = array(
                                            'auto_leave'=>$m->get_data('auto_leave'),
                                            'avg_working_days'=>$m->get_data('avg_working_days'),
                                            'is_leave'=>$m->get_data('is_leave'),
                                            'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                                        );

                                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                                    }

                                    // END checking half day

                                    $attHisQry = $d->selectRow("attendance_break_history_id,break_end_date,break_out_time,break_start_date,break_in_time","attendance_break_history_master","attendance_id = '$attendance_id' AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");

                                    if (mysqli_num_rows($attHisQry) > 0) {
                                        while($attData = mysqli_fetch_array($attHisQry)){
                                            $attendance_break_history_id = $attData['attendance_break_history_id'];
                                            $break_start_date = $attData['break_start_date'];
                                            $break_in_time = $attData['break_in_time'];
                                            $break_end_date = date('Y-m-d');
                                            $break_out_time = date('H:i:s');

                                            $total_break_time = getTotalHours($break_start_date,$break_end_date,
                                                $break_in_time,$break_out_time);

                                            $time = explode(':', $total_break_time.":00");
                                            $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                                            $m->set_data('break_end_date',$break_end_date);
                                            $m->set_data('break_out_time',$break_out_time);
                                            $m->set_data('total_break_time',$total_break_time);
                                            $m->set_data('total_break_time_minutes',$total_break_time_minutes);

                                            $a = array(
                                                'break_end_date'=>$m->get_data('break_end_date'),
                                                'break_out_time'=>$m->get_data('break_out_time'),
                                                'total_break_time'=>$m->get_data('total_break_time'),
                                                'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
                                            );

                                            $qry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id' AND attendance_id = '$attendance_id'");
                                        }
                                    }
                                }else{
                                    array_push($successEntry, $_POST['user_id_raw'][$i]);
                                } 
                            } else {
                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            }
                        }
                    }
                } else {
                    array_push($successEntry, $_POST['user_id_raw'][$i]);
                }
            }

            $response["success_ids"] = implode(",", $successEntry);
            $response["message"] = "Data Sync Successfully";
            $response["status"] = "200";
            echo json_encode($response);  
        } else if($_POST['updateDeviceLocation']=="updateDeviceLocation" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $location_name = $device_location;

            $faceDeviceQry = $d->selectRow("face_app_device_id","face_app_device_master","device_location = '' AND face_app_device_id = '$face_app_device_id'");

            if (mysqli_num_rows($faceDeviceQry) > 0) {
                
                $updateAry = array("device_location" => $location_name);

                $d->update("face_app_device_master",$updateAry,"face_app_device_id = '$face_app_device_id'");
            }

            
            $response["message"] = "Data Updated Successfully";
            $response["status"] = "200";
            echo json_encode($response);  
        }         
    }else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }
}

function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "00:00";
    }
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function timeFormat($time){
    $time = explode(':', $time);
    $hours = $time[0];
    $minutes = $time[1];
    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}
?>