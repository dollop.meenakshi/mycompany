<?php
include_once 'lib.php'; 

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
    $societyLngName=  $xml->string->society;

  
    if ($key==$keydb ) { 
    
        $response = array();
        extract(array_map("test_input" , $_POST));

        $currentDate = date('Y-m-d');

        if($_POST['getBlocks']=="getBlocks" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $blockQry=$d->select("block_master","society_id = '$society_id' AND block_status = '0'","ORDER BY block_sort ASC");
            $response["blocks"] = array();
            $response["floors"] = array();

            if(mysqli_num_rows($blockQry) > 0){


                while($blockData=mysqli_fetch_array($blockQry)) {

                    $blocks = array(); 

                    $blocks["block_id"]=$blockData['block_id'];
                    $blocks["block_name"]=$blockData['block_name'];

                    array_push($response["blocks"], $blocks);
                }

            }

            $floor_data=$d->select("floors_master","society_id = '$society_id' AND floor_status = '0'","ORDER BY floor_sort ASC");

            if(mysqli_num_rows($floor_data) > 0){


                while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $floors = array(); 

                    $floors["floor_id"]=$data_floor_list['floor_id'];
                    $floors["block_id"]=$data_floor_list['block_id'];
                    $floors["floor_name"]=$data_floor_list['floor_name'];

                    array_push($response["floors"], $floors);
                }

                $response["message"]="Success.";
                $response["status"]="200";
                echo json_encode($response);
            
            }
        }else if($_POST['verifyPin']=="verifyPin" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qry=$d->select("bms_admin_master","admin_active_status = '0' AND is_attendance_taking_on = '1' AND admin_pin = '$admin_pin' AND society_id = '$society_id'");

            if(mysqli_num_rows($qry) > 0){
                $response["message"]="Verified";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Invalid PIN";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['cancelAttendance']=="cancelAttendance" && $attendance_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $attQry = $d->select("attendance_master,users_master","attendance_master.attendance_id = '$attendance_id' AND users_master.user_id = attendance_master.user_id");
            $attData = mysqli_fetch_array($attQry);

            $attUserId = $attData['user_id'];
            $attendance_date_end = $attData['attendance_date_end'];
            $attendance_date_start = $attData['attendance_date_start'];
            $punch_in_time = $attData['punch_in_time'];
            $punch_out_time = $attData['punch_out_time'];
            $user_token = $attData['user_token'];
            $device = $attData['device'];
            $user_first_name = $attData['user_first_name'];

            if ($in_out_status == "0") {
                $qryDelete = $d->delete("attendance_master","attendance_id = '$attendance_id'");                

                $title = "Punch in cancelled from face app";                
                $description = "For Date : ".date('d-m-Y',strtotime($attendance_date_start)).' '.date('h:i A',strtotime($punch_in_time));
                $tabPosition = "0";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"attendance","attendance_tracker.png",$tabPosition,"user_id = '$user_id'");

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);

            }else if ($in_out_status == "1"){

                $m->set_data('attendance_date_end',"");
                $m->set_data('punch_out_time',"");
                $m->set_data('punch_out_latitude',"");
                $m->set_data('punch_out_longitude',"");
                $m->set_data('punch_out_image',"");
                $m->set_data('early_out',"0");
                $m->set_data('auto_leave',"0");
                $m->set_data('attendance_out_from',"");

                $a = array(
                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                    'punch_out_time'=>$m->get_data('punch_out_time'),
                    'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                    'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                    'punch_out_image'=>$m->get_data('punch_out_image'),
                    'early_out'=>$m->get_data('early_out'),
                    'auto_leave'=>$m->get_data('auto_leave'),
                    'attendance_out_from'=>$m->get_data('attendance_out_from')
                );

                $punchOutQryUpdate = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                $qryDelete = $d->delete("leave_master","user_id = '$attUserId' AND leave_type_id = '0' AND leave_start_date = '$attendance_date_end'");

                $title = "Punch out cancelled from face app";                
                $description = "For Date : ".date('d-m-Y',strtotime($attendance_date_end)).' '.date('h:i A',strtotime($punch_out_time));
                $tabPosition = "0";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"attendance","attendance_tracker.png",$tabPosition,"user_id = '$user_id'");

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="Something went wrong, try again!";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['cancelBreak']=="cancelBreak" && $attendance_break_history_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
            $userData = mysqli_fetch_array($checkShift);
            $shift_time_id = $userData['shift_time_id'];
            $unit_id = $userData['unit_id'];

            if ($break_in_out_status == "0") {
                $qryDelete = $d->delete("attendance_break_history_master","attendance_break_history_id = '$attendance_break_history_id'");

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);
            }else if ($break_in_out_status == "1"){

                $attQry = $d->select("attendance_break_history_master","attendance_break_history_id = '$attendance_break_history_id'");
                $attData = mysqli_fetch_array($attQry);

                $attUserId = $attData['user_id'];
                $attendance_date_end = $attData['attendance_date_end'];

                $m->set_data('break_end_date',"");
                $m->set_data('break_out_time',"");
                $m->set_data('break_out_latitude',"");
                $m->set_data('break_out_longitude',"");
                $a = array(
                    'break_end_date'=>$m->get_data('break_end_date'),
                    'break_out_time'=>$m->get_data('break_out_time'),
                    'break_out_latitude'=>$m->get_data('break_out_latitude'),
                    'break_out_longitude'=>$m->get_data('break_out_longitude'),
                );

                $bQry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="Something went wrong, try again!";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getMembersNew']=="getMembersNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $qrySoc = $d->selectRow("save_attendance_image","society_master","society_id = '$society_id'");
            $socData = mysqli_fetch_array($qrySoc);

            if ($socData['save_attendance_image'] == "0") {
                $response['take_attendance_image'] = false;
            }else{
                $response['take_attendance_image'] = true;
            }

            $response['face_accuracy'] = "0.87";

            if ($add_face == "true") {
                $appendQry = " AND user_face_id != ''";
            }else if ($add_face == "false") {
                $appendQry = " AND user_face_id = ''";
            }

            if($floor_id != ''){
                $floorQry = " AND floor_id = '$floor_id'";
            }

            if($user_id != ''){
                $userQry = " AND user_id = '$user_id'";
            }

            $unit_data=$d->select("users_master","society_id ='$society_id' AND delete_status='0' AND user_status='1' $appendQry $floorQry $userQry");

            if (mysqli_num_rows($unit_data) > 0) {

                $response["employees"] =array();

                while($data_units_list=mysqli_fetch_array($unit_data)) {
                     
                    $employees = array(); 

                    $data_units_list = array_map("html_entity_decode", $data_units_list);
                      
                    $user_full_name= $data_units_list['user_full_name'];

                    $units_id=$data_units_list['unit_id'];

                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["unit_id"]=$data_units_list['unit_id'].'';
                    $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                    $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                    $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                    $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                    $employees["society_id"]=$data_units_list['society_id'];
                    $employees["shift_time_id"]=$data_units_list['shift_time_id'];
                    $employees["designation"]=$data_units_list['user_designation'];
                     
                    if ($data_units_list['user_profile_pic'] != '') {
                        $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $employees["user_face_id"]=$data_units_list['user_face_id'].'';
                    $employees["user_face_data"]=$data_units_list['user_face_data'].'';
                    $employees["face_added_date"]=date("d M Y h:i A", strtotime($data_units_list['face_added_date'].''));

                    if ($data_units_list['face_data_image'] != '') {
                        $employees["face_data_image"] = $base_url . "img/attendance_face_image/" . $data_units_list['face_data_image'];
                    } else {
                        $employees["face_data_image"] = "";
                    }

                    if ($data_units_list['face_data_image_two'] != '' && file_exists("../img/attendance_face_image/$data_units_list[face_data_image_two]")) {
                        $employees["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $data_units_list['face_data_image_two'];
                        array_push($response["employees"], $employees);
                    } 

                }

                $response["backLightControll"] = array();
                for ($i=0; $i <2 ; $i++) { 
                    $backLightControll = array();
                    if ($i==0) {
                        $backLightControll['start_time'] = "08:30";
                        $backLightControll['end_time'] = "11:00";
                    } else if ($i==1) {
                        $backLightControll['start_time'] = "17:30";
                        $backLightControll['end_time'] = "20:30";
                    }

                    array_push($response["backLightControll"], $backLightControll);

                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);


        
            }else{

                $response["message"]="Get Member Fail.";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getMembersNewAddFace']=="getMembersNewAddFace" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $qrySoc = $d->selectRow("save_attendance_image","society_master","society_id = '$society_id'");
            $socData = mysqli_fetch_array($qrySoc);

            if ($socData['save_attendance_image'] == "0") {
                $response['take_attendance_image'] = false;
            }else{
                $response['take_attendance_image'] = true;
            }

            $response['face_accuracy'] = "0.87";

            // if ($add_face == "true") {
            //     $appendQry = " AND user_face_id != ''";
            // }else if ($add_face == "false") {
            //     $appendQry = " AND user_face_id = ''";
            // }

            if($floor_id != ''){
                $floorQry = " AND floor_id = '$floor_id'";
            }

            if($user_id != ''){
                $userQry = " AND user_id = '$user_id'";
            }

            $unit_data=$d->select("users_master","society_id ='$society_id' AND delete_status='0' AND user_status='1' $appendQry $floorQry $userQry");

            if (mysqli_num_rows($unit_data) > 0) {

                $response["employees"] =array();

                while($data_units_list=mysqli_fetch_array($unit_data)) {
                     
                    $employees = array(); 

                    $data_units_list = array_map("html_entity_decode", $data_units_list);
                      
                    $user_full_name= $data_units_list['user_full_name'];

                    $units_id=$data_units_list['unit_id'];

                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["unit_id"]=$data_units_list['unit_id'].'';
                    $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                    $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                    $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                    $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                    $employees["society_id"]=$data_units_list['society_id'];
                    $employees["shift_time_id"]=$data_units_list['shift_time_id'];
                    $employees["designation"]=$data_units_list['user_designation'];
                     
                    if ($data_units_list['user_profile_pic'] != '') {
                        $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $employees["user_face_id"]=$data_units_list['user_face_id'].'';
                    $employees["user_face_data"]=$data_units_list['user_face_data'].'';
                    $employees["face_added_date"]=date("d M Y h:i A", strtotime($data_units_list['face_added_date'].''));

                    if ($data_units_list['face_data_image'] != '') {
                        $employees["face_data_image"] = $base_url . "img/attendance_face_image/" . $data_units_list['face_data_image'];
                    } else {
                        $employees["face_data_image"] = "";
                    }

                    $employees["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $data_units_list['face_data_image_two'];
                        array_push($response["employees"], $employees);

                }

                $response["backLightControll"] = array();
                for ($i=0; $i <2 ; $i++) { 
                    $backLightControll = array();
                    if ($i==0) {
                        $backLightControll['start_time'] = "08:30";
                        $backLightControll['end_time'] = "11:00";
                    } else if ($i==1) {
                        $backLightControll['start_time'] = "17:30";
                        $backLightControll['end_time'] = "20:30";
                    }

                    array_push($response["backLightControll"], $backLightControll);

                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);


        
            }else{

                $response["message"]="Get Member Fail.";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['checkQRCode']=="checkQRCode" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $newDateTime = date('Ymdhis',strtotime('+20 seconds'));
            $currentTimeStamp = date('Ymdhis',strtotime('-10 seconds'));

            if ($date_time >= $currentTimeStamp && $date_time <= $newDateTime) {
                $unit_data=$d->select("users_master","society_id ='$society_id' AND delete_status='0' AND user_status='1' AND user_id = '$user_id'");

                if (mysqli_num_rows($unit_data) > 0) {

                    $data_units_list=mysqli_fetch_array($unit_data);

                    $data_units_list = array_map("html_entity_decode", $data_units_list);
                      
                    $user_full_name= $data_units_list['user_full_name'];
                    $units_id=$data_units_list['unit_id'];

                    $response["user_id"]=$data_units_list['user_id'].'';
                    $response["unit_id"]=$data_units_list['unit_id'].'';
                    $response["user_first_name"]=$data_units_list['user_first_name'].'';
                    $response["user_last_name"]=$data_units_list['user_last_name'].'';
                    $response["user_mobile"]=$data_units_list['user_mobile'].'';
                    $response["user_full_name"]=html_entity_decode($user_full_name).'';
                    $response["society_id"]=$data_units_list['society_id'];
                    $response["shift_time_id"]=$data_units_list['shift_time_id'];
                    $response["designation"]=$data_units_list['user_designation'];
                     
                    if ($data_units_list['user_profile_pic'] != '') {
                        $response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $response["message"]="Success";
                    $response["status"]="200";
                    echo json_encode($response);
                }else{

                    $response["message"]="Get Member Fail.";
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else{
                $response["message"]="Invalid QR Code";
                $response["status"]="201";
                echo json_encode($response);
                exit(); 
            }
   
        }else if($_POST['addEmployeeFace']=="addEmployeeFace" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $face_data_image = "";

            if ($_FILES["face_data_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image"]["name"]);
                    $face_data_image = "face_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image"]["tmp_name"], "../img/attendance_face_image/" . $face_data_image);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $face_data_image_two = "";

            if ($_FILES["face_data_image_two"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image_two"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image_two']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image_two"]["name"]);
                    $face_data_image_two = "face_two_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image_two"]["tmp_name"], "../img/attendance_face_image/" . $face_data_image_two);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $face_added_date = date('Y-m-d H:i:s');

            $m->set_data('user_face_id',$user_face_id);
            $m->set_data('user_face_data',$user_face_data);
            $m->set_data('face_data_image',$face_data_image);
            $m->set_data('face_data_image_two',$face_data_image_two);
            $m->set_data('face_added_date',$face_added_date);

            $a = array(
                'user_face_id'=>$m->get_data('user_face_id'),
                'user_face_data'=>$m->get_data('user_face_data'),
                'face_data_image'=>$m->get_data('face_data_image'),
                'face_data_image_two'=>$m->get_data('face_data_image_two'),
                'face_added_date'=>$m->get_data('face_added_date'),
            );

            $qry = $d->update("users_master",$a,"user_id = '$user_id'");

            if ($qry == true) {

                $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0'");
                $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1'");
                $title = "sync_data";
                $description = "Face Data Updated on ".date('d-M-y h:i A');
                $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
                $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

                
                $response["message"] = "Employee face added";
                $response["face_added_date"] = $face_added_date;
                $response["face_image_one"] = $base_url.'img/attendance_face_image/'.$face_data_image;
                $response["face_image_two"] = $base_url.'img/attendance_face_image/'.$face_data_image_two;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['newFaceRequest']=="newFaceRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $face_data_image = "";

            if ($_FILES["face_data_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image"]["name"]);
                    $face_data_image = "face_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image"]["tmp_name"], "../img/attendance_face_image/" . $face_data_image);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $face_data_image_two = "";

            if ($_FILES["face_data_image_two"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image_two"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image_two']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image_two"]["name"]);
                    $face_data_image_two = "face_two_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image_two"]["tmp_name"], "../img/attendance_face_image/" . $face_data_image_two);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('user_face_data',$user_face_data);
            $m->set_data('face_data_image',$face_data_image);
            $m->set_data('face_data_image_two',$face_data_image_two);
            $m->set_data('face_request_date',date('Y-m-d H:i:s'));

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'user_id'=>$m->get_data('user_id'),
                'user_face_data'=>$m->get_data('user_face_data'),
                'face_data_image'=>$m->get_data('face_data_image'),
                'face_data_image_two'=>$m->get_data('face_data_image_two'),
                'face_request_date'=>$m->get_data('face_request_date'),
            );

            $qry = $d->insert("user_face_data_request",$a);

            if ($qry == true) {
                
                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['removeEmployeeFace']=="removeEmployeeFace" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('user_face_id',"");
            $m->set_data('user_face_data',"");
            $m->set_data('face_data_image',"");
            $m->set_data('face_added_date',"");
            $m->set_data('face_data_image_two',"");

            $a = array(
                'user_face_id'=>$m->get_data('user_face_id'),
                'user_face_data'=>$m->get_data('user_face_data'),
                'face_data_image'=>$m->get_data('face_data_image'),
                'face_added_date'=>$m->get_data('face_added_date'),
                'face_data_image_two'=>$m->get_data('face_data_image_two'),
            );

            $qry = $d->update("users_master",$a,"user_id = '$user_id'");

            if ($qry == true) {

                $fcmArray=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='0' ");
                $fcmArrayIos=$d->get_android_fcm("face_app_device_master"," user_token!='' AND society_id='$society_id' AND device_type='1' ");
                $title = "sync_data";
                $description = "Face Data Updated on ".date('d-M-y h:i A');
                $nResident->noti("",'',$society_id,$fcmArray,$title,$description,"");
                $nResident->noti_ios("",'',$society_id,$fcmArrayIos,$title,$description,"");

                $response["message"] = "Employee face removed";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getAttendanceDataNew']=="getAttendanceDataNew" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
            $userData = mysqli_fetch_array($checkShift);
            $shift_time_id = $userData['shift_time_id'];
            $unit_id = $userData['unit_id'];
            $block_id = $userData['block_id'];
            $floor_id = $userData['floor_id'];
            $user_work_report_on = $userData['user_work_report_on'];
            $user_full_name = $userData['user_full_name'];
            $user_profile_pic = $userData['user_profile_pic'];
            $user_designation = $userData['user_designation'];
            $user_token = $userData['user_token'];
            $track_user_location = $userData['track_user_location'];
            $device = $userData['device'];
            $user_first_name = $userData['user_first_name'];

            if ($checkShift == true && $shift_time_id == '0') {
                $response["message"] = "Your shift is missing, please contact to admin";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            if (mysqli_num_rows($shiftQ) > 0) {
                $shiftData = mysqli_fetch_array($shiftQ);
                

                $perdayHours = $shiftData['per_day_hour'];
                $shift_start_time = $shiftData['shift_start_time'];
                $per_day_hour = $shiftData['per_day_hour'];
                $shift_end_time = $shiftData['shift_end_time'];
                $late_time_start = $shiftData['late_time_start'];
                $early_out_time = $shiftData['early_out_time'];
                $half_day_time_start = $shiftData['half_day_time_start'];
                $halfday_before_time = $shiftData['halfday_before_time'];
                $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                $shift_type = $shiftData['shift_type'];
                $maximum_in_out = $shiftData['maximum_in_out'];

                $shiftData = array(
                    "shift_time_id" => $shift_time_id,
                    "per_day_hour" => $per_day_hour,
                    "shift_start_time" => $shift_start_time,
                    "shift_end_time" => $shift_end_time,
                );

                $response['shift_data'] = $shiftData;
            }

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');
            $punchOUTTime = date('H:i:s');
            $punchOUTDate = date('Y-m-d');
            $attendance_date_start = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            if ($shift_type == "Night") {

                $attendance_date_previous_day = date('Y-m-d',strtotime('-1 days'));

                $varQry = " AND (attendance_master.attendance_date_start = '$attendance_date_previous_day' OR attendance_master.attendance_date_start = '$attendance_date_start')";

                $varQryTmp = " AND ((attendance_master.attendance_date_start = '$attendance_date_previous_day' AND attendance_master.attendance_date_end = '0000-00-00') OR attendance_master.attendance_date_start = '$attendance_date_start' AND attendance_master.attendance_date_end != '0000-00-00')"; 
            }else{
                $varQry = " AND (attendance_master.attendance_date_start = '$attendance_date_start')";

                $varQryTmp = " AND (attendance_master.attendance_date_start = '$attendance_date_start')"; 
            }


            $getTodayAttendace = $d->selectRow("users_master.*,floors_master.*,attendance_master.*,work_report_master.work_report_id","users_master,floors_master,attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND attendance_master.user_id = work_report_master.user_id","attendance_master.user_id = '$user_id' AND attendance_master.shift_time_id = '$shift_time_id'  AND attendance_master.attendance_date_end = '0000-00-00' AND attendance_master.punch_out_time = '00:00:00' AND attendance_master.user_id = users_master.user_id AND users_master.floor_id = floors_master.floor_id $varQry","ORDER BY attendance_master.attendance_id DESC LIMIT 1");

            if (mysqli_num_rows($getTodayAttendace) > 0) {

                $attData = mysqli_fetch_array($getTodayAttendace);

                if ($attData['work_report_id'] == null || $attData['work_report_id'] == '') {

                    $qSociety1 = $d->count_data_direct("society_id","society_master", "society_id ='$society_id' AND (work_report_on = '1' OR (work_report_on = '2' AND FIND_IN_SET('$block_id',work_report_on_ids)) OR (work_report_on = '3' AND FIND_IN_SET('$floor_id',work_report_on_ids)))");

                    $work_report_on = false;

                    if ($qSociety1 > 0) {
                        $work_report_on = true;
                    }

                    if ($user_work_report_on == "1") {
                        $work_report_on = true;
                    }else if ($user_work_report_on == "2"){
                        $work_report_on = false;
                    }

                    if($work_report_on == true){                        
                        $response['work_report_pending'] = $work_report_on;
                        $response['message'] = "Please add work report from employee app and then punch out";
                        $response['status'] = "201";
                        echo json_encode($response);
                        exit();
                    }
                }


                // Code for Punch Out
               
                $attendance_id = $attData['attendance_id'];
                $attendance_date_start = $attData["attendance_date_start"];
                $punch_in_time = $attData["punch_in_time"];
                $todayLateIn  = $attData['late_in'];
                
                $pDate = $attStartDate." ".$punch_in_time;

                $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                $date_a = new DateTime($inTimeAMPM);
                $date_b = new DateTime($dateTimeCurrent);

                $interval = $date_a->diff($date_b);
               
                $hours = $interval->format('%h');
                $minutes = $interval->format('%i');
                $sec = $interval->format('%s');

                if ($sec < 45) {
                   $sec += 5;
                }

                $finalTime =  $attendance_date_start." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);


                $datePunchIN = $attendance_date_start ." ".$punch_in_time;
                $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

                if ($datePunchOUT < $datePunchIN) {
                    $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                    $response["status"] = "202";
                    echo json_encode($response);
                    exit();
                }

                $punch_image = "";

                if ($_FILES["punch_image"]["tmp_name"] != null) {
                    // code...
                    $file11=$_FILES["punch_image"]["tmp_name"];
                    $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["punch_image"]["name"]);
                        $punch_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                    }
                }

                // Early Punch OUT

                $min = date('i',strtotime($early_out_time));
                $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                $strMin = "-".$min." minutes";
                $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                $qryLeave = $d->select("leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                $hasLeave = false;

                if (mysqli_num_rows($qryLeave) > 0) {

                    $hasLeave = true;

                    $leaveData = mysqli_fetch_array($qryLeave);

                    $leave_id = $leaveData['leave_id'];
                    $leave_type_id = $leaveData['leave_type_id'];
                    $paid_unpaid = $leaveData['paid_unpaid'];
                    $leave_reason = $leaveData['leave_reason'];
                    $leave_day_type = $leaveData['leave_day_type'];
                    $half_day_session = $leaveData['half_day_session'];
                }

                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = "0";
                }else{
                    $early_out = "1";              
                }

                if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                    $early_out = "0";
                }

                // Early Punch Out END

                // Total Working Hour Count

                $parts = explode(':', $perdayHours);
                $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                $halfDayHourMinute = $perDayHourMinute/2;
                $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

                $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                $punch_in_time,$punchOUTTime);

                $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                $punch_in_time,$punchOUTTime);

                $times[] = $totalHours;

                $time = explode(':', $totalHours.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                // Average Working days calculations

                $avgWorkingDays = $total_minutes/$perDayHourMinute;

                $avg_working_days = round($avgWorkingDays * 2) / 2;

                $extra_working_hours_minutes = 0;

                if ($total_minutes > $perDayHourMinute) {
                    $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                    $hours  = floor($extra_working_hours_minutes/60);
                    $minutes = $extra_working_hours_minutes % 60;

                    $extra_working_hours = $hours.':'.$minutes;
                }

                $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

                $qryBreakData = mysqli_fetch_array($qryBreak);

                $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

                if ($total_minutes > $totalBreakinutes) {
                    $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                    $hoursPM  = floor($productive_working_hours_minutes/60);
                    $minutesPM = $productive_working_hours_minutes % 60;

                    $productive_working_hours = $hoursPM.':'.$minutesPM;
                }else{
                    $productive_working_hours_minutes = $total_minutes;

                    $hoursPM  = floor($productive_working_hours_minutes/60);
                    $minutesPM = $productive_working_hours_minutes % 60;

                    $productive_working_hours = $hoursPM.':'.$minutesPM;
                }


                if ($track_user_location == 1) {
                    $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                    $query11Data=mysqli_fetch_array($query11);

                    if ($query11Data['SUM(last_distance)'] > 0) {
                        $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                    }else{
                        $total_travel_meter = '0';
                    }
                }

                $m->set_data('attendance_date_end',$punchOUTDate);
                $m->set_data('punch_out_time',$punchOUTTime);
                $m->set_data('punch_out_latitude',$punch_in_latitude);
                $m->set_data('punch_out_longitude',$punch_in_longitude);
                $m->set_data('punch_out_image',$punch_image);
                $m->set_data('early_out',$early_out);
                $m->set_data('avg_working_days',$avg_working_days);
                $m->set_data('total_working_hours',$totalHours);
                $m->set_data('total_working_minutes',$total_minutes);
                $m->set_data('total_travel_meter',$total_travel_meter);
                $m->set_data('extra_working_hours',$extra_working_hours);
                $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                $m->set_data('punch_out_address',$google_address);
                $m->set_data('punch_out_face_app_location',$device_location);
                $m->set_data('productive_working_hours',$productive_working_hours);
                $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                $m->set_data('attendance_out_from',"0");

                $a = array(
                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                    'punch_out_time'=>$m->get_data('punch_out_time'),
                    'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                    'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                    'punch_out_image'=>$m->get_data('punch_out_image'),
                    'early_out'=>$m->get_data('early_out'),
                    'avg_working_days'=>$m->get_data('avg_working_days'),
                    'total_working_hours'=>$m->get_data('total_working_hours'),
                    'total_working_minutes'=>$m->get_data('total_working_minutes'),
                    'attendance_out_from'=>$m->get_data('attendance_out_from'),
                    'total_travel_meter'=>$m->get_data('total_travel_meter'),
                    'extra_working_hours'=>$m->get_data('extra_working_hours'),
                    'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                    'punch_out_address'=>$m->get_data('punch_out_address'),
                    'punch_out_face_app_location'=>$m->get_data('punch_out_face_app_location'),
                    'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                    'productive_working_hours'=>$m->get_data('productive_working_hours'),
                );

                $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                if ($punchOutQry == true) {

                    $title = "Punched OUT from face app";
                    $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                    if ($device == 'android') {
                        $nResident->noti("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'punch_out',
                        'notification_logo'=>'attendance_tracker.png',
                        'notification_type'=>'0',
                    );
                    
                    $d->insert("user_notification",$notiAry);

                    // Check Half Day


                    $leaveValue = false;

                    if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                        $leaveValue = true;
                    } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                        $leaveValue = true;
                    }

                    $alreadyLeaveQryCheck = false; 

                    $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                    $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                    if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != '00:00:00') {
                        $noLeave = true;
                    }else{
                        $noLeave = false;
                    }

                    $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                    
                    if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                        $leave_total_days = 1;

                        if ($total_minutes < $maximum_halfday_hours_min) {
                            $leaveType = "0";
                            $half_day_session = "0";        
                        }else{
                            $leaveType = "1";
                        }

                        if ($hasLeave == true) {

                            $title = "Auto Leave Applied"." - ".$leaveTypeName;
                            $titleMessage = "Working hours not completed ($totalHoursWithName)";
                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            $m->set_data('leave_type_id',$leave_type_id);
                            $m->set_data('paid_unpaid',$paid_unpaid);
                            $m->set_data('leave_day_type',$leaveType);
                            $m->set_data('half_day_session',$half_day_session);
                            $m->set_data('auto_leave_reason',$titleMessage);

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'half_day_session'=>$m->get_data('half_day_session'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            );

                            $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                            
                        }else{

                            if ($leaveType == 0) {
                                $workingDaysTotal = "0";
                                $title = "Auto Leave Applied - Full Day";
                            }else{
                                $workingDaysTotal = "0.5";
                                $title = "Auto Leave Applied - Half Day";
                            }
                            
                            $titleMessage = "Working hours not completed ($totalHoursWithName)";
                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            $m->set_data('leave_type_id',"0");
                            $m->set_data('society_id',$society_id);
                            $m->set_data('paid_unpaid',"1");
                            $m->set_data('unit_id',$unit_id);
                            $m->set_data('user_id',$user_id);
                            $m->set_data('auto_leave_reason',$titleMessage);
                            $m->set_data('leave_start_date',$attendance_date_start);
                            $m->set_data('leave_end_date',$punchOUTDate);
                            $m->set_data('leave_total_days',$leave_total_days);
                            $m->set_data('leave_day_type',$leaveType);
                            $m->set_data('leave_status',"1");
                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'society_id' =>$m->get_data('society_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'leave_status'=>$m->get_data('leave_status'),
                                'leave_created_date'=>$m->get_data('leave_created_date'),
                            );

                            $leaveQry = $d->insert("leave_master",$a);

                            $alreadyLeaveQryCheck = true;

                            
                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                        }

                        $m->set_data('auto_leave',"1");
                        $m->set_data('avg_working_days',$workingDaysTotal);

                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave'),
                            'avg_working_days'=>$m->get_data('avg_working_days'),
                        );

                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    }else if($noLeave == true && $hasLeave == true){
                        $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                        $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                        $title = "Applied Leave Cancelled";
                        $description = "For Date : ".$attendance_date_start;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker"," leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                    }

                    $startMonthDate = date('Y-m-01');
                    $endMonthDate = date('Y-m-t');


                    $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                    $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                    $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                    if ($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                        $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$punchOUTDate);
                        $m->set_data('leave_total_days',"1");
                        $m->set_data('leave_day_type',"1");
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $title = "Auto Leave Applied - Half Day";

                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                        $m->set_data('auto_leave',"1");
                        $m->set_data('avg_working_days',"0.5");

                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave'),
                            'avg_working_days'=>$m->get_data('avg_working_days'),
                        );

                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    }

                    // END checking half day

                    $m->set_data('break_end_date',$punchOUTDate);
                    $m->set_data('break_out_time',$punchOUTTime);

                    $a = array(
                        'break_end_date'=>$m->get_data('break_end_date'),
                        'break_out_time'=>$m->get_data('break_out_time'),
                    );

                    $qry = $d->update("attendance_break_history_master",$a,"attendance_id = '$attendance_id'  AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");
                    

                    $response['user_full_name'] =$attData["user_full_name"];
                    $response['society_id'] =$attData["society_id"];
                    $response['block_id'] =$attData["block_id"];
                    $response['floor_id'] =$attData["floor_id"];
                    $response['unit_id'] =$attData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$attData["user_profile_pic"];
                    $response['user_designation'] =$attData["user_designation"];
                    
                    $response['attendance_id'] =$attData["attendance_id"];
                    $response['punch_in_time'] =$attData["punch_in_time"];
                    $response['punch_in_time_view'] = date("h:i A",strtotime($attData["punch_in_time"]));
                    $response['date_time'] =$finalTime;
                    $response['time_diff'] =$time_diff;
                    $response["in_out_status"] = "1";
                    $response["message"] = "Goodbye $user_first_name";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }else{
                    $response["message"] = "Something went wrong";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            
            }else{
                // Code For punch IN

                // Code for remove WFH request

                $d->delete("wfh_master","user_id = '$user_id' AND wfh_start_date = '$punchInDate' AND wfh_day_type = '0'");

                $punchInDateTime = $punchInDate." ".$punchInTime;

                $isPunchIn = $d->selectRow("attendance_master.*,leave_master.leave_id","attendance_master LEFT JOIN leave_master ON leave_master.leave_start_date = '$attendance_date_start' AND leave_master.user_id = '$user_id' AND leave_master.user_id = attendance_master.user_id AND leave_master.leave_day_type = '1' ","attendance_master.user_id = '$user_id' AND attendance_master.society_id = '$society_id' AND attendance_master.shift_time_id = '$shift_time_id' $varQryTmp");

                $aData = mysqli_fetch_array($isPunchIn);


                if ((mysqli_num_rows($isPunchIn) > 2) || (mysqli_num_rows($isPunchIn) > 0 && $aData['leave_id'] == null) ) {

                    if ($aData['attendance_date_end'] != "00:00:00") {
                        $response["message"] = "You have already punched out";               
                    }else{
                        $response["message"] = "You have already punched in";                
                    }
                    $response["status"] = "202";
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    echo json_encode($response);
                    exit();
                }

                $punch_image = "";

                if ($_FILES["punch_image"]["tmp_name"] != null) {
                    // code...
                    $file11=$_FILES["punch_image"]["tmp_name"];
                    $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["punch_image"]["name"]);
                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                    }
                }

                $min = date('i',strtotime($late_time_start));
                $defaultINTime = $punchInDate." ".$shift_start_time;
                $strMin = "+".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                if ($punchInDateTime > $defaultTime) {
                    $late_in = "1";
                }else{
                    $late_in = "0";
                }

                $m->set_data('shift_time_id',$shift_time_id);
                $m->set_data('total_shift_hours',$per_day_hour);
                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('attendance_date_start',$punchInDate);
                $m->set_data('punch_in_time',$punchInTime);
                $m->set_data('punch_in_latitude',$punch_in_latitude);
                $m->set_data('punch_in_longitude',$punch_in_longitude);
                $m->set_data('punch_in_image',$punch_image);
                $m->set_data('punch_in_address',$google_address);
                $m->set_data('punch_in_face_app_location',$device_location);
                $m->set_data('late_in',$late_in);
                $m->set_data('attendance_status',"1");
                $m->set_data('attendance_in_from',"0");
                $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

                $a = array(
                    'shift_time_id'=>$m->get_data('shift_time_id'),
                    'total_shift_hours'=>$m->get_data('total_shift_hours'),
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'attendance_date_start'=>$m->get_data('attendance_date_start'),
                    'punch_in_time'=>$m->get_data('punch_in_time'),
                    'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                    'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                    'punch_in_image'=>$m->get_data('punch_in_image'),
                    'late_in'=>$m->get_data('late_in'),
                    'attendance_status'=>$m->get_data('attendance_status'),
                    'attendance_in_from'=>$m->get_data('attendance_in_from'),
                    'attendance_created_time'=>$m->get_data('attendance_created_time'),
                    'punch_in_address'=>$m->get_data('punch_in_address'),
                    'punch_in_face_app_location'=>$m->get_data('punch_in_face_app_location'),
                );

                $punchInQry = $d->insert("attendance_master",$a);
                $attendance_id = $con->insert_id;

                if ($punchInQry == true) {

                    /*$dataAry = array(
                        "society_id"=>$society_id,
                    );

                    $dataJson = json_encode($dataAry);*/

                    $title = "Punched IN from face app";
                    $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                    if ($device == 'android') {
                        $nResident->noti("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'punch_in',
                        'notification_logo'=>'attendance_tracker.png',
                        'notification_type'=>'0',
                    );
                    
                    $d->insert("user_notification",$notiAry);
                
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    
                    $response['attendance_id'] =$attendance_id.'';
                    $response['punch_in_time'] =$punchInTime;
                    $response['punch_in_time_view'] = date("h:i A",strtotime($punchInTime));
                    $response['date_time'] ="";
                    $response['time_diff'] ="";
                    $response["in_out_status"] = "0";
                    $response["message"] = "Welcome $user_first_name";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }else{
                    $response["message"] = "Something went wrong";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            }

            
            $response["auto_api_call_time"] = 6000;
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);  
        }else if($_POST['getAttendanceData']=="getAttendanceData" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

            $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
            $userData = mysqli_fetch_array($checkShift);
            $shift_time_id = $userData['shift_time_id'];
            $unit_id = $userData['unit_id'];
            $block_id = $userData['block_id'];
            $floor_id = $userData['floor_id'];
            $user_work_report_on = $userData['user_work_report_on'];
            $user_full_name = $userData['user_full_name'];
            $user_profile_pic = $userData['user_profile_pic'];
            $user_designation = $userData['user_designation'];
            $user_token = $userData['user_token'];
            $track_user_location = $userData['track_user_location'];
            $device = $userData['device'];
            $user_first_name = $userData['user_first_name'];

            if ($checkShift == true && $shift_time_id == '0') {
                $response["message"] = "Your shift is missing, please contact to admin";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            if (mysqli_num_rows($shiftQ) > 0) {
                $shiftData = mysqli_fetch_array($shiftQ);
                

                $perdayHours = $shiftData['per_day_hour'];
                $shift_start_time = $shiftData['shift_start_time'];
                $per_day_hour = $shiftData['per_day_hour'];
                $shift_end_time = $shiftData['shift_end_time'];
                $late_time_start = $shiftData['late_time_start'];
                $early_out_time = $shiftData['early_out_time'];
                $half_day_time_start = $shiftData['half_day_time_start'];
                $halfday_before_time = $shiftData['halfday_before_time'];
                $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                $shift_type = $shiftData['shift_type'];
                $maximum_in_out = $shiftData['maximum_in_out'];
                $max_shift_hour = $shiftData['max_shift_hour'];

                $parts = explode(':', $max_shift_hour);
                $max_shift_hour_minutes = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);

                $parts1 = explode(':', $per_day_hour);
                $per_day_hour_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);


                $shiftData = array(
                    "shift_time_id" => $shift_time_id,
                    "per_day_hour" => $per_day_hour,
                    "shift_start_time" => $shift_start_time,
                    "shift_end_time" => $shift_end_time,
                );

                $response['shift_data'] = $shiftData;
            }

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            //$punchInDate ="2022-10-15";
            //$punchInTime ="20:12:00";

            $punchOUTTime = date('H:i:s');
            $punchOUTDate = date('Y-m-d');

            //$punchOUTTime = "23:59:12";
            //$punchOUTDate = "2022-10-14";
            $attendance_date_start = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            if ($shift_type == "Night") {

                $attendance_date_previous_day = date('Y-m-d',strtotime('-1 days'));

                $varQry = " AND (attendance_master.attendance_date_start = '$attendance_date_previous_day' OR attendance_master.attendance_date_start = '$attendance_date_start')";

                $varQryTmp = " AND ((attendance_master.attendance_date_start = '$attendance_date_previous_day' AND attendance_master.attendance_date_end = '0000-00-00') OR attendance_master.attendance_date_start = '$attendance_date_start' AND attendance_master.attendance_date_end != '0000-00-00')";

                 //$varQryTmp = " AND attendance_master.attendance_date_start = '$attendance_date_start' AND attendance_master.attendance_date_end = '0000-00-00'"; 
            }else{
                $varQry = " AND (attendance_master.attendance_date_start = '$attendance_date_start')";

                $varQryTmp = " AND (attendance_master.attendance_date_start = '$attendance_date_start')"; 
            }

            if ($inOut == 1) {
                $getTodayAttendace = $d->selectRow("users_master.*,floors_master.*,attendance_master.*,work_report_master.work_report_id","users_master,floors_master,attendance_master LEFT JOIN work_report_master ON work_report_master.work_report_date = attendance_master.attendance_date_start AND attendance_master.user_id = work_report_master.user_id","attendance_master.user_id = '$user_id' AND attendance_master.shift_time_id = '$shift_time_id'  AND attendance_master.attendance_date_end = '0000-00-00' AND attendance_master.punch_out_time = '00:00:00' AND attendance_master.user_id = users_master.user_id AND users_master.floor_id = floors_master.floor_id $varQry","ORDER BY attendance_master.attendance_id DESC LIMIT 1");

                if (mysqli_num_rows($getTodayAttendace) > 0) {

                    $attData = mysqli_fetch_array($getTodayAttendace);

                    if ($attData['work_report_id'] == null || $attData['work_report_id'] == '') {

                        $qSociety1 = $d->count_data_direct("society_id","society_master", "society_id ='$society_id' AND (work_report_on = '1' OR (work_report_on = '2' AND FIND_IN_SET('$block_id',work_report_on_ids)) OR (work_report_on = '3' AND FIND_IN_SET('$floor_id',work_report_on_ids)))");

                        $work_report_on = false;

                        if ($qSociety1 > 0) {
                            $work_report_on = true;
                        }

                        if ($user_work_report_on == "1") {
                            $work_report_on = true;
                        }else if ($user_work_report_on == "2"){
                            $work_report_on = false;
                        }

                        if($work_report_on == true){                        
                            $response['work_report_pending'] = $work_report_on;
                            $response['message'] = "Please add work report from employee app and then punch out";
                            $response['status'] = "201";
                            echo json_encode($response);
                            exit();
                        }
                    }


                    // Code for Punch Out
                   
                    $attendance_id = $attData['attendance_id'];
                    $attendance_date_start = $attData["attendance_date_start"];
                    $punch_in_time = $attData["punch_in_time"];
                    $todayLateIn  = $attData['late_in'];
                    
                    $pDate = $attStartDate." ".$punch_in_time;

                    $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                    $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                    $date_a = new DateTime($inTimeAMPM);
                    $date_b = new DateTime($dateTimeCurrent);

                    $interval = $date_a->diff($date_b);
                   
                    $hours = $interval->format('%h');
                    $minutes = $interval->format('%i');
                    $sec = $interval->format('%s');

                    if ($sec < 45) {
                       $sec += 5;
                    }

                    $finalTime =  $attendance_date_start." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                    $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);


                    $datePunchIN = $attendance_date_start ." ".$punch_in_time;
                    $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                    $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

                    if ($datePunchOUT < $datePunchIN) {
                        $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                    }

                    $punch_image = "";

                    if ($_FILES["punch_image"]["tmp_name"] != null) {
                        // code...
                        $file11=$_FILES["punch_image"]["tmp_name"];
                        $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                        $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                        if(in_array($extId,$extAllow)) {
                            $temp = explode(".", $_FILES["punch_image"]["name"]);
                            $punch_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                            move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                        }
                    }

                    // Early Punch OUT

                    $min = date('i',strtotime($early_out_time));
                    $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                    $strMin = "-".$min." minutes";
                    $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                    $qryLeave = $d->select("leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                    $hasLeave = false;

                    if (mysqli_num_rows($qryLeave) > 0) {

                        $hasLeave = true;

                        $leaveData = mysqli_fetch_array($qryLeave);

                        $leave_id = $leaveData['leave_id'];
                        $leave_type_id = $leaveData['leave_type_id'];
                        $paid_unpaid = $leaveData['paid_unpaid'];
                        $leave_reason = $leaveData['leave_reason'];
                        $leave_day_type = $leaveData['leave_day_type'];
                        $half_day_session = $leaveData['half_day_session'];
                    }

                    if ($datePunchOUT >= $defaultOutTime) {
                        $early_out = "0";
                    }else{
                        $early_out = "1";              
                    }

                    if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                        $early_out = "0";
                    }

                    // Early Punch Out END

                    // Total Working Hour Count

                    $parts = explode(':', $perdayHours);
                    $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                    $halfDayHourMinute = $perDayHourMinute/2;
                    $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

                    $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                    $punch_in_time,$punchOUTTime);

                    $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                    $punch_in_time,$punchOUTTime);

                    $times[] = $totalHours;

                    $time = explode(':', $totalHours.":00");
                    $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                    if (($total_minutes > $max_shift_hour_minutes && $max_shift_hour_minutes != 0) || ($total_minutes > 1440 && $total_minutes > $per_day_hour_minutes)) {


                        $response["message"] = "Punch out time limit over";                
                        $response["status"] = "202";
                        $response['user_full_name'] =$userData["user_full_name"];
                        $response['society_id'] =$userData["society_id"];
                        $response['block_id'] =$userData["block_id"];
                        $response['floor_id'] =$userData["floor_id"];
                        $response['unit_id'] =$userData["unit_id"];
                        $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                        $response['user_designation'] =$userData["user_designation"];
                        echo json_encode($response);
                        exit();
                    }

                    // Average Working days calculations

                    $avgWorkingDays = $total_minutes/$perDayHourMinute;

                    $avg_working_days = round($avgWorkingDays * 2) / 2;

                    $extra_working_hours_minutes = 0;

                    if ($total_minutes > $perDayHourMinute) {
                        $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                        $hours  = floor($extra_working_hours_minutes/60);
                        $minutes = $extra_working_hours_minutes % 60;

                        $extra_working_hours = $hours.':'.$minutes;
                    }

                    $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

                    $qryBreakData = mysqli_fetch_array($qryBreak);

                    $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

                    if ($total_minutes > $totalBreakinutes) {
                        $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                        $hoursPM  = floor($productive_working_hours_minutes/60);
                        $minutesPM = $productive_working_hours_minutes % 60;

                        $productive_working_hours = $hoursPM.':'.$minutesPM;
                    }else{
                        $productive_working_hours_minutes = $total_minutes;

                        $hoursPM  = floor($productive_working_hours_minutes/60);
                        $minutesPM = $productive_working_hours_minutes % 60;

                        $productive_working_hours = $hoursPM.':'.$minutesPM;
                    }


                    if ($track_user_location == 1) {
                        $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                        $query11Data=mysqli_fetch_array($query11);

                        if ($query11Data['SUM(last_distance)'] > 0) {
                            $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                        }else{
                            $total_travel_meter = '0';
                        }
                    }

                    $m->set_data('attendance_date_end',$punchOUTDate);
                    $m->set_data('punch_out_time',$punchOUTTime);
                    $m->set_data('punch_out_latitude',$punch_in_latitude);
                    $m->set_data('punch_out_longitude',$punch_in_longitude);
                    $m->set_data('punch_out_image',$punch_image);
                    $m->set_data('early_out',$early_out);
                    $m->set_data('avg_working_days',$avg_working_days);
                    $m->set_data('total_working_hours',$totalHours);
                    $m->set_data('total_working_minutes',$total_minutes);
                    $m->set_data('total_travel_meter',$total_travel_meter);
                    $m->set_data('extra_working_hours',$extra_working_hours);
                    $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                    $m->set_data('punch_out_address',$google_address);
                    $m->set_data('punch_out_face_app_location',$device_location);
                    $m->set_data('productive_working_hours',$productive_working_hours);
                    $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                    $m->set_data('attendance_out_from',"0");

                    $a = array(
                        'attendance_date_end'=>$m->get_data('attendance_date_end'),
                        'punch_out_time'=>$m->get_data('punch_out_time'),
                        'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                        'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                        'punch_out_image'=>$m->get_data('punch_out_image'),
                        'early_out'=>$m->get_data('early_out'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'total_working_hours'=>$m->get_data('total_working_hours'),
                        'total_working_minutes'=>$m->get_data('total_working_minutes'),
                        'attendance_out_from'=>$m->get_data('attendance_out_from'),
                        'total_travel_meter'=>$m->get_data('total_travel_meter'),
                        'extra_working_hours'=>$m->get_data('extra_working_hours'),
                        'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                        'punch_out_address'=>$m->get_data('punch_out_address'),
                        'punch_out_face_app_location'=>$m->get_data('punch_out_face_app_location'),
                        'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                        'productive_working_hours'=>$m->get_data('productive_working_hours'),
                    );

                    $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                    if ($punchOutQry == true) {

                        $title = "Punched OUT from face app";
                        $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                        if ($device == 'android') {
                            $nResident->noti("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("punch_out","",$society_id,$user_token,$title,$description,$society_id);
                        }

                        $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$user_id,
                            'notification_title'=>$title,
                            'notification_desc'=>$description,    
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'punch_out',
                            'notification_logo'=>'attendance_tracker.png',
                            'notification_type'=>'0',
                        );
                        
                        $d->insert("user_notification",$notiAry);

                        // Check Half Day


                        $leaveValue = false;

                        if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                            $leaveValue = true;
                        } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                            $leaveValue = true;
                        }

                        $alreadyLeaveQryCheck = false; 

                        $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                        $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                        if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != '00:00:00') {
                            $noLeave = true;
                        }else{
                            $noLeave = false;
                        }

                        $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                        
                        if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                            $leave_total_days = 1;

                            if ($total_minutes < $maximum_halfday_hours_min) {
                                $leaveType = "0";
                                $half_day_session = "0";        
                            }else{
                                $leaveType = "1";
                            }

                            if ($hasLeave == true) {

                                $title = "Auto Leave Applied"." - ".$leaveTypeName;
                                $titleMessage = "Working hours not completed ($totalHoursWithName)";
                                $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                $m->set_data('leave_type_id',$leave_type_id);
                                $m->set_data('paid_unpaid',$paid_unpaid);
                                $m->set_data('leave_day_type',$leaveType);
                                $m->set_data('half_day_session',$half_day_session);
                                $m->set_data('auto_leave_reason',$titleMessage);

                                $a = array(
                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                    'half_day_session'=>$m->get_data('half_day_session'),
                                    'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                );

                                $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");

                                if ($device == 'android') {
                                    $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                } else if ($device == 'ios') {
                                    $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                }

                                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                
                            }else{

                                if ($leaveType == 0) {
                                    $workingDaysTotal = "0";
                                    $title = "Auto Leave Applied - Full Day";
                                }else{
                                    $workingDaysTotal = "0.5";
                                    $title = "Auto Leave Applied - Half Day";
                                }
                                
                                $titleMessage = "Working hours not completed ($totalHoursWithName)";
                                $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                $m->set_data('leave_type_id',"0");
                                $m->set_data('society_id',$society_id);
                                $m->set_data('paid_unpaid',"1");
                                $m->set_data('unit_id',$unit_id);
                                $m->set_data('user_id',$user_id);
                                $m->set_data('auto_leave_reason',$titleMessage);
                                $m->set_data('leave_start_date',$attendance_date_start);
                                $m->set_data('leave_end_date',$punchOUTDate);
                                $m->set_data('leave_total_days',$leave_total_days);
                                $m->set_data('leave_day_type',$leaveType);
                                $m->set_data('leave_status',"1");
                                $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                $a = array(
                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                    'society_id' =>$m->get_data('society_id'),
                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                    'unit_id'=>$m->get_data('unit_id'),
                                    'user_id'=>$m->get_data('user_id'),
                                    'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                    'leave_start_date'=>$m->get_data('leave_start_date'),
                                    'leave_end_date'=>$m->get_data('leave_end_date'),
                                    'leave_total_days'=>$m->get_data('leave_total_days'),
                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                    'leave_status'=>$m->get_data('leave_status'),
                                    'leave_created_date'=>$m->get_data('leave_created_date'),
                                );

                                $leaveQry = $d->insert("leave_master",$a);

                                $alreadyLeaveQryCheck = true;

                                
                                if ($device == 'android') {
                                    $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                } else if ($device == 'ios') {
                                    $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                }

                                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                            }

                            $m->set_data('auto_leave',"1");
                            $m->set_data('avg_working_days',$workingDaysTotal);

                            $a1 = array(
                                'auto_leave'=>$m->get_data('auto_leave'),
                                'avg_working_days'=>$m->get_data('avg_working_days'),
                            );

                            $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                        }else if($noLeave == true && $hasLeave == true){
                            $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                            $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                            $title = "Applied Leave Cancelled";
                            $description = "For Date : ".$attendance_date_start;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker"," leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                        }

                        $startMonthDate = date('Y-m-01');
                        $endMonthDate = date('Y-m-t');


                        $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                        $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                        $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                        if ($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                            $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                            $m->set_data('leave_type_id',"0");
                            $m->set_data('society_id',$society_id);
                            $m->set_data('paid_unpaid',"1");
                            $m->set_data('unit_id',$unit_id);
                            $m->set_data('user_id',$user_id);
                            $m->set_data('auto_leave_reason',$titleMessage);
                            $m->set_data('leave_start_date',$attendance_date_start);
                            $m->set_data('leave_end_date',$punchOUTDate);
                            $m->set_data('leave_total_days',"1");
                            $m->set_data('leave_day_type',"1");
                            $m->set_data('leave_status',"1");
                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'society_id' =>$m->get_data('society_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'leave_status'=>$m->get_data('leave_status'),
                                'leave_created_date'=>$m->get_data('leave_created_date'),
                            );

                            $leaveQry = $d->insert("leave_master",$a);

                            $title = "Auto Leave Applied - Half Day";

                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                            $m->set_data('auto_leave',"1");
                            $m->set_data('avg_working_days',"0.5");

                            $a1 = array(
                                'auto_leave'=>$m->get_data('auto_leave'),
                                'avg_working_days'=>$m->get_data('avg_working_days'),
                            );

                            $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                        }

                        // END checking half day

                        $m->set_data('break_end_date',$punchOUTDate);
                        $m->set_data('break_out_time',$punchOUTTime);

                        $a = array(
                            'break_end_date'=>$m->get_data('break_end_date'),
                            'break_out_time'=>$m->get_data('break_out_time'),
                        );

                        $qry = $d->update("attendance_break_history_master",$a,"attendance_id = '$attendance_id'  AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");
                        

                        $response['user_full_name'] =$attData["user_full_name"];
                        $response['society_id'] =$attData["society_id"];
                        $response['block_id'] =$attData["block_id"];
                        $response['floor_id'] =$attData["floor_id"];
                        $response['unit_id'] =$attData["unit_id"];
                        $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$attData["user_profile_pic"];
                        $response['user_designation'] =$attData["user_designation"];
                        
                        $response['attendance_id'] =$attData["attendance_id"];
                        $response['punch_in_time'] =$attData["punch_in_time"];
                        $response['punch_in_time_view'] = date("h:i A",strtotime($attData["punch_in_time"]));
                        $response['date_time'] =$finalTime;
                        $response['time_diff'] =$time_diff;
                        $response["in_out_status"] = "1";
                        $response["message"] = "Goodbye $user_first_name";
                        $response["status"] = "200";
                        echo json_encode($response);
                        exit();
                    }else{
                        $response["message"] = "Something went wrong";
                        $response["status"] = "201";
                        echo json_encode($response);
                    } 
                
                }else{
                    $response["message"] = "You have already punched out";                
                    $response["status"] = "202";
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    echo json_encode($response);
                    exit();
                }
            }else{
                // Code For punch IN

                // Code for remove WFH request

                $d->delete("wfh_master","user_id = '$user_id' AND wfh_start_date = '$punchInDate' AND wfh_day_type = '0'");

                $punchInDateTime = $punchInDate." ".$punchInTime;

                $isPunchIn = $d->selectRow("attendance_master.*,leave_master.leave_id","attendance_master LEFT JOIN leave_master ON leave_master.leave_start_date = '$attendance_date_start' AND leave_master.user_id = '$user_id' AND leave_master.user_id = attendance_master.user_id AND leave_master.leave_day_type = '1' ","attendance_master.user_id = '$user_id' AND attendance_master.society_id = '$society_id' AND attendance_master.shift_time_id = '$shift_time_id' $varQryTmp");


                $aData = mysqli_fetch_array($isPunchIn);

                //if ((mysqli_num_rows($isPunchIn) > 2) || (mysqli_num_rows($isPunchIn) > 0 && $aData['leave_id'] == null) ) {
                if (mysqli_num_rows($isPunchIn) > 0 ) {

                    if ($aData['attendance_date_end'] != "0000-00-00") {
                        $response["message"] = "You have already punched out";               
                    }else{
                        $response["message"] = "You have already punched in";                
                    }
                    $response["status"] = "202";
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    echo json_encode($response);
                    exit();
                }


                $punch_image = "";

                if ($_FILES["punch_image"]["tmp_name"] != null) {
                    // code...
                    $file11=$_FILES["punch_image"]["tmp_name"];
                    $extId = pathinfo($_FILES['punch_image']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["punch_image"]["name"]);
                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["punch_image"]["tmp_name"], "../img/attendance/" . $punch_image);
                    }
                }

                $min = date('i',strtotime($late_time_start));
                $defaultINTime = $punchInDate." ".$shift_start_time;
                $strMin = "+".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                if ($punchInDateTime > $defaultTime) {
                    $late_in = "1";
                }else{
                    $late_in = "0";
                }

                $m->set_data('shift_time_id',$shift_time_id);
                $m->set_data('total_shift_hours',$per_day_hour);
                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('attendance_date_start',$punchInDate);
                $m->set_data('punch_in_time',$punchInTime);
                $m->set_data('punch_in_latitude',$punch_in_latitude);
                $m->set_data('punch_in_longitude',$punch_in_longitude);
                $m->set_data('punch_in_image',$punch_image);
                $m->set_data('punch_in_address',$google_address);
                $m->set_data('punch_in_face_app_location',$device_location);
                $m->set_data('late_in',$late_in);
                $m->set_data('attendance_status',"1");
                $m->set_data('attendance_in_from',"0");
                $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

                $a = array(
                    'shift_time_id'=>$m->get_data('shift_time_id'),
                    'total_shift_hours'=>$m->get_data('total_shift_hours'),
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'attendance_date_start'=>$m->get_data('attendance_date_start'),
                    'punch_in_time'=>$m->get_data('punch_in_time'),
                    'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                    'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                    'punch_in_image'=>$m->get_data('punch_in_image'),
                    'late_in'=>$m->get_data('late_in'),
                    'attendance_status'=>$m->get_data('attendance_status'),
                    'attendance_in_from'=>$m->get_data('attendance_in_from'),
                    'attendance_created_time'=>$m->get_data('attendance_created_time'),
                    'punch_in_address'=>$m->get_data('punch_in_address'),
                    'punch_in_face_app_location'=>$m->get_data('punch_in_face_app_location'),
                );

                $punchInQry = $d->insert("attendance_master",$a);
                $attendance_id = $con->insert_id;

                if ($punchInQry == true) {

                    /*$dataAry = array(
                        "society_id"=>$society_id,
                    );

                    $dataJson = json_encode($dataAry);*/

                    $title = "Punched IN from face app";
                    $description = "Date: ".date('Y-m-d')." Time: ".date('h:i A');

                    if ($device == 'android') {
                        $nResident->noti("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("punch_in","",$society_id,$user_token,$title,$description,$society_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'punch_in',
                        'notification_logo'=>'attendance_tracker.png',
                        'notification_type'=>'0',
                    );
                    
                    $d->insert("user_notification",$notiAry);
                
                    $response['user_full_name'] =$userData["user_full_name"];
                    $response['society_id'] =$userData["society_id"];
                    $response['block_id'] =$userData["block_id"];
                    $response['floor_id'] =$userData["floor_id"];
                    $response['unit_id'] =$userData["unit_id"];
                    $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$userData["user_profile_pic"];
                    $response['user_designation'] =$userData["user_designation"];
                    
                    $response['attendance_id'] =$attendance_id.'';
                    $response['punch_in_time'] =$punchInTime;
                    $response['punch_in_time_view'] = date("h:i A",strtotime($punchInTime));
                    $response['date_time'] ="";
                    $response['time_diff'] ="";
                    $response["in_out_status"] = "0";
                    $response["message"] = "Welcome $user_first_name";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }else{
                    $response["message"] = "Something went wrong";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            }

            
            $response["auto_api_call_time"] = 6000;
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);  
        }else if($_POST['getBreakTypes']=="getBreakTypes" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('token', $token);
            $m->set_data('society_id', $society_id);
            $m->set_data('admin_id', $user_id);
            $m->set_data('device_model', $model);
            $m->set_data('device_mac', $device_mac);
            $m->set_data('app_version', $app_version);
            $m->set_data('device_latitude', $device_latitude);
            $m->set_data('device_longitude', $device_longitude);
            $m->set_data('device_type', $device_type);
            $m->set_data('last_syc_date', date("Y-m-d H:i:s"));


            $a = array(
                'society_id' => $m->get_data('society_id'),
                'admin_id' => $m->get_data('admin_id'),
                'user_token' => $m->get_data('token'),
                'device_mac' => $m->get_data('device_mac'),
                'device_model' => $m->get_data('device_model'),
                'app_version' => $m->get_data('app_version'),
                'device_latitude' => $m->get_data('device_latitude'),
                'device_longitude' => $m->get_data('device_longitude'),
                'device_type' => $m->get_data('device_type'),
                'last_syc_date' => $m->get_data('last_syc_date'),
            );
            

            $qt=$d->select("face_app_device_master","device_mac='$device_mac'");
            if (mysqli_num_rows($qt)>0) {
                $deviceData = mysqli_fetch_array($qt);
                $device_location = $deviceData['device_location'];
                $face_app_device_id = $deviceData['face_app_device_id'];
                $d->update("face_app_device_master",$a,"device_mac='$device_mac'");
            } else {
                $d->insert("face_app_device_master",$a);
                $face_app_device_id = $con->insert_id;
                $device_location = "";
            }

            $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_active_status = '0' AND attendance_type_delete = '0'");              

            $q=$d->select("bms_admin_master,society_master,role_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.admin_id='$user_id' AND role_master.society_id=society_master.society_id AND bms_admin_master.admin_active_status=0");

            $user_data = mysqli_fetch_array($q);


            if(mysqli_num_rows($attendanceTypeQry)>0){

                $response["attendance_types"] = array();
                
                while($leaveTypeData=mysqli_fetch_array($attendanceTypeQry)) {

                    $leaveTypeData = array_map("html_entity_decode", $leaveTypeData);

                    $attendance_type = array(); 

                    $attendance_type["attendance_type_id"] = $leaveTypeData['attendance_type_id'];
                    $attendance_type["attendance_type_name"] = $leaveTypeData['attendance_type_name'];

                    array_push($response["attendance_types"], $attendance_type);
                }



                $response["face_app_device_id"]=$face_app_device_id;
                $response["device_location"]=$device_location;
                $response["admin_pin"]=$user_data['admin_pin'];
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["device_location"]=$device_location;
                $response["admin_pin"]=$user_data['admin_pin'];
                $response["message"]="No Break Type Found";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['checkBreakData']=="checkBreakData" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
            $userData = mysqli_fetch_array($checkShift);
            $shift_time_id = $userData['shift_time_id'];
            $user_first_name = $userData['user_first_name'];
            $response['user_full_name'] = $user_full_name = $userData['user_full_name'];
            $response['user_profile_pic'] = $base_url . "img/users/recident_profile/" .$user_profile_pic = $userData['user_profile_pic'];
            $response['user_designation'] = $user_designation = $userData['user_designation'];

            if ($checkShift == true && $shift_time_id == '0') {
                $response["message"] = "Your shift is missing, please contact to admin";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQ);


            $lunch_break_start_time = $shiftData['lunch_break_start_time'];
            $tea_break_start_time = $shiftData['tea_break_start_time'];
            $shift_type = $shiftData['shift_type'];
            $per_day_hour = $shiftData['per_day_hour'];
            
            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');
            $attendance_date_start = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            if ($shift_type == "Night") {
                $attendance_date_previous_day = date('Y-m-d',strtotime('-1 days'));
                $varQry = " AND (attendance_date_start = '$attendance_date_previous_day' OR attendance_date_start = '$attendance_date_start')";
            }else{
                $varQry = " AND (attendance_date_start = '$attendance_date_start')";
            }

            $getTodayAttendace = $d->selectRow("attendance_id ","attendance_master","user_id = '$user_id' AND shift_time_id = '$shift_time_id' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00' $varQry","ORDER BY attendance_id DESC LIMIT 1");

            if (mysqli_num_rows($getTodayAttendace) > 0) {

                $attData = mysqli_fetch_array($getTodayAttendace);

                $attendance_id = $attData['attendance_id'];

                $getTodayBreak = $d->selectRow("
                    attendance_break_history_master.attendance_break_history_id,
                    attendance_break_history_master.attendance_type_id,
                    attendance_break_history_master.break_start_date,
                    attendance_break_history_master.break_end_date,
                    attendance_break_history_master.break_in_time,
                    attendance_break_history_master.break_out_time,
                    attendance_type_master.attendance_type_name,
                    attendance_type_master.is_multipletime_use
                    ","
                    attendance_type_master,
                    attendance_break_history_master
                    ","attendance_break_history_master.attendance_id = '$attendance_id'
                    AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id 
                    AND attendance_break_history_master.attendance_type_id = '$attendance_type_id'
                    ","
                    ORDER BY attendance_break_history_master.attendance_break_history_id DESC LIMIT 1");

                if (mysqli_num_rows($getTodayBreak) > 0) {

                    $breakData = mysqli_fetch_array($getTodayBreak);

                    $attendance_break_history_id = $breakData['attendance_break_history_id'];
                    $is_multipletime_use = $breakData['is_multipletime_use'];
                    $break_start_date = $breakData['break_start_date'];
                    $break_end_date = $breakData['break_end_date'];
                    $break_in_time = $breakData['break_in_time'];
                    $break_out_time = $breakData['break_out_time'];

                    if ($break_start_date != '0000-00-00' && $break_end_date != '0000-00-00' && $is_multipletime_use == '0') {
                        $breakDate = $break_end_date;
                        $breakTime = $break_out_time;
                    }else{
                        $breakDate = date('Y-m-d');
                        $breakTime = date('H:i:s');
                    }                    

                    $totalBreakHoursName = getTotalHoursWithNames($break_start_date,$breakDate,$break_in_time,$breakTime);

                    $response['total_time'] =$totalBreakHoursName;

                    if ($break_start_date != '0000-00-00' && $break_end_date != '0000-00-00' && $is_multipletime_use == '0') {
                        $response['break_in_time'] =date('h:i A',strtotime($break_in_time));
                        $response['break_out_time'] =date('h:i A',strtotime($breakTime));
                        $response["message"] = "Hi ".$user_first_name.', '. $breakData['attendance_type_name']." already used by you.";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }else{                        


                        if ($break_end_date == '0000-00-00' && $break_out_time == '00:00:00') {

                            $total_break_time = getTotalHours($break_start_date,$breakDate,
                                            $break_in_time,$breakTime);

                            $time = explode(':', $total_break_time.":00");
                            $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                            // end brek...
                            $m->set_data('break_end_date',$breakDate);
                            $m->set_data('break_out_time',$breakTime);
                            $m->set_data('break_out_latitude',$break_latitude);
                            $m->set_data('break_out_longitude',$break_longitude);
                            $m->set_data('total_break_time',$total_break_time);
                            $m->set_data('total_break_time_minutes',$total_break_time_minutes);

                            $a = array(
                                'break_end_date'=>$m->get_data('break_end_date'),
                                'break_out_time'=>$m->get_data('break_out_time'),
                                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                                'break_out_longitude'=>$m->get_data('break_out_longitude'),
                                'total_break_time'=>$m->get_data('total_break_time'),
                                'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
                            );
                            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");
                            $response["break_in_out_status"] = "1";
                            $response["message"] = "Hi ".$user_first_name.', Your '. $breakData['attendance_type_name']." Ended";

                        } else {

                            $m->set_data('attendance_id',$attendance_id);
                            $m->set_data('attendance_type_id',$attendance_type_id);
                            $m->set_data('break_start_date',$breakDate);
                            $m->set_data('break_in_time',$breakTime);
                            $m->set_data('break_in_latitude',$break_latitude);
                            $m->set_data('break_in_longitude',$break_longitude);

                            $a = array(
                                'attendance_id'=>$m->get_data('attendance_id'),
                                'attendance_type_id' =>$m->get_data('attendance_type_id'),
                                'break_start_date' =>$m->get_data('break_start_date'),
                                'break_in_time'=>$m->get_data('break_in_time'),
                                'break_in_latitude'=>$m->get_data('break_in_latitude'),
                                'break_in_longitude'=>$m->get_data('break_in_longitude')
                            );


                            $breakOut = $d->insert("attendance_break_history_master",$a);
                            $attendance_break_history_id = $con->insert_id;
                            $response["break_in_out_status"] = "0";
                            $response["message"] = "Hi ".$user_first_name.', Your '. "$attendance_type_name Started ";
                        }
                        // out code

                        if ($breakOut == true) {

                            $pDate = $break_start_date." ".$break_in_time;

                            $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                            $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                            $date_a = new DateTime($inTimeAMPM);
                            $date_b = new DateTime($dateTimeCurrent);

                            $interval = $date_a->diff($date_b);
                           
                            $hours = $interval->format('%h');
                            $minutes = $interval->format('%i');
                            $sec = $interval->format('%s');

                            if ($sec < 45) {
                               $sec += 5;
                            }

                            $finalTime =  $breakDate." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                            $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);
                            
                            $response['attendance_break_history_id'] =$attendance_break_history_id;
                            $response['break_in_time'] =date('h:i A',strtotime($break_in_time));
                            $response['break_out_time'] =date('h:i A',strtotime($breakTime));   
                                              
                            $response["date_time"] = $finalTime.'';                     
                            $response["time_diff"] = $time_diff.'';                     
                            
                            $response["status"] = "200";
                            echo json_encode($response);
                            exit();
                        }else{
                            $response["message"] = "Something went wrong";
                            $response["status"] = "201";
                            echo json_encode($response);
                            exit();
                        }
                    }
                }else{

                    $breakInTime = date('H:i:s');
                    $breakInDate = date('Y-m-d');

                    $m->set_data('attendance_id',$attendance_id);
                    $m->set_data('attendance_type_id',$attendance_type_id);
                    $m->set_data('break_start_date',$breakInDate);
                    $m->set_data('break_in_time',$breakInTime);
                    $m->set_data('break_in_latitude',$break_latitude);
                    $m->set_data('break_in_longitude',$break_longitude);

                    $a = array(
                        'attendance_id'=>$m->get_data('attendance_id'),
                        'attendance_type_id' =>$m->get_data('attendance_type_id'),
                        'break_start_date' =>$m->get_data('break_start_date'),
                        'break_in_time'=>$m->get_data('break_in_time'),
                        'break_in_latitude'=>$m->get_data('break_in_latitude'),
                        'break_in_longitude'=>$m->get_data('break_in_longitude')
                    );

                    $attendanceName = strtolower($attendance_type_name);
                    if ($attendanceName=="lunch" || $attendanceName=="lunch break" && $lunch_break_start_time!="00:00:00") {
                        $lunch_break_start_time  = $breakInDate.' '.$lunch_break_start_time;
                        $tempTimeCHeck = date('Y-m-d H:i:s',strtotime("-60 minutes",strtotime($lunch_break_start_time)));

                        $datePunchOUTCheck = $breakInDate.' '.$breakInTime;
                        if ($datePunchOUTCheck < $tempTimeCHeck) {
                            $response["message"] = "Early $attendanceName not allowed";
                            $response["status"] = "201";
                            echo json_encode($response);
                            exit();
                        }

                    } else  if ($attendanceName=="tea" || $attendanceName=="tea break" && $tea_break_start_time!="00:00:00") {
                       $lunch_break_start_time  = $breakInDate.' '.$tea_break_start_time;
                       $tempTimeCHeck = date('Y-m-d H:i:s',strtotime("-60 minutes",strtotime($lunch_break_start_time)));

                       $datePunchOUTCheck = $breakInDate.' '.$breakInTime;
                        if ($datePunchOUTCheck < $tempTimeCHeck) {
                            $response["message"] = "Early $attendanceName not allowed";
                            $response["status"] = "201";
                            echo json_encode($response);
                            exit();
                        }
                    }
                   
                    $punchInQry = $d->insert("attendance_break_history_master",$a);

                    $attendance_break_history_id = $con->insert_id;

                    if ($punchInQry == true) {
                        $response["message"] = "Hi ".$user_first_name.', Your '."$attendance_type_name Started";
                        $response["break_in_out_status"] = "0";
                        $response['break_in_time'] =date('h:i A',strtotime($breakInTime));
                        $response["attendance_break_history_id"] = $attendance_break_history_id."";
                        $response["status"] = "200";
                        echo json_encode($response);
                        exit();
                    }else{
                        $response["message"] = "Something went wrong";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }  
                }
            }else{

                $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND shift_time_id = '$shift_time_id' $varQry");

                if (mysqli_num_rows($isPunchIn) > 0) {
                    $aData = mysqli_fetch_array($isPunchIn);

                    if ($aData['attendance_date_end'] != "00:00:00") {
                        $response["message"] = "Hi ".$user_first_name.', '."You have already punched out";//.date('d-m-Y',strtotime($aData['attendance_date_end']));                        
                    }else{
                        $response["message"] = "Hi ".$user_first_name.', '."You have already punched in";//.date('d-m-Y',strtotime($aData['attendance_date_start']));                        
                    }
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }else{
                    $response["message"] = "Hi ".$user_first_name.', '."Please punch in first";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }                
            }        
        } else if($_POST['syncRequest']=="syncRequest" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $totalEntry  = count($_POST['user_id_raw']);

            $successEntry = array();
            for ($i=0; $i <$totalEntry ; $i++) { 
                $user_id = $_POST['user_id'][$i];
                $latitude = $_POST['latitude'][$i];
                $longitude = $_POST['longitude'][$i];
                $checkShift = $d->selectRow("*","users_master","user_id = '$user_id'");
                $userData = mysqli_fetch_array($checkShift);
                $shift_time_id = $userData['shift_time_id'];
                $unit_id = $userData['unit_id'];
                $user_full_name = $userData['user_full_name'];
                $user_profile_pic = $userData['user_profile_pic'];
                $user_designation = $userData['user_designation'];
                $user_token = $userData['user_token'];
                $device = $userData['device'];
                $user_first_name = $userData['user_first_name'];
                $user_first_name = $userData['user_first_name'];



                if ($shift_time_id > '0') {

                $shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
                $shiftData = mysqli_fetch_array($shiftQ);
                $perdayHours = $shiftData['per_day_hour'];
                $shift_start_time = $shiftData['shift_start_time'];
                $per_day_hour = $shiftData['per_day_hour'];
                $shift_end_time = $shiftData['shift_end_time'];
                $late_time_start = $shiftData['late_time_start'];
                $early_out_time = $shiftData['early_out_time'];
                $half_day_time_start = $shiftData['half_day_time_start'];
                $halfday_before_time = $shiftData['halfday_before_time'];
                $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                $shift_type = $shiftData['shift_type'];
                $maximum_in_out = $shiftData['maximum_in_out'];


                $punchInTime =$_POST['time'][$i];
                $punchInDate =$_POST['date'][$i];
                $punchOUTTime = $_POST['time'][$i];
                $punchOUTDate = $_POST['date'][$i];
               
                $punchInDateTime = $punchInDate.' '.$punchInTime;

                $attendance_date_start =$_POST['date'][$i];
                $attendance_type_id =$_POST['breakId'][$i];

                $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$user_id' AND leave_start_date = '$punchInDate' AND leave_status = '1'");

                $hasLeave = false;

                if (mysqli_num_rows($qryLeave) > 0) {

                    $hasLeave = true;

                    $leaveData = mysqli_fetch_array($qryLeave);

                    $leave_id = $leaveData['leave_id'];
                    $leave_type_id = $leaveData['leave_type_id'];
                    $paid_unpaid = $leaveData['paid_unpaid'];
                    $leave_reason = $leaveData['leave_reason'];
                    $leave_day_type = $leaveData['leave_day_type'];
                    $half_day_session = $leaveData['half_day_session'];
                }
                
                    if ($_POST['inOut'][$i]==0) {
                        // for In Code
                        if ($_POST['breakId'][$i]>0) {
                            // brek in Code
                            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND unit_id = '$unit_id' AND shift_time_id = '$shift_time_id' AND attendance_date_start = '$punchInDate'");
                            $isPunchIn = mysqli_fetch_array($isPunchIn);
                            $attendance_id = $isPunchIn['attendance_id'];

                            $getTodayBreak = $d->selectRow("attendance_break_history_master.attendance_break_history_id,attendance_break_history_master.attendance_type_id,attendance_break_history_master.break_start_date,attendance_break_history_master.break_end_date,attendance_break_history_master.break_in_time,attendance_break_history_master.break_out_time,attendance_type_master.attendance_type_name,attendance_type_master.is_multipletime_use","attendance_type_master,attendance_break_history_master","attendance_break_history_master.attendance_id = '$attendance_id' AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.attendance_type_id = '$attendance_type_id'","ORDER BY attendance_break_history_master.attendance_break_history_id DESC LIMIT 1");
                            $breakData = mysqli_fetch_array($getTodayBreak);
                            $is_multipletime_use = $breakData['is_multipletime_use'];

                            if ($is_multipletime_use == '0' && mysqli_num_rows($getTodayBreak) > 0) {
                                // code...
                            } else {

                                $m->set_data('attendance_id',$attendance_id);
                                $m->set_data('attendance_type_id',$attendance_type_id);
                                $m->set_data('break_start_date',$punchInDate);
                                $m->set_data('break_in_time',$punchInTime);
                                $m->set_data('break_in_latitude',$latitude);
                                $m->set_data('break_in_longitude',$longitude);

                                $a = array(
                                    'attendance_id'=>$m->get_data('attendance_id'),
                                    'attendance_type_id' =>$m->get_data('attendance_type_id'),
                                    'break_start_date' =>$m->get_data('break_start_date'),
                                    'break_in_time'=>$m->get_data('break_in_time'),
                                    'break_in_latitude'=>$m->get_data('break_in_latitude'),
                                    'break_in_longitude'=>$m->get_data('break_in_longitude')
                                );
                                if ($attendance_id>0) {
                                    $punchInQry = $d->insert("attendance_break_history_master",$a);
                                }
                            }


                            array_push($successEntry, $_POST['user_id_raw'][$i]);

                        } else {
                            // attendance in code

                            if ($shift_type == "Night") {

                                $attendance_date_previous_day = date('Y-m-d',strtotime('-1 days',strtotime($punchInDate)));

                                $isPunchIn = $d->selectRow("attendance_master.*,leave_master.leave_id","attendance_master LEFT JOIN leave_master ON leave_master.leave_start_date = '$punchInDate' AND leave_master.user_id = '$user_id' AND leave_master.user_id = attendance_master.user_id AND leave_master.leave_day_type = '1'","attendance_master.user_id = '$user_id' AND attendance_master.society_id = '$society_id' AND  attendance_master.shift_time_id = '$shift_time_id' AND  attendance_master.attendance_date_start = '$punchInDate'");

                                $punchInSize = mysqli_num_rows($isPunchIn);

                                if ($punchInSize == 0) {
                                    $runQry = true;
                                }else{
                                    $runQry = false;
                                }

                            }else{
                                
                                $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND unit_id = '$unit_id' AND shift_time_id = '$shift_time_id' AND attendance_date_start = '$punchInDate'");

                                $punchInSize = mysqli_num_rows($isPunchIn);

                                if ($punchInSize > 0) {
                                    $runQry = false;
                                } else {
                                  $runQry = true;  
                                }
                            }

                           

                            if ($runQry == true) {
                                $punch_image = "";
                                if ($_FILES["punch_image"]["tmp_name"][$i] != null) {
                                     // code...
                                    $file11=$_FILES["punch_image"]["tmp_name"][$i];
                                    $extId = pathinfo($_FILES['punch_image']['name'][$i], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["punch_image"]["name"][$i]);
                                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                                        move_uploaded_file($_FILES["punch_image"]["tmp_name"][$i], "../img/attendance/" . $punch_image);
                                    } 
                                }
                                

                                $min = date('i',strtotime($late_time_start));
                                $defaultINTime = $punchInDate." ".$shift_start_time;
                                $strMin = "+".$min." minutes";
                                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                                if ($punchInDateTime > $defaultTime && $late_time_start != '') {
                                    $late_in = "1";
                                }else{
                                    $late_in = "0";
                                }

                                if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 1) {
                                    $late_in = "0";
                                }

                                $m->set_data('shift_time_id',$shift_time_id);
                                $m->set_data('total_shift_hours',$per_day_hour);
                                $m->set_data('society_id',$society_id);
                                $m->set_data('unit_id',$unit_id);
                                $m->set_data('user_id',$user_id);
                                $m->set_data('attendance_date_start',$punchInDate);
                                $m->set_data('punch_in_time',$punchInTime);
                                $m->set_data('punch_in_latitude',$latitude);
                                $m->set_data('punch_in_longitude',$longitude);
                                $m->set_data('punch_in_image',$punch_image);
                                $m->set_data('late_in',$late_in);
                                $m->set_data('punch_in_address',$google_address);
                                $m->set_data('punch_in_face_app_location',$device_location);
                                $m->set_data('attendance_status',"1");
                                $m->set_data('attendance_in_from',"0");
                                $m->set_data('offline_sync_in',"1");
                                $m->set_data('offline_sync_in_time',date('Y-m-d H:i:s'));
                                $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

                                $a = array(
                                    'shift_time_id'=>$m->get_data('shift_time_id'),
                                    'total_shift_hours'=>$m->get_data('total_shift_hours'),
                                    'society_id' =>$m->get_data('society_id'),
                                    'unit_id'=>$m->get_data('unit_id'),
                                    'user_id'=>$m->get_data('user_id'),
                                    'attendance_date_start'=>$m->get_data('attendance_date_start'),
                                    'punch_in_time'=>$m->get_data('punch_in_time'),
                                    'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                                    'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                                    'punch_in_image'=>$m->get_data('punch_in_image'),
                                    'late_in'=>$m->get_data('late_in'),
                                    'attendance_status'=>$m->get_data('attendance_status'),
                                    'attendance_in_from'=>$m->get_data('attendance_in_from'),
                                    'offline_sync_in'=>$m->get_data('offline_sync_in'),
                                    'offline_sync_in_time'=>$m->get_data('offline_sync_in_time'),
                                    'attendance_created_time'=>$m->get_data('attendance_created_time'),
                                    'punch_in_address'=>$m->get_data('punch_in_address'),
                                    'punch_in_face_app_location'=>$m->get_data('punch_in_face_app_location'),
                                );

                                $punchInQry = $d->insert("attendance_master",$a);
                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            } else {
                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            }

                        }
                    } else  if ($_POST['inOut'][$i]==1) {
                         // for out Code
                        if ($_POST['breakId'][$i]>0) {
                            // brek out Code

                            $m->set_data('break_end_date',$punchInDate);
                            $m->set_data('break_out_time',$punchInTime);
                            $m->set_data('break_out_latitude',$latitude);
                            $m->set_data('break_out_longitude',$longitude);

                            $a = array(
                                'break_end_date'=>$m->get_data('break_end_date'),
                                'break_out_time'=>$m->get_data('break_out_time'),
                                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                                'break_out_longitude'=>$m->get_data('break_out_longitude'),
                            );

                            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_type_id = '$attendance_type_id' AND break_end_date='0000-00-00'");
                             array_push($successEntry, $_POST['user_id_raw'][$i]);

                        } else {
                            // attendance out code
                            $qq = $d->selectRow("*","attendance_master","attendance_date_start = '$punchInDate' AND user_id = '$user_id' AND attendance_date_end='0000-00-00'");
                            $dataQ = mysqli_fetch_array($qq);
                            if (mysqli_num_rows($qq)>0) {
                                $attendance_id = $dataQ['attendance_id'];
                                $todayLateIn  = $dataQ['late_in'];
                                $punch_in_time = $dataQ["punch_in_time"];
                                $attendance_date_start = $dataQ["attendance_date_start"];
                                $datePunchIN = $attendance_date_start.' '.$punch_in_time;

                                $punch_image = "";
                                if ($_FILES["punch_image"]["tmp_name"][$i] != null) {
                                    // code...
                                    $file11=$_FILES["punch_image"]["tmp_name"][$i];
                                    $extId = pathinfo($_FILES['punch_image']['name'][$i], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["punch_image"]["name"][$i]);
                                        $punch_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                                        move_uploaded_file($_FILES["punch_image"]["tmp_name"][$i], "../img/attendance/" . $punch_image);
                                    } 
                                }

                                $min = date('i',strtotime($early_out_time));
                                $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                                $strMin = "-".$min." minutes";
                                $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));
                                $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

                                if ($datePunchOUT >= $defaultOutTime && $early_out_time != '') {
                                    $early_out = "0";
                                }else{
                                    $early_out = "1";              
                                }

                                if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                                    $early_out = "0";
                                }
                                
                                $parts = explode(':', $perdayHours);
                                $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
                                $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

                                $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                                $punch_in_time,$punchOUTTime);

                                $times[] = $totalHours;

                                $time = explode(':', $totalHours.":00");
                                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                                $avgWorkingDays = $total_minutes/$perDayHourMinute;

                                $avg_working_days = round($avgWorkingDays * 2) / 2;

                                $datePunchOUT = $punchOUTDate.' '.$punchOUTTime;
                                $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                                $query11Data=mysqli_fetch_array($query11);

                                if ($query11Data['SUM(last_distance)'] > 0) {
                                    $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                                }else{
                                    $total_travel_meter = '0';
                                }

                                $m->set_data('attendance_date_end',$punchOUTDate);
                                $m->set_data('punch_out_time',$punchOUTTime);
                                $m->set_data('punch_out_latitude',$latitude);
                                $m->set_data('punch_out_longitude',$longitude);
                                $m->set_data('punch_out_image',$punch_image);
                                $m->set_data('avg_working_days',$avg_working_days);
                                $m->set_data('early_out',$early_out);
                                $m->set_data('total_working_hours',$totalHours);
                                $m->set_data('punch_out_address',$google_address);
                                $m->set_data('punch_out_face_app_location',$device_location);
                                $m->set_data('total_travel_meter',$total_travel_meter);
                                $m->set_data('offline_sync_out',"1");
                                $m->set_data('offline_sync_out_time',date('Y-m-d H:i:s'));
                                $m->set_data('attendance_out_from',"0");

                                $a = array(
                                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                                    'punch_out_time'=>$m->get_data('punch_out_time'),
                                    'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                                    'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                                    'avg_working_days'=>$m->get_data('avg_working_days'),
                                    'punch_out_image'=>$m->get_data('punch_out_image'),
                                    'early_out'=>$m->get_data('early_out'),
                                    'total_working_hours'=>$m->get_data('total_working_hours'),
                                    'offline_sync_out'=>$m->get_data('offline_sync_out'),
                                    'offline_sync_out_time'=>$m->get_data('offline_sync_out_time'),
                                    'attendance_out_from'=>$m->get_data('attendance_out_from'),
                                    'punch_out_address'=>$m->get_data('punch_out_address'),
                                    'punch_out_face_app_location'=>$m->get_data('punch_out_face_app_location'),
                                    'total_travel_meter'=>$m->get_data('total_travel_meter'),
                                );

                                $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                                 array_push($successEntry, $_POST['user_id_raw'][$i]);


                                // Check Half Day

                                $qryLeave = $d->select("leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                                $hasLeave = false;

                                if (mysqli_num_rows($qryLeave) > 0) {

                                    $hasLeave = true;

                                    $leaveData = mysqli_fetch_array($qryLeave);

                                    $leave_id = $leaveData['leave_id'];
                                    $leave_type_id = $leaveData['leave_type_id'];
                                    $paid_unpaid = $leaveData['paid_unpaid'];
                                    $leave_reason = $leaveData['leave_reason'];
                                    $leave_day_type = $leaveData['leave_day_type'];
                                }

                                $leaveValue = false;

                                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                                    $leaveValue = true;
                                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                                    $leaveValue = true;
                                }

                                $alreadyLeaveQryCheck = false; 

                                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                                if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != "00:00:00") {
                                    $noLeave = true;
                                }else{
                                    $noLeave = false;
                                }

                                $tabPosition = "0"; // 1 for all leaves, 0 for my leaves
                                
                                if ($noLeave == false) {

                                    if ($leaveValue == true) {

                                        $leave_total_days = 1;

                                        if ($total_minutes < $maximum_halfday_hours_min) {
                                            $leaveType = "1";
                                        }else{
                                            $leaveType = "0";
                                        }

                                        if ($hasLeave == true) {

                                            $m->set_data('leave_type_id',$leave_type_id);
                                            $m->set_data('paid_unpaid',$paid_unpaid);
                                            $m->set_data('leave_day_type',$leaveType);

                                            $a = array(
                                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                            );

                                            $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");
                                            
                                        }else{
                                            $m->set_data('leave_type_id',"0");
                                            $m->set_data('society_id',$society_id);
                                            $m->set_data('paid_unpaid',"1");
                                            $m->set_data('unit_id',$unit_id);
                                            $m->set_data('user_id',$user_id);
                                            $m->set_data('auto_leave_reason',"Auto Leave");
                                            $m->set_data('leave_start_date',$attendance_date_start);
                                            $m->set_data('leave_end_date',$punchOUTDate);
                                            $m->set_data('leave_total_days',$leave_total_days);
                                            $m->set_data('leave_day_type',$leaveType);
                                            $m->set_data('leave_status',"1");
                                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                            $a = array(
                                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                                'society_id' =>$m->get_data('society_id'),
                                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                                'unit_id'=>$m->get_data('unit_id'),
                                                'user_id'=>$m->get_data('user_id'),
                                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                                'leave_status'=>$m->get_data('leave_status'),
                                                'leave_created_date'=>$m->get_data('leave_created_date'),
                                            );

                                            $leaveQry = $d->insert("leave_master",$a);

                                            $alreadyLeaveQryCheck = true;

                                            if ($leaveType == 0) {
                                                $avfWorkingDay = "0";
                                                $title = "Auto Leave Applied - Full Day";
                                            }else{
                                                $avfWorkingDay = "0.5";
                                                $title = "Auto Leave Applied - Half Day";
                                            }
                                            
                                            $description = "For Date : ".$attendance_date_start;

                                            if ($device == 'android') {
                                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            } else if ($device == 'ios') {
                                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                            }

                                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                        }

                                        $m->set_data('auto_leave',"1");
                                        $m->set_data('avg_working_days',$avfWorkingDay);

                                        $a1 = array(
                                            'auto_leave'=>$m->get_data('auto_leave'),
                                            'avg_working_days'=>$m->get_data('avg_working_days'),
                                        );

                                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                                    }
                                }else if($noLeave == true && $hasLeave == true){
                                    $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                                    $title = "Applied Leave Cancelled";
                                    $description = "For Date : ".$attendance_date_start;

                                    if ($device == 'android') {
                                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                    } else if ($device == 'ios') {
                                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                    }

                                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                                }

                                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND late_in = '1'");

                                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND early_out = '1'");

                                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                                if ($maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                                    $titleMessage = "Maximum Late In($currentMonthLateInCount) & Early Out($currentMonthEarlyOutCount) Limit Reached";

                                    $m->set_data('leave_type_id',"0");
                                    $m->set_data('society_id',$society_id);
                                    $m->set_data('paid_unpaid',"1");
                                    $m->set_data('unit_id',$unit_id);
                                    $m->set_data('user_id',$user_id);
                                    $m->set_data('auto_leave_reason',$titleMessage);
                                    $m->set_data('leave_start_date',$attendance_date_start);
                                    $m->set_data('leave_end_date',$punchOUTDate);
                                    $m->set_data('leave_total_days',"1");
                                    $m->set_data('leave_day_type',"1");
                                    $m->set_data('leave_status',"1");
                                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                                    $a = array(
                                        'leave_type_id'=>$m->get_data('leave_type_id'),
                                        'society_id' =>$m->get_data('society_id'),
                                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                        'unit_id'=>$m->get_data('unit_id'),
                                        'user_id'=>$m->get_data('user_id'),
                                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                        'leave_start_date'=>$m->get_data('leave_start_date'),
                                        'leave_end_date'=>$m->get_data('leave_end_date'),
                                        'leave_total_days'=>$m->get_data('leave_total_days'),
                                        'leave_day_type'=>$m->get_data('leave_day_type'),
                                        'leave_status'=>$m->get_data('leave_status'),
                                        'leave_created_date'=>$m->get_data('leave_created_date'),
                                    );

                                    $leaveQry = $d->insert("leave_master",$a);

                                    $title = "Auto Leave Applied - Half Day";

                                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                                    if ($device == 'android') {
                                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                    } else if ($device == 'ios') {
                                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                                    }

                                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                                    $m->set_data('auto_leave',"1");
                                    $m->set_data('avg_working_days',"0.5");

                                    $a1 = array(
                                        'auto_leave'=>$m->get_data('auto_leave'),
                                        'avg_working_days'=>$m->get_data('avg_working_days'),
                                    );

                                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                                }

                                // END checking half day

                                $m->set_data('break_end_date',$punchOUTDate);
                                $m->set_data('break_out_time',$punchOUTTime);

                                $a = array(
                                    'break_end_date'=>$m->get_data('break_end_date'),
                                    'break_out_time'=>$m->get_data('break_out_time'),
                                );

                                $qry = $d->update("attendance_break_history_master",$a,"attendance_id = '$attendance_id'  AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");
                            } else {
                                array_push($successEntry, $_POST['user_id_raw'][$i]);
                            }

                        }
                    }

                   
                } else {
                    array_push($successEntry, $_POST['user_id_raw'][$i]);
                }
            }


            $response["success_ids"] = implode(",", $successEntry);
            $response["message"] = "Data Sys Successfully";
            $response["status"] = "200";
            echo json_encode($response);  
        }         
    }else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }
}

function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "00:00";
    }
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}
?>