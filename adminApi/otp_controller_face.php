<?php
include_once 'lib.php';
 
if (isset($_POST) && !empty($_POST)) {
	$societyLngName=  $xml->string->society;
	if ($key == $keydb ) {
		$response = array();
		extract(array_map("test_input", $_POST));
		$today = date('Y-m-d');
		$todayTime = date('Y-m-d H:i:s');

		if (isset($send_otp) && $send_otp == 'send_otp' && filter_var($user_mobile, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {
			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
			// $user_password=mysqli_real_escape_string($con, $user_password);
			if (strlen($user_mobile) < 8) {
				$response["message"] = "Please Enter Valid Mobile Number";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$q = $d->select("users_master,block_master,floors_master",
				"users_master.delete_status=0 AND users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0
                AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                AND  users_master.society_id ='$society_id'
              AND users_master.user_status=1 AND users_master.user_status=1 AND users_master.country_code='$country_code'");

			$qPending = $d->select("users_master,block_master,floors_master",
				"users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0
                AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                AND users_master.society_id ='$society_id'
              AND  users_master.user_status=0 AND users_master.country_code='$country_code'");

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE) {

				$old_otp = $user_data['otp'];

				$user_id = $user_data['user_id'];
				$digits = 6;

				$to_time = strtotime($todayTime);
				$from_time = strtotime($user_data['otp_attempt_time']);
				$minitues = round(abs($to_time - $from_time) / 60, 2);

				if ($user_data['is_demo']==1 && $old_otp!='') {
					$otp = $old_otp;
				} else if ($old_otp != "" && strlen($old_otp) == 6 && $minitues < 60) {
					$otp = $old_otp;
				} else {
					$otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
					$otp = $otp;
				}


				if ($minitues < 60 && $user_data['opt_attempt'] > 3) {
					$response["message"] = "Too many attempts by you, Please  try again after 1 hour !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}

				if ($minitues > 60) {
					$opt_attempt = 1;
				} else if ($user_data['opt_attempt'] == "" || $opt_attempt < 1) {
					$opt_attempt = $user_data['opt_attempt'] + 1;
				} else {
					$opt_attempt = $user_data['opt_attempt'] + 1;
				}

				$m->set_data('otp', $otp);
				$a = array(
					'otp' => $m->get_data('otp'),
					'opt_attempt' => $opt_attempt,
					'otp_attempt_time' => $todayTime,
				);

				$d->update("users_master", $a, "user_id='$user_id'");
				$qSociety = $d->select("society_master", "society_id ='$society_id'");
				$user_data_society = mysqli_fetch_array($qSociety);
				$society_name = $user_data_society['society_name'];
				if ($user_data_society['society_status'] == '0') {
				
					$smsObj->send_otp($user_mobile, $otp,$country_code);
					$d->add_sms_log($user_mobile,"User App OTP SMS",$society_id,$country_code,1);
					$otp_send_successfully = $xml->string->otp_send_successfully;
					$response["message"] = "OTP Sent";
					
					$a = mt_rand(100000,999999); 

					$response["trx_id"] = $a.$user_id.'';
					$response["status"] = "200";
					echo json_encode($response);
				} else {
					$response["message"] = "Your company is Deactive.";
					$response["status"] = "204";
					echo json_encode($response);
				}
			} else {
				if (mysqli_num_rows($qPending) > 0) {
					$response["message"] = "Account Not Activated Please Contact to $societyLngName Admin";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				} else {
					$mobile_number_is_not_register = $xml->string->mobile_number_is_not_register;
					$response["message"] = "Mobile number is not registered";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}
			}
		} else if (isset($verify_otp) && $verify_otp == 'verify_otp' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
			$otp = mysqli_real_escape_string($con, $otp);

			$q11 = $d->select("users_master", "user_mobile ='$user_mobile' ");
			$userData = mysqli_fetch_array($q11);

			$to_time = strtotime($todayTime);
			$from_time = strtotime($userData['otp_verfiy_time']);
			$minitues = round(abs($to_time - $from_time) / 60, 2);

			if ($minitues < 60 && $userData['otp_verify_attempt'] > 5) {
				$response["message"] = "Too many attempts by you, Please  try again after 1 hour !";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			if(strlen($otp) == 6){
				$q = $d->select("users_master,block_master,floors_master",
					"users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0  AND users_master.otp='$otp' AND users_master.country_code='$country_code'
                AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                 AND users_master.society_id ='$society_id'
             	");
			}

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE ) {

				if ($user_data['unit_status'] != '4' && $user_data['user_status'] != '2') {

					$user_id = $user_data['user_id'];


					$m->set_data('otp', '');
					$m->set_data('opt_attempt', '');
					$m->set_data('otp_attempt_time', '');
					$m->set_data('otp_verify_attempt', '');
					$m->set_data('otp_verfiy_time', '');

					$a = array(
						'otp' => $m->get_data('otp'),
						'opt_attempt' => $m->get_data('opt_attempt'),
						'otp_attempt_time' => $m->get_data('otp_attempt_time'),
						'otp_verify_attempt' => $m->get_data('otp_verify_attempt'),
						'otp_verfiy_time' => $m->get_data('otp_verfiy_time'),
					);

					$d->update("users_master", $a, "user_mobile='$user_mobile' AND user_mobile!='' AND user_mobile!='0'");	

					$qSociety = $d->select("society_master", "society_id ='$society_id'");
					$user_data_society = mysqli_fetch_array($qSociety);

					if ($user_data_society['society_status'] == '0') {

						$user_full_name= $user_data['user_full_name'];

	                    $units_id=$user_data['unit_id'];

	                    $response["user_id"]=$user_data['user_id'].'';
	                    $response["unit_id"]=$user_data['unit_id'].'';
	                    $response["user_first_name"]=$user_data['user_first_name'].'';
	                    $response["user_last_name"]=$user_data['user_last_name'].'';
	                    $response["user_mobile"]=$user_data['user_mobile'].'';
	                    $response["user_full_name"]=html_entity_decode($user_full_name).'';
	                    $response["society_id"]=$user_data['society_id'];
	                    $response["shift_time_id"]=$user_data['shift_time_id'];
	                    $response["designation"]=$user_data['user_designation'];
	                     
	                    if ($user_data['user_profile_pic'] != '') {
	                        $response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];
	                    } else {
	                        $response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
	                    }

	                    $response["user_face_id"]=$user_data['user_face_id'].'';
	                    $response["user_face_data"]=$user_data['user_face_data'].'';
	                    $response["face_added_date"]=date("d M Y h:i A", strtotime($user_data['face_added_date'].''));

	                    if ($user_data['face_data_image'] != '') {
	                        $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image'];
	                    } else {
	                        $response["face_data_image"] = "";
	                    }

	                    if ($user_data['face_data_image_two'] != '') {
	                        $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image_two'];
	                    } else {
	                        $response["face_data_image_two"] = "";
	                    }
						$response["message"] = "OTP Verified";
						$response["status"] = "200";
						echo json_encode($response);
					} else {
						$response["message"] = "Society deactive.";
						$response["status"] = "204";
						echo json_encode($response);
					}

				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}

			} else {

				if ($minitues > 60) {
					$otp_verify_attempt = 1;
				} else {

					$otp_verify_attempt = $userData['otp_verify_attempt'] + 1;
				}

				$otp_verfiy_time = date("Y-m-d H:i:s");

				$a = array(
					'otp_verify_attempt' => $otp_verify_attempt,
					'otp_verfiy_time' => $otp_verfiy_time,
				);

				$d->update("users_master", $a, "user_mobile='$user_mobile'");

				$otp_not_match = $xml->string->otp_not_match;

				$response["message"] = "OPT not matched";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else {
			$response["message"] = "Invalid Request";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {
		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}

?>