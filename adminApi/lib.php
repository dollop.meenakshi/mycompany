<?php
session_start();


$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../apAdmin/lib/dao.php';
include_once '../apAdmin/lib/sms_api.php';
include_once '../apAdmin/lib/model.php';
include_once '../apAdmin/fcm_file/admin_fcm.php';
include_once '../apAdmin/fcm_file/gaurd_fcm.php';
include_once '../apAdmin/fcm_file/resident_fcm.php';

$d = new dao();
$m = new model();
$smsObj = new sms_api();

$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$default_time_zone=$d->getTimezone($_REQUEST['society_id']);
date_default_timezone_set($default_time_zone);
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

$base_url=$m->base_url();
$keydb = $m->api_key();
if(is_array($headers)) {
	extract($headers);
}
$key = $_SERVER['HTTP_KEY'];  


?>