<?php //include 'header.php'; ?>

<body>
        <!-- Start wrapper-->
        <div id="wrapper">
            <!--Start sidebar-wrapper-->
            <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
                <div class="brand-logo">
                    <a href="welcome.php">
                        <img src="../img/logo.png" class="logo-icon" alt="logo">
                        <h5 class="logo-text text-white">My Company</h5>
                    </a>
               </div>
                <ul class="sidebar-menu do-nicescrol" >
                    <li class="sidebar-header text-center"><b><?= $_COOKIE['user_name']; ?></b></li>
                    <li class="">
                        <a href="welcome.php" class="waves-effect ">
                        <i class="fa fa-home"></i> <span>Dashboard</span> 
                        </a>
                    </li>
                    <li class="">
                        <a href="commercialService.php" class="waves-effect ">
                        <i class="fa fa-sign-in"></i> <span>Entry</span> 
                        </a>
                    </li>
                    <li class="">
                        <a href="viewVisitors.php" class="waves-effect ">
                        <i class="fa fa-eye"></i> <span>View Visitors</span> 
                        </a>
                    </li>
                    <li>
                        <a href="javaScript:void();" class="waves-effect">
                            <i class="fa fa-file-o"></i> <span>Reports</span>
                            <i class="fa fa-angle-left float-right"></i>
                        </a>
                        <ul class="sidebar-submenu">
                            <li><a href="daywiseReports.php"><i class="zmdi zmdi-star-outline"></i>Weekly Reports</a></li>
                            <li><a href="dateReports.php"><i class="zmdi zmdi-star-outline"></i>Monthly Reports</a></li>
                            <li><a href="datewiseReports.php"><i class="zmdi zmdi-star-outline"></i>Date Wise Reports</a></li>
                            <li><a href="monthlyReports.php"><i class="zmdi zmdi-star-outline"></i>Yearly Reports</a></li>
                            <li><a href="userReports.php"><i class="zmdi zmdi-star-outline"></i>User Reports</a></li>
                        </ul>
                    </li>
                </ul>   
            </div>
        <!--End sidebar-wrapper-->

        <!--Start topbar header-->
        <header class="topbar-nav">
            <nav class="navbar navbar-expand fixed-top bg-white">
                <ul class="navbar-nav mr-auto align-items-center">
                    <li class="nav-item">
                        <a class="nav-link toggle-menu" href="javascript:void();">
                        <i class="icon-menu menu-icon text-dark"></i>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                            <span class="user-profile ">
                            

                            <img onerror="this.src='../apAdmin/img/user.png'"  src="<?php echo $profilePath; ?>" class="img-circle" alt="user avatar"> <span class="hidden-xs"> <?php
                            $nameAry = explode(" ",$_COOKIE['user_name']);
                            echo $nameAry[0];
                            ?> </span> <i class="fa fa-angle-down "></i></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-item user-details">
                            <a href="javaScript:void();">
                            <div class="media">
                                <div class="avatar"><img onerror="this.src='../apAdmin/img/user.png'"  class="align-self-start mr-3" src="<?php echo $profilePath; ?>" alt="user avatar"></div>
                                <div class="media-body">
                                <h6 class="mt-2 user-title"><?php echo $_COOKIE['user_name']; ?></h6>
                                <!-- <p class="user-subtitle"><?php echo $_COOKIE['user_name'] ?></p> -->
                                </div>
                            </div>
                            </a>
                            </li>
                            <li class="dropdown-divider"></li>
                        
                            <a class="d-md-none " href="sound.php"><li class="dropdown-item"><i class="fa fa-bell"></i> <?php echo $xml->string->my_notification_setting; ?></li></a>
                            <li class="dropdown-divider"></li>
                            <form method="POST" action="logout.php">
                            <button class="form-btn dropdown-item p-0"><li class="dropdown-item"><i class="icon-power mr-2"></i> Logout</li></button>
                            </form>
                        </ul>
                    </li>
                    <li class="nav-item dropdown-lg">
                        <form method="POST" action="logout.php">
                            <button class="form-btn btnLogout btn-link form-btn nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect"><i class="fa fa-power-off"></i></button>
                        </form>

                    </li>
                </ul>
            </nav>
        </header>
        <!--End topbar header-->