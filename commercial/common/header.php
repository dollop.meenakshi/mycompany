<?php
error_reporting(0);
session_start();
$_SESSION["token"] = md5(uniqid(mt_rand(), true));

include 'object.php';
include 'checkLogin.php';
if (isset($_COOKIE['user_id'])) {
    $user_id = $_COOKIE['user_id'];
    $aq=$d->select("users_master,society_master","society_master.society_id=users_master.society_id AND users_master.user_id='$user_id'");
    $userData= mysqli_fetch_array($aq);
    //extract($userData);
    if ($userData['delete_status']==1) {
       $_SESSION['msg1']="Your Account Deactivated.";
       header("location:logout.php");
       exit();
    }
    if(file_exists("../img/users/recident_profile/$userData[user_profile_pic]")) {
        $profilePath = "../img/users/recident_profile/$userData[user_profile_pic]";
    } else {
        $profilePath= "../apAdmin/img/user.png";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>
        <title>Entry Logs | My Company</title>
        <link rel="icon" href="../img/fav.png" type="image/png">
        <link href="../apAdmin/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="../apAdmin/assets/css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="css/sidebar-menu.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/css/app-style9.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="../apAdmin/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="../apAdmin/assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../apAdmin/assets/plugins/notifications/css/lobibox.min.css"/>
        <link href="../apAdmin/assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/plugins/inputtags/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="../apAdmin/assets/plugins/jquery-multi-select/multi-select.css" rel="stylesheet" type="text/css">
        <link href="../apAdmin/assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
        <link href="../apAdmin/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="../apAdmin/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="../apAdmin/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
        <link href="../apAdmin/assets/plugins/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
        <link href="../apAdmin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <style type="text/css">
            .btn-group{
                margin-bottom: 20px;
            }
            .swal{
                font-size: 1.6rem !important;
                color:red !important;
            }
        </style>

    </head>
    <style type="text/css">
        .col-sm-6{
            width: 50% !important;
        }
        .col-sm-9{
            width: 75% !important;
        }
        .col-sm-3{
            width: 25% !important;
        }
        .btn-reports{
            padding: 7px 15px !important;
        }
    </style>

    