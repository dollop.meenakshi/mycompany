        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
        <!-- Bootstrap core JavaScript-->
<script src="../apAdmin/assets/js/jquery.min.js"></script>
<script src="../apAdmin/assets/js/popper.min.js"></script>
<script src="../apAdmin/assets/js/bootstrap.min.js"></script>
<script src="../apAdmin/assets/js/jquery-ui.js"></script>
<!-- simplebar js -->
<script src="../apAdmin/assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="../apAdmin/assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="../apAdmin/assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="../apAdmin/assets/js/app-script.js"></script>

<!--Data Tables js-->
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>

<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
<script src="../apAdmin/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>
<!--notification js -->
<script src="../apAdmin/assets/plugins/notifications/js/lobibox.min.js"></script>
<script src="../apAdmin/assets/plugins/notifications/js/notifications.min.js"></script>
<script src="../apAdmin/assets/plugins/notifications/js/notification-custom-script.js"></script>


<!--Sweet Alerts -->
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>

<!-- Chart js -->
<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<!--Peity Chart -->
<script src="../apAdmin/assets/plugins/peity/jquery.peity.min.js"></script>
<!-- Index js -->
<!-- <script src="../apAdmin/assets/js/index.js"></script> -->
<!--Lightbox-->
<script src="../apAdmin/assets/plugins/fancybox/js/jquery.fancybox.min.js"></script>

<script src="../apAdmin/assets/js/custom16.js"></script>

<!-- /////////////Dollop Infotech(October 21 2021)  //////////////  -->
<script src="../apAdmin/assets/js/custom17.js"></script>

<script src="../apAdmin/assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
 
 <?php include 'common/alert.php'; ?>

<!--Form Validatin Script-->
<script src="../apAdmin/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../apAdmin/assets/js/validate7.js"></script>

<!--Select Plugins Js-->
<script src="../apAdmin/assets/plugins/select2/js/select2.min.js"></script>
<!--Inputtags Js-->
<script src="../apAdmin/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

<!--Bootstrap Datepicker Js-->
<script src="../apAdmin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- pg-calendar-master -->
<!-- <script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.min.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.min.js'></script> -->

  <!-- full cALANDER -->
  <script src='../apAdmin/assets/plugins/fullcalendar/js/moment.min.js'></script>
  <script src='../apAdmin/assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>
  <script src="../apAdmin/assets/plugins/fullcalendar/js/fullcalendar-custom-script.js"></script>
  
    <!--Multi Select Js-->
    <script src="../apAdmin/assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="../apAdmin/assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="../apAdmin/assets/js/select.js"></script>

    <!--material date picker js-->
    <script src="../apAdmin/assets/plugins/material-datepicker/js/moment.min.js"></script>
    <script src="../apAdmin/assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
    <script src="../apAdmin/assets/plugins/material-datepicker/js/ja.js"></script>
    <link rel="stylesheet" href="../apAdmin/assets/css/jquery.timepicker.min.css">
    <script src="../apAdmin/assets/js/jquery.timepicker.min.js"></script>

    <script src="../apAdmin/assets/js/datepicker.js"></script>
    <script src="../apAdmin/assets/js/purchase.js"></script>
    
  <script type="text/javascript" src="../apAdmin/assets/js/lazyload.js"></script>
        <?php include 'common/alert.php'; ?>
        <?php if(isset($_SESSION['success'])) {
          echo '<script type="text/javascript">swal("Good job!", "'.$_SESSION['success'].'", "success");</script>';
          unset($_SESSION['success']);
        }else if (isset($_SESSION['error'])) {
          echo '<script type="text/javascript">swal("Error !", "'.$_SESSION['error'].'", "error");</script>';
          unset($_SESSION['error']);
        } ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.validateForm').validate();
            });
            $(".number").keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        </script>
        
    </body>

</html>