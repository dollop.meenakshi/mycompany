<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Visitors</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Visitors</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card card-body">
                    <form id="commercialEntryForm" class="validateForm" action="controller/commercialEntryController.php" method="post" enctype="multipart/form-data">
                        <h4 class="form-header text-uppercase">
                            <i class="fa fa-file"></i>
                            Add Visitors
                        </h4>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Person Name <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <input type="text" required="" autocomplete="off" name="visitor_name" id="visitor_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Contact No. <span class="required">*</span></label>
                                <div class="row m-0">
                                    <!-- <div class="position-relative has-icon-right col-sm-4 col-3">
                                        <select  name="country_code" class="form-control single-select" id="country_code" required="">  
                                                <?php include '../apAdmin/country_code_option_list.php'; ?>
                                        </select>
                                    </div> -->
                                    <div class="position-relative has-icon-right col-sm-12">
                                        <input type="text" name="visitor_mobile" id="visitor_mobile" class="form-control onlyNumber" required="" autocomplete="off" minlength="10" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">No. Of Person <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <input type="text" name="number_of_visitor" id="number_of_visitor" class="form-control onlyNumber" required="" autocomplete="off" min='1' max="200">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Reason <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <input type="text" name="visiting_reason" id="visiting_reason" class="form-control" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Department <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <select class="form-control single-select" onchange="getEmployeeByFloorId(this.value)" name="floor_id" id="floor_id">
                                        <option value="">--Select Department--</option>
                                        <?php
                                        $dq = $d->select("floors_master","block_id='$_COOKIE[block_id]'","ORDER BY floor_sort");
                                        while ($deptData = mysqli_fetch_assoc($dq)) {
                                        ?>
                                            <option value="<?php echo $deptData['floor_id'] ?>"><?php echo $deptData['floor_name'] ?></option>
                                        <?php  } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Employee Name <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <select class="form-control single-select" name="user_id" id="user_id">
                                        <option value="">--Select Employee--</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-md-12">
                                <label class="col-sm-12 text-uppercase">Remarks</label>
                                <div class="form-group col-sm-12">
                                    <textarea name="remarks" id="remarks" maxlength="500" class="form-control" autocomplete="off"></textarea>
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Visitor Type <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <select class="form-control single-select" name="visitor_type" id="visitor_type" required onchange="getVisitorSubType(this.value)">
                                        <option value="">--Select Visitor Type--</option>
                                        <?php
                                        $vtq = $d->select("visitorMainType");
                                        while ($visitorTypeData = mysqli_fetch_assoc($vtq)) {
                                        ?>
                                            <option data-id="<?php echo $visitorTypeData['visitor_main_type_id'] ?>" value="<?php echo $visitorTypeData['visitor_type'] ?>"><?php echo $visitorTypeData['main_type_name'] ?></option>
                                        <?php  } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-sm-12 text-uppercase">Visitor Sub Type <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <select class="form-control single-select" name="visitor_sub_type_id" id="visitor_sub_type_id" required onchange="visitFrom();">
                                        <option value="">--Select Visitor Sub Type--</option>
                                        <?php
                                        $vstq = $d->select("visitorSubType","visitor_main_type_id='$visitor_type'");
                                        while ($visitorSubTypeData = mysqli_fetch_assoc($vstq)) {
                                        ?>
                                            <option value="<?php echo $visitorSubTypeData['visitor_sub_type_id'] ?>"><?php echo $visitorSubTypeData['visitor_sub_type_name'] ?></option>
                                        <?php  } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 visit_from d-none">
                                <label class="col-sm-12 text-uppercase">Visit From <span class="required">*</span></label>
                                <div class="form-group col-sm-12">
                                    <input type="text" name="visit_from" id="visit_from" class="form-control" required="" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" value="<?php echo $_COOKIE['block_id'] ?>" name="block_id">
                            <input type="hidden" value="<?php echo $_COOKIE['society_id'] ?>" name="society_id">
                            <input type="hidden" value="addVisitor>" name="addVisitor">
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">

    $.validator.addMethod("noSpace", function(value, element) { 
        return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");

    $("#commercialEntryForm").validate({
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent()); // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span')); // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element); // default
            }
        },
        rules: {
            visitor_name: {
                required: true,
                noSpace:true,
            },
            visitor_mobile: {
                required: true,
                digits: true,
                noSpace:true,
            },
            number_of_visitor: {
                required: true,
                digits: true,
                min: 1,
                max: 200
            },
            visiting_reason: {
                required: true,
                noSpace:true,
                maxlength: 100,
            },
            floor_id: {
                required: true,
            },
            user_id: {
                required: true,
            },
            visitor_type:{
                required: true,
            },
            visitor_sub_type_id: {
                required: true,
            },
            visit_from: {
                required: true,
                noSpace:true,
            }
        },
        messages: {
            visitor_name: {
                required: "please Enter Name",
                noSpace: "No space please and don't leave it empty",
            },
            visitor_mobile: {
                required: "Please Enter Mobile Number",
                noSpace: "No space please and don't leave it empty",
            },
            number_of_visitor: {
                required: "Please Enter Number of Person"
            },
            visiting_reason: {
                required: "Please Enter reason",
                noSpace: "No space please and don't leave it empty",
            },
            floor_id: {
                required: "Please Select Department",
            },
            user_id: {
                required: "Please Select Employee Name",
            },
            visitor_type:{
                required: "Please Select Visitor Type",
            },
            visitor_sub_type_id: {
                required: "Please Select Visitor Sub Type"
            },
            visit_from: {
                required: "Please Enter Visit From",
                noSpace: "No space please and don't leave it empty",
            }
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit();
        }

    });


    function getEmployeeByFloorId(id) {
        var main_content = "";
        $.ajax({
            url: "controller/commercialEntryController.php",
            type: "POST",
            data: {
                action: "getEmployeeByFloorId",
                floor_id: id,
            },
            dataType: 'json',
            success: function(response) {
                main_content += "<option value=''>--Select Employee--</option>"
                $.each(response.users, function(key, value) {
                    main_content += `<option value=`+value.user_id+`~`+value.unit_id+`>`+value.user_full_name+`</option>`;
                });
                    
                $("#user_id").html(main_content);
            }
        })
    }

    function getVisitorSubType() {
        var id = $('select#visitor_type option:selected').attr('data-id')
        //alert(id);
        var main_content = "";
        $.ajax({
            url: "controller/commercialEntryController.php",
            type: "POST",
            data: {
                action: "getVisitorSubType",
                visitor_main_type_id: id,
            },
            dataType: 'json',
            success: function(response) {
                main_content += "<option value=''>--Select Employee--</option>"
                $.each(response.visitor_sub_type, function(key, value) {
                    main_content += `<option value=`+value.visitor_sub_type_id+`>`+value.visitor_sub_type_name+`</option>`;
                });
                    
                $("#visitor_sub_type_id").html(main_content);
            }
        })
    }

    function visitFrom(){
        var value = $( "#visitor_sub_type_id option:selected" ).text();
        if(value == "Other"){
            $('.visit_from').removeClass('d-none');
        }else{
            $('.visit_from').addClass('d-none');
        }
    }
   
</script>