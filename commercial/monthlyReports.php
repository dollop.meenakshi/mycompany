<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  $currentYear = date('Y');
	$nextYear = date('Y', strtotime('+1 year'));
	$onePreviousYear = date('Y', strtotime('-1 year'));
	$twoPreviousYear = date('Y', strtotime('-2 year'));
  $year = (int)$_REQUEST['year'];
  /* if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-365 Days'));
    $endDate = date('Y-m-d');
    $id = 'All';
  } */
?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Yearly Report</h4>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Yearly Report</li>
            </ol>
        </div>
    </div>

    <form class="branchDeptFilter" action="" method="get">
        <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
            <select name="year" class="form-control">
              <option <?php if($year==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
              <option <?php if($year==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
              <option <?php if($year==$currentYear) { echo 'selected';}?> <?php if($year=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            </select>
          </div>
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
        </div>
    </form>
  <div class="row">
  <div class="col-lg-12">
    <div class="card">
      <!-- <div class="card-header">
          <form method="POST">
          <div class="row">
            <div class="col-sm-12 col-lg-6 form-group">
              <div id="daterange-picker">
                <div class="input-daterange input-group">
                  <input <?php if(isset($_POST['start'])){ ?>
                  value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                  <?php } else{ ?> value="<?php echo date('Y-m-d', strtotime('-365 Days')); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                    <div class="input-group-prepend">
                     <span class="input-group-text">to</span>
                    </div>
                    <input <?php if(isset($_POST['start'])){ ?>
                    value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                  <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                </div>
              </div>
            </div>
            <div class="col-sm-9 col-lg-5 form-group">
              <select class="form-control single-select" name="id">
                <option <?php if($id=='All'){echo "selected";} ?> value="All">All</option>
                <?php 
                    $q = $d->select("users_master","unit_id='$_SESSION[unit_id]' AND user_status=1");
                    while ($userdata = mysqli_fetch_array($q)) {
                ?>
                    <option <?php if($id==$userdata['user_id']){echo "selected";} ?> value="<?php echo $userdata['user_id'] ?>"><?php echo $userdata['user_full_name'] ?></option>
                <?php  }?>
                <option <?php if($id=='0'){echo "selected";} ?> value="0">Other</option>
              </select>
            </div>
            <div class="col-sm-3 col-lg-1 form-group">
              <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-reports btn-success"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </form>
      </div> -->
      <div class="card-body">
        <canvas id="monthwiseCounter"></canvas>
      </div>
    </div>
    </div>
    </div>
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
    if(isset($year) && $year >0){
      $y= $year;
    }else{
      $y= $currentYear;
    }
    
    $monthArrayQuery = array();

    for ($i=01; $i < 13; $i++) { 
        $countMonth = 0;
          $monthQuery = $d->selectRow("visitor_id,number_of_visitor","visitors_master","MONTH(visit_date)='$i' AND YEAR(visit_date)='$y' AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");
        
        while ($monthdataQuery = mysqli_fetch_array($monthQuery)) {
            $countMonth += $monthdataQuery['number_of_visitor'];
        }
        array_push($monthArrayQuery, $countMonth);
    }
    $monthString = implode(",", $monthArrayQuery);

?>
<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">

  if ($('#monthwiseCounter').length) {
   var ctx = document.getElementById('monthwiseCounter').getContext('2d');       
   var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      datasets: [{
        label: 'Visitors',
        data: [<?php echo $monthString ?>],
        backgroundColor: '#5E72E4',
        borderColor: '1a4089',
        borderWidth: 3
      }]
    },
    options: {
      legend: {
        position: 'bottom',
        display: true,
        labels: {
          boxWidth:40
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
      }
    }
  });
 }
</script>
