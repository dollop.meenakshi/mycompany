<?php session_start(); if(isset($_SESSION['login_status'])){ header('location:welcome.php'); } ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Commercial Login | My Company</title>
	<link rel="icon" href="../img/fav.png" type="image/png">
	<link href="../apAdmin/assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="../apAdmin/assets/css/animate.css" rel="stylesheet" type="text/css"/>
	<link href="../apAdmin/assets/css/icons.css" rel="stylesheet" type="text/css"/>
	<link href="../apAdmin/assets/css/app-style9.css" rel="stylesheet"/>
</head>

<body class="bg-dark">
	<div id="wrapper">
		<div class="card card-authentication1 mx-auto my-5">
			<div class="card-body">
				<div class="card-content p-2">
					<div class="text-center">
						<img src="../img/logo.png" alt="My Company Logo" width="130">
					</div>
					<div class="card-title text-uppercase text-center">Set New Password</div>
			  		<?php
	              		include_once '../apAdmin/lib/dao.php';
						include '../apAdmin/lib/model.php';
						$d = new dao();
						$m = new model();

	                    extract(array_map("test_input" , $_GET));
	                    $forgotTime=date("Y-m-d");
	                    $q=$d->select("users_entry_login","token='$t' AND user_entry_id = '$f' AND token_date='$forgotTime'");
	                    $data=mysqli_fetch_array($q);
	                    if ($data>0) {
	                    $user_name=$data['login_name'];
	                    $_SESSION['forgot_user_entry_id']=$f;
	                ?>
					    <form action="controller/loginController.php" id="resetPassword" method="post">
					    	<div class="form-group">
						  		<label for="otp" class="">OTP</label>
						   		<div class="position-relative has-icon-right">
							  		<input type="type" name="otp" required="" id="otp" class="form-control input-shadow" placeholder="OTP" autocomplete="off">
							  		<div class="form-control-position"><i class="fa fa-key"></i></div>
						   		</div>
						  	</div>
						  	<div class="form-group">
						  		<label for="password" class="">New Password</label>
						   		<div class="position-relative has-icon-right">
							  		<input type="password" name="password" required="" id="password" class="form-control input-shadow" placeholder="Password">
							  		<div class="form-control-position"><i class="fa fa-lock"></i></div>
						   		</div>
						  	</div>
						  	<div class="form-group">
						  		<label for="confirm_password" class="">Confirm New Password</label>
						   		<div class="position-relative has-icon-right">
							  		<input type="password" name="password2" required="" id="confirm_password" class="form-control input-shadow" placeholder="Confirm Password">
							 		 <div class="form-control-position"><i class="fa fa-lock"></i></div>
						   		</div>
						  	</div>
						  	<div id="PassMatch" class="form-group text-center">
						  		
						  	</div>
						  	<button type="submit" class="btn btn-primary submitBTN shadow-primary btn-block waves-effect waves-light mt-3">Set Password</button>
						  	<div >
						 		<br>
				            	<?php include 'common/alert.php'; ?>
				        	</div>
						</form>
					<?php } else{  ?>
	                	<div class="alert alert-danger alert-dismissible" role="alert">
						   <button type="button" class="close" data-dismiss="alert">×</button>
							<div class="alert-icon">
							 <i class="fa fa-times"></i>
							</div>
							<div class="alert-message">
							  <span><strong>Error!</strong> Invalid URL or Token Expired</span>
							</div>
						 </div>
            		<?php } ?>
				</div>
			</div>
			<div class="card-footer text-center py-3">
			    <p class="text-muted mb-0">Return to the <a href="index.php"> Sign In</a></p>
			</div>
		</div>
	</div>

<script src="../apAdmin/assets/js/jquery.min.js"></script>
<script src="../apAdmin/assets/js/popper.min.js"></script>
<script src="../apAdmin/assets/js/bootstrap.min.js"></script>
<script src="../apAdmin/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<!--Sweet Alerts -->
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
<?php include 'common/alert.php'; ?>
<script type="text/javascript">
	$().ready(function() {
		$("#resetPassword").validate({
			rules: {
				otp: {
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
				password: {
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
					maxlength: 12,
	                minlength: 5,
				},
				password2: {
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
					equalTo: "#password",
				},
			}
		});
	});
</script>


</body>

</html>
