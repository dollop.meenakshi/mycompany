<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-5 Days'));
    $endDate = date('Y-m-d');
  }
?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Last 15 Days
        </div>
        <div class="card-body" >
            <canvas id="last15DaysChart"></canvas>
        </div>
    </div>
    <div class="card">
          <div class="card-header">
            <form method="POST">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div id="daterange-picker">
                    <div class="input-daterange input-group">
                      <input <?php if(isset($_POST['start'])){ ?>
                      value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                      <?php } else{ ?> value="<?php echo date('Y-m-d', strtotime('-5 Days')); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                        <div class="input-group-prepend">
                         <span class="input-group-text">to</span>
                        </div>
                        <input <?php if(isset($_POST['start'])){ ?>
                        value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                      <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                      <div class="input-group-prepend">
                          <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </div>
    <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header text-uppercase">Day Chart</div>
            <div class="card-body">
              <canvas id="daywiseCounter"></canvas>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header text-uppercase">Time Chart</div>
            <div class="card-body">
              <canvas id="timewiseCounter"></canvas>
            </div>
          </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header text-uppercase">Month Chart</div>
        <div class="card-body">
          <canvas id="monthwiseCounter"></canvas>
        </div>
    </div>
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
    $m = date("m"); $de= date("d"); $y= date("Y");
    $dateArray = array();
    $dateArrayQuery = array();
    $dateFilter = array();
    $dateFilterArray = array();
    $countArrayQuery = array();
    $monthArrayQuery = array();
    $countDateQuery = array();
    for($i=0; $i<=14; $i++){
        $dateArray[] = date('d M', mktime(0,0,0,$m,($de-$i),$y)); 
        $dateArrayQuery[] = date('Y-m-d', mktime(0,0,0,$m,($de-$i),$y)); 
    }

    $dates = array();
    $current = strtotime($startDate);
    $date2 = strtotime($endDate);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
     $dateFilter[] = date('d M', $current);
     $dateFilterArray[] = date('Y-m-d', $current);
     $current = strtotime($stepVal, $current);
    }
    $dateArray = array_reverse($dateArray);
    $dateArrayQuery = array_reverse($dateArrayQuery);
    $dateString = "'" . implode("','", $dateArray). "'";
    $dateFilterString = "'" . implode("','", $dateFilter). "'";
    for ($i=0; $i < count($dateArrayQuery); $i++) { 
        $count = 0;
        $countQuery = $d->selectRow("entry_id,no_of_person","commercialentry","DATE(entrytime)='$dateArrayQuery[$i]' AND unit_id='$_SESSION[unit_id]' AND society_id='$_COOKIE[society_id]'");
        while ($dataQuery = mysqli_fetch_array($countQuery)) {
            $count += $dataQuery['no_of_person'];
        }
        array_push($countArrayQuery, $count);
    };
    $countString = implode(",",$countArrayQuery);

    for ($i=0; $i < count($dateFilterArray); $i++) { 
        $count = 0;
        $countQuery = $d->selectRow("entry_id,no_of_person","commercialentry","DATE(entrytime)='$dateFilterArray[$i]' AND unit_id='$_SESSION[unit_id]' AND society_id='$_COOKIE[society_id]'");
        while ($dataQuery = mysqli_fetch_array($countQuery)) {
            $count += $dataQuery['no_of_person'];
        }
        array_push($countDateQuery, $count);

    }
    $countDateString = implode(",",$countDateQuery);

    $counttimeQuery = $d->selectRow("entry_id,no_of_person,entrytime","commercialentry","DATE(entrytime) BETWEEN DATE('$startDate') AND DATE('$endDate') AND unit_id='$_SESSION[unit_id]' AND society_id='$_COOKIE[society_id]'");
    while ($timeQuery = mysqli_fetch_array($counttimeQuery)) {
        $time = date('H', strtotime($timeQuery['entrytime']));
        if ($time >= 8 AND $time < 12) {
            $countMorning += $timeQuery['no_of_person'];
        } else if ($time >= 12 AND $time < 18) {
            $countAfternoon += $timeQuery['no_of_person'];
        } else if ($time >= 18 AND $time < 22) {
            $countEvening += $timeQuery['no_of_person'];
        } else{
            $countNight += $timeQuery['no_of_person'];
        }
    }

    for ($i=1; $i < 13; $i++) { 
        $countMonth = 0;
        $monthQuery = $d->selectRow("entry_id,no_of_person","commercialentry","MONTH(entrytime)='$i' AND unit_id='$_SESSION[unit_id]' AND society_id='$_COOKIE[society_id]'");
        while ($monthdataQuery = mysqli_fetch_array($monthQuery)) {
            $countMonth += $monthdataQuery['no_of_person'];
        }
        array_push($monthArrayQuery, $countMonth);
    }
    $monthString = "'" . implode("','", $monthArrayQuery). "'";

?>
<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
    if ($('#last15DaysChart').length) {
       var ctx = document.getElementById('last15DaysChart').getContext('2d');
       
       var gradientStroke1 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStroke1.addColorStop(0, '#4facfe');
        gradientStroke1.addColorStop(1, '#00f2fe');
       
       var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php echo $dateString ?>],
                datasets: [{
                    label: 'Visitors',
                    data: [<?php echo $countString ?>],
                    backgroundColor: 'rgba(94, 114, 228, 0.3)',
                    borderColor: '1a4089',
                    borderWidth: 3
                }]
            }
        });
    }

    if ($('#timewiseCounter').length) {
        var ctx = document.getElementById("timewiseCounter").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Morning(8 AM to 12 PM)", "Afternoon(12 PM to 6 PM)", "Evening(6 PM to 10 PM)", "Night(10 PM to 8 AM)"],
                datasets: [{
                    backgroundColor: [
                        "1a4089",
                        "#2dce89",
                        "#11cdef",
                        "#ff2fa0",
                    ],
                    data: [<?php echo $countMorning ?>, <?php echo $countAfternoon ?>, <?php echo $countEvening ?>, <?php echo $countNight ?>]
                }]
            },
            options: {
                legend: {
                  position: 'bottom',
                  display: true,
                  labels: {
                    boxWidth:40
                  }
                }
            }
        });
    }

    if ($('#daywiseCounter').length) {
        var ctx = document.getElementById("daywiseCounter").getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php echo $dateFilterString ?>],
                datasets: [{
                    label: 'Visitors',
                    data: [<?php echo $countDateString ?>],
                    backgroundColor: 'rgba(228, 114, 94, 0.3)',
                    borderColor: '1a4089',
                    borderWidth: 3
                }]
            },
            options: {
                legend: {
                  position: 'bottom',
                  display: true,
                  labels: {
                    boxWidth:40
                  }
                }
            }
        });
    }

    if ($('#monthwiseCounter').length) {
       var ctx = document.getElementById('monthwiseCounter').getContext('2d');
       
       var gradientStroke1 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStroke1.addColorStop(0, '#4facfe');
        gradientStroke1.addColorStop(1, '#00f2fe');
       
       var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [{
                    label: 'Visitors',
                    data: [<?php echo $monthString ?>],
                    backgroundColor: 'rgba(94, 114, 228, 0.3)',
                    borderColor: '1a4089',
                    borderWidth: 3
                }]
            }
        });
    }
</script>
