<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
error_reporting(0);

 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Daily Visitor  Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          
           
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          <div class="col-lg-2">
            <!-- <?php  if (isset($_GET['block_id'])) { ?>
            <label  class="form-control-label"> </label>
              <a href="exportCsv.php?block_id=<?php echo $_GET['block_id'] ?>&from=<?php echo $_GET['from'] ?>&toDate=<?php echo $_GET['toDate']; ?>&report=Visitor" style="margin-top:30px;" title="Export to CSV" class="btn btn-warning" type="submit" name="getReport" class="form-control" value="G">Export <i class="fa fa-file-excel-o"></i></a>
            <?php } ?> -->

          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
               // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                  
                       $q=$d->select("users_master,visitors_master","users_master.user_id=visitors_master.user_id AND visitors_master.society_id='$_COOKIE[society_id]' AND visitors_master.block_id ='$_COOKIE[block_id]' AND visitors_master.visit_date BETWEEN '$from' AND '$toDate'");
                  

                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Employee</th>
                        <th>In Time</th>
                        <th>Out Time</th>
                        <th>Type</th>
                        <th>Vehicle Number </th>
                        <th>Visiting Reason</th>
                        <th>Status</th>
                        <th>Data Add Time</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo custom_echo($data['visitor_name'],15); ?></td>
                        <td><?php echo $data['country_code'] . " " . substr($data['visitor_mobile'], 0, 2) . '*****' . substr($data['visitor_mobile'], -3); ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php
                        if($data['visitor_status'] == '2' || $data['visitor_status'] == '3' || $data['visitor_status'] == '5')
                        {
                         echo date("d M Y h:i A", strtotime($data['visit_date']." ".$data['visit_time']));
                        }
                        ?>
                        </td>
                        <td><?php 
                          if ($data['exit_date']!="") {
                               echo date("d M Y h:i A", strtotime($data['exit_date']." ".$data['exit_time'])); 
                            }
                        ?></td>
                        <td>
                          <?php if ($data['visitor_type']==0) {
                          echo "Guest";
                         } elseif ($data['visitor_type']==1) {
                          if ($data['expected_type']==2) {
                            echo "Exp. Delivery";
                          } else if ($data['expected_type']==3) {
                            echo "Exp. Cab";
                          } else {
                             echo "Exp. Guest";
                          }
                         } elseif ($data['visitor_type']==2) {
                          echo "Delivery ";
                         } elseif ($data['visitor_type']==3) {
                          echo "Cab ";
                         }  
                         ?>
                        </td>
                        <td><?php echo $data['vehicle_no']; ?></td>
                        <td><?php echo $data['visiting_reason']; ?></td>
                        <td>
                         <?php if ($data['visitor_status']==0) {
                          echo "Pending";
                         } elseif ($data['visitor_status']==1) {
                          echo "Approved ";
                         } elseif ($data['visitor_status']==2) {
                          echo "Entered ";
                         } elseif ($data['visitor_status']==3) {
                          if ($data['exit_date']!="") {
                            echo "Exit ";
                          } else {
                            echo "Auto Exit";
                          }
                         } elseif ($data['visitor_status']==4) {
                          echo "Rejected ";
                         } elseif ($data['visitor_status']==5) {
                          echo "Deleted";
                         }  elseif ($data['visitor_status']==6) {
                          echo "Hold";
                         } 
                         ?>
                        </td>
                        <td><?php if($data['entry_time']!="") { echo date('Y-m-d h:i A', strtotime($data['entry_time'])); } ?></td>
                       
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
             
                <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<?php include "common/footer.php"; ?>
<!-- <script type="text/javascript">
  var date = new Date();
  $('#autoclose-datepickerFrom').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
  $('#autoclose-datepickerTo').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script> -->