<?php session_start(); if(isset($_SESSION['login_status'])){ header('location:welcome.php'); } ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Commercial Login | My Company</title>
	<link rel="icon" href="../img/fav.png" type="image/png">
	<link href="../apAdmin/assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="../apAdmin/assets/css/animate.css" rel="stylesheet" type="text/css"/>
	<link href="../apAdmin/assets/css/icons.css" rel="stylesheet" type="text/css"/>
	<link href="../apAdmin/assets/css/app-style9.css" rel="stylesheet"/>
</head>

<body class="bg-dark">
	<div id="wrapper">
		<div class="card card-authentication1 mx-auto my-5">
			<div class="card-body">
				<div class="card-content p-2">
					<div class="text-center">
						<img src="../img/logo.png" alt="My Company Logo" width="130">
					</div>
					<div class="card-title text-uppercase text-center pb-2 py-3">My Company - Reset Password</div>
				    <p class="pb-2">Please enter your registered Mobile Number. You will receive a link to create a new password via SMS.</p>
					<form id="forgotPassForm" action="controller/loginController.php" method="post">
				  		<div class="form-group">
				  			<label for="exampleInputEmailAddress" class="">Mobile Number</label>
				   			<div class="position-relative has-icon-right">
					  			<input type="text" name="forgot_mobile" autocomplete="off" required id="exampleInputEmailAddress" class="form-control input-shadow">
					  			<div class="form-control-position">
						  			<i class="fa fa-mobile"></i>
					  			</div>
				   			</div>
				  		</div>
				 
						<button type="submit" class="btn btn-primary shadow-primary btn-block waves-effect waves-light mt-3">Reset Password</button>
				 	</form>
				</div>
			</div>
			<div class="card-footer text-center py-3">
			    <p class="text-muted mb-0">Return to the <a href="index.php"> Sign In</a></p>
			</div>
		</div>
	</div>

<script src="../apAdmin/assets/js/jquery.min.js"></script>
<script src="../apAdmin/assets/js/popper.min.js"></script>
<script src="../apAdmin/assets/js/bootstrap.min.js"></script>
<script src="../apAdmin/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<!--Sweet Alerts -->
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
<script src="../apAdmin/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
<?php include 'common/alert.php'; ?>
<script>
	$(document).ready(function() {
		$("#forgotPassForm").validate({
			rules: {
				forgot_mobile:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
			}
		});
	});

</script>


</body>

</html>
