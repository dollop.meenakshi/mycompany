<?php include "common/header.php"; ?>
<!--End topbar header-->
<?php include "common/sidebar.php"; ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">View Users</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">View Users</li>
         </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Created</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 $i=1;
                 $sql =$d->select("users_entry_login","unit_id = '$_SESSION[unit_id]'","ORDER BY user_entry_id DESC");

                 while ($response=mysqli_fetch_array($sql)) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $response['login_name']; ?></td>
                    <td><?php echo $response['login_mobile']; ?></td>
                    <td><?php echo date('d-M-Y', strtotime($response['login_created_date'])); ?></td>
                  </tr>
                <?php } ?> 
              </tbody>
            </div>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!-- End Row-->
</div>
</div>
<?php include "common/footer.php"; ?>