<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $monthDayCount = date('t');
	$nextYear = date('Y', strtotime('+1 year'));
	$onePreviousYear = date('Y', strtotime('-1 year'));
	$twoPreviousYear = date('Y', strtotime('-2 year'));
  $year = (int)$_REQUEST['year'];
  $month = (int)$_REQUEST['month'];
  /* if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-365 Days'));
    $endDate = date('Y-m-d');
    $id = 'All';
  } */
?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Monthly Report</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Monthly Report</li>
            </ol>
        </div>
    </div>
    <form class="branchDeptFilter" action="" method="get">
        <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
            <select name="year" class="form-control">
              <option <?php if($year==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
              <option <?php if($year==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
              <option <?php if($year==$currentYear) { echo 'selected';}?> <?php if($year=='') { echo 'selected';}?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            </select>
          </div>
          <div class="col-md-3 form-group" >
            <select class="form-control single-select"  name="month" id="month">
              <option value="">-- Select --</option> 
              <?php
              $selected = "";
              for ($a = 1; $a <= 12; $a++) {
                $monthName = date('F', mktime(0, 0, 0, $a, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $a, date('Y'));
                if (isset($month)  && $month !="") {
                  if($month == $a)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                } else {
                  $selected = "";
                  if($a==date('n'))
                  { 
                    $selected = "selected";
                  }
                  else
                  {
                    $selected = "";
                  }
                }

              ?>
              <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $a; ?>"><?php echo $monthName; ?></option> 
            <?php }?>
            </select> 
          </div>
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
        </div>
    </form>
    <div class="row">
  <div class="col-lg-12">
    <div class="card">
      <!-- <div class="card-header">
        <form method="POST">
          <div class="row">
            <div class="col-sm-12 col-lg-6 form-group">
              <div id="daterange-picker">
                <div class="input-daterange input-group">
                  <input <?php if(isset($_POST['start'])){ ?>
                  value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                  <?php } else{ ?> value="<?php echo date('Y-m-d', strtotime('-90 Days')); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                    <div class="input-group-prepend">
                     <span class="input-group-text">to</span>
                    </div>
                    <input <?php if(isset($_POST['start'])){ ?>
                    value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                  <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                </div>
              </div>
            </div>
            <div class="col-sm-9 col-lg-5 form-group">
              <select class="form-control single-select" name="id">
                <option <?php if($id=='All'){echo "selected";} ?> value="All">All</option>
                <?php 
                    $q = $d->select("users_master","unit_id='$_SESSION[unit_id]' AND user_status=1");
                    while ($userdata = mysqli_fetch_array($q)) {
                ?>
                    <option <?php if($id==$userdata['user_id']){echo "selected";} ?> value="<?php echo $userdata['user_id'] ?>"><?php echo $userdata['user_full_name'] ?></option>
                <?php  }?>
                <option <?php if($id=='0'){echo "selected";} ?> value="0">Other</option>
              </select>
            </div>
            <div class="col-sm-3 col-lg-1 form-group">
              <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-reports btn-success"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </form>
      </div> -->
      <div class="card-body">
        <canvas id="daywiseCounter"></canvas>
      </div>
    </div>
    </div>
    </div>
   
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
  $dateArray = array();
  $countDateArray = array();

  if(isset($year) && $year >0 && isset($month) && $month >0){
    $y= $year;
    $m= $month;
    $day= cal_days_in_month(CAL_GREGORIAN,$m,$y);
  }else{
    $y= $currentYear;
    $m= $currentMonth;
    $day= $monthDayCount;
  }
/* print_r("monthDayCount  ");
print_r($d); */
  for ($i=1; $i <= $day; $i++) { 
      $count = 0;
     
      $countQuery = $d->selectRow("visitor_id,number_of_visitor","visitors_master","DAY(visit_date)='$i' AND MONTH(visit_date)='$m' AND YEAR(visit_date)='$y' AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");
        
      while ($dataQuery = mysqli_fetch_array($countQuery)) {
          $count += $dataQuery['number_of_visitor'];
      }
      array_push($dateArray, $i);
      array_push($countDateArray, $count);

  }
  $countDateString = implode("','",$countDateArray);
  $dateString = implode("','",$dateArray);
?>

<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">

  if ($('#daywiseCounter').length) {
   var ctx = document.getElementById('daywiseCounter').getContext('2d');       
   var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['<?php echo $dateString ?>'],
      datasets: [{
        label: 'Visitors',
        data: ['<?php echo $countDateString ?>'],
        backgroundColor: '#5E72E4',
        borderColor: '1a4089',
        borderWidth: 3
      }]
    },
    options: {
      legend: {
        position: 'bottom',
        display: true,
        labels: {
          boxWidth:40
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
      }
    }
  });
 }
</script>