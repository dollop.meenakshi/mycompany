<?php include "common/header.php"; ?>
<!--End topbar header-->
<?php include "common/sidebar.php"; ?>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">My Profile</h4>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-4">
        <div class="card profile-card-2">
          <div class="card-img-block">
            <img class="img-fluid" src="../apAdmin/img/31.jpg" alt="Card image cap">
          </div>
          <div class="card-body pt-5">
            <img id="blah"  onerror="this.src='../apAdmin/img/user.png'" src="img/profile/<?php echo $_SESSION['login_profile']?>"  width="75" height="75"   src="#" alt="your image" class='profile' />
            <h5 class="card-title"><?php echo $_SESSION['login_name']; ?></h5>

          </div>

          <div class="card-body border-top">
            <div class="media align-items-center">
              <div>
                <i class="fa fa-phone"></i>
              </div>
              <div class="media-body text-left ml-3">
                <div class="progress-wrapper">
                  <?php echo $_SESSION['login_mobile']; ?>
                </div>                   
              </div>
            </div>


          </div>
        </div>

      </div>

      <div class="col-lg-8">
        <div class="card">
          <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
              <li class="nav-item">
                <a href="javascript:void();" data-target="#edit" data-toggle="pill" class="nav-link active"><i class="icon-note"></i> <span class="hidden-xs">Edit Profile</span></a>
              </li>
              <li class="nav-item">
                <a href="javascript:void();" data-target="#messages" data-toggle="pill" class="nav-link"><i class="fa fa-key"></i> <span class="hidden-xs">Change Password</span></a>
              </li>

            </ul>
            <div class="tab-content p-3">

              <div class="tab-pane" id="messages">

                <div class="">
                  <form id="updatePasswordValidation" action="controller/profileController.php" method="post">
                    <div class="form-group row">
                      <label class="col-lg-3 col-form-label form-control-label">Old Password <span class="required">*</span></label>
                      <div class="col-lg-9">
                        <input class="form-control" required="" name="old_password"  id="old_password" type="password" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-form-label form-control-label">Password <span class="required">*</span></label>
                      <div class="col-lg-9">
                        <input class="form-control" required="" type="password" name="password" id="password" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-form-label form-control-label">Confirm password <span class="required">*</span></label>
                      <div class="col-lg-9">
                        <input class="form-control" required="" name="password2" id="password2" type="password" value="">
                        <input type="hidden" name="passwordChange">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">Change Password</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="tab-pane active" id="edit">
                <form id="updateValidation" class="validateForm" action="controller/profileController.php" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Full Name <span class="required">*</span></label>
                    <div class="col-lg-9">
                      <input required="" class="form-control" name="user_name" id="user_name" type="text" value="<?php echo $_SESSION['login_name']; ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Mobile <span class="required">*</span></label>
                    <div class="col-lg-9">

                      <input class="form-control" disabled="" type="text" value="<?php echo $_SESSION['login_mobile']; ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Profile Picture</label>
                    <div class="col-lg-9">
                      <input class="form-control-file border photoOnly" id="imgInp" accept="image/*" name="profile_image" type="file">
                      <input type="hidden" name="old_profile_image" value="<?php echo $_SESSION['login_profile'] ?>">

                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"></label>
                    <div class="col-lg-9">
                      <button class="btn btn-primary" name="profileUpdate">Update Profile</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  jQuery.validator.addMethod("image", function (value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, jQuery.validator.format("Please add a valid image file."));

    $.validator.addMethod('filesize', function (value, element, arg) {
        var size =2000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }, $.validator.format("file size must be less than or equal to 2MB."));
    $("#updateValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_name: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            profile_image:{
              filesize: true,
              image: true,
            }
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
        }
         
    });
    $("#updatePasswordValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            old_password: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            password:{
              maxlength: 20,
              minlength: 6,
              required: true,
            },
            password2:{
              required: true,
              equalTo:'#password'
            }
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
        }
         
    });
</script>

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInp").change(function() {
    readURL(this);
  });
</script>