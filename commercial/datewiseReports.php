<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-30 Days'));
    $endDate = date('Y-m-d');
  }
?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Date Wise Report</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Date Wise Report</li>
            </ol>
        </div>
    </div>

    <div class="card">
      <div class="card-header">
        <form method="POST">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div id="daterange-picker">
                <div class="input-daterange input-group">
                  <input <?php if(isset($_POST['start'])){ ?>
                  value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                  <?php } else{ ?> value="<?php echo date('Y-m-d', strtotime('-30 Days')); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                    <div class="input-group-prepend">
                     <span class="input-group-text">to</span>
                    </div>
                    <input <?php if(isset($_POST['start'])){ ?>
                    value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                  <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                  <div class="input-group-prepend">
                      <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="card-body">
        <canvas id="daywiseCounter"></canvas>
      </div>
    </div>
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
    $m = date("m"); $de= date("d"); $y= date("Y");
    $dateArray = array();
    $dateArrayQuery = array();
    $dateFilter = array();
    $dateFilterArray = array();
    $countArrayQuery = array();
    $monthArrayQuery = array();
    $countDateQuery = array();
    for($i=0; $i<=31; $i++){
        $dateArray[] = date('d M', mktime(0,0,0,$m,($de-$i),$y)); 
        $dateArrayQuery[] = date('Y-m-d', mktime(0,0,0,$m,($de-$i),$y)); 
    }

    $dates = array();
    $current = strtotime($startDate);
    $date2 = strtotime($endDate);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
     $dateFilter[] = date('d M', $current);
     $dateFilterArray[] = date('Y-m-d', $current);
     $current = strtotime($stepVal, $current);
    }
    $dateArray = array_reverse($dateArray);
    $dateArrayQuery = array_reverse($dateArrayQuery);
    $dateString = "'" . implode("','", $dateArray). "'";
    $dateFilterString = "'" . implode("','", $dateFilter). "'";
    for ($i=0; $i < count($dateArrayQuery); $i++) { 
        $count = 0;
        $countQuery = $d->selectRow("visitor_id,number_of_visitor","visitors_master","DATE(visit_date)='$dateArrayQuery[$i]' AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");
        while ($dataQuery = mysqli_fetch_array($countQuery)) {
            $count += 1;
        }
        array_push($countArrayQuery, $count);
    };
    $countString = implode(",",$countArrayQuery);

    for ($i=0; $i < count($dateFilterArray); $i++) { 
        $count = 0;
        $countQuery = $d->selectRow("visitor_id,number_of_visitor","visitors_master","DATE(visit_date)='$dateFilterArray[$i]' AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");
        while ($dataQuery = mysqli_fetch_array($countQuery)) {
            $count += $dataQuery['number_of_visitor'];
        }
        array_push($countDateQuery, $count);

    }
    $countDateString = implode(",",$countDateQuery);

?>
<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">

  if ($('#daywiseCounter').length) {
    var ctx = document.getElementById("daywiseCounter").getContext('2d');

    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [<?php echo $dateFilterString ?>],
        datasets: [{
          label: 'Visitors',
          data: [<?php echo $countDateString ?>],
          backgroundColor: '#5E72E4',
          borderColor: '1a4089',
          borderWidth: 3
        }]
      },
      options: {
        legend: {
          position: 'bottom',
          display: true,
          labels: {
            boxWidth:40
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }],
        }
      }
    });
  }

</script>
