<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php
  error_reporting(0);
  if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-15 Days'));
    $endDate = date('Y-m-d');
  }
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Last 15 Days
        </div>
        <div class="card-body" >
            <canvas id="last15DaysChart"></canvas>
        </div>
    </div>
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
    $m = date("m"); $de= date("d"); $y= date("Y");
    $dateArray = array();
    $dateArrayQuery = array();
    $dateFilter = array();
    $dateFilterArray = array();
    $countArrayQuery = array();
    $monthArrayQuery = array();
    $countDateQuery = array();
    for($i=0; $i<=14; $i++){
        $dateArray[] = date('d M', mktime(0,0,0,$m,($de-$i),$y)); 
        $dateArrayQuery[] = date('Y-m-d', mktime(0,0,0,$m,($de-$i),$y)); 
    }

    $dates = array();
    $current = strtotime($startDate);
    $date2 = strtotime($endDate);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
     $dateFilter[] = date('d M', $current);
     $dateFilterArray[] = date('Y-m-d', $current);
     $current = strtotime($stepVal, $current);
    }
    $dateArray = array_reverse($dateArray);
    $dateArrayQuery = array_reverse($dateArrayQuery);
    $dateString = "'" . implode("','", $dateArray). "'";
    $dateFilterString = "'" . implode("','", $dateFilter). "'";
    for ($i=0; $i < count($dateArrayQuery); $i++) { 
        $count = 0;
        //$countQuery = $d->selectRow("entry_id,no_of_person","commercialentry","DATE(entrytime)='$dateArrayQuery[$i]' AND unit_id='$_SESSION[unit_id]' AND society_id='$_COOKIE[society_id]'");
        $countQuery = $d->selectRow("visitor_id,number_of_visitor","visitors_master","DATE(visit_date)='$dateArrayQuery[$i]' AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");
        while ($dataQuery = mysqli_fetch_array($countQuery)) {
            $count += $dataQuery['number_of_visitor'];
        }
        array_push($countArrayQuery, $count);
    };
    $countString = implode(",",$countArrayQuery);


?>
<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
    if ($('#last15DaysChart').length) {
       var ctx = document.getElementById('last15DaysChart').getContext('2d');
       
       var gradientStroke1 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStroke1.addColorStop(0, '#4facfe');
        gradientStroke1.addColorStop(1, '#00f2fe');
       
       var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?php echo $dateString ?>],
                datasets: [{
                    label: 'Visitors',
                    data: [<?php echo $countString ?>],
                    backgroundColor: '#5E72E4',
                    borderColor: '1a4089',
                    borderWidth: 3
                }]
            }
        });
    }
</script>
