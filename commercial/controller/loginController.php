<?php
include '../common/objectController.php';
extract(array_map("test_input" , $_POST));

if(isset($_COOKIE['user_id']))
{
	header("location:../welcome.php");
}
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
	if(isset($_POST["userMobile"]) && $_POST['SendOPT'] =="yes")
	{
		extract(array_map("test_input" , $_POST));
		$userMobile=mysqli_real_escape_string($con, $userMobile);
		$country_code=mysqli_real_escape_string($con, $country_code);
		$q=$d->select("users_master","user_mobile='$userMobile' AND country_code='$country_code'");
	 	$users_master_data = mysqli_fetch_array($q);
		if ( mysqli_num_rows($q) == 1 && $users_master_data['allow_front_desk_portal_login']==1 && $users_master_data['delete_status']==0 && $users_master_data['active_status']==0)
		{
		 	extract($users_master_data);
		 	$digits = 6;
         	$otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
         	$m->set_data('otp', $otp);
            $a = array(
                'otp' => $m->get_data('otp'),
            );
	        $sq=$d->selectRow("society_name,city_name","society_master","");
	        $societyData=mysqli_fetch_array($sq);
	        $society_name=$societyData['society_name'];
	        $city_name=$societyData['city_name'];
	       	$society_id=$societyData['society_id'];
	        $result = $d->update("users_master",$a,"user_mobile='$userMobile' ");
	        if($result)
	        {
	        	$society_name = html_entity_decode($society_name);
	        	$smsObj->send_otp_admin($otp,$userMobile,$society_name,$country_code);
            	$d->add_sms_log($userMobile,"Admin OTP SMS",$society_id,$country_code,1);
				echo "1";
	        }
	        else
	        {
	        	echo "2";
	        }
        }
        else if ( mysqli_num_rows($q) == 1 && $users_master['active_status']==1)
        {
        	echo "5";
        }
        else
        {
        	$user_email=mysqli_real_escape_string($con, $userMobile);
			$q=$d->select("users_master,society_master","users_master.society_id = society_master.society_id AND user_email='$user_email' AND active_status=0 AND delete_status=0 AND allow_front_desk_portal_login=1");
			if ( mysqli_num_rows($q) == 1)
			{
				$data = mysqli_fetch_array($q);
				extract($data);
				$digits = 6;
		        $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
		        $m->set_data('otp', $otp);
	            $a = array(
	                'otp' => $m->get_data('otp'),
	            );
		        $result = $d->update("users_master",$a,"user_email='$user_email'");
		        if($result)
		        {
		        	$to = $user_email;
					$subject = "My Company Admin Panel Login OTP";
					$society_name = html_entity_decode($society_name);
					$msg= " $otp";
					$app = "Commercial Panel";
					include '../../apAdmin/mail/adminPanelLoginOTP.php';
					include '../../apAdmin/mail.php';
					echo "1";
		        }
		        else
		        {
		        	echo "2";
		        }
			}
			else
			{
				echo "0";
			}
        }

	}
	//IS_3534

	if(isset($_POST["mobile"])) {

		extract(array_map("test_input" , $_POST));
		$mobile=mysqli_real_escape_string($con, $mobile);
		$otp_web=(int)$otp_web;

		if (strlen($otp_web)==6) {
			$apeendOTPQueryMainAdmin = " AND otp_web='$otp_web'";
			$apeendOTPQuery = "  AND users_master.otp='$otp_web'";
		}

		$q=$d->select("users_master,society_master","(society_master.society_id=users_master.society_id AND users_master.otp='$otp_web') AND (users_master.user_mobile='$mobile' OR users_master.user_email='$mobile')");

		$data = mysqli_fetch_array($q); 
		if ($data > 0 && mysqli_num_rows($q) == 1 && $data['delete_status']==0) {
			
			$country_id  = $data['country_id'];
		    $society_id  = $data['society_id'];
		    
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			          "getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id");

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'key: bmsapikey'
			));

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$server_output=json_decode($server_output,true);

			$arrayCount= count($server_output['language_key']);
			$language_id = $server_output['language_id'];
			$myFile = "../../img/$language_id.xml";
			
			$fh = fopen($myFile, 'w') or die("can't open file");
			$rss_txt = "";
			$rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
			$rss_txt .= "<rss version='2.0'>";
			    
			        $rss_txt .= '<string>';
			    for ($i1=0; $i1 < $arrayCount ; $i1++) { 
			    	$key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

			    	$keyName  = $server_output['language_key'][$i1]['key_name'];
			    	

			        $rss_txt .= "<$keyName>$key_value</$keyName>";
			    }
			        $rss_txt .= '</string>';
			$rss_txt .= '</rss>';

			fwrite($fh, $rss_txt);
			fclose($fh);

			
				
			setcookie('user_name', $data['user_full_name'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_name', $data['society_name'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('user_id', $data['user_id'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('block_id', $data['block_id'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_id', $data['society_id'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('society_type', $data['society_type'], time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('language_id', $language_id, time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			setcookie('country_id', $country_id, time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
			
			setcookie("lngUpdated", "lngUpdated", time() + (86400 * 5), "/$cookieUrl"); // 86400 = 5 day
			
			$_SESSION['msg']= "Welcome $_COOKIE[user_full_name]";
			
			
			

			// Session Data insert
			// $admin_id=$_COOKIE['bms_admin_id'];
			// $ip_address=$_SERVER['REMOTE_ADDR']; # Save The IP Address
			// $browser=$_SERVER['HTTP_USER_AGENT']; # Save The User Agent
			// $loginTime=date("d M,Y h:i:sa");//Login Time
			// $m->set_data('admin_id',$admin_id);
			// $m->set_data('name',$_COOKIE['admin_name']);
			// $m->set_data('role_name','Admin');
			// $m->set_data('ip_address',$ip_address);
			// $m->set_data('browser',$browser);
			// $m->set_data('loginTime',$loginTime);
			// $a1= array ('admin_id'=> $m->get_data('admin_id'),
			// 	'name'=> $m->get_data('name'),
			// 	'role_name'=> $m->get_data('role_name'),
			// 	'ip_address'=> $m->get_data('ip_address'),
			// 	'browser'=> $m->get_data('browser'),
			// 	'loginTime'=> $m->get_data('loginTime'),
			// );
			// $insert=$d->insert('session_log',$a1); 
        // Redirqt to homepage
        // echo "Success";
		$_SESSION['msg']= "Welcome ".$data['user_full_name'];
		header("location:../welcome.php");
	}else if ($data > 0 && mysqli_num_rows($q) == 1 && $data['delete_status']==1) {
		$_SESSION['msg1']= "Your account is deactivated !";
		header("location:../");
	} 
	else {
		$_SESSION['msg1']= "Wrong Credentials Details";
		header("location:../");
	}
}





} else  {
	header("location:../forgot.php");
}
?>