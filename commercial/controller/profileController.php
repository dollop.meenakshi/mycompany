<?php
include '../common/objectController.php';
if($_POST){
	extract($_POST);
	if(isset($profileUpdate)){
		$extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
		$uploadedFile = $_FILES['profile_image']['tmp_name'];
		$ext = pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION);
		if(!empty($_FILES['profile_image']['name'])) {
			if(in_array($ext,$extension)) {
				$sourceProperties = getimagesize($uploadedFile);
				$newFileName = rand();
				$dirPath = "../img/profile/";
				$imageType = $sourceProperties[2];
				$imageHeight = $sourceProperties[1];
				$imageWidth = $sourceProperties[0];
				if ($imageWidth>400) {
					$newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
					$newImageWidth = $imageWidth * $newWidthPercentage /100;
					$newImageHeight = $imageHeight * $newWidthPercentage /100;
				} else {
					$newImageWidth = $imageWidth;
					$newImageHeight = $imageHeight;
				}



				switch ($imageType) {

				case IMAGETYPE_PNG:
				$imageSrc = imagecreatefrompng($uploadedFile); 
				$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
				break;           

				case IMAGETYPE_JPEG:
				$imageSrc = imagecreatefromjpeg($uploadedFile); 
				$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
				break;

				case IMAGETYPE_GIF:
				$imageSrc = imagecreatefromgif($uploadedFile); 
				$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
				break;

				default:
				$_SESSION['msg1']="Invalid Image";
				header("Location: ../profile.php");
				exit;
				break;
				}
				$profile_image= $newFileName."_user.".$ext;
			} else {
				$_SESSION['msg1']="Invalid Photo";
				header("location:../profile.php");
				exit();
			}
		} else{
			$profile_image = $old_profile_image;
		}
		$m->set_data("login_name",$user_name);
		$m->set_data("login_profile",$profile_image);

		$a['login_name'] = $m->get_data('login_name');
		$a['login_profile'] = $m->get_data('login_profile');

		$q_temp=$d->update("users_entry_login",$a,"user_entry_id='$_SESSION[login_entry_id]'");

		$_SESSION['login_name']=$user_name;
		$_SESSION['login_profile']=$profile_image;

		if($q_temp>0){
			$_SESSION['msg']="Profile successfully updated.";
			header("location:../profile.php");
		}
		else{
			$_SESSION['msg1']="Something Wrong";
			header("location:../profile.php");
		}
	}else if(isset($passwordChange)){
		$userDetails = mysqli_fetch_array($d->select("users_entry_login","user_entry_id='$_SESSION[login_entry_id]'"));
		if($userDetails['login_password']==$old_password){
			if($password==$password2){
				if($password!=$old_password){
					if($d->update("users_entry_login",array('login_password'=>$password),"user_entry_id='$_SESSION[login_entry_id]'")){
						$_SESSION['msg']= "Password Changed Successfully";
						header('location:../profile.php');
					}else{
						$_SESSION['msg1']= "Failed To Update Password";
						header('location:../profile.php');
					}
				}else{
					$_SESSION['msg1']= "Please Use New Password Different From Old Password";
					header('location:../profile.php');
				}
			}else{
				$_SESSION['msg1']= "Password Does Not Match";
				header('location:../profile.php');
			}
		}else{
			$_SESSION['msg1']= "Old Password is Wrong";
			header('location:../profile.php');
		}
	}else{
		$_SESSION['msg1']= "Invalid Action";
		header('location:..');
	}
}else{
	$_SESSION['msg1']= "Invalid Action";
	header('location:..');
} ?>