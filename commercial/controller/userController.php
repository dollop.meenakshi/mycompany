<?php 
include '../common/objectController.php';
extract($_POST);
if (isset($addUser)) {
	$extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
	$uploadedFile = $_FILES['profile']['tmp_name'];
	$ext = pathinfo($_FILES['profile']['name'], PATHINFO_EXTENSION);
	if(!empty($_FILES['profile']['name'])) {
		if(in_array($ext,$extension)) {
			$sourceProperties = getimagesize($uploadedFile);
			$newFileName = rand();
			$dirPath = "../img/profile/";
			$imageType = $sourceProperties[2];
			$imageHeight = $sourceProperties[1];
			$imageWidth = $sourceProperties[0];
			if ($imageWidth>400) {
				$newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
				$newImageWidth = $imageWidth * $newWidthPercentage /100;
				$newImageHeight = $imageHeight * $newWidthPercentage /100;
			} else {
				$newImageWidth = $imageWidth;
				$newImageHeight = $imageHeight;
			}



			switch ($imageType) {

			case IMAGETYPE_PNG:
			$imageSrc = imagecreatefrompng($uploadedFile); 
			$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
			break;           

			case IMAGETYPE_JPEG:
			$imageSrc = imagecreatefromjpeg($uploadedFile); 
			$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
			break;

			case IMAGETYPE_GIF:
			$imageSrc = imagecreatefromgif($uploadedFile); 
			$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
			break;

			default:
			$_SESSION['msg1']="Invalid Image";
			header("Location: ../profile.php");
			exit;
			break;
			}
			$profile= $newFileName."_user.".$ext;
		} else {
			$_SESSION['msg1']="Invalid Photo";
			header("location:../profile.php");
			exit();
		}
	}

	$m->set_data("name",$name);
	$m->set_data("unit_id",$unit_id);
	$m->set_data("society_id",$society_id);
	$m->set_data("phone",$phone);
	$m->set_data("password",$password);
	$m->set_data("login_created_date",date('Y-m-d H:i:s'));
	$m->set_data("profile",$profile);

	$entry = array('login_name' => $m->get_data("name"),
				'unit_id' => $m->get_data("unit_id"),
				'society_id' => $m->get_data("society_id"),
				'login_mobile' => $m->get_data("phone"),
				'login_password' => $m->get_data("password"),
				'login_created_date' => $m->get_data("login_created_date"),
				'login_profile' => $m->get_data("profile"),
			);
	$query=$d->insert("users_entry_login",$entry);
	if ($query === true) {
		$_SESSION['msg']= "Successfully Added";
		header("location:../viewUsers.php");
	}else{
		$_SESSION['msg1']= "Something Wents Wrong";
		header("location:../viewUsers.php");
	}
}else{
	$_SESSION['msg1']= "Invalid Action";
	header('location:../viewUsers.php');
}
?>