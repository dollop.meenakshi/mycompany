<?php 
include '../common/objectController.php';
extract($_POST);
if(isset($_POST) && !empty($_POST) )//it can be $_GET doesn't matter
{
	if(isset($_POST['action']) && $_POST['action']=="getEmployeeByFloorId"){
		$mainData = array();
        if ($floor_id != '') {
            $q = $d->select("users_master", "floor_id='$floor_id' AND delete_status=0 AND user_status=1 AND active_status=0");
            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["users"] = $mainData;
                $response["message"] = "users";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "users";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
	}

	if(isset($_POST['action']) && $_POST['action']=="getVisitorSubType"){
		$mainData = array();
        if ($visitor_main_type_id != '') {
            $q = $d->select("visitorSubType", "visitor_main_type_id='$visitor_main_type_id' AND active_status=0");
            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["visitor_sub_type"] = $mainData;
                $response["message"] = "Visitor Sub Type";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Visitor Sub Type";
                $response["visitor_sub_type"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
	}

	if (isset($addVisitor)) {
		
		mysqli_autocommit($con,FALSE);

		$result = explode('~', $_POST['user_id']);
		$user_id = $result[0];
		$unit_id = $result[1];
		
		$m->set_data("society_id",$society_id);
		$m->set_data("visitor_name",$visitor_name);
		$m->set_data("visitor_mobile",$visitor_mobile);
		$m->set_data("number_of_visitor",$number_of_visitor);
		$m->set_data("floor_id",$floor_id);
		$m->set_data("block_id",$block_id);
		$m->set_data("unit_id",$unit_id);
		$m->set_data("user_id",$user_id);
		$m->set_data("emp_id",$_COOKIE['user_id']);
		$m->set_data("entry_by",1);
		$m->set_data("visitor_type",$visitor_type);
		$m->set_data("visit_from",$visit_from);
		$m->set_data("visit_date",date('Y-m-d'));
		$m->set_data("visit_time",date('H:i:s'));
		$m->set_data("visitor_status",2);
		$m->set_data("visiting_reason",$visiting_reason);
		$m->set_data("visitor_sub_type_id",$visitor_sub_type_id);
		$m->set_data("entry_time",date('Y-m-d H:i:s'));

		$a = array('society_id' => $m->get_data("society_id"),
					'visitor_name' => $m->get_data("visitor_name"),
					'visitor_mobile' => $m->get_data("visitor_mobile"),
					'number_of_visitor' => $m->get_data("number_of_visitor"),
					'floor_id' => $m->get_data("floor_id"),
					'block_id' => $m->get_data("block_id"),
					'unit_id' => $m->get_data("unit_id"),
					'user_id' => $m->get_data("user_id"),
					'emp_id' => $m->get_data("emp_id"),
					'entry_by' => $m->get_data("entry_by"),
					'visitor_type' => $m->get_data("visitor_type"),
					'visit_from' => $m->get_data("visit_from"),
					'visit_date' => $m->get_data("visit_date"),
					'visit_time' => $m->get_data("visit_time"),
					'visitor_status' => $m->get_data("visitor_status"),
					'visiting_reason' => $m->get_data("visiting_reason"),
					'visitor_sub_type_id' => $m->get_data("visitor_sub_type_id"),
					'entry_time' => $m->get_data("entry_time"),
				);

		$q=$d->insert("visitors_master",$a);
		$visitor_id = $con->insert_id;
		$a2 = array('parent_visitor_id' => $visitor_id);
		$q1 = $d->update("visitors_master", $a2, "visitor_id='$visitor_id'");

		if ($q && $q1) {

			$user = $d->selectRow('*',"users_master", "user_id='$user_id'");
        	$userData = mysqli_fetch_assoc($user); 

			$title = "New Visitor";
            $description = "$visitor_name entered at ".date('d F Y h:i A');
            $menuClick = "";
            $image = "";
            $activity = "0";
            $user_token = $userData['user_token'];
            $device = $userData['device'];
            if ($device=='android') {
                $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            } else {
                $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
            }

            $d->insertUserNotification($society_id,$title,$description,"leave_tracker","leave_tracker.png","user_id = '$user_id'");

			$d->insert_log("", "$society_id", "$_COOKIE[user_id]", "$created_by", "Visitor Detail Successfully Added");
			mysqli_commit($con);
			$_SESSION['msg']= "Successfully Added";
			header("location:../viewVisitors.php");
		}else{
			mysqli_rollback($con);
			$_SESSION['msg1']= "Something Wrong";
			header("location:../commercialService.php");
		}
	}

	if (isset($visitorExit)) {
		
		mysqli_autocommit($con,FALSE);

		$m->set_data("exit_date",$exit_date);
		$m->set_data("exit_time",$exit_time);
		$m->set_data("visitor_status",3);

		$a = array('exit_date' => $m->get_data("exit_date"),
					'exit_time' => $m->get_data("exit_time"),
					'visitor_status' => $m->get_data("visitor_status"),
				);

		$q1 = $d->update("visitors_master", $a, "visitor_id='$visitor_id'");

		if ($q1) {
			$d->insert_log("", "$society_id", "$_COOKIE[user_id]", "$created_by", "Visitor Exit");
			mysqli_commit($con);
			$_SESSION['msg']= "Status Change Successfully";
			header("location:../viewVisitors.php");
		}else{
			mysqli_rollback($con);
			$_SESSION['msg1']= "Something Wrong";
			header("location:../viewVisitors.php");
		}
	}

}else{
	header('location:../login.php');
}
?>