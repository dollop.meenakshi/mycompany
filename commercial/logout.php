<?php 
include_once '../apAdmin/lib/dao.php';
include '../apAdmin/lib/model.php';
$d = new dao();
$m = new model();
$con=$d->dbCon();
$base_url=$m->base_url();
$urlArya = explode("/", $base_url);
$cookieUrl = $urlArya[3];
session_start();
session_destroy();
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
        setcookie($name, '', time()-1000, "/$cookieUrl");
    }
}

header("location:./index.php");
 ?>