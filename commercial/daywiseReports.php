<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-d', strtotime('-28 Days'));
    $endDate = date('Y-m-d');
  }
?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Weekly Report</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Weekly Report</li>
            </ol>
        </div>
    </div>

    <div class="card">
      <div class="card-header">
        <form method="POST">
          <div class="row">
            <div class="col-sm-12 col-lg-6 form-group">
              <div id="daterange-picker">
                <div class="input-daterange input-group">
                  <input <?php if(isset($_POST['start'])){ ?>
                  value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                  <?php } else{ ?> value="<?php echo date('Y-m-d', strtotime('-28 Days')); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                    <div class="input-group-prepend">
                     <span class="input-group-text">to</span>
                    </div>
                    <input <?php if(isset($_POST['start'])){ ?>
                    value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                  <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-lg-1 form-group">
              <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-reports btn-success"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </form>
      </div>
      <div class="card-body">
        <canvas id="daywiseReport"></canvas>
      </div>
    </div>
    
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var date = new Date();
  $('#daterange-picker .input-daterange').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  });
</script>
<?php 
    
    $counttimeQuery = $d->selectRow("visitor_id,number_of_visitor,entry_time","visitors_master","DATE(visit_date) BETWEEN DATE('$startDate') AND DATE('$endDate') AND society_id='$_COOKIE[society_id]' AND block_id='$_COOKIE[block_id]'");

    while ($timeQuery = mysqli_fetch_array($counttimeQuery)) {
        $time = date('N', strtotime($timeQuery['entry_time']));
        if ($time == 0) {
            $sun += $timeQuery['number_of_visitor'];
        } else if ($time == 1) {
            $mon += $timeQuery['number_of_visitor'];
        } else if ($time == 2) {
            $tues += $timeQuery['number_of_visitor'];
        } else if ($time == 3) {
            $wed += $timeQuery['number_of_visitor'];
        } else if ($time == 4) {
            $thurs += $timeQuery['number_of_visitor'];
        } else if ($time == 5) {
            $fri += $timeQuery['number_of_visitor'];
        } else if ($time == 6) {
            $sat += $timeQuery['number_of_visitor'];
        } else{

        }
    }
    if ($sun=='') {
        $sun = 0;
    }
    if ($mon=='') {
        $mon = 0;
    }
    if ($tues=='') {
        $tues = 0;
    }
    if ($wed=='') {
        $wed = 0;
    }
    if ($thurs=='') {
        $thurs = 0;
    }
    if ($fri=='') {
        $fri = 0;
    }
    if ($sat=='') {
        $sat = 0;
    }


?>

<script src="../apAdmin/assets/plugins/Chart.js/Chart.min.js"></script>
<script type="text/javascript">

  if ($('#daywiseReport').length) {
   var ctx = document.getElementById('daywiseReport').getContext('2d');       
   var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
      datasets: [{
        label: 'Visitors',
        data: ['<?php echo $sun ?>','<?php echo $mon ?>','<?php echo $tues ?>','<?php echo $wed ?>','<?php echo $thurs ?>','<?php echo $fri ?>','<?php echo $sat ?>'],
        backgroundColor: '#5E72E4',
        borderColor: '1a4089',
        borderWidth: 3
      }]
    },
    options: {
      legend: {
        position: 'bottom',
        display: true,
        labels: {
          boxWidth:40
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
      }
    }
  });
 }
</script>