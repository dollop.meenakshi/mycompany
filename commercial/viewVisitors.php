<?php include "common/header.php"; ?>
<!--End topbar header-->
<?php include "common/sidebar.php"; ?>
<?php 
  error_reporting(0);
  if (isset($_POST['start'])) {
    extract(array_map("test_input" , $_POST));
    $startDate = date('Y-m-d', strtotime($start));
    $endDate = date('Y-m-d', strtotime($end));
  }else{
    $startDate = date('Y-m-01');
    $endDate = date('Y-m-d');
  }
?>
<style type="text/css">
  .tableWidth{
    white-space:normal !important;
    max-width: 250px !important;
  }
</style>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">View Visitors</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">View Visitors</li>
         </ol>
      </div>
      <div class="col-sm-3">
        <a href="commercialService.php" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add New</a>
      </div>
    </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>
     </div>
    </form>


    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!--<div class="card-header">
            <form method="POST">
              <div class="row">
                <div class="col-sm-12">
                  <div id="daterange-picker">
                    <div class="input-daterange input-group">
                      <input <?php if(isset($_POST['start'])){ ?>
                      value="<?php echo date('Y-m-d',strtotime($startDate)); ?>"
                      <?php } else{ ?> value="<?php echo date('Y-m-01'); ?>" <?php } ?> type="text" class="form-control p-0" name="start"/>
                        <div class="input-group-prepend">
                         <span class="input-group-text">to</span>
                        </div>
                        <input <?php if(isset($_POST['start'])){ ?>
                        value="<?php echo date('Y-m-d',strtotime($endDate)); ?>"
                      <?php } else{ ?>value="<?php echo date('Y-m-d'); ?>"<?php } ?> type="text" class="form-control p-0" name="end"/>
                      <div class="input-group-prepend">
                        <button type="submit" data-toggle="tooltip" title="Search" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                          
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>Date & Time</th>
                    <th>Visitor Name</th>
                    <th>Contact No.</th>
                    <th>No. of Person</th>
                    <th>Whom To Meet</th>
                    <th>Visiting Reason</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 $i=1;
                
                 if($_GET['from']!= '' && $_GET['toDate']!= ''){
                    extract(array_map("test_input" , $_GET));
                    $date=date_create($from);
                    $dateTo=date_create($toDate);
                    $from= date_format($date,"Y-m-d");
                    $toDate= date_format($dateTo,"Y-m-d");
                 }else{
                    $toDate = date('Y-m-d');
                    $from = date('Y-m-d', strtotime('-1 day'));
                 }
                    
                  $i=1; 
                 $sql =$d->selectRow("visitors_master.*,users_master.user_full_name,floors_master.floor_name","visitors_master,users_master,floors_master","users_master.user_id=visitors_master.user_id AND floors_master.floor_id=visitors_master.floor_id AND visitors_master.society_id='$_COOKIE[society_id]' AND visitors_master.block_id='$_COOKIE[block_id]' AND visitors_master.visit_date BETWEEN '$from' AND '$toDate'","ORDER BY visitors_master.visitor_id DESC");

                 while ($response=mysqli_fetch_array($sql)) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php if($response['entry_by']==1){ 
                     if($response['visitor_status']==2){ ?>
                      <button title="Decline Leave" type="button" class="btn btn-sm btn-danger ml-1" onclick="visitorExitFunction('<?php echo $response['visitor_id']; ?>','<?php echo $response['visit_date']; ?>','<?php echo $response['visit_time']; ?>')">Exit</button>
                        <?php }else{ ?> Exit <?php } }else{ ?>   <?php } ?>
                    </td>
                    <td><?php echo date('d-M-Y', strtotime($response['visit_date'])); ?> <?php echo date('h:i A', strtotime($response['visit_time'])); ?></td>
                    <td><?php echo $response['visitor_name']; ?></td>
                    <td><?php echo $response['visitor_mobile']; ?></td>
                    <td><?php echo $response['number_of_visitor']; ?></td>
                    <td><?php echo $response['user_full_name']; ?> (<?php echo $response['floor_name']; ?>)</td>
                    <td><?php echo $response['visiting_reason']; ?></td>
                  </tr>
                <?php } ?> 
              </tbody>
            </div>
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!-- End Row-->
</div>
</div>

<div class="modal fade" id="visitorExitModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Visitor Exit</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="leaveStatusApproved" action="controller/commercialEntryController.php" enctype="multipart/form-data" method="post">  
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Date <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <input type="text" name="exit_date" id="date-picker-commercial" class="form-control" required value="<?php echo date('Y-m-d'); ?>">
                        </div>                   
                    </div>                 
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Time <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <input type="text" name="exit_time" id="time-picker-commercial" class="form-control" required value="<?php echo date('H:i:s'); ?>">
                        </div>                   
                    </div>    
                    <div class="form-footer text-center">
                      <input type="hidden" name="visitorExit"  value="visitorExit">
                      <input type="hidden" name="visitor_id"  id="visitor_id">
                      <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Submit </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<?php include "common/footer.php"; ?>
<script type="text/javascript">
  var visit_date = ''
  var visit_time = ''
  var FromEndDate = new Date();
  $('#date-picker-commercial').bootstrapMaterialDatePicker({
    time: false,
    maxDate: FromEndDate,
    minDate: visit_date
  });
  $('#time-picker-commercial').bootstrapMaterialDatePicker({
    date: false,
    format: 'hh:mm ss',
    maxDate: FromEndDate,
  });

function visitorExitFunction(id,date,time){
  $('#visitorExitModal').modal();
  $('#visitor_id').val(id);
  visit_date = date;
  visit_time = time;
}
  
</script>