<?php include "common/header.php"; ?>
<?php include "common/sidebar.php"; ?>
	
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Users</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="welcome.php">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add User</li>
            </ol>
        </div>
    </div>

    <div class="card card-body">
        <form id="addUserValidation" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            <h4 class="form-header text-uppercase">
                <i class="fa fa-file"></i>
               Add User
            </h4>
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="addUser" >
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                <div class="row">
                    <label class="col-sm-2 text-uppercase">Name <span class="required">*</span></label>
                    <div class="form-group col-sm-4">
                        <input type="text" required="" autocomplete="off" name="name" id="name" class="form-control">
                    </div>
                    <label class="col-sm-2 text-uppercase">Unit <span class="required">*</span></label>
                    <div class="form-group col-sm-4">
                        <select class="form-control single-select" name="unit_id"> 
                            <option value="">--SELECT--</option>
                            <?php  $q3=$d->select("unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.society_id='$_COOKIE[society_id]' AND unit_master.unit_status!=0 ","ORDER BY unit_master.unit_id ASC");
                            while ($data = mysqli_fetch_array($q3)) {
                            ?>
                            <option value="<?php echo $data['unit_id'] ?>"><?php echo $data['block_name']." - ".$data['unit_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
             </div> 
            <div class="col-md-12">
                <div class="row">
                    <label class="col-sm-2 text-uppercase">Contact No. <span class="required">*</span></label>
                    <div class="form-group col-sm-4">
                        <input type="text" name="phone" id="phone" onblur="checkMobile(this.value)" class="form-control number" required="" autocomplete="off" minlength="10" maxlength="10">
                        <span id="error-mobile" class="text-danger mt-1 font-weight-bold"></span>
                    </div>
                    <label class="col-sm-2 text-uppercase">Password <span class="required">*</span></label>
                    <div class="form-group col-sm-4">
                        <input type="password" name="password" id="password" class="form-control" required="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <label class="col-sm-2 text-uppercase">Profile</label>
                    <div class="form-group col-sm-4">
                        <input type="file" name="profile" id="imgInp" class="form-control-file border" autocomplete="off">
                    </div>
                    <div class="form-group col-sm-6 text-center">
                        <img id="blah" class="rounded-circle m-auto" src='../apAdmin/img/user.png' width="75" height="75"  alt="your image" class='profile' />
                    </div>
                </div>
            </div>
            
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
  </div>
</div><!-- End Breadcrumb-->
<?php include "common/footer.php"; ?>
<script type="text/javascript">
    jQuery.validator.addMethod("image", function (value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, jQuery.validator.format("Please add a valid image file."));

    $.validator.addMethod('filesize', function (value, element, arg) {
        var size =2000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }, $.validator.format("file size must be less than or equal to 2MB."));

    $("#addUserValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            name: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            unit_id: {
                required: true,
            },
            phone:{
                required:true,
                digits: true
            }, 
            profile:{
                image: true,
                filesize: true,

            },
            password:{
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 12,
                minlength: 5,
            },
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
        }
         
    });
</script>

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
<script type="text/javascript">

    function checkMobile(value) {
        $.ajax({
            url: "getMobileNumber.php",
            cache: false,
            type: "POST",
            data: {mobile : value},
            success: function(response){
                if (response==1) {
                    $('#error-mobile').html("Mobile already Registered");
                }
                  
            }
        });
    }                   
</script>